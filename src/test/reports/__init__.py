import gdtlive.store.db as db
from gdt.constants import DIR_STR

ACCOUNT_ID = 3

db.init()


session = db.Session()

runids = session.query(db.LiveRunConfig.strategyrun_id).filter(db.LiveRunConfig.brokerAccountId==ACCOUNT_ID).all()

print 'Amount, Open time, Open price, Close price, Symbol' 
for runid, in runids:
    trades = session.query(db.Trade).filter(db.Trade.strategyrun_id==runid).order_by(db.Trade.id).all()
    for trade in trades:
        print "%d, %s, %f, %f, %s" % (trade.amount, trade.open_time.strftime("%Y-%m-%d %H:%M"), trade.open_price, trade.close_price, trade.symbol)
        #print trade.id, trade.symbol, DIR_STR[trade.direction], trade.open_time, trade.open_price, trade.close_time, trade.close_price
        
        
        
session.close()
    
    

