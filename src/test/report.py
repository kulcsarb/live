'''
Created on Nov 14, 2012

@author: gdt
'''
import gdtlive.store.db as db
from datetime import datetime
from gdtlive.constants import *
from gdtlive.core.constants import *

ACCOUNT_ID = 1
FROM = datetime(2012,11,1)
TO = datetime(2012,11,15)


db.SQL_USER = "gdt"
db.SQL_PASS = "gdt" 
#db.SQL_URL = "localhost/live"
db.SQL_URL = "loft3/live_november"

#----Main----
print "Initing DB connection..."
db.init()
print "SQL init done."


session = db.Session()

info = session.query(db.AccountData).get(ACCOUNT_ID)
if not info:
    print 'Account not found !' 
print info.name
print info.mmplugin_parameters

liverunconfigs = session.query(db.LiveRunConfig).filter(db.LiveRunConfig.brokerAccountId == ACCOUNT_ID)
for lrc in liverunconfigs:
    trades = session.query(db.Trade).filter(db.Trade.strategyrun_id == lrc.strategyRunId).filter(db.Trade.open_time >= FROM).filter(db.Trade.open_time < TO).filter(db.Trade.close_time >= FROM).filter(db.Trade.close_time < TO).all()
    if trades:    
        #print 'Strategy ID #%d   current open trades: %d, current profit: %.2f, current floating profit: %.2f' % (lrc.id, lrc.opentrades or 0, lrc.net_profit or 0, lrc.floating_profit or 0)
        for trade in trades:
            print 'Trade %d: %s %d %s @ %f,  SL: %f TP: %f \t %s\n\topen:\t%s\t%f\n\tclose:\t%s\t%f\treason: %s' % \
                ( trade.id, DIR_STR[trade.direction], trade.amount, trade.symbol, trade.req_price, trade.sl_price, trade.tp_price, TRADE_STR[trade.state], 
                  trade.open_time, trade.open_price, 
                  trade.close_time, trade.close_price, ORDERTYPE_STR[trade.close_type])
            print '\tprofit: $%f' % (trade.profit or trade.floating_profit)
