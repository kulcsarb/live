
import gdtlive.core.fix.dukascopy.fix as fix
#from fix.dukascopy.servertests import ServerTester
#from gdtlive.core.fix.messages import *
from gdtlive.core.fix.initiator import FIXInitiator
from logging.handlers import RotatingFileHandler 
import logging
import traceback
import time
import socket
from datetime import datetime, timedelta
import ssl
import pprint 
import re

URL = 'demo-api.dukascopy.com'
FEED_PORT = 9443
TRADE_PORT = 10443
SENDERCOMPID = 'DEMO3NxKxd_DEMOFIX'
TRADE_TARGETCOMPID = 'DUKASCOPYFIX'
FEED_TARGETCOMPID = 'DUKASCOPYFIX'
PASSWORD = 'NxKxd'
USERNAME = 'DEMO3NxKxd'


class SimpleFixAccount():

    def __init__(self):
        self.MsgSeqNum = 0
        self.last_send_time = None
        self.heartbeat_interval = 0
        self.port = TRADE_PORT
        self.url = URL
        self.SenderCompID = SENDERCOMPID
        self.TargetCompID = TRADE_TARGETCOMPID
        self.show_raw_message = False
        self.show_message = True
        self.exposure = {}
        self.trades = {}
        self.stops = {}
        
        
    def connect(self):
        self.send_heartbeat = False        
        self.MsgSeqNum = 1
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket = ssl.wrap_socket(self.socket)
        self.socket.settimeout(1)        
        self.socket.connect((self.url, self.port))
        
    
    def login(self, username, password):
        self.send(fix.Logon(Password=password, Username=username, HeartBtInt=60, ResetSeqNumFlag=True, EncryptMethod=0))        
    
    def logout(self):
        self.send(fix.Logout())
        
    def disconnect(self):
        try:
            self.socket.close()
        except:
            pass
          
          
    def send(self, message):        
        message.MsgSeqNum = self.MsgSeqNum
        message.SendingTime = datetime.utcnow()        
        message.SenderCompID = self.SenderCompID
        message.TargetCompID = self.TargetCompID
        if self.show_message:
            print 'OUT:', datetime.utcnow(), '\t', message
        encoded = fix.encode(message)
        if self.show_raw_message:
            print 'OUT:', datetime.utcnow(), '\t', pprint.pprint(encoded)        
        self.socket.send(encoded)
        self.last_send_time = datetime.utcnow()
        self.MsgSeqNum += 1
    
    
    def receive(self):
        
        self.buffer = ''
        while True:
            try:
                data = self.socket.recv(4096)
                if not data:
                    print datetime.utcnow(), 'Connection closed', data
                    raise Exception('connection closed!')
                self.buffer += data
                
                for message in self.decode_messages():
                    yield message                                
            
            except socket.error:
                break


    def decode_messages(self):
        while True:        
            match = re.search(fix.RE_MESSAGE_PATTERN, self.buffer, re.M)
            if not match:                
                break
                                                            
            raw_message = match.group(0)                    
            self.buffer = self.buffer[len(raw_message):]
            try:
                if self.show_raw_message:
                    print 'IN: ', datetime.utcnow(), '\t', pprint.pprint(raw_message)                
                yield fix.decode(raw_message)
            except GeneratorExit:
                pass
    
    
    def market_order(self, symbol, amount):
        self.send(fix.NewOrderSingle(ClOrdID='GDT_MO_%d' % time.time(), 
                                                Currency=symbol[:3],
                                                Symbol=symbol, 
                                                Side=fix.Side.BUY if amount > 0 else fix.Side.SELL, 
                                                OrderQty=abs(amount),                                        
                                                TransactTime = datetime.utcnow(),
                                                OrdType = fix.OrdType.MARKET,
                                                TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL,
                                                Slippage = 20
                                                ))
        
    
    def stop_order(self, symbol, amount, price):
        self.send(fix.NewOrderSingle(ClOrdID='GDT_SL_%d' % time.time() , 
                                            Currency=symbol[:3],
                                            Symbol=symbol, 
                                            Side=fix.Side.SELL if amount > 0 else fix.Side.BUY, 
                                            OrderQty= abs(amount),
                                            Price = round(price, 4),
                                            TransactTime = datetime.utcnow(),
                                            OrdType = fix.OrdType.STOP,
                                            TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL,
                                            Slippage = 10,
                                            ))
        
    
    def cancel_stop(self, orig_clordid, order_id, side, symbol):
        self.send(fix.OrderCancelRequest(ClOrdID='GDT_CANCEL_%d' % time.time(),
                                 OrigClOrdID=orig_clordid,
                                 OrderID = order_id,
                                 Side = side,
                                 TransactTime = datetime.utcnow(),    
                                 Symbol = symbol))
        
        
    def modify_stop(self, message, amount, sl_price):
        self.send(fix.OrderCancelReplaceRequest(ClOrdID='GDT_MODIFY_%d' % time.time(),
                                 OrigClOrdID = message.ClOrdID,
                                 OrderID = message.OrderID,
                                 OrdType = fix.OrdType.STOP,
                                 OrderQty = abs(amount),
                                 Price = round(sl_price, 4),                                                                  
                                 Side = fix.Side.SELL if amount > 0 else fix.Side.BUY,
                                 Symbol = message.Symbol,
                                 TransactTime = datetime.utcnow(),                                     
                                 Slippage = 10))                                                     
    

    def process_response(self):
        self.stops = {}
        for message in self.receive():                
            print 'IN', message
            if type(message) == fix.InstrumentPositionInfo:
                self.exposure[message.Symbol] = message.Amount
            if type(message) == fix.ExecutionReport:                
                if message.OrdStatus == fix.OrdStatus.PENDING_NEW and message.OrdType == fix.OrdType.STOP:                    
                    self.stops[message.Symbol] = message
                    
    
    def close_positions(self):
        for symbol in self.exposure:
            if self.exposure[symbol]:                        
                self.market_order(symbol, -1 * self.exposure[symbol])
                

    def open_positions(self):
        for symbol, (amount, sl) in self.trades.items():
            if amount:                        
                self.market_order(symbol, amount)
                if sl:
                    self.stop_order(symbol, amount, sl)


    def cancel_all_stop(self):
        while True:                                        
            for message in self.stops.values():                        
                self.cancel_stop(message.ClOrdID, message.OrderID, message.Side, message.AvgPx)        
            
            self.status_request()
            self.process_response()
            
            if not self.stops:
                print 'All SL cancelled'
                break
            print 'cancelling remaining SLs'
                
    
                
    
    def adjust_positions(self):
        for symbol, (amount, sl_price) in self.trades.items():            
            difference = amount - self.exposure.get(symbol,0)
            print 'DIFFERENCE FOR ', symbol, difference
            if difference:
                self.market_order(symbol, difference)
                                                                        
            if sl_price:
                if symbol in self.stops:                                        
                    self.modify_stop(self.stops[symbol], amount, sl_price)                    
                else:
                    self.stop_order(symbol, amount, sl_price)
            else:
                if symbol in self.stops:
                    self.cancel_stop(self.stops[symbol].ClOrdID, self.stops[symbol].OrderID, self.stops[symbol].Side, symbol)
                    
                
    def status_request(self):
        self.send(fix.OrderMassStatusRequest(MassStatusReqType = fix.MassStatusReqType.STATUS_FOR_ALL_ORDERS, MassStatusReqID='gdt'))
            
            
    def transfer(self, trades, mode='ADJUST'):
        try:
            self.mode = mode
            self.trades = trades
            self.connect()
            self.login(USERNAME, PASSWORD)
            self.status_request()                                
            self.process_response()
            
            print 'OLD EXPOSURE'
            print self.exposure
            if self.stops:
                print 'OLD SL'
                print self.stops['EUR/USD']
                                    
            if mode == 'ADJUST':
                print 'ADJUSTING POSITONS...'
                self.adjust_positions()                
            elif mode == 'CLOSE':
                print 'CLOSING POSITIONS...'
                self.cancel_all_stop()                    
                self.close_positions()             
                print 'OPENING POSITIONS...'
                self.open_positions()                            
            
            self.status_request()
            self.process_response()                                      
            self.logout()
            result = True           
        except Exception as e:
            print e
            raise e                                             
        finally:
            self.disconnect()
            print '----------------------------'
            print 'NEW EXPOSURE'
            print self.exposure
            if self.stops:
                print 'NEW SL'
                print self.stops['EUR/USD']
            
        return result
    
    def transferred(self, trades):
        if self.mode != 'ADJUST' and self.mode != 'CLOSE':
            return True
        for symbol in trades:            
            amount = trades[symbol][0]
            sl = trades[symbol][1]
            
            if self.exposure.get(symbol,0) != amount:
                return False
             
            if sl :
                sl_dir = (fix.Side.BUY if amount < 0 else fix.Side.SELL)
                if symbol not in self.stops or \
                    self.stops[symbol].AvgPx != sl or \
                    self.stops[symbol].OrderQty != amount or \
                    self.stops[symbol].Side != sl_dir:                    
                    return False 
                
        return True
        
    
    def safe_transfer(self, trade, max_retries = 10, mode='ADJUST'):
        wait = 1
        retries = max_retries or 1
        while retries:        
            try:
                self.transfer(trades, mode)
                if self.transferred(trades):
                    print '--- TRANSFER OK ---' 
                    break            
                print 'waiting for %d sec before retry' % wait
            except:
                print '--- TRANSFER FAILED -- '
                
            time.sleep(wait)
            wait += 1
            retries -= 1
            
                        
            
if __name__ == '__main__':
    server = SimpleFixAccount()
    trades = {'EUR/USD': (1000, 1.3576)}    
    server.safe_transfer(trades, 5, 'ADJUST')
    
