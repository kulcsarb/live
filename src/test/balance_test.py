'''
Created on Mar 19, 2013

@author: gdt
'''
import gdtlive.store.db as db 
from gdtlive.admin.system.log import initialize_loggers

db.SQL_USER = 'gdtlive'
db.SQL_PASS = 'gdt'
db.SQL_URL = 'loft1:54320/live1'

ACCOUNT_ID = 1

initialize_loggers()
db.init()
session = db.Session()

trades = session.query(db.Trade).filter(db.Trade.account_id == ACCOUNT_ID).order_by(db.Trade.open_time).all()

pos_tnum = 0
pos_sum = 0
neg_tnum = 0
neg_sum = 0
for trade in trades:
    ot = '' if not trade.open_time else trade.open_time.strftime("%Y-%m-%d %H:%M")
    ct = '' if not trade.close_time else  trade.close_time.strftime("%Y-%m-%d %H:%M")
    print '%d;%s;%s;%d;%.2f;%.1f' % (trade.id, ot, ct, trade.amount, round(trade.profit, 2), round(abs(trade.close_price - trade.open_price)/0.0001,1))
    if trade.profit < 0:
        neg_tnum += 1
        neg_sum += trade.profit
    else:
        pos_tnum += 1
        pos_sum += trade.profit
        
print '---------------------------------'
print pos_tnum, pos_sum
print neg_tnum, neg_sum
print '---------------'
print pos_tnum - neg_tnum, pos_sum + neg_sum






session.close()
