'''
Created on Dec 3, 2012

@author: gdt
'''
import socket
import json
from gdtlive.utils import OrderedDict
import pprint
import traceback
from datetime import datetime
import calendar

#buff = '{"msgType":"HISTORY","key":0,"msg":{"symbol":"EUR A0-FX","period":"INTRADAY:15","headers":["STARTTIME","OPEN","HIGH","LOW","CLOSE","VOLUME","OPEN_INTEREST"],"bars":[[1354533300,1.30237,1.303,1.302,1.3025,6311.0,0.0],[1354534200,1.3025,1.30342,1.302,1.303,5722.,0.],[1354535100,1.3032,1.30347,1.3024,1.3024,7402.,0.],[1354536000,1.30266,1.304,1.3023,1.3038,7601.,0.],[1354536900,1.3038,1.30393,1.3032,1.30366,725.0,0.0]]}}\r\r'
#
#
#msg = json.loads(buff)
#
#print msg['msg']['bars'][0]
#
#for bar in msg['msg']['bars']:
#    print datetime.utcfromtimestamp(bar[0]), bar[1], bar[2], bar[3], bar[4]


#quit()


auth = {} #OrderedDict()

auth["msg"] = {"username" : "GENOMEAPI",
                 "pwd" : "GENOMEAPI",
#                 'properties' : {
#                                 "app_name" : 'IDC JSON Silverlight Sample - Streaming Data',
#                                 'app_version' : '1.0'
#                                 }
                }
auth["msgType"] = "AUTHENTICATE"        

 
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(("mq-beta.esignal.com",2812))
sock.settimeout(1)

print 'connected'
print 'auth', json.dumps(auth)
sock.sendall(json.dumps(auth)+'\r\n')
print 'auth sended'

snapshot = {"msgType":'SNAPSHOT',"msg": {"symbols": ['EURUSD']}}
advise = {"msgType" : 'ADVISE', 'msg' : {'symbols' : ['JPY A0-FX'], 'fields': ["BID", "ASK"]}}
#history = {"msgType": "HISTORY", "msg": {"symbol": 'EUR A0-FX', "period": "INTRADAY:15", "bars": 100}}

start = datetime(2012,6,10)
end = datetime(2012,6,15)
print calendar.timegm(start.utctimetuple())
print calendar.timegm(end.utctimetuple())

history = {"msgType": "HISTORY", "msg": {"symbol": 'EUR A0-FX', "period": "INTRADAY:15", 'start':calendar.timegm(start.utctimetuple()), 'end': calendar.timegm(end.utctimetuple())}}
heartbeat = {'msgType' : 'HEARTBEAT', 'key':0}


def decode(buffer):
    index = buffer.find('\r\r')
    if index != -1:
        message = buffer[:index]
        buffer = buffer[index+2:]            
        message = message.replace('.,','.0,').replace('.]','.0]').replace('.}','.0}')        
        return json.loads(message), buffer
    
    return None, buffer
    
    
    
def print_history(message):
    print datetime.utcnow(), 
    print len(message['msg']['bars'])
    for bar in message['msg']['bars']:
        print datetime.utcfromtimestamp(bar[0]), bar[1], bar[2], bar[3], bar[4]
    

a = 1
buff = ''
while True:
    try:        
        reply = sock.recv(4096)
        if not reply:
            print 'connection closed'
            break
        #print reply
        buff += reply
        print reply
        try:
            message, buff = decode(buff)                           
            #message = decode(message)        
            if message:
                if message['msgType'] == 'HISTORY':
                    sock.sendall(json.dumps(heartbeat)+'\r\n')
                if message['msgType'] == 'AUTHENTICATE':
                    sock.sendall(json.dumps(history)+'\r\n')
                if message['msgType'] == 'HISTORY':
                    print_history(message)
        except:
            print traceback.format_exc()
            
    except:
        pass
        #print 'send', datetime.utcnow()
        #sock.sendall(json.dumps(history)+'\r\n')
#    except:
#        print traceback.format_exc()
#        buff = ''
#        pass




