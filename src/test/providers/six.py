'''
Created on Dec 7, 2012

@author: gdt
'''
import urllib
from xml.dom.minidom import parseString
from gdtlive.utils import OrderedDict
import copy
URL = "apidintegra.tkfweb.com"
PORT = 80

CUSTOMERNO = "AT27613"
CLIENTID = 'Genome'
USERID = "AT27613-apigenfiint02"
PASSWORD = "telekurs"
SESSIONID = 942900039

PARAMS = {          
          'ui' : USERID,
          'id' : SESSIONID,
          'ci' : CLIENTID  
          }

def logon():
    global SESSIONID
    #http://[server-url]/request?method=login&ci=[client-id]&ui=[user-id]&pwd=[password]
    u = urllib.urlopen('http://%s/apid/request?method=login&ci=%s&ui=%s&pwd=%s' % (URL, CLIENTID, USERID, PASSWORD))
    data = u.read()
    print data
    dom = parseString(data)
    SESSION = dom.documentElement.childNodes[0].attributes['v'].nodeValue
    print SESSIONID
    
#    r = ['<?xml version="1.0" encoding="ISO-8859-1"?>', '<!DOCTYPE XRF SYSTEM "/apid/xml/xrf.dtd">', '<XRF r="8.6.2" c="Genome" g="" u="AT27613-apigenfiint02" k="" d="20121214" t="111042">', '<A k="i0005" n="3" v="942900039"/>', '</XRF>']
#s = ''.join(r)
#dom = parseString(s)
# print 'SerialID:', dom.documentElement.childNodes[0].attributes['v'].nodeValue 

    return None


def send_request(params):    
    params.update(PARAMS)
    url_params = '&'.join(['%s=%s' % (key, value)  for key, value in params.items()])    
    u = urllib.urlopen('http://%s/apid/request?%s' % (URL, url_params))
    print u.read()
    

def getHIKU():
    params = {}                
    params['method'] = 'getHikuData'    
    params['date_from'] = '01.12.2012'
    params['date_to'] = '14.12.2012'
    params['ik'] = r'946854%2C148%2C474'
    params['pk'] = r'3%2C4'
    params['pts'] = '1'
    
    return send_request(params)
               
    #url_params = get_urlparams(params) 
    
    
    #url_params = r'ci=GENOME&date_from=12.12.2011&date_to=11.12.2012&id=942900039&ik=946854%2C148%2C474&method=getHikuData&pk=3%2C4&pts=1&ui=AT27613-apigenfiint02'
    #u = urllib.urlopen('http://%s/apid/request?%s' % (URL, url_params))
        
    #return u.read()
#    http://apidintegra.tkfweb.com/apid/request?
#ci=Genome&ui=AT27613-apigenfiint02&date_to=12.12.2012&pk=3%2C4&date_from=12.1.2012&pts=1&id=942900039&ik=946854%2C148%2C474&method=getHikuData
#ci=apiD_D&date_from=12.12.2011&date_to=11.12.2012&id=942900039&ik=946854%2C148%2C474&method=getHikuData&pk=3%2C4&pts=1&ui=AT27613-apigenfiint02
    

#dom = parseString()

#print logon()
    

def getXMLTable(table_code):
    
    params = {'method' : 'getXMLTable',
              'name' : str(table_code)}    
    data = send_request(params)            
    return data
    
    

def getTimeSeries(ik):
    params = {
              'method' : 'getTimeSeries',
              'ik' : ik,
              'pk' : '1,2,3',
              'mode' : 'snap',
#              'max' : '5',
              'period' : '15m',
              'date_from' : '03.12.2012',
              'date_to' : '07.12.2012',               
              }
    return send_request(params)
    



def getEntitlement():
    params = {
              'method' : 'getEntitlement',
              'mode' : 'all'
              }
    return send_request(params)


def getExchangeListing(code):
    params = {
              'method' : 'getExchangeListings',
              'bc' : str(code)
              }
    return send_request(params)


def getListingData(ik):
    params = {
              'method' : 'getListingData',
              'mk' : 'avail',
              'pk' : 'avail',
              'ik1' : str(ik)              
              }
    return send_request(params)

#FOREX RATES EXC: 148
#FOREX CALCULATED RATES EXc: 149
#print getEntitlement()
print getListingData("2447524,149,366")
#print getExchangeListing(148)
#print getExchangeListing(213)
#print getXMLTable(713)
#print getXMLTable(2)
#print getXMLTable(74)

#print getTimeSeries('198902,149,333')

#print getHIKU()    


    
# http://[server-url]/request?method=getTimeSeries&ik=1222171,380,1&pk=1&mode=snap&max=5&period=15m&date_from=05.06.2011&date_to=10.06.2011&id=[session-key]&ci=[client-id]&ui=[user-id]

