'''
Created on Mar 4, 2013

@author: gdt
'''
from gdt.admin.system.log import initialize_loggers
from gdt.evol.plugin import individual_list
from gdtlive.admin.system.log import initialize_loggers as initialize_live_loggers
import gdt.store.db as db
import gdtlive.store.db as livedb 
from datetime import date

# 3, EURUSD, 100
# 4, GBPUSD, 100
# 5, GBPUSD, 150
# 6, AUDUSD, 150
# 7, USDCAD, 150
# 8, USDJPY, 150

ID = 8
symbol = 'USDJPY'
mmplugin_parameters = {"positionsize":100,"positionmode":2,"sltype":3,"riskmanagement":1,"modifysl":0,"maxrisk":150,"setsl":1,"maxnumoftrades":1,"margincall_rate":50}
timeframe = 1440
from_date = date(2012,1,2) 
to_date = date(2013,8,17)
#name = "EURUSD 344/500"

description = "" 

"""source: database/predict_2year_allinstruments
evolconfig: 5
selection filter: best 10 of losing_trades, net_profit
selection backtest: 1,5y
inverted: no
check period: 1 month
runs: 2
number of strategies: 40
SL: 100
"""


initialize_loggers()
initialize_live_loggers()

SQL_PASS = 'gdt'
SQL_USER = 'gdt'
SQL_URL = 'database/predict_candidates_201308'

livedb.SQL_USER = 'gdtlive'
livedb.SQL_PASS = 'gdt'
livedb.SQL_URL = 'localhost:54320/live_predict'

db.init(False, SQL_USER, SQL_PASS, SQL_URL)
session = db.Session()

livedb.init()
livesession = livedb.Session()


print 'strategy', ID, 
dns, name, = session.query(db.Strategy.dns, db.EntityName.element_name).filter(db.Strategy.id == ID).filter(db.EntityName.element_id == ID).filter(db.EntityName.type==db.ELEMENT_TYPE_STRATEGY).first()
mmplugin_parameters = str(mmplugin_parameters)

#print config.to_date, individual_list[eval(config.evolplugin_parameters)['individual_type']].__name__
print '\t',name , symbol, timeframe, mmplugin_parameters

strategy = livedb.Strategy(dns, timeframe, symbol, 1, mmplugin_parameters)
strategy.description = description
strategy.evolved_from = from_date
strategy.evolved_to = to_date
strategy.individual = "GNetEchoPredictMulti"
strategy.stratplugin_id = 4
strategy.weekend_start = 20
livesession.add(strategy)
livesession.commit()
name = db.EntityName(5, strategy.id, name)
livesession.add(name)
livesession.commit()
livesession.close()
session.close()
print 'END'

