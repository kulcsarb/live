'''
Created on Sep 18, 2013

@author: gdt
'''
import gdtlive.store.db as db
import gdtlive.historic.plugin.dukascopy_fs as dukas
import gdtlive.historic.datarow
from datetime import date
import re


text = "Position 26151EUR/USD ENTRY #99865959 price changed from 1.32567 USD to 1.3508 USD)"
filter = 'Position \d+[A-Z]{3}/[A-Z]{3} ENTRY #(\d+) price changed from ([\d\.]+) [A-Z]{3} to ([\d\.]+) [A-Z]{3}'
m = re.search(filter, text)
print m.groups()


text='Order CANCELLED: #99866041 ENTRY SELL 0.001 mil. GBP/USD @ MKT IF BID <= 1.57542 - Position #26151GBP/USD(TRADE-8, Stoploss-6 1379462443)'
filter ='Order CANCELLED: #(\d+)'
m = re.search(filter, text)
print m.groups()



#db.init()
#dukas.initialize_datarow_descriptors()            
#dukas.cleanup_db()

#datarow = gdtlive.historic.datarow.load(2, date(2013,9,1), date.today())
#print datarow[0][-1],