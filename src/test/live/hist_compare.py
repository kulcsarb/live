'''
Created on Oct 17, 2012

@author: gdt
'''
from datetime import datetime

#with open('/home/gdt/hist2012_newdownload.txt') as f:
with open('/home/gdt/hist.txt') as f:
    hist1 = f.readlines()

#with open('/home/gdt/hist2012_olddownload.txt') as f:
with open('/home/gdt/hist-office.txt') as f:
    hist2 = f.readlines()
    
for i in xrange(len(hist1)):
    if hist1[i] != hist2[i]:        
        d1, t1, a1, b1 = hist1[i].split(' ')
        d2, t2, a2, b2 = hist2[i].split(' ')
        if d1 != d2 or t1 != t2:
            dt1 = datetime.strptime(d1+t1, '%Y-%m-%d%H:%M:%S')
            dt2 = datetime.strptime(d2+t2, '%Y-%m-%d%H:%M:%S')            
            #print dt1, dt2
#            if dt1 > dt2:
#                print dt1, a1, b1, 'is missing from hist2'
#            if dt1 < dt2:
#                print dt2, a2, b2, 'is missing from hist1'
            #break
        print d1, t1, '%.5f' % (float(a1) - float(a2)), '%.5f' % (float(b1) - float(b2))
    else:
        print '-'
        


