'''
Created on Oct 16, 2012

@author: gdt
'''

import gdtlive.store.db as db 
from gdt.evol.gnodes.GenomeBase import GTree


db.init()

session = db.Session()

DNS = session.query(db.Strategy.dns).all()
genome = None

for dns, in DNS:
    if not genome:        
        genome = GTree.decode(dns)
    else:
        genome.mergeWith(GTree.decode(dns)) 
    
    
print genome
session.close()


