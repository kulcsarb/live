'''
Created on Oct 17, 2012

@author: gdt
'''
import glob

A = '/tmp/historic/EURUSD/2012'
B = '/var/spool/gdt/historic/EURUSD/2012'

A_files = []
B_files = []

for m in sorted(glob.glob(A+'/*')):
    for d in sorted(glob.glob(m+'/[0-9][0-9]*')):
        A_files.append(d+'/candles_min_15.bin')

for m in sorted(glob.glob(B+'/*')):
    for d in sorted(glob.glob(m+'/[0-9][0-9]*')):
        B_files.append(d+'/candles_min_15.bin')

#print A_files
#print B_files

for i in xrange(len(A_files)):    
    with open(A_files[i], 'rb') as f:
        contA = f.read()
    with open(B_files[i], 'rb') as f:
        contB = f.read()
    
    if contA != contB:
        print A_files[i]    
        print B_files[i]
        print 
    
        




