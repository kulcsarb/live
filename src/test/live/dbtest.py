'''
Created on Nov 14, 2012

@author: gdt
'''
import gdtlive.store.db as db
import random
from datetime import datetime
from threading import Thread
from multiprocessing import Process
import time
from gdtlive.admin.system.log import initialize_loggers
import logging
import multiprocessing





db.SQL_USER = "gdt"
db.SQL_PASS = "gdt" 
db.SQL_URL = "188.138.124.44/live_test"

initialize_loggers()
        
#----Main----
print "Initing DB connection..."
db.init()
print "SQL init done."

log = logging.getLogger('sqlalchemy')
log.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler()    
log.addHandler(console_handler)


table = db.LiveRunConfig.__table__     
db.engine.execute(table.update().where(table.c.id==1).\
                  values(opentrades=random.randint(1,5), floating_profit=random.randint(10,100), last_checked_time=datetime.now(), running_from=datetime.now(), running_to=datetime.now()))


quit()

def test(cid):
    for x in xrange(1000):        
        table = db.LiveRunConfig.__table__     
        s = time.time()
        db.engine.execute(table.update().where(table.c.id==cid).\
                          values(opentrades=random.randint(1,5), floating_profit=random.randint(10,100), last_checked_time=datetime.now(), running_from=datetime.now(), running_to=datetime.now()))
        print cid, x, '%.4f' % (time.time() - s)


threads = []
for i in range(10):
#    t1 = Thread(target=test, args=(22,))
#    t2 = Thread(target=test, args=(23,))
#    t3 = Thread(target=test, args=(24,))
#    t4 = Thread(target=test, args=(25,))
#    t5 = Thread(target=test, args=(26,))
#    t6 = Thread(target=test, args=(27,))
#    t7 = Thread(target=test, args=(28,))
    t1 = Process(target=test, args=(1,))
    t2 = Process(target=test, args=(2,))
    t3 = Process(target=test, args=(3,))
    t4 = Process(target=test, args=(4,))
    t5 = Process(target=test, args=(5,))
    t6 = Process(target=test, args=(6,))
    t7 = Process(target=test, args=(7,))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t5.start()
    t6.start()
    t7.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
    t5.join()
    t6.join()
    t7.join()
    print '---------------------------------------------------------------------------------'
