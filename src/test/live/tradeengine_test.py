'''
Created on 2012.09.28.

@author: kulcsarb
'''
from datetime import datetime, timedelta
import gdtlive.store.db as db
from gdtlive.core.tradeengine import TradeEngine
from gdtlive.evol.gnodes.GenomeBase import GTree
from gdtlive.core.datafeed import DatarowBuilder
import gdtlive.core.datafeed
import gdtlive.core.datafeed.historic as historic
import gdtlive.core.tradeengine
import gdtlive.historic.plugin.dukascopy_fs
import logging
from gdtlive.core.constants import LOG_FORMAT
from gdtlive.constants import PRICE_TIME, PRICE_ASKCLOSE, PRICE_BIDCLOSE, PRICE_ASKOPEN,\
    PRICE_BIDOPEN, PRICE_ASKLOW, PRICE_BIDLOW, PRICE_ASKHIGH, PRICE_BIDHIGH, PRICE_PIPVALUE,\
    ORDERTYPE_CLOSE_BY_SL, ORDERTYPE_CLOSE_BY_TP, ORDERTYPE_CLOSE
from logging.handlers import RotatingFileHandler
import gdtlive.config as config
import os    
import traceback
import socket
import ssl
import time
import calendar
import gdtlive.core.fix.dukascopy.fix as fix
import gdtlive.core.fix.dukascopy.messages
import numpy as np

from Queue import Queue

TIMEFRAME = 15
GENOMES = [
           "[[0, [0, None, [], [1]]], [1, [199, [19, 1.0], [0], [2]]], [2, [215, ['USDJPY'], [1], [3]]], [3, [803, [-1, 0.74942847868433404, 10], [2], [4]]], [4, [304, [], [3], [7, 5]]], [5, [1055, [145], [4], [6]]], [6, [710, ['USDJPY', 0, 5], [5], []]], [7, [1042, [191], [4], [8]]], [8, [1022, [158], [7], [9]]], [9, [1068, [56], [8], [10]]], [10, [1056, [155], [9], [11]]], [11, [709, ['USDJPY', 1, 8], [10], []]]]",
           "[[0, [0, None, [], [1]]], [1, [199, [18, 1.0], [0], [2]]], [2, [214, ['USDJPY'], [1], [3]]], [3, [803, [1, 0.55086267865545224, 8], [2], [4]]], [4, [303, [], [3], [9, 5]]], [5, [1080, [147], [4], [6]]], [6, [1070, [167], [5], [7]]], [7, [1061, [91, 0.36022336162540558], [6], [8]]], [8, [709, ['USDJPY', 0, 8], [7], []]], [9, [1056, [135], [4], [10]]], [10, [1074, [35], [9], [11]]], [11, [1068, [152], [10], [12]]], [12, [1068, [49], [11], [13]]], [13, [1042, [186], [12], [14]]], [14, [709, ['USDJPY', 1, 17], [13], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [17, 1.0], [0], [2]]], [2, [215, ['EURUSD'], [1], [3]]], [3, [803, [-1, 0.57011304109642769, 2], [2], [4]]], [4, [302, [], [3], [12, 5]]], [5, [1070, [50], [4], [6]]], [6, [1069, [178, 0.84356592705925626], [5], [7]]], [7, [1056, [15], [6], [8]]], [8, [1074, [194], [7], [9]]], [9, [1047, [82], [8], [10]]], [10, [1072, [171], [9], [11]]], [11, [709, ['EURUSD', 0, 2], [10], []]], [12, [1047, [169], [4], [13]]], [13, [1068, [186], [12], [14]]], [14, [1039, [139], [13], [15]]], [15, [707, ['EURUSD', 1, 3], [14], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [16, 1.0], [0], [2]]], [2, [211, ['EURUSD'], [1], [3]]], [3, [803, [1, 0.73380687605380912, 11], [2], [4]]], [4, [303, [], [3], [11, 5]]], [5, [1066, [123, 2, 90, 6], [4], [6]]], [6, [1042, [37], [5], [7]]], [7, [1038, [166, 112], [6], [8]]], [8, [1038, [146, 98], [7], [9]]], [9, [1068, [64], [8], [10]]], [10, [707, ['EURUSD', 0, 20], [9], []]], [11, [1070, [166], [4], [12]]], [12, [1019, [32], [11], [13]]], [13, [1023, [162], [12], [14]]], [14, [708, ['EURUSD', 0, 6], [13], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [15, 1.0], [0], [2]]], [2, [215, ['EURUSD'], [1], [3]]], [3, [803, [-1, 0.34948286831516051, 2], [2], [4]]], [4, [303, [], [3], [8, 5]]], [5, [1023, [15], [4], [6]]], [6, [1038, [46, 42], [5], [7]]], [7, [707, ['EURUSD', 0, 0], [6], []]], [8, [1069, [180, 0.30565610126345905], [4], [9]]], [9, [1021, [117], [8], [10]]], [10, [1027, [5, 200], [9], [11]]], [11, [1056, [65], [10], [12]]], [12, [707, ['EURUSD', 1, 8], [11], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [14, 1.0], [0], [2]]], [2, [214, ['GBPUSD'], [1], [3]]], [3, [803, [1, 0.3762256420782587, 3], [2], [4]]], [4, [302, [], [3], [11, 5]]], [5, [1014, [176], [4], [8, 6]]], [6, [1047, [150], [5], [7]]], [7, [710, ['GBPUSD', 1, 5], [6], []]], [8, [1038, [186, 41], [5], [9]]], [9, [1005, [5, 111, 0], [8], [10]]], [10, [708, ['GBPUSD', 1, 20], [9], []]], [11, [1027, [0, 87], [4], [12]]], [12, [1039, [171], [11], [13]]], [13, [1027, [2, 58], [12], [14]]], [14, [1017, [163], [13], [15]]], [15, [1068, [128], [14], [16]]], [16, [707, ['GBPUSD', 0, 20], [15], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [13, 1.0], [0], [2]]], [2, [214, ['USDCAD'], [1], [3]]], [3, [803, [1, 0.73005811973283918, 16], [2], [4]]], [4, [303, [], [3], [11, 5]]], [5, [1012, [148, 1.5057062725556392, 1.6882574495562639, 6], [4], [6]]], [6, [1053, [3], [5], [7]]], [7, [1027, [6, 156], [6], [8]]], [8, [1074, [100], [7], [9]]], [9, [1070, [40], [8], [10]]], [10, [708, ['USDCAD', 1, 1], [9], []]], [11, [1012, [137, 0.38568677866487833, 0.15145297907654012, 0], [4], [12]]], [12, [1057, [72], [11], [13]]], [13, [1057, [187], [12], [14]]], [14, [1057, [187], [13], [15]]], [15, [1054, [35], [14], [16]]], [16, [1042, [115], [15], [17]]], [17, [1057, [136], [16], [18]]], [18, [708, ['USDCAD', 0, 18], [17], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [12, 1.0], [0], [2]]], [2, [211, ['AUDUSD'], [1], [3]]], [3, [803, [1, 0.68132525587222881, 4], [2], [4]]], [4, [304, [], [3], [13, 5]]], [5, [1005, [53, 107, 3], [4], [6]]], [6, [1038, [4, 187], [5], [7]]], [7, [1066, [190, 71, 58, 4], [6], [8]]], [8, [1038, [5, 165], [7], [9]]], [9, [1005, [47, 84, 0], [8], [10]]], [10, [1056, [152], [9], [11]]], [11, [1012, [39, 1.0205417456310959, 2.6733130151104021, 5], [10], [12]]], [12, [708, ['AUDUSD', 0, 0], [11], []]], [13, [1027, [1, 178], [4], [14]]], [14, [1014, [172], [13], [17, 15]]], [15, [1013, [15, 2.4970784065961471, 2.450755229123843, 2], [14], [16]]], [16, [708, ['AUDUSD', 0, 2], [15], []]], [17, [1072, [45], [14], [18]]], [18, [707, ['AUDUSD', 0, 13], [17], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [11, 1.0], [0], [2]]], [2, [215, ['AUDUSD'], [1], [3]]], [3, [803, [1, 0.86546590410217206, 8], [2], [4]]], [4, [302, [], [3], [15, 5]]], [5, [1061, [147, 1.0716187105767623], [4], [6]]], [6, [1061, [144, 1.0926249011900961], [5], [7]]], [7, [1022, [88], [6], [8]]], [8, [1053, [176], [7], [9]]], [9, [1070, [56], [8], [10]]], [10, [1061, [145, 1.1195733249550184], [9], [11]]], [11, [1022, [86], [10], [12]]], [12, [1053, [175], [11], [13]]], [13, [1070, [56], [12], [14]]], [14, [710, ['AUDUSD', 1, 2], [13], []]], [15, [1022, [87], [4], [16]]], [16, [1053, [180], [15], [17]]], [17, [1070, [57], [16], [18]]], [18, [710, ['AUDUSD', 1, 2], [17], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [10, 1.0], [0], [2]]], [2, [215, ['EURUSD'], [1], [3]]], [3, [803, [1, 0.17330540074466566, 9], [2], [4]]], [4, [302, [], [3], [11, 5]]], [5, [1012, [183, 2.8765939131371101, 0.18238650180922616, 4], [4], [6]]], [6, [1012, [18, 2.3846463025299478, 2.9110733280692398, 0], [5], [7]]], [7, [1039, [37], [6], [8]]], [8, [1053, [196], [7], [9]]], [9, [1012, [104, 0.34423592513138623, 0.67715545218066042, 6], [8], [10]]], [10, [709, ['EURUSD', 0, 11], [9], []]], [11, [1055, [111], [4], [12]]], [12, [1022, [158], [11], [13]]], [13, [1074, [164], [12], [14]]], [14, [1069, [119, 0], [13], [15]]], [15, [709, ['EURUSD', 1, 17], [14], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [9, 1.0], [0], [2]]], [2, [215, ['EURUSD'], [1], [3]]], [3, [803, [1, 0.91458492118663426, 6], [2], [4]]], [4, [302, [], [3], [10, 5]]], [5, [1012, [70, 2.5515563292876386, 0.83385269550904773, 3], [4], [6]]], [6, [1013, [37, 2.1296864449207105, 1.1929708176430822, 0], [5], [7]]], [7, [1012, [143, 1.1505446924548888, 2.7425961937044088, 1], [6], [8]]], [8, [1042, [109], [7], [9]]], [9, [707, ['EURUSD', 1, 11], [8], []]], [10, [1042, [157], [4], [11]]], [11, [1027, [3, 140], [10], [12]]], [12, [1053, [135], [11], [13]]], [13, [709, ['EURUSD', 1, 11], [12], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [8, 1.0], [0], [2]]], [2, [214, ['GBPUSD'], [1], [3]]], [3, [803, [1, 0.15334585341532797, 14], [2], [4]]], [4, [302, [], [3], [12, 5]]], [5, [1038, [100, 111], [4], [6]]], [6, [1042, [94], [5], [7]]], [7, [1072, [111], [6], [8]]], [8, [1039, [57], [7], [9]]], [9, [1042, [21], [8], [10]]], [10, [1080, [173], [9], [11]]], [11, [709, ['GBPUSD', 1, 0], [10], []]], [12, [1027, [6, 130], [4], [13]]], [13, [1014, [121], [12], [16, 14]]], [14, [1054, [97], [13], [15]]], [15, [710, ['GBPUSD', 1, 7], [14], []]], [16, [1057, [59], [13], [17]]], [17, [709, ['GBPUSD', 0, 14], [16], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [7, 1.0], [0], [2]]], [2, [214, ['GBPUSD'], [1], [3]]], [3, [803, [-1, 0.11962982673395059, 6], [2], [4]]], [4, [303, [], [3], [9, 5]]], [5, [1054, [79], [4], [6]]], [6, [1042, [99], [5], [7]]], [7, [1026, [102], [6], [8]]], [8, [707, ['GBPUSD', 0, 15], [7], []]], [9, [1021, [59], [4], [10]]], [10, [1067, [136, 30, 56, 0], [9], [11]]], [11, [1068, [100], [10], [12]]], [12, [710, ['GBPUSD', 1, 10], [11], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [6, 1.0], [0], [2]]], [2, [214, ['GBPUSD'], [1], [3]]], [3, [803, [1, 0.14934926381802363, 1], [2], [4]]], [4, [305, [], [3], [11, 5]]], [5, [1069, [5, 0.67346732464008574], [4], [6]]], [6, [1026, [31], [5], [7]]], [7, [1072, [40], [6], [8]]], [8, [1047, [133], [7], [9]]], [9, [1077, [186, 0.91648227141758709], [8], [10]]], [10, [709, ['GBPUSD', 0, 17], [9], []]], [11, [1070, [199], [4], [12]]], [12, [1012, [127, 0.53528442697366752, 2.6955056121124015, 4], [11], [13]]], [13, [1053, [25], [12], [14]]], [14, [707, ['GBPUSD', 0, 0], [13], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [5, 1.0], [0], [2]]], [2, [214, ['GBPUSD'], [1], [3]]], [3, [803, [1, 0.15469777091459297, 1], [2], [4]]], [4, [305, [], [3], [9, 5]]], [5, [1069, [5, 0.67346732464008574], [4], [6]]], [6, [1026, [31], [5], [7]]], [7, [1077, [186, 0.93410842591786802], [6], [8]]], [8, [709, ['GBPUSD', 0, 17], [7], []]], [9, [1070, [199], [4], [10]]], [10, [1012, [128, 0.52120596054246204, 2.6788738314540179, 4], [9], [11]]], [11, [1053, [25], [10], [12]]], [12, [707, ['GBPUSD', 0, 0], [11], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [4, 1.0], [0], [2]]], [2, [214, ['GBPUSD'], [1], [3]]], [3, [803, [1, 0.14934926381802363, 1], [2], [4]]], [4, [305, [], [3], [8, 5]]], [5, [1069, [3, 0.64624321906022775], [4], [6]]], [6, [1061, [92, 1.1284250036872223], [5], [7]]], [7, [708, ['GBPUSD', 1, 7], [6], []]], [8, [1070, [199], [4], [9]]], [9, [1012, [134, 0.62810997678890712, 2.668849267174064, 4], [8], [10]]], [10, [1053, [25], [9], [11]]], [11, [707, ['GBPUSD', 0, 0], [10], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [3, 1.0], [0], [2]]], [2, [215, ['AUDUSD'], [1], [3]]], [3, [803, [1, 0.16248621925794959, 2], [2], [4]]], [4, [305, [], [3], [17, 5]]], [5, [1005, [197, 171, 2], [4], [6]]], [6, [1054, [3], [5], [7]]], [7, [1061, [37, 1.4351057836288486], [6], [8]]], [8, [1072, [183], [7], [9]]], [9, [1053, [53], [8], [10]]], [10, [1021, [37], [9], [11]]], [11, [1017, [178], [10], [12]]], [12, [1042, [69], [11], [13]]], [13, [1021, [31], [12], [14]]], [14, [1017, [184], [13], [15]]], [15, [1013, [156, 2.7487700860838835, 1.1573110430962634, 0], [14], [16]]], [16, [708, ['AUDUSD', 0, 19], [15], []]], [17, [1022, [193], [4], [18]]], [18, [1042, [72], [17], [19]]], [19, [1021, [32], [18], [20]]], [20, [1017, [186], [19], [21]]], [21, [1013, [156, 2.7653850781679186, 1.1823564722322726, 0], [20], [22]]], [22, [708, ['AUDUSD', 0, 19], [21], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [2, 1.0], [0], [2]]], [2, [215, ['AUDUSD'], [1], [3]]], [3, [803, [-1, 0.33230020147371186, 0], [2], [4]]], [4, [305, [], [3], [9, 5]]], [5, [1039, [144], [4], [6]]], [6, [1023, [170], [5], [7]]], [7, [1069, [157, 0.66263588707970145], [6], [8]]], [8, [708, ['AUDUSD', 0, 4], [7], []]], [9, [1012, [143, 2.206619756639379, 2.2460864285854667, 0], [4], [10]]], [10, [1022, [84], [9], [11]]], [11, [1054, [119], [10], [12]]], [12, [1077, [194, 0.27477383451545351], [11], [13]]], [13, [709, ['AUDUSD', 1, 17], [12], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [1, 1.0], [0], [2]]], [2, [215, ['AUDUSD'], [1], [3]]], [3, [803, [1, 0.84549189612060793, 5], [2], [4]]], [4, [302, [], [3], [8, 5]]], [5, [1005, [110, 191, 1], [4], [6]]], [6, [1054, [126], [5], [7]]], [7, [709, ['AUDUSD', 0, 7], [6], []]], [8, [1019, [175], [4], [9]]], [9, [1012, [175, 2.5193669610168108, 2.7531330905231028, 2], [8], [10]]], [10, [1021, [172], [9], [11]]], [11, [710, ['AUDUSD', 0, 11], [10], []]]]",
#           "[[0, [0, None, [], [1]]], [1, [199, [0, 1.0], [0], [2]]], [2, [215, ['EURUSD'], [1], [3]]], [3, [803, [-1, 0.97207104053299853, 3], [2], [4]]], [4, [304, [], [3], [11, 5]]], [5, [1021, [76], [4], [6]]], [6, [1067, [91, 31, 81, 4], [5], [7]]], [7, [1038, [89, 38], [6], [8]]], [8, [1055, [180], [7], [9]]], [9, [1039, [89], [8], [10]]], [10, [710, ['EURUSD', 0, 20], [9], []]], [11, [1068, [88], [4], [12]]], [12, [1022, [138], [11], [13]]], [13, [1069, [154, 0.62243052992952608], [12], [14]]], [14, [707, ['EURUSD', 0, 8], [13], []]]]",
           ]

FROM_DATE = datetime(2005,1,30)
TO_DATE = datetime(2005,2,11)
START_TIME = datetime(2005,2,7,0,0)

EQUITY = 1272
LEVERAGE = 30

MM_SETTINGS = {
               'positionsize' : 1000,
               'positionmode' : 2,
               'riskmanagement' : True,
               'setsl' : 1,
               'maxrisk' : 1,
               'riskreward_rate' : 0.0,
               'maxnumoftrades': 1,
               'margincall_rate' : 50
               }
            

ACCOUNT = {
           'userId' : 1,
           'brokerId' : 1,
           'name' : 'TestAccount',
           'username' : 'DEMO3NxKxd',
           'password' : 'NxKxd',
           'api' : 1,
           'feed_senderCompId' : 'FEED_DEMO3NxKxd_DEMOFIX',
           'trade_senderCompId' : 'DEMO3NxKxd_DEMOFIX',
           'targetCompId' : 'DUKASCOPYFIX',
           'mmpluginId' : 1,
           'mmplugin_parameters' : ''                      
           }


log = logging.getLogger('gdtlive')

def init_logging(level = logging.WARNING):
    log = logging.getLogger('gdtlive.evol.gnodes')
    log.setLevel(logging.DEBUG)
    handler = RotatingFileHandler(config.LOG_PATH + os.sep + 'gdtlive.evol.gnodes.log','w+',20*1024*1024, 10)
    handler.setFormatter(logging.Formatter("%(asctime)s %(message)s"))
    handler.setLevel(logging.DEBUG)        
    log.addHandler(handler)
        
    
    log = logging.getLogger('gdtlive')
    log.setLevel(level)
    console_handler = logging.StreamHandler()    
    console_handler.setFormatter(logging.Formatter(LOG_FORMAT))
    console_handler.setLevel(level)
    log.addHandler(console_handler)



class StubDatafeedServer():
    
    def register_instrument(self, symbol, timeframe):
        pass
    
    def unregister_instrument(self, sybol, timeframe):
        pass
    
    def preload(self, timeframe):
        pass
    



class FIXEngine():
    
    def __init__(self, queue):
        self.msgseqnum = 0
        self.orderid = 1000000
        self.limit_orders = []
        self.queue = queue        
        
    
    def handle_message(self, data):
        self.message = fix.decode(data)        
        if type(self.message) == gdtlive.core.fix.dukascopy.messages.Logon:        
            self.reply_logon(self.message)
        elif type(self.message) == gdtlive.core.fix.dukascopy.messages.AccountInfoRequest:
            self.reply_accountinfo(self.message)
        elif type(self.message) == gdtlive.core.fix.dukascopy.messages.TestRequest:
            self.reply_testrequest(self.message)
        elif type(self.message) == gdtlive.core.fix.dukascopy.messages.Heartbeat:
            self.reply_heartbeat(self.message)
        elif type(self.message) == gdtlive.core.fix.dukascopy.messages.NewOrderSingle:
            self.reply_neworder(self.message)
        elif type(self.message) == gdtlive.core.fix.dukascopy.messages.OrderCancelRequest:
            self.reply_ordercancel(self.message)
        else:
            print 'IMPLEMENT REPLY FOR MESSAGE TYPE', type(self.message)
            return None                
                        
        
    def send(self, reply):        
        reply.MsgSeqNum = self.msgseqnum
        reply.SenderCompID = self.message.TargetCompID
        reply.TargetCompID = self.message.SenderCompID
        reply.SendingTime = datetime.utcnow()
        self.msgseqnum += 1
        self.queue.put(reply.encode())    


    def reply_logon(self, message):
        if message.ResetSeqNumFlag:
            self.msgseqnum = 1
            
        reply = fix.Logon()
        reply.Username = message.Username
        reply.Password = message.Password
        reply.HeartBtInt = message.HeartBtInt
        reply.ResetSeqNumFlag = True
        reply.EncryptMethod = 0
        self.send(reply)
        

    def reply_accountinfo(self, message):
        reply = fix.AccountInfo()
        reply.Equity = EQUITY
        reply.Leverage = LEVERAGE
        reply.UsableMargin = EQUITY
        self.send(reply)
        

    def reply_heartbeat(self, message):
        reply = fix.Heartbeat()
        self.send(reply)

        
    def reply_testrequest(self, message):
        reply = fix.Heartbeat()
        reply.TestReqID = message.TestReqID
        self.send(reply)
        
         
    def reply_neworder(self, message):        
        
        if message.OrdType == fix.OrdType.MARKET:
            s = message.Symbol[:3] + message.Symbol[4:]                        
            if message.Side == fix.Side.BUY:
                price = gdtlive.core.historic.DATAROWS[TIMEFRAME][s][PRICE_ASKCLOSE][-1]
            else:
                price = gdtlive.core.historic.DATAROWS[TIMEFRAME][s][PRICE_BIDCLOSE][-1]
                
            reply = fix.ExecutionReport()    
            reply.AvgPx = price
            reply.ClOrdID = message.ClOrdID
            reply.CumQty = message.OrderQty
            reply.ExecID = self.orderid
            reply.ExecType = fix.ExecType.ORDER_STATUS
            reply.LeavesQty = 0.0
            reply.OrdStatus = fix.OrdStatus.FILLED
            reply.OrdType = message.OrdType
            reply.OrderID = self.orderid
            reply.OrderQty = message.OrderQty
            reply.Side = message.Side
            reply.Symbol = message.Symbol
            reply.TimeInForce = message.TimeInForce
            reply.TransactTime = datetime.utcnow()
            self.orderid += 10
            self.send(reply) 
            return
        
        if message.OrdType == fix.OrdType.STOP or message.OrdType == fix.OrdType.STOP_LIMIT:            
            reply = fix.ExecutionReport()    
            reply.AvgPx = message.Price
            reply.ClOrdID = message.ClOrdID
            reply.CumQty = 0.0
            reply.ExecID = self.orderid
            reply.ExecType = fix.ExecType.ORDER_STATUS
            reply.LeavesQty = message.OrderQty
            reply.OrdStatus = fix.OrdStatus.PENDING_NEW
            reply.OrdType = message.OrdType
            reply.OrderID = self.orderid
            reply.OrderQty = message.OrderQty
            reply.Side = message.Side
            reply.Symbol = message.Symbol            
            reply.TransactTime = datetime.utcnow()
            self.orderid += 10
            self.limit_orders.append(message)
            self.send(reply)                        
            return
            
    
    
    def reply_ordercancel(self, message):        
        for order in self.limit_orders[:]:
            if order.ClOrdID == message.OrigClOrdID:
                self.limit_orders.remove(order) 
                        
        reply = fix.Notification()
        reply.AccountName = message.SenderCompID 
        reply.NotifPriority = fix.NotifPriority.INFO
        reply.Text = "ORDER CANCELED: #%s ENTRY %s %s" % (message.OrderID, message.Symbol, message.ClOrdID )        
        self.send(reply)
        
        reply = fix.ExecutionReport()
        reply.AvgPx = 0.0
        reply.ClOrdID = message.ClOrdID
        reply.CumQty = 0.0
        reply.ExecID = self.orderid
        reply.ExecType = fix.ExecType.ORDER_STATUS
        reply.LeavesQty = 0.0
        reply.OrdStatus = fix.OrdStatus.CANCELED        
        reply.OrderID = self.orderid
        reply.OrderQty = 0.0
        reply.Side = fix.Side.UNDISCLOSED
        reply.Symbol = message.Symbol
        reply.TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL
        reply.TransactTime = datetime.utcnow()
        self.send(reply)         
        
    
    def check_sltp(self, timeframe, symbol):        
        for order in self.limit_orders[:]:
            if order.Symbol != symbol[:3] + '/' + symbol[3:]:
                continue
            if order.OrdType == fix.OrdType.STOP_LIMIT:
                if order.Side == fix.Side.BUY:
                    if gdtlive.core.historic.DATAROWS[TIMEFRAME][symbol][PRICE_ASKLOW][-1] <= order.Price:                            
                        self.condorder_hit(order)
                else:
                    if gdtlive.core.historic.DATAROWS[TIMEFRAME][symbol][PRICE_BIDHIGH][-1] >= order.Price:                            
                        self.condorder_hit(order)
            else:
                if order.Side == fix.Side.BUY:
                    if gdtlive.core.historic.DATAROWS[TIMEFRAME][symbol][PRICE_ASKHIGH][-1] >= order.Price:                            
                        self.condorder_hit(order)                            
                else:
                    if gdtlive.core.historic.DATAROWS[TIMEFRAME][symbol][PRICE_BIDLOW][-1] <= order.Price:                            
                        self.condorder_hit(order)
                            
        
        
        
    def condorder_hit(self, order):
        self.limit_orders.remove(order)
        reply = fix.ExecutionReport()
        reply.ClOrdID = order.ClOrdID
        reply.OrdStatus = fix.OrdStatus.FILLED
        reply.CumQty = order.OrderQty
        reply.AvgPx = order.Price
        reply.TransactTime = datetime.utcnow()
        self.send(reply)
        
        
                         
                

class SSLSocket():
    def __init__(self, engine, queue):
        self.engine = engine
        self.queue = queue
        self._timeout = 60
        self.output_buffer = ''
     
        
    def settimeout(self, sec):
        self._timeout = sec
        
        
    def connect(self, address):        
        #raise  # if simulating connection faliure 
        return 
    
        
    def write(self, data):        
        self.engine.handle_message(data)            
                
            
    def read(self, max_size):        
        #data = self.queue.get()        
        try:
            data = self.queue.get(True, self._timeout)
            self.queue.task_done()
        except:
            raise socket.error('timed out')
        
        return data  
        
    
    
    def shutdown(self, mode):
        pass
    
    def close(self):
        pass
    
    

    
    
class LiveEngineTester():
    
    def __init__(self, timeframe, genomes, from_date, to_date, start_time, equity, leverage, mmparams):            
        self.mm_params = mmparams
        self.from_date = from_date 
        self.to_date = to_date
        print start_time
        self.start_time = start_time
        self.genomes = genomes
        self.timeframe = timeframe
        self.tradeengine = TradeEngine()
        self.liverunconfigs_ids = []
        self.strategy_ids = []
        self.symbols = set()
        self.DATAROW = {}
        self.datarow_builder = DatarowBuilder()
        
        self.fix_queue = Queue()
        self.fix_engine = FIXEngine(self.fix_queue)        
        self.ssl_socket = SSLSocket(self.fix_engine, self.fix_queue)
        
        gdtlive.core.tradeengine.feedserver = StubDatafeedServer()        
        ssl.wrap_socket = self.wrap_socket
                
        db.SQL_URL = 'localhost/live'
        db.init()
                     
        gdtlive.historic.plugin.dukascopy_fs.initialize_datarow_descriptors()

    def wrap_socket(self, socket):        
        return self.ssl_socket


    def prepare(self):                        
        session = db.Session()
        try:
            log.info('creating AccountData')
            ACCOUNT['mmplugin_parameters'] = str(self.mm_params)
            self.account_data = db.AccountData(**ACCOUNT)
            session.add(self.account_data)                        
            session.commit()    
            for dns in self.genomes:
                genome = GTree.decode(dns)
                self.symbols |= genome.getSymbols()
                
                strategy = db.Strategy(0,0,0,0,0, dns)
                session.add(strategy)
                session.commit()
                log.info('strategy %d created' % strategy.id)
                self.strategy_ids.append(strategy.id)
                lrconfig = db.LiveRunConfig(ACCOUNT['userId'], ACCOUNT['mmpluginId'], '', self.account_data.id, strategy.id, 0, self.timeframe)
                session.add(lrconfig)
                session.commit()                
                log.info('liverunconfig %d created' % lrconfig.id)
                self.liverunconfigs_ids.append(lrconfig.id)                        
                
        except:
            log.error(traceback.format_exc())
        finally:
            session.close()
    
    def prepare_datarows(self):        
        for symbol in list(self.symbols):
            self.datarow_builder.register(symbol, self.timeframe)
            self.datarow_builder.preload(symbol, self.timeframe)
                        
        self.datarow_builder.calc_pipvalue(self.timeframe)
        
        for price in gdtlive.core.datafeed.DATAROW[self.timeframe][symbol]:                                                                    
            # numpy conversion
            #print len(gdtlive.core.datafeed.DATAROW[self.timeframe][symbol][price])
            if price == PRICE_TIME:                                                                
                gdtlive.core.historic.DATAROWS[self.timeframe][symbol][price] = gdtlive.core.datafeed.DATAROW[self.timeframe][symbol][price]
            else:
                gdtlive.core.historic.DATAROWS[self.timeframe][symbol][price] = np.array(gdtlive.core.datafeed.DATAROW[self.timeframe][symbol][price], dtype=np.double)
            
            print 
    
        
    def update_datarow(self, index, symbol):                                 
        gdtlive.core.historic.CURRENT_TIME = gdtlive.core.datafeed.DATAROW[self.timeframe][symbol][PRICE_TIME][index] # + timedelta(minutes = self.timeframe)        
                
        gdtlive.core.historic.CURRENT_PRICE[symbol]['ASK'] = gdtlive.core.datafeed.DATAROW[self.timeframe][symbol][PRICE_ASKCLOSE][index]
        gdtlive.core.historic.CURRENT_PRICE[symbol]['BID'] = gdtlive.core.datafeed.DATAROW[self.timeframe][symbol][PRICE_BIDCLOSE][index]
        gdtlive.core.historic.CURRENT_PRICE[symbol]['PIPVALUE'] = gdtlive.core.datafeed.DATAROW[self.timeframe][symbol][PRICE_PIPVALUE][index]
        
        for price in gdtlive.core.datafeed.DATAROW[self.timeframe][symbol]:                                                                    
            # numpy conversion
            if price == PRICE_TIME:                                                                
                gdtlive.core.historic.DATAROWS[self.timeframe][symbol][price] = gdtlive.core.datafeed.DATAROW[self.timeframe][symbol][price][:index+1]
            else:
                gdtlive.core.historic.DATAROWS[self.timeframe][symbol][price] = np.array(gdtlive.core.datafeed.DATAROW[self.timeframe][symbol][price][:index+1], dtype=np.double)
        
        
#        print index, gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_TIME][-1],
#        print gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_ASKOPEN][-1],
#        print gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_ASKHIGH][-1],
#        print gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_ASKLOW][-1],
#        print gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_ASKCLOSE][-1],
#        
#        print '\t', gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_BIDOPEN][-1],
#        print gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_BIDHIGH][-1],
#        print gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_BIDLOW][-1],
#        print gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_BIDCLOSE][-1]
        
#        print gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_TIME][-10:]
#        print 'ASK O', gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_ASKOPEN][-10:]
#        print 'ASK H', gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_ASKHIGH][-10:]
#        print 'ASK L', gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_ASKLOW][-10:]
#        print 'ASK C', gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_ASKCLOSE][-10:]
#        
#        print 'BID O', gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_BIDOPEN][-10:]
#        print 'BID H', gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_BIDHIGH][-10:]
#        print 'BID L', gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_BIDLOW][-10:]
#        print 'BID C', gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_BIDCLOSE][-10:]
        
        #print 'BID', gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_BIDHIGH][-10:]
        
        #print len(gdtlive.core.historic.DATAROWS[TIMEFRAME][symbol][PRICE_TIME]),
        log.info('--------------------------')
        log.warning('----- %s : %s -----' % (symbol, gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_TIME][-1] + timedelta(minutes = self.timeframe)))
        log.warning('----- %f : %f -----' % (gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_ASKCLOSE][-1], gdtlive.core.historic.DATAROWS[self.timeframe][symbol][PRICE_BIDCLOSE][-1]))
        log.info('--------------------------')
        
        
    def check_starttime(self):
        result = True
        for symbol in self.symbols:
            if self.start_time not in gdtlive.core.datafeed.DATAROW[self.timeframe][symbol][PRICE_TIME]:
                result = False
        return result
    
    
    def run(self):
        self.cleanup()
        self.prepare()
        self.prepare_datarows()
        
        while not self.check_starttime():
            self.start_time += timedelta(minutes = 15)
            print self.start_time
        print 'B', self.start_time
        try:
            for liverunconfig_id in self.liverunconfigs_ids:
                self.tradeengine.start_strategy(liverunconfig_id)
                self.tradeengine.strategy_runners[0].account.engine.heartbeat_int = 0   
                
            
            self.current_time = self.start_time
            
            while self.current_time.date() <= self.to_date.date():                
                for symbol in self.symbols:
                    try:
                        index = gdtlive.core.datafeed.DATAROW[self.timeframe][symbol][PRICE_TIME].index(self.current_time)
                        self.update_datarow(index, symbol)                
                        self.fix_engine.check_sltp(self.timeframe, symbol)                                            
                    except ValueError:
                        pass
                    except:
                        log.error(traceback.format_exc())
                
                self.tradeengine.evaluate(self.timeframe)
                
                self.current_time += timedelta(minutes = self.timeframe)                                                                    
            
            self.generate_tradelog()
            
            self.tradeengine.shutdown()
        except:
            log.error(traceback.format_exc())
            
            
    def generate_tradelog(self):
        session = db.Session()  
        tradelog = []      
        trade_id = 0
        try:
            qry = session.query(db.Trade, db.MarketOrder, db.Stoploss, db.Takeprofit).filter(db.MarketOrder.id==db.Trade.marketorder_id).filter(db.Stoploss.id==db.Trade.stoploss_id).filter(db.Takeprofit.id==db.Trade.takeprofit_id).order_by(db.Trade.id)
            for trade, mo, sl, tp in qry.all():
                
                trade_open = (trade_id, str(trade.symbol), trade.direction, calendar.timegm(trade.open_time.timetuple()), trade.price, trade.amount, trade.sl_price, trade.tp_price, 0)                
                
                if sl.fill_price:
                    close_type = ORDERTYPE_CLOSE_BY_SL
                    close_time = calendar.timegm(sl.fill_time.timetuple())
                    close_price = sl.fill_price
                elif tp.fill_price:
                    close_type = ORDERTYPE_CLOSE_BY_TP
                    close_time = calendar.timegm(tp.fill_time.timetuple())
                    close_price = tp.fill_price
                elif mo.close_price:
                    close_type = ORDERTYPE_CLOSE
                    close_time = calendar.timegm(mo.close_fill_time.timetuple())
                    close_price = mo.close_price
                else:
                    close_type = 0
                    close_time = 0
                    close_price = 0
                    
                
                trade_close = (close_type, close_time, close_price, 0, trade.profit ,0 ,0, trade.group_id)
                
                tradelog.append((trade_open, trade_close))
                
                trade_id += 1
                
            
        except:
            log.error(traceback.format_exc())
        finally:
            session.close()
        self.tradelog = tradelog
                

    def cleanup(self):
        session = db.Session()
        try:
            log.info('clearing database')
            session.query(db.AccountData).delete()            
            session.query(db.Strategy).delete()                    
            session.query(db.LiveRunConfig).delete()            
            session.query(db.AccountPerformance).delete()
            session.query(db.Trade).delete()
            session.query(db.Takeprofit).delete()
            session.query(db.Stoploss).delete()
            session.query(db.MarketOrder).delete()
            session.query(db.OrderHistory).delete()
            session.query(db.Notification).delete()
            session.query(db.InstrumentInfo).delete()
            session.commit()            
        except:
            log.error(traceback.format_exc())
        finally:
            session.close()
        


if __name__ == '__main__':
    init_logging()            
    tester = LiveEngineTester(TIMEFRAME, GENOMES, FROM_DATE, TO_DATE, START_TIME, EQUITY, LEVERAGE, MM_SETTINGS)    
    tester.run()
    
    





