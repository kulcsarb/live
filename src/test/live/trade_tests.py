'''
Created on 2012.10.02.

@author: kulcsarb
'''
import gdtlive.store.db as db
import socket
import ssl
import gdtlive.core.datafeed.historic as historic
import gdtlive.core.fix.dukascopy.messages
import gdtlive.core.fix.dukascopy.fix as fix
from gdtlive.core.accounting.accounts import LiveAccount
from gdtlive.core.accounting.trade import Trade
from gdtlive.core.constants import LOG_FORMAT
from datetime import datetime
from gdtlive.constants import *
from Queue import Queue
import logging

db.init()


log = logging.getLogger('gdtlive')
log.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler()    
console_handler.setFormatter(logging.Formatter(LOG_FORMAT))
log.addHandler(console_handler)

EQUITY = 10000
LEVERAGE = 30


class FIXEngine():
    
    def __init__(self, queue):
        self.msgseqnum = 0
        self.orderid = 1000000
        self.limit_orders = []
        self.queue = queue        
        
    
    def handle_message(self, data):        
        self.message = fix.decode(data)        
        if type(self.message) == gdtlive.core.fix.dukascopy.messages.Logon:        
            self.reply_logon(self.message)
        elif type(self.message) == gdtlive.core.fix.dukascopy.messages.AccountInfoRequest:
            self.reply_accountinfo(self.message)
        elif type(self.message) == gdtlive.core.fix.dukascopy.messages.TestRequest:
            self.reply_testrequest(self.message)
        elif type(self.message) == gdtlive.core.fix.dukascopy.messages.Heartbeat:
            self.reply_heartbeat(self.message)
        elif type(self.message) == gdtlive.core.fix.dukascopy.messages.NewOrderSingle:
            self.reply_neworder(self.message)
        elif type(self.message) == gdtlive.core.fix.dukascopy.messages.OrderCancelRequest:
            self.reply_ordercancel(self.message)
        else:
            print 'IMPLEMENT REPLY FOR MESSAGE TYPE', type(self.message)
            return None                
                        
        
    def send(self, reply):
        reply.MsgSeqNum = self.msgseqnum
        reply.SenderCompID = self.message.TargetCompID
        reply.TargetCompID = self.message.SenderCompID
        reply.SendingTime = datetime.utcnow()
        self.msgseqnum += 1
        self.queue.put(reply.encode())    


    def reply_logon(self, message):
        if message.ResetSeqNumFlag:
            self.msgseqnum = 1
            
        reply = fix.Logon()
        reply.Username = message.Username
        reply.Password = message.Password
        reply.HeartBtInt = message.HeartBtInt
        reply.ResetSeqNumFlag = True
        reply.EncryptMethod = 0
        self.send(reply)        
        

    def reply_accountinfo(self, message):
        reply = fix.AccountInfo()
        reply.Equity = EQUITY
        reply.Leverage = LEVERAGE
        reply.UsableMargin = EQUITY
        self.send(reply)
        

    def reply_heartbeat(self, message):
        reply = fix.Heartbeat()
        self.send(reply)

        
    def reply_testrequest(self, message):
        reply = fix.Heartbeat()
        reply.TestReqID = message.TestReqID
        self.send(reply)
        
         
    def reply_neworder(self, message):        
        
        if message.OrdType == fix.OrdType.MARKET:
            s = message.Symbol[:3] + message.Symbol[4:]                        
            if message.Side == fix.Side.BUY:
                price = historic.CURRENT_PRICE[s]['ASK']
            else:
                price = historic.CURRENT_PRICE[s]['BID']                
                
            reply = fix.ExecutionReport()    
            reply.AvgPx = price
            reply.ClOrdID = message.ClOrdID
            reply.CumQty = message.OrderQty
            reply.ExecID = self.orderid
            reply.ExecType = fix.ExecType.ORDER_STATUS
            reply.LeavesQty = 0.0
            reply.OrdStatus = fix.OrdStatus.FILLED
            reply.OrdType = message.OrdType
            reply.OrderID = self.orderid
            reply.OrderQty = message.OrderQty
            reply.Side = message.Side
            reply.Symbol = message.Symbol
            reply.TimeInForce = message.TimeInForce
            reply.TransactTime = datetime.utcnow()
            self.orderid += 10
            self.send(reply) 
            return
        
        if message.OrdType == fix.OrdType.STOP or message.OrdType == fix.OrdType.STOP_LIMIT:            
            reply = fix.ExecutionReport()    
            reply.AvgPx = message.Price
            reply.ClOrdID = message.ClOrdID
            reply.CumQty = 0.0
            reply.ExecID = self.orderid
            reply.ExecType = fix.ExecType.ORDER_STATUS
            reply.LeavesQty = message.OrderQty
            reply.OrdStatus = fix.OrdStatus.PENDING_NEW
            reply.OrdType = message.OrdType
            reply.OrderID = self.orderid
            reply.OrderQty = message.OrderQty
            reply.Side = message.Side
            reply.Symbol = message.Symbol            
            reply.TransactTime = datetime.utcnow()
            self.orderid += 10
            self.limit_orders.append(message)
            self.send(reply)                        
            return
            
    
    
    def reply_ordercancel(self, message):
        reply = fix.ExecutionReport()
        reply.AvgPx = 0.0
        reply.ClOrdID = message.ClOrdID
        reply.CumQty = 0.0
        reply.ExecID = self.orderid
        reply.ExecType = fix.ExecType.ORDER_STATUS
        reply.LeavesQty = 0.0
        reply.OrdStatus = fix.OrdStatus.CANCELED        
        reply.OrderID = self.orderid
        reply.OrderQty = 0.0
        reply.Side = fix.Side.UNDISCLOSED
        reply.Symbol = message.Symbol
        reply.TimeInForce = message.TimeInForce
        reply.TransactTime = datetime.utcnow()
        self.send(reply)         
        
        
QUEUE = Queue()
FIX_ENGINE = FIXEngine(QUEUE)                 

                

class SSLSocket():
    def __init__(self):
        self._timeout = 60
        self.output_buffer = ''
     
        
    def settimeout(self, sec):
        self._timeout = sec
        
        
    def connect(self, address):
        #raise  # if simulating connection faliure 
        return 
    
        
    def write(self, data):
        global FIX_ENGINE
        FIX_ENGINE.handle_message(data)            
                
            
    def read(self, max_size):
        global QUEUE            
        try:
            data = QUEUE.get(True, self._timeout)            
            QUEUE.task_done()
        except:
            raise socket.error('timed out')
        
        return data  
        
        
    def shutdown(self, mode):
        pass
    
    def close(self):
        pass
    
    
    

def wrap_socket(socked):
    return SSLSocket()


ssl.wrap_socket = wrap_socket

MM_SETTINGS = {
               'positionsize' : 20000,
               'positionmode' : 2,
               'riskmanagement' : True,
               'setsl' : 1,
               'maxrisk' : 2,
               'riskreward_rate' : 0.0,
               'maxnumoftrades': 1,
               'margincall_rate' : 50
               }


class Strategy:
    def __init__(self, id, symbol):
        self.strategy_id = id
        self.symbols = set([symbol])
        self.timeframe = 15
        self.floating_profit = 0
        self.open_trades_num = 0
        
    def update_performance(self):
        pass

    def on_trade_open(self, trade):
        pass
    
    

def cleanup():
    session = db.Session()
    session.query(db.Trade).delete()
    session.query(db.MarketOrder).delete()
    session.query(db.Stoploss).delete()
    session.query(db.Takeprofit).delete()
    session.commit()    
    session.close()


cleanup()

account = LiveAccount(1, 'username', 'password', 'SENDERID', 'TARGETID')
account.config_mm(1, MM_SETTINGS)
account.add_strategy(Strategy(1, 'EURUSD'))
account.connect()

historic.CURRENT_PRICE = {'EURUSD' : {'ASK': 1.3500, 'BID': 1.3498}}
historic.CURRENT_TIME = datetime(2009,1,1,12,0)

trade = Trade('EURUSD', DIR_BUY, datetime.utcnow(), 1.3500, 20000, 1.3450, 0, 1, 1, 1, account)
trade.send_open()
print '%r' % trade
account.shutdown()


