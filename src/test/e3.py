'''
Created on Mar 28, 2013

@author: gdt
'''
from gdt.evol.gnodes.GenomeBase import GTree
from gdt.admin.system.log import initialize_loggers
from gdt.evol.plugin import individual_list
from gdtlive.admin.system.log import initialize_loggers as initialize_live_loggers
import gdt.store.db as db
import gdtlive.store.db as livedb 
from datetime import datetime


initialize_loggers()
initialize_live_loggers()

SQL_PASS = 'gdt'
SQL_USER = 'gdt'
SQL_URL = 'loft1/live_november2'

livedb.SQL_USER = 'gdt'
livedb.SQL_PASS = 'gdt'
livedb.SQL_URL = 'main/live'

db.init(False, SQL_USER, SQL_PASS, SQL_URL)
session = db.Session()

livedb.init()
livesession = livedb.Session()

TO = datetime(2012,11,1)
FROM = datetime(2009,1,1)
MM = '{"positionsize":100,"positionmode":2,"sltype":3,"riskmanagement":1,"modifysl":0,"maxrisk":100,"setsl":1,"maxnumoftrades":1000,"margincall_rate":50,"base_equity":10000}'

portfolio = None
DNS = session.query(db.Strategy.dns).all()
i = 0
for dns, in DNS:
    i += 1
    genome = GTree.decode(dns)
    if not portfolio:
        portfolio = GTree.decode(dns)
    else:
        portfolio.mergeWith(GTree.decode(dns))        
    symbol = list(genome.getSymbols())[0]
    strategy = livedb.Strategy(dns.encode(), 15, symbol, 1, MM)
    strategy.evolved_from = FROM
    strategy.evolved_to = TO
    strategy.individual = "Individual" #individual_list[eval(config.evolplugin_parameters)['individual_type']].__name__
    livesession.add(strategy)
    livesession.commit()
    name = db.EntityName(5, strategy.id ,'2012 indi %d' % i)
    livesession.add(name)
    livesession.commit()    
    print i


strategy = livedb.Strategy(portfolio.encode(), 15, ','.join(list(portfolio.getSymbols())), 1, MM)
strategy.evolved_from = FROM
strategy.evolved_to = TO
strategy.individual = "Individual" #individual_list[eval(config.evolplugin_parameters)['individual_type']].__name__
livesession.add(strategy)
livesession.commit()
name = db.EntityName(5, strategy.id ,'2012 Portfolio %d' % i)
livesession.add(name)
livesession.commit()

livesession.close()
session.close()