'''
Created on Sep 2, 2013

@author: gdt
'''
from datetime import datetime, timedelta

class C():
    def __init__(self):
        self.weekend_start = 16
        self.weekend = False
        self.strategy_id = 1
        self.timeframe = 15
        
    def _check_weekend(self, candle_time):        
        if self.weekend_start:
            if self.timeframe == 1440:
                if candle_time.isoweekday() in [5,6]:                    
                    self.weekend = True                
                else:
                    self.weekend = False
            else:                                     
                if candle_time.isoweekday() == 5 and candle_time.hour >= self.weekend_start:
                    if not self.weekend:
                        print 'strategy %d - weekend starts, closing all open trades' % self.strategy_id                        
                    self.weekend = True
                elif candle_time.isoweekday() > 5:
                    self.weekend = True
                else:
                    if self.weekend:
                        print 'strategy %d - weekend ended.' % self.strategy_id                    
                    self.weekend = False
        
        #print 'strategy %d - Weekend: %s' % (self.strategy_id, self.weekend)
        
        
candle = datetime(2013,8,27,0,0)
c = C()
while candle < datetime(2013,9,4, 0):
    print candle, 
    c._check_weekend(candle)
    print c.weekend
    candle += timedelta(hours=24)
    
    
    
    

