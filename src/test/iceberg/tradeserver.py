# -*- encoding: utf-8 -*- 
'''
Created on Jun 14, 2012

@author: gdtlive
'''
from test.iceberg import *
import gdtlive.core.fix.dukascopy.fix as fix
from nose.plugins.attrib import attr
from gdtlive.constants import DIR_SELL, DIR_BUY

server = None
connection_closed = False
HEARTBTINT = 0
CURRENT_PRICE = 1.2900



def setup():
    global server, connection_closed, CURRENT_PRICE
    server = ServerTester(TRADE_URL, TRADE_PORT, TRADE_SENDERCOMPID, TRADE_TARGETCOMPID)
    server.connect()   
    connection_closed = False
    
    server.send(fix.Logon(Username=USERNAME, Password=PASSWORD, HeartBtInt=HEARTBTINT, ResetSeqNumFlag=True, EncryptMethod=0))    
    server.recv()            
    server.next()    
    if not CURRENT_PRICE:        
        CURRENT_PRICE = get_current_price()        
        #print CURRENT_PRICE           
    
        
def teardown():
    global server, connection_closed
    if not connection_closed:
        server.send(fix.Logout())
        server.recv()
        server.next()                    
        server.disconnect()
        connection_closed = True



def market_order(symbol, direction, amount, client_id):
    return fix.NewOrderSingle(ClOrdID=client_id, 
                                        Currency=symbol[:3],                                        
                                        Symbol=symbol, 
                                        Side={DIR_BUY: fix.Side.BUY, DIR_SELL: fix.Side.SELL}[direction], 
                                        OrderQty=amount,
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.MARKET,
                                        TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL
                                        )


def stop_order(symbol, direction, amount, price, client_id):
    return fix.NewOrderSingle(ClOrdID = client_id,
                                        Currency=symbol[:3],
                                        Symbol = symbol, 
                                        Side = {DIR_BUY: fix.Side.BUY, DIR_SELL: fix.Side.SELL}[direction],
                                        OrderQty = amount,
                                        Price = price,
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.STOP,
                                        TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL,
                                        DiscretionOffsetValue = 5
                                        )
    

def limit_order(symbol, direction, amount, price, client_id):
    return fix.NewOrderSingle(ClOrdID = client_id,
                                        Currency=symbol[:3],
                                        Symbol = symbol, 
                                        Side = {DIR_BUY: fix.Side.BUY, DIR_SELL: fix.Side.SELL}[direction],
                                        OrderQty = amount,
                                        Price = price,
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.LIMIT,
                                        TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL,
                                        DiscretionOffsetValue = 5
                                        )
    


def get_current_price(self):
    global server    
    server.send(market_order('EUR/USD', DIR_SELL, self.amount, "2"))
    reply = None
    for i in xrange(5):
        server.recv()
        reply = server.next()    
    server.send(market_order('EUR/USD', DIR_BUY, self.amount, "2"))    
    for i in xrange(5):
        server.recv()
        server.next()             
    return reply.Price


class TestOtherMessages(TestBase):
    
    def setup(self):
        global server
        self.server = server
        self.HeartBeatInt = HEARTBTINT
    
    def teardown(self):
        pass
                    
    #@attr('off')
#    def testAccountInfo(self):
#        self.send(fix.AccountInfoRequest())
#        self.expect(fix.AccountInfo, (
#                         'Currency == "USD"',
#                         'AccountName == SENDERCOMPID',
#                         'TrdType == %d' % fix.TrdType.REGULAR_TRADE, 
#                         'Leverage > 0',
#                         'Equity > 0',
#                         'UsableMargin > 0'
#                    ))
#                
#                
                
    @attr('off')
    def testOrderMassStatusRequest(self):
        '''
        OrderMassStatusRequest
        
        előfeltétel:
             ha nincs nyitott pozició
        teszteset:
            OrderMassStatusRequest üzenet küldése adott azonosítóval
        elvárt viselkedés:
            A szerver TradingSessionStatus üzenetet ad válaszként    
                        
        '''
        
        self.send(fix.OrderMassStatusRequest('REQ'))
        self.expect(fix.TradingSessionStatus)
        


class TestMarkerOrderReject(TestBase):

    def __init__(self):
        TestBase.__init__(self)
        self.amount = 1000
        self.symbol = 'EUR/USD'
        
    def setup(self):
        global server
        self.server = server
        self.HeartBeatInt = HEARTBTINT
    
    
    def teardown(self):
        pass
    
    
    @attr('off')
    def testAmountTooSmall(self):
        """        
        
        """
        amount = 1
        clordid = "testAmountTooSmall" + str(datetime.utcnow())
        self.send(market_order(self.symbol, DIR_BUY, amount, clordid))                
        self.out = None
        
        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID == "0"',
                        'ExecType == "%s"' % fix.ExecType.PENDING_NEW,
                        'LeavesQty == %f' % float(amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW,
                        'OrderID == "0"',
                        'OrderQty == %f' % float(amount),
                        'Side == "%s"' % fix.Side.BUY,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))

        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.NEW,
                        'LeavesQty == %f' % float(amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.NEW,
                        'OrderID is not None',
                        'OrderQty == %f' % float(amount),
                        'Price == 0.0',
                        'Side == "%s"' % fix.Side.BUY,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))

        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx > 0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == %f' % float(amount),
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.TRADE,
                        'LastPx > 0',
                        'LastQty == %f' % float(amount),
                        'LeavesQty == 0.0',
                        'OrdStatus == "%s"' % fix.OrdStatus.FILLED,
                        'OrderID is not None',
                        'OrderQty == %f' % float(amount),
                        'Price > 0',
                        'Side == "%s"' % fix.Side.BUY,
                        'Symbol == "%s"' % self.symbol,
                        'Text == "Trade Done"'                                                                                   
                    ))
        
       
    
    @attr('off')
    def testAmountTooBig(self):
        amount = 1000000
        self.send(market_order(self.symbol, DIR_BUY, amount, "1"))                
        self.out = None        
        
        self.expect(fix.Notification, (
                    '"REJECTED: no margin available" in Text',
                    'NotifPriority == 1',
                    'AccountName == SENDERCOMPID'
                    ))
        
        
        self.expect(fix.ExecutionReport, (
                     'ClOrdID == "1"',
                     'OrderQty == 0.0',
                     'CumQty == 0.0',
                     'OrdType == "%s"' % fix.OrdType.MARKET,
                     'TimeInForce == "%s"' % fix.TimeInForce.FILL_OR_KILL,
                     'OrdStatus == "%s"' % fix.OrdStatus.REJECTED,                           
                     'Symbol == symbol',
                     'LeavesQty == 0.0',
                     'OrderID is not None',
                     'AvgPx == 0.0',
                     'ExecID is not None',
                     'ExpireTime is None',
                     'OrdRejReason == %d' % fix.OrdRejReason.ORDER_EXCEEDS_LIMIT,
                     'Side == "%s"' % fix.Side.UNDISCLOSED,
                     'CashMargin is None',
                     'Slippage is None',
                     'Account is None'
                     ))        
        
        self.expect(fix.InstrumentPositionInfo, (
                     'Symbol == symbol',
                     'Amount == 0.0',
                     'Price == 0.0',
                     'AccountName == SENDERCOMPID'
                     ))                                

    

class TestMarketOrderOK(TestBase):    
    
    def __init__(self):
        TestBase.__init__(self)
        self.amount = 1000
        self.symbol = 'EUR/USD'
        
    def setup(self):
        global server
        self.server = server
        self.HeartBeatInt = HEARTBTINT
    
    
    def teardown(self):
        if type(self.out) == fix.NewOrderSingle:
            dir = DIR_SELL if self.out.Side == 'Buy' else DIR_BUY
            close = market_order(self.out.Symbol, dir, self.out.OrderQty, "close")
            self.send(close)
            self.expect(fix.ExecutionReport)
            self.expect(fix.InstrumentPositionInfo)
            self.expect(fix.Notification)
            self.expect(fix.ExecutionReport)
            self.expect(fix.InstrumentPositionInfo)   
        
    
    #@attr('off')
    def testBuySuccessfull(self):
        amount = 1000
                
        clordid = "testBuySuccesfull" + str(datetime.utcnow())
        self.send(market_order(self.symbol, DIR_BUY, amount, clordid))                
        self.out = None
        
        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID == "0"',
                        'ExecType == "%s"' % fix.ExecType.PENDING_NEW,
                        'LeavesQty == %f' % float(amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW,
                        'OrderID == "0"',
                        'OrderQty == %f' % float(amount),
                        'Side == "%s"' % fix.Side.BUY,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))

        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.NEW,
                        'LeavesQty == %f' % float(amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.NEW,
                        'OrderID is not None',
                        'OrderQty == %f' % float(amount),
                        'Price == 0.0',
                        'SettlDate is not None',
                        'Side == "%s"' % fix.Side.BUY,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))

        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx > 0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == %f' % float(amount),
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.TRADE,
                        'LastPx > 0',
                        'LastQty == %f' % float(amount),
                        'LeavesQty == 0.0',
                        'OrdStatus == "%s"' % fix.OrdStatus.FILLED,
                        'OrderID is not None',
                        'OrderQty == %f' % float(amount),
                        'Price > 0',
                        'SettlDate is not None',
                        'Side == "%s"' % fix.Side.BUY,
                        'Symbol == "%s"' % self.symbol,
                        'Text == "Trade Done"'                                                                                   
                    ))

        
    
    @attr('off')
    def testSellSuccessfull(self):
        amount = 1000
                
        clordid = "testSellSuccesfull" + str(datetime.utcnow())
        self.send(market_order(self.symbol, DIR_SELL, amount, clordid))                
        self.out = None
        
        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID == "0"',
                        'ExecType == "%s"' % fix.ExecType.PENDING_NEW,
                        'LeavesQty == %f' % float(amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW,
                        'OrderID == "0"',
                        'OrderQty == %f' % float(amount),                        
                        'Side == "%s"' % fix.Side.SELL,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))

        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.NEW,
                        'LeavesQty == %f' % float(amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.NEW,
                        'OrderID is not None',
                        'OrderQty == %f' % float(amount),
                        'Price == 0.0',
                        'SettlDate is not None',
                        'Side == "%s"' % fix.Side.SELL,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))

        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx > 0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == %f' % float(amount),
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.TRADE,
                        'LastPx > 0',
                        'LastQty == %f' % float(amount),
                        'LeavesQty == 0.0',
                        'OrdStatus == "%s"' % fix.OrdStatus.FILLED,
                        'OrderID is not None',
                        'OrderQty == %f' % float(amount),
                        'Price > 0',
                        'SettlDate is not None',
                        'Side == "%s"' % fix.Side.SELL,
                        'Symbol == "%s"' % self.symbol,
                        'Text == "Trade Done"'                                                                                   
                    ))

            
        

class TestLimitOrderInstantFill(TestBase):
    def __init__(self):
        TestBase.__init__(self)
        self.amount = 1000
        self.symbol = 'EUR/USD'
        self.close_type = None
                    
    def setup(self):    
        global server
        self.server = server
        self.HeartBeatInt = HEARTBTINT
        
         
    def teardown(self):
        if self.close_type is not None:                    
            close = market_order(self.out.Symbol, self.close_type, self.out.OrderQty, "close" + str(datetime.utcnow()))
            self.close_type = None
            self.send(close)
            self.expect(fix.ExecutionReport)            
            self.expect(fix.ExecutionReport)
            self.expect(fix.ExecutionReport)            
               

    @attr('off')
    def testPlaceBuyAboveMarket(self):
        assert CURRENT_PRICE
        
        clordid = "testPlaceBuyAbove" + str(datetime.utcnow())
        side = fix.Side.BUY
        self.send(limit_order('EUR/USD', DIR_BUY, self.amount, CURRENT_PRICE + 0.1000, clordid))
        self.close_type = DIR_SELL        
        
# ExecutionReport(Account=20151500001,AvgPx=0.0,ClOrdID=GDT_20,CumQty=0.0,Currency=EUR,ExecID=0,ExecType=PENDING_NEW,LeavesQty=1000.0,MsgSeqNum=2,MsgType=EXECUTION_REPORT,OrdStatus=PENDING_NEW,OrderID=0,OrderQty=1000.0,Price=1.4,SenderCompID=ADSSUATTRADE,SendingTime=2012-10-29 14:28:10.686000,Side=BUY,Symbol=EUR/USD,TargetCompID=FixIBdemo)

        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID == "0"',
                        'ExecType == "%s"' % fix.ExecType.PENDING_NEW,
                        'LeavesQty == %f' % float(self.amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW,
                        'OrderID == "0"',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f' % (CURRENT_PRICE + 0.1),
                        'Side == "%s"' % side,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))

# ExecutionReport(Account=20151500001,AvgPx=0.0,ClOrdID=GDT_20,CumQty=0.0,Currency=EUR,ExecID=4048396907268562,ExecType=NEW,LeavesQty=1000.0,MsgSeqNum=3,MsgType=EXECUTION_REPORT,OrdStatus=NEW,OrderID=4048396907268562,OrderQty=1000.0,Price=1.4,SenderCompID=ADSSUATTRADE,SendingTime=2012-10-29 14:28:10.795000,SettlDate=2012-10-31,Side=BUY,Symbol=EUR/USD,TargetCompID=FixIBdemo)
        
        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.NEW,
                        'LeavesQty == %f' % float(self.amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.NEW,
                        'OrderID is not None',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f' % (CURRENT_PRICE + 0.1),
                        'SettlDate is not None',                        
                        'Side == "%s"' % side,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))

# ExecutionReport(Account=20151500001,AvgPx=1.29012,ClOrdID=GDT_20,CumQty=1000.0,Currency=EUR,ExecID=4048396907913101000,ExecType=TRADE,LastPx=1.29012,LastQty=1000.0,LeavesQty=0.0,MsgSeqNum=4,MsgType=EXECUTION_REPORT,OrdStatus=FILLED,OrderID=4048396907268562,OrderQty=1000.0,Price=1.4,SenderCompID=ADSSUATTRADE,SendingTime=2012-10-29 14:28:10.922000,SettlDate=2012-10-31,Side=BUY,Symbol=EUR/USD,TargetCompID=FixIBdemo,Text=Trade Done)

        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx < %f' % (CURRENT_PRICE + 0.1),
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == %f' % float(self.amount),
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.TRADE,
                        'LastPx < %f ' % (CURRENT_PRICE + 0.1),
                        'LastQty == %f' % float(self.amount),
                        'LeavesQty == 0.0',
                        'OrdStatus == "%s"' % fix.OrdStatus.FILLED,
                        'OrderID is not None',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f ' % (CURRENT_PRICE + 0.1),
                        'SettlDate is not None',
                        'Side == "%s"' % side,
                        'Symbol == "%s"' % self.symbol,
                        'Text == "Trade Done"'                                                                                   
                    ))
            

    @attr('off')
    def testPlaceSellBelowMarket(self):
        assert CURRENT_PRICE
        
        clordid = "testPlaceSellBelow" + str(datetime.utcnow())
        side = fix.Side.SELL
        self.send(limit_order('EUR/USD', DIR_SELL, self.amount, CURRENT_PRICE - 0.1000, clordid))
        self.close_type = DIR_BUY                

        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID == "0"',
                        'ExecType == "%s"' % fix.ExecType.PENDING_NEW,
                        'LeavesQty == %f' % float(self.amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW,
                        'OrderID == "0"',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f' % (CURRENT_PRICE - 0.1),
                        'Side == "%s"' % side,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))

        
        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.NEW,
                        'LeavesQty == %f' % float(self.amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.NEW,
                        'OrderID is not None',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f' % (CURRENT_PRICE - 0.1),
                        'SettlDate is not None',                        
                        'Side == "%s"' % side,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))


        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx > %f' % (CURRENT_PRICE - 0.1),
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == %f' % float(self.amount),
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.TRADE,
                        'LastPx > %f ' % (CURRENT_PRICE - 0.1),
                        'LastQty == %f' % float(self.amount),
                        'LeavesQty == 0.0',
                        'OrdStatus == "%s"' % fix.OrdStatus.FILLED,
                        'OrderID is not None',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f ' % (CURRENT_PRICE - 0.1),
                        'SettlDate is not None',
                        'Side == "%s"' % side,
                        'Symbol == "%s"' % self.symbol,
                        'Text == "Trade Done"'                                                                                   
                    ))
            
            
            
class TestLimitOrderPlaceAndCancel(TestBase):            
                    
    def __init__(self):
        TestBase.__init__(self)
        self.amount = 1000
        self.symbol = 'EUR/USD'
        self.close_type = None
                    
    def setup(self):    
        global server
        self.server = server
        self.HeartBeatInt = HEARTBTINT        
    
    def teardown(self):
        pass
    
    
    @attr('off')
    def testPlaceBuyBelowMarket(self):
        assert CURRENT_PRICE
        
        clordid = "testPlaceBuyBelow"+str(datetime.now())
        self.send(limit_order('EUR/USD', DIR_BUY, self.amount, CURRENT_PRICE - 0.1000, clordid))
        
        """
        ExecutionReport(Account=20151500001,AvgPx=0.0,ClOrdID=GDT_21,CumQty=0.0,Currency=EUR,ExecID=0,ExecType=PENDING_NEW,LeavesQty=1000.0,MsgSeqNum=2,MsgType=EXECUTION_REPORT,OrdStatus=PENDING_NEW,OrderID=0,OrderQty=1000.0,Price=1.2,SenderCompID=ADSSUATTRADE,SendingTime=2012-10-29 14:55:07.690000,Side=BUY,Symbol=EUR/USD,TargetCompID=FixIBdemo)
        """
        self.expect(fix.ExecutionReport, (
                'Account is not None',
                'AvgPx == 0.0',
                'ClOrdID == "%s"' % clordid,
                'CumQty == 0.0',
                'Currency == "%s"' % self.symbol[:3],
                'ExecID == "0"',
                'ExecType == "%s"' % fix.ExecType.PENDING_NEW,
                'LeavesQty == %f' % float(self.amount),
                'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW,
                'OrderID == "0"',
                'OrderQty == %f' % float(self.amount),
                'Price == %f' % (CURRENT_PRICE - 0.1),
                'Side == "%s"' % fix.Side.BUY,
                'Symbol == "%s"' % self.symbol                                                                                   
            ))

        """
        ExecutionReport(Account=20151500001,AvgPx=0.0,ClOrdID=GDT_21,CumQty=0.0,Currency=EUR,ExecID=4048413076927606,ExecType=NEW,LeavesQty=1000.0,MsgSeqNum=3,MsgType=EXECUTION_REPORT,OrdStatus=NEW,OrderID=4048413076927606,OrderQty=1000.0,Price=1.2,SenderCompID=ADSSUATTRADE,SendingTime=2012-10-29 14:55:07.790000,SettlDate=2012-10-31,Side=BUY,Symbol=EUR/USD,TargetCompID=FixIBdemo)
        """
        
        reply = self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.NEW,
                        'LeavesQty == %f' % float(self.amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.NEW,
                        'OrderID is not None',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f' % (CURRENT_PRICE - 0.1),
                        'SettlDate is not None',                                                
                        'Side == "%s"' % fix.Side.BUY,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))
                                                
        self.send(fix.OrderCancelRequest(ClOrdID='testPlaceBuyBelow_cancel' + str(datetime.now()),
                                 OrigClOrdID= reply.ClOrdID,
                                 Account = reply.Account,
                                 Side = reply.Side,
                                 TransactTime = datetime.utcnow(),
                                 Symbol = 'EUR/USD'))

        
        """
        ExecutionReport(Account=20151500001,AvgPx=0.0,ClOrdID=GDT_21,CumQty=0.0,Currency=EUR,ExecID=4048413076927606,ExecType=CANCELED,LastPx=0.0,LastQty=0.0,LeavesQty=1000.0,MsgSeqNum=2,MsgType=EXECUTION_REPORT,OrdStatus=CANCELED,OrderID=4048413076927606,OrderQty=1000.0,Price=1.2,SenderCompID=ADSSUATTRADE,SendingTime=2012-10-29 14:58:39.874000,SettlDate=2012-10-31,Side=BUY,Symbol=EUR/USD,TargetCompID=FixIBdemo,Text=Canceled by user.)
        """
        
        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.CANCELED,
                        'LastPx == 0.0',
                        'LeavesQty == %f' % float(self.amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.CANCELED,
                        'OrderID is not None',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f' % (CURRENT_PRICE - 0.1),
                        'SettlDate is not None',                                                
                        'Side == "%s"' % fix.Side.BUY,
                        'Symbol == "%s"' % self.symbol,
                        'Text == "Canceled by user."'                                                                                   
                    ))
        
        
        
    @attr('off')
    def testPlaceSellAboveMarket(self):
        assert CURRENT_PRICE
        
        clordid = "testPlaceSellAbove"+str(datetime.now())
        self.send(limit_order('EUR/USD', DIR_SELL, self.amount, CURRENT_PRICE + 0.1000, clordid))
        
        self.expect(fix.ExecutionReport, (
                'Account is not None',
                'AvgPx == 0.0',
                'ClOrdID == "%s"' % clordid,
                'CumQty == 0.0',
                'Currency == "%s"' % self.symbol[:3],
                'ExecID == "0"',
                'ExecType == "%s"' % fix.ExecType.PENDING_NEW,
                'LeavesQty == %f' % float(self.amount),
                'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW,
                'OrderID == "0"',
                'OrderQty == %f' % float(self.amount),
                'Price == %f' % (CURRENT_PRICE + 0.1),
                'Side == "%s"' % fix.Side.SELL,
                'Symbol == "%s"' % self.symbol                                                                                   
            ))

        reply = self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.NEW,
                        'LeavesQty == %f' % float(self.amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.NEW,
                        'OrderID is not None',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f' % (CURRENT_PRICE + 0.1),
                        'SettlDate is not None',                                                
                        'Side == "%s"' % fix.Side.SELL,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))
                                                
        self.send(fix.OrderCancelRequest(ClOrdID='testPlaceSellAbove_cancel' + str(datetime.now()),
                                 OrigClOrdID= reply.ClOrdID,
                                 Account = reply.Account,
                                 Side = reply.Side,
                                 TransactTime = datetime.utcnow(),
                                 Symbol = 'EUR/USD'))

        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.CANCELED,
                        'LastPx == 0.0',
                        'LeavesQty == %f' % float(self.amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.CANCELED,
                        'OrderID is not None',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f' % (CURRENT_PRICE + 0.1),
                        'SettlDate is not None',                                                
                        'Side == "%s"' % fix.Side.SELL,
                        'Symbol == "%s"' % self.symbol,
                        'Text == "Canceled by user."'                                                                                   
                    ))
        


class TestStopOrderInstantFill(TestBase):
        
    
    def __init__(self):
        TestBase.__init__(self)
        self.amount = 1000
        self.symbol = 'EUR/USD'
        self.close_type = None        
                    
    def setup(self):
        global server
        self.server = server
        self.HeartBeatInt = HEARTBTINT   

         
    def teardown(self):
        if self.close_type is not None:                    
            close = market_order(self.out.Symbol, self.close_type, self.out.OrderQty, "close")
            self.close_type = None
            self.send(close)
            self.expect(fix.ExecutionReport)
            self.expect(fix.ExecutionReport)
            self.expect(fix.ExecutionReport)
                                                     
        
    @attr('off')
    def testPlaceBuyBelowMarket(self):
        assert CURRENT_PRICE
        
        clordid = "testPlaceBuyBelow" + str(datetime.utcnow())
        side = fix.Side.BUY
        self.send(stop_order('EUR/USD', DIR_BUY, self.amount, CURRENT_PRICE - 0.1000, clordid))                
        
        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID == "0"',
                        'ExecType == "%s"' % fix.ExecType.PENDING_NEW,
                        'LeavesQty == %f' % float(self.amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW,
                        'OrderID == "0"',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f' % (CURRENT_PRICE - 0.1),
                        'Side == "%s"' % side,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))
        
        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.NEW,
                        'LeavesQty == %f' % float(self.amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.NEW,
                        'OrderID is not None',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f' % (CURRENT_PRICE - 0.1),
                        'SettlDate is not None',                        
                        'Side == "%s"' % side,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))


        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0 ',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.CANCELED,
                        'LastPx == 0.0 ',
                        'LastQty == 0.0',
                        'LeavesQty == %f' % self.amount,
                        'OrdStatus == "%s"' % fix.OrdStatus.CANCELED,
                        'OrderID is not None',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f ' % (CURRENT_PRICE - 0.1),
                        'SettlDate is not None',
                        'Side == "%s"' % side,
                        'Symbol == "%s"' % self.symbol,
                        'Text == "Canceled"'                                                                                   
                    ))                
        

        
    @attr('off')
    def testPlaceSellAboveMarket(self):
        assert CURRENT_PRICE
        
        clordid = "testPlaceSellAbove" + str(datetime.utcnow())
        side = fix.Side.SELL
        self.send(stop_order('EUR/USD', DIR_SELL, self.amount, CURRENT_PRICE + 0.1000, clordid))                
        
        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID == "0"',
                        'ExecType == "%s"' % fix.ExecType.PENDING_NEW,
                        'LeavesQty == %f' % float(self.amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW,
                        'OrderID == "0"',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f' % (CURRENT_PRICE + 0.1),
                        'Side == "%s"' % side,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))
        
        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.NEW,
                        'LeavesQty == %f' % float(self.amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.NEW,
                        'OrderID is not None',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f' % (CURRENT_PRICE + 0.1),
                        'SettlDate is not None',                        
                        'Side == "%s"' % side,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))


        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0 ',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.CANCELED,
                        'LastPx == 0.0 ',
                        'LastQty == 0.0',
                        'LeavesQty == %f' % self.amount,
                        'OrdStatus == "%s"' % fix.OrdStatus.CANCELED,
                        'OrderID is not None',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f ' % (CURRENT_PRICE + 0.1),
                        'SettlDate is not None',
                        'Side == "%s"' % side,
                        'Symbol == "%s"' % self.symbol,
                        'Text == "Canceled"'                                                                                   
                    ))                
        
        
        
class TestStopOrderPlaceAndCancel(TestBase):
        
    
    def __init__(self):
        TestBase.__init__(self)
        self.amount = 1000
        self.symbol = 'EUR/USD'
                
                    
    def setup(self):
        global server
        self.server = server
        self.HeartBeatInt = HEARTBTINT   
        

    def teardown(self):
        pass
    
    
    @attr('off')
    def testPlaceSellBelowMarket(self):        
        assert CURRENT_PRICE
        
        clordid = "testPlaceSellBelow"+str(datetime.now())
        self.send(stop_order('EUR/USD', DIR_SELL, self.amount, CURRENT_PRICE - 0.1000, clordid))
        
        self.expect(fix.ExecutionReport, (
                'Account is not None',
                'AvgPx == 0.0',
                'ClOrdID == "%s"' % clordid,
                'CumQty == 0.0',
                'Currency == "%s"' % self.symbol[:3],
                'ExecID == "0"',
                'ExecType == "%s"' % fix.ExecType.PENDING_NEW,
                'LeavesQty == %f' % float(self.amount),
                'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW,
                'OrderID == "0"',
                'OrderQty == %f' % float(self.amount),
                'Price == %f' % (CURRENT_PRICE - 0.1),
                'Side == "%s"' % fix.Side.SELL,
                'Symbol == "%s"' % self.symbol                                                                                   
            ))

        reply = self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.NEW,
                        'LeavesQty == %f' % float(self.amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.NEW,
                        'OrderID is not None',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f' % (CURRENT_PRICE - 0.1),
                        'SettlDate is not None',                                                
                        'Side == "%s"' % fix.Side.SELL,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))
                                                
        self.send(fix.OrderCancelRequest(ClOrdID='testPlaceSellBelow_cancel' + str(datetime.now()),
                                 OrigClOrdID= reply.ClOrdID,
                                 Account = reply.Account,
                                 Side = reply.Side,
                                 TransactTime = datetime.utcnow(),
                                 Symbol = 'EUR/USD'))

        
        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.CANCELED,
                        'LastPx == 0.0',
                        'LeavesQty == %f' % float(self.amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.CANCELED,
                        'OrderID is not None',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f' % (CURRENT_PRICE - 0.1),
                        'SettlDate is not None',                                                
                        'Side == "%s"' % fix.Side.SELL,
                        'Symbol == "%s"' % self.symbol,
                        'Text == "Canceled by user."'                                                                                   
                    ))
        
        

    @attr('off')
    def testPlaceBuyAboveMarket(self):        
        assert CURRENT_PRICE
        
        clordid = "testPlaceBuyAbove"+str(datetime.now())
        self.send(stop_order('EUR/USD', DIR_BUY, self.amount, CURRENT_PRICE + 0.1000, clordid))
        
        self.expect(fix.ExecutionReport, (
                'Account is not None',
                'AvgPx == 0.0',
                'ClOrdID == "%s"' % clordid,
                'CumQty == 0.0',
                'Currency == "%s"' % self.symbol[:3],
                'ExecID == "0"',
                'ExecType == "%s"' % fix.ExecType.PENDING_NEW,
                'LeavesQty == %f' % float(self.amount),
                'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW,
                'OrderID == "0"',
                'OrderQty == %f' % float(self.amount),
                'Price == %f' % (CURRENT_PRICE + 0.1),
                'Side == "%s"' % fix.Side.BUY,
                'Symbol == "%s"' % self.symbol                                                                                   
            ))

        reply = self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.NEW,
                        'LeavesQty == %f' % float(self.amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.NEW,
                        'OrderID is not None',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f' % (CURRENT_PRICE + 0.1),
                        'SettlDate is not None',                                                
                        'Side == "%s"' % fix.Side.BUY,
                        'Symbol == "%s"' % self.symbol                                                                                   
                    ))
                                                
        self.send(fix.OrderCancelRequest(ClOrdID='testPlaceBuyAbove_cancel' + str(datetime.now()),
                                 OrigClOrdID= reply.ClOrdID,
                                 Account = reply.Account,
                                 Side = reply.Side,
                                 TransactTime = datetime.utcnow(),
                                 Symbol = 'EUR/USD'))

        
        self.expect(fix.ExecutionReport, (
                        'Account is not None',
                        'AvgPx == 0.0',
                        'ClOrdID == "%s"' % clordid,
                        'CumQty == 0.0',
                        'Currency == "%s"' % self.symbol[:3],
                        'ExecID is not None',
                        'ExecType == "%s"' % fix.ExecType.CANCELED,
                        'LastPx == 0.0',
                        'LeavesQty == %f' % float(self.amount),
                        'OrdStatus == "%s"' % fix.OrdStatus.CANCELED,
                        'OrderID is not None',
                        'OrderQty == %f' % float(self.amount),
                        'Price == %f' % (CURRENT_PRICE + 0.1),
                        'SettlDate is not None',                                                
                        'Side == "%s"' % fix.Side.BUY,
                        'Symbol == "%s"' % self.symbol,
                        'Text == "Canceled by user."'                                                                                   
                    ))
        