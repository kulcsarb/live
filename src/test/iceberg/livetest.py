'''
Created on Dec 10, 2012

@author: gdt
'''

import gdtlive.core.fix.dukascopy.fix as fix
#from fix.dukascopy.servertests import ServerTester
#from gdtlive.core.fix.messages import *
from gdtlive.core.fix.initiator import FIXInitiator
from logging.handlers import RotatingFileHandler 
import logging
import traceback
import time
import socket
from datetime import datetime 
import ssl
import pprint 

MsgSeqNum = 1


def send(sock, message):
    global MsgSeqNum
    message.MsgSeqNum = MsgSeqNum
    message.SendingTime = datetime.utcnow()        
    message.SenderCompID = SENDERCOMPID
    message.TargetCompID = TARGETCOMPID
    print datetime.now(), 'OUT:', message
    encoded = fix.encode(message)
    print datetime.now(), 'OUT:', pprint.pprint(encoded)
    sock.send(encoded)
    MsgSeqNum += 1
    


URL = '85.233.206.2'
FEED_PORT = 22301
TRADE_PORT = 22882
SENDERCOMPID = 'FIXKV'
TARGETCOMPID = 'ADSPRODORD'
PASSWORD = 'B.xafdhhbgqu1Kpo4.A4Q.'
USERNAME = None
    



sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock = ssl.wrap_socket(sock)
sock.connect((URL, TRADE_PORT))
logon = fix.Logon(Password=PASSWORD, Username=USERNAME, HeartBtInt=0, ResetSeqNumFlag=True, EncryptMethod=0)
send(sock, logon)
reply =  sock.recv(4096)
print datetime.now(), 'IN:', pprint.pprint(reply)
print datetime.now(), 'IN:', fix.decode(reply)



