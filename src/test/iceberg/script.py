'''
Created on Oct 18, 2012

@author: gdt
'''
import gdtlive.core.fix.dukascopy.fix as fix
#from fix.dukascopy.servertests import ServerTester
#from gdtlive.core.fix.messages import *
from gdtlive.core.fix.initiator import FIXInitiator
from logging.handlers import RotatingFileHandler 
import logging
import traceback
import time
import socket
from datetime import datetime 
import ssl
import pprint 

MsgSeqNum = 1


def send(sock, message):
    global MsgSeqNum
    message.MsgSeqNum = MsgSeqNum
    message.SendingTime = datetime.utcnow()        
    message.SenderCompID = SENDERCOMPID
    message.TargetCompID = TRADE_TARGETCOMPID
    print datetime.now(), 'OUT:', message
    encoded = fix.encode(message)
    print datetime.now(), 'OUT:', pprint.pprint(encoded)
    sock.send(encoded)
    MsgSeqNum += 1
    

URL = 'demo-api.dukascopy.com'
FEED_PORT = 9443
TRADE_PORT = 10443
SENDERCOMPID = 'DEMO3NxKxd_DEMOFIX'
TRADE_TARGETCOMPID = 'DUKASCOPYFIX'
FEED_TARGETCOMPID = 'DUKASCOPYFIX'
PASSWORD = 'NxKxd'
USERNAME = 'DEMO3NxKxd'



#URL = '85.233.206.5'
#FEED_PORT = 22301
#TRADE_PORT = 22302
#SENDERCOMPID = 'FixIBdemo'
#TRADE_TARGETCOMPID = 'ADSSUATTRADE'
#FEED_TARGETCOMPID = 'ADSSUAT'
#PASSWORD = 'XR.GT8TQXxOKb2L/RICny.'
#USERNAME = None

#URL = "85.233.206.2"
#TRADE_PORT = 22882
#SENDERCOMPID = "FIXIBL"
#TRADE_TARGETCOMPID = "ADSPRODORD"
#PASSWORD = "IrmcNIejBBvpaTdVK/lNE."



sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock = ssl.wrap_socket(sock)
sock.connect((URL, TRADE_PORT))
logon = fix.Logon(Password=PASSWORD, Username=USERNAME, HeartBtInt=5, ResetSeqNumFlag=True, EncryptMethod=0)
send(sock, logon)
reply =  sock.recv(4096)
print datetime.now(), 'IN:', pprint.pprint(reply)
print datetime.now(), 'IN:', fix.decode(reply)
#
#reply =  sock.recv(4096)
#print datetime.now(), 'IN:', pprint.pprint(reply)
#print datetime.now(), 'IN:', fix.decode(reply)
#
#reply =  sock.recv(4096)
#print datetime.now(), 'IN:', pprint.pprint(reply)
#print datetime.now(), 'IN:', fix.decode(reply)
#
#reply =  sock.recv(4096)
#print 'connection lost'
##sock.close()
##time.sleep(60)
#
#sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
##socket = ssl.wrap_socket(socket)
#sock.connect((URL, TRADE_PORT))
#logon = fix.Logon(Password=PASSWORD, HeartBtInt=70, ResetSeqNumFlag=False, EncryptMethod=0)
#send(sock, logon)
#reply =  sock.recv(4096)
###socket.write(fix.encode(logon))
###reply = socket.read(4096)
#print datetime.now(), 'IN:', pprint.pprint(reply)
#print datetime.now(), 'IN:', fix.decode(reply)

#
#message = fix.ResendRequest(BeginSeqNo=1, EndSeqNo=0)
#send(sock, message)

#quit()

#quit()
#message = fix.MarketDataRequest(MDReqID='GDT', 
#                                               SubscriptionRequestType = fix.SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES,
#                                               MarketDepth = 1,
#                                               MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
#                                               NoMDEntryTypes = [
#                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
#                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
#                                                                 ],
#                                               NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol='EUR/USD')]
#                                               )
#
#
#cancel = fix.MarketDataRequest(MDReqID='GDT', 
#                                               SubscriptionRequestType = fix.SubscriptionRequestType.DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST,
#                                               MarketDepth = 1,
#                                               MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
#                                               NoMDEntryTypes = [
#                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
#                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
#                                                                 ],
#                                               NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol='EUR/USD')]
#                                               )
#send(sock, message)
# 1.27131
#message = fix.NewOrderSingle(ClOrdID='GDT_113', 
#                                        Currency='EUR',
#                                        Symbol='EUR/USD', 
#                                        Side=fix.Side.BUY, 
#                                        OrderQty=10000,
#                                        Price = 1.3000,
#                                        TransactTime = datetime.utcnow(),
#                                        OrdType = fix.OrdType.STOP,
#                                        TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL,
#                                        DiscretionOffsetValue = 0,
#                                        )

#message = fix.NewOrderSingle(ClOrdID='GDT_108', 
#                                        Currency='EUR',                                        
#                                        Symbol='USDEUR', 
#                                        Side=fix.Side.SELL, 
#                                        OrderQty=10000,
#                                        TransactTime = datetime.utcnow(),
#                                        OrdType = fix.OrdType.MARKET,
#                                        TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL
#                                        )
#send(sock, message)

#message = fix.OrderCancelRequest(ClOrdID='GDT_110_Cancel',
#                                 OrigClOrdID='GDT_110',
#                                 Account = '20151500001',
#                                 Side = fix.Side.BUY,
#                                 TransactTime = datetime.utcnow(),
#                                 Symbol = 'EUR/USD')
#
#send(sock, message)

#TRADE-169, Stoploss-173 1353415506: pending new
#TRADE-169, Takeprofit-173 1353415506: pending new
#message = fix.OrderCancelReplaceRequest(OrderID=1707757234, OrigClOrdID='TRADE-16, Stoploss-173 1353415506', ClOrdID='TRADE-16, Stoploss-173 1353415506', OrdType=fix.OrdType.STOP, Symbol='EUR/USD', TimeInForce=fix.TimeInForce.GOOD_TILL_CANCEL, Side=fix.Side.SELL)
#message = fix.OrderCancelReplaceRequest(OrderID=170775701, OrigClOrdID='SYNC1', ClOrdID='TRADE-169, Stoploss-173 1353415506', OrdType=fix.OrdType.STOP, Symbol='EUR/USD', Side=fix.Side.SELL)
message = fix.OrderMassStatusRequest(MassStatusReqID='GDT', MassStatusReqType=fix.MassStatusReqType.STATUS_FOR_ALL_ORDERS)

send(sock, message)
#send(sock, fix.TestRequest(TestReqID='GDT'))

#print 'SEND', message
#print fix.encode(message)
#send(sock, message)
#send(sock, fix.Heartbeat())
rec = 0
buffer = ''
while True:
    message = sock.recv(4096)
    if not message:
        break
    message += sock.recv(4096)
    
    #print datetime.now(), 'IN:', pprint.pprint(message)
    print datetime.now(), 'IN:', fix.decode(message)
    #message = fix.decode(message)
    #print buffer
    
#    if type(message) == fix.TestRequest:
#        message = fix.Heartbeat(TestReqID = message.TestReqID)
#        send(sock, message)
#        
#    rec += 1
    #if rec == 2:
    #    message = fix.OrderStatusRequest(Symbol='EUR/USD', ClOrdID="GDT_113", Side=fix.Side.BUY)
    #    send(sock, message)    
    #socket.send(fix.encode(fix.Heartbeat()))
    
    
quit()
print datetime.now(), 'connection lost'
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#socket = ssl.wrap_socket(socket)
sock.connect((URL, TRADE_PORT))
logon = fix.Logon(Password=PASSWORD, HeartBtInt=5, ResetSeqNumFlag=True, EncryptMethod=0)
send(sock, logon)
reply =  sock.recv(4096)
#socket.write(fix.encode(logon))
#reply = socket.read(4096)
print datetime.now(), 'IN:', reply
print datetime.now(), 'IN:', fix.decode(reply)


#socket.send(fix.encode(message))
#reply =  socket.recv(4096)
#print reply
#print fix.decode(reply)
quit()

