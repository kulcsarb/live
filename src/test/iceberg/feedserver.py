# -*- encoding: utf-8 -*- 
'''
Created on Jun 14, 2012

@author: gdtlive
'''

from test.iceberg import TestBase, ServerTester, USERNAME, PASSWORD
from nose.plugins.attrib import attr
import gdtlive.core.fix.dukascopy.fix as fix
import time
from datetime import date

class TestFeedServer(TestBase):
    
    def __init__(self):
        TestBase.__init__(self, feed=True)        


    def setup(self):        
        self.server = ServerTester(self.URL, self.PORT, self.SENDERCOMPID, self.TARGETCOMPID)
        self.server.connect()         
        self.connection_closed = False
               
        self.send(fix.Logon(Username=USERNAME, Password=PASSWORD, HeartBtInt=self.HeartBeatInt, ResetSeqNumFlag=True, EncryptMethod=0))                                
        self.expect(fix.Logon, ('HeartBtInt == 30', 'ResetSeqNumFlag == True','EncryptMethod == 0'), timeout=5)

    
        
    @attr('off')
    def testMarketDataSnapshotUSDJPY(self):
        self.send(fix.MarketDataRequest(MDReqID='GDT', 
                                           SubscriptionRequestType = fix.SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES,
                                           MarketDepth = 1,
                                           MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                           NoMDEntryTypes = [
                                                             fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                             fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                             ],
                                           NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol='USD/JPY')]
                                           ))
        
        
        for i in xrange(3):
            self.expect(fix.MarketDataSnapshotFullRefresh, ('MDReqID=="GDT"', 
                                                            'Symbol=="USD/JPY"', 
                                                            'len(NoMDEntries) == 2',
                                                            
                                                            'NoMDEntries[0].ExpireDate is not None',                                                        
                                                            'type(NoMDEntries[0].MDEntryPx) == float',
                                                            'NoMDEntries[0].QuoteCondition == "%s" ' % fix.QuoteCondition.OPEN_ACTIVE,
                                                            'NoMDEntries[0].Currency == "JPY"',
                                                            'NoMDEntries[0].MDEntryType == "%s"' % fix.MDEntryType.BID,
                                                            'NoMDEntries[0].QuoteEntryID is not None',
                                                            'type(NoMDEntries[0].MDEntrySize) == float',
                                                            
                                                            'NoMDEntries[1].ExpireDate is not None',                                                        
                                                            'type(NoMDEntries[1].MDEntryPx) == float',
                                                            'NoMDEntries[1].QuoteCondition == "%s" ' % fix.QuoteCondition.OPEN_ACTIVE,
                                                            'NoMDEntries[1].Currency == "JPY"',
                                                            'NoMDEntries[1].MDEntryType == "%s"' % fix.MDEntryType.OFFER,
                                                            'NoMDEntries[1].QuoteEntryID is not None',
                                                            'type(NoMDEntries[1].MDEntrySize) == float'
                                                            
                                                            
                                                            ), timeout=20)
        
#        self.expect(fix.MarketDataSnapshotFullRefresh, timeout = 10)
        
        self.send(fix.MarketDataRequest(MDReqID='GDT', 
                                           SubscriptionRequestType = fix.SubscriptionRequestType.DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST,
                                           MarketDepth = 1,
                                           MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                           NoMDEntryTypes = [
                                                             fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                             fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                             ],
                                           NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol='USD/JPY')]
                                           ))                             
        time.sleep(2)                                           
        self.expect(None, timeout=5)
        
    
    #@attr('off')
    def testMarketDataSnapshotEURUSD(self):
        self.send(fix.MarketDataRequest(MDReqID='GDT', 
                                           SubscriptionRequestType = fix.SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES,
                                           MarketDepth = 1,
                                           MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                           NoMDEntryTypes = [
                                                             fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                             fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                             ],
                                           NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol='EUR/USD')]
                                           ))

        
        for i in xrange(3):
            self.expect(fix.MarketDataSnapshotFullRefresh, ('MDReqID=="GDT"', 
                                                            'Symbol=="EUR/USD"', 
                                                            'len(NoMDEntries) == 2',
                                                            
                                                            'NoMDEntries[0].ExpireDate is not None',                                                        
                                                            'type(NoMDEntries[0].MDEntryPx) == float',
                                                            'NoMDEntries[0].QuoteCondition == "%s" ' % fix.QuoteCondition.OPEN_ACTIVE,
                                                            'NoMDEntries[0].Currency == "USD"',
                                                            'NoMDEntries[0].MDEntryType == "%s"' % fix.MDEntryType.BID,
                                                            'NoMDEntries[0].QuoteEntryID is not None',
                                                            'type(NoMDEntries[0].MDEntrySize) == float',
                                                            
                                                            'NoMDEntries[1].ExpireDate is not None',                                                        
                                                            'type(NoMDEntries[1].MDEntryPx) == float',
                                                            'NoMDEntries[1].QuoteCondition == "%s" ' % fix.QuoteCondition.OPEN_ACTIVE,
                                                            'NoMDEntries[1].Currency == "USD"',
                                                            'NoMDEntries[1].MDEntryType == "%s"' % fix.MDEntryType.OFFER,
                                                            'NoMDEntries[1].QuoteEntryID is not None',
                                                            'type(NoMDEntries[1].MDEntrySize) == float'
                                                            
                                                            
                                                            ), timeout=10)

        
        self.send(fix.MarketDataRequest(MDReqID='GDT', 
                                           SubscriptionRequestType = fix.SubscriptionRequestType.DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST,
                                           MarketDepth = 1,
                                           MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                           NoMDEntryTypes = [
                                                             fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                             fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                             ],
                                           NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol='EUR/USD')]
                                           ))                             
        time.sleep(2)                                           
        self.expect(None, timeout=5)
        
    
        
    @attr('off')
    def testMarketDataRequestInvalidSymbol(self):        
        symbol = 'EURUSD'          
        self.send(fix.MarketDataRequest(MDReqID='GDT', 
                                           SubscriptionRequestType = fix.SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES,
                                           MarketDepth = 1,
                                           MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                           NoMDEntryTypes = [
                                                             fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                             fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                             ],
                                           NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol=symbol)]
                                           ))
        
        self.expect(fix.MarketDataRequestReject, ('"Invalid Symbol: %s" in Text' % symbol,
                                              'MDReqRejReason == "%s"' % fix.MDReqRejReason.UNKNOWN_SYMBOL),
                    skip=fix.QuoteStatusReport)
        
        
        
        
        
        
        
        
