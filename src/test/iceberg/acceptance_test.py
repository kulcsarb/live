'''
Created on Dec 6, 2012

@author: gdt
'''

import gdtlive.core.fix.dukascopy.fix as fix
#from fix.dukascopy.servertests import ServerTester
#from gdtlive.core.fix.messages import *
from gdtlive.core.fix.initiator import FIXInitiator
from logging.handlers import RotatingFileHandler 
import logging
import traceback
import time
import socket
from datetime import datetime, timedelta
import ssl
import pprint 
import re



URL = '85.233.206.5'
FEED_PORT = 22301
TRADE_PORT = 22302
SENDERCOMPID = 'FixKSROdemo'
TRADE_TARGETCOMPID = 'ADSSUATTRADE'
FEED_TARGETCOMPID = 'ADSSUAT'
PASSWORD = 'XR.GT8TQXxOKb2L/RICny.'
USERNAME = None
ACCOUNT_NUMBER = 20185500001

#URL = "85.233.206.2"
#TRADE_PORT = 22882
#SENDERCOMPID = "FIXIBL"
#TRADE_TARGETCOMPID = "ADSPRODORD"
#PASSWORD = "IrmcNIejBBvpaTdVK/lNE."


class ServerTest():

    def __init__(self):
        self.MsgSeqNum = 0
        self.last_send_time = None
        self.heartbeat_interval = 5
        self.port = TRADE_PORT
        self.url = URL
        self.SenderCompID = SENDERCOMPID
        self.TargetCompID = TRADE_TARGETCOMPID
        self.show_raw_message = True
        self.show_message = True
        
        
    def connect(self):
        self.send_heartbeat = False        
        self.MsgSeqNum = 1
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(0.1)
        #sock = ssl.wrap_socket(sock)
        self.socket.connect((self.url, self.port))
        
              
    def logout(self):
        self.send(fix.Logout())
        
    def disconnect(self):
        self.socket.close()
          
          
    def send(self, message):        
        message.MsgSeqNum = self.MsgSeqNum
        message.SendingTime = datetime.utcnow()        
        message.SenderCompID = self.SenderCompID
        message.TargetCompID = self.TargetCompID
        if self.show_message:
            print 'OUT:', datetime.utcnow(), '\t', message
        encoded = fix.encode(message)
        if self.show_raw_message:
            print 'OUT:', datetime.utcnow(), '\t', pprint.pprint(encoded)        
        self.socket.send(encoded)
        self.last_send_time = datetime.utcnow()
        self.MsgSeqNum += 1
    
    
    def receive(self):
        
        self.buffer = ''
        while True:
            try:
                data = self.socket.recv(4096)
                if not data:
                    print datetime.utcnow(), 'Connection closed'
                    break
                self.buffer += data
                
                for message in self.decode_messages():
                    yield message                                
            
            except socket.error:
                if self.send_heartbeat and self.last_send_time + timedelta(seconds=self.heartbeat_interval) < datetime.utcnow():                    
                    self.send(fix.Heartbeat())
                

    def decode_messages(self):
        while True:        
            match = re.search(fix.RE_MESSAGE_PATTERN, self.buffer, re.M)
            if not match:                
                break
                                                            
            raw_message = match.group(0)                    
            self.buffer = self.buffer[len(raw_message):]
            try:
                if self.show_raw_message:
                    print 'IN: ', datetime.utcnow(), '\t', pprint.pprint(raw_message)                
                yield fix.decode(raw_message)
            except GeneratorExit:
                pass


class FIXServerTest(ServerTest):
    
    def FIX_logon(self):
        print "1.1 Fix Logon"
        print
        self.connect() 
        self.send(fix.Logon(Password=PASSWORD, Username=USERNAME, HeartBtInt=self.heartbeat_interval, ResetSeqNumFlag=True, EncryptMethod=0))
        
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        
        #self.logout()
        self.disconnect()


    def FIX_reconnection(self):
        print
        print
        print "1.2 Fix reconnection"
        print
        self.connect() 
        self.send(fix.Logon(Password=PASSWORD, Username=USERNAME, HeartBtInt=self.heartbeat_interval, ResetSeqNumFlag=True, EncryptMethod=0))
        for message in self.receive():
            if message:
                print  'IN: ', datetime.utcnow(), '\t', message
        
        self.connect() 
        self.send(fix.Logon(Password=PASSWORD, Username=USERNAME, HeartBtInt=self.heartbeat_interval, ResetSeqNumFlag=True, EncryptMethod=0))
        
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        
        #self.logout()
        self.disconnect()
         
    def FIX_Heartbeat(self):
        print
        print
        print "2 Heartbeats"
        print
        self.connect()
        self.send_heartbeat = True 
        self.send(fix.Logon(Password=PASSWORD, Username=USERNAME, HeartBtInt=self.heartbeat_interval, ResetSeqNumFlag=True, EncryptMethod=0))
        
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message        
        
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message        
                        
        self.disconnect()
        
        
    def FIX_TestRequest(self):
        print
        print
        print "3 Test Request"
        print
        self.connect()        
        self.send(fix.Logon(Password=PASSWORD, Username=USERNAME, HeartBtInt=self.heartbeat_interval, ResetSeqNumFlag=True, EncryptMethod=0))
        
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message

        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        
        self.send(fix.Heartbeat(TestReqID=message.TestReqID))
        
        self.disconnect()


    def FIX_ResendRequest(self):
        print
        print
        print "4 Resend Request"
        print
        self.connect()        
        self.send(fix.Logon(Password=PASSWORD, Username=USERNAME, HeartBtInt=0, ResetSeqNumFlag=True, EncryptMethod=0))
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message                
        print 'disconnect'
        self.disconnect()        
        time.sleep(60)
        print 'connecting'
        self.connect()
        self.MsgSeqNum = 2
        self.send(fix.Logon(Password=PASSWORD, Username=USERNAME, HeartBtInt=0, ResetSeqNumFlag=False, EncryptMethod=0))
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        
        self.send(fix.ResendRequest(BeginSeqNo=1, EndSeqNo=0))
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message        
        
        self.disconnect()
        
        
        


class QuoteWorkflowTest(ServerTest):
    def __init__(self):
        ServerTest.__init__(self)
        print
        print 
        print '2.2 Quote Workflow'
        print 
        self.MsgSeqNum = 0
        self.last_send_time = None
        self.heartbeat_interval = 5
        self.port = FEED_PORT
        self.url = URL
        self.SenderCompID = SENDERCOMPID
        self.TargetCompID = FEED_TARGETCOMPID
        
        
    def subscription(self):
        print 
        print '1.1 Pair subscription'
        print
        self.connect()
        self.send(fix.Logon(Password=PASSWORD, Username=USERNAME, HeartBtInt=0, ResetSeqNumFlag=True, EncryptMethod=0))
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        
        message = fix.MarketDataRequest(MDReqID='GDT', 
                                               SubscriptionRequestType = fix.SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES,
                                               MarketDepth = 1,
                                               MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                               NoMDEntryTypes = [
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                                 ],
                                               NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol='EUR/USD')]
                                               )
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        self.disconnect()
        
    
    def subscription_wrong(self):
        print 
        print '1.2 Incorrect pair subscription'
        print
        self.connect()
        self.send(fix.Logon(Password=PASSWORD, Username=USERNAME, HeartBtInt=0, ResetSeqNumFlag=True, EncryptMethod=0))
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        
        message = fix.MarketDataRequest(MDReqID='GDT', 
                                               SubscriptionRequestType = fix.SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES,
                                               MarketDepth = 1,
                                               MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                               NoMDEntryTypes = [
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                                 ],
                                               NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol='USDEUR')]
                                               )
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        
        self.disconnect()
        
        
    def subscription_unsubscription(self):
        print 
        print '2 Pair un-subscription'
        print
        self.connect()
        self.send(fix.Logon(Password=PASSWORD, Username=USERNAME, HeartBtInt=10, ResetSeqNumFlag=True, EncryptMethod=0))
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        
        message = fix.MarketDataRequest(MDReqID='GDT', 
                                               SubscriptionRequestType = fix.SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES,
                                               MarketDepth = 1,
                                               MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                               NoMDEntryTypes = [
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                                 ],
                                               NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol='EUR/USD')]
                                               )
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message                
        
        message = fix.MarketDataRequest(MDReqID='GDT', 
                                               SubscriptionRequestType = fix.SubscriptionRequestType.DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST,
                                               MarketDepth = 1,
                                               MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                               NoMDEntryTypes = [
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                                 ],
                                               NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol='EUR/USD')])

        self.send(message)
        
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        
        self.disconnect()



class OrderWorkflowTest(ServerTest):
    def __init__(self):
        ServerTest.__init__(self)
        print
        print 
        print '3 Order Workflow'        
    
    def login(self):
        self.show_message = self.show_raw_message = True
        self.connect()
        self.send(fix.Logon(Password=PASSWORD, Username=USERNAME, HeartBtInt=0, ResetSeqNumFlag=True, EncryptMethod=0))
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        
        
    def marketorder_DAY(self):
        print
        print '1.1 Market Order with DAY option'
        print 
        self.login()
        clordid = 'GDT_'+str(datetime.now())
        message = fix.NewOrderSingle(ClOrdID=clordid, 
                                        Currency='EUR',
                                        Symbol='EUR/USD', 
                                        Side=fix.Side.BUY, 
                                        OrderQty=10000,        
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.MARKET,
                                        TimeInForce = fix.TimeInForce.DAY,                                        
                                        )
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        
        # Cleanup
        self.show_raw_message = self.show_message = False
        clordid = 'GDT_'+str(datetime.now())
        message = fix.NewOrderSingle(ClOrdID=clordid, 
                                        Currency='EUR',
                                        Symbol='EUR/USD', 
                                        Side=fix.Side.SELL, 
                                        OrderQty=10000,        
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.MARKET,
                                        TimeInForce = fix.TimeInForce.DAY,                                        
                                        )
        self.send(message)        
        
        message = self.receive().next()
        message = self.receive().next()
        message = self.receive().next()                        
        self.disconnect()
        
        

    def marketorder_GTC(self):
        print
        print '1.1 Market Order with GTC/GTD option'
        print 
        self.login()
        clordid = 'GDT_'+str(datetime.now())
        message = fix.NewOrderSingle(ClOrdID=clordid, 
                                        Currency='EUR',
                                        Symbol='EUR/USD', 
                                        Side=fix.Side.BUY, 
                                        OrderQty=10000,        
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.MARKET,
                                        TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL,                                        
                                        )
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        
        # Cleanup
        self.show_raw_message = self.show_message = False
        clordid = 'GDT_'+str(datetime.now())
        message = fix.NewOrderSingle(ClOrdID=clordid, 
                                        Currency='EUR',
                                        Symbol='EUR/USD', 
                                        Side=fix.Side.SELL, 
                                        OrderQty=10000,        
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.MARKET,
                                        TimeInForce = fix.TimeInForce.DAY,                                        
                                        )
        self.send(message)        
        
        message = self.receive().next()
        message = self.receive().next()
        message = self.receive().next()                        
        self.disconnect()

        
    def limitorder_DAY(self):
        print
        print '2.1 Limit Order with DAY option'
        print 
        self.login()
        clordid = 'GDT_'+str(datetime.now())
        message = fix.NewOrderSingle(ClOrdID=clordid, 
                                        Currency='EUR',
                                        Symbol='EUR/USD', 
                                        Side=fix.Side.BUY, 
                                        OrderQty=10000,   
                                        Price = 1.1,     
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.LIMIT,
                                        TimeInForce = fix.TimeInForce.DAY,
                                        DiscretionOffsetValue = 5                                        
                                        )
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message        
        
        # Cleanup
        self.show_raw_message = self.show_message = False        
        self.send(fix.OrderCancelRequest(ClOrdID='GDT_' + str(datetime.now()),
                                 OrigClOrdID= clordid,
                                 Account = ACCOUNT_NUMBER,
                                 Side = fix.Side.BUY,
                                 TransactTime = datetime.utcnow(),
                                 Symbol = 'EUR/USD'))                                    
        
        message = self.receive().next()                                        
        self.disconnect()
    
    def limitorder_IOC(self):
        print
        print '2.2 Limit Order with IOC/FOK option'
        print 
        self.login()
        clordid = 'GDT_'+str(datetime.now())
        message = fix.NewOrderSingle(ClOrdID=clordid, 
                                        Currency='EUR',
                                        Symbol='EUR/USD', 
                                        Side=fix.Side.BUY, 
                                        OrderQty=10000,   
                                        Price = 1.1,     
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.LIMIT,
                                        TimeInForce = fix.TimeInForce.IMMEDIATE_OR_CANCEL,
                                        DiscretionOffsetValue = 5                                        
                                        )
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message        
        
        # Cleanup
        self.show_raw_message = self.show_message = False        
        self.send(fix.OrderCancelRequest(ClOrdID='GDT_' + str(datetime.now()),
                                 OrigClOrdID= clordid,
                                 Account = ACCOUNT_NUMBER,
                                 Side = fix.Side.BUY,
                                 TransactTime = datetime.utcnow(),
                                 Symbol = 'EUR/USD'))                                    
        
        message = self.receive().next()                                        
        self.disconnect()


    def limitorder_GTC(self):
        print
        print '2.3 Limit Order with GTC/GTD option'
        print 
        self.login()
        clordid = 'GDT_'+str(datetime.now())
        message = fix.NewOrderSingle(ClOrdID=clordid, 
                                        Currency='EUR',
                                        Symbol='EUR/USD', 
                                        Side=fix.Side.BUY, 
                                        OrderQty=10000,   
                                        Price = 1.1,     
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.LIMIT,
                                        TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL,
                                        DiscretionOffsetValue = 5                                        
                                        )
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message        
        
        # Cleanup
        self.show_raw_message = self.show_message = False        
        self.send(fix.OrderCancelRequest(ClOrdID='GDT_' + str(datetime.now()),
                                 OrigClOrdID= clordid,
                                 Account = ACCOUNT_NUMBER,
                                 Side = fix.Side.BUY,
                                 TransactTime = datetime.utcnow(),
                                 Symbol = 'EUR/USD'))                                    
        
        message = self.receive().next()                                        
        self.disconnect()
        
        
    def stoporder_DAY(self):
        print
        print '3.1 Stop Order with DAY option'
        print 
        self.login()
        clordid = 'GDT_'+str(datetime.now())
        message = fix.NewOrderSingle(ClOrdID=clordid, 
                                        Currency='EUR',
                                        Symbol='EUR/USD', 
                                        Side=fix.Side.BUY, 
                                        OrderQty=10000,   
                                        Price = 1.7,     
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.STOP,
                                        TimeInForce = fix.TimeInForce.DAY,
                                        DiscretionOffsetValue = 5                                        
                                        )
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message        
        
        # Cleanup
        self.show_raw_message = self.show_message = False        
        self.send(fix.OrderCancelRequest(ClOrdID='GDT_' + str(datetime.now()),
                                 OrigClOrdID= clordid,
                                 Account = ACCOUNT_NUMBER,
                                 Side = fix.Side.BUY,
                                 TransactTime = datetime.utcnow(),
                                 Symbol = 'EUR/USD'))                                    
        
        message = self.receive().next()                                        
        self.disconnect()
        

    def stoporder_GTC(self):
        print
        print '3.2 Stop Order with GTC/GTD option'
        print 
        self.login()
        clordid = 'GDT_'+str(datetime.now())
        message = fix.NewOrderSingle(ClOrdID=clordid, 
                                        Currency='EUR',
                                        Symbol='EUR/USD', 
                                        Side=fix.Side.BUY, 
                                        OrderQty=10000,   
                                        Price = 1.7,     
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.STOP,
                                        TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL,
                                        DiscretionOffsetValue = 5                                        
                                        )
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message        
        
        # Cleanup
        self.show_raw_message = self.show_message = False        
        self.send(fix.OrderCancelRequest(ClOrdID='GDT_' + str(datetime.now()),
                                 OrigClOrdID= clordid,
                                 Account = ACCOUNT_NUMBER,
                                 Side = fix.Side.BUY,
                                 TransactTime = datetime.utcnow(),
                                 Symbol = 'EUR/USD'))                                    
        
        message = self.receive().next()                                        
        self.disconnect()
        

    def order_rejection(self):
        print
        print '4.1 Order rejection'
        print 
        self.login()
        clordid = 'GDT_'+str(datetime.now())
        message = fix.NewOrderSingle(ClOrdID=clordid, 
                                        Currency='EUR',
                                        Symbol='EUXSD', 
                                        Side=fix.Side.BUY, 
                                        OrderQty=10000,                                          
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.MARKET,
                                        TimeInForce = fix.TimeInForce.DAY,                                                                            
                                        )
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        
        

class OrderCancelAndModifyTest(ServerTest):
    def __init__(self):
        ServerTest.__init__(self)
        print
        print 
        print '4. Order Cancel & Modified test'        
    
    def login(self):
        self.show_message = True
        self.show_raw_message = True
        self.connect()
        self.send(fix.Logon(Password=PASSWORD, Username=USERNAME, HeartBtInt=0, ResetSeqNumFlag=True, EncryptMethod=0))
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message


    def limitorder_DAY(self):
        print
        print '1 Limit Order with DAY option'
        print 
        self.login()
        clordid = 'GDT_'+str(datetime.now())
        message = fix.NewOrderSingle(ClOrdID=clordid, 
                                        Currency='EUR',
                                        Symbol='EUR/USD', 
                                        Side=fix.Side.BUY, 
                                        OrderQty=10000,   
                                        Price = 1.1,     
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.LIMIT,
                                        TimeInForce = fix.TimeInForce.DAY,
                                        DiscretionOffsetValue = 5                                        
                                        )
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message        
                
        # Cleanup
        self.show_raw_message = self.show_message = False        
        self.send(fix.OrderCancelRequest(ClOrdID='GDT_' + str(datetime.now()),
                                 OrigClOrdID= clordid,
                                 Account = ACCOUNT_NUMBER,
                                 Side = fix.Side.BUY,
                                 TransactTime = datetime.utcnow(),
                                 Symbol = 'EUR/USD'))                                    
        
        message = self.receive().next()                                        
        self.disconnect()

    def order_modify(self):
        print
        print '1.1 Order Modify Request'
        print 
        self.login()
        clordid = 'GDT_'+str(datetime.now())
        message = fix.NewOrderSingle(ClOrdID=clordid, 
                                        Currency='EUR',
                                        Symbol='EUR/USD', 
                                        Side=fix.Side.BUY, 
                                        OrderQty=10000,   
                                        Price = 1.1,     
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.LIMIT,
                                        TimeInForce = fix.TimeInForce.DAY,
                                        DiscretionOffsetValue = 5                                        
                                        )
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message        
        order_id = message.OrderID
        
        message = fix.OrderCancelReplaceRequest(OrderID=order_id,
                                                OrigClOrdID=clordid,
                                                ClOrdID=clordid,
                                                OrdType=fix.OrdType.LIMIT,
                                                OrderQty=20000,
                                                Price = 1.0,     
                                                Symbol='EUR/USD',                                                
                                                TransactTime = datetime.utcnow(),
                                                TimeInForce=fix.TimeInForce.DAY,
                                                Side=fix.Side.BUY)
        
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
                
        # Cleanup
        self.show_raw_message = self.show_message = False        
        self.send(fix.OrderCancelRequest(ClOrdID='GDT_' + str(datetime.now()),
                                 OrigClOrdID= clordid,
                                 Account = ACCOUNT_NUMBER,
                                 Side = fix.Side.BUY,
                                 TransactTime = datetime.utcnow(),
                                 Symbol = 'EUR/USD'))                                    
        
        message = self.receive().next()                                        
        self.disconnect()
        
        

    def order_cancel(self):
        print
        print '1.2 Order Cancel'
        print 
        self.login()
        clordid = 'GDT_'+str(datetime.now())
        message = fix.NewOrderSingle(ClOrdID=clordid, 
                                        Currency='EUR',
                                        Symbol='EUR/USD', 
                                        Side=fix.Side.BUY, 
                                        OrderQty=10000,   
                                        Price = 1.1,     
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.LIMIT,
                                        TimeInForce = fix.TimeInForce.DAY,
                                        DiscretionOffsetValue = 5                                        
                                        )
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message        
                
        # Cleanup        
        self.send(fix.OrderCancelRequest(ClOrdID='GDT_' + str(datetime.now()),
                                 OrigClOrdID= clordid,
                                 Account = ACCOUNT_NUMBER,
                                 Side = fix.Side.BUY,
                                 TransactTime = datetime.utcnow(),
                                 Symbol = 'EUR/USD'))                                    
        
        message = self.receive().next()                                        
        self.disconnect()


    def order_status(self):
        print
        print '1.3 Order Status Request'
        print 
        self.login()
        clordid = 'GDT_'+str(datetime.now())
        message = fix.NewOrderSingle(ClOrdID=clordid, 
                                        Currency='EUR',
                                        Symbol='EUR/USD', 
                                        Side=fix.Side.BUY, 
                                        OrderQty=10000,   
                                        Price = 1.1,     
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.LIMIT,
                                        TimeInForce = fix.TimeInForce.DAY,
                                        DiscretionOffsetValue = 5                                        
                                        )
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message        
                
        message = fix.OrderStatusRequest(Symbol = 'EUR/USD', 
                                         ClOrdID = clordid,
                                         Side = fix.Side.BUY)
        
        self.send(message)
        message = self.receive().next()
        print  'IN: ', datetime.utcnow(), '\t', message        
        
        # Cleanup
        self.show_raw_message = self.show_message = False        
        self.send(fix.OrderCancelRequest(ClOrdID='GDT_' + str(datetime.now()),
                                 OrigClOrdID= clordid,
                                 Account = ACCOUNT_NUMBER,
                                 Side = fix.Side.BUY,
                                 TransactTime = datetime.utcnow(),
                                 Symbol = 'EUR/USD'))                                    
        
        message = self.receive().next()                                        
        self.disconnect()


if __name__ == '__main__':
    t = FIXServerTest()
    t.FIX_logon()
    t.FIX_reconnection()
    t.FIX_Heartbeat()
    t.FIX_TestRequest()
    t.FIX_ResendRequest()
    
    
    t = QuoteWorkflowTest()
    t.subscription()
    t.subscription_wrong()
    t.subscription_unsubscription()
    
    t = OrderWorkflowTest()
    t.marketorder_DAY()
    t.marketorder_GTC()    
    t.limitorder_DAY()
    t.limitorder_IOC()
    t.limitorder_GTC()
    t.stoporder_DAY()
    t.stoporder_GTC()
    t.order_rejection()
    
    t = OrderCancelAndModifyTest()
    t.limitorder_DAY()
    t.order_modify()
    t.order_cancel()
    t.order_status()

