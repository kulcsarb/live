'''
Created on Jun 17, 2014

@author: gdt
'''
import minilive.db as db
from gdtlive.evol.gnets.NetBaseEchoPredict import GNetEchoPredictMulti
from gdtlive.constants import TIMEFRAME, PRICE_TIME, DIR_BUY, DIR_SELL
from gdtlive.core.constants import TRADE_OPENED, TRADE_CLOSED
from helpers import calc_sl
import logging
import time
import traceback

log = logging.getLogger('live')


class Strategy:

    def __init__(self, portfolio_id, strategy_id, name, dns, symbol, timeframe):
        log.info('  strategy #%d, "%s", instrument: %s%s' % (strategy_id, name, symbol, TIMEFRAME[timeframe]))
        self.portfolio_id = portfolio_id
        self.id = strategy_id                          
        self.dns = dns
        self.symbol = symbol
        self.timeframe = timeframe                
        self.genome = GNetEchoPredictMulti.decode(self.dns)

                                                                                                                                                                
                                                                                                                                                                
    def evaluate(self):   
        import minilive.datafeed.historic as historic
        self.vote = 0
        try:
            s = time.time() 
            index = len(historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME])-1                                
            self.genome.prepareForBacktest(historic.DATAROWS[self.timeframe], self)
            self.vote = self.genome.signal[index]                                                          
            log.info('  strategy %d  %s  evaluate: %.2fs  vote:%d' % (self.id, historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME][index], (time.time() - s), self.vote))                                                
        except:
            log.error(traceback.format_exc())
        
        return self.vote
        
    
    def get_trade(self):        
        import minilive.datafeed.historic as historic        
        if not self.vote: 
            return                         
        sl = calc_sl(self.symbol, self.vote, 120)
        dir = DIR_BUY if self.vote > 0 else DIR_SELL
        price = 'ASK' if self.vote > 0 else 'BID'                            
        trade = {
                 'strategy_id' : self.id,
                 'portfolio_id' : self.portfolio_id,
                 'state' : TRADE_OPENED,
                 'symbol' : self.symbol,
                 'direction' : dir,
                 'open_time' : historic.CURRENT_TIME,
                 'open_price' : historic.CURRENT_PRICE[self.symbol][price],
                 'amount' : 10000,
                 'sl_price' : sl                 
                 }
        return trade
        