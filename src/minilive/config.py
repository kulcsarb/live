# -*- encoding: utf-8 -*- 
'''
OK Konfigurációs változókat tároló modul.

A modul feladata a szoftver eltérő területein használt beállítások, változók, konstansok
egységes tárolása, valamint a szoftver inditásakor azok értékeinek automatikus betöltése
a konfigurációs fileból. 

@status: Ellenőrzive, rendben.
@author: kulcsarb
'''
from os.path import dirname, sep
import sys
from ConfigParser import ConfigParser
global configured


configured = False
config_files = [dirname(__file__) + sep + 'etc' + sep + 'minilive.conf', '/etc/gdtlive/minilive.conf'] 

RESOURCES_ROOT = dirname(__file__)+sep+'resources'+sep


def load_config(files):    
    parser = ConfigParser()
    parser.read(files)    

    for section in parser.sections():
        #print '[', section,']'
        for option in parser.options(section):
            key = option.upper()
            value = parser.get(section, option)
            sys.modules['minilive.config'].__dict__[key] = value
            

if not configured:
    load_config(config_files)
    configured = True
