'''
Created on May 21, 2014

@author: gdt
'''
# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText


def send(email, subject, text):    
    msg = MIMEText(text)    
    msg['Subject'] = subject
    msg['From'] = 'gdt@live'
    msg['To'] = email
    s = smtplib.SMTP('localhost')
    s.sendmail('gdt@live', [email], msg.as_string())
    s.quit()