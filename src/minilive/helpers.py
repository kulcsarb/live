'''
Created on Jun 20, 2014

@author: gdt
'''
import logging
import logging.handlers
from gdtlive.constants import PIP_MULTIPLIER

def init_logging():
    log = logging.getLogger('live')    
    log.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(message)s"))
    log.addHandler(handler)

    handler = logging.handlers.SysLogHandler('/dev/log')
    handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(message)s"))
    log.addHandler(handler)

    handler = logging.handlers.SMTPHandler('localhost', 'gdt@live', 'kulcsarb@gmail.com', 'CRITICAL ERROR IN LIVE')
    handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(message)s"))
    handler.setLevel(logging.ERROR)
    log.addHandler(handler)


    
def calc_sl(symbol, vote, sl):
    import minilive.datafeed.historic as historic        
    if vote > 0:
        sl = historic.CURRENT_PRICE[symbol]['BID'] - sl * PIP_MULTIPLIER[symbol]            
    elif vote < 0:            
        sl = historic.CURRENT_PRICE[symbol]['ASK'] + sl * PIP_MULTIPLIER[symbol]      
    else:
        sl = 0
    return sl
                                        
