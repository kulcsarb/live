'''
Created on Jan 29, 2013

@author: gdt
'''

from gdtlive.backtest.backtester import Backtester
from gdtlive.constants import STRATEGYRUN_BACKTEST, PERFORMANCE_CURRENCY
from gdtlive.admin.system.log import initialize_loggers
import gdtlive.historic.datarow
import minilive.datarow
import gdtlive.config
import minilive.db as db
from helpers import init_logging
from ast import literal_eval
from datetime import date, datetime
from multiprocessing import Process
import traceback
import logging
import random
import datarow

log = logging.getLogger('live')


def backtest(session, symbol, timeframe, dns, mm_config, run_from, run_to):    
    accPuginId = 1
    gdtlive.config.WEEKEND_START = 16
    backtest = Backtester(db.Session)
    backtest.setStratPlugin(4, accPuginId, 1)
        
    datarow = session.query(db.DatarowDescriptor).filter(db.DatarowDescriptor.symbol == symbol).filter(db.DatarowDescriptor.timeframe_num == timeframe).first()
    if not datarow:
        log.info('datarow not found for %s %d' % (symbol, timeframe))
        return
    if type(run_to) == date:
        run_to = datetime(run_to.year, run_to.month, run_to.day)    
    if type(run_from) == date:
        run_from = datetime(run_from.year, run_from.month, run_from.day)
    
    run_from = max(run_from, datarow.from_date)    
    run_to = min(run_to, datarow.to_date)        
    
    backtest.add_datarow(datarow.id)
                
    backtest.from_date = run_from
    backtest.to_date = run_to            
    backtest.strategy.configMM(1, mm_config)
    backtest.use_orderlog(True)
    backtest.strategy.commission = 0.5
    backtest.strategy.load_historic()
    log.info('evaluate...')        
    performance = backtest.evaluate(dns)                
    serial = backtest.get_serial_performance()
    tradelog = backtest.get_tradelog()    
    return mm_config, performance, serial, tradelog, run_from, run_to           
    
    

def main():
    # monkey patch the old function, so the backtest module use the new one
    gdtlive.historic.datarow.load = minilive.datarow.load
    initialize_loggers()
    init_logging()
    log.info('START ------------------------- ')
    db.init()        
    try:
        session = db.Session()        
        for strategy in session.query(db.Strategy).order_by(db.Strategy.id):
            try:
                log.info('backtesting strategy %d, %s' % (strategy.id, strategy.name))
                #eval_result = backtest(session, strategy.symbol, strategy.timeframe, strategy.dns, strategy.mm_config, strategy.backtest_to, strategy.eval_to)
                #log.info('eval net_profit: %.1f' % eval_result[1]['net_profit'])
                live_result = backtest(session, strategy.symbol, strategy.timeframe, strategy.dns, strategy.mm_config, strategy.eval_to, date.today())
                log.info('result net_profit: %.1f' % live_result[1]['net_profit'])                                
                strategy.live_perf = live_result
                strategy.live_to = date.today()
                session.commit()
            except:
                log.error(traceback.format_exc())            
             
    except:
        log.error(traceback.format_exc())    
    finally:
        session.close()
        log.info('END ---------------------------')        
        
        
        
if __name__ == '__main__':    
    main()
    