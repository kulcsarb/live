'''
Created on Jun 23, 2014

@author: gdt
'''
import minilive.db as db
from helpers import init_logging
from accounts import Account
import time

TIMEFRAME = 1440

def main():
    # init_logging()
    db.init()
    import minilive.datafeed.historic as historic                
    historic.init(TIMEFRAME)        
    
    session = db.Session()
    
    q = session.query(db.Account, db.Portfolio.id)
    q = q.filter(db.Account.id == db.PortfolioAccount.account_id)
    q = q.filter(db.PortfolioAccount.portfolio_id == db.Portfolio.id)
    q = q.filter(db.Portfolio.active == True)

    processes = []
    for account, portfolio_id in q:                
        proc = Account(account, portfolio_id)                
        if proc.symbol:        
            processes.append(proc)
            historic.server.register_instrument(proc.symbol, TIMEFRAME)
    
    session.close()        
    historic.server.preload()
        
    for p in processes:    
        p.start()    
    
    timeout = 60    
    while any([p.is_alive() for p in processes]) and timeout:
        time.sleep(1)
        timeout -= 1
        
    for p in processes:
        try:
            if p.is_alive():
                p.terminate()
        except:
            pass
    

if __name__ == '__main__':
    main()