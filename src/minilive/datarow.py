'''
Created on Jun 18, 2014

@author: gdt
'''
from gdtlive.constants import *
import minilive.db as db
from datetime import datetime, timedelta, date
import calendar
import logging
import traceback 
from datafeed.jforex import JForexLoader
from sqlalchemy import desc, and_

log = logging.getLogger('live')


def refresh(symbol, timeframe):
    session = db.Session()
    datarow = session.query(db.DatarowDescriptor).filter(db.DatarowDescriptor.symbol == symbol).filter(db.DatarowDescriptor.timeframe_num == timeframe).first()
        
    if not datarow:
        log.info('Creating datarow for %s%s' % (symbol, TIMEFRAME[timeframe])) 
        datarow = db.DatarowDescriptor(symbol, timeframe, None, None)
        session.add(datarow)
        session.commit()
        
    q = session.query(db.DatarowData.time).filter(db.DatarowData.datarowId == datarow.id).order_by(desc(db.DatarowData.time))
    try:
        last_candle, = q.first()
        log.info('last candle: %s' % last_candle)    
    except:
        last_candle = None
        
    if not last_candle:        
        last_candle = datetime(2010,1,1)                
    else:                
        last_candle += timedelta(minutes=timeframe)
            
    sec = calendar.timegm(datetime.utcnow().utctimetuple())
    sec = ((sec / (timeframe*60)) * (timeframe*60)) - (timeframe * 60)
    to_time = datetime.utcfromtimestamp(sec)    
    if last_candle <= to_time:
        log.info('starting loader...')
        loader = JForexLoader([symbol], timeframe, last_candle, to_time)        
        result = loader.run()
        log.info('loader exited.')
        candles = result[symbol[:3]+'/'+symbol[3:]]                               
        save_candles(candles, datarow.symbol, datarow.timeframe_num)                              
    else:
        log.info('nothing to refresh')
    
    session.commit()
    session.close()
    return None


def save_candles(candles, symbol, timeframe):
    if not candles: 
        return 
    
    session = db.Session()
    descriptor = session.query(db.DatarowDescriptor).filter(db.DatarowDescriptor.symbol == symbol).filter(db.DatarowDescriptor.timeframe_num == timeframe).first()
    data = []
    for candle in candles:
        data.append( {'datarowId': descriptor.id,
                 'time': datetime.utcfromtimestamp(candle[0]), 
                 'ask_open': candle[1],
                 'ask_high': candle[2],
                 'ask_low': candle[3],
                 'ask_close': candle[4],
                 'bid_open': candle[5],
                 'bid_high': candle[6],
                 'bid_low': candle[7],
                 'bid_close': candle[8],
                 'volume' : candle[9]
                } )
    
    table = db.DatarowData.__table__
    command = table.insert().values(data)
    db.engine.execute(command)
    if not descriptor.from_date:
        descriptor.from_date = data[0]['time']
    descriptor.to_date = data[-1]['time']
    
    log.info('Datarow %s%s is from %s to %s' % (symbol, TIMEFRAME[timeframe], descriptor.from_date, descriptor.to_date))
    session.commit()
    session.close()
    
    
def load(datarow_id, from_date, to_date):    
    from datetime import time
    result = {
                  PRICE_TIME : [],
                  PRICE_ASKOPEN : [],
                  PRICE_ASKHIGH : [],
                  PRICE_ASKLOW : [],
                  PRICE_ASKCLOSE : [],
                  PRICE_BIDOPEN : [],
                  PRICE_BIDHIGH : [],
                  PRICE_BIDLOW : [],
                  PRICE_BIDCLOSE : [],
                  PRICE_VOLUME: []                  
              }
            
    session = db.Session()
    if type(from_date) == date:
        from_date = datetime.combine(from_date, time.min)
    if type(to_date) == date:
        to_date = datetime.combine(to_date, time.min)
    
    try:        
        datarow = session.query(db.DatarowDescriptor).get(datarow_id)                        

        from_date = from_date if from_date > datarow.from_date else datarow.from_date
        to_date = to_date if to_date < datarow.to_date else datarow.to_date
        
        if from_date > to_date:
            candles = []
        else:
            data = db.DatarowData.__table__                
            candles = db.engine.execute(
                data.select().\
                          where(
                                and_(                                     
                                     data.c.datarowId == datarow.id,
                                     data.c.time >= from_date,
                                     data.c.time <= to_date
                                     )
                                ).\
                                order_by(data.c.time)                                
            )
            
        for c in candles:
            t = c['time']
            if t < from_date or (t > to_date and datarow.timeframe_num >= 1440) or (t >= to_date and datarow.timeframe_num < 1440):
                continue                        
            result[PRICE_TIME].append(c['time'])
            result[PRICE_ASKOPEN].append(c['ask_open'])
            result[PRICE_ASKHIGH].append(c['ask_high'])
            result[PRICE_ASKLOW].append(c['ask_low'])
            result[PRICE_ASKCLOSE].append(c['ask_close'])
            result[PRICE_BIDOPEN].append(c['bid_open'])
            result[PRICE_BIDHIGH].append(c['bid_high'])
            result[PRICE_BIDLOW].append(c['bid_low'])
            result[PRICE_BIDCLOSE].append(c['bid_close'])
            result[PRICE_VOLUME].append(c['volume'])
        
    except:        
        print traceback.format_exc()
    finally:
        session.close()
    return result



def load2(symbol, timeframe, from_date):
    data = db.DatarowData.__table__
    descriptor = db.DatarowDescriptor.__table__    
    candles = db.engine.execute(
                data.select().\
                          where(
                                and_(                                     
                                     data.c.datarowId == descriptor.c.id,
                                     data.c.time >= from_date,
                                     descriptor.c.symbol == symbol,
                                     descriptor.c.timeframe_num == timeframe)
                                ).\
                                order_by(data.c.time)                                
            )

    result = {
              PRICE_TIME : [],
              PRICE_ASKOPEN : [],
              PRICE_ASKHIGH : [],
              PRICE_ASKLOW : [],
              PRICE_ASKCLOSE : [],
              PRICE_BIDOPEN : [],
              PRICE_BIDHIGH : [],
              PRICE_BIDLOW : [],
              PRICE_BIDCLOSE : [],              
              PRICE_VOLUME : []
          }
        
    for c in candles:                        
        result[PRICE_TIME].append(c['time'])
        result[PRICE_ASKOPEN].append(c['ask_open'])
        result[PRICE_ASKHIGH].append(c['ask_high'])
        result[PRICE_ASKLOW].append(c['ask_low'])
        result[PRICE_ASKCLOSE].append(c['ask_close'])
        result[PRICE_BIDOPEN].append(c['bid_open'])
        result[PRICE_BIDHIGH].append(c['bid_high'])
        result[PRICE_BIDLOW].append(c['bid_low'])
        result[PRICE_BIDCLOSE].append(c['bid_close'])
        result[PRICE_VOLUME].append(c['volume'])                            
    
    return result   

        