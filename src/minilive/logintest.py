'''
Created on Jun 20, 2014

@author: gdt
'''
import minilive.db as db
import gdtlive.core.fix.dukascopy.fix as fix
from accounts.fix.server import FixAccountBase
import logging
import traceback
from helpers import init_logging


log = logging.getLogger('live')

SETTINGS = {
            'URL': 'demo-api.dukascopy.com',
            'PORT' : 10443,
            'SENDERCOMPID': 'DEMO3NxKxd_DEMOFIX',
            'TARGETCOMPID': 'DUKASCOPYFIX',            
            'PASSWORD' : 'NxKxd',
            'USERNAME' : 'DEMO3NxKxd'            
            }


class AccountLoginTest(FixAccountBase):
    
    def __init__(self, settings):
        FixAccountBase.__init__(self, settings)
        self.show_message = True
        self.success = False 
        self.status = ''      
        self.error = ''
        self.login_msg = None
        self.logout_msg = None     
        
        
    def run(self):        
        try:
            self.connect()
        except Exception as e:
            self.status = "Connection failed - wrong url, and/or port settings, or the current IP is not registered with dukascopy"
            self.error = traceback.format_exc()
            return
        
        try:
            self.login()
            for message in self.receive():                            
                if type(message) == fix.Logout:
                    self.logout_msg = message
                    self.status = 'Login rejected (wrong username/password)'
                if type(message) == fix.Logon:
                    self.login_msg = message
                    self.success = False if self.logout_msg else True                
                    if self.success:
                        self.status = 'Login successfull'   
            
            if not self.login_msg and not self.logout_msg:
                self.status = 'Connection closed after login - wrong FIX SenderCompID/TargetCompID or the current IP is not registered'
                return 
            
            self.logout()
            
        except:
            self.status = "Unknown error occured"
            self.error = traceback.format_exc()
            
        finally:
            self.disconnect()
        
        

def main():
    init_logging()
    db.init()
    
    session = db.Session()
    q = session.query(db.Account)
    q = q.filter(db.Account.plugin_id == 1)
    for account in q:
        log.info('----- testing %s -----' % account.name)
        test = AccountLoginTest(account.plugin_settings)
        test.run()        
        log.info('status:  %s' % test.status)
        if test.error:
            log.error('error: %s' % test.error)
        
         
    session.close()
    
    
    
if __name__ == '__main__':
    main()
    # init_logging()
    # test = AccountLoginTest(SETTINGS)
    # test.run()
    # log.info('status:  %s' % test.status)
    # if test.error:
    #     log.error('error: %s' % test.error)
