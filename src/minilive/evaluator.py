'''
Created on Jun 17, 2014

@author: gdt
'''
import traceback
import logging
import time
import sys
from datetime import datetime

from sqlalchemy import and_

import minilive.db as db
from portfolio import Portfolio
from helpers import init_logging
from gdtlive.constants import DIR_BUY, DIR_SELL, ORDERTYPE_CLOSE
from gdtlive.core.constants import TRADE_OPENED, TRADE_CLOSED


log = logging.getLogger('live')


class Evaluator:
    def load_portfolios(self):
        session = db.Session()
        self.portfolios = [Portfolio(portfolio) for portfolio in
                           session.query(db.Portfolio).filter(db.Portfolio.active == True)]
        session.close()

    def run(self):
        try:
            import minilive.datafeed.historic as historic

            historic.init(1440)
            historic.server.register_instrument('EURUSD', 1440)

            self.load_portfolios()

            historic.server.preload()
            self.prepare()

            if historic.server.load_next_candle():
                self.evaluate()

            sys.exit(0)
        except:
            log.critical(traceback.format_exc())
            sys.exit(1)


    def prepare(self):
        for p in self.portfolios:
            p.prepare()


    def evaluate(self):
        self.close_trades()

        for p in self.portfolios:
            p.evaluate()

        self.save_trades()


    def close_trades(self):
        import minilive.datafeed.historic as historic
        from sqlalchemy import and_

        s = time.time()
        table = db.Trade.__table__
        connection = db.engine.connect()

        command = table.update().where(and_(table.c.state == TRADE_OPENED, table.c.direction == DIR_BUY)). \
            values(
                state=TRADE_CLOSED,
                close_price=historic.CURRENT_PRICE['EURUSD']['BID'],
                close_type=ORDERTYPE_CLOSE,
                close_time=historic.CURRENT_TIME
            )
        connection.execute(command)

        command = table.update().where(and_(table.c.state == TRADE_OPENED, table.c.direction == DIR_SELL)). \
            values(
                state=TRADE_CLOSED,
                close_price=historic.CURRENT_PRICE['EURUSD']['ASK'],
                close_type=ORDERTYPE_CLOSE,
                close_time=historic.CURRENT_TIME
            )
        connection.execute(command)

        connection.close()

        log.info('all open trades closed in %.2f' % (time.time() - s))


    def save_trades(self):
        s = time.time()
        data = []
        for p in self.portfolios:
            trade = p.get_trade()
            if trade:
                data.append(trade)

            for strategy in p.strategies:
                trade = strategy.get_trade()
                if trade:
                    data.append(trade)

        table = db.Trade.__table__
        command = table.insert().values(data)
        db.engine.execute(command)

        log.info('%d trades saved in %.2f' % (len(data), time.time() - s))


def main():
    init_logging()
    db.init()
    Evaluator().run()


def sqltest():
    from datetime import datetime

    db.init()
    print 'insert',
    s = time.time()
    data = []
    trade = {
        'strategy_id': 1,
        'portfolio_id': 1,
        'state': TRADE_OPENED,
        'symbol': 'EURUSD',
        'direction': DIR_BUY,
        'open_time': datetime.utcnow(),
        'open_price': 1.2345,
        'amount': 10000,
        'sl_price': 1.4321
    }
    data = [trade] * 1000

    table = db.Trade.__table__
    command = table.insert().values(data)
    db.engine.execute(command)

    print '%.4f' % (time.time() - s)


def closetest():
    db.init()
    print 'close all',
    s = time.time()
    table = db.Trade.__table__

    connection = db.engine.connect()

    command = table.update().where(and_(table.c.state == TRADE_OPENED, table.c.direction == DIR_BUY)). \
        values(
        state=TRADE_CLOSED,
        close_price=2.3245,
        close_type=ORDERTYPE_CLOSE,
        close_time=datetime.utcnow()
    )
    connection.execute(command)

    command = table.update().where(and_(table.c.state == TRADE_OPENED, table.c.direction == DIR_SELL)). \
        values(
        state=TRADE_CLOSED,
        close_price=2.2345,
        close_type=ORDERTYPE_CLOSE,
        close_time=datetime.utcnow()
    )
    connection.execute(command)

    connection.close()

    print '%.4f' % (time.time() - s)


if __name__ == '__main__':
    main()

# count = 1
# while True:
#         sqltest()
#         closetest()
#         print count*1000, ' trades'
#         time.sleep(0.5)
#         count += 1        

