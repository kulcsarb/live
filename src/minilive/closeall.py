'''
Created on Jun 20, 2014

@author: gdt
'''
import gdtlive.core.fix.dukascopy.fix as fix
import minilive.db as db
from accounts.fixaccount import SimpleFixAccount
import logging
from helpers import init_logging
import traceback
import mail


log = logging.getLogger('live')


def close_all_active_account():
    session = db.Session()
    q = session.query(db.Account)
    q = q.filter(db.Account.plugin_id == 1)
    q = q.filter(db.Account.id == db.PortfolioAccount.account_id)
    q = q.filter(db.PortfolioAccount.portfolio_id == db.Portfolio.id)
    q = q.filter(db.Portfolio.active == True)    
    for account in q:    
        log.info('Closing %s' % account.name)
        SimpleFixAccount(account.plugin_settings).set_exposure('EURUSD', 0, 0)
    
    session.close()
            

def main():
    init_logging()
    db.init()
    close_all_active_account()
        

if __name__ == '__main__':
    main()
    