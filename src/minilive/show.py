'''
Created on Jun 23, 2014

@author: gdt
'''
import minilive.db as db 

PLUGINS = {
           1 : 'FIX Account'           
           }


def main():
    db.init()
    session = db.Session()        
    for portfolio in session.query(db.Portfolio):
        print 'Portfolio ID %d, "%s" %s' % (portfolio.id, portfolio.name, 'ACTIVE' if portfolio.active else 'PASSIVE')                
        print '  running_from:  %s' % portfolio.running_from
        print '  running_top:  %s' % portfolio.running_to
        strategy_count = session.query(db.PortfolioStrategy.id).filter(db.PortfolioStrategy.portfolio_id == portfolio.id).count()
        print '  # of strategies: %d' % strategy_count 
        
        print '  Accounts: '
        account_q = session.query(db.Account).filter(db.Account.id == db.PortfolioAccount.account_id).filter(db.PortfolioAccount.portfolio_id == portfolio.id)
        for account in account_q:
            print '    ID %d, %s "%s"' % (account.id, PLUGINS[account.plugin_id], account.name)
        
        print
        print 
        
        
if __name__ == '__main__':
    main()


