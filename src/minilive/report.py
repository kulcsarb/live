'''
Created on Jun 20, 2014

@author: gdt
'''
from datetime import datetime
import logging
import traceback
import sys

import gdtlive.core.fix.dukascopy.fix as fix
import minilive.db as db
from accounts.fix.server import FixAccountBase
from helpers import init_logging
import mail


log = logging.getLogger('live')

SETTINGS = {
    'URL': 'demo-api.dukascopy.com',
    'PORT': 10443,
    'SENDERCOMPID': 'DEMO3NxKxd_DEMOFIX',
    'TARGETCOMPID': 'DUKASCOPYFIX',
    'PASSWORD': 'NxKxd',
    'USERNAME': 'DEMO3NxKxd'
}


class AccountInfo(FixAccountBase):
    def __init__(self, settings):
        FixAccountBase.__init__(self, settings)
        self.show_message = False
        self.info = None
        self.instruments = []
        self.orders = []


    def get(self):
        try:
            self.connect()
            self.login()
            self.account_info()
            for message in self.receive():
                if type(message) == fix.AccountInfo:
                    self.info = message
                if type(message) == fix.InstrumentPositionInfo:
                    self.instruments.append(message)
                if type(message) == fix.ExecutionReport:
                    self.orders.append(message)
            self.logout()
        except:
            log.error(traceback.format_exc())
        finally:
            self.disconnect()
        return self


def format_message(name, account):
    message = """---- %s ----\n
Login: %s
    
Equity : %s
Usable Margin: %s    

------ Positions ------    
""" % (name, account.info.AccountName, account.info.Equity, account.info.UsableMargin)

    for instrument in account.instruments:
        message += "  %s  %s  %d  (%.5f)\n" % (
            instrument.Symbol, 'LONG' if instrument.Amount > 0 else 'SHORT', abs(instrument.Amount), instrument.Price)

    message += "\n"
    message += "------ Orders ------\n"
    for order in account.orders:
        message += '  %s  %s  %s  %s  %s  %d  %.5f\n' % (
            order.ClOrdID, order.get('OrdType'), order.get('OrdStatus'), order.Symbol, order.get('Side'),
            order.OrderQty,
            order.AvgPx)

    return message


def save_performance(session, account_id, account):
    if account.info:
        log.info('saving performance')
        now = datetime.utcnow()
        session.add(db.AccountEquity(account_id, now, account.info.Equity, account.info.UsableMargin))

        for instrument in account.instruments:
            session.add(db.AccountExposure(account_id, now, instrument.Symbol, instrument.Amount, instrument.Price))

        for order in account.orders:
            session.add(db.AccountOrder(account_id, now, order.get('OrdType'), order.get('OrdStatus'), order.ClOrdID,
                                        order.Symbol, order.get('Side'), order.OrderQty, order.AvgPx))

        session.commit()
    else:
        log.info('account info missing!')


def make_reports(account_id = 0, send_mail = False):
    session = db.Session()
    if account_id:
        q = session.query(db.Account).filter(db.Account.id == account_id)
    else:
        q = session.query(db.Account)
        q = q.filter(db.Account.plugin_id == 1)
        q = q.filter(db.Account.id == db.PortfolioAccount.account_id)
        q = q.filter(db.PortfolioAccount.portfolio_id == db.Portfolio.id)
        q = q.filter(db.Portfolio.active == True)

    for account in q:
        log.info('Account ID %d - %s' % (account.id, account.name))
        acc = AccountInfo(account.plugin_settings).get()

        if acc.info:
            save_performance(session, account.id, acc)

            message = format_message(account.name, acc)
            subject = 'Account report: %s' % account.name
            log.info(message)

            if send_mail and account.email:
                log.info('sending email...')
                mail.send(account.email, subject, message)

    session.close()


def main():
    init_logging()
    db.init()
    try:
        account = int(sys.argv[1])
    except:
        account = 0
    try:
        send_mail = True if sys.argv[1] == '--mail' or sys.argv[2] == '--mail' else False
    except:
        send_mail = False

    make_reports(account, send_mail)


if __name__ == '__main__':
    main()

# account = AccountInfo(SETTINGS).get()
# print account.info.Equity
#     print account.info.Leverage
#     print account.info.UsableMargin
#     
#     print account.instruments
    
