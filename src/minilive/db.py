# -*- encoding: utf-8 -*- 
'''
Created on Jun 17, 2014

@author: gdt
'''
from sqlalchemy.pool import NullPool
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float, Time, Boolean, DateTime, Date, Text, create_engine, ForeignKey, Table, Sequence, LargeBinary, MetaData,  PickleType
from sqlalchemy.orm import sessionmaker, Session, relationship
from sqlalchemy.schema import Index
from sqlalchemy.exc import OperationalError
from datetime import datetime, date, timedelta
import logging
from gdtlive.constants import PRICE_BIDASK
from minilive.config import SQL_URL, SQL_USER, SQL_PASS


log = logging.getLogger('live')


new_db_created = False
session = None
engine = None
Session = None


def init(sql_user=SQL_USER, sql_pass=SQL_PASS, sql_url=SQL_URL):
    global session, Session, engine, new_db_created
    
    new_db_created = False    
    log.info('connecting to %s:%s@%s' % (sql_user, sql_pass, sql_url))            
    engine = create_engine('postgresql+psycopg2://%s:%s@%s' % (sql_user, sql_pass, sql_url), poolclass = NullPool, echo = False)        
    
    metadata = MetaData()
    try:
        metadata.create_all(engine)
    except OperationalError, e:
        if "not exist" in str(e):
            try:
                server_url, db_name = sql_url.split('/')
                log.info('Creating database %s....' % db_name)
                engine = create_engine('postgresql+psycopg2://%s:%s@%s/postgres' % (sql_user, sql_pass, server_url), poolclass = NullPool, echo = False)
                session = sessionmaker(bind = engine)()
                session.connection().connection.set_isolation_level(0)
                session.execute("CREATE DATABASE %s WITH OWNER = %s ENCODING = 'UTF8' CONNECTION LIMIT = -1" % (db_name, sql_user))
                session.connection().connection.set_isolation_level(1)
                log.info('Database created.')
                engine = create_engine('postgresql+psycopg2://%s:%s@%s' % (sql_user, sql_pass, sql_url), poolclass = NullPool, echo = False)
                new_db_created = True


            except Exception, e:
                log.error(str(e))
                return False
        else:
            log.error(str(e))
            return False

    Session = sessionmaker(bind = engine, expire_on_commit=False)
    metadata.create_all(engine)        
    
    Portfolio.metadata.create_all(engine)
    if new_db_created:
        session = Session()
        session.add(DatarowDescriptor('EURUSD', 1440, None, None))
        session.commit()
        session.close()
    return True


def getAttrNames(base):
    return [c.name for c in base.__table__.columns]


def todict(self):
    def convert_datetime(value):
        if value:
            return value.strftime("%Y-%m-%d %H:%M:%S")
        else:
            return value

    d = {}
    for c in self.__table__.columns:
        if isinstance(c.type, DateTime):
            value = convert_datetime(getattr(self, c.name))
        else:
            value = getattr(self, c.name)

        yield(c.name, value)

def iterfunc(self):
    """Returns an iterable that supports .next()
        so we can do dict(sa_instance)
 
    """
    return self.todict()

Base = declarative_base()
Base.todict = todict
Base.__iter__ = iterfunc



class Portfolio(Base):
    __tablename__ = 'portfolios'
    id = Column(Integer, primary_key = True)
    name = Column(String)    
    active = Column(Boolean, default=False)            
    running_from = Column(DateTime)
    running_to = Column(DateTime)
    
    
    def __init__(self, name, active):
        self.name = name           
        self.active = active         
        


class PortfolioStrategy(Base):
    __tablename__ = 'portfolio_strategies'    
    id = Column(Integer, primary_key = True)
    portfolio_id = Column(Integer)
    strategy_id = Column(Integer)
   

    def __init__(self, portfolio_id, strategy_id):
        self.portfolio_id = portfolio_id
        self.strategy_id = strategy_id


class PortfolioAccount(Base):
    __tablename__ = 'portfolio_accounts'
    id = Column(Integer, primary_key = True)
    portfolio_id = Column(Integer)
    account_id = Column(Integer)


    def __init__(self, portfolio_id, account_id):
        self.portfolio_id = portfolio_id
        self.account_id = account_id        

    
class Strategy(Base):
    __tablename__ = 'strategies'    
    id = Column(Integer, primary_key = True, index = True)
    name = Column(String)
    dns = Column(PickleType)
    evol_name = Column(String)
    evolrun = Column(Integer)
    timeframe = Column(Integer)
    symbol = Column(String)
    mm_config = Column(PickleType)
    element_num = Column(Integer)
    
    backtest_from = Column(DateTime)
    backtest_to = Column(DateTime)
    backtest_perf = Column(PickleType)
    backtest_serial = Column(PickleType)
    backtest_tradelog = Column(PickleType)

    eval_from = Column(DateTime)
    eval_to = Column(DateTime)                
    eval_perf = Column(PickleType)
    eval_serial = Column(PickleType)
    eval_tradelog = Column(PickleType)
    eval_net_profit = Column(Float)
    eval_percent_profitable = Column(Float)
    eval_max_dd = Column(Float)
    
    live_to = Column(DateTime)    
    live_perf = Column(PickleType)
    live_serial = Column(PickleType)
    live_tradelog = Column(PickleType)
    live_net_profit = Column(Float)
    live_percent_profitable = Column(Float)
    live_max_dd = Column(Float)
    
    new = Column(Boolean, default=True)
    live = Column(Boolean, default=False)
    creation_date = Column(DateTime)
    live_date = Column(DateTime)
    
    
    def __init__(self, name, dns, timeframe, symbol, mm_config):
        self.name = name
        self.dns = dns
        self.timeframe = timeframe
        self.symbol = symbol
        self.mm_config = mm_config

        

class Account(Base):
    __tablename__ = 'account'

    id = Column(Integer, primary_key = True)
    name = Column(String)
    email = Column(String)
    plugin_id = Column(Integer)
    plugin_settings = Column(PickleType) 
    base_equity = Column(Float)   #GUI            
    unit_size = Column(Integer)
    sizing_mode = Column(String)    
    risk_multiplier = Column(Float)
    sl = Column(Integer)
                     
#     account_number = Column(String)   #GUIRA!, nem kotelezo
#     username = Column(String)   # nem kotelezo
#     password = Column(String)       
#     sendercompid = Column(String)
#     targetcompid = Column(String)
#     server_url = Column(String)   #GUI
#     server_port = Column(Integer)   #GUI
#     use_ssl = Column(Boolean)   #GUI
                         
    
    def __init__(self, plugin_id, settings, name, email, unit_size, sizing_mode, risk_multiplier, sl):
        self.user_id = 0        
        self.plugin_id = plugin_id
        self.plugin_settings = settings
        self.name = name
        self.email = email        
        self.unit_size = unit_size
        self.sizing_mode = sizing_mode                        
        self.risk_multiplier = risk_multiplier        
        self.sl = sl


class AccountEquity(Base):
    
    __tablename__ = 'account_equity'
    
    id = Column(Integer, primary_key = True, index=True)
    account_id = Column(Integer)
    time = Column(DateTime)
    equity = Column(Float)
    usable_margin = Column(Float)
    
    
    def __init__(self, account_id, time, equity, usable_margin):
        self.account_id = account_id
        self.time = time
        self.equity = equity
        self.usable_margin = usable_margin
    


class AccountExposure(Base):
    
    __tablename__ = 'account_exposure'
    id = Column(Integer, primary_key = True, index=True)
    account_id = Column(Integer)
    time = Column(DateTime)
    symbol = Column(String)
    amount = Column(Integer)
    price = Column(Float)
    
    def __init__(self, account_id, time, symbol, amount, price):
        self.account_id = account_id
        self.time = time
        self.symbol = symbol
        self.amount = amount
        self.price = price
        

class AccountOrder(Base):
    __tablename__ = 'account_orders'
    id = Column(Integer, primary_key = True, index=True)
    account_id = Column(Integer)
    time = Column(DateTime)
    type = Column(String)
    status = Column(String)
    clordid = Column(String)
    symbol = Column(String)
    side = Column(String)
    amount = Column(Integer)
    price = Column(Float)
    
    
    def __init__(self, account_id, time, type, status, clordid, symbol, side, amount, price):
        self.account_id = account_id
        self.time = time
        self.type = type
        self.status = status
        self.clordid = clordid
        self.symbol = symbol
        self.side = side
        self.amount = int(amount)
        self.price = price
        
    


class DatarowDescriptor(Base):
    '''
    Historikus adatsorok jellemzőit tároló perzisztens osztály
    @author: Elekes Árpád
    '''
    __tablename__ = 'datarowDescriptors'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True, index = True)
    '''
    @ivar: Egyedi azonosító
    @type: int
    '''
    pluginId = Column(Integer)
    '''
    @ivar: referencia a historikus plugin osztály beépitett id-jére 
    @type: int
    '''    
    configId = Column(Integer)
    '''
    @ivar: Referencia a historicPluginsConfigs.id mezőre
    @type: int
    '''    
    configRowId = Column(Integer)
    '''
    @ivar: Referencia a historicPluginsConfigsInstrument.id mezőre
    @type: int
    '''
    symbol = Column(String, index = True)
    '''
    @ivar: Instrumentum név
    @type: string
    '''
    timeframe_num = Column(Integer, index = True)
    '''
    @ivar: Időtáv azonosító percekben kifejezve (1 = M1, 5=M5, ....)
    @type: int
    '''
    download_date = Column(DateTime)
    '''
    @ivar: Letöltés időpontja
    @type: datetime
    '''
    from_date = Column(DateTime)
    '''
    @ivar: Legkorábbi letöltött árfolyamadat időpontja 
    @type: datetime
    '''
    to_date = Column(DateTime)
    '''
    @ivar: Legutolsó letöltött árfolyamadat időpontja
    @type: datetime
    '''
    price_type = Column(Integer)   #  
    '''
    @ivar: Az adatsor által tartalmazott ár típusa (0:ask, 1:bid, 2:middle) 
    @type: int
    '''
    status = Column(String)
    '''
    @ivar: A legutolsó letöltési művelet állapota 
    @type: str
    '''
    status_text = Column(Text)
    '''
    @ivar: A legutolsó letöltési művelet naplója 
    @type: string
    '''
    bar_count = Column(Integer)
    '''
    @ivar: Az összes, a descriptorhoz tartozó gyertya száma 
    @type: int 
    '''
    invalid_count = Column(Integer)
    '''
    @ivar: A hibás gyertyák száma 
    @type: int
    '''
    def __init__(self, symbol, timeframe_num, from_date, to_date):
        '''
        Konstruktor
        '''        
        self.symbol = symbol
        self.timeframe_num = timeframe_num        
        self.from_date = from_date
        self.to_date = to_date
        self.price_type = PRICE_BIDASK
        self.status = ''
        self.status_text = ''
        


class DatarowData(Base):
    '''
    Historikus adatsorok árfolyamadatait tároló perzisztens osztály
    @author: Elekes Árpád
    '''
    __tablename__ = 'DatarowData'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True)
    '''
    @ivar: Egyedi azonosító
    @type: int
    '''
    datarowId = Column(Integer)
    '''
    @ivar: Referencia a DatarowDescriptor.id mezőre
    @type: int
    '''
    ask_open = Column(Float)
    '''
    @ivar: ASK gyertya nyitó ára
    @type: float
    '''
    ask_high = Column(Float)
    '''
    @ivar: ASK gyertya legmagasabb árponja
    @type: float
    '''
    ask_low = Column(Float)
    '''
    @ivar: ASK gyertya legalacsonyabb árpontja
    @type: float
    '''
    ask_close = Column(Float)
    '''
    @ivar: ASK gyertya záró ára
    @type: float
    '''
    bid_open = Column(Float)
    '''
    @ivar: BID gyertya nyitó ára
    @type: float
    '''
    bid_high = Column(Float)
    '''
    @ivar: BID gyertya legmagasabb árponja
    @type: float
    '''
    bid_low = Column(Float)
    '''
    @ivar: BID gyertya legalacsonyabb árpontja
    @type: float
    '''
    bid_close = Column(Float)
    '''
    @ivar: BID gyertya záró ára
    @type: float
    '''
    volume = Column(Integer)
    '''
    @ivar: A gyertya ideje alatt lévő forgalom
    @type: int
    '''
    time = Column(DateTime)
    '''
    @ivar: A gyertya nyitásának időpontja
    @type: datetime
    '''
    invalid = Column(Boolean)
    '''
    @ivar: A gyertyában tárolt adatok helyesek-e?
    @type: boolean
    '''

    def __init__(self, datarowId, open, high, low, close, time, volume, invalid = False):
        '''
        Konstruktor
        '''
        self.datarowId = datarowId
        self.ask_open = open
        self.ask_high = high
        self.ask_low = low
        self.ask_close = close
        self.bid_open = 0.0
        self.bid_high = 0.0
        self.bid_low = 0.0
        self.bid_close = 0.0
        self.volume = volume
        self.time = time
        self.invalid = invalid

    def __repr__(self):
        return str(self.time) + " (%.4f, %.4f, %.4f, %.4f) (%.4f, %.4f, %.4f, %.4f) %d" % \
                    (self.ask_open, self.ask_high, self.ask_low, self.ask_close,
                     self.bid_open, self.bid_high, self.bid_low, self.bid_close,
                     self.volume)

    def as_tohlc(self):
        # return {'time': self.time.strftime('%Y-%m-%d %H:%M:%S'), 'open': self.open, 'high': round(self.high,4), 'low': round(self.low,4), 'close': round(self.close,4)}
        return [self.time.strftime('%Y-%m-%d %H:%M:%S'), self.ask_open, self.ask_high, self.ask_low, self.ask_close]


    
class Trade(Base):
    
    __tablename__ = 'trades'    
    id = Column(Integer, primary_key = True)
    portfolio_id = Column(Integer)    
    strategy_id = Column(Integer)                
    state = Column(Integer)
    symbol = Column(String)    
    direction = Column(Integer)
    open_time = Column(DateTime)    
    open_price = Column(Float)    
    amount = Column(Integer)    
    sl_price = Column(Float)
    tp_price = Column(Float)    
    close_price = Column(Float)
    close_type = Column(Integer)
    close_time = Column(DateTime)
    
    floating_profit = Column(Float)    
    profit = Column(Float)                
    MAE = Column(Float)
    MFE = Column(Float)
    
    Index('idx_statedir', 'state', 'direction')

    def __init__(self, portfolio_id, strategy_id, state, symbol, direction, open_time, open_price, amount, sl_price):
        self.portfolio_id = portfolio_id
        self.strategy_id = strategy_id
        self.state = state
        self.symbol = symbol
        self.direction = direction
        self.open_time = open_time
        self.open_price = open_price
        self.amount = amount
        self.sl_price = sl_price
    
    