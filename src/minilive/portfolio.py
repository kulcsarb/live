'''
Created on Jun 17, 2014

@author: gdt
'''
from gdtlive.constants import DIR_BUY, DIR_SELL
from gdtlive.core.constants import TRADE_OPENED
from helpers import calc_sl
from strategy import Strategy
from accounts import Account
import minilive.db as db 
import logging
import time
import traceback
import os


log = logging.getLogger('live')



class Portfolio:
    
    def __init__(self, portfolio):                        
        self.id = portfolio.id
        self.name = portfolio.name        
        self.symbol = 'EURUSD'
        self.strategies = []
        self.accounts = []
        self.load()
        
    
    def load(self):        
        log.info('loading portfolio: %s' % self.name)
        
        session = db.Session()
        
        log.info('loading strategies... ')
        q = session.query(db.Strategy) \
            .filter(db.Strategy.id == db.PortfolioStrategy.strategy_id) \
            .filter(db.PortfolioStrategy.portfolio_id == self.id)
        
        for strategy in q:
            self.strategies.append( Strategy(self.id, strategy.id, strategy.name, strategy.dns, strategy.symbol, strategy.timeframe) )
        
#         log.info('loading accounts')
#         q = session.query(db.Account).filter(db.PortfolioAccount.account_id == db.Account.id).filter(db.PortfolioAccount.portfolio_id == self.id)
#         for account in q:
#             self.accounts.append( Account(account, self.id) )
        
        session.close()
        
    
    def prepare(self):
        log.info('Portfolio %s - prepare' % self.name)
        for strategy in self.strategies:
            strategy.evaluate()
    
    
    def evaluate(self):
        log.info('Portfolio %s - evaluate' % self.name)        
        self.votes = 0
                        
        for strategy in self.strategies:            
            self.votes += strategy.evaluate()
                    
        log.info(' final vote: %d' % self.votes)
    
        self.write_signal()
        
#         for account in self.accounts:
#             try:
#                 account.transfer_vote(self.symbol, self.votes, len(self.strategies))
#             except:
#                 log.error(traceback.format_exc())

    
    
    def write_signal(self):
        path = os.path.expanduser('~/live/portfolio/%d' % self.id)
        try:
            os.makedirs(path)
        except:
            pass
        
        with open(path+'/signal', 'w+') as f:
            f.write('%s\t%d\t%d' % (self.symbol, self.votes, len(self.strategies)))
        
    
    
    def get_trade(self):                    
        import minilive.datafeed.historic as historic        
        if not self.votes:
            return 
                        
        sl = calc_sl(self.symbol, self.votes, 120)
        dir = DIR_BUY if self.votes > 0 else DIR_SELL
        price = 'ASK' if self.votes > 0 else 'BID'
        amount = round(abs(10000 * ( float(self.votes) / len(self.strategies) )), -2)
        
        trade = {
                 'strategy_id' : 0,
                 'portfolio_id' : self.id,
                 'state' : TRADE_OPENED,
                 'symbol' : self.symbol,
                 'direction' : dir,
                 'open_time' : historic.CURRENT_TIME,
                 'open_price' : historic.CURRENT_PRICE[self.symbol][price],
                 'amount' : amount,
                 'sl_price' : sl                 
                 }
        return trade

        
                                                    
        
    
            