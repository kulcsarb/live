'''
Created on Jun 17, 2014

@author: gdt
'''

import gdtlive.core.fix.dukascopy.fix as fix
from gdtlive.core.fix.initiator import FIXInitiator
from logging.handlers import RotatingFileHandler 
from fix.server import FixAccountBase
import logging
import traceback
import time
import socket
from datetime import datetime, timedelta
import ssl
import pprint 
import re
import multiprocessing as mp

log = logging.getLogger('live')


class SimpleFixAccount(FixAccountBase):

    def __init__(self, settings):    
        FixAccountBase.__init__(self, settings)        
        self.show_message = False
        self.exposure = {}
        self.mode = 'ADJUST'
        self.symbol = None
        self.amount = 0
        self.sl = 0
        self.stops = {}
                                                    

    def process_response(self):
        self.stops = {}
        for message in self.receive():
            if self.show_message:                
                log.debug('  %s' % message)
            if type(message) == fix.InstrumentPositionInfo:                
                self.exposure[message.Symbol] = message.Amount
            if type(message) == fix.ExecutionReport:                                
                if message.OrdStatus == fix.OrdStatus.PENDING_NEW and message.OrdType == fix.OrdType.STOP:                                        
                    self.stops[message.Symbol] = message
            if type(message) == fix.Notification:
                log.info(" " + message.Text)
                    
    
    def close_positions(self):
        if not self.exposure:
            return 
        log.info('CLOSING POSITIONS')
        for symbol in self.exposure:
            if self.exposure[symbol]:                        
                self.market_order(symbol, -1 * self.exposure[symbol])
                

    def open_positions(self):        
        if self.amount:
            log.info('OPENING POSITION')                        
            self.market_order(self.symbol, self.amount)
            if self.sl:
                log.info('PLACING SL')
                self.stop_order(self.symbol, self.amount, self.sl)


    def cancel_all_stop(self):
        if not self.stops:
            return         
        log.info('CANCELLING STOP ORDERS')
        while True:                                                    
            for message in self.stops.values():                                        
                self.cancel_stop(message.ClOrdID, message.OrderID, message.Side, message.AvgPx)        
            
            self.status_request()
            self.process_response()
            
            if not self.stops:
                log.info('All SL cancelled')
                break
            log.info('cancelling remaining SLs')
                
                        
    def adjust_positions(self):
        log.info('adjusting positions...')        
                    
        difference = int(self.amount - self.exposure.get(self.symbol,0))        
        if difference:
            log.info('adjusting current exposure with %d' % difference)
            self.market_order(self.symbol, difference)
        else:
            log.info('leaving current exposure as is')
                                                                    
        if self.sl:
            if self.symbol in self.stops:
                new_side = fix.Side.SELL if self.amount > 0 else fix.Side.BUY
                if self.stops[self.symbol].Side == new_side:           
                    if self.stops[self.symbol].AvgPx != self.sl or self.amount != self.stops[self.symbol].OrderQty:         
                        self.modify_stop(self.stops[self.symbol], self.amount, self.sl)
                    else:
                        log.info('leaving current sl as is')
                else:
                    self.cancel_stop(self.stops[self.symbol].ClOrdID, self.stops[self.symbol].OrderID, self.stops[self.symbol].Side, self.symbol)
                    self.stop_order(self.symbol, self.amount, self.sl)
            else:
                self.stop_order(self.symbol, self.amount, self.sl)
        else:
            if self.symbol in self.stops:
                self.cancel_stop(self.stops[self.symbol].ClOrdID, self.stops[self.symbol].OrderID, self.stops[self.symbol].Side, self.symbol)
                    
    
    def log_status(self):
        log.info('EXPOSURE:')
        for symbol, exp in self.exposure.items():
            log.info('  %s : %d' % (symbol, exp))
        if self.stops:
            log.info('STOPS: ')
            for message in self.stops.values():                                
                log.info('  %s  %s  %s  %.4f  %d' % (message.ClOrdID, message.Symbol, message.get('Side'), message.AvgPx, message.OrderQty))
                
    
    def transfer(self, symbol, amount, sl):
        try:            
            self.symbol = symbol if '/' in symbol else symbol[:3]+'/'+symbol[3:]
            self.amount = int(amount)
            self.sl = sl                                                              
            self.connect()
            self.login()
            self.status_request()                                
            self.process_response()
            
            self.log_status()                                                                
                                    
            if self.mode == 'ADJUST':                
                self.adjust_positions()                
            elif self.mode == 'CLOSE':                
                self.cancel_all_stop()
                self.process_response()                    
                self.close_positions() 
                self.process_response()               
                self.open_positions()                            
            
            self.status_request()            
            self.process_response()                                      
            self.logout()                   
        except Exception as e:            
            log.error(traceback.format_exc())    
            self.disconnect()     
            raise e   
        finally:
            self.disconnect()
            self.log_status()                                         
                    
    
    def transferred(self, symbol, amount, sl):
        if self.mode != 'ADJUST' and self.mode != 'CLOSE':
            return True
                                
        if self.exposure.get(symbol,0) != amount:  
            return False
         
        if sl:
            sl_dir = (fix.Side.BUY if amount < 0 else fix.Side.SELL)            
            if symbol not in self.stops or \
                self.stops[symbol].AvgPx != sl or \
                self.stops[symbol].OrderQty != abs(amount) or \
                self.stops[symbol].Side != sl_dir:                    
                return False 
                
        return True
        
    
    def safe_transfer(self, symbol, amount, sl, max_retries = 10):
        symbol = symbol if '/' in symbol else symbol[:3]+'/'+symbol[3:]
        amount = int(amount)
        sl = round(sl, 4)
        wait = 1
        retries = max_retries or 1
        while retries: 
            try:       
                self.transfer(symbol, amount, sl)
                if self.transferred(symbol, amount, sl):
                    log.info('--- TRANSFER OK ---') 
                    break            
            except:
                log.error('--- TRANSFER FAILED ---')
                log.error(traceback.format_exc())
                
            log.info('waiting for %d sec before retry' % wait)
            time.sleep(wait)
            wait += 1
            retries -= 1
        
    
    def set_exposure(self, symbol, amount, sl):    
        retries = 2            
        while retries:
            try:        
                p = mp.Process(target = self.safe_transfer, args=(symbol, amount, sl,), )
                p.start()
                p.join(60)
                if not p.is_alive():
                    log.info('Transfer process finished in time')
                    break        
                log.warning('Transfer process is still running, something went wrong')
                p.terminate()
                time.sleep(0.1)
            except:
                log.error(traceback.format_exc())
                time.sleep(0.1)                      
            retries -= 1                   
            
if __name__ == '__main__':
    log.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(name)s - %(message)s"))
    log.addHandler(handler)
    
    settings = {
            'URL': 'demo-api.dukascopy.com',
            'PORT' : 10443,
            'SENDERCOMPID': 'DEMO3NxKxd_DEMOFIX',
            'TARGETCOMPID': 'DUKASCOPYFIX',            
            'PASSWORD' : 'NxKxd',
            'USERNAME' : 'DEMO3NxKxd'            
            }
    
    server = SimpleFixAccount(settings)    
    server.set_exposure('EURUSD', 0, 0)
    
