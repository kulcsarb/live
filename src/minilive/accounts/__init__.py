import minilive.db as db 
from gdtlive.constants import PIP_MULTIPLIER
from fixaccount import SimpleFixAccount
from fileaccount import FileAccount
import logging
import traceback
import os
from minilive.helpers import calc_sl
from multiprocessing import Process

account_plugins = {
                   1 : SimpleFixAccount,                    
                   3 : FileAccount                 
                   }

log = logging.getLogger('live')


class Account(Process):
        
    def __init__(self, account, portfolio_id):
        global account_plugins
        Process.__init__(self)        
        log.info('  Account: %s' % account.name)        
        self.portfolio_id = portfolio_id
        self.name = account.name
        self.sizing_mode = account.sizing_mode
        self.unit_size = account.unit_size
        self.risk_multiplier = account.risk_multiplier
        self.sl = account.sl                
        self.plugin = account_plugins[account.plugin_id](account.plugin_settings)
        self.path = os.path.expanduser('~/live/portfolio/%d' % self.portfolio_id)      
        self.read_vote()
        
    
    def read_vote(self):
        try:
            with open(self.path + '/signal', 'r') as f:
                self.symbol, self.vote, self.strategy_num = f.readline().strip().split('\t')
                self.vote = int(self.vote)
                self.strategy_num = int(self.strategy_num)
        except:
            self.symbol = None
            self.vote = 0
            self.strategy_num = 0
            
    
    def init_logging(self):
        from logging.handlers import TimedRotatingFileHandler
         
        log.setLevel(logging.DEBUG)
        handler = logging.StreamHandler()
        handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(message)s"))
        log.addHandler(handler)
        try:
            os.makedirs(self.path+'/accounts')
        except:
            pass        
        handler = TimedRotatingFileHandler(self.path+'/accounts/%s.log' % self.name, when='D')        
        handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(message)s"))
        log.addHandler(handler)
        log.info('-------------- START ----------------')
        
    
    def calc_amount(self, vote, strategy_num):
        amount = 0                
        if self.sizing_mode == 'FIX':
            amount = vote * self.unit_size
        elif self.sizing_mode == 'PERCENT':
            amount = self.unit_size * (float(vote) / strategy_num)        
        elif self.sizing_mode == 'BALANCE_PERCENT':
            #FIXME: get balance ....            
            base = self.plugin.balance * self.risk_multiplier
            amount = base * (float(vote) / strategy_num)        
        
        if amount > 0:
            amount = max(round(amount, -2), 1000) 
        if amount < 0:
            amount = min(round(amount, -2), -1000)
        
        return amount
    
    
    def transfer_vote(self, symbol, vote, strategy_num):
        try:                                        
            sl = calc_sl(symbol, vote, self.sl)
            amount = self.calc_amount(vote, strategy_num)
            log.info('Transfering: %s  %d  %f' % (symbol, amount, sl))
            self.plugin.set_exposure(symbol, amount, sl)
        except:
            log.error(traceback.format_exc())
            
                    
    def run(self):
        self.init_logging()
        try:                                                
            sl = calc_sl(self.symbol, self.vote, self.sl)
            amount = self.calc_amount(self.vote, self.strategy_num)
            log.info('Transfering: %s  %d  %f' % (self.symbol, amount, sl))
            self.plugin.safe_transfer(self.symbol, amount, sl)
        except:
            log.error(traceback.format_exc())
        