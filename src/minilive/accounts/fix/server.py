'''
Created on Jun 20, 2014

@author: gdt
'''

import gdtlive.core.fix.dukascopy.fix as fix
from gdtlive.core.fix.initiator import FIXInitiator
import logging
import traceback
import socket
from datetime import datetime, timedelta
import ssl
import pprint 
import re
import time

log = logging.getLogger('live')



class FixAccountBase():

    def __init__(self, settings):        
        self.settings = settings
        self.MsgSeqNum = 0
        self.last_send_time = None
        self.heartbeat_interval = 0                
        self.show_raw_message = False
        self.show_message = True        
        
        
    def connect(self):
        log.info('connecting to %s:%d' % (self.settings['URL'], self.settings['PORT']))
        self.send_heartbeat = False        
        self.MsgSeqNum = 1
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket = ssl.wrap_socket(self.socket)
        self.socket.settimeout(1)                
        self.socket.connect((self.settings['URL'], self.settings['PORT']))
        
    
    def login(self):
        log.info('logging in')
        self.send(fix.Logon(Password=self.settings['PASSWORD'], Username=self.settings['USERNAME'], HeartBtInt=60, ResetSeqNumFlag=True, EncryptMethod=0))        
    
    def logout(self):
        log.info('logout')
        self.send(fix.Logout())
        
    def disconnect(self):
        try:
            self.socket.close()
        except:
            pass
          
          
    def send(self, message):        
        message.MsgSeqNum = self.MsgSeqNum
        message.SendingTime = datetime.utcnow()        
        message.SenderCompID = self.settings['SENDERCOMPID']
        message.TargetCompID = self.settings['TARGETCOMPID']
        if self.show_message:
            log.debug('OUT: %s' % (message))
        encoded = fix.encode(message)
        if self.show_raw_message:
            log.debug('OUT: %s' % ( pprint.pprint(encoded)))            
        self.socket.send(encoded)
        self.last_send_time = datetime.utcnow()
        self.MsgSeqNum += 1
    
    
    def receive(self):
        
        self.buffer = ''
        while True:
            try:
                data = self.socket.recv(4096)
                if not data:
                    log.error('Connection closed by Dukas')
                    break
                self.buffer += data
                
                for message in self.decode_messages():
                    yield message                                
            
            except socket.error:
                break


    def decode_messages(self):
        while True:        
            match = re.search(fix.RE_MESSAGE_PATTERN, self.buffer, re.M)
            if not match:                
                break
                                                            
            raw_message = match.group(0)                    
            self.buffer = self.buffer[len(raw_message):]
            try:
                if self.show_raw_message:
                    log.debug('IN: %s' % pprint.pprint(raw_message))                
                yield fix.decode(raw_message)
            except GeneratorExit:
                pass


    def market_order(self, symbol, amount):
                      
        self.send(fix.NewOrderSingle(ClOrdID='GDT_MO_%d' % time.time(), 
                                                Currency=symbol[:3],
                                                Symbol = symbol, 
                                                Side=fix.Side.BUY if amount > 0 else fix.Side.SELL, 
                                                OrderQty=int(abs(amount)),                                        
                                                TransactTime = datetime.utcnow(),
                                                OrdType = fix.OrdType.MARKET,
                                                TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL,
                                                Slippage = 20
                                                ))
        
    
    def stop_order(self, symbol, amount, price):
        log.info('new stop order')
        self.send(fix.NewOrderSingle(ClOrdID='GDT_SL_%d' % time.time() , 
                                            Currency=symbol[:3],
                                            Symbol=symbol, 
                                            Side=fix.Side.SELL if amount > 0 else fix.Side.BUY, 
                                            OrderQty= int(abs(amount)),
                                            Price = round(price, 4),
                                            TransactTime = datetime.utcnow(),
                                            OrdType = fix.OrdType.STOP,
                                            TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL,
                                            Slippage = 10,
                                            ))
        
    
    def cancel_stop(self, orig_clordid, order_id, side, symbol):        
        log.info('cancel stop order')
        self.send(fix.OrderCancelRequest(ClOrdID='GDT_CANCEL_%d' % time.time(),
                                 OrigClOrdID=orig_clordid,
                                 OrderID = order_id,
                                 Side = side,
                                 TransactTime = datetime.utcnow(),    
                                 Symbol = symbol))
        
        
    def modify_stop(self, message, amount, sl_price):
        log.info('modify stop order')
        self.send(fix.OrderCancelReplaceRequest(ClOrdID='GDT_SL_%d' % time.time(),
                                 OrigClOrdID = message.ClOrdID,
                                 OrderID = message.OrderID,
                                 OrdType = fix.OrdType.STOP,
                                 OrderQty = int(abs(amount)),
                                 Price = round(sl_price, 4),                                                                  
                                 Side = fix.Side.SELL if amount > 0 else fix.Side.BUY,
                                 Symbol = message.Symbol,
                                 TransactTime = datetime.utcnow(),                                     
                                 Slippage = 10))                                                     

    def status_request(self):
        self.send(fix.OrderMassStatusRequest(MassStatusReqType = fix.MassStatusReqType.STATUS_FOR_ALL_ORDERS, MassStatusReqID='gdt'))

    def account_info(self):
        self.send(fix.AccountInfoRequest())
        
        