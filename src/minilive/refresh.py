'''
Created on Jun 22, 2014

@author: gdt
'''
import datarow
import minilive.db as db
from helpers import init_logging

def main():
    init_logging()
    db.init()
    session = db.Session()
    for descriptor in session.query(db.DatarowDescriptor):
        datarow.refresh(descriptor.symbol, descriptor.timeframe_num)

    session.close()


if __name__ == '__main__':
    main()    