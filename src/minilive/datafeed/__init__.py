# -*- encoding: utf-8 -*- 
'''
Created on Jun 25, 2012

@author: gdtlive
'''
from gdtlive.constants import TIMEFRAME, PIP_MULTIPLIER, PRICE_PIPVALUE, PRICE_TIME, PRICE_ASKOPEN, PRICE_ASKHIGH, PRICE_ASKLOW, PRICE_ASKCLOSE, PRICE_BIDOPEN, PRICE_BIDHIGH, PRICE_BIDLOW, PRICE_BIDCLOSE, PRICE_VOLUME
from gdtlive.core.constants import *
import gdtlive.config as config
import minilive.db as db
import historic
import gdtlive.historic.datarow as h_datarow
from jforex import JForexLoader
from datetime import datetime, timedelta
from threading import Thread, Lock
import threading
from Queue import Queue, Empty
import time
import calendar
import random
import traceback
import copy
import logging
import subprocess
import StringIO
import numpy as np
import ast 
import os
import cPickle
from datetime import date



log = logging.getLogger('live')
        

class DatarowBuilder:
    
    def __init__(self):                        
        historic.CURRENT_PRICE = {}
        historic.DATAROW = {}
        self.current = {}                                                
        self.strategy_counter = {}        
        self.disconnect_time = None
        self.datarows_cleaned = False
        self.stop = False                
            
    
    def register(self, symbol, timeframe):                                                
        self.current.setdefault(timeframe, {})                            
        historic.DATAROW.setdefault(timeframe, {})        
        historic.DATAROWS.setdefault(timeframe, {})
        self.strategy_counter.setdefault(timeframe, {})
                                
        historic.CURRENT_PRICE.setdefault(symbol, {'ASK':0, 'BID':0, 'PIPVALUE':0.0001})
                               
        self.strategy_counter[timeframe].setdefault(symbol, 0)
        historic.DATAROW[timeframe].setdefault(symbol, {})
        historic.DATAROWS[timeframe].setdefault(symbol, {})
        
        if symbol not in self.current[timeframe]:
            log.info('Registering instrument %s_%s' % (symbol, TIMEFRAME[timeframe]))                            
            self.current[timeframe][symbol] = {}
            for price in xrange(PRICE_TIME, PRICE_PIPVALUE+1):                    
                self.current[timeframe][symbol][price] = 0
                historic.DATAROW[timeframe][symbol][price] = []                                    
                if price == PRICE_TIME:
                    historic.DATAROWS[timeframe][symbol][price] = []
                else:
                    historic.DATAROWS[timeframe][symbol][price] = np.array(historic.DATAROW[timeframe][symbol][price], dtype=np.double)
                    
            self.current[timeframe][symbol][PRICE_TIME] = None                              
        
        else:
            self.strategy_counter[timeframe][symbol] += 1
                                                                                                            
    
    def preload(self, symbol, timeframe):
        import minilive.datarow as datarow
        datarow.refresh(symbol, timeframe)        
        historic.DATAROW[timeframe][symbol] = datarow.load2(symbol, timeframe, datetime(2014, 1, 1))
        self.export_datarow(symbol, timeframe)                
        
        log.info('loaded candles:  %s -> %s' % (historic.DATAROW[timeframe][symbol][PRICE_TIME][0], historic.DATAROW[timeframe][symbol][PRICE_TIME][-1]))
    


    def export_datarow(self, symbol, timeframe):        
        for price in historic.DATAROW[timeframe][symbol]:            
            if price == PRICE_TIME:                                                                
                historic.DATAROWS[timeframe][symbol][price] = historic.DATAROW[timeframe][symbol][price]
            else:
                historic.DATAROWS[timeframe][symbol][price] = np.array(historic.DATAROW[timeframe][symbol][price], dtype=np.double)
        
        historic.CURRENT_PRICE[symbol]['ASK'] = historic.DATAROW[timeframe][symbol][PRICE_ASKCLOSE][-1]
        historic.CURRENT_PRICE[symbol]['BID'] = historic.DATAROW[timeframe][symbol][PRICE_BIDCLOSE][-1]
        historic.CURRENT_TIME = datetime.utcnow()     
        
        

    def calc_pipvalue(self, timeframe):
        '''Kiszámolja az összes betöltött adatsor pip értékét'''        
        log.info('Calculating pipvalues')
        try:
            for symbol in historic.DATAROW[timeframe]:
                self._calc_pipvalue(timeframe, symbol)
        except:
            log.error('Failed to compute pipvalue for historical data')
            log.error(traceback.format_exc())
            
    def _calc_pipvalue(self, timeframe, symbol):
        
        if PRICE_PIPVALUE not in historic.DATAROW[timeframe][symbol]:
            historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE] = []
            
            if symbol[3:] == 'USD':
                self._pipvalue_calc_case1(timeframe, symbol)
            elif symbol[:3] == 'USD':
                self._pipvalue_calc_case2(timeframe, symbol)
            else:
                self._pipvalue_calc_case3(timeframe, symbol)
        
        
    def _pipvalue_calc_case1(self, timeframe, symbol):
        
        
        '''Kiszámolja az USD-re végződő instrumentumok pip értékét minden gyertyára nézve.
        
        Az eredményt a `candles`[symbol][PRICE_PIPVALUE] tömbben tárolja
                
        
        :Parameters:
            symbol : str
                Az instrumentum azonosítója, ahol instrumentum = "*USD"  (AUDUSD, EURUSD, GBPUSD, ....)
        
        Algoritmus 
        ==========
        
        Determine your account’s currency (in about 90% cases it’s USD). If it’s the same currency 
        as the based currency of the pair (the second one, which goes after “/”) then you should 
        simply multiply the amount of currency units in your position (100,000 for 1 standard lot) 
        by the size of one pip. You’ll get a pip value of 10 currency units per 1 standard lot. 
        
        '''
        datarow_length = len(historic.DATAROW[timeframe][symbol][PRICE_TIME])
        historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE] = [PIP_MULTIPLIER[symbol] for i in range(datarow_length)]        



    def _pipvalue_calc_case2(self, timeframe, symbol):
        
        '''Kiszámolja az USD-vel kezdődő instrumentumok pip értékét minden gyertyára nézve.
        
        Az eredményt a `candles`[symbol][PRICE_PIPVALUE] tömbben tárolja
                
        
        :Parameters:
            symbol : str
                Az instrumentum azonosítója, ahol instrumentum = "USD*"  (USDCAD, USDZAR, ....)
        
        Algoritmus 
        ==========
        
        If the account’s currency is different from the base currency but is the same as the currency 
        pair’s long currency (the first one, which goes before “/”) then check this currency pair’s 
        current Ask rate (the highest of the rates).
        You should multiply the amount of currency units in your position (100,000 for 1 standard lot) 
        by the size of one pip and then divide the result by the Ask rate 
        '''        
        datarow_length = len(historic.DATAROW[timeframe][symbol][PRICE_TIME])
        historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE] = [0] * datarow_length        
        for i in xrange(len(historic.DATAROW[timeframe][symbol][PRICE_TIME])):
            value = 1 * PIP_MULTIPLIER[symbol] / historic.DATAROW[timeframe][symbol][PRICE_ASKCLOSE][i]
            historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE][i] = value


    def _pipvalue_calc_case3(self, timeframe, symbol):
        
        '''Kiszámolja az adott keresztárfolyam pip értékét minden gyertyára nézve.

        Az eredményt a `candles`[symbol][PRICE_PIPVALUE] tömbben tárolja

        :Parameters:
            symbol : str
                Az instrumentum azonosítója, ahol az instrumentumban nem szerepel az USD  (CADJPY, AUDNZD, ....)

        Algoritmus
        ==========

        If the account’s currency is different from any of the currencies from the pair of the position,
        you have to check the current rate of this currency relative to the base currency of the pair:
            3.A:
                If the currency pair combined of the account’s currency and the base currency of the position
                has the account’s currency as the base currency (second, after “/”) then you should check
                and remember its current Bid rate (the lowest of the rates).
            3.B
                If the currency pair combined of the account’s currency and the base currency of the position
                has the account’s currency as the long currency (first, before “/”) then you should check
                and remember its current Ask rate (the highest of the rates).

        You should multiply the amount of currency units in your position (100,000 for 1 standard lot) by
        the size of one pip and either multiply the result by the Bid rate received in step 3a or divide
        the result by the Ask rate received in step 3b.

        :TODO: unittest
        '''
        
        symbol_3B = 'USD' + symbol[:3]   # CASE 3.B : account currency as long 
        symbol_3A = symbol[:3] + 'USD'    # CASE 3.A : account currency as short    
        case = ''
        if symbol_3B in historic.DATAROW[timeframe]:
            case = '3B'            
        elif symbol_3A in historic.DATAROW[timeframe]: 
            case = '3A'            

        datarow_length = len(historic.DATAROW[timeframe][symbol][PRICE_TIME])
        historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE] = [0] * datarow_length        
        
        if case == '3A':
            for i in xrange(len(historic.DATAROW[timeframe][symbol][PRICE_TIME])):                                      
                historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE][i] = PIP_MULTIPLIER[symbol_3A] * historic.DATAROW[timeframe][symbol_3A][PRICE_ASKCLOSE][i]

        elif case == '3B':
            for i in xrange(len(historic.DATAROW[timeframe][symbol][PRICE_TIME])):                                 
                historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE][i] = PIP_MULTIPLIER[symbol_3B] / historic.DATAROW[timeframe][symbol_3B][PRICE_ASKCLOSE][i]

        else:
            # ha nincs meg az árfolyam, 1-nek vesszük.... 
            for i in xrange(len(historic.DATAROW[timeframe][symbol][PRICE_TIME])):
                value = 0.0001
                historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE][i] = value                


    
    
    def load_jforex(self, symbol, timeframe, from_date, to_date):  
        # print 'length before', len(historic.DATAROWS[timeframe][symbol][PRICE_TIME])      
        loader = JForexLoader(symbol, timeframe, from_date, to_date)        
        result = loader.run()        
        loader.process_results()
        self.export_datarow(symbol, timeframe)
        import minilive.datarow as datarow
        candles = result[symbol[:3]+'/'+symbol[3:]]
        datarow.save_candles(candles, symbol, timeframe)
        
        # self.calc_pipvalue(timeframe)
        return result                                       

                                                                                                
    def get_pipvalue(self, symbol):
        pipvalue = 0.0001
        try:
            if symbol.endswith('USD'):
                pipvalue = PIP_MULTIPLIER[symbol]
            elif symbol.startswith('USD'):
                pipvalue = PIP_MULTIPLIER[symbol] / historic.CURRENT_PRICE[symbol]['ASK']
            else:
                symbol_3B = 'USD' + symbol[:3]   # CASE 3.B : account currency as long 
                symbol_3A = symbol[:3] + 'USD'    # CASE 3.A : account currency as short                
                if symbol_3A in historic.CURRENT_PRICE:
                    pipvalue = PIP_MULTIPLIER[symbol_3A] * historic.CURRENT_PRICE[symbol_3A]['ASK'] 
                elif symbol_3B in historic.CURRENT_PRICE:
                    pipvalue = PIP_MULTIPLIER[symbol_3B] /  historic.CURRENT_PRICE[symbol_3B]['ASK']
                else:
                    pipvalue = 0.0001
        except:
            pass
            
        return pipvalue

    
            
    
    def new_candle(self, current_time):                                       
        new_candle_added = False
        
        # DO NOT REMOVE IT, OR I WILL KILL YOU, MOTHERFUCKER!
        current_time = current_time.replace(second=0, microsecond=0)
        
        historic.CURRENT_TIME = current_time                                           
                            
        for timeframe in sorted(self.current.keys()):                
            gapfill_symbols = []                
            gapfill_from = None
            gapfill_to = None
            for symbol in sorted(self.current[timeframe]):
                
                if calendar.timegm(current_time.utctimetuple()) % (timeframe*60) == 0 :
                    
                    if len(historic.DATAROW[timeframe][symbol][PRICE_TIME]):
                        sym_from = historic.DATAROW[timeframe][symbol][PRICE_TIME][-1] + timedelta(minutes = timeframe)
                        sym_to = current_time - timedelta(minutes = timeframe)
                        if (not gapfill_from or gapfill_from > sym_from):
                            gapfill_from = sym_from
                        if (not gapfill_to or gapfill_to < sym_to):                            
                            gapfill_to = sym_to if sym_to >= sym_from else sym_from
                                                                                                                    
                        gapfill_symbols.append(symbol)

                                                                                                                                              
            if gapfill_symbols:
                
                log.info('starting jforex loader')
                result = self.load_jforex(gapfill_symbols[0], timeframe, gapfill_from, gapfill_to)
                if not result:
                    log.info('loader failed!')                    
                else:
                    log.info('loader finished: %s ' % result)                    
                    new_candle_added = True    
            
        return new_candle_added                                                   
                                            
        


class DatafeedServer:
        
    def __init__(self, timeframe):        
        log.info('DatafeedServer initializing')                                    
        self.datarows = DatarowBuilder()    
        self.current_day = None        
        self.delta = 30
        self.timeframe = timeframe
                          
    
    def time_for_action(self, candle_time):        
        return calendar.timegm(candle_time.utctimetuple()) % (self.timeframe * 60) == 0
    
    
    def load_next_candle(self):    
        log.info('waiting for the new candle')                                
        while True:
            time.sleep(1)    # 0.01-nel a CPU usage 1-2%, 0.001: 4-5%, printekkel , print nelkul 2% !            
            current = datetime.utcnow()
            
            next = current + timedelta(seconds=self.delta)                          
            next_candle_time = datetime(next.year, next.month, next.day, next.hour, next.minute, 0, 0, tzinfo=None)
                        
            if self.time_for_action(next_candle_time):                               
                return self.datarows.new_candle(next_candle_time)            
                                                                                                                                            
        return False    
                        
                                                              
    def register_instrument(self, symbol, timeframe):                
        self.datarows.register(symbol, timeframe)                                                                                                                                               
        return True

            
    def preload(self, timeframe=None):                        
        for timeframe in historic.DATAROW.keys():
            self.preload_tf(timeframe)
            
                            
    def preload_tf(self, timeframe):
        symbols = historic.DATAROW[timeframe].keys()        
    
        for symbol in symbols:
            self.datarows.preload(symbol, timeframe)
            self.datarows.export_datarow(symbol, timeframe)
    
        #if symbols:        
        #    self.datarows.calc_pipvalue(timeframe)




