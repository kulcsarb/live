# -*- encoding: utf-8 -*- 
'''
Created on 2012.05.27.

@author: kulcsarb
'''

DATAROW = {}
DATAROWS = {}
CURRENT_PRICE = {}
CURRENT_TIME = None
CURRENT_CANDLE_TIME = None
server = None


def init(timeframe=1440):
    from minilive.datafeed import DatafeedServer
    global server
    
    server = DatafeedServer(timeframe)
    return server
    