'''
Created on Dec 3, 2012

@author: gdt
'''

from gdtlive.constants import *
import time
import traceback
import logging
import os
import ast
import calendar
import subprocess
import gdtlive.config as config
import numpy as np
import minilive.db as db
from datetime import datetime, timedelta 


log = logging.getLogger('live')

class JForexLoader:
    
    def __init__(self, symbols, timeframe, from_date, to_date):
        
        if type(symbols) == list:        
            self.symbols = []
            for symbol in symbols:            
                self.symbols.append(symbol[:3] + '/' + symbol[3:])
            self.symbols = ' '.join(self.symbols)
        else:
            self.symbols = symbols[:3] + '/' + symbols[3:]
        
        self.loaded_data = None
        self.candles = {}
        self.timeframe = timeframe
        self.from_date = from_date
        self.to_date = to_date
        self.output = ""        
        
        self.max_restart_count = 50
        self.restart_count = 0        
        self.start_timeout = 30
        self.wait_timeout = 60
        self.elapsed = 0
        
                                                                        
    def run(self):
        log.info('loading candles for : %s %s-%s %s' % (TIMEFRAME[self.timeframe], self.from_date, self.to_date, self.symbols))
        try:
            self.retcode = None
            self.start_loader()            
            while True:            
                self.retcode = self.process.poll()
                if self.retcode is not None:
                    self.tempfile.seek(0, os.SEEK_SET)
                    self.output = self.tempfile.read()
                    self.tempfile.close()                
                    if self.retcode == 0 and self.output_is_valid():                    
                        log.info('preload.jar finished with exit status 0')
                        break
                    else:
                        log.warning('preload.jar has issues (retcode: %d)' % self.retcode)
                        log.warning(self.output)
                        if not self.restart_loader():
                            return False                
                    
                time.sleep(1)
                self.elapsed += 1
                #log.info('checking preloader... %d' % elapsed)
                self.check_if_started()
                            
                if self.elapsed >= self.start_timeout and not self.started:                                                    
                    if not self.restart_loader():
                        return False
                if self.elapsed >= self.wait_timeout and self.started:                    
                    if not self.restart_loader():
                        return False
    
    
            if self.process.returncode == 0:
                log.info('load finished')            
                log.debug(self.output)            
                return self.loaded_data
            else:            
                log.error('load failed!')            
                log.error(self.output)
                return False
        except:
            log.error(traceback.format_exc())
            
            
    def output_is_valid(self):
        try:
            lines = self.output.split('\n')
            if not lines:
                log.error('Wrong output: %s' % self.output)
                return False
            if '{' not in self.output or '}' not in self.output:
                log.error('Wrong output: %s' % self.output)
                return False        
        
            while not lines[0].startswith('{'):                 
                del lines[0]
            while not lines[-1].startswith('}'):                 
                del lines[-1]
                            
            self.loaded_data = ast.literal_eval(''.join(lines))
            
        except:
            log.error(traceback.format_exc())
            return False
                
        return True    
        
           
    def process_results(self):
        import minilive.datafeed.historic as historic                                                                      
        try:
            if not self.loaded_data:
                log.warning('there is no loaded price data, nothing to process!')
                return False
            
            datarows = self.loaded_data
            for _symbol in datarows:
                try:
                    if _symbol == 'log':
                        continue
                    symbol = _symbol[:3] + _symbol[4:]
                    candles = datarows[_symbol]
                    if candles:
                        log.info('%s first candle: %s' % (symbol, datetime.utcfromtimestamp(candles[0][0])))
                        log.info('%s last candle: %s' % (symbol, datetime.utcfromtimestamp(candles[-1][0])))                         
                        for candle in reversed(candles):                
                            dt = datetime.utcfromtimestamp(candle[0])
                            if dt not in historic.DATAROW[self.timeframe][symbol][PRICE_TIME]:                                                                     
                                if not historic.DATAROW[self.timeframe][symbol][PRICE_TIME]:                                                                                                                 
                                    position = 0                        
                                elif dt + timedelta(minutes=self.timeframe) in historic.DATAROW[self.timeframe][symbol][PRICE_TIME]:
                                    position = historic.DATAROW[self.timeframe][symbol][PRICE_TIME].index(dt + timedelta(minutes=self.timeframe))
                                elif dt - timedelta(minutes=self.timeframe) in historic.DATAROW[self.timeframe][symbol][PRICE_TIME]:
                                    position = historic.DATAROW[self.timeframe][symbol][PRICE_TIME].index(dt - timedelta(minutes=self.timeframe)) + 1
                                elif dt < historic.DATAROW[self.timeframe][symbol][PRICE_TIME][0]:
                                    position = 0
                                elif dt > historic.DATAROW[self.timeframe][symbol][PRICE_TIME][-1]:
                                    position = len(historic.DATAROW[self.timeframe][symbol][PRICE_TIME])
                                    
                                #log.debug('add %s %s, pos: %d' % (symbol, dt, position))
                                historic.DATAROW[self.timeframe][symbol][PRICE_TIME].insert(position, dt)                    
                                historic.DATAROW[self.timeframe][symbol][PRICE_ASKOPEN].insert(position, candle[1])
                                historic.DATAROW[self.timeframe][symbol][PRICE_ASKHIGH].insert(position, candle[2])
                                historic.DATAROW[self.timeframe][symbol][PRICE_ASKLOW].insert(position, candle[3])
                                historic.DATAROW[self.timeframe][symbol][PRICE_ASKCLOSE].insert(position, candle[4])
                                historic.DATAROW[self.timeframe][symbol][PRICE_BIDOPEN].insert(position, candle[5])
                                historic.DATAROW[self.timeframe][symbol][PRICE_BIDHIGH].insert(position, candle[6])
                                historic.DATAROW[self.timeframe][symbol][PRICE_BIDLOW].insert(position, candle[7])
                                historic.DATAROW[self.timeframe][symbol][PRICE_BIDCLOSE].insert(position, candle[8])
                                #historic.DATAROW[self.timeframe][symbol][PRICE_VOLUME].insert(position, candle[9])
                                #historic.DATAROW[self.timeframe][symbol][PRICE_PIPVALUE].insert(position, 0.0001)
                    
                        for price in historic.DATAROW[self.timeframe][symbol]:
                            if price == PRICE_TIME:                                                                
                                historic.DATAROWS[self.timeframe][symbol][price] = historic.DATAROW[self.timeframe][symbol][price]
                            else:
                                historic.DATAROWS[self.timeframe][symbol][price] = np.array(historic.DATAROW[self.timeframe][symbol][price], dtype=np.double)
                        
                        historic.CURRENT_PRICE[symbol]['ASK'] = historic.DATAROW[self.timeframe][symbol][PRICE_ASKCLOSE][-1]
                        historic.CURRENT_PRICE[symbol]['BID'] = historic.DATAROW[self.timeframe][symbol][PRICE_BIDCLOSE][-1]
                        historic.CURRENT_TIME = datetime.utcnow()                    
                        log.info('DATAROW LENGTH: %d' % len(historic.DATAROWS[self.timeframe][symbol][PRICE_TIME]))
                        log.info('CANDLES: %s' % str(historic.DATAROWS[self.timeframe][symbol][PRICE_TIME][-10:]))
                        log.info('CURRENT CANDLE: %s %f %f' % (symbol, historic.CURRENT_PRICE[symbol]['ASK'], historic.CURRENT_PRICE[symbol]['BID']))
                    else:
                        log.info('No new candles for %s%s' % (symbol, TIMEFRAME[self.timeframe]))
                except:
                    log.error(traceback.format_exc())
                    
            log.info('candles loaded')                                    
            return True
        except:
            log.error(traceback.format_exc())
            return False
                            
            
    def check_if_started(self):        
        self.tempfile.seek(0, os.SEEK_SET)
        output = self.tempfile.read()            
        if '{' in output and not self.started:
            self.started = True
            log.info('preloader started, and is waiting for candle data ')
                                    
    
    def start_loader(self):
        self.started = False
        self.tempfile = os.tmpfile()        
        self.process = subprocess.Popen('%s %d %d %d %s' % (config.PRELOADER_JAR, self.timeframe, calendar.timegm(self.from_date.utctimetuple()), calendar.timegm(self.to_date.utctimetuple()), self.symbols), stdout=self.tempfile, shell=True)        


    def stop_loader(self):
        try:
            self.process.kill()
        except:
            log.error(traceback.format_exc())
    
    
    def restart_loader(self):
        if self.restart_count < self.max_restart_count:
            log.info('restarting preloader, retries left: %d' % (self.max_restart_count - self.restart_count))
            if self.retcode is None:              
                self.stop_loader()
            self.start_loader()
            self.restart_count += 1
            self.elapsed = 0
            return True    
        else:
            log.info('maximum restart count reached, giving up...')
            log.error('Unable to use preloader! ')
            return False
        
