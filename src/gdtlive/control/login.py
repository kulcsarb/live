# -*- encoding: utf-8 -*-
'''
OK A felhasználók be- és kiléptetéséért felelős modul

@status: Ellenőrizve
@author: kulcsarb
@newfield url: URL
'''

import random
import gdtlive.store.db as db
from datetime import datetime, timedelta
from gdtlive.admin.system.message import send_mail
from gdtlive.languages.default import OK, EXCEPTION_OCCURED, NOT_FOUND, FORGOT_PASSW_EMAIL_TEMPLATE, EMAIL_SENDING_FAILED, RENEW_EMAIL_SENT_SUCCESSFULLY, MISSING_DATA, RENEW_TIME_EXPIRED, FOR_THIS_ID
from gdtlive.admin.system.log import log, errorlog, extra
from gdtlive.utils import hashed, checkPassword
import json
from gdtlive.config import RENEW_HTML, RESOURCES_ROOT, INDEX_HTML, BROWSE_MAX_TABS, POSTLOAD_DAYS, PRELOAD_DAYS
import traceback
import cherrypy.lib
from gdtlive.control.router import ajax_wrapper
from gdtlive.control.session import handle_session

@cherrypy.expose
@ajax_wrapper
def login(username, password):
    '''Felhasználó beléptetése a rendszerbe. Sikeres azonosítás esetén a felhasználó azonosítóját (User.id) 
    eltárolja a paraméterül kapott HttpSession objektumba, és azt szinkronizálja az adatbázissal. 

    @url: /login/login

    @type username:  string
    @param username: felhasználónév
    @type password:  string
    @param password: jelszó

    @rtype:    (bool, str, list) tuple
    @return:    Állapotjelzés, szöveges üzenet, a felhasználói adatok név-érték szótárának listája
    '''
    session = db.Session()
    http_session = handle_session()
    try:
        user = session.query(db.User).filter_by(username=username).first()
        if not user:
            msg = 'Unknown user: ' + username
            log.info(msg);
            return False, msg
        if user.password != hashed(password):
            msg = 'User ' + username + ' provided wrong password'
            log.info(msg);
            return False, msg
        msg = 'User ' + username + ' logged in'
        log.info(msg)
        http_session.userId = user.id
        session.merge(http_session)
        session.commit()
        return True, OK, dict(user)
    except:
        errorlog.error('Exception raised during login', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@ajax_wrapper
def logout():
    '''Kilépteti az aktuális felahasználót a rendszerből

    @url: /login/logout

    @rtype: bool
    '''
    session = db.Session()
    http_session = handle_session()
    http_session.userId = 0
    try:
        session.merge(http_session)
        session.commit()
    except:
        errorlog.error('Exception raised during logout', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()    
    return True

@cherrypy.expose
@ajax_wrapper
def forgotpass(email):
    '''Jelszóemlékeztető levelet küld a megadott email címre
    
    @url: /login/forgotpass
    
    @type email: string
    @param email: a felhasználó email címe
    
    @rtype: (bool, str) tuple
    @return: A művelet sikerességét jelző boolean és szöveges érték
    '''
    session = db.Session()
    try:
        user = session.query(db.User).filter(db.User.email==email).first() 
        if not user:
            return False, NOT_FOUND + 'user to the email'
        renewId = hashed(str(random.randint(0,999999)))
        link = 'http://%s:%s/login/renewform?renewId=%s' % (cherrypy.server.socket_host, cherrypy.server.socket_port, renewId)            
        success = send_mail('kulcsarb', email,'Subject:renew password', FORGOT_PASSW_EMAIL_TEMPLATE + link)    
        if not success[0]:
            return False, EMAIL_SENDING_FAILED
        renewPassword = db.RenewPassword(renewId, user.id, datetime.now())
        session.add(renewPassword)        
        session.commit()
    except:
        errorlog.error('Exception raised during forgotPass', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return (False, EXCEPTION_OCCURED)
    finally:
        session.close()
    return True, RENEW_EMAIL_SENT_SUCCESSFULLY

@cherrypy.expose
@ajax_wrapper
def renewform(renewId):
    '''Visszaadja a böngészőnek a jelszóváltoztató formot

    @url: /login/renewForm
    @ticket: 130
    @type renewId: string
    @param renewId: a felhasználóhoz előzőleg hozzárendelt jelszóváltoztató ID

    @rtype: string
    @return: A jelszóváltoztató html oldal
    '''
    try:
        http_response = cherrypy.lib.static.serve_file(RESOURCES_ROOT+RENEW_HTML)
    except IOError:
        http_response = ['Not found']
    cherrypy.response.headers['content-type'] = 'text/html'
    return http_response

@cherrypy.expose
def index():    
    '''Visszaadja a böngészőnek a GUI kezdőoldalát

    @url: /
    @rtype: string
    @return: A kezdő html oldal
    '''
    try:
        http_response = cherrypy.lib.static.serve_file(RESOURCES_ROOT+INDEX_HTML)
    except IOError:
        http_response = 'Not found'
    return http_response

@cherrypy.expose
@ajax_wrapper
def renewPass(renewId, password, confirmPassword):
    '''Megváltoztatja a felhasználó jelszavát

    @url: /login/renewPass

    @type renewId: string
    @param renewId: a felhasználóhoz előzőleg hozzárendelt jelszóváltoztató ID
    @type password: string
    @param password: a kért jelszó 
    @type confirmPassword: string
    @param confirmPassword: a kért jelszó ellenőrző párja
    
    @rtype: (bool, str) tuple
    @return: A művelet sikerességét jelző boolean és szöveges érték
    '''
    result = checkPassword(password, confirmPassword)
    if not result[0]:
        return result
    if (not renewId):
        return (False,MISSING_DATA+'renewId')
    session = db.Session()

    query = session.query(db.RenewPassword).filter(db.RenewPassword.renewId==renewId)
    try:
        renewPassword = query.first()
        if not renewPassword:        
            session.close()
            return False, NOT_FOUND + " renewId"
        renewTime = renewPassword.time           
        if(datetime.now()-renewTime > timedelta(1)):
            session.close()
            return False, RENEW_TIME_EXPIRED

        user = session.query(db.User).filter(db.User.id==renewPassword.userId).first()            
        if not user:   
            return False, NOT_FOUND + "user" + FOR_THIS_ID   
        else:
            user.password = hashed(password)

        query.delete()
        session.commit()
    except:
        errorlog.error('Exception raised during renewPass', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()
    return True, OK

@cherrypy.expose
def user_info():
    '''Visszaadja, hogy az adott HTTP kapcsolathoz tartozik-e bejelentkezett felhasználó. Ha igen, akkor visszaadja annak adatait.       

    @url: /login/user_info

    @rtype: string
    @result: json kódolt adatok a GUInak
    '''
    session = db.Session()
    http_session = handle_session()
    if not http_session.userId:
        return json.dumps({'loggedin': False})
    try:
        user = session.query(db.User).filter(db.User.id==http_session.userId).first()        
        if not user:
            return json.dumps({'loggedin': False})
    except:
        errorlog.error('Exception raised during user_info', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()
    return json.dumps({'loggedin': True, 'user': dict(user), 'config': {'BROWSE_MAX_TABS': BROWSE_MAX_TABS, 'POSTLOAD_DAYS': POSTLOAD_DAYS, 'PRELOAD_DAYS': PRELOAD_DAYS}})
