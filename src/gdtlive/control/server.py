# -*- encoding: utf-8 -*-
'''
OK HTTP szervert megvalósító osztályok, funkciók

@status: váltás folyamatban CherryPy-re.

@author: kulcsarb
@newfield url: URL
'''

#from gdtlive.languages.default import *
#from gdtlive.admin.system.log import extra
from os.path import dirname
import cherrypy
import os
import logging

log = logging.getLogger('gdtlive.httpserver')
errorlog = logging.getLogger('errors')


class HTTPServer(object):
    '''
    Wrapper osztály a python WSGIRef osztálya köré.
    Feladata, hogy parancssori használat esetén elindítsa és leállítsa a szoftver
    beépített http szerverét

    @todo: unittestek megírása
    '''
    started = False

    @staticmethod
    def start():
        '''Elindítja a webszervert
        '''
        if not HTTPServer.started:
            HTTPServer.started = True
            log.info('Starting webserver')
            from gdtlive.control import login
            from gdtlive.admin import rollover, spread, defaults
            from gdtlive.historic import datarow
            from gdtlive.evol import info
            from gdtlive.store import strategy, liveaccount, livestrategy, trade, liverun, portfolio, strategyrun
            from gdtlive.core import web, mobile
            root = login.index
            root.login = login
            root.defaults = defaults
            root.rollover = rollover
            root.spread = spread
            root.datarow = datarow
            root.info = info
            root.strategy = strategy
            root.livestrategy = livestrategy
            root.trade = trade
            root.liverun = liverun
            root.liveaccount = liveaccount
            root.portfolio = portfolio
            root.strategyrun = strategyrun
            root.web = web
            root.m = mobile
            
            cherrypy.engine.signal_handler.handlers = {}
            if os.path.exists('/etc/gdtlive/cherrypy.conf'):
                confpath = '/etc/gdtlive'
            else:
                confpath = dirname(__file__) + os.sep + '..' + os.sep + 'etc'
            cherrypy.quickstart(root, config=confpath + os.sep + 'cherrypy.conf')


    @staticmethod
    def stop():
        '''Leállítja a webszervert'''
        if HTTPServer.started:
            print 'Stopping webserver...'
            log.info('Stopping webserver')
            raise SystemExit
