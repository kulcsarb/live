import functools
import json


def ext_response(result):
    if type(result) == bool:        
        result = json.dumps({'success': result})
    if type(result) == tuple:
        if len(result) == 1:
            result = json.dumps({'success': result[0]})
        if len(result) == 2:
            result = json.dumps({'success': result[0], 'msg': result[1]})
        if len(result) == 3:
            result = json.dumps({'success': result[0], 'msg': result[1], 'data': result[2]})
        if len(result) == 4:
            result = json.dumps({'success': result[0], 'msg': result[1], 'data': result[2], 'total': result[3]})
    return result


def ajax_wrapper(func):
    @functools.wraps(func)
    def wrapper(*args, **kw):
        if 'tradeId' in kw:
            kw['tradeId'] = int(kw['tradeId']);
        return ext_response(func(*args, **kw))
    return wrapper


def store_wrapper(func):
    @functools.wraps(func)
    def wrapper(*args, **kw):
        data = '{}'
        if 'data' in kw:
            data = kw['data']
        else:
            for i in args:
                if 'data' in i:
                    data = i['data']
        try:
            data = json.loads(data)
        except ValueError:
            return json.dumps({'success': False, 'msg':'Server error: DATA parameter not in JSON format'})
        except TypeError:
            data = int(data)

        if type(data) is dict:
            result = ext_response(func(**data))
        else:
            result = ext_response(func(data))
        return result
    return wrapper


def tree_wrapper(func):
    @functools.wraps(func)
    def wrapper(*args, **kw):
        if 'node' in kw:
            del kw['node']
        if 'id' in kw:
            kw['id'] = int(kw['id']);
#        if 'workflowrunId' in kw:
#            kw['workflowrunId'] = int(kw['workflowrunId']);
#        if 'showSize' in kw:
#            kw['showSize'] = int(kw['showSize']);
        result = ext_response(func(*args, **kw))
        return result 
    return wrapper


def paging_wrapper(func):
    @functools.wraps(func)
    def wrapper(*args, **kw):
        if 'start' in kw:
            kw['start'] = int(kw['start']);
        if 'limit' in kw:
            kw['limit'] = int(kw['limit']);
        if 'id' in kw:
            kw['id'] = int(kw['id']);
        result = ext_response(func(*args, **kw))
        return result
    return wrapper
