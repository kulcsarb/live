# -*- encoding: utf-8 -*- 
'''
OK HTTP session kezelést végző funkciók

@status: Átírva, nem ellenőrizve.

@author: kulcsarb, vendelin8
@newfield alg: Algoritmus, Algoritmusok
@newfield kov: Követelmény, Követelmények

'''
import cherrypy
import random
import gdtlive.store.db as db
from datetime import datetime
from time import time, strftime, gmtime
from hashlib import md5
#from gdtlive.languages.default import *
import logging, traceback

log = logging.getLogger('gdtlive.control.session')

def handle_session():
    '''
    A megkapott WSGI környezeti paraméterek alapján elvégzi a munkamenet kezeléssel kapcsolatos feladatokat. 
    Eredményként visszaad egy L{gdtlive.store.db.HttpSession} objektumot, mely az aktuális kapcsolathoz tartozó
    munkamenet információkat tartalmazza. Új session létrehozása esetén módosítja a paraméterként kapott 
    http headert

    @return: Aktuális kapcsolatot leíró Session objektum
    @rtype: L{gdtlive.store.db.HttpSession}

    @alg: Algoritmusok/SessionManagement
    @kov: 0.2 
    '''
    HTTPSession = None
    session = db.Session()
    _clear_expired_sessions(session)
    try:
        if 'GDTLIVESession' in cherrypy.request.cookie:
            sessionId = cherrypy.request.cookie['GDTLIVESession'].value
            HTTPSession = session.query(db.HttpSession).filter_by(id=sessionId).first()
            if HTTPSession:
                if cherrypy.request.remote.ip and HTTPSession.ip != cherrypy.request.remote.ip:
                    HTTPSession = None
                else:
                    HTTPSession.last_access_time = datetime.now()        
                    session.commit()    
        if not HTTPSession:
            HTTPSession = _create_new_session()
            session.add(HTTPSession)
            session.commit()
            cherrypy.response.cookie['GDTLIVESession'] = HTTPSession.id
            cherrypy.response.cookie['GDTLIVESession']['max-age'] = 24 * 3600
            cherrypy.response.cookie['GDTLIVESession']['path'] = '/'
    except:
        session.rollback()
        log.error(traceback.format_exc())
    finally:
        session.refresh(HTTPSession)
        session.close()
    return HTTPSession


def _clear_expired_sessions(session):
    '''
    Törli az 1 napnál régebben az adatbázisban tárolt munkamenet bejegyzéseket.

    @alg: Algoritmusok/SessionManagement
    @kov: 0.2
    '''
    limit_ts = time()
    limit_ts -= 24 * 3600
    limit = datetime.fromtimestamp(limit_ts)
    query = session.query(db.HttpSession)
    for HTTPSession in query.all():
        if HTTPSession.last_access_time < limit:
            session.delete(HTTPSession)
    session.commit()


def _create_new_session():
    '''
    Létrehoz egy új HttpSession objektumot, és elmenti az adatbázisba.

    @return: Munkamenet objektum
    @rtype: L{gdtlive.store.db.HttpSession}

    @alg: Algoritmusok/SessionManagement
    @kov: 0.2
    '''
    hash = md5()
    hash.update(strftime('%a, %d %b %Y %H:%M:%S +0000',gmtime()))
    hash.update(str(random.randint(0,999999999999999)))
    sessionid = hash.hexdigest()
    Session = db.HttpSession(sessionid, datetime.now(), cherrypy.request.remote.ip, 0)
    return Session
