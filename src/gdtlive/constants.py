'''
Created on 2011.02.14.

@author: Balazs
'''

DIGIT2 = 0.01
DIGIT4 = 0.0001

PIP_MULTIPLIER = {
        'AUDJPY' : DIGIT2,
        'AUDNZD' : DIGIT4,
        'AUDUSD' : DIGIT4,
        'CADJPY' : DIGIT2,
        'CHFJPY' : DIGIT2,
        'EURAUD' : DIGIT4,
        'EURCAD' : DIGIT4,
        'EURCHF' : DIGIT4,
        'EURGBP' : DIGIT4,
        'EURJPY' : DIGIT2,
        'EURUSD' : DIGIT4,
        'EURNOK' : DIGIT4,
        'EURSEK' : DIGIT4,
        'GBPJPY' : DIGIT2,
        'GBPUSD' : DIGIT4,
        'NZDUSD' : DIGIT4,
        'USDCAD' : DIGIT4,
        'USDCHF' : DIGIT4,
        'USDDDK' : DIGIT4,
        'USDJPY' : DIGIT2,
        'USDNOK' : DIGIT4,
        'USDSEK' : DIGIT4
        }

CURRENCY = ['AUD', 'CAD', 'CHF', 'EUR', 'DDK', 'GBP', 'JPY', 'NOK', 'NZD', 'SEK', 'USD']

TIMEFRAME = {1 : 'M1',
             5 : 'M5',
             15 : 'M15',
             30 : 'M30',
             60 : 'H1',
             240 : 'H4',
             1440 : 'D1',
             10080 : 'W1',
             302400 : 'MO1'
             }

INSTRUMENT = PIP_MULTIPLIER.keys()

PRICE_BID = 0
PRICE_ASK = 1
PRICE_MIDDLE = 2
PRICE_BIDASK = 3

PRICES = {
          PRICE_BID : 'BID',
          PRICE_ASK : 'ASK',
          PRICE_MIDDLE : 'Middle',
          PRICE_BIDASK : 'Bid & Ask'
          }


DIGIT2 = 0.01
DIGIT4 = 0.0001

PIP_MULTIPLIER = {
        'AUDJPY' : DIGIT2,
        'AUDNZD' : DIGIT4,
        'AUDUSD' : DIGIT4,
        'CADJPY' : DIGIT2,
        'CHFJPY' : DIGIT2,
        'EURAUD' : DIGIT4,
        'EURCAD' : DIGIT4,
        'EURCHF' : DIGIT4,
        'EURGBP' : DIGIT4,
        'EURJPY' : DIGIT2,
        'EURUSD' : DIGIT4,
        'EURNOK' : DIGIT4,
        'EURSEK' : DIGIT4,
        'GBPJPY' : DIGIT2,
        'GBPUSD' : DIGIT4,
        'NZDUSD' : DIGIT4,
        'USDCAD' : DIGIT4,
        'USDCHF' : DIGIT4,
        'USDDDK' : DIGIT4,
        'USDJPY' : DIGIT2,
        'USDNOK' : DIGIT4,
        'USDSEK' : DIGIT4
        }

AVAILABLE_CURRENCIES = ['AUD', 'CAD', 'CHF', 'EUR', 'DDK', 'GBP', 'JPY', 'NOK', 'NZD', 'SEK', 'USD']


PRICE_TIME = 0

PRICE_OPEN = 1
PRICE_HIGH = 2
PRICE_LOW = 3
PRICE_CLOSE = 4

PRICE_ASKOPEN = 1
PRICE_ASKHIGH = 2
PRICE_ASKLOW = 3
PRICE_ASKCLOSE = 4

PRICE_BIDOPEN = 5
PRICE_BIDHIGH = 6
PRICE_BIDLOW = 7
PRICE_BIDCLOSE = 8

PRICE_VOLUME = 9
PRICE_PIPVALUE = 10
PRICE_BASEUSD_RATE = 11

DIR_BUY = 0
DIR_SELL = 1
DIR_STR = {DIR_BUY:'BUY', DIR_SELL:'SELL'}

ORDERTYPE_BUY = 0
ORDERTYPE_SELL = 1
ORDERTYPE_CLOSE = 2
ORDERTYPE_PARTIAL_CLOSE = 3
ORDERTYPE_CLOSE_BY_TP = 4
ORDERTYPE_CLOSE_BY_SL = 5
ORDERTYPE_SETSL = 6
ORDERTYPE_SETTP = 7


ORDERTYPE_CLOSETRADE = 8
ORDERTYPE_CLOSEFIRST = 9
ORDERTYPE_CLOSELAST = 10
ORDERTYPE_CLOSETRADE_PARTIAL = 11
ORDERTYPE_CLOSETRADE_PERCENT = 12
ORDERTYPE_CLOSEALL = 13
ORDERTYPE_CLOSEAMOUNT = 14
ORDERTYPE_CLOSEAMOUNTPERCENT = 15
ORDERTYPE_SETSL_FORALL = 16
ORDERTYPE_SETTP_FORALL = 17
ORDERTYPE_STOPLOSSHIT = 18
ORDERTYPE_TAKEPROFITHIT = 19
ORDERTYPE_CLOSEALL_LOSING = 20
ORDERTYPE_FORCE_CLOSE = 21


ORDERTYPE_STR = {
            ORDERTYPE_BUY : 'Buy',
            ORDERTYPE_SELL : 'Sell',
            ORDERTYPE_CLOSE : 'Close',
            ORDERTYPE_PARTIAL_CLOSE : 'Partial close',
            ORDERTYPE_CLOSEALL : 'Close all',
            ORDERTYPE_CLOSE_BY_TP : 'Takeprofit hit',
            ORDERTYPE_CLOSE_BY_SL : 'Stoploss hit',
            ORDERTYPE_SETSL : 'Stoploss',
            ORDERTYPE_SETTP : 'Takeprofit',
            ORDERTYPE_FORCE_CLOSE : 'Force close'
            }

MINIMAL_SL_DISTANCE = 10
MINIMAL_TP_DISTANCE = 10

NODETYPE_FLOAT = 1
NODETYPE_INT = 2
NODETYPE_BOOL = 3
NODETYPE_SIGNAL = 4
NODETYPE_TIME = 5
NODETYPE_COMMAND = 6
NODETYPE_ROOT = 7
NODETYPE_ONOFFSIGNAL = 8
NODETYPE_COMMANDGROUP = 9

NODETYPE_NAMES = {
                  NODETYPE_FLOAT:   'FLOAT',
                  NODETYPE_INT:     'INT',
                  NODETYPE_BOOL:    'BOOL',
                  NODETYPE_SIGNAL:  'SIGNAL',
                  NODETYPE_TIME:    'TIME',
                  NODETYPE_COMMAND: 'COMMAND',
                  NODETYPE_ROOT:    'ROOT',
                  NODETYPE_ONOFFSIGNAL: 'ON/OFFSIGNAL',
                  NODETYPE_COMMANDGROUP : 'COMMANDGROUP'
                  }
#
#
#PERFORMANCE_POINTS = 1
PERFORMANCE_CURRENCY = 2
#
STRATEGYRUN_EVOL = 1
STRATEGYRUN_BACKTEST = 2
STRATEGYRUN_LIVE = 3

STRATEGYRUN_STR = {
                   STRATEGYRUN_EVOL : 'evol',
                   STRATEGYRUN_BACKTEST : 'backtest',
                   STRATEGYRUN_LIVE : 'live'
                   }

TEMP_DIRECTORY = './'

#TASK_TYPE_WORKFLOW = 0
#TASK_TYPE_EVOL = 1
#TASK_TYPE_BACKTEST = 2
#
#TASK_TYPE_STR = {
#                TASK_TYPE_WORKFLOW : "Workflow",
#                TASK_TYPE_EVOL : "Evol",
#                TASK_TYPE_BACKTEST : "Backtest"
#                 }
#
#HISTORY_ACTION_SAVED = 1
#HISTORY_ACTION_STARTED = 2
#HISTORY_ACTION_STOPPED = 3
#HISTORY_ACTION_VIEWED = 4
#HISTORY_ACTION_IMPORTED = 5
#HISTORY_ACTION_FAILED = 6
#
#HISTORY_ACTION_TYPE = {
#                      HISTORY_ACTION_SAVED : "Saved",
#                      HISTORY_ACTION_STARTED : "Started",
#                      HISTORY_ACTION_STOPPED : "Stopped",
#                      HISTORY_ACTION_VIEWED : "Viewed",
#                      HISTORY_ACTION_IMPORTED : "Imported",
#                      HISTORY_ACTION_FAILED : "Failed"
#                      }
#

ELEMENT_TYPE_WORKFLOW = 0
ELEMENT_TYPE_WORKFLOWRUN = 1
ELEMENT_TYPE_EVOLCONFIG = 2
ELEMENT_TYPE_EVOLRUN = 3
ELEMENT_TYPE_EVOLCYCLE = 4
ELEMENT_TYPE_STRATEGY = 5
ELEMENT_TYPE_STRATEGYRUN = 6
ELEMENT_TYPE_BACKTESTCONFIG = 7

ELEMENT_TYPE = {ELEMENT_TYPE_WORKFLOW : "Workflow",
                ELEMENT_TYPE_WORKFLOWRUN : "WorkflowRun",
                ELEMENT_TYPE_EVOLCONFIG : "Evol",
                ELEMENT_TYPE_EVOLRUN : "EvolRun",
                ELEMENT_TYPE_EVOLCYCLE : "EvolCycle",
                ELEMENT_TYPE_STRATEGY : "Strategy",
                ELEMENT_TYPE_STRATEGYRUN : "StrategyRun",
                ELEMENT_TYPE_BACKTESTCONFIG : "BacktestConfig"
                }

#LIST_ELEMENT_TYPE = {
#                    ELEMENT_TYPE_WORKFLOW : "Workflow",
#                    ELEMENT_TYPE_WORKFLOWRUN : "WorkflowRun",
#                    ELEMENT_TYPE_EVOLCONFIG : "Evol",
#                    ELEMENT_TYPE_EVOLRUN : "EvolRun",
#                    ELEMENT_TYPE_EVOLCYCLE : "EvolCycle",
#                    ELEMENT_TYPE_STRATEGY : "Strategy",
#                    ELEMENT_TYPE_STRATEGYRUN : "StrategyRun",
#                    ELEMENT_TYPE_BACKTESTCONFIG : "BacktestConfig"
#                    }
#
##SEARCH_ELEMENT_TYPE ={
##                    0:"Strategy",
##                    1:"EvolCycle",
##                    2:"EvolRun",
##                    3:"EvolConfig",
##                    4:"WorkflowRun",
##                    5:"Workflow"
##                    }
#
#
LIVERUN_STATUS_STARTING = 0
LIVERUN_STATUS_RUNNING = 1
LIVERUN_STATUS_STOPPING = 2
LIVERUN_STATUS_ABORTING = 3
LIVERUN_STATUS_STOPPED = 4
LIVERUN_STATUS_ABORTED = 5
LIVERUN_STATUS_FUCKEDUP = 6
LIVERUN_STATUS_EXITED = 7

LIVERUN_STATUS_STR = {
                      LIVERUN_STATUS_STARTING : 'starting',
                      LIVERUN_STATUS_RUNNING : 'running',
                      LIVERUN_STATUS_STOPPING : 'waiting to stop',
                      LIVERUN_STATUS_ABORTING : 'aborting',
                      LIVERUN_STATUS_STOPPED : 'stopped',
                      LIVERUN_STATUS_ABORTED : 'aborted',
                      LIVERUN_STATUS_FUCKEDUP : 'fuckedup, call sysadmin',
                      LIVERUN_STATUS_EXITED : 'exited'
                      }


LIVE_BROKERS = {
                0:'Benchmark',
                1:'Dukascopy',
                2:'Iceberg',
                3:'Dukascopy JForex',
                4:'Dukascopy Aggregate'
                }


LIVE_API_JFOREX = 0
LIVE_API_FIX = 1

#LIVE_BROKER_CONNECTIONS = {
#                        1: (LIVE_API_JFOREX, LIVE_API_FIX),
#                        2: LIVE_API_FIX
#                        }


#LIVE_BROKER_APIS = {
#                    LIVE_API_FIX : 'FIX',
#                    LIVE_API_JFOREX : 'JForex',
#                    }

PM_DYNAMIC = 0 
PM_STATIC = 1

PORTF_MANAGEMENT = {
                    PM_DYNAMIC : 'Dynamic',
                    PM_STATIC : 'Static'                    
                    }



STORAGE_PATH = '/var/spool/gdt/historic'
