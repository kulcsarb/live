'''
Created on Jan 29, 2013

@author: gdt
'''

from gdtlive.backtest.backtester import Backtester, StratPlugins
from gdtlive.constants import STRATEGYRUN_BACKTEST, PERFORMANCE_CURRENCY
from gdtlive.admin.system.log import initialize_loggers
import gdtlive.config
import gdtlive.historic.plugin.dukascopy_fs
import gdtlive.store.db as db
from ast import literal_eval
from datetime import date, datetime
from multiprocessing import Process
import traceback
import logging
import random

log = logging.getLogger('gdtlive.core.managers')



def calculate(session, strategy_id, symbol, timeframe, dns, mmplugin_id, mmplugin_parameters, run_from, run_to, weekend_start, stratPuginId):    
    accPuginId = 1
    gdtlive.config.WEEKEND_START = weekend_start
    backtest = Backtester()    
    backtest.setStratPlugin(stratPuginId, accPuginId, mmplugin_id)
    if ',' in symbol:
        symbols = symbol.split(',')
    else:
        symbols = [symbol]
    for s in symbols:
        datarow = session.query(db.DatarowDescriptor).filter(db.DatarowDescriptor.symbol == s).filter(db.DatarowDescriptor.timeframe_num == timeframe).first()
        if not datarow:
            log.info('datarow not found for %s %d' % (s, timeframe))
            return
        backtest.add_datarow(datarow.id)
                
    backtest.from_date = run_from
    backtest.to_date = run_to        
    mmplugin_params = {'equity' : 10000,
                       'leverage' : 30,
                       'parameters' : literal_eval(mmplugin_parameters)
                       }
    backtest.strategy.configMM(mmplugin_id, mmplugin_params)
    backtest.use_orderlog(True)
    backtest.strategy.load_spread(1)
    backtest.strategy.load_rollover(1)
    backtest.strategy.commission = 0.5
    backtest.strategy.load_historic()
    log.info('evaluate...')        
    performance = backtest.evaluate(dns)                
    serial = backtest.get_serial_performance()
    tradelog = backtest.get_tradelog()
    orderlog = backtest.get_orderlog()
    return performance, serial, tradelog, orderlog                
    
    
def backtest(session, strategy_id, symbol, timeframe, dns, mmplugin_id, mmplugin_parameters, evolved_from, evolved_to, weekend_start, stratplugin_id):    
    
    log.info('backtesting strategy %d' % strategy_id)    
    try:                          
        q = session.query(db.StrategyRun).filter(db.StrategyRun.type == STRATEGYRUN_BACKTEST).filter(db.StrategyRun.strategyId == strategy_id)
        q = q.filter(db.StrategyRun.configId == db.BacktestConfig.id).filter(db.BacktestConfig.from_date==evolved_from).filter(db.BacktestConfig.to_date==evolved_to)
        evolbacktest = q.first()        
        if not evolbacktest:                        
            log.info('creating a new backtest for evol')
            performance, serial, tradelog, orderlog = calculate(session, strategy_id, symbol, timeframe, dns, mmplugin_id, mmplugin_parameters, evolved_from, evolved_to, weekend_start, stratplugin_id)
            
            config = db.BacktestConfig(str([strategy_id]), evolved_from, evolved_to, 1, 1, mmplugin_id, mmplugin_parameters, 10000, 30, 0, 0, datetime.utcnow(), 1, 1, stratpluginId = stratplugin_id)                        
            if ',' in symbol:
                symbols = symbol.split(',')
            else:
                symbols = [symbol]
            for s in symbols:
                datarow = session.query(db.DatarowDescriptor).filter(db.DatarowDescriptor.symbol == s).filter(db.DatarowDescriptor.timeframe_num == timeframe).first()
                config.datarows.append(datarow)
                
            serial_perf = db.PerformanceStrategySerialData([], [], [], [], [], [], [], [], [], [], [], [])
            serial_perf.update(serial)
            perf = db.PerformanceData(PERFORMANCE_CURRENCY, performance)
            
            session.add(perf)
            session.add(serial_perf)
            session.add(config)
            session.commit()
            
            run = db.StrategyRun(strategy_id, STRATEGYRUN_BACKTEST, None, 0, perf.id, serial_perf.id, config.id)
            run.tradelog = str(tradelog)
            run.orderlog = str(orderlog)
            session.add(run)
            session.commit()
            
        
        q = session.query(db.StrategyRun).filter(db.StrategyRun.type == STRATEGYRUN_BACKTEST).filter(db.StrategyRun.strategyId == strategy_id)
        q = q.filter(db.StrategyRun.configId == db.BacktestConfig.id).filter(db.BacktestConfig.from_date==evolved_to)
        backtestrun = q.first()
        performance, serial, tradelog, orderlog = calculate(session, strategy_id, symbol, timeframe, dns, mmplugin_id, mmplugin_parameters, evolved_to, date.today(), weekend_start, stratplugin_id)
        
        if not backtestrun:
            log.info('creating a new backtest for evol_to - today')
                        
            config = db.BacktestConfig(str([strategy_id]), evolved_to, date.today(), 1, 1, mmplugin_id, mmplugin_parameters, 10000, 30, 0, 0, datetime.utcnow(), 1, 1, stratpluginId = stratplugin_id)                        
            if ',' in symbol:
                symbols = symbol.split(',')
            else:
                symbols = [symbol]
            for s in symbols:
                datarow = session.query(db.DatarowDescriptor).filter(db.DatarowDescriptor.symbol == s).filter(db.DatarowDescriptor.timeframe_num == timeframe).first()
                config.datarows.append(datarow)   
            
            serial_perf = db.PerformanceStrategySerialData([], [], [], [], [], [], [], [], [], [], [], [])
            serial_perf.update(serial)
            perf = db.PerformanceData(PERFORMANCE_CURRENCY, performance)
            
            session.add(perf)
            session.add(serial_perf)
            session.add(config)
            session.commit()
            
            run = db.StrategyRun(strategy_id, STRATEGYRUN_BACKTEST, None, 0, perf.id, serial_perf.id, config.id)
            run.tradelog = str(tradelog)
            run.orderlog = str(orderlog)
            session.add(run)
            session.commit()
            
        else:
            log.info('updating existing performance info')
            config = session.query(db.BacktestConfig).get(backtestrun.configId)
            config.to_date = date.today()
            backtestrun.tradelog = str(tradelog)
            backtestrun.orderlog = str(orderlog)
            serial_perf = session.query(db.PerformanceStrategySerialData).get(backtestrun.performanceSerialId)
            serial_perf.update(serial)            
            perf = session.query(db.PerformanceData).get(backtestrun.performanceIdCurrency)
            perf.update(performance)
            session.commit()
            
            
            
    except:
        log.error(traceback.format_exc())
    finally:
        pass
    


def backtest_all_strategies():
    try:
        session = db.Session()
        strategies = session.query(db.Strategy).order_by(db.Strategy.id).all()        
        for strategy in strategies:                
            backtest(session, strategy.id, strategy.symbol, strategy.timeframe, strategy.dns, strategy.mmplugin_id, strategy.mmplugin_parameters, strategy.evolved_from, strategy.evolved_to, strategy.weekend_start, strategy.stratplugin_id) 
    except:
        log.error(traceback.format_exc())    
    finally:
        session.close()        
        
        
if __name__ == '__main__':
    initialize_loggers()
#    db.SQL_PASS = 'gdt'
#    db.SQL_USER = 'gdt'
#    db.SQL_URL = 'main/live'    
    db.init()
    gdtlive.historic.plugin.dukascopy_fs.initialize_datarow_descriptors()
    gdtlive.historic.plugin.dukascopy_fs.cleanup_db()
    backtest_all_strategies()
    