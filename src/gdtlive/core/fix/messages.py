# -*- encoding: utf-8 -*- 
'''
Created on 2012.05.27.

@author: kulcsarb
'''
from nose.plugins.attrib import attr
from datetime import datetime
from gdtlive.constants import DIR_BUY, DIR_SELL
import threading


class Message(object):        
    
    def __init__(self):
        self.MsgType = None
        self.MsgSeqNum = None
        self.SenderCompID = None
        self.TargetCompID = None
        self.SendingTime = None
        self.PossDupFlag = None
        self._resend = True
        self._processed_event = threading.Event()
        self._processed = False
        self._process_result = True
        
        
    def processed(self, result=True):
        self._processed = True
        self._process_result = result
        self._processed_event.set()
        
                                                
    def wait_until_processed(self):        
        self._processed_event.wait()
        return self._process_result


    @staticmethod
    def decode(dict_message):        
        msg_cls = {
                   'Logon' : LogonMessage,
                   'Logout' : LogoutMessage,
                   'HeartBeat' : HeartBeatMessage,
                   'Test Request' : TestRequestMessage,
                   'Resend Request' : ResendRequestMessage,
                   'Sequence Reset' : SequenceResetMessage,
                   'Reject' : RejectMessage,
                   'BusinessMessageReject' : BusinessMessageReject,
                   'NewOrderSingle' : OrderSingleNewMessage,
                   'OrderCancelRequest': OrderCancelRequestMessage,
                   'OrderCancelReject' : OrderCancelReject,
                   'ExecutionReport': ExecutionReportMessage,
                   'Notification' : Notification,
                   'AccountInfoRequest' : AccountInfoRequestMessage, 
                   'AccountInfo' : AccountInfoMessage,
                   'OrderMassStatusRequest' : OrderMassStatusRequestMessage,
                   'OvernightReport' : OvernightReportMessage,
                   'TradingSessionStatus' : TradingSessionStatus,
                   'InstrumentPositionInfo' : InstrumentPositionInfo,
                   'QuoteStatusReport' : QuoteStatusReport,
                   'MarketDataRequest' : MarketDataRequest,
                   'MarketDataRequestReject' :  MarketDataRequestReject,
                   'MarketDataSnapshot' : MarketDataSnapshot
                   }[dict_message['MsgType']]        
        
        return msg_cls().create_from(dict_message)
    
    
    def create_from(self, data):
        m = self.__class__()
        for k,v in data.iteritems():
            #if k not in ['CheckSum','BeginString','BodyLength']:
            m.__dict__[k] = v
        return m
    
    def __repr__(self):
        return self.__class__.__name__ + '(' + str(self.__dict__) + ')'
   


# --------------------------------------------------
#    Administrative messages 
# ---------------------------------------------------

class LogonMessage(Message):
    def __init__(self, username=None, password=None, heartbeatint=30, resetseqnumflag=True):
        Message.__init__(self)
        assert type(heartbeatint) == int
        assert type(resetseqnumflag) == bool
        assert username is None or type(username) == str
        assert password is None or type(password) == str 
        self.MsgType = 'Logon'
        self.HeartBtInt = heartbeatint
        self.ResetSeqNumFlag = resetseqnumflag
        self.Username = username
        self.Password = password
        self.EncryptMethod = 0
                
    def __str__(self):
        return 'Logon(%s, %s, %s)' % (self.Username, self.Password, self.HeartBtInt)    
        
    
class LogoutMessage(Message):
    def __init__(self):
        Message.__init__(self)
        self.MsgType = 'Logout'
        self.Text = None
        
    def __str__(self):
        return 'Logout()'
    

class TestRequestMessage(Message):
    
    def __init__(self, testreqid=None):
        Message.__init__(self)
        assert testreqid is None or type(testreqid) == str
        
        self.MsgType = 'Test Request'                                                     
        self.TestReqID = testreqid    
    
    def __str__(self):
        return 'TestRequest(TestReqID = %s)' % (str(self.TestReqID))
     

class HeartBeatMessage(Message):
    def __init__(self, testreqid=None):
        Message.__init__(self)
        self.MsgType = 'HeartBeat'
        self.TestReqID = testreqid       
        if testreqid : 
            assert type(testreqid) == str             

    def __str__(self):        
        return 'HeartBeat' + ('(TestReqID = %s)' % self.TestReqID if self.TestReqID else '()')
        

class ResendRequestMessage(Message):
    def __init__(self, begin=None, end=None):
        Message.__init__(self)
        self.MsgType = 'Resend Request'
        self.BeginSeqNo = begin
        self.EndSeqNo = end

    def __str__(self):
        return 'ResendRequest(begin=%d, end=%d)' % (self.BeginSeqNo, self.EndSeqNo)
    
    
class SequenceResetMessage(Message):
    def __init__(self, gapfillflag = None, newseqno=None):
        Message.__init__(self)
        self.MsgType = 'Sequence Reset'
        self.GapFillFlag = gapfillflag
        self.NewSeqNo = newseqno
        
    def __str__(self):
        s = 'SeqReset-GapFill' if self.GapFillFlag else 'SeqReset-Reset'        
        s += '(%d)' % self.NewSeqNo if self.NewSeqNo else '()'
        return s
        
        
class SeqReset_GapFill(SequenceResetMessage):    
    def __init__(self, newseqno):
        SequenceResetMessage.__init__(self, True, newseqno)       
        self.PossDupFlag = True     

class SeqReset_Reset(SequenceResetMessage):
    def __init__(self, newseqno):
        SequenceResetMessage.__init__(self, False, newseqno)        
    


class RejectMessage(Message):
    def __init__(self):
        Message.__init__(self)
        self.MsgType = 'Reject'
        self.RefSeqNum = None
        self.RefTagID = None
        self.RefMsgType = None
        self.SessionRejectReason = None
        self.Text = None
        self.EncodedTextLen = None
        self.EncodedText = None
                
    def __str__(self):
        return 'Reject(message: %s, type: %s, tag: %s, reason: %s, "%s")' % (str(self.RefSeqNum), str(self.RefMsgType), str(self.RefTagID), str(self.SessionRejectReason), str(self.Text)) 
    
    
# ------------------------------------------------------------------------------------
#
#            Application messages
#
# ------------------------------------------------------------------------------------

        
class BusinessMessageReject(Message):
    
    def __init__(self):
        Message.__init__(self)
        self.MsgType = 'BusinessMessageReject'
        self.RefSeqNum = None
        self.RefMsgType = None
        self.BusinessRejectRefID = None
        self.BusinessRejectReason = None
        self.Text = None
        self.EncodedTextLen = None
        self.EncodedText = None
        
        
    def __str__(self):
        return 'BusinessMessageReject(original: %s, %s; reason: %s, %s, "%s"' % (str(self.RefSeqNum), self.RefMsgType, self.BusinessRejectRefID, self.BusinessRejectReason, self.Text)
        


#    ------------------------------------------------
#            Order handling
#    ------------------------------------------------

    
class OrderSingleNewMessage(Message):
    def __init__(self, symbol=None, direction=None, amount=0, clordid=None):
        Message.__init__(self)
        self.MsgType = 'NewOrderSingle'        
        self.ClOrdID = clordid
        self.AvgPx = None
        self.OrdType = None
        self.TimeInForce = None
        self.Symbol = symbol        
        self.OrderQty = float(amount)# / 1000000.0      #FIXME: Jó ez igy ? 
        self.Side = {DIR_BUY:'Buy', DIR_SELL:'Sell'}.get(direction, None)        
        self.TransactTime = datetime.now()
        self.ExpireTime = None
        self.Account = None
        self.Slippage = None    # FIXME: a slippage-t impement'lni kell a fix44-ben !
                                
        
    
class MarketOrder(OrderSingleNewMessage):
    def __init__(self, symbol=None, direction=None, amount=0, clordid=None):
        OrderSingleNewMessage.__init__(self, symbol, direction, amount, clordid)
        self.OrdType = 'Market'
        self.TimeInForce = 'FOK'  #FIXME: KELL EZ IDE ?
                        
            
        
class StopOrder(OrderSingleNewMessage):
    def __init__(self, symbol=None, direction=None, amount=0, price=None, clordid=None):    
        OrderSingleNewMessage.__init__(self, symbol, direction, amount, clordid)
        self.Price = price
        self.OrdType = 'Stop'
        self.TimeInForce = 'GTC'


class LimitOrder(OrderSingleNewMessage):
    def __init__(self, symbol=None, direction=None, amount=0, price=None, clordid=None):    
        OrderSingleNewMessage.__init__(self, symbol, direction, amount, clordid)
        self.Price = price
        self.OrdType = 'Stop limit'
        self.TimeInForce = 'GTC'
        
        
class OrderCancelRequestMessage(Message):
    def __init__(self, symbol=None, order_id=None, orig_clord_id=None, clord_id=None):
        Message.__init__(self)
        self.MsgType = 'OrderCancelRequest'
        self.OrderID = order_id
        self.OrigClOrdID = orig_clord_id
        self.ClOrdID = clord_id
        self.Account = None
        self.Symbol = symbol
        
class OrderCancelReject(Message):
    def __init__(self):
        Message.__init__(self)
        self.MsgType = 'OrderCancelReject'
        
        
class ExecutionReportMessage(Message):
    def __init__(self):
        Message.__init__(self)
        self.MsgType = 'ExecutionReport'
        self.OrderID = None
        self.ClOrdID = None
        self.ExecID = None
        self.OrdStatus = None
        self.ExecType = None
        self.Symbol = None
        self.TimeInForce = None
        self.CumQty = None
        self.LeavesQty = None
        self.OrderQty = None
        self.Side = None
        self.OrdType = None
        self.AvgPx = None
        self.ExpireTime = None
        self.TransactTime = None
        self.LastRptRequested = None
        self.Account = None
        self.Slippage = None
        self.OrdRejReason = None
        self.CashMargin = None




class Notification(Message):
    
    def __init__(self, priority = 0, text=None):
        Message.__init__(self)
        self.MsgType = 'Notification'
        self.NotifPriority = priority
        self.Text = text
        
        
    def __str__(self):
        s = self.__class__.__name__ + '(%s, "%s")' % (self.NotifPriority, self.Text)            
        return  s
        

class AccountInfoRequestMessage(Message):
    
    def __init__(self):
        Message.__init__(self)
        self.MsgType = 'AccountInfoRequest'
        
    def __str__(self):
        return 'AccountInfoRequest()'
    
    
class AccountInfoMessage(Message):
    
    def __init__(self):
        Message.__init__(self)
        self.MsgType = 'AccountInfo'
        self.Leverage = None
        self.UsableMargin = None
        self.Equity = None
        self.Currency = None
        self.AccountName = None

    def __str__(self):
        eq = '' if not self.Equity else '%.2f' % self.Equity
        lev = '' if not self.Leverage else '%.1f' % self.Leverage
        um = '' if not self.UsableMargin else '%.1f' % self.UsableMargin
        return 'AccountInfo(%s %s, equity: %s, 1:%s, usablemargin: %s)' % (self.AccountName, self.Currency, eq, lev, um)
    
    
class InstrumentPositionInfo(Message):
    
    def __init__(self):
        Message.__init__(self)
        self.MsgType = 'InstrumentPositionInfo'
        self.Symbol = None
        self.Amount = None
        self.AccountName = None
        
    def __str__(self):
        return '%s(%s, %s: %s)' % (self.__class__.__name__, self.AccountName, self.Symbol, self.Amount)
    
    
        
class OrderMassStatusRequestMessage(Message):
    
    def __init__(self, massstatusreqid = None):
        Message.__init__(self)
        self.MsgType = 'OrderMassStatusRequest'
        self.MassStatusReqType = 'Status for all orders'
        self.MassStatusReqID = massstatusreqid         
        
    def __str__(self):
        return 'OrderMassStatusRequest(ID: %s, %s)' % (self.MassStatusReqID, self.MassStatusReqType) 
        
        
class OvernightReportMessage(Message):
    
    def __init__(self): 
        Message.__init__(self)
        self.MsgType = 'OvernightReport'
        self.Account = None
        self.Symbol = None
        self.Amount = None
        


class TradingSessionStatus(Message):
    
    def __init__(self):
        #FIXME: this is a minimal implementation of the message`s fields!
        Message.__init__(self)
        self.MsgType = 'TradingSessionStatus'        
        self.TradSesReqID = None
        self.TradingSessionID = None
        self.TradingSessionSubID = None
        self.TradSesMethod = None
        self.TradSesMode = None
        self.UnsolicitedIndicator = None
        self.TradSesStatus = None
        self.TradSesStatusRejReason = None
        self.TradSesStartTime = None
        self.TradSesOpenTime = None
        self.TradSesPreCloseTime = None
        self.TradSesCloseTime = None
        self.TradSesEndTime = None
        self.TotalVolumeTraded = None
        self.Text = None
        self.EncodedTextLen = None
        self.EncodedText = None                         
        
    def __str__(self):
        return 'TradingSession(%s, %s)' % (self.TradingSessionID, self.TradSesStatus)
        
                
                
#    ------------------------------------------------
#                Market Data 
#    ------------------------------------------------

        
class MarketDataRequest(Message):
    def __init__(self, symbol = None): 
        Message.__init__(self)
        self.MsgType = 'MarketDataRequest'
        self.MDReqID = 'GDT'
        self.SubscriptionRequestType = 'Subscribe' 
        self.MarketDepth = 'Top of Book'
        self.MDUpdateType = 'Full Refresh'        
        self.MDEntryTypes = [{'MDEntryType' : 'Bid'},
                             {'MDEntryType' : 'Offer'}]
                  
        self.RelatedSym = [{'Symbol' :  symbol}]
        
        # Use this field only in case if you want to subscribe for “Fill or Kill” data feed!
        # self.TimeInForce = None      

class MarketData_Subscribe(MarketDataRequest):
    def __init__(self, symbol):
        MarketDataRequest.__init__(self, symbol)
        self.SubscriptionRequestType = 'Subscribe'


class MarketData_Unsubscribe(MarketDataRequest):
    def __init__(self, symbol):
        MarketDataRequest.__init__(self, symbol)
        self.SubscriptionRequestType = 'Unsubscribe'
        


class MarketDataRequestReject(Message):
    def __init__(self):
        Message.__init__(self)
        self.MsgType = 'MarketDataRequestReject'
        self.MDReqID = None 
        self.MDReqRejReason = None          
        self.Text = None
        self.EncodedTextLen = None
        self.EncodedText = None
    
        
class MarketDataSnapshot(Message):
    def __init__(self):
        Message.__init__(self)
        self.MsgType = 'MarketDataSnapshot'        
        self.Symbol = None
        self.MDEntries = None
        
    def bid(self):
        try:
            return self.MDEntries[1]['MDEntryPx']
        except:
            return None
    
    def ask(self):
        try:
            return self.MDEntries[0]['MDEntryPx']
        except:
            return None
       
    def __str__(self):
        return '%s(%s, %.5f, %.5f)' % (self.__class__.__name__, self.Symbol, self.ask(), self.bid())
        
    

class QuoteStatusReport(Message):
    def __init__(self): 
        Message.__init__(self)
        self.MsgType = 'QuoteStatusReport'
        self.QuoteID = None
        self.Symbol = None
        self.QuoteType = None 
            
    