# -*- encoding: utf-8 -*- 
'''
Created on 2012.05.28.

@author: kulcsarb
'''
import threading
import socket
import ssl
import logging
import traceback
from engine import FIXEngine
from gdtlive.core.constants import STATE_LOGON_INITIATED, STATE_SESSION_ESTABLISHED
import gdtlive.core.fix.dukascopy.fix as fix

log = logging.getLogger('gdtlive.core.engine')



class FIXAcceptorServer(threading.Thread):
    thread_num = 1
    
    def __init__(self, host, port, acceptor = None):
        threading.Thread.__init__(self)
        self.name = 'FIXAcceptorServer-%d' % self.thread_num
        self.thread_num += 1
        self.host = host
        self.port = port
        self.server_threads = []
        self._stop = False
        self.acceptor = acceptor        
    
    def run(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.host, self.port))
        self.socket.listen(1)                
        log.info('%s - acceptor server started' % self.name)                
        try:
            while True:
                client_socket, addr = self.socket.accept()
                if self._stop:
                    break
                log.info('%s - client connected: %s ' % (self.name, str(addr)))
                client_socket = ssl.wrap_socket(client_socket, server_side=True, certfile="/home/gdt/ssl/root-ca.crt",
                                 keyfile="/home/gdt/ssl/root-ca.key")
                if self.acceptor:
                    acceptor = self.acceptor(client_socket, addr)
                else:
                    acceptor = FIXAcceptor(client_socket, addr)                
                log.info('%s - server thread started' % self.name)
                acceptor.start()
                self.server_threads.append(acceptor)
        except Exception as exc:
            if exc.errno == 22:
                # normál kilépés, a socket lezárása eredményezi a self.shutdown()-ban
                pass
            else:
                log.error(traceback.format_exc())
                
        self.close_socket()
        
        log.info('%s - shutting down server threads' % self.name)    
        try:
            for acceptor in self.server_threads[:]:
                acceptor.shutdown()
            self.server_threads = []
        except:
            log.error(traceback.format_exc())        
        
    def close_socket(self):
        try:
            self.socket.shutdown(socket.SHUT_RDWR)
            self.socket.close()
        except:
            pass
               
    def shutdown(self):
        log.info('shutting down AcceptorServer')
        self.close_socket()
        
                                
class FIXAcceptor(FIXEngine):
    def __init__(self, socket, addr, message_handler = None):
        FIXEngine.__init__(self, message_handler)
        self.socket = socket   
        self.client_addr = addr
        self.name = 'FIXAcceptor-%d (%s)' % (self.thread_num, str(addr))
        self.thread_num += 1        


    def _handle_logon(self, message):
        log.info('%s - Logon message received' % self.name)
        #### Authenticating #####
        if message.Username is not None and message.Password is not None:
            log.info('%s - Client logged in' % self.name)
            self.connection_state = STATE_LOGON_INITIATED
            self.client_loggedon = True
            self.SenderCompID = message.TargetCompID
            self.TargetCompID = message.SenderCompID                        
            if message.ResetSeqNumFlag:
                log.debug('%s - Reset sequence numbers' % self.name)
                self.sender_thread.store.reset()
                self.store.reset()                
                
            log.debug('%s - Sending confirmation Logon message' % self.name)
            reply = fix.Logon(HeartBtInt=self.heartbeat_int, ResetSeqNumFlag=True, EncryptMethod=0)                        
            self.send(reply)
            self.connection_state = STATE_SESSION_ESTABLISHED
            log.info('%s - FIX Session established' % self.name)
            return True
        else:
            self.socket.close()
            return False


    def _handle_logout(self, message):
        log.info('%s - Logout message received' % self.name)
        log.info('%s - sending confirmation Logout message' % self.name)
        self.send(fix.Logout())

