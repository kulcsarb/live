# -*- encoding: utf-8 -*- 
'''
Created on 2012.05.22.

@author: kulcsarb
'''
import re
import gdtlive.core.storage.store as store
import socket
#import ssl
import traceback
import copy 
import time
import pprint
from datetime import datetime, timedelta
import threading
from Queue import Queue

import logging 
from gdtlive.core.constants import STATE_NOT_CONNECTED, STATE_CONNECTED, ENGINE_SHUTDOWN_INTERNAL_ERROR, ENGINE_SHUTDOWN_RECONNECT_FAILED,\
    STATE_SHUTDOWN, STATE_SESSION_ESTABLISHED, ENGINE_SHUTDOWN_LOWERMSGSEQNUM, STATE_LOGON_INITIATED, CONNECTION_STATE
    
import dukascopy.fix as fix



class MessageSender(threading.Thread):
    
    def __init__(self, msg_queue, socket, sendercompid, targetcompid, use_ssl=True, receiver=None):
        threading.Thread.__init__(self)        
        self.msg_queue = msg_queue
        self.socket = socket
        self.use_ssl = use_ssl
        self.SenderCompID = sendercompid
        self.TargetCompID = targetcompid
        self.store = store.FileStore(store.STORETYPE_OUT, self.SenderCompID)
        self.log = logging.getLogger('gdtlive.core.engine.%s' % sendercompid)
        self.receiver_thread = receiver        
        self.last_out_time = None        
        
        
    def run(self):
        self.log.debug('thread started')
        while True:
            try:            
                message = self.msg_queue.get()                
                
                # Leállás
                if not message:                            
                    break
                
                # Újraküldési kérelem
                if type(message) == tuple:                                        
                    self._resend(message[0], message[1])                
                    continue
                                
                message.MsgSeqNum = self.store.expected_seqnum
                message.SendingTime = datetime.utcnow()        
                message.SenderCompID = self.SenderCompID
                message.TargetCompID = self.TargetCompID
                                                
                self.log.debug('OUT: %s' % message)
                try:
                    raw_message = fix.encode(message)
                    self.log.debug('OUT: %s' % pprint.pformat(raw_message))
                    self.last_out_time = datetime.utcnow()
                    if self.use_ssl:                                        
                        self.socket.write(raw_message)
                    else:
                        self.socket.sendall(raw_message)                        
                    self.store += message               
                    self.store.processed(message, True)
                except socket.error as exc:
                    if "Broken pipe" in str(exc) or \
                        "bad write entry" in str(exc):
                        self.log.warning('connection loss detected in Sender')
                        if self.receiver_thread:
                            self.receiver_thread._handle_connection_loss()
                        message.processed(False)
                    else:
                        self.log.warning(str(exc))
                except:                    
                    self.log.error(traceback.format_exc())
                    time.sleep(1)
                    message.processed(False)                    
            except:
                self.log.error(traceback.format_exc())
                self.log.error(locals())
                
                
        self.log.info('SENDER THREAD EXIT')
            
            
    def _resend(self, first_msgnum, last_msgnum):
        try:
            if not last_msgnum:
                last_msgnum = self.store.last_processed_seqnum
#            print 'RESEND', first_msgnum, last_msgnum
                        
            for i in xrange(first_msgnum, last_msgnum+1):                                
                # Ha alkalmazás üzenet, vagy Reject, akkor kiküldjük újra
                if type(self.store[i]) not in fix.IGNORE:            
                    #TODO: régen küldött piaci ordereket nem kéne kiküldeni
                    message = copy.copy(self.store[i])                                                                        
                    message.OrigSendingTime = message.SendingTime                
                else:
                    # adminisztrativ üzenetek helyett SeqReset-GapFill-t küldünk
                    #message = SequenceResetMessage(gapfillflag=True)
                    message = fix.SequenceReset(GapFillFlag=True, NewSeqNo=i+1)
                    message.SenderCompID = self.SenderCompID
                    message.TargetCompID = self.TargetCompID
                    message.MsgSeqNum = i
                    
                message.PossDupFlag = True                        
                message.SendingTime = datetime.utcnow()
                raw_message = fix.encode(message)
                self.log.debug('resending: %s' % message)
                self.log.debug('resending: %s' % pprint.pformat(raw_message))
                self.last_out_time = datetime.utcnow()
                if self.use_ssl:
                    self.socket.write(raw_message)
                else:
                    self.socket.sendalll(raw_message)
        except:
            self.log.error('Error occured during resend ')
            self.log.error('locals(): ' + str(locals()))
            self.log.error(traceback.format_exc())            


class FIXEngine(threading.Thread):
    thread_num = 1
    
    def __init__(self, message_handler=None):
        threading.Thread.__init__(self)
        self.name = 'FIXEngine thread'
        
        self.out_message_queue = Queue()
        self.sender_thread = None
        
        self.store = None
                        
        self.in_seq_num = 0                
        self.last_in_time = None                                
        self.socket = None
        
        self._socket_timeout = 1.0
        self.connection_state = STATE_NOT_CONNECTED
        self.SenderCompID = 0
        self.TargetCompID = 0
        self.use_ssl = True
        self.heartbeat_int = 60
        self.heartbeat_tolerance = 10
        self.last_testreqid = 0
        self.login_sent = None
        self.relogin_initiator = False
        self.relogin_initiated = False
        self.invalid_messages = []
        self._shutdown = False
        self.shutdown_reason = 0        
        self.resendrequest_received_count = 0
        
        self.callbacks = {
                      "on_disconnect" : None,
                      "on_connect" : None,
                      "on_logon" : None,
                      "on_logout" : None,                      
                      }
        
        self.events = {
                       'engine_started' : threading.Event(),
                       'session_established' : threading.Event(),
                       'test_request_confirmed' : threading.Event(),
                       'login_succeded' : threading.Event(),
                       'logout_succeded' : threading.Event(),                       
                       }
        
        self.message_handlers = {
                         'application' : self._handle_application_message, 
                         fix.Logon : self._handle_logon,
                         fix.Logout : self._handle_logout,
                         fix.Reject : self._handle_reject,
                         fix.Heartbeat : self._handle_heartbeat,
                         fix.TestRequest : self._handle_testrequest,
                         fix.ResendRequest : self._handle_resendrequest,
                         fix.SequenceReset : self._handle_seqreset_gapfill                                                  
                         }
        
        if message_handler:
            self.message_handlers['application'] = message_handler         


    def _debug(self, log_message):
        self.log.debug(log_message)
        
    def _info(self, log_message):
        self.log.info(log_message)
    
    def _warn(self, log_message):
        self.log.warn(log_message)
    
    def _error(self, log_message):
        self.log.warn(log_message)    
    
    def _critical(self, log_message):
        self.log.critical(log_message)


    def send(self, message):        
        self.out_message_queue.put(message)


    def shutdown(self):
        try:
            self.connection_state = STATE_SHUTDOWN
            self._shutdown = True                        
            self.join()                                   
            self._info('shutdown complete')
        except:
            pass
            

    def run(self):
        if not (self.SenderCompID and self.TargetCompID):
            self.log.error('Configuration error: SenderCompID or TargetCompID is not set')
            return False
        
        if not self.socket:
            self.log.critical('Engine initialization error, socket object not set! ')
            return False
        
        if not self.connection_state == STATE_CONNECTED:
            self.log.error('socket not connected to endpoint, ensure that connect() is succeeded before call this')
            return False
                
        self.store = store.FileStore(store.STORETYPE_IN, self.SenderCompID) 
            
        self.socket.settimeout(self._socket_timeout)        
        
        self.sender_thread = MessageSender(self.out_message_queue, self.socket, self.SenderCompID, self.TargetCompID, self.use_ssl, self)
        self.sender_thread.name = self.name+'-Sender'        
        self.sender_thread.start()        
        self.events['engine_started'].set()
        
        self.read_buffer = ''
        while True:
            try:                        
                data = self._network_handler()
                if not data:    
                    break
                                                          
                self.last_in_time = datetime.utcnow()
                self.read_buffer += data  
                self.log.debug('IN: %s' % pprint.pformat(data))
                                              
                for message in self._decode_messages():                                
                    try:                                        
                        if not self._handle_received_message(message):
                            break
                    except:
                        self.log.error(traceback.format_exc())                    
                                                                                                                                                                              
            
            except socket.error as exc:                
                self.shutdown_reason = ENGINE_SHUTDOWN_INTERNAL_ERROR
                self._terminate_session('Internal error occured, shutting down connection', reconnect=False, send_logout=True)                      
                self.log.error(traceback.format_exc())     
                break
            except Exception as e:        
                self.log.critical(str(type(e)))         
                self.log.critical(e)                    
                self.log.critical(traceback.format_exc())
                self._terminate_session('Internal error occured, shutting down connection', reconnect=False, send_logout=True)          
                self.shutdown_reason = ENGINE_SHUTDOWN_INTERNAL_ERROR   
                break               
                
        self.log.info('READER THREAD EXIT')
        self._close_socket()
        self.out_message_queue.put(None)
        self.sender_thread.join()    
        self.shutdown()                     
        return True


    def _network_handler(self):
        while True:
            try:
                if not self._fix_session_check():
                    break
                
                if self.use_ssl:
                    data = self.socket.read(4096)
                else:
                    data = self.socket.recv(4096)
                                
                if data:                                         
                    return data
                                                                                         
                # megszakadt a kapcsolat                                                                    
                if not self._handle_connection_loss():
                    # ha nem sikerült visszaállítani a kapcsolatot                    
                    # akkor kilépünk
                    self.connection_state = STATE_SHUTDOWN
                    self.shutdown_reason = ENGINE_SHUTDOWN_RECONNECT_FAILED
                    self._error('cant reestablish connection, shutting down the Engine')                                                
                    break
                            
            except socket.error as exc:
                # ha shutdown folyamatban, azonnal kilépünk
                if self.connection_state == STATE_SHUTDOWN or self._shutdown: break                
                                
                try:
                    if "timed out" in exc.args[0]:
                        #print self.host,':', self.port, CONNECTION_STATE[self.connection_state], self.login_sent 
                        if self.connection_state == STATE_LOGON_INITIATED and self.login_sent:
                            if self.login_sent + timedelta(seconds=30) < datetime.utcnow():
                                self._terminate_session('Confirmation Logon not received in time', reconnect = True, send_logout=False)
                        
                    else:
                        self.log.debug(str(exc) + ' args:' + str(exc.args))
                        raise
                except:
                    print traceback.format_exc()                    
                    pass
        return False
        
                    
    def _fix_session_check(self):
        if self.connection_state == STATE_SESSION_ESTABLISHED and self.heartbeat_int > 0:
                                    
            if not self.last_testreqid:
                if datetime.utcnow() - self.last_in_time > timedelta(seconds=self.heartbeat_int + self.heartbeat_tolerance):
                    self.log.info('not receiving any heartbeat messages in %d sec' % (self.heartbeat_int + self.heartbeat_tolerance))
                    self._send_testrequest()                                                                
            else:                    
                if datetime.utcnow() - self.last_in_time > timedelta(seconds=(self.heartbeat_int * 2 + self.heartbeat_tolerance)):
                    self.log.info('not received response to our Test Request, shutting down')
                    is_connected = self._terminate_session('Response not received for TestRequest %s ' % self.last_testreqid, reconnect=True, send_logout=False)
                    return is_connected
            
            if self.sender_thread.last_out_time:
                if datetime.utcnow() - self.sender_thread.last_out_time > timedelta(seconds=self.heartbeat_int):
                    self._send_heartbeat()            
        
        return True        
                
        
    def _decode_messages(self):
        while True:        
            match = re.search(fix.RE_MESSAGE_PATTERN, self.read_buffer, re.M)
            if not match:
                break
                                                            
            raw_message = match.group(0)                    
            self.read_buffer = self.read_buffer[len(raw_message):]

            try:                
                yield fix.decode(raw_message)
            except GeneratorExit:
                pass
            except fix.GarbledMessage as e:
                # ha ez az első, hogy megkapjuk ezt az üzenetet
                if raw_message not in self.invalid_messages:                                    
                    self.invalid_messages.append(raw_message)                    
                    self.log.error('garbled message received ' + str(e))
                    self.log.error(pprint.pformat(raw_message))
            
                else:
                    # ha már másodszor jön ugyanaz, akkor baj van
                    self.log.error('GARBLED MESSAGE RECEIVED SECOND TIME, IGNORING IT ')
                    self.log.error(pprint.pformat(raw_message))  
                    self.store.last_processed_seqnum += 1
            except:
                self.log.error(raw_message)
                self.log.error(traceback.format_exc())                    
                #self._handle_parse_error(raw_message, e) # ez meg kezelje le az else: ág alatt lévő dolgokat !                                                                                                                                                    
    
    
    
    def _handle_received_message(self, message):        
        self.log.debug('IN: %s' % (message))
        message._received_time = datetime.utcnow()
        
#        self.log.debug('%s - %s' % (self.name, self.store))

        if message.MsgSeqNum is None:
            self._terminate_session('MsgSeqNum tag is missing!')
            return False
        
        # Két üzenet esik ki az MsgSeqNum kezelés szabályai alól:
        # A Logon + ResetSeqNumFlag esete, és a SeqReset-Reset
        # Ezeket teljesen külön kell kezelni a többi üzenettől
        if type(message) == fix.Logon and message.ResetSeqNumFlag == True:
            self._handle_logon(message)
            self.store += message
            return True
       
        if type(message) == fix.SequenceReset and not message.GapFillFlag:
            self._handle_seqreset_reset(message)                                                        
            return True                 
        
        if message.MsgSeqNum == self.store.expected_seqnum:            
            #self.log.debug('%s - SeqNum OK' % self.name)
            self.store += message            
                                    
        elif message.MsgSeqNum > self.store.expected_seqnum:
            self.log.warn('Gap detected! %d > %d' % (message.MsgSeqNum, self.store.expected_seqnum))
            self.log.warn('%s' % (str(message)))                        
            
#            if message.MsgType == 'Logon':
#                result = self.message_handlers[message.MsgType](message)                
#                if result:
#                    self.send_resendrequest(self.in_seq_num+1, message.MsgSeqNum-1)
#                    self.store += message
#                    self.in_seq_num = message.MsgSeqNum
#                return True 
#                
#            elif message.MsgType == 'Logout':
#                self.message_handlers[message.MsgType](message)
#                self.store += message
#                self.in_seq_num = message.MsgSeqNum
#                return True
#            
            if type(message) == fix.ResendRequest:                                
                self.message_handlers[message.MsgType](message)
                self._send_resendrequest(self.store.expected_seqnum, 0)    
                message._processed = True            
                self.store += message
                return True 
#            
#            elif message.MsgType == 'Sequence Reset':
#                self._send_resendrequest(self.in_seq_num+1, 0)
#                self.store += message
#                self.in_seq_num = message.MsgSeqNum
#                return True 
#                
            else:                                                      
                self._send_resendrequest(self.store.expected_seqnum, 0)
                self.store += message
                                                        
        
        elif message.MsgSeqNum < self.store.expected_seqnum:
        
            if message.PossDupFlag == True:
                self.log.info('Possible Duplicate message received')
                self.store += message
                                                                                                    
                if type(message) == fix.SequenceReset and message.GapFillFlag == True and message.NewSeqNo:
                    for seq_num in xrange(message.MsgSeqNum, message.NewSeqNo):
                        self.store[seq_num] = message
                        
            else:
                # serious fuckup !
                error_desc = 'MsgSeqNum is too low. expected_seqnum: %d  recived: %d' % (self.store.expected_seqnum, message.MsgSeqNum) 
                self.log.critical('%s' % (error_desc))                
                self.shutdown_reason = ENGINE_SHUTDOWN_LOWERMSGSEQNUM      
                self._terminate_session(error_desc, reconnect=False)           
                return False 
                        
        # kiemeljük a következő feldolgozandó üzenetet
        message = self.store.next_unprocessed()
        while message:        
            try:                                                
                self._debug('Processing message: %d, %s' % (message.MsgSeqNum, message))
                # Ha adminisztrativ üzenet
                if message.__class__ in self.message_handlers:
                    if not message._processed:
                        # ha duplikátum,        
                        if type(message) != fix.SequenceReset and message.PossDupFlag == True:
                            # akkor nem dolgozzuk fel
                            self.log.warn('Admin message with PossDupFlag==True detected, skip processing')                                                        
                        else:
                            # egyébként igen
                            self.message_handlers[message.__class__](message)                        
                # ha nem adminisztrativ üzenet, 
                else:                    
                    # átadjuk az business logic rétegnek
                    self.message_handlers['application'](message)
                    
                self.store.processed(message, True)                                                            
            except:
                self.log.error('Exception occured during message handling')
                self.log.error(traceback.format_exc())
                self.store.processed(message, False)            
            finally:
                message = self.store.next_unprocessed()
                
        return True


    def _handle_logon(self, message):
        return False
    
    def _handle_logout(self, message):
        pass
        
        
    def _handle_reject(self, message):
        self.log.error('Reject message received : %s' % (message))
        
        
    def _handle_heartbeat(self, message):
        self._debug('HeartBeat received')
        if message.TestReqID:
            if message.TestReqID == self.last_testreqid:                            
                self._info('TestReqID found, connection seems OK')
                self.events['test_request_confirmed'].set()
            else:
                self._warn('invalid TestReqID found! sent: %s, received: %s ' % (self.last_testreqid, message.TestReqID))                                        
            self.last_testreqid = None
    
    
    def _handle_testrequest(self, message):
        self._send_heartbeat(message.TestReqID)


    def _handle_resendrequest(self, message):
        self.resendrequest_received_count += 1
        if self.resendrequest_received_count == 3:
            self.resendrequest_received_count = 0
            self._terminate_session('3 ResendRequest received', reconnect = True, send_logout = False)
            return 
        
        self.out_message_queue.put((message.BeginSeqNo, message.EndSeqNo))                    


    def _handle_seqreset_gapfill(self, message):        
        pass
    
    
    def _handle_seqreset_reset(self, message):        
        if message.NewSeqNo >= self.store.expected_seqnum:                
            self.store.expected_seqnum = message.NewSeqNo
            self.store.last_processed_seqnum = message.NewSeqNo - 1 
            
        elif message.NewSeqNo < self.store.expected_seqnum:
            reply = fix.Reject()
            reply.Text = "Value is incorrect (out of range) for this tag"
            reply.SessionRejectReason = "Value is incorrect (out of range) for this tag"
            reply.RefMsgType = 'Sequence Reset'
            reply.RefSeqNum = message.MsgSeqNum
            self.send(reply)
            self.store.expected_seqnum += 1
            self.store.last_processed_seqnum += 1
            
                

    def _handle_application_message(self, message):
        self._warn('unhandled message: %r' % (message))        
    
    
    def _handle_parse_error(self, raw_message, exc):
        self._error('PARSE ERROR:' + str(exc))
        self._error(raw_message)

       
    def _handle_connection_loss(self):
        self.connection_state = STATE_NOT_CONNECTED
        return False
   
      
    def _send_heartbeat(self, testreqid=None):
        self._debug('Sending HeartBeat ')
        self.send(fix.Heartbeat(TestReqID=testreqid))
        
    
    def _send_testrequest(self):        
        self.last_testreqid = str(time.time())
        self._info('Sending Test Request with ID: %s' % (self.last_testreqid))
        self.events['test_request_confirmed'].clear()        
        self.send(fix.TestRequest(TestReqID=self.last_testreqid))                
    
        
    def _send_resendrequest(self, begin, end):
        self.send(fix.ResendRequest(BeginSeqNo=begin, EndSeqNo=end))
        
            
    def _terminate_session(self, error_text, reconnect = True, send_logout=True):
        if send_logout:
            message = fix.Logout(Text=error_text)            
            self.send(message)                
            message.wait_until_processed()            
        if reconnect:            
            self._close_socket()                                
            if self._handle_connection_loss():
                return True
                        
            self.shutdown_reason = ENGINE_SHUTDOWN_RECONNECT_FAILED    
        else:                                                    
            self.connection_state = STATE_SHUTDOWN
        
        return False
    
    
    def _close_socket(self):
        if self.connection_state != STATE_SHUTDOWN:
            self.connection_state = STATE_NOT_CONNECTED
        try:
            self.socket.shutdown(socket.SHUT_RDWR)
        except:
            pass
        try:
            self.socket.close()
        except:
            pass
        
