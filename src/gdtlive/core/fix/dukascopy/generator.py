'''
Created on 2012.06.16.

@author: kulcsarb
'''
import xml.dom.minidom
import pprint

FIELD_TYPES =  {
                'UTCTIMESTAMP': str,
                'UTCDATEONLY': str,
                'COUNTRY': str,
                'CHAR': str,
                'CURRENCY': str,
                'LOCALMKTDATE': str,
                'DATA': str,
                'NUMINGROUP': int,
                'QTY': float,
                'PERCENTAGE': float,
                'AMT': float,
                'STRING': str,
                'EXCHANGE': str,
                'PRICEOFFSET': float,
                'UTCTIMEONLY': str,
                'MULTIPLEVALUESTRING': str,
                'MONTHYEAR': str,
                'SEQNUM': int,
                'PRICE': float,
                'FLOAT': float,
                'INT': int,
                'LENGTH': int,
                'BOOLEAN': str}    


def open_xml(filename):
    with open(filename) as f:
        xml_document = f.read()    
    dom = xml.dom.minidom.parseString(xml_document)
    return dom.documentElement
    
    
def generate_header(root):
    out = 'VERSION = "FIX.%d.%d"\n' % (int(root.getAttribute('major')), int(root.getAttribute('minor')))
    out += 'SOH = "\\x01"\n'        
    return out + '\n'


def generate_header_fields(root):
    
    header_nodes = root.getElementsByTagName('header')[0]
    headers = []
    req = []
    for field in header_nodes.childNodes:
        if field.nodeType != xml.dom.minidom.Node.TEXT_NODE and field.tagName=='field':
            headers.append(str(field.attributes['name'].value))
            if field.attributes['required'].value == 'Y':
                req.append(str(field.attributes['name'].value))
                
    out = 'HEADER = ' + str(headers) + '\n'
    out += 'REQUIRED_HEADER = ' + str(req) + '\n'         
    return out + '\n'


def generate_trailer_fields(root):
    out = 'TRAILER = '
    header_nodes = root.getElementsByTagName('trailer')[0]
    headers = []
    for field in header_nodes.childNodes:
        if field.nodeType != xml.dom.minidom.Node.TEXT_NODE and field.tagName=='field':
            headers.append(str(field.attributes['name'].value))
    out += str(headers) + '\n'        
    return out + '\n'


def generate_fields(root):
    field_nodes = root.getElementsByTagName('fields')[0]
    fields = {}
    types = set()
    #tag_types = {}        
    for field in field_nodes.getElementsByTagName('field'):    
        field_number = int(field.attributes['number'].value)
        field_name = str(field.attributes['name'].value)
        field_type = str(field.attributes['type'].value)            
        fields[field_number] = (field_name, field_type)
        #fields[field_name] = fields[field_number] 
        #tag_types[field_number] = field_type
        types.add(field_type)            

#    type_list = dict(zip(sorted(types), ((None, None, None),) * len(types)))   
#    converter = "ENCODER = 0\nDECODER = 1\nVALIDATOR = 2\n\n"
#    converter += 'CONVERTER = %s\n\n' % pprint.pformat(type_list)            
    
    fields = 'FIELDS = %s\n\n' % pprint.pformat(fields)
    fields += "for field_num in FIELDS.keys(): FIELDS[FIELDS[field_num][0]] = ( field_num, FIELDS[field_num][1] )"
     
    #tag_types = 'TYPES = %s\n\n' % pprint.pformat(tag_types, width=10) 
    
    return fields


def generate_constants(root):
    field_nodes = root.getElementsByTagName('fields')[0]
    values = {}
    constants = []
    for field in field_nodes.getElementsByTagName('field'):    
        field_number = int(field.attributes['number'].value)
        field_name = str(field.attributes['name'].value)
        field_type = str(field.attributes['type'].value)            
        
        if field.childNodes:       
            constants.append('class %s:' % field_name)   
            values[field_name] = {}                          
            for value in field.getElementsByTagName('value'):                
                enum = str(value.attributes['enum'].value)
                if field_type in FIELD_TYPES:
                    enum = FIELD_TYPES[field_type](enum)
                desc = str(value.attributes['description'].value)
                if FIELD_TYPES[field_type] == int:
                    #c = '%s_%s = %d' % (field_name.upper(), desc.upper(), enum)
                    c = '\t%s = %d' % (desc.upper(), enum)
                elif FIELD_TYPES[field_type] == float:
                    #c = '%s_%s = %f' % (field_name.upper(), desc.upper(), enum)
                    c = '\t%s = %f' % (desc.upper(), enum)
                else:
                    #c = '%s_%s = "%s"' % (field_name.upper(), desc.upper(), enum)
                    c = '\t%s = "%s"' % (desc.upper(), enum)
                values[field_name][enum] = desc                         
                constants.append(c)                
            constants.append('')
    constants = '\n'.join(constants) + '\n\n'
    values = 'VALUES = %s\n\n' % pprint.pformat(values, width=10)
    return constants + values



baseclass_template = """import fix
import threading

class Message(object):

    __header__ = HEADER    
    __trailer__ = TRAILER    
    
    def __init__(self, **data):
        global MSGTYPE
#        for field in Message.__header__:
#            self.__dict__[field] = None
#        for field in Message.__trailer__:
#            self.__dict__[field] = None
#        for field in self.__fields__:
#            self.__dict__[field] = None
        
        for f,v in data.items():
            setattr(self, f, v)
        self.MsgType = MSGTYPE[self.__class__]
        self.__dict__['_processed_event'] = threading.Event()
        self.__dict__['_processed'] = False
        self.__dict__['_process_result'] = True   
        self.__dict__['_resend_count'] = 0     
        self.__dict__['_owner'] = None
    
    def encode(self):
        return fix.encode(self)
    
    @staticmethod
    def decode(data):        
        return fix.decode(data)
        
    def is_valid(self):
        return fix.validate(self)

    def __setattr__(self, name, value):
        if (name not in self.__header__ + self.__trailer__ + self.__fields__) \
            and name not in self.__dict__:
            raise KeyError('Field "%s" cannot be assigned to this message' % name)            
        self.__dict__[name] = value
        
        
    
    def __getattr__(self, name):    
        if name in self.__dict__:
            return self.__dict__[name]
        
        if name in self.__header__ + self.__trailer__ + self.__fields__:
            return None
        else:
            raise KeyError        
                    
    def processed(self, result=True):
        self._processed = True
        self._process_result = result
        self._processed_event.set()
        
                                                
    def wait_until_processed(self):        
        self._processed_event.wait()
        return self._process_result
        

"""

class_template = """
class CLASSNAME(Message):
    __fields__ = FIELDS
    __reqfields__ = REQUIRED    
    GROUPS        
    def __init__(self, **data):
        Message.__init__(self, **data)
    
    def __str__(self):
        s = self.__class__.__name__ + '(' 
        s += ','.join(['%s=%s' % (k, v) for k,v in self.__dict__.items() if k[0] != '_' and v is not None])
        s += ')'           
        return s        

"""


groupbase_template = """
class Group(object):
    __fields__ = ()
    __reqfields__ = ()

    def __init__(self, **data):
        for field in self.__fields__:
            self.__dict__[field] = None
        
        for field, value in data.items():            
            if field in self.__dict__:
                self.__dict__[field] = value

"""

group_template = """
    class GROUPNAME(Group):
        __fields__ = FIELDS
        __reqfields__ = REQUIRED

"""


def get_component_fields(component_name):
    global xml_root
    fields = []
    components = xml_root.getElementsByTagName('components')[0]
    for node in components.childNodes:
        if node.nodeType != xml.dom.minidom.Node.TEXT_NODE and node.tagName == 'component':            
            if node.attributes['name'].value == component_name:
                fields = get_message_fields(node)
                break
    return fields 

def get_component_reqfields(component_name):
    global xml_root
    fields = []
    components = xml_root.getElementsByTagName('components')[0]
    for node in components.childNodes:
        if node.nodeType != xml.dom.minidom.Node.TEXT_NODE and node.tagName == 'component':            
            if node.attributes['name'].value == component_name:
                fields = get_message_reqfields(node)
                break
    return fields 
    

def get_message_fields(message_node):
    fields = []
    for field in message_node.childNodes:
        if field.nodeType != xml.dom.minidom.Node.TEXT_NODE:
            if field.tagName == 'field':
                fields.append(str(field.attributes['name'].value))
            if field.tagName == 'group':
                fields.append(str(field.attributes['name'].value))
            if field.tagName == 'component':
                a = get_component_fields(field.attributes['name'].value)
                fields.extend(a)
                
    return fields

def get_message_reqfields(message_node):
    fields = []
    for field in message_node.childNodes:
        if field.nodeType != xml.dom.minidom.Node.TEXT_NODE:
            if field.tagName=='field':
                if field.attributes['required'].value == 'Y':
                    fields.append(str(field.attributes['name'].value))
            if field.tagName=='group':
                if field.attributes['required'].value == 'Y':
                    fields.append(str(field.attributes['name'].value))
            if field.tagName=='component':
                if field.attributes['required'].value == 'Y':                
                    fields.extend(get_component_reqfields(field.attributes['name'].value))
                
    return fields


def get_message_groups(message_node):
    fields = []
    for field in message_node.getElementsByTagName('group'):        
        fields.append(field)
    return fields


def generate_messages(root):
    header_nodes = root.getElementsByTagName('header')[0]
    headers = []
    #req = []
    for field in header_nodes.childNodes:
        if field.nodeType != xml.dom.minidom.Node.TEXT_NODE and field.tagName=='field':
            headers.append(str(field.attributes['name'].value))
            #if field.attributes['required'].value == 'Y':
            #    req.append(str(field.attributes['name'].value))
    
    code = baseclass_template.replace('HEADER', str(tuple(headers)))
    
    code += groupbase_template
    
    trailer_nodes = root.getElementsByTagName('trailer')[0]
    trailers = []
    #req = []
    for field in trailer_nodes.childNodes:
        if field.nodeType != xml.dom.minidom.Node.TEXT_NODE and field.tagName=='field':
            trailers.append(str(field.attributes['name'].value))
            #if field.attributes['required'].value == 'Y':
            #    req.append(str(field.attributes['name'].value))
    
    code = code.replace('TRAILER', str(tuple(trailers)))
    
    messages = root.getElementsByTagName('messages')[0]
    messages = messages.getElementsByTagName('message')
    classes = {}
    for message in messages:        
        classname = str(message.attributes['name'].value)
        msgtype = str(message.attributes['msgtype'].value)
        print ('MESSAGE %s' % classname).center(50,'-') 
        fields = tuple(get_message_fields(message))
        req_fields = tuple(get_message_reqfields(message))
        print 'FIELDS:', fields
        
        groupdef = ''
        groups = get_message_groups(message)
        for group in groups:
            print ' --- GROUP %s --- ' % group.attributes['name'].value
            group_fields = tuple(get_message_fields(group))
            group_req_fields = tuple(get_message_reqfields(group))
            
            gdef = group_template.replace('GROUPNAME', group.attributes['name'].value)
            gdef = gdef.replace('FIELDS', str(group_fields))
            gdef = gdef.replace('REQUIRED', str(group_req_fields))
            groupdef += gdef             
            print group_fields            
                        
        classes[classname] = msgtype        
        classdef = class_template.replace('CLASSNAME',classname)
        classdef = classdef.replace('FIELDS',str(fields))            
        classdef = classdef.replace('REQUIRED',str(req_fields))
        classdef = classdef.replace('GROUPS', groupdef)
        code += classdef
                        
    
    code += "MSGTYPE = {\n" 
    for classname, msgtype in classes.items():
        code += '\t\t%s : "%s",\n' % (classname, msgtype)
    code += "}\n\n"
    code += "for k in MSGTYPE.keys()[:]: MSGTYPE[MSGTYPE[k]] = k\n"
    return code
    
    
def generate_groups(root):
    code = groupbase_template
    
    groups_set = set()
    groups = root.getElementsByTagName('group')
    for group in groups:
        groupname = group.attributes['name'].value
        if groupname not in groups_set:
            groups_set.add(groupname)
            group_fields = get_message_fields(group)
            group_reqfields = get_message_reqfields(group)
            #print groupname
            #print group_fields
            #print group_reqfields
            group_code = group_template.replace('GROUPNAME', groupname[2:])
            group_code = group_code.replace('FIELDS', str(group_fields))
            group_code = group_code.replace('REQUIRED', str(group_reqfields))
            
            #print group_code
            code += group_code            
    
    return code
        

def save(filename, content):
    with open(filename, 'w+') as f:
        f.writelines(content)


if __name__ == '__main__':
    global xml_root
    
    xml_root = open_xml('./DUKAFIX44.xml') 
    
    code = ""
    
    code += generate_header(xml_root)
    code += generate_header_fields(xml_root)    
    code += generate_trailer_fields(xml_root)
    code += generate_fields(xml_root)
        
    #save('./fields.py', code)
    
    #code = generate_constants(xml_root)
    #save('./constants.py', code)

    code = generate_messages(xml_root)
    save('./messages.py', code)
    
    #code = generate_groups(xml_root)
    #save('./groups.py', code)
    
    
    
