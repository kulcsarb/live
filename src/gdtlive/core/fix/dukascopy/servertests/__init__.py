import gdtlive.core.fix.dukascopy.fix as fix
import re
import ssl
import time
import copy
import socket
import logging
import traceback
from datetime import datetime

log = logging.getLogger('gdtlive.tester')
#log.addHandler(logging.StreamHandler())
#log.setLevel(logging.DEBUG)



TRADE_URL = 'demo-api.dukascopy.com' 
TRADE_PORT = 10443
TRADE_SENDERCOMPID = 'DEMO3NxKxd_DEMOFIX'
#TRADE_SENDERCOMPID = 'DEMO3gzrBY_DEMOFIX' 
TRADE_TARGETCOMPID = 'DUKASCOPYFIX'


FEED_URL = 'demo-api.dukascopy.com'
FEED_PORT = 9443
FEED_SENDERCOMPID = 'FEED_DEMO3NxKxd_DEMOFIX'
#FEED_SENDERCOMPID = 'FEED_DEMO3gzrBY_DEMOFIX'
FEED_TARGETCOMPID = 'DUKASCOPYFIX'
 

USERNAME = 'DEMO3NxKxd'
PASSWORD = 'NxKxd'
#USERNAME = 'DEMO3gzrBY'
#PASSWORD = 'gzrBY'


class TestBase:
    def __init__(self, feed=False):
        self.HeartBeatInt = 0        
        if not feed:
            self.URL = TRADE_URL
            self.PORT = TRADE_PORT
            self.SENDERCOMPID = TRADE_SENDERCOMPID
            self.TARGETCOMPID = TRADE_TARGETCOMPID
        else:
            self.URL = FEED_URL
            self.PORT = FEED_PORT
            self.SENDERCOMPID = FEED_SENDERCOMPID
            self.TARGETCOMPID = FEED_TARGETCOMPID
        
    
    def setup(self):        
        
        self.server = ServerTester(self.URL, self.PORT, self.SENDERCOMPID, self.TARGETCOMPID)
        self.server.connect()         
        self.connection_closed = False   
        self.send(fix.Logon(Username=USERNAME, Password=PASSWORD, HeartBtInt=self.HeartBeatInt, ResetSeqNumFlag=True, EncryptMethod=0))                                        
        self.expect(fix.Logon, ('HeartBtInt == HeartBeatInt', 'Username is None','Password is None'), timeout=2)        
        self.expect(fix.TradingSessionStatus, skip=(fix.InstrumentPositionInfo, fix.Heartbeat))    

        
    def teardown(self):
        if not self.connection_closed:             
            self.server.send(fix.Logout())
            self.server.recv()                    
            self.server.disconnect()
        
    
    def send(self, message=None, after=None):
        if message:
            if after: 
                time.sleep(after)
            self.out = message
            self.server.send(message)


    def expect(self, expected, arguments=None, timeout=0, skip=None):        
        if timeout:
            self.server.socket.settimeout(timeout)
        received = None
        try:
            while True:                                
                self.server.recv()
                received = self.server.next()
                if skip:  
                    if type(skip) == tuple: 
                        if any([isinstance(received, sk) for sk in skip]):
                            continue
                    else:                        
                        if isinstance(received, skip):
                            continue
                break
        except ssl.SSLError as e:            
            if not expected:
                return 
            else:
                assert False, 'Timeout'
        finally:
            self.server.socket.settimeout(None)
        
        if not received:
            self.connection_closed = True
            return 
                        
        if type(expected) == tuple:
            assert any([isinstance(received, exp) for exp in expected]), 'Different message received, %s not in %s' % (type(received), expected)
        else:
            assert isinstance(received, expected), 'Different message received! %s != %s' % (type(received), expected)
        print received
        if arguments:                        
            if type(arguments) == str:
                arguments = (arguments, )
            for expression in arguments:     
                #print expression, received.__dict__           
                result = eval(expression, copy.copy(received.__dict__), copy.copy(self.__dict__))
                assert result, expression                
        
        return received

class ServerTester:
    
    def __init__(self, url, port, sendercomp_id, targetcomp_id):
        self.url = url
        self.port = port
        self.sendercomp_id = sendercomp_id
        self.targetcomp_id = targetcomp_id        
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket = ssl.wrap_socket(self.socket)                         
        self.in_message_queue = []
        self.rbuff = ''
        self.out_seq_num = 1        
                
    def connect(self):
        self.socket.connect((self.url, self.port))                    
                    
    def send(self, message):
        message.SenderCompID = self.sendercomp_id
        message.TargetCompID = self.targetcomp_id
        if not message.MsgSeqNum:
            message.MsgSeqNum = self.out_seq_num
        message.SendingTime = datetime.utcnow()
        self.out_seq_num += 1        
        log.info('sending: %r' % message)
        raw_message = fix.encode(message)
        while True:
            #raw_message = '8=FIX.4.4asdadadasd\x0110=123\x01'
            log.debug('sending: %s' % raw_message)             
            sent_bytes = self.socket.write(raw_message)            
            raw_message = raw_message[sent_bytes:]
            if not raw_message:                
                break
        
        
    def recv(self):
        log.debug('reading....')        
        data = self.socket.read()
        if not data:
            log.error('connection dropped')
            return 
        if data == '8':
            data += self.socket.read()
        log.debug('read: %s ' % data)
        self.rbuff += data
        self.process_read_buffer()
        
        
    def process_read_buffer(self):        
        while True:
            match = re.search(fix.RE_MESSAGE_PATTERN, self.rbuff, re.M)
            if not match:
                break
                                                        
            raw_message = match.group(0)                    
            self.rbuff = self.rbuff[len(raw_message):]
            
            try:                
                message = fix.decode(raw_message)
            except fix.GarbledMessage as e:
                log.warn('garbled message %s ' % raw_message)
                log.error(traceback.format_exc())
            else:
                log.info('received: %r' % message)
                self.in_message_queue.append(message)                            
                                                                         
        
        
    def next(self):
        try:
            return self.in_message_queue.pop(0)
        except:
            return None
             
            
    def disconnect(self):
        self.socket.close()
        
        

def setup():
    # module level initialization 
    pass


def teardown():
    # module level deinit
    pass


