# -*- encoding: utf-8 -*- 
'''
Created on Jun 14, 2012

@author: gdtlive
'''
from gdtlive.core.fix.dukascopy.servertests import *
import gdtlive.core.fix.dukascopy.fix as fix
from nose.plugins.attrib import attr
from gdtlive.constants import DIR_SELL, DIR_BUY
import logging

log = logging.getLogger('gdtlive.tester')
log.addHandler(logging.StreamHandler())
log.setLevel(logging.DEBUG)


server = None
connection_closed = False
HEARTBTINT = 0
CURRENT_PRICE = 1.2600



def setup():
    global server, connection_closed, CURRENT_PRICE
    server = ServerTester(TRADE_URL, TRADE_PORT, TRADE_SENDERCOMPID, TRADE_TARGETCOMPID)
    server.connect()   
    connection_closed = False
    
    server.send(fix.Logon(Username=USERNAME, Password=PASSWORD, HeartBtInt=HEARTBTINT, ResetSeqNumFlag=True, EncryptMethod=0))    
    server.recv() #LOGON            
    server.next() #LOGON
    #server.recv() #TRADINGSESSION STATUS
    #server.next() #TRADINGSESSION STATUS
    
    if not CURRENT_PRICE:
        CURRENT_PRICE = get_current_price()           
    
        
def teardown():
    global server, connection_closed
    if not connection_closed:
        server.send(fix.Logout())
        server.recv()
        server.next()                    
        server.disconnect()
        connection_closed = True



def market_order(symbol, direction, amount, client_id):
    return fix.NewOrderSingle(ClOrdID=client_id, 
                                        Symbol=symbol, 
                                        Side={DIR_BUY: fix.Side.BUY, DIR_SELL: fix.Side.SELL}[direction], 
                                        OrderQty=amount,
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.MARKET,
                                        TimeInForce = fix.TimeInForce.FILL_OR_KILL
                                        )


def stop_order(symbol, direction, amount, price, client_id):
    return fix.NewOrderSingle(ClOrdID = client_id,
                                        Symbol = symbol, 
                                        Side = {DIR_BUY: fix.Side.BUY, DIR_SELL: fix.Side.SELL}[direction],
                                        OrderQty = amount,
                                        Price = price,
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.STOP,
                                        TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL
                                        )
    

def limit_order(symbol, direction, amount, price, client_id):
    return fix.NewOrderSingle(ClOrdID = client_id,
                                        Symbol = symbol, 
                                        Side = {DIR_BUY: fix.Side.BUY, DIR_SELL: fix.Side.SELL}[direction],
                                        OrderQty = amount,
                                        Price = price,
                                        TransactTime = datetime.utcnow(),
                                        OrdType = fix.OrdType.STOP_LIMIT,
                                        TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL
                                        )
    


def get_current_price(self):
    global server
    server.send(market_order('EUR/USD', DIR_SELL, self.amount, "2"))
    reply = None
    for i in xrange(5):
        server.recv()
        reply = server.next()
    server.send(market_order('EUR/USD', DIR_BUY, self.amount, "2"))
    for i in xrange(5):
        server.recv()
        server.next()             
    return reply.Price


class TestOtherMessages(TestBase):
    
    def setup(self):
        global server
        self.server = server
        self.HeartBeatInt = HEARTBTINT
    
    def teardown(self):
        pass
                    
    @attr('off')
    def testAccountInfo(self):
        self.send(fix.AccountInfoRequest())
        self.expect(fix.AccountInfo, (
                         'Currency == "USD"',
                         'AccountName == SENDERCOMPID',
                         'TrdType == %d' % fix.TrdType.REGULAR_TRADE, 
                         'Leverage > 0',
                         'Equity > 0',
                         'UsableMargin > 0'
                    ))
                
                
                
    @attr('off')
    def testOrderMassStatusRequest(self):
        '''
        OrderMassStatusRequest
        
        előfeltétel:
             ha nincs nyitott pozició
        teszteset:
            OrderMassStatusRequest üzenet küldése adott azonosítóval
        elvárt viselkedés:
            A szerver TradingSessionStatus üzenetet ad válaszként    
                        
        '''
        
        self.send(fix.OrderMassStatusRequest('REQ'))
        self.expect(fix.TradingSessionStatus)
        


class TestMarkerOrderReject(TestBase):

    def __init__(self):
        TestBase.__init__(self)
        self.amount = 1000
        self.symbol = 'EUR/USD'
        
    def setup(self):
        global server
        self.server = server
        self.HeartBeatInt = HEARTBTINT
    
    
    def teardown(self):
        pass
    
    @attr('off')
    def testAmountTooSmall(self):
        amount = 1
        self.send(market_order(self.symbol, DIR_BUY, amount, "1"))                
        self.out = None        
        
        self.expect(fix.Notification, '"amount should be => 1000" in Text')
        
        self.expect(fix.ExecutionReport, (
                     'ClOrdID == "1"',
                     'OrderQty == 0.0',
                     'CumQty == 0.0',
                     'OrdType == "%s"' % fix.OrdType.MARKET,
                     'TimeInForce == "%s"' % fix.TimeInForce.FILL_OR_KILL,
                     'OrdStatus == "%s"' % fix.OrdStatus.REJECTED,      
                     'Symbol == symbol',
                     'LeavesQty == 0.0',
                     'OrderID is not None',
                     'AvgPx == 0.0',
                     'ExecID is not None',
                     'ExpireTime is None',
                     'OrdRejReason == 99',
                     'Side == "%s"' % fix.Side.UNDISCLOSED,
                     'CashMargin is None',
                     'Slippage is None',
                     'Account is None'
                     ))        

        self.expect(fix.InstrumentPositionInfo, (
                     'Symbol == symbol',
                     'Amount == 0.0',
                     'Price == 0.0',
                     'AccountName == SENDERCOMPID'
                     ))                                
        
    
    @attr('off')
    def testAmountTooBig(self):
        amount = 1000000
        self.send(market_order(self.symbol, DIR_BUY, amount, "1"))                
        self.out = None        
        
        self.expect(fix.Notification, (
                    '"REJECTED: no margin available" in Text',
                    'NotifPriority == 1',
                    'AccountName == SENDERCOMPID'
                    ))
        
        
        self.expect(fix.ExecutionReport, (
                     'ClOrdID == "1"',
                     'OrderQty == 0.0',
                     'CumQty == 0.0',
                     'OrdType == "%s"' % fix.OrdType.MARKET,
                     'TimeInForce == "%s"' % fix.TimeInForce.FILL_OR_KILL,
                     'OrdStatus == "%s"' % fix.OrdStatus.REJECTED,                           
                     'Symbol == symbol',
                     'LeavesQty == 0.0',
                     'OrderID is not None',
                     'AvgPx == 0.0',
                     'ExecID is not None',
                     'ExpireTime is None',
                     'OrdRejReason == %d' % fix.OrdRejReason.ORDER_EXCEEDS_LIMIT,
                     'Side == "%s"' % fix.Side.UNDISCLOSED,
                     'CashMargin is None',
                     'Slippage is None',
                     'Account is None'
                     ))        
        
        self.expect(fix.InstrumentPositionInfo, (
                     'Symbol == symbol',
                     'Amount == 0.0',
                     'Price == 0.0',
                     'AccountName == SENDERCOMPID'
                     ))                                

    

class TestMarketOrderOK(TestBase):    
    
    def __init__(self):
        TestBase.__init__(self)
        self.amount = 1000
        self.symbol = 'EUR/USD'
        
    def setup(self):
        global server
        self.server = server
        self.HeartBeatInt = HEARTBTINT
    
    
    def teardown(self):
        if type(self.out) == fix.NewOrderSingle:
            dir = DIR_SELL if self.out.Side == 'Buy' else DIR_BUY
            close = market_order(self.out.Symbol, dir, self.out.OrderQty, "close")
            self.send(close)
            self.expect(fix.ExecutionReport)
            self.expect(fix.InstrumentPositionInfo)
            self.expect(fix.Notification)
            self.expect(fix.ExecutionReport)
            self.expect(fix.InstrumentPositionInfo)   
        
    
    @attr('off')
    def testBuySuccessfull(self):
        amount = 1000
        self.send(market_order('EUR/USD', DIR_BUY, amount, "1"))
        
        self.expect(fix.ExecutionReport, (
                         'OrderQty == %.1f' % amount,
                         'CumQty == 0',
                         'OrdType == "%s"' % fix.OrdType.MARKET,        
                         'TimeInForce == "%s"' % fix.TimeInForce.FILL_OR_KILL,                                          
                         'ExecType == "%s"' % fix.ExecType.ORDER_STATUS,
                         'OrdStatus == "%s"' % fix.OrdStatus.CALCULATED,
                         'Symbol == "EUR/USD"',
                         'LeavesQty == %.1f' % amount,
                         'Slippage is None',
                         'TransactTime is not None',
                         'ClOrdID == "1"',
                         'OrderID is not None',
                         'AvgPx == 0.0',
                         'Side == "%s"' % fix.Side.BUY,
                         'ExecID is not None',
                         'ExpireTime is None'
                    ))
        self.expect(fix.InstrumentPositionInfo, 'Amount == 0.0')                        
        self.expect(fix.Notification, ('"Order FILLED at" in Text',
                                   'NotifPriority==0',
                                   'AccountName=="%s"' % self.SENDERCOMPID) )
        self.expect(fix.ExecutionReport)
        self.expect(fix.InstrumentPositionInfo, ('Amount == %.1f' % amount,
                                             'Price > 0.0' ))
    
    @attr('off')
    def testSellSuccessfull(self):
        amount = 1000
        self.send(market_order('EUR/USD', DIR_SELL, amount, "2"))
        self.expect(fix.ExecutionReport, ('OrderQty == %.1f' % amount,
                                             'CumQty == 0',
                                             'OrdType == "%s"' % fix.OrdType.MARKET,        
                                             'TimeInForce == "%s"' % fix.TimeInForce.FILL_OR_KILL,     
                                             'ExecType == "%s"' % fix.ExecType.ORDER_STATUS,
                                             'OrdStatus == "%s"' % fix.OrdStatus.CALCULATED,                                                                                     
                                             'Symbol == "EUR/USD"',
                                             'LeavesQty == %.1f' % amount,
                                             'Slippage is None',
                                             'TransactTime is not None',
                                             'ClOrdID == "2"',
                                             'OrderID is not None',
                                             'AvgPx == 0.0',
                                             'Side == "%s"' % fix.Side.SELL,                                             
                                             'ExecID is not None',
                                             'ExpireTime is None'
                                             ))
        self.expect(fix.InstrumentPositionInfo, 'Amount == 0.0')                        
        self.expect(fix.Notification, ('"Order FILLED at" in Text',
                                   'NotifPriority==0',
                                   'AccountName=="%s"' % self.SENDERCOMPID) )
        self.expect(fix.ExecutionReport)
        self.expect(fix.InstrumentPositionInfo, ('Amount == %.1f' % (-1 * amount),
                                             'Price > 0.0' ))
            
        

class TestLimitOrderInstantFill(TestBase):
    def __init__(self):
        TestBase.__init__(self)
        self.amount = 1000
        self.symbol = 'EUR/USD'
        self.close_type = None
                    
    def setup(self):    
        global server
        self.server = server
        self.HeartBeatInt = HEARTBTINT
        
         
    def teardown(self):
        if self.close_type is not None:                    
            close = market_order(self.out.Symbol, self.close_type, self.out.OrderQty, "close")
            self.close_type = None
            self.send(close)
            self.expect(fix.ExecutionReport)
            self.expect(fix.InstrumentPositionInfo)
            self.expect(fix.Notification)
            self.expect(fix.ExecutionReport)
            self.expect(fix.InstrumentPositionInfo)
               

    @attr('off')
    def testPlaceBuyAboveMarket(self):
        assert CURRENT_PRICE
        self.send(limit_order('EUR/USD', DIR_BUY, self.amount, CURRENT_PRICE + 0.1000, "1"))
        self.close_type = DIR_SELL        
        self.expect(fix.Notification)
        self.expect(fix.ExecutionReport,'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW)
        self.expect(fix.InstrumentPositionInfo, 'Amount == 0.0')
        self.expect(fix.ExecutionReport,'OrdStatus == "Calculated"')
        self.expect(fix.Notification)        
        self.expect(fix.ExecutionReport, 'OrdStatus == "%s"' % fix.OrdStatus.FILLED)
        self.expect(fix.InstrumentPositionInfo, 'Amount == %.1f' % self.amount)
            

    @attr('off')
    def testPlaceSellBelowMarket(self):
        assert CURRENT_PRICE
        self.send(limit_order('EUR/USD', DIR_SELL, self.amount, CURRENT_PRICE - 0.1000, "1"))
        self.close_type = DIR_BUY        
        self.expect(fix.Notification)
        self.expect(fix.ExecutionReport,'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW)
        self.expect(fix.InstrumentPositionInfo, 'Amount == 0.0')
        self.expect(fix.ExecutionReport,'OrdStatus == "%s"' % fix.OrdStatus.CALCULATED)
        self.expect(fix.Notification)        
        self.expect(fix.ExecutionReport, 'OrdStatus == "%s"' % fix.OrdStatus.FILLED)
        self.expect(fix.InstrumentPositionInfo, 'Amount == %.1f' % ( -1 *self.amount))

            
            
            
class TestLimitOrderPlaceAndCancel(TestBase):            
                    
    def __init__(self):
        TestBase.__init__(self)
        self.amount = 1000
        self.symbol = 'EUR/USD'
        self.close_type = None
                    
    def setup(self):    
        global server
        self.server = server
        self.HeartBeatInt = HEARTBTINT        
    
    def teardown(self):
        pass
    
    @attr('off')
    def testPlaceBuyBelowMarket(self):
        assert CURRENT_PRICE
        self.send(limit_order('EUR/USD', DIR_BUY, self.amount, CURRENT_PRICE - 0.1000, "1"))                
        self.expect(fix.Notification,'"ACCEPTED" in Text')
        reply = self.expect(fix.ExecutionReport,'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW)
        self.expect(fix.InstrumentPositionInfo, ('Amount == 0.0',
                                             'Symbol == "EUR/USD"'))            
        
        self.send(fix.OrderCancelRequest(reply.Symbol, reply.OrderID, reply.ClOrdID, "2"))
        self.expect(fix.Notification, '"CANCELLED" in Text')
        self.expect(fix.ExecutionReport, ('OrdStatus == "%s"' % fix.OrdStatus.CANCELED,
                                             'OrdType == "%s"' % fix.OrdType.STOP_LIMIT))
        self.expect(fix.InstrumentPositionInfo, ('Symbol == "EUR/USD"',
                                             'Amount == 0.0'))
        
        
    @attr('off')
    def testPlaceSellAboveMarket(self):
        assert CURRENT_PRICE
        self.send(limit_order('EUR/USD', DIR_SELL, self.amount, CURRENT_PRICE + 0.1000, "1"))                
        self.expect(fix.Notification,'"ACCEPTED" in Text')
        reply = self.expect(fix.ExecutionReport,'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW)
        self.expect(fix.InstrumentPositionInfo, ('Amount == 0.0',
                                             'Symbol == "EUR/USD"'))            
        
        self.send(fix.OrderCancelRequest(reply.Symbol, reply.OrderID, reply.ClOrdID, "2"))
        self.expect(fix.Notification, '"CANCELLED" in Text')
        self.expect(fix.ExecutionReport, ('OrdStatus == "%s"' % fix.OrdStatus.CANCELED,
                                             'OrdType == "%s"' % fix.OrdType.STOP_LIMIT))
        self.expect(fix.InstrumentPositionInfo, ('Symbol == "EUR/USD"',
                                             'Amount == 0.0'))
                


class TestStopOrderInstantFill(TestBase):
        
    
    def __init__(self):
        TestBase.__init__(self)
        self.amount = 1000
        self.symbol = 'EUR/USD'
        self.close_type = None        
                    
    def setup(self):
        global server
        self.server = server
        self.HeartBeatInt = HEARTBTINT   

         
    def teardown(self):
        if self.close_type is not None:                    
            close = market_order(self.out.Symbol, self.close_type, self.out.OrderQty, "close")
            self.close_type = None
            self.send(close)
            self.expect(fix.ExecutionReport)
            self.expect(fix.InstrumentPositionInfo)
            self.expect(fix.Notification)
            self.expect(fix.ExecutionReport)
            self.expect(fix.InstrumentPositionInfo)
                                         
        
    @attr('off')
    def testPlaceBuyBelowMarket(self):
        assert CURRENT_PRICE
        self.send(stop_order('EUR/USD', DIR_BUY, self.amount, CURRENT_PRICE - 0.1000, "1"))        
        self.expect(fix.Notification,'"ACCEPTED" in Text')
        self.expect(fix.ExecutionReport,'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW)
        self.expect(fix.InstrumentPositionInfo, 'Amount == 0.0')
        self.expect(fix.ExecutionReport,'OrdStatus == "%s"' % fix.OrdStatus.CALCULATED)
        self.expect(fix.Notification, '"FILLED" in Text') 
        self.close_type = DIR_SELL       
        self.expect(fix.ExecutionReport, 'OrdStatus == "%s"' % fix.OrdStatus.FILLED)
        self.expect(fix.InstrumentPositionInfo, 'Amount == %.1f' % ( self.amount ))
        

        
    @attr('off')
    def testPlaceSellAboveMarket(self):
        assert CURRENT_PRICE
        self.send(stop_order('EUR/USD', DIR_SELL, self.amount, CURRENT_PRICE + 0.1000, "1"))
        self.expect(fix.Notification,'"ACCEPTED" in Text')
        self.expect(fix.ExecutionReport,'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW)
        self.expect(fix.InstrumentPositionInfo, 'Amount == 0.0')
        self.expect(fix.ExecutionReport,'OrdStatus == "%s"' % fix.OrdStatus.CALCULATED)
        self.expect(fix.Notification, '"FILLED" in Text') 
        self.close_type = DIR_BUY       
        self.expect(fix.ExecutionReport, 'OrdStatus == "%s"' % fix.OrdStatus.FILLED)
        self.expect(fix.InstrumentPositionInfo, 'Amount == %.1f' % ( -1 * self.amount ))
        
        
        
class TestStopOrderPlaceAndCancel(TestBase):
        
    
    def __init__(self):
        TestBase.__init__(self)
        self.amount = 1000
        self.symbol = 'EUR/USD'
                
                    
    def setup(self):
        global server
        self.server = server
        self.HeartBeatInt = HEARTBTINT   
        

    def teardown(self):
        pass
    
    
    @attr('off')
    def testPlaceSellBelowMarket(self):        
        assert CURRENT_PRICE
        self.send(stop_order('EUR/USD', DIR_SELL, self.amount, CURRENT_PRICE - 0.1000, "1"))
        self.expect(fix.Notification,'"ACCEPTED" in Text')
        reply = self.expect(fix.ExecutionReport,'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW)
        self.expect(fix.InstrumentPositionInfo, ('Amount == 0.0',
                                             'Symbol == "EUR/USD"'))      
        
        self.send(fix.OrderCancelRequest(reply.Symbol, reply.OrderID, reply.ClOrdID, "2"))
        self.expect(fix.Notification, '"CANCELLED" in Text')
        self.expect(fix.ExecutionReport, ('OrdStatus == "%s"' % fix.OrdStatus.CANCELED,
                                             'OrdType == "%s"' % fix.OrdType.STOP))
        self.expect(fix.InstrumentPositionInfo, ('Symbol == "EUR/USD"',
                                             'Amount == 0.0'))        
        

    #@attr('off')
    def testPlaceBuyAboveMarket(self):
        global server        
        assert CURRENT_PRICE 
        #return                
        self.send(stop_order('EUR/USD', DIR_BUY, self.amount, CURRENT_PRICE + 0.1000, "1"))          
        self.expect(fix.Notification,'"ACCEPTED" in Text')
        reply = self.expect(fix.ExecutionReport,'OrdStatus == "%s"' % fix.OrdStatus.PENDING_NEW)
        self.expect(fix.InstrumentPositionInfo, ('Amount == 0.0',
                                             'Symbol == "EUR/USD"'))      
                
        self.send(fix.OrderCancelRequest(Symbol=reply.Symbol, OrderID=reply.OrderID, ClOrdID="2", OrigClOrdID="1"))
        self.expect(fix.Notification, '"CANCELLED" in Text')
        self.expect(fix.ExecutionReport, ('OrdStatus == "%s"' % fix.OrdStatus.CANCELED,
                                             'OrdType == "%s"' % fix.OrdType.STOP))
        self.expect(fix.InstrumentPositionInfo, ('Symbol == "EUR/USD"',
                                             'Amount == 0.0'))
