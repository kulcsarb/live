# -*- encoding: utf-8 -*- 
'''
Created on Jun 14, 2012

@author: gdtlive
'''
from gdtlive.core.fix.dukascopy.servertests import TestBase, ServerTester, USERNAME, PASSWORD
from nose.plugins.attrib import attr
#from datetime import timedelta
import gdtlive.core.fix.dukascopy.fix as fix


class TestLogonLogout(TestBase):
            
    def setup(self):
        self.HeartBeatInt = 10
        self.server = ServerTester(self.URL, self.PORT, self.SENDERCOMPID, self.TARGETCOMPID)
        self.server.connect()   
                                
                    
    def teardown(self):
        self.server.disconnect()                
    
    #@attr('off')
    def testLogonLogout(self):        
        self.send(fix.Logon(Username=USERNAME, Password=PASSWORD, HeartBtInt=self.HeartBeatInt, ResetSeqNumFlag=True, EncryptMethod=0))                                
        self.expect(fix.Logon, (
                                   'HeartBtInt == HeartBeatInt',
                                   'ResetSeqNumFlag == True',
                                   'EncryptMethod == 0',
                                   'MsgSeqNum == 1',
                                   'Username is None',
                                   'Password is None'), timeout=2)
        self.expect(fix.TradingSessionStatus, skip=fix.InstrumentPositionInfo) 
    
        self.send(fix.Logout())
        
        self.expect(fix.Logout)
        
        
    @attr('off')
    def testReloginWithResetSeqNumTrueMsgnum1(self):
        self.send(fix.Logon(Username=USERNAME, Password=PASSWORD, HeartBtInt=self.HeartBeatInt, ResetSeqNumFlag=True, EncryptMethod=0))                                
        self.expect(fix.Logon, (
                                   'HeartBtInt == HeartBeatInt',
                                   'ResetSeqNumFlag == True',
                                   'EncryptMethod == 0',
                                   'MsgSeqNum == 1',
                                   'Username is None',
                                   'Password is None'), timeout=2)
        self.expect(fix.TradingSessionStatus, skip=fix.InstrumentPositionInfo)
        
        self.send(fix.TestRequest(TestReqID='A'))
        self.expect(fix.Heartbeat, 'TestReqID=="A"')
        
        m = fix.Logon(Username=USERNAME, Password=PASSWORD, HeartBtInt=self.HeartBeatInt, ResetSeqNumFlag=True, EncryptMethod=0)
        m.MsgSeqNum = 1
        self.send(m)
        self.expect(fix.Logon, (
                                   'HeartBtInt == HeartBeatInt',
                                   'ResetSeqNumFlag == True',
                                   'EncryptMethod == 0',
                                   'MsgSeqNum == 1',
                                   'Username is None',
                                   'Password is None'), timeout=2)
        
        
        
    @attr('off')
    def testReloginWithResetSeqNumTrue(self):
        self.send(fix.Logon(Username=USERNAME, Password=PASSWORD, HeartBtInt=self.HeartBeatInt, ResetSeqNumFlag=True, EncryptMethod=0))                                
        self.expect(fix.Logon, (
                                   'HeartBtInt == HeartBeatInt',
                                   'ResetSeqNumFlag == True',
                                   'EncryptMethod == 0',
                                   'MsgSeqNum == 1',
                                   'Username is None',
                                   'Password is None'), timeout=2)
        self.expect(fix.TradingSessionStatus, skip=fix.InstrumentPositionInfo)
        
        self.send(fix.TestRequest(TestReqID='A'))
        self.expect(fix.Heartbeat, 'TestReqID=="A"')
        
        self.send(fix.Logon(Username=USERNAME, Password=PASSWORD, HeartBtInt=self.HeartBeatInt, ResetSeqNumFlag=True, EncryptMethod=0))
        self.expect(fix.Logon, (
                                   'HeartBtInt == HeartBeatInt',
                                   'ResetSeqNumFlag == True',
                                   'EncryptMethod == 0',
                                   'MsgSeqNum == 1',
                                   'Username is None',
                                   'Password is None'), timeout=2)
    

    
    
class TestHeartBeat(TestBase):
    def __init__(self):
        TestBase.__init__(self)
        self.HeartBeatInt = 2
        
    
    @attr('off')
    def testSendTestReqMessage(self):        
        self.send(fix.TestRequest(TestReqID='TEST'))
        self.expect(fix.Heartbeat, ('TestReqID == out.TestReqID', 
                                       'MsgType == "HeartBeat"'))        
                
    @attr('off')
    def testHeartBeatReceive(self):
        self.send(fix.Heartbeat())
        self.expect(fix.Heartbeat, ('TestReqID is None'), timeout=self.HeartBeatInt * 2)
    
        
    @attr('off')
    def testTestReqReceivedAndNoAnswerSent(self):
        self.expect(fix.TestRequest, skip=fix.Heartbeat)
        self.expect(None)
        assert self.connection_closed == True
        
    
    @attr('off')
    def testTestReqReceivedAndAnswerSent(self):
        req = self.expect(fix.TestRequest, skip=fix.Heartbeat)
        self.send(fix.Heartbeat(TestReqID=req.TestReqID))
        self.expect((fix.TestRequest, fix.Heartbeat))
        assert self.connection_closed == False
        
        
        
class TestGapFill(TestBase):
                      

    @attr('off')
    def testResendRequestReceivedSendGapFill1(self):
        self.server.out_seq_num += 1
        self.send(fix.Heartbeat())
        req = self.expect(fix.ResendRequest, (
                                           'BeginSeqNo == 2',
                                           'EndSeqNo == 0 or EndSeqNo == 2'                                           
                                           ))
        
        if req.EndSeqNo == 0:
            self.server.out_seq_num = 2                                   
            self.send(fix.SequenceReset(GapFillFlag=True, NewSeqNo=4))          # resend the two HeartBeat (2-3) in one message             
            self.server.out_seq_num = 4             # send out a test request to ensure there wont be more resendrequest coming from dukas
            self.send(fix.TestRequest(TestReqID='A'))
        
            self.expect(fix.Heartbeat,'TestReqID=="A"')
        else:
            assert req.EndSeqNo != 0, 'Test case not written for this situation.... '
            
                
    @attr('off')
    def testResendRequestReceivedSendGapFill2(self):
        # assuming we have sent a HeartBeat with MsgSeqnum = 2
        self.server.out_seq_num += 1
        # Send out with MsgSeqNum == 3
        self.send(fix.Heartbeat())
        
        req = self.expect(fix.ResendRequest, (
                                           'BeginSeqNo == 2',
                                           'EndSeqNo == 0 or EndSeqNo == 2'                                           
                                           ))
        
        if req.EndSeqNo == 0:
            self.server.out_seq_num = 2
            self.send(fix.SequenceReset(GapFillFlag=True, NewSeqNo=3))                                   
            self.send(fix.SequenceReset(GapFillFlag=True, NewSeqNo=4))             
            self.server.out_seq_num = 4             # send out a test request to ensure there wont be more resendrequest coming from dukas
            self.send(fix.TestRequest(TestReqID='A'))
        
            self.expect(fix.Heartbeat,'TestReqID=="A"')
        else:
            assert req.EndSeqNo != 0, 'Test case not written for this situation.... '
        
        
    @attr('off')
    def testSendResendRequestForOneAdminMessage(self):
        # Requesting resend for the first TradingSessionStatus message received after logon confirm
        self.send(fix.ResendRequest(BeginSeqNo=2, EndSeqNo=0))
        self.expect(fix.SequenceReset, (
                    'GapFillFlag == True',
                    'PossDupFlag == True',
                    'MsgSeqNum == 2',
                    'NewSeqNo == 3',
                    'OrigSendingTime is not None'
                    ))        


        
class TestErrorHandling(TestBase):
    
    @attr('off')
    def testReject1(self):
        self.send(fix.ResendRequest(BeginSeqNo=None, EndSeqNo=0))
        self.expect(fix.Reject, (
                'RefMsgType == "%s"' % fix.MsgType.RESEND_REQUEST, 
                'RefTagID == %d' % fix.FIELDS['BeginSeqNo'][0], 
                'SessionRejectReason == "%s"' % fix.SessionRejectReason.REQUIRED_TAG_MISSING
                ))                
        
    @attr('off')
    def testSendingLowerThanExpectedMsgNum(self):
        self.server.out_seq_num -= 1
        self.send(fix.TestRequest(TestReqID='123'))
        self.expect(fix.Logout, '"MsgSeqNum too low" in Text')
        self.expect(None)
        assert self.connection_closed == True
        
    
    @attr('off')
    def testSeqResetResetWithLowerNewSeqNum1(self):
        s = fix.SequenceReset(NewSeqNo=1)        
        self.send(s)
        self.expect(fix.Reject, ('"Value is incorrect" in Text', 'RefSeqNum == out.MsgSeqNum', 'RefMsgType == out.MsgType'))
    
    @attr('off')
    def testSeqResetResetWithLowerNewSeqNum2(self):
        s = fix.SequenceReset(NewSeqNo=1)
        s.MsgSeqNum = 0        
        self.send(s)
        self.expect(fix.Reject, ('"Value is incorrect" in Text', 'RefSeqNum == out.MsgSeqNum', 'RefMsgType == out.MsgType'))
                
    @attr('off')
    def testSeqResetResetWithLowerNewSeqNum3(self):
        s = fix.SequenceReset(NewSeqNo=1)
        s.MsgSeqNum = 2345        
        self.send(s)
        self.expect(fix.Reject, ('"Value is incorrect" in Text', 'RefSeqNum == out.MsgSeqNum', 'RefMsgType == out.MsgType'))
        
        
    @attr('off')
    def testSeqResetResetWithHigherNewSeqNum1(self):
        s = fix.SequenceReset(NewSeqNo=100)        
        self.send(s)        
        self.server.out_seq_num = 100                
        self.send(fix.TestRequest(TestReqID='A'))
        self.expect(fix.Heartbeat, ('TestReqID == "A"', 'MsgSeqNum==3'))   
        
    @attr('off')
    def testSeqResetResetWithHigherNewSeqNum2(self):
        s = fix.SequenceReset(NewSeqNo=200)      
        s.MsgSeqNum = 0  
        self.send(s)        
        self.server.out_seq_num = 200                
        self.send(fix.TestRequest(TestReqID='B'))
        self.expect(fix.Heartbeat, ('TestReqID == "B"', 'MsgSeqNum==3'))
        
    @attr('off')
    def testSeqResetResetWithHigherNewSeqNum3(self):
        s = fix.SequenceReset(NewSeqNo=300)      
        s.MsgSeqNum = 2345
        self.send(s)        
        self.server.out_seq_num = 300                
        self.send(fix.TestRequest(TestReqID='C'))
        self.expect(fix.Heartbeat, ('TestReqID == "C"', 'MsgSeqNum==3'))
        
