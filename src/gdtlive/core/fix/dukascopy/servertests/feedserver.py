# -*- encoding: utf-8 -*- 
'''
Created on Jun 14, 2012

@author: gdtlive
'''

from gdtlive.core.fix.dukascopy.servertests import TestBase, ServerTester, USERNAME, PASSWORD
from nose.plugins.attrib import attr
import gdtlive.core.fix.dukascopy.fix as fix

class TestFeedServer(TestBase):
    
    def __init__(self):
        TestBase.__init__(self, feed=True)        


    def setup(self):        
        self.server = ServerTester(self.URL, self.PORT, self.SENDERCOMPID, self.TARGETCOMPID)
        self.server.connect()         
        self.connection_closed = False
               
        self.send(fix.Logon(Username=USERNAME, Password=PASSWORD, HeartBtInt=self.HeartBeatInt, ResetSeqNumFlag=True, EncryptMethod=0))                                
        self.expect(fix.Logon, ('HeartBtInt == HeartBeatInt', 'ResetSeqNumFlag == True','EncryptMethod == 0'), timeout=5)

    
    @attr('off')
    def testReceiveQuoteStatusReports(self):
        
        self.expect(fix.QuoteStatusReport, ('Symbol == "USD/RUB"',
                                        'QuoteID is not None', 
                                        'QuoteType in ["Tradeable", "Restricted Tradeable"]'
                                        ))
        self.expect(fix.QuoteStatusReport, ('Symbol == "EUR/TRY"',
                                        'QuoteID is not None', 
                                        'QuoteType in ["Tradeable", "Restricted Tradeable"]'
                                        ))
        self.expect(fix.QuoteStatusReport, ('Symbol == "CAD/HKD"',
                                        'QuoteID is not None', 
                                        'QuoteType in ["Tradeable", "Restricted Tradeable"]',
                                        ))
        
    @attr('off')
    def testMarketDataSnapshot(self):
        self.send(fix.MarketDataRequest(MDReqID='GDT', 
                                           SubscriptionRequestType = fix.SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES,
                                           MarketDepth = 1,
                                           MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                           NoMDEntryTypes = [
                                                             fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                             fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                             ],
                                           NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol='EUR/USD')]
                                           ))
        
        
        self.expect(fix.MarketDataSnapshotFullRefresh, skip=fix.QuoteStatusReport)
        self.expect(fix.MarketDataSnapshotFullRefresh, skip=fix.QuoteStatusReport)
        self.expect(fix.MarketDataSnapshotFullRefresh, skip=fix.QuoteStatusReport)
        
        self.send(fix.MarketDataRequest(MDReqID='GDT', 
                                           SubscriptionRequestType = fix.SubscriptionRequestType.DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST,
                                           MarketDepth = 1,
                                           MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                           NoMDEntryTypes = [
                                                             fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                             fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                             ],
                                           NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol='EUR/USD')]
                                           ))                                                      
                  
        self.expect(None, timeout=5, skip=fix.QuoteStatusReport)
        
        
    #@attr('off')
    def testMarketDataRequestInvalidSymbol(self):
        return       
        symbol = 'EURUSD'          
        self.send(fix.MarketDataRequest(MDReqID='GDT', 
                                           SubscriptionRequestType = fix.SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES,
                                           MarketDepth = 1,
                                           MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                           NoMDEntryTypes = [
                                                             fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                             fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                             ],
                                           NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol=symbol)]
                                           ))
        
        self.expect(fix.MarketDataRequestReject, ('"Invalid Symbol: %s" in Text' % symbol,
                                              'MDReqRejReason == "%d"' % fix.MDReqRejReason.UNKNOWN_SYMBOL),
                    skip=fix.QuoteStatusReport)
        
        
        
        
        
        
        
        
