import fix
from messages import MarketDataSnapshotFullRefresh

if __name__ == '__main__':
    from datetime import datetime
    
    m = MarketDataSnapshotFullRefresh()
    m.SenderCompID = 'a'
    m.TargetCompID = 'b'
    m.MsgSeqNum = 1
    m.SendingTime = datetime.now()
    m.Symbol = 'EUR/USD'
    m.NoMDEntries = [MarketDataSnapshotFullRefresh.NoMDEntries(MDEntryType = fix.MDEntryType.OFFER),
                     MarketDataSnapshotFullRefresh.NoMDEntries(MDEntryType = fix.MDEntryType.BID),
                     ]
    #m.NoMDEntries[0].MDEntryType = constants.MDENTRYTYPE_TRADE
    #print m.is_valid()
    
    print m.encode()
    for k in sorted(m.__dict__): print k, m.__dict__[k],
    print
    
    result = fix.encode(m)
    m = fix.decode(result)
    
    print m.encode()
    for k in sorted(m.__dict__): print k, m.__dict__[k],
    
    #print m.is_valid()
    

