# -*- encoding: utf-8 -*- 
from datetime import datetime, time
from fields import HEADER, REQUIRED_HEADER, TRAILER, FIELDS
from messages import *
from constants import *
import copy

FIX_VERSION = "FIX.4.4"
SOH = "\x01"

ADMIN = set([Logon, Logout, ResendRequest, Heartbeat, TestRequest, SequenceReset, Reject])

IGNORE = ADMIN - set([Reject])


TIMEFMT = '%H:%M:%S'
DATETIMEFMT = '%Y%m%d-%H:%M:%S'
DATEFMT = '%Y%m%d'
MONTHFMT = '%Y%m'
RE_MESSAGE_PATTERN = '(.+?%s=\d\d\d%s)' % ( FIELDS['CheckSum'][0], SOH )


def bool_decode(x):
    return {'Y': True, 'N': False}[x]

def bool_encode(x):
    return {True: 'Y', False: 'N'}[x]

def date_encode(d):
    return d.strftime(DATEFMT)

def date_decode(d):
    return datetime.strptime(d, DATEFMT).date()

def month_encode(d):
    return d.strftime(MONTHFMT)

def month_decode(d):
    return datetime.strptime(d, MONTHFMT).date()

def time_encode(t):    
    return t.strftime(TIMEFMT)

def time_decode(t):
    base, milli = t.split('.')
    t = datetime.strptime(base, TIMEFMT)    
    t = t.replace(microsecond=int(milli) * 1000)
    t = time(t.hour, t.minute, t.second, t.microsecond)
    return t     

def dt_encode(dt):
    return dt.strftime(DATETIMEFMT)

def dt_decode(dt):
    if len(dt) == 17:
        return datetime.strptime(dt, DATETIMEFMT)
    base, milli = dt.split('.')
    dt = datetime.strptime(base, DATETIMEFMT)
    dt = dt.replace(microsecond=int(milli) * 1000)
    return dt

ENCODE = 0
DECODE = 1
VALIDATE = 2

CONVERT = {
 'AMT':                 (str,           float,          None),
 'BOOLEAN':             (bool_encode,   bool_decode,    None),
 'CHAR':                (str,           str,            None),
 'COUNTRY':             (str,           str,            None),
 'CURRENCY':            (str,           str,            None),
 'DATA':                (str,           str,            None),
 'DAYOFMONTHS':         (str,           int,            None),
 'EXCHANGE':            (str,           str,            None),
 'FLOAT':               (str,           float,          None),
 'INT':                 (str,           int,            None),
 'LENGTH':              (str,           int,            None),
 'LOCALMKTDATE':        (date_encode,   date_decode,    None),
 'MONTHYEAR':           (month_encode,  month_decode,   None),
 'MULTIPLEVALUESTRING': (str,           str,            None),
 'NUMINGROUP':          (str,           int,            None),
 'PERCENTAGE':          (str,           float,          None),
 'PRICE':               (str,           float,          None),
 'PRICEOFFSET':         (str,           float,          None),
 'QTY':                 (str,           float,          None),
 'SEQNUM':              (str,           int,            None),
 'STRING':              (str,           str,            None),
 'TAGNUM' :             (str,           int,            None),
 'UTCDATEONLY':         (date_encode,   date_decode,    None),
 'UTCTIMEONLY':         (time_encode,   time_decode,    None),
 'UTCTIMESTAMP':        (dt_encode,     dt_decode,      None)
}


def validate(self):
    data = self.__dict__
    for req_header in REQUIRED_HEADER:
        if req_header not in ['BeginString', 'BodyLength'] \
            and ( req_header not in data or data[req_header] is None):    
            print 'Header missing:', req_header        
            return False
    
    if not validate_body(self):
        return False
    
    return True

def validate_body(self):
    data = self.__dict__
    for field in self.__reqfields__:
        if data.get(field, None) is None:
            print 'field missing:', field
            return False
        if type(data[field]) ==  list:
            for group_obj in data[field]:        
                if not validate_body(group_obj):
                    print 'group', field,'failed'
                    return False 
    return True


 
def format(tag, value):            
    if type(value) == list:
        s = [format(tag, len(value))] 
        for group in value:            
            for f, v in group.__dict__.iteritems():
                if v:
                    s.append(format(f, v))                
        return SOH.join(s)        
    else:        
        tagnum, tagtype = FIELDS[tag]        
        return "%d=%s" % (tagnum, CONVERT[tagtype][ENCODE](value))    


def encode(self):                    
    data = copy.copy(self.__dict__)        
    data.pop('BeginString', None)
    data.pop('BodyLength', None)
    data.pop('CheckSum', None)    
    for attr in data.keys()[:]:
        if attr.startswith('_'):
            del data[attr]
            
    header = [ format('MsgType', data.pop('MsgType')) ]          
    header += [ format(field, data.pop(field)) for field in HEADER if field in data and data[field] is not None]
        
    trailer = [ format(field, data.pop(field)) for field in TRAILER if field in data and data[field] is not None]
    
    body = [ format(field, value) for field, value in data.items() if data[field] is not None]                 
    
    payload = SOH.join( header + body + trailer ) + SOH                   
    message = format('BeginString', FIX_VERSION) + SOH
    message += format('BodyLength', len(payload)) + SOH
    message += payload
                            
    checksum = sum(ord(c) for c in message) & 255
    message += format('CheckSum', '%03i' % checksum) + SOH
    self.BeginString = FIX_VERSION
    self.BodyLength = len(payload)
    self.CheckSum = checksum
    return message

MESSAGE_VALID = 0 
MESSAGE_LAST_SOH_MISSING = 1
MESSAGE_BEGINSTRING_ERROR = 2
MESSAGE_BODYLENGTH_ERROR = 3
MESSAGE_MSGTYPE_ERROR = 4
MESSAGE_CHECKSUM_ERROR = 5

class GarbledMessage(Exception):
    def __init__(self, errno):
        self.errno = errno
        


def decode(data):
    from messages import MSGTYPE    
    tag_list = data.split('\x01')    
    
    if tag_list.pop() != '':        
        raise GarbledMessage(MESSAGE_LAST_SOH_MISSING)
                     
    checksum = tag_list.pop()
    beginstring = tag_list.pop(0)    
    bodylength = tag_list.pop(0)    
    msgtype = tag_list[0]
    
    if beginstring != '%s=%s' % (FIELDS['BeginString'][0], FIX_VERSION):        
        raise GarbledMessage(MESSAGE_BEGINSTRING_ERROR)
            
    my_bodylength = '%s=%d' % ( FIELDS['BodyLength'][0], len(SOH.join(tag_list)+SOH)) 
    if bodylength != my_bodylength:
        raise GarbledMessage(MESSAGE_BODYLENGTH_ERROR)
            
    
    if not msgtype.startswith('%d=' % FIELDS['MsgType'][0]):
        #print 'MsgTYPE error'
        raise GarbledMessage(MESSAGE_MSGTYPE_ERROR)
    
    data, cs_field, cs_num = data.rpartition('%d=' % FIELDS['CheckSum'][0])
    cs = sum(ord(c) for c in data) & 255 
    if cs != int(cs_num[:-1]):
        #print 'CheckSum error', cs, cs_num, raw_message
        raise GarbledMessage(MESSAGE_CHECKSUM_ERROR)            
    
    msg_type = msgtype.partition('=')[2]    
    message = MSGTYPE[msg_type]()    
        
    while tag_list:
        tag_name, value = decode_tag(tag_list.pop(0))
        
        if hasattr(message.__class__, tag_name):            
            value = decode_group(message.__class__, tag_name, tag_list)
            
        setattr(message, tag_name, value)                
        
    return message
    
    
def decode_tag(tag):
    code, eq, value = tag.partition('=')
    field_name, field_type = FIELDS[int(code)]        
    return field_name, CONVERT[field_type][DECODE](value)


def decode_group(message_class, group_name, tag_list):
    groups = []
    group_class = getattr(message_class, group_name)     
    group = group_class()        
    groups.append(group)
    
    while tag_list:
        
        field_name, value = decode_tag(tag_list[0])
        if field_name not in group.__fields__:
            return groups
        
        del tag_list[0]
        
        if getattr(group, field_name):            
            group = group_class()
            groups.append(group)
        
        # if we find an embedded group
        # TODO: need to be tested
        if hasattr(message_class, field_name):
            value = decode_group(message_class, field_name, tag_list) 
            
        setattr(group, field_name, value)
    
    return groups
