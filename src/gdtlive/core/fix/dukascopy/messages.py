#import fix
import constants
import threading

class Message(object):

    __header__ = ('BeginString', 'BodyLength', 'MsgType', 'SenderCompID', 'TargetCompID', 'OnBehalfOfCompID', 'DeliverToCompID', 'SecureDataLen', 'SecureData', 'MsgSeqNum', 'SenderSubID', 'SenderLocationID', 'TargetSubID', 'TargetLocationID', 'OnBehalfOfSubID', 'OnBehalfOfLocationID', 'DeliverToSubID', 'DeliverToLocationID', 'PossDupFlag', 'PossResend', 'SendingTime', 'OrigSendingTime', 'XmlDataLen', 'XmlData', 'MessageEncoding', 'LastMsgSeqNumProcessed')    
    __trailer__ = ('SignatureLength', 'Signature', 'CheckSum')    
    
    def __init__(self, **data):
        global MSGTYPE
#        for field in Message.__header__:
#            self.__dict__[field] = None
#        for field in Message.__trailer__:
#            self.__dict__[field] = None
#        for field in self.__fields__:
#            self.__dict__[field] = None
        
        for f,v in data.items():
            setattr(self, f, v)
            
        self.MsgType = MSGTYPE[self.__class__]
        self.__dict__['_processed_event'] = threading.Event()
        self.__dict__['_processed'] = False
        self.__dict__['_process_result'] = True
        self.__dict__['_resend_count'] = 0        
        self.__dict__['_owner'] = None
        self.__dict__['_received_time'] = None
        
    def encode(self):
        import fix
        return fix.encode(self)
    
    @staticmethod
    def decode(data):        
        import fix
        return fix.decode(data)    
        
    def is_valid(self):
        import fix
        return fix.validate(self)


    def __setattr__(self, name, value):
        if (name not in self.__header__ + self.__trailer__ + self.__fields__) \
            and name not in self.__dict__:
            raise KeyError('Field "%s" cannot be assigned to this message' % name)        
        self.__dict__[name] = value
    
    def __getattr__(self, name):    
        if name in self.__dict__:            
            return self.__dict__[name]
        
        if name in self.__header__ + self.__trailer__ + self.__fields__:
            return None
        else:
            raise KeyError        
                    
    def processed(self, result=True):                
        self._processed = True        
        self._process_result = result        
        self._processed_event.set()
        
        
                                                
    def wait_until_processed(self):                
        self._processed_event.wait()
        return self._process_result
        
    def __str__(self):
        s = self.__class__.__name__ + '('
        tags = []
        for field in sorted(self.__dict__.keys()):
            value = self.__dict__[field]
            if field[0] != '_' and value is not None:                
                if field in constants.VALUES:
                    value = constants.VALUES[field][value]                
                tags.append('%s=%s' % (field, value))
                                
        s += ','.join(tags)
        s += ')'
        return s
    
    def get(self, field):
        if field in self.__dict__:
            value = self.__dict__[field]
            if field[0] != '_' and value is not None:                
                if field in constants.VALUES:
                    value = constants.VALUES[field][value]
                    return value
                    

class Group(object):
    __fields__ = ()
    __reqfields__ = ()

    def __init__(self, **data):
        for field in self.__fields__:
            self.__dict__[field] = None
        
        for field, value in data.items():            
            if field in self.__dict__:
                self.__dict__[field] = value

    def __str__(self):
        s = self.__class__.__name__ + '(' 
        s += ','.join(['%s=%s' % (k, v) for k,v in self.__dict__.items() if k[0] != '_' and v is not None])
        s += ')'           
        return s
    
    def __repr__(self):
        return self.__str__()
     

class Heartbeat(Message):
    __fields__ = ('TestReqID',)
    __reqfields__ = ()    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class Logon(Message):
    __fields__ = ('EncryptMethod', 'HeartBtInt', 'RawDataLength', 'RawData', 'ResetSeqNumFlag', 'NextExpectedMsgSeqNum', 'MaxMessageSize', 'NoMsgTypes', 'TestMessageIndicator', 'Username', 'Password')
    __reqfields__ = ('EncryptMethod', 'HeartBtInt')    
    
    class NoMsgTypes(Group):
        __fields__ = ('RefMsgType', 'MsgDirection')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class TestRequest(Message):
    __fields__ = ('TestReqID',)
    __reqfields__ = ('TestReqID',)    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class ResendRequest(Message):
    __fields__ = ('BeginSeqNo', 'EndSeqNo')
    __reqfields__ = ('BeginSeqNo', 'EndSeqNo')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class Reject(Message):
    __fields__ = ('RefSeqNum', 'RefTagID', 'RefMsgType', 'SessionRejectReason', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('RefSeqNum',)    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class SequenceReset(Message):
    __fields__ = ('GapFillFlag', 'NewSeqNo')
    __reqfields__ = ('NewSeqNo',)    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class Logout(Message):
    __fields__ = ('Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ()    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class BusinessMessageReject(Message):
    __fields__ = ('RefSeqNum', 'RefMsgType', 'BusinessRejectRefID', 'BusinessRejectReason', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('RefMsgType', 'BusinessRejectReason')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class UserRequest(Message):
    __fields__ = ('UserRequestID', 'UserRequestType', 'Username', 'Password', 'NewPassword', 'RawDataLength', 'RawData')
    __reqfields__ = ()    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class UserResponse(Message):
    __fields__ = ('UserRequestID', 'Username', 'UserStatus', 'UserStatusText')
    __reqfields__ = ()    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class Advertisement(Message):
    __fields__ = ('AdvId', 'AdvTransType', 'AdvRefID', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'NoLegs', 'NoUnderlyings', 'AdvSide', 'Quantity', 'QtyType', 'Price', 'Currency', 'TradeDate', 'TransactTime', 'Text', 'EncodedTextLen', 'EncodedText', 'URLLink', 'LastMkt', 'TradingSessionID', 'TradingSessionSubID')
    __reqfields__ = ('AdvId', 'AdvTransType', 'Symbol', 'AdvSide', 'Quantity')    
    
    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ('UnderlyingSymbol',)

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class IndicationOfInterest(Message):
    __fields__ = ('IOIid', 'IOITransType', 'IOIRefID', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'Side', 'QtyType', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'IOIQty', 'Currency', 'NoStipulations', 'NoLegs', 'PriceType', 'Price', 'ValidUntilTime', 'IOIQltyInd', 'IOINaturalFlag', 'NoIOIQualifiers', 'Text', 'EncodedTextLen', 'EncodedText', 'TransactTime', 'URLLink', 'NoRoutingIDs', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType')
    __reqfields__ = ('IOIid', 'IOITransType', 'Symbol', 'Side', 'IOIQty')    
    
    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'LegIOIQty', 'NoLegStipulations')
        __reqfields__ = ()


    class NoIOIQualifiers(Group):
        __fields__ = ('IOIQualifier',)
        __reqfields__ = ()


    class NoRoutingIDs(Group):
        __fields__ = ('RoutingType', 'RoutingID')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class News(Message):
    __fields__ = ('OrigTime', 'Urgency', 'Headline', 'EncodedHeadlineLen', 'EncodedHeadline', 'NoRoutingIDs', 'NoRelatedSym', 'NoLegs', 'NoUnderlyings', 'LinesOfText', 'URLLink', 'RawDataLength', 'RawData')
    __reqfields__ = ('Headline', 'LinesOfText')    
    
    class NoRoutingIDs(Group):
        __fields__ = ('RoutingType', 'RoutingID')
        __reqfields__ = ()


    class NoRelatedSym(Group):
        __fields__ = ('Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class LinesOfText(Group):
        __fields__ = ('Text', 'EncodedTextLen', 'EncodedText')
        __reqfields__ = ('Text',)

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class Email(Message):
    __fields__ = ('EmailThreadID', 'EmailType', 'OrigTime', 'Subject', 'EncodedSubjectLen', 'EncodedSubject', 'NoRoutingIDs', 'NoRelatedSym', 'NoUnderlyings', 'NoLegs', 'OrderID', 'ClOrdID', 'LinesOfText', 'RawDataLength', 'RawData')
    __reqfields__ = ('EmailThreadID', 'EmailType', 'Subject', 'LinesOfText')    
    
    class NoRoutingIDs(Group):
        __fields__ = ('RoutingType', 'RoutingID')
        __reqfields__ = ()


    class NoRelatedSym(Group):
        __fields__ = ('Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()


    class LinesOfText(Group):
        __fields__ = ('Text', 'EncodedTextLen', 'EncodedText')
        __reqfields__ = ('Text',)

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class QuoteRequest(Message):
    __fields__ = ('QuoteReqID', 'RFQReqID', 'ClOrdID', 'OrderCapacity', 'NoRelatedSym', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('QuoteReqID', 'NoRelatedSym')    
    
    class NoRelatedSym(Group):
        __fields__ = ('Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'PrevClosePx', 'QuoteRequestType', 'QuoteType', 'TradingSessionID', 'TradingSessionSubID', 'TradeOriginationDate', 'Side', 'QtyType', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'SettlType', 'SettlDate', 'SettlDate2', 'OrderQty2', 'Currency', 'NoStipulations', 'Account', 'AcctIDSource', 'AccountType', 'NoLegs', 'NoQuoteQualifiers', 'QuotePriceType', 'OrdType', 'ValidUntilTime', 'ExpireTime', 'TransactTime', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'PriceType', 'Price', 'Price2', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType', 'NoPartyIDs')
        __reqfields__ = ('Symbol',)


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'LegQty', 'LegSwapType', 'LegSettlType', 'LegSettlDate', 'NoLegStipulations', 'NoNestedPartyIDs', 'LegBenchmarkCurveCurrency', 'LegBenchmarkCurveName', 'LegBenchmarkCurvePoint', 'LegBenchmarkPrice', 'LegBenchmarkPriceType')
        __reqfields__ = ()


    class NoQuoteQualifiers(Group):
        __fields__ = ('QuoteQualifier',)
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class QuoteResponse(Message):
    __fields__ = ('QuoteRespID', 'QuoteID', 'ClOrdID', 'OrderCapacity', 'IOIid', 'QuoteType', 'NoQuoteQualifiers', 'NoPartyIDs', 'TradingSessionID', 'TradingSessionSubID', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'Side', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'SettlType', 'SettlDate', 'SettlDate2', 'OrderQty2', 'Currency', 'NoStipulations', 'Account', 'AcctIDSource', 'AccountType', 'NoLegs', 'BidPx', 'OfferPx', 'MktBidPx', 'MktOfferPx', 'MinBidSize', 'BidSize', 'MinOfferSize', 'OfferSize', 'ValidUntilTime', 'BidSpotRate', 'OfferSpotRate', 'BidForwardPoints', 'OfferForwardPoints', 'MidPx', 'BidYield', 'MidYield', 'OfferYield', 'TransactTime', 'OrdType', 'BidForwardPoints2', 'OfferForwardPoints2', 'SettlCurrBidFxRate', 'SettlCurrOfferFxRate', 'SettlCurrFxRateCalc', 'Commission', 'CommType', 'CustOrderCapacity', 'ExDestination', 'Text', 'EncodedTextLen', 'EncodedText', 'Price', 'PriceType', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType')
    __reqfields__ = ('QuoteRespID', 'Symbol')    
    
    class NoQuoteQualifiers(Group):
        __fields__ = ('QuoteQualifier',)
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'LegQty', 'LegSwapType', 'LegSettlType', 'LegSettlDate', 'NoLegStipulations', 'NoNestedPartyIDs', 'LegPriceType', 'LegBidPx', 'LegOfferPx', 'LegBenchmarkCurveCurrency', 'LegBenchmarkCurveName', 'LegBenchmarkCurvePoint', 'LegBenchmarkPrice', 'LegBenchmarkPriceType')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class QuoteRequestReject(Message):
    __fields__ = ('QuoteReqID', 'RFQReqID', 'QuoteRequestRejectReason', 'NoRelatedSym', 'NoQuoteQualifiers', 'QuotePriceType', 'OrdType', 'ExpireTime', 'TransactTime', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'PriceType', 'Price', 'Price2', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType', 'NoPartyIDs', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('QuoteReqID', 'QuoteRequestRejectReason', 'NoRelatedSym')    
    
    class NoRelatedSym(Group):
        __fields__ = ('Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'PrevClosePx', 'QuoteRequestType', 'QuoteType', 'TradingSessionID', 'TradingSessionSubID', 'TradeOriginationDate', 'Side', 'QtyType', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'SettlType', 'SettlDate', 'SettlDate2', 'OrderQty2', 'Currency', 'NoStipulations', 'Account', 'AcctIDSource', 'AccountType', 'NoLegs')
        __reqfields__ = ('Symbol',)


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'LegQty', 'LegSwapType', 'LegSettlType', 'LegSettlDate', 'NoLegStipulations', 'NoNestedPartyIDs', 'LegBenchmarkCurveCurrency', 'LegBenchmarkCurveName', 'LegBenchmarkCurvePoint', 'LegBenchmarkPrice', 'LegBenchmarkPriceType')
        __reqfields__ = ()


    class NoQuoteQualifiers(Group):
        __fields__ = ('QuoteQualifier',)
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class RFQRequest(Message):
    __fields__ = ('RFQReqID', 'NoRelatedSym', 'SubscriptionRequestType')
    __reqfields__ = ('RFQReqID', 'NoRelatedSym')    
    
    class NoRelatedSym(Group):
        __fields__ = ('Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'NoUnderlyings', 'NoLegs', 'PrevClosePx', 'QuoteRequestType', 'QuoteType', 'TradingSessionID', 'TradingSessionSubID')
        __reqfields__ = ('Symbol',)


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class Quote(Message):
    __fields__ = ('QuoteReqID', 'QuoteID', 'QuoteRespID', 'QuoteType', 'NoQuoteQualifiers', 'QuoteResponseLevel', 'NoPartyIDs', 'TradingSessionID', 'TradingSessionSubID', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'Side', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'SettlType', 'SettlDate', 'SettlDate2', 'OrderQty2', 'Currency', 'NoStipulations', 'Account', 'AcctIDSource', 'AccountType', 'NoLegs', 'BidPx', 'OfferPx', 'MktBidPx', 'MktOfferPx', 'MinBidSize', 'BidSize', 'MinOfferSize', 'OfferSize', 'ValidUntilTime', 'BidSpotRate', 'OfferSpotRate', 'BidForwardPoints', 'OfferForwardPoints', 'MidPx', 'BidYield', 'MidYield', 'OfferYield', 'TransactTime', 'OrdType', 'BidForwardPoints2', 'OfferForwardPoints2', 'SettlCurrBidFxRate', 'SettlCurrOfferFxRate', 'SettlCurrFxRateCalc', 'CommType', 'Commission', 'CustOrderCapacity', 'ExDestination', 'OrderCapacity', 'PriceType', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('QuoteID', 'Symbol')    
    
    class NoQuoteQualifiers(Group):
        __fields__ = ('QuoteQualifier',)
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'LegQty', 'LegSwapType', 'LegSettlType', 'LegSettlDate', 'NoLegStipulations', 'NoNestedPartyIDs', 'LegPriceType', 'LegBidPx', 'LegOfferPx', 'LegBenchmarkCurveCurrency', 'LegBenchmarkCurveName', 'LegBenchmarkCurvePoint', 'LegBenchmarkPrice', 'LegBenchmarkPriceType')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class QuoteCancel(Message):
    __fields__ = ('QuoteReqID', 'QuoteID', 'QuoteCancelType', 'QuoteResponseLevel', 'NoPartyIDs', 'Account', 'AcctIDSource', 'AccountType', 'TradingSessionID', 'TradingSessionSubID', 'NoQuoteEntries')
    __reqfields__ = ('QuoteID', 'QuoteCancelType')    
    
    class NoQuoteEntries(Group):
        __fields__ = ('Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'NoLegs')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class QuoteStatusRequest(Message):
    __fields__ = ('QuoteStatusReqID', 'QuoteID', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'NoLegs', 'NoPartyIDs', 'Account', 'AcctIDSource', 'AccountType', 'TradingSessionID', 'TradingSessionSubID', 'SubscriptionRequestType')
    __reqfields__ = ('Symbol',)    
    
    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class QuoteStatusReport(Message):
    __fields__ = ('QuoteStatusReqID', 'QuoteReqID', 'QuoteID', 'QuoteRespID', 'QuoteType', 'NoPartyIDs', 'TradingSessionID', 'TradingSessionSubID', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'Side', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'SettlType', 'SettlDate', 'SettlDate2', 'OrderQty2', 'Currency', 'NoStipulations', 'Account', 'AcctIDSource', 'AccountType', 'NoLegs', 'NoQuoteQualifiers', 'ExpireTime', 'Price', 'PriceType', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType', 'BidPx', 'OfferPx', 'MktBidPx', 'MktOfferPx', 'MinBidSize', 'BidSize', 'MinOfferSize', 'OfferSize', 'ValidUntilTime', 'BidSpotRate', 'OfferSpotRate', 'BidForwardPoints', 'OfferForwardPoints', 'MidPx', 'BidYield', 'MidYield', 'OfferYield', 'TransactTime', 'OrdType', 'BidForwardPoints2', 'OfferForwardPoints2', 'SettlCurrBidFxRate', 'SettlCurrOfferFxRate', 'SettlCurrFxRateCalc', 'CommType', 'Commission', 'CustOrderCapacity', 'ExDestination', 'QuoteStatus', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('QuoteID', 'Symbol')    
    
    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'LegQty', 'LegSwapType', 'LegSettlType', 'LegSettlDate', 'NoLegStipulations', 'NoNestedPartyIDs')
        __reqfields__ = ()


    class NoQuoteQualifiers(Group):
        __fields__ = ('QuoteQualifier',)
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class MassQuote(Message):
    __fields__ = ('QuoteReqID', 'QuoteID', 'QuoteType', 'QuoteResponseLevel', 'NoPartyIDs', 'Account', 'AcctIDSource', 'AccountType', 'DefBidSize', 'DefOfferSize', 'NoQuoteSets')
    __reqfields__ = ('QuoteID', 'NoQuoteSets')    
    
    class NoQuoteSets(Group):
        __fields__ = ('QuoteSetID', 'UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips', 'QuoteSetValidUntilTime', 'TotNoQuoteEntries', 'LastFragment', 'NoQuoteEntries')
        __reqfields__ = ('QuoteSetID', 'TotNoQuoteEntries', 'NoQuoteEntries')


    class NoQuoteEntries(Group):
        __fields__ = ('QuoteEntryID', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'NoLegs', 'BidPx', 'OfferPx', 'BidSize', 'OfferSize', 'ValidUntilTime', 'BidSpotRate', 'OfferSpotRate', 'BidForwardPoints', 'OfferForwardPoints', 'MidPx', 'BidYield', 'MidYield', 'OfferYield', 'TransactTime', 'TradingSessionID', 'TradingSessionSubID', 'SettlDate', 'OrdType', 'SettlDate2', 'OrderQty2', 'BidForwardPoints2', 'OfferForwardPoints2', 'Currency')
        __reqfields__ = ('QuoteEntryID',)


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class MassQuoteAcknowledgement(Message):
    __fields__ = ('QuoteReqID', 'QuoteID', 'QuoteStatus', 'QuoteRejectReason', 'QuoteResponseLevel', 'QuoteType', 'NoPartyIDs', 'Account', 'AcctIDSource', 'AccountType', 'Text', 'EncodedTextLen', 'EncodedText', 'NoQuoteSets')
    __reqfields__ = ('QuoteStatus',)    
    
    class NoQuoteSets(Group):
        __fields__ = ('QuoteSetID', 'UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips', 'TotNoQuoteEntries', 'LastFragment', 'NoQuoteEntries')
        __reqfields__ = ()


    class NoQuoteEntries(Group):
        __fields__ = ('QuoteEntryID', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'NoLegs', 'BidPx', 'OfferPx', 'BidSize', 'OfferSize', 'ValidUntilTime', 'BidSpotRate', 'OfferSpotRate', 'BidForwardPoints', 'OfferForwardPoints', 'MidPx', 'BidYield', 'MidYield', 'OfferYield', 'TransactTime', 'TradingSessionID', 'TradingSessionSubID', 'SettlDate', 'OrdType', 'SettlDate2', 'OrderQty2', 'BidForwardPoints2', 'OfferForwardPoints2', 'Currency', 'QuoteEntryRejectReason')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class MarketDataRequest(Message):
    __fields__ = ('MDReqID', 'SubscriptionRequestType', 'MarketDepth', 'MDUpdateType', 'AggregatedBook', 'OpenCloseSettlFlag', 'Scope', 'MDImplicitDelete', 'NoMDEntryTypes', 'NoRelatedSym', 'NoTradingSessions', 'ApplQueueAction', 'ApplQueueMax', 'TimeInForce')
    __reqfields__ = ('MDReqID', 'SubscriptionRequestType', 'MarketDepth', 'NoMDEntryTypes', 'NoRelatedSym')    
    
    class NoMDEntryTypes(Group):
        __fields__ = ('MDEntryType',)
        __reqfields__ = ('MDEntryType',)


    class NoRelatedSym(Group):
        __fields__ = ('Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'NoUnderlyings', 'NoLegs')
        __reqfields__ = ('Symbol',)


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()


    class NoTradingSessions(Group):
        __fields__ = ('TradingSessionID', 'TradingSessionSubID')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class MarketDataSnapshotFullRefresh(Message):
    __fields__ = ('MDReqID', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'NoUnderlyings', 'NoLegs', 'FinancialStatus', 'CorporateAction', 'NetChgPrevDay', 'NoMDEntries', 'ApplQueueDepth', 'ApplQueueResolution')
    __reqfields__ = ('Symbol', 'NoMDEntries')    
    
    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()


    class NoMDEntries(Group):
        __fields__ = ('MDEntryType', 'MDEntryPx', 'Currency', 'MDEntrySize', 'MDEntryDate', 'MDEntryTime', 'TickDirection', 'MDMkt', 'TradingSessionID', 'TradingSessionSubID', 'QuoteCondition', 'TradeCondition', 'MDEntryOriginator', 'LocationID', 'DeskID', 'OpenCloseSettlFlag', 'TimeInForce', 'ExpireDate', 'ExpireTime', 'MinQty', 'ExecInst', 'SellerDays', 'OrderID', 'QuoteEntryID', 'MDEntryBuyer', 'MDEntrySeller', 'NumberOfOrders', 'MDEntryPositionNo', 'Scope', 'PriceDelta', 'Text', 'EncodedTextLen', 'EncodedText')
        __reqfields__ = ('MDEntryType',)

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class MarketDataIncrementalRefresh(Message):
    __fields__ = ('MDReqID', 'NoMDEntries', 'ApplQueueDepth', 'ApplQueueResolution')
    __reqfields__ = ('NoMDEntries',)    
    
    class NoMDEntries(Group):
        __fields__ = ('MDUpdateAction', 'DeleteReason', 'MDEntryType', 'MDEntryID', 'MDEntryRefID', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'NoUnderlyings', 'NoLegs', 'FinancialStatus', 'CorporateAction', 'MDEntryPx', 'Currency', 'MDEntrySize', 'MDEntryDate', 'MDEntryTime', 'TickDirection', 'MDMkt', 'TradingSessionID', 'TradingSessionSubID', 'QuoteCondition', 'TradeCondition', 'MDEntryOriginator', 'LocationID', 'DeskID', 'OpenCloseSettlFlag', 'TimeInForce', 'ExpireDate', 'ExpireTime', 'MinQty', 'ExecInst', 'SellerDays', 'OrderID', 'QuoteEntryID', 'MDEntryBuyer', 'MDEntrySeller', 'NumberOfOrders', 'MDEntryPositionNo', 'Scope', 'PriceDelta', 'NetChgPrevDay', 'Text', 'EncodedTextLen', 'EncodedText')
        __reqfields__ = ('MDUpdateAction',)


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class MarketDataRequestReject(Message):
    __fields__ = ('MDReqID', 'MDReqRejReason', 'NoAltMDSource', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('MDReqID',)    
    
    class NoAltMDSource(Group):
        __fields__ = ('AltMDSourceID',)
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class SecurityDefinitionRequest(Message):
    __fields__ = ('SecurityReqID', 'SecurityRequestType', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'DeliveryForm', 'PctAtRisk', 'NoInstrAttrib', 'NoUnderlyings', 'Currency', 'Text', 'EncodedTextLen', 'EncodedText', 'TradingSessionID', 'TradingSessionSubID', 'NoLegs', 'ExpirationCycle', 'SubscriptionRequestType')
    __reqfields__ = ('SecurityReqID', 'SecurityRequestType')    
    
    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class SecurityDefinition(Message):
    __fields__ = ('SecurityReqID', 'SecurityResponseID', 'SecurityResponseType', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'DeliveryForm', 'PctAtRisk', 'NoInstrAttrib', 'NoUnderlyings', 'Currency', 'TradingSessionID', 'TradingSessionSubID', 'Text', 'EncodedTextLen', 'EncodedText', 'NoLegs', 'ExpirationCycle', 'RoundLot', 'MinTradeVol')
    __reqfields__ = ('SecurityReqID', 'SecurityResponseID', 'SecurityResponseType')    
    
    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class SecurityTypeRequest(Message):
    __fields__ = ('SecurityReqID', 'Text', 'EncodedTextLen', 'EncodedText', 'TradingSessionID', 'TradingSessionSubID', 'Product', 'SecurityType', 'SecuritySubType')
    __reqfields__ = ('SecurityReqID',)    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class SecurityTypes(Message):
    __fields__ = ('SecurityReqID', 'SecurityResponseID', 'SecurityResponseType', 'TotNoSecurityTypes', 'LastFragment', 'NoSecurityTypes', 'Text', 'EncodedTextLen', 'EncodedText', 'TradingSessionID', 'TradingSessionSubID', 'SubscriptionRequestType')
    __reqfields__ = ('SecurityReqID', 'SecurityResponseID', 'SecurityResponseType')    
    
    class NoSecurityTypes(Group):
        __fields__ = ('SecurityType', 'SecuritySubType', 'Product', 'CFICode')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class SecurityListRequest(Message):
    __fields__ = ('SecurityReqID', 'SecurityListRequestType', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'DeliveryForm', 'PctAtRisk', 'NoInstrAttrib', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'NoLegs', 'Currency', 'Text', 'EncodedTextLen', 'EncodedText', 'TradingSessionID', 'TradingSessionSubID', 'SubscriptionRequestType')
    __reqfields__ = ('SecurityReqID', 'SecurityListRequestType')    
    
    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class SecurityList(Message):
    __fields__ = ('SecurityReqID', 'SecurityResponseID', 'SecurityRequestResult', 'TotNoRelatedSym', 'LastFragment', 'NoRelatedSym')
    __reqfields__ = ('SecurityReqID', 'SecurityResponseID', 'SecurityRequestResult')    
    
    class NoRelatedSym(Group):
        __fields__ = ('Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'DeliveryForm', 'PctAtRisk', 'NoInstrAttrib', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'Currency', 'NoStipulations', 'NoLegs', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType', 'RoundLot', 'MinTradeVol', 'TradingSessionID', 'TradingSessionSubID', 'ExpirationCycle', 'Text', 'EncodedTextLen', 'EncodedText')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'LegSwapType', 'LegSettlType', 'NoLegStipulations', 'LegBenchmarkCurveCurrency', 'LegBenchmarkCurveName', 'LegBenchmarkCurvePoint', 'LegBenchmarkPrice', 'LegBenchmarkPriceType')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class DerivativeSecurityListRequest(Message):
    __fields__ = ('SecurityReqID', 'SecurityListRequestType', 'UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips', 'SecuritySubType', 'Currency', 'Text', 'EncodedTextLen', 'EncodedText', 'TradingSessionID', 'TradingSessionSubID', 'SubscriptionRequestType')
    __reqfields__ = ('SecurityReqID', 'SecurityListRequestType')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class DerivativeSecurityList(Message):
    __fields__ = ('SecurityReqID', 'SecurityResponseID', 'SecurityRequestResult', 'UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips', 'TotNoRelatedSym', 'LastFragment', 'NoRelatedSym')
    __reqfields__ = ('SecurityReqID', 'SecurityResponseID', 'SecurityRequestResult')    
    
    class NoRelatedSym(Group):
        __fields__ = ('Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'Currency', 'ExpirationCycle', 'DeliveryForm', 'PctAtRisk', 'NoInstrAttrib', 'NoLegs', 'TradingSessionID', 'TradingSessionSubID', 'Text', 'EncodedTextLen', 'EncodedText')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class SecurityStatusRequest(Message):
    __fields__ = ('SecurityStatusReqID', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'DeliveryForm', 'PctAtRisk', 'NoInstrAttrib', 'NoUnderlyings', 'NoLegs', 'Currency', 'SubscriptionRequestType', 'TradingSessionID', 'TradingSessionSubID')
    __reqfields__ = ('SecurityStatusReqID', 'Symbol', 'SubscriptionRequestType')    
    
    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class SecurityStatus(Message):
    __fields__ = ('SecurityStatusReqID', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'DeliveryForm', 'PctAtRisk', 'NoInstrAttrib', 'NoUnderlyings', 'NoLegs', 'Currency', 'TradingSessionID', 'TradingSessionSubID', 'UnsolicitedIndicator', 'SecurityTradingStatus', 'FinancialStatus', 'CorporateAction', 'HaltReason', 'InViewOfCommon', 'DueToRelated', 'BuyVolume', 'SellVolume', 'HighPx', 'LowPx', 'LastPx', 'TransactTime', 'Adjustment', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('Symbol',)    
    
    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class TradingSessionStatusRequest(Message):
    __fields__ = ('TradSesReqID', 'TradingSessionID', 'TradingSessionSubID', 'TradSesMethod', 'TradSesMode', 'SubscriptionRequestType', 'Account')
    __reqfields__ = ('TradSesReqID', 'SubscriptionRequestType', 'Account')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class TradingSessionStatus(Message):
    __fields__ = ('TradSesReqID', 'TradingSessionID', 'TradingSessionSubID', 'TradSesMethod', 'TradSesMode', 'UnsolicitedIndicator', 'TradSesStatus', 'TradSesStatusRejReason', 'TradSesStartTime', 'TradSesOpenTime', 'TradSesPreCloseTime', 'TradSesCloseTime', 'TradSesEndTime', 'TotalVolumeTraded', 'Text', 'EncodedTextLen', 'EncodedText', 'Account')
    __reqfields__ = ('TradingSessionID', 'TradSesStatus')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class NewOrderSingle(Message):
    __fields__ = ('ClOrdID', 'SecondaryClOrdID', 'ClOrdLinkID', 'NoPartyIDs', 'TradeOriginationDate', 'TradeDate', 'Account', 'AcctIDSource', 'AccountType', 'DayBookingInst', 'BookingUnit', 'PreallocMethod', 'AllocID', 'NoAllocs', 'SettlType', 'SettlDate', 'CashMargin', 'ClearingFeeIndicator', 'HandlInst', 'ExecInst', 'MinQty', 'MaxFloor', 'ExDestination', 'NoTradingSessions', 'ProcessCode', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'PrevClosePx', 'Side', 'LocateReqd', 'TransactTime', 'NoStipulations', 'QtyType', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'OrdType', 'PriceType', 'Price', 'StopPx', 'Slippage', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType', 'Currency', 'ComplianceID', 'SolicitedFlag', 'IOIid', 'QuoteID', 'TimeInForce', 'EffectiveTime', 'ExpireDate', 'ExpireTime', 'GTBookingInst', 'Commission', 'CommType', 'CommCurrency', 'FundRenewWaiv', 'OrderCapacity', 'OrderRestrictions', 'CustOrderCapacity', 'ForexReq', 'SettlCurrency', 'BookingType', 'Text', 'EncodedTextLen', 'EncodedText', 'SettlDate2', 'OrderQty2', 'Price2', 'PositionEffect', 'CoveredOrUncovered', 'MaxShow', 'PegOffsetValue', 'PegMoveType', 'PegOffsetType', 'PegLimitType', 'PegRoundDirection', 'PegScope', 'DiscretionInst', 'DiscretionOffsetValue', 'DiscretionMoveType', 'DiscretionOffsetType', 'DiscretionLimitType', 'DiscretionRoundDirection', 'DiscretionScope', 'TargetStrategy', 'TargetStrategyParameters', 'ParticipationRate', 'CancellationRights', 'MoneyLaunderingStatus', 'RegistID', 'Designation')
    __reqfields__ = ('ClOrdID', 'Symbol', 'Side', 'TransactTime', 'OrdType')    
    
    class NoAllocs(Group):
        __fields__ = ('AllocAccount', 'AllocAcctIDSource', 'AllocSettlCurrency', 'IndividualAllocID', 'NoNestedPartyIDs', 'AllocQty')
        __reqfields__ = ()


    class NoTradingSessions(Group):
        __fields__ = ('TradingSessionID', 'TradingSessionSubID')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class ExecutionReport(Message):
    __fields__ = ('OrderID', 'SecondaryOrderID', 'SecondaryClOrdID', 'SecondaryExecID', 'ClOrdID', 'OrigClOrdID', 'ClOrdLinkID', 'QuoteRespID', 'OrdStatusReqID', 'MassStatusReqID', 'TotNumReports', 'LastRptRequested', 'NoPartyIDs', 'TradeOriginationDate', 'NoContraBrokers', 'ListID', 'CrossID', 'OrigCrossID', 'CrossType', 'ExecID', 'ExecRefID', 'ExecType', 'OrdStatus', 'WorkingIndicator', 'OrdRejReason', 'ExecRestatementReason', 'Account', 'AcctIDSource', 'AccountType', 'DayBookingInst', 'BookingUnit', 'PreallocMethod', 'SettlType', 'SettlDate', 'CashMargin', 'ClearingFeeIndicator', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'Side', 'NoStipulations', 'QtyType', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'OrdType', 'PriceType', 'Price', 'StopPx', 'Slippage', 'PegOffsetValue', 'PegMoveType', 'PegOffsetType', 'PegLimitType', 'PegRoundDirection', 'PegScope', 'DiscretionInst', 'DiscretionOffsetValue', 'DiscretionMoveType', 'DiscretionOffsetType', 'DiscretionLimitType', 'DiscretionRoundDirection', 'DiscretionScope', 'PeggedPrice', 'DiscretionPrice', 'TargetStrategy', 'TargetStrategyParameters', 'ParticipationRate', 'TargetStrategyPerformance', 'Currency', 'ComplianceID', 'SolicitedFlag', 'TimeInForce', 'EffectiveTime', 'ExpireDate', 'ExpireTime', 'ExecInst', 'OrderCapacity', 'OrderRestrictions', 'CustOrderCapacity', 'LastQty', 'UnderlyingLastQty', 'LastPx', 'UnderlyingLastPx', 'LastParPx', 'LastSpotRate', 'LastForwardPoints', 'LastMkt', 'TradingSessionID', 'TradingSessionSubID', 'TimeBracket', 'LastCapacity', 'LeavesQty', 'CumQty', 'AvgPx', 'DayOrderQty', 'DayCumQty', 'DayAvgPx', 'GTBookingInst', 'TradeDate', 'TransactTime', 'ReportToExch', 'Commission', 'CommType', 'CommCurrency', 'FundRenewWaiv', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType', 'GrossTradeAmt', 'NumDaysInterest', 'ExDate', 'AccruedInterestRate', 'AccruedInterestAmt', 'InterestAtMaturity', 'EndAccruedInterestAmt', 'StartCash', 'EndCash', 'TradedFlatSwitch', 'BasisFeatureDate', 'BasisFeaturePrice', 'Concession', 'TotalTakedown', 'NetMoney', 'SettlCurrAmt', 'SettlCurrency', 'SettlCurrFxRate', 'SettlCurrFxRateCalc', 'HandlInst', 'MinQty', 'MaxFloor', 'PositionEffect', 'MaxShow', 'BookingType', 'Text', 'EncodedTextLen', 'EncodedText', 'SettlDate2', 'OrderQty2', 'LastForwardPoints2', 'MultiLegReportingType', 'CancellationRights', 'MoneyLaunderingStatus', 'RegistID', 'Designation', 'TransBkdTime', 'ExecValuationPoint', 'ExecPriceType', 'ExecPriceAdjustment', 'PriorityIndicator', 'PriceImprovement', 'LastLiquidityInd', 'NoContAmts', 'NoLegs', 'CopyMsgIndicator', 'NoMiscFees')
    __reqfields__ = ('OrderID', 'ExecID', 'ExecType', 'OrdStatus', 'Symbol', 'Side', 'LeavesQty', 'CumQty', 'AvgPx')    
    
    class NoContraBrokers(Group):
        __fields__ = ('ContraBroker', 'ContraTrader', 'ContraTradeQty', 'ContraTradeTime', 'ContraLegRefID')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoContAmts(Group):
        __fields__ = ('ContAmtType', 'ContAmtValue', 'ContAmtCurr')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'LegQty', 'LegSwapType', 'NoLegStipulations', 'LegPositionEffect', 'LegCoveredOrUncovered', 'NoNestedPartyIDs', 'LegRefID', 'LegPrice', 'LegSettlType', 'LegSettlDate', 'LegLastPx')
        __reqfields__ = ()


    class NoMiscFees(Group):
        __fields__ = ('MiscFeeAmt', 'MiscFeeCurr', 'MiscFeeType', 'MiscFeeBasis')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class DontKnowTrade(Message):
    __fields__ = ('OrderID', 'SecondaryOrderID', 'ExecID', 'DKReason', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'NoUnderlyings', 'NoLegs', 'Side', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'LastQty', 'LastPx', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('OrderID', 'ExecID', 'DKReason', 'Symbol', 'Side')    
    
    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class OrderCancelReplaceRequest(Message):
    __fields__ = ('OrderID', 'NoPartyIDs', 'TradeOriginationDate', 'TradeDate', 'OrigClOrdID', 'ClOrdID', 'SecondaryClOrdID', 'ClOrdLinkID', 'ListID', 'OrigOrdModTime', 'Account', 'AcctIDSource', 'AccountType', 'DayBookingInst', 'BookingUnit', 'PreallocMethod', 'AllocID', 'NoAllocs', 'SettlType', 'SettlDate', 'CashMargin', 'ClearingFeeIndicator', 'HandlInst', 'ExecInst', 'MinQty', 'MaxFloor', 'ExDestination', 'NoTradingSessions', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'Side', 'TransactTime', 'QtyType', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'OrdType', 'PriceType', 'Price', 'StopPx', 'Slippage', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType', 'PegOffsetValue', 'PegMoveType', 'PegOffsetType', 'PegLimitType', 'PegRoundDirection', 'PegScope', 'DiscretionInst', 'DiscretionOffsetValue', 'DiscretionMoveType', 'DiscretionOffsetType', 'DiscretionLimitType', 'DiscretionRoundDirection', 'DiscretionScope', 'TargetStrategy', 'TargetStrategyParameters', 'ParticipationRate', 'ComplianceID', 'SolicitedFlag', 'Currency', 'TimeInForce', 'EffectiveTime', 'ExpireDate', 'ExpireTime', 'GTBookingInst', 'Commission', 'CommType', 'CommCurrency', 'FundRenewWaiv', 'OrderCapacity', 'OrderRestrictions', 'CustOrderCapacity', 'ForexReq', 'SettlCurrency', 'BookingType', 'Text', 'EncodedTextLen', 'EncodedText', 'SettlDate2', 'OrderQty2', 'Price2', 'PositionEffect', 'CoveredOrUncovered', 'MaxShow', 'LocateReqd', 'CancellationRights', 'MoneyLaunderingStatus', 'RegistID', 'Designation')
    __reqfields__ = ('OrigClOrdID', 'ClOrdID', 'Symbol', 'OrdType')    
    
    class NoAllocs(Group):
        __fields__ = ('AllocAccount', 'AllocAcctIDSource', 'AllocSettlCurrency', 'IndividualAllocID', 'NoNestedPartyIDs', 'AllocQty')
        __reqfields__ = ()


    class NoTradingSessions(Group):
        __fields__ = ('TradingSessionID', 'TradingSessionSubID')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class OrderCancelRequest(Message):
    __fields__ = ('OrigClOrdID', 'OrderID', 'ClOrdID', 'SecondaryClOrdID', 'ClOrdLinkID', 'ListID', 'OrigOrdModTime', 'Account', 'AcctIDSource', 'AccountType', 'NoPartyIDs', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'Side', 'TransactTime', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'ComplianceID', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('OrigClOrdID', 'ClOrdID', 'Symbol')    
    
    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class OrderCancelReject(Message):
    __fields__ = ('OrderID', 'SecondaryOrderID', 'SecondaryClOrdID', 'ClOrdID', 'ClOrdLinkID', 'OrigClOrdID', 'OrdStatus', 'WorkingIndicator', 'OrigOrdModTime', 'ListID', 'Account', 'AcctIDSource', 'AccountType', 'TradeOriginationDate', 'TradeDate', 'TransactTime', 'CxlRejResponseTo', 'CxlRejReason', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('OrderID', 'ClOrdID', 'OrigClOrdID', 'OrdStatus', 'CxlRejResponseTo')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class OrderStatusRequest(Message):
    __fields__ = ('OrderID', 'ClOrdID', 'SecondaryClOrdID', 'ClOrdLinkID', 'NoPartyIDs', 'OrdStatusReqID', 'Account', 'AcctIDSource', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'Side')
    __reqfields__ = ('ClOrdID', 'Symbol', 'Side')    
    
    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class OrderMassCancelRequest(Message):
    __fields__ = ('ClOrdID', 'SecondaryClOrdID', 'MassCancelRequestType', 'TradingSessionID', 'TradingSessionSubID', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips', 'Side', 'TransactTime', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('ClOrdID', 'MassCancelRequestType', 'TransactTime')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class OrderMassCancelReport(Message):
    __fields__ = ('ClOrdID', 'SecondaryClOrdID', 'OrderID', 'SecondaryOrderID', 'MassCancelRequestType', 'MassCancelResponse', 'MassCancelRejectReason', 'TotalAffectedOrders', 'NoAffectedOrders', 'TradingSessionID', 'TradingSessionSubID', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips', 'Side', 'TransactTime', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('OrderID', 'MassCancelRequestType', 'MassCancelResponse')    
    
    class NoAffectedOrders(Group):
        __fields__ = ('OrigClOrdID', 'AffectedOrderID', 'AffectedSecondaryOrderID')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class OrderMassStatusRequest(Message):
    __fields__ = ('MassStatusReqID', 'MassStatusReqType', 'NoPartyIDs', 'Account', 'AcctIDSource', 'TradingSessionID', 'TradingSessionSubID', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips', 'Side')
    __reqfields__ = ('MassStatusReqID', 'MassStatusReqType')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class NewOrderCross(Message):
    __fields__ = ('CrossID', 'CrossType', 'CrossPrioritization', 'NoSides', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'NoUnderlyings', 'NoLegs', 'SettlType', 'SettlDate', 'HandlInst', 'ExecInst', 'MinQty', 'MaxFloor', 'ExDestination', 'NoTradingSessions', 'ProcessCode', 'PrevClosePx', 'LocateReqd', 'TransactTime', 'NoStipulations', 'OrdType', 'PriceType', 'Price', 'StopPx', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType', 'Currency', 'ComplianceID', 'IOIid', 'QuoteID', 'TimeInForce', 'EffectiveTime', 'ExpireDate', 'ExpireTime', 'GTBookingInst', 'MaxShow', 'PegOffsetValue', 'PegMoveType', 'PegOffsetType', 'PegLimitType', 'PegRoundDirection', 'PegScope', 'DiscretionInst', 'DiscretionOffsetValue', 'DiscretionMoveType', 'DiscretionOffsetType', 'DiscretionLimitType', 'DiscretionRoundDirection', 'DiscretionScope', 'TargetStrategy', 'TargetStrategyParameters', 'ParticipationRate', 'CancellationRights', 'MoneyLaunderingStatus', 'RegistID', 'Designation')
    __reqfields__ = ('CrossID', 'CrossType', 'CrossPrioritization', 'NoSides', 'Symbol', 'TransactTime', 'OrdType')    
    
    class NoSides(Group):
        __fields__ = ('Side', 'ClOrdID', 'SecondaryClOrdID', 'ClOrdLinkID', 'NoPartyIDs', 'TradeOriginationDate', 'TradeDate', 'Account', 'AcctIDSource', 'AccountType', 'DayBookingInst', 'BookingUnit', 'PreallocMethod', 'AllocID', 'NoAllocs', 'QtyType', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'Commission', 'CommType', 'CommCurrency', 'FundRenewWaiv', 'OrderCapacity', 'OrderRestrictions', 'CustOrderCapacity', 'ForexReq', 'SettlCurrency', 'BookingType', 'Text', 'EncodedTextLen', 'EncodedText', 'PositionEffect', 'CoveredOrUncovered', 'CashMargin', 'ClearingFeeIndicator', 'SolicitedFlag', 'SideComplianceID')
        __reqfields__ = ('Side', 'ClOrdID')


    class NoAllocs(Group):
        __fields__ = ('AllocAccount', 'AllocAcctIDSource', 'AllocSettlCurrency', 'IndividualAllocID', 'NoNestedPartyIDs', 'AllocQty')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()


    class NoTradingSessions(Group):
        __fields__ = ('TradingSessionID', 'TradingSessionSubID')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class CrossOrderCancelReplaceRequest(Message):
    __fields__ = ('OrderID', 'CrossID', 'OrigCrossID', 'CrossType', 'CrossPrioritization', 'NoSides', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'NoUnderlyings', 'NoLegs', 'SettlType', 'SettlDate', 'HandlInst', 'ExecInst', 'MinQty', 'MaxFloor', 'ExDestination', 'NoTradingSessions', 'ProcessCode', 'PrevClosePx', 'LocateReqd', 'TransactTime', 'NoStipulations', 'OrdType', 'PriceType', 'Price', 'StopPx', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType', 'Currency', 'ComplianceID', 'IOIid', 'QuoteID', 'TimeInForce', 'EffectiveTime', 'ExpireDate', 'ExpireTime', 'GTBookingInst', 'MaxShow', 'PegOffsetValue', 'PegMoveType', 'PegOffsetType', 'PegLimitType', 'PegRoundDirection', 'PegScope', 'DiscretionInst', 'DiscretionOffsetValue', 'DiscretionMoveType', 'DiscretionOffsetType', 'DiscretionLimitType', 'DiscretionRoundDirection', 'DiscretionScope', 'TargetStrategy', 'TargetStrategyParameters', 'ParticipationRate', 'CancellationRights', 'MoneyLaunderingStatus', 'RegistID', 'Designation')
    __reqfields__ = ('CrossID', 'OrigCrossID', 'CrossType', 'CrossPrioritization', 'NoSides', 'Symbol', 'TransactTime', 'OrdType')    
    
    class NoSides(Group):
        __fields__ = ('Side', 'OrigClOrdID', 'ClOrdID', 'SecondaryClOrdID', 'ClOrdLinkID', 'OrigOrdModTime', 'NoPartyIDs', 'TradeOriginationDate', 'TradeDate', 'Account', 'AcctIDSource', 'AccountType', 'DayBookingInst', 'BookingUnit', 'PreallocMethod', 'AllocID', 'NoAllocs', 'QtyType', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'Commission', 'CommType', 'CommCurrency', 'FundRenewWaiv', 'OrderCapacity', 'OrderRestrictions', 'CustOrderCapacity', 'ForexReq', 'SettlCurrency', 'BookingType', 'Text', 'EncodedTextLen', 'EncodedText', 'PositionEffect', 'CoveredOrUncovered', 'CashMargin', 'ClearingFeeIndicator', 'SolicitedFlag', 'SideComplianceID')
        __reqfields__ = ('Side', 'OrigClOrdID', 'ClOrdID')


    class NoAllocs(Group):
        __fields__ = ('AllocAccount', 'AllocAcctIDSource', 'AllocSettlCurrency', 'IndividualAllocID', 'NoNestedPartyIDs', 'AllocQty')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()


    class NoTradingSessions(Group):
        __fields__ = ('TradingSessionID', 'TradingSessionSubID')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class CrossOrderCancelRequest(Message):
    __fields__ = ('OrderID', 'CrossID', 'OrigCrossID', 'CrossType', 'CrossPrioritization', 'NoSides', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'NoUnderlyings', 'NoLegs', 'TransactTime')
    __reqfields__ = ('CrossID', 'OrigCrossID', 'CrossType', 'CrossPrioritization', 'NoSides', 'Symbol', 'TransactTime')    
    
    class NoSides(Group):
        __fields__ = ('Side', 'OrigClOrdID', 'ClOrdID', 'SecondaryClOrdID', 'ClOrdLinkID', 'OrigOrdModTime', 'NoPartyIDs', 'TradeOriginationDate', 'TradeDate', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'ComplianceID', 'Text', 'EncodedTextLen', 'EncodedText')
        __reqfields__ = ('Side', 'OrigClOrdID', 'ClOrdID')


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class NewOrderMultileg(Message):
    __fields__ = ('ClOrdID', 'SecondaryClOrdID', 'ClOrdLinkID', 'NoPartyIDs', 'TradeOriginationDate', 'TradeDate', 'Account', 'AcctIDSource', 'AccountType', 'DayBookingInst', 'BookingUnit', 'PreallocMethod', 'AllocID', 'NoAllocs', 'SettlType', 'SettlDate', 'CashMargin', 'ClearingFeeIndicator', 'HandlInst', 'ExecInst', 'MinQty', 'MaxFloor', 'ExDestination', 'NoTradingSessions', 'ProcessCode', 'Side', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'NoUnderlyings', 'PrevClosePx', 'NoLegs', 'LocateReqd', 'TransactTime', 'QtyType', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'OrdType', 'PriceType', 'Price', 'StopPx', 'Currency', 'ComplianceID', 'SolicitedFlag', 'IOIid', 'QuoteID', 'TimeInForce', 'EffectiveTime', 'ExpireDate', 'ExpireTime', 'GTBookingInst', 'Commission', 'CommType', 'CommCurrency', 'FundRenewWaiv', 'OrderCapacity', 'OrderRestrictions', 'CustOrderCapacity', 'ForexReq', 'SettlCurrency', 'BookingType', 'Text', 'EncodedTextLen', 'EncodedText', 'PositionEffect', 'CoveredOrUncovered', 'MaxShow', 'PegOffsetValue', 'PegMoveType', 'PegOffsetType', 'PegLimitType', 'PegRoundDirection', 'PegScope', 'DiscretionInst', 'DiscretionOffsetValue', 'DiscretionMoveType', 'DiscretionOffsetType', 'DiscretionLimitType', 'DiscretionRoundDirection', 'DiscretionScope', 'TargetStrategy', 'TargetStrategyParameters', 'ParticipationRate', 'CancellationRights', 'MoneyLaunderingStatus', 'RegistID', 'Designation', 'MultiLegRptTypeReq')
    __reqfields__ = ('ClOrdID', 'Side', 'Symbol', 'NoLegs', 'TransactTime', 'OrdType')    
    
    class NoAllocs(Group):
        __fields__ = ('AllocAccount', 'AllocAcctIDSource', 'AllocSettlCurrency', 'IndividualAllocID', 'NoNested3PartyIDs', 'AllocQty')
        __reqfields__ = ()


    class NoTradingSessions(Group):
        __fields__ = ('TradingSessionID', 'TradingSessionSubID')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'LegQty', 'LegSwapType', 'NoLegStipulations', 'NoLegAllocs', 'LegPositionEffect', 'LegCoveredOrUncovered', 'NoNestedPartyIDs', 'LegRefID', 'LegPrice', 'LegSettlType', 'LegSettlDate')
        __reqfields__ = ()


    class NoLegAllocs(Group):
        __fields__ = ('LegAllocAccount', 'LegIndividualAllocID', 'NoNested2PartyIDs', 'LegAllocQty', 'LegAllocAcctIDSource', 'LegSettlCurrency')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class MultilegOrderCancelReplaceRequest(Message):
    __fields__ = ('OrderID', 'OrigClOrdID', 'ClOrdID', 'SecondaryClOrdID', 'ClOrdLinkID', 'OrigOrdModTime', 'NoPartyIDs', 'TradeOriginationDate', 'TradeDate', 'Account', 'AcctIDSource', 'AccountType', 'DayBookingInst', 'BookingUnit', 'PreallocMethod', 'AllocID', 'NoAllocs', 'SettlType', 'SettlDate', 'CashMargin', 'ClearingFeeIndicator', 'HandlInst', 'ExecInst', 'MinQty', 'MaxFloor', 'ExDestination', 'NoTradingSessions', 'ProcessCode', 'Side', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'NoUnderlyings', 'PrevClosePx', 'NoLegs', 'LocateReqd', 'TransactTime', 'QtyType', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'OrdType', 'PriceType', 'Price', 'StopPx', 'Currency', 'ComplianceID', 'SolicitedFlag', 'IOIid', 'QuoteID', 'TimeInForce', 'EffectiveTime', 'ExpireDate', 'ExpireTime', 'GTBookingInst', 'Commission', 'CommType', 'CommCurrency', 'FundRenewWaiv', 'OrderCapacity', 'OrderRestrictions', 'CustOrderCapacity', 'ForexReq', 'SettlCurrency', 'BookingType', 'Text', 'EncodedTextLen', 'EncodedText', 'PositionEffect', 'CoveredOrUncovered', 'MaxShow', 'PegOffsetValue', 'PegMoveType', 'PegOffsetType', 'PegLimitType', 'PegRoundDirection', 'PegScope', 'DiscretionInst', 'DiscretionOffsetValue', 'DiscretionMoveType', 'DiscretionOffsetType', 'DiscretionLimitType', 'DiscretionRoundDirection', 'DiscretionScope', 'TargetStrategy', 'TargetStrategyParameters', 'ParticipationRate', 'CancellationRights', 'MoneyLaunderingStatus', 'RegistID', 'Designation', 'MultiLegRptTypeReq')
    __reqfields__ = ('OrigClOrdID', 'ClOrdID', 'Side', 'Symbol', 'NoLegs', 'TransactTime', 'OrdType')    
    
    class NoAllocs(Group):
        __fields__ = ('AllocAccount', 'AllocAcctIDSource', 'AllocSettlCurrency', 'IndividualAllocID', 'NoNested3PartyIDs', 'AllocQty')
        __reqfields__ = ()


    class NoTradingSessions(Group):
        __fields__ = ('TradingSessionID', 'TradingSessionSubID')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'LegQty', 'LegSwapType', 'NoLegStipulations', 'NoLegAllocs', 'LegPositionEffect', 'LegCoveredOrUncovered', 'NoNestedPartyIDs', 'LegRefID', 'LegPrice', 'LegSettlType', 'LegSettlDate')
        __reqfields__ = ()


    class NoLegAllocs(Group):
        __fields__ = ('LegAllocAccount', 'LegIndividualAllocID', 'NoNested2PartyIDs', 'LegAllocQty', 'LegAllocAcctIDSource', 'LegSettlCurrency')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class BidRequest(Message):
    __fields__ = ('BidID', 'ClientBidID', 'BidRequestTransType', 'ListName', 'TotNoRelatedSym', 'BidType', 'NumTickets', 'Currency', 'SideValue1', 'SideValue2', 'NoBidDescriptors', 'NoBidComponents', 'LiquidityIndType', 'WtAverageLiquidity', 'ExchangeForPhysical', 'OutMainCntryUIndex', 'CrossPercent', 'ProgRptReqs', 'ProgPeriodInterval', 'IncTaxInd', 'ForexReq', 'NumBidders', 'TradeDate', 'BidTradeType', 'BasisPxType', 'StrikeTime', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('ClientBidID', 'BidRequestTransType', 'TotNoRelatedSym', 'BidType', 'BidTradeType', 'BasisPxType')    
    
    class NoBidDescriptors(Group):
        __fields__ = ('BidDescriptorType', 'BidDescriptor', 'SideValueInd', 'LiquidityValue', 'LiquidityNumSecurities', 'LiquidityPctLow', 'LiquidityPctHigh', 'EFPTrackingError', 'FairValue', 'OutsideIndexPct', 'ValueOfFutures')
        __reqfields__ = ()


    class NoBidComponents(Group):
        __fields__ = ('ListID', 'Side', 'TradingSessionID', 'TradingSessionSubID', 'NetGrossInd', 'SettlType', 'SettlDate', 'Account', 'AcctIDSource')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class BidResponse(Message):
    __fields__ = ('BidID', 'ClientBidID', 'NoBidComponents')
    __reqfields__ = ('NoBidComponents',)    
    
    class NoBidComponents(Group):
        __fields__ = ('Commission', 'CommType', 'CommCurrency', 'FundRenewWaiv', 'ListID', 'Country', 'Side', 'Price', 'PriceType', 'FairValue', 'NetGrossInd', 'SettlType', 'SettlDate', 'TradingSessionID', 'TradingSessionSubID', 'Text', 'EncodedTextLen', 'EncodedText')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class NewOrderList(Message):
    __fields__ = ('ListID', 'BidID', 'ClientBidID', 'ProgRptReqs', 'BidType', 'ProgPeriodInterval', 'CancellationRights', 'MoneyLaunderingStatus', 'RegistID', 'ListExecInstType', 'ListExecInst', 'EncodedListExecInstLen', 'EncodedListExecInst', 'AllowableOneSidednessPct', 'AllowableOneSidednessValue', 'AllowableOneSidednessCurr', 'TotNoOrders', 'LastFragment', 'NoOrders')
    __reqfields__ = ('ListID', 'BidType', 'TotNoOrders', 'NoOrders')    
    
    class NoOrders(Group):
        __fields__ = ('ClOrdID', 'SecondaryClOrdID', 'ListSeqNo', 'ClOrdLinkID', 'SettlInstMode', 'NoPartyIDs', 'TradeOriginationDate', 'TradeDate', 'Account', 'AcctIDSource', 'AccountType', 'DayBookingInst', 'BookingUnit', 'AllocID', 'PreallocMethod', 'NoAllocs', 'SettlType', 'SettlDate', 'CashMargin', 'ClearingFeeIndicator', 'HandlInst', 'ExecInst', 'MinQty', 'MaxFloor', 'ExDestination', 'NoTradingSessions', 'ProcessCode', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'NoUnderlyings', 'PrevClosePx', 'Side', 'SideValueInd', 'LocateReqd', 'TransactTime', 'NoStipulations', 'QtyType', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'OrdType', 'PriceType', 'Price', 'StopPx', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType', 'Currency', 'ComplianceID', 'SolicitedFlag', 'IOIid', 'QuoteID', 'TimeInForce', 'EffectiveTime', 'ExpireDate', 'ExpireTime', 'GTBookingInst', 'Commission', 'CommType', 'CommCurrency', 'FundRenewWaiv', 'OrderCapacity', 'OrderRestrictions', 'CustOrderCapacity', 'ForexReq', 'SettlCurrency', 'BookingType', 'Text', 'EncodedTextLen', 'EncodedText', 'SettlDate2', 'OrderQty2', 'Price2', 'PositionEffect', 'CoveredOrUncovered', 'MaxShow', 'PegOffsetValue', 'PegMoveType', 'PegOffsetType', 'PegLimitType', 'PegRoundDirection', 'PegScope', 'DiscretionInst', 'DiscretionOffsetValue', 'DiscretionMoveType', 'DiscretionOffsetType', 'DiscretionLimitType', 'DiscretionRoundDirection', 'DiscretionScope', 'TargetStrategy', 'TargetStrategyParameters', 'ParticipationRate', 'Designation')
        __reqfields__ = ('ClOrdID', 'ListSeqNo', 'Symbol', 'Side')


    class NoAllocs(Group):
        __fields__ = ('AllocAccount', 'AllocAcctIDSource', 'AllocSettlCurrency', 'IndividualAllocID', 'NoNestedPartyIDs', 'AllocQty')
        __reqfields__ = ()


    class NoTradingSessions(Group):
        __fields__ = ('TradingSessionID', 'TradingSessionSubID')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class ListStrikePrice(Message):
    __fields__ = ('ListID', 'TotNoStrikes', 'LastFragment', 'NoStrikes', 'NoUnderlyings')
    __reqfields__ = ('ListID', 'TotNoStrikes', 'NoStrikes')    
    
    class NoStrikes(Group):
        __fields__ = ('Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate')
        __reqfields__ = ('Symbol',)


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips', 'PrevClosePx', 'ClOrdID', 'SecondaryClOrdID', 'Side', 'Price', 'Currency', 'Text', 'EncodedTextLen', 'EncodedText')
        __reqfields__ = ('Price',)

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class ListStatus(Message):
    __fields__ = ('ListID', 'ListStatusType', 'NoRpts', 'ListOrderStatus', 'RptSeq', 'ListStatusText', 'EncodedListStatusTextLen', 'EncodedListStatusText', 'TransactTime', 'TotNoOrders', 'LastFragment', 'NoOrders')
    __reqfields__ = ('ListID', 'ListStatusType', 'NoRpts', 'ListOrderStatus', 'RptSeq', 'TotNoOrders', 'NoOrders')    
    
    class NoOrders(Group):
        __fields__ = ('ClOrdID', 'SecondaryClOrdID', 'CumQty', 'OrdStatus', 'WorkingIndicator', 'LeavesQty', 'CxlQty', 'AvgPx', 'OrdRejReason', 'Text', 'EncodedTextLen', 'EncodedText')
        __reqfields__ = ('ClOrdID', 'CumQty', 'OrdStatus', 'LeavesQty', 'CxlQty', 'AvgPx')

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class ListExecute(Message):
    __fields__ = ('ListID', 'ClientBidID', 'BidID', 'TransactTime', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('ListID', 'TransactTime')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class ListCancelRequest(Message):
    __fields__ = ('ListID', 'TransactTime', 'TradeOriginationDate', 'TradeDate', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('ListID', 'TransactTime')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class ListStatusRequest(Message):
    __fields__ = ('ListID', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('ListID',)    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class AllocationInstruction(Message):
    __fields__ = ('AllocID', 'AllocTransType', 'AllocType', 'SecondaryAllocID', 'RefAllocID', 'AllocCancReplaceReason', 'AllocIntermedReqType', 'AllocLinkID', 'AllocLinkType', 'BookingRefID', 'AllocNoOrdersType', 'NoOrders', 'NoExecs', 'PreviouslyReported', 'ReversalIndicator', 'MatchType', 'Side', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'DeliveryForm', 'PctAtRisk', 'NoInstrAttrib', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'NoLegs', 'Quantity', 'QtyType', 'LastMkt', 'TradeOriginationDate', 'TradingSessionID', 'TradingSessionSubID', 'PriceType', 'AvgPx', 'AvgParPx', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'Currency', 'AvgPxPrecision', 'NoPartyIDs', 'TradeDate', 'TransactTime', 'SettlType', 'SettlDate', 'BookingType', 'GrossTradeAmt', 'Concession', 'TotalTakedown', 'NetMoney', 'PositionEffect', 'AutoAcceptIndicator', 'Text', 'EncodedTextLen', 'EncodedText', 'NumDaysInterest', 'AccruedInterestRate', 'AccruedInterestAmt', 'TotalAccruedInterestAmt', 'InterestAtMaturity', 'EndAccruedInterestAmt', 'StartCash', 'EndCash', 'LegalConfirm', 'NoStipulations', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType', 'TotNoAllocs', 'LastFragment', 'NoAllocs')
    __reqfields__ = ('AllocID', 'AllocTransType', 'AllocType', 'AllocNoOrdersType', 'Side', 'Symbol', 'Quantity', 'AvgPx', 'TradeDate', 'NoAllocs')    
    
    class NoOrders(Group):
        __fields__ = ('ClOrdID', 'OrderID', 'SecondaryOrderID', 'SecondaryClOrdID', 'ListID', 'NoNested2PartyIDs', 'OrderQty', 'OrderAvgPx', 'OrderBookingQty')
        __reqfields__ = ()


    class NoExecs(Group):
        __fields__ = ('LastQty', 'ExecID', 'SecondaryExecID', 'LastPx', 'LastParPx', 'LastCapacity')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()


    class NoAllocs(Group):
        __fields__ = ('AllocAccount', 'AllocAcctIDSource', 'MatchStatus', 'AllocPrice', 'AllocQty', 'IndividualAllocID', 'ProcessCode', 'NoNestedPartyIDs', 'NotifyBrokerOfCredit', 'AllocHandlInst', 'AllocText', 'EncodedAllocTextLen', 'EncodedAllocText', 'Commission', 'CommType', 'CommCurrency', 'FundRenewWaiv', 'AllocAvgPx', 'AllocNetMoney', 'SettlCurrAmt', 'AllocSettlCurrAmt', 'SettlCurrency', 'AllocSettlCurrency', 'SettlCurrFxRate', 'SettlCurrFxRateCalc', 'AccruedInterestAmt', 'AllocAccruedInterestAmt', 'AllocInterestAtMaturity', 'SettlInstMode', 'NoMiscFees', 'NoClearingInstructions', 'ClearingInstruction', 'ClearingFeeIndicator', 'AllocSettlInstType', 'SettlDeliveryType', 'StandInstDbType', 'StandInstDbName', 'StandInstDbID', 'NoDlvyInst')
        __reqfields__ = ('AllocAccount', 'AllocQty')


    class NoMiscFees(Group):
        __fields__ = ('MiscFeeAmt', 'MiscFeeCurr', 'MiscFeeType', 'MiscFeeBasis')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class AllocationInstructionAck(Message):
    __fields__ = ('AllocID', 'NoPartyIDs', 'SecondaryAllocID', 'TradeDate', 'TransactTime', 'AllocStatus', 'AllocRejCode', 'AllocType', 'AllocIntermedReqType', 'MatchStatus', 'Product', 'SecurityType', 'Text', 'EncodedTextLen', 'EncodedText', 'NoAllocs')
    __reqfields__ = ('AllocID', 'TransactTime', 'AllocStatus')    
    
    class NoAllocs(Group):
        __fields__ = ('AllocAccount', 'AllocAcctIDSource', 'AllocPrice', 'IndividualAllocID', 'IndividualAllocRejCode', 'AllocText', 'EncodedAllocTextLen', 'EncodedAllocText')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class AllocationReport(Message):
    __fields__ = ('AllocReportID', 'AllocID', 'AllocTransType', 'AllocReportRefID', 'AllocCancReplaceReason', 'SecondaryAllocID', 'AllocReportType', 'AllocStatus', 'AllocRejCode', 'RefAllocID', 'AllocIntermedReqType', 'AllocLinkID', 'AllocLinkType', 'BookingRefID', 'AllocNoOrdersType', 'NoOrders', 'NoExecs', 'PreviouslyReported', 'ReversalIndicator', 'MatchType', 'Side', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'DeliveryForm', 'PctAtRisk', 'NoInstrAttrib', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'NoLegs', 'Quantity', 'QtyType', 'LastMkt', 'TradeOriginationDate', 'TradingSessionID', 'TradingSessionSubID', 'PriceType', 'AvgPx', 'AvgParPx', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'Currency', 'AvgPxPrecision', 'NoPartyIDs', 'TradeDate', 'TransactTime', 'SettlType', 'SettlDate', 'BookingType', 'GrossTradeAmt', 'Concession', 'TotalTakedown', 'NetMoney', 'PositionEffect', 'AutoAcceptIndicator', 'Text', 'EncodedTextLen', 'EncodedText', 'NumDaysInterest', 'AccruedInterestRate', 'AccruedInterestAmt', 'TotalAccruedInterestAmt', 'InterestAtMaturity', 'EndAccruedInterestAmt', 'StartCash', 'EndCash', 'LegalConfirm', 'NoStipulations', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType', 'TotNoAllocs', 'LastFragment', 'NoAllocs')
    __reqfields__ = ('AllocReportID', 'AllocTransType', 'AllocReportType', 'AllocStatus', 'AllocNoOrdersType', 'Side', 'Symbol', 'Quantity', 'AvgPx', 'TradeDate', 'NoAllocs')    
    
    class NoOrders(Group):
        __fields__ = ('ClOrdID', 'OrderID', 'SecondaryOrderID', 'SecondaryClOrdID', 'ListID', 'NoNested2PartyIDs', 'OrderQty', 'OrderAvgPx', 'OrderBookingQty')
        __reqfields__ = ()


    class NoExecs(Group):
        __fields__ = ('LastQty', 'ExecID', 'SecondaryExecID', 'LastPx', 'LastParPx', 'LastCapacity')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()


    class NoAllocs(Group):
        __fields__ = ('AllocAccount', 'AllocAcctIDSource', 'MatchStatus', 'AllocPrice', 'AllocQty', 'IndividualAllocID', 'ProcessCode', 'NoNestedPartyIDs', 'NotifyBrokerOfCredit', 'AllocHandlInst', 'AllocText', 'EncodedAllocTextLen', 'EncodedAllocText', 'Commission', 'CommType', 'CommCurrency', 'FundRenewWaiv', 'AllocAvgPx', 'AllocNetMoney', 'SettlCurrAmt', 'AllocSettlCurrAmt', 'SettlCurrency', 'AllocSettlCurrency', 'SettlCurrFxRate', 'SettlCurrFxRateCalc', 'AllocAccruedInterestAmt', 'AllocInterestAtMaturity', 'NoMiscFees', 'NoClearingInstructions', 'ClearingFeeIndicator', 'AllocSettlInstType', 'SettlDeliveryType', 'StandInstDbType', 'StandInstDbName', 'StandInstDbID', 'NoDlvyInst')
        __reqfields__ = ('AllocAccount', 'AllocQty')


    class NoMiscFees(Group):
        __fields__ = ('MiscFeeAmt', 'MiscFeeCurr', 'MiscFeeType', 'MiscFeeBasis')
        __reqfields__ = ()


    class NoClearingInstructions(Group):
        __fields__ = ('ClearingInstruction',)
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class AllocationReportAck(Message):
    __fields__ = ('AllocReportID', 'AllocID', 'NoPartyIDs', 'SecondaryAllocID', 'TradeDate', 'TransactTime', 'AllocStatus', 'AllocRejCode', 'AllocReportType', 'AllocIntermedReqType', 'MatchStatus', 'Product', 'SecurityType', 'Text', 'EncodedTextLen', 'EncodedText', 'NoAllocs')
    __reqfields__ = ('AllocReportID', 'AllocID', 'TransactTime', 'AllocStatus')    
    
    class NoAllocs(Group):
        __fields__ = ('AllocAccount', 'AllocAcctIDSource', 'AllocPrice', 'IndividualAllocID', 'IndividualAllocRejCode', 'AllocText', 'EncodedAllocTextLen', 'EncodedAllocText')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class Confirmation(Message):
    __fields__ = ('ConfirmID', 'ConfirmRefID', 'ConfirmReqID', 'ConfirmTransType', 'ConfirmType', 'CopyMsgIndicator', 'LegalConfirm', 'ConfirmStatus', 'NoPartyIDs', 'NoOrders', 'AllocID', 'SecondaryAllocID', 'IndividualAllocID', 'TransactTime', 'TradeDate', 'NoTrdRegTimestamps', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'DeliveryForm', 'PctAtRisk', 'NoInstrAttrib', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'NoLegs', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType', 'AllocQty', 'QtyType', 'Side', 'Currency', 'LastMkt', 'NoCapacities', 'AllocAccount', 'AllocAcctIDSource', 'AllocAccountType', 'AvgPx', 'AvgPxPrecision', 'PriceType', 'AvgParPx', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'ReportedPx', 'Text', 'EncodedTextLen', 'EncodedText', 'ProcessCode', 'GrossTradeAmt', 'NumDaysInterest', 'ExDate', 'AccruedInterestRate', 'AccruedInterestAmt', 'InterestAtMaturity', 'EndAccruedInterestAmt', 'StartCash', 'EndCash', 'Concession', 'TotalTakedown', 'NetMoney', 'MaturityNetMoney', 'SettlCurrAmt', 'SettlCurrency', 'SettlCurrFxRate', 'SettlCurrFxRateCalc', 'SettlType', 'SettlDate', 'SettlDeliveryType', 'StandInstDbType', 'StandInstDbName', 'StandInstDbID', 'NoDlvyInst', 'Commission', 'CommType', 'CommCurrency', 'FundRenewWaiv', 'SharedCommission', 'NoStipulations', 'NoMiscFees')
    __reqfields__ = ('ConfirmID', 'ConfirmTransType', 'ConfirmType', 'ConfirmStatus', 'TransactTime', 'TradeDate', 'Symbol', 'NoUnderlyings', 'NoLegs', 'AllocQty', 'Side', 'NoCapacities', 'AllocAccount', 'AvgPx', 'GrossTradeAmt', 'NetMoney')    
    
    class NoOrders(Group):
        __fields__ = ('ClOrdID', 'OrderID', 'SecondaryOrderID', 'SecondaryClOrdID', 'ListID', 'NoNested2PartyIDs', 'OrderQty', 'OrderAvgPx', 'OrderBookingQty')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()


    class NoCapacities(Group):
        __fields__ = ('OrderCapacity', 'OrderRestrictions', 'OrderCapacityQty')
        __reqfields__ = ('OrderCapacity', 'OrderCapacityQty')


    class NoMiscFees(Group):
        __fields__ = ('MiscFeeAmt', 'MiscFeeCurr', 'MiscFeeType', 'MiscFeeBasis')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class ConfirmationAck(Message):
    __fields__ = ('ConfirmID', 'TradeDate', 'TransactTime', 'AffirmStatus', 'ConfirmRejReason', 'MatchStatus', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('ConfirmID', 'TradeDate', 'TransactTime', 'AffirmStatus')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class ConfirmationRequest(Message):
    __fields__ = ('ConfirmReqID', 'ConfirmType', 'NoOrders', 'AllocID', 'SecondaryAllocID', 'IndividualAllocID', 'TransactTime', 'AllocAccount', 'AllocAcctIDSource', 'AllocAccountType', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('ConfirmReqID', 'ConfirmType', 'TransactTime')    
    
    class NoOrders(Group):
        __fields__ = ('ClOrdID', 'OrderID', 'SecondaryOrderID', 'SecondaryClOrdID', 'ListID', 'NoNested2PartyIDs', 'OrderQty', 'OrderAvgPx', 'OrderBookingQty')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class SettlementInstructions(Message):
    __fields__ = ('SettlInstMsgID', 'SettlInstReqID', 'SettlInstMode', 'SettlInstReqRejCode', 'Text', 'EncodedTextLen', 'EncodedText', 'SettlInstSource', 'ClOrdID', 'TransactTime', 'NoSettlInst')
    __reqfields__ = ('SettlInstMsgID', 'SettlInstMode', 'TransactTime')    
    
    class NoSettlInst(Group):
        __fields__ = ('SettlInstID', 'SettlInstTransType', 'SettlInstRefID', 'NoPartyIDs', 'Side', 'Product', 'SecurityType', 'CFICode', 'EffectiveTime', 'ExpireTime', 'LastUpdateTime', 'SettlDeliveryType', 'StandInstDbType', 'StandInstDbName', 'StandInstDbID', 'NoDlvyInst', 'PaymentMethod', 'PaymentRef', 'CardHolderName', 'CardNumber', 'CardStartDate', 'CardExpDate', 'CardIssNum', 'PaymentDate', 'PaymentRemitterID')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class SettlementInstructionRequest(Message):
    __fields__ = ('SettlInstReqID', 'TransactTime', 'NoPartyIDs', 'AllocAccount', 'AllocAcctIDSource', 'Side', 'Product', 'SecurityType', 'CFICode', 'EffectiveTime', 'ExpireTime', 'LastUpdateTime', 'StandInstDbType', 'StandInstDbName', 'StandInstDbID')
    __reqfields__ = ('SettlInstReqID', 'TransactTime')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class TradeCaptureReportRequest(Message):
    __fields__ = ('TradeRequestID', 'TradeRequestType', 'SubscriptionRequestType', 'TradeReportID', 'SecondaryTradeReportID', 'ExecID', 'ExecType', 'OrderID', 'ClOrdID', 'MatchStatus', 'TrdType', 'TrdSubType', 'TransferReason', 'SecondaryTrdType', 'TradeLinkID', 'TrdMatchID', 'NoPartyIDs', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'DeliveryForm', 'PctAtRisk', 'NoInstrAttrib', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'NoUnderlyings', 'NoLegs', 'NoDates', 'ClearingBusinessDate', 'TradingSessionID', 'TradingSessionSubID', 'TimeBracket', 'Side', 'MultiLegReportingType', 'TradeInputSource', 'TradeInputDevice', 'ResponseTransportType', 'ResponseDestination', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('TradeRequestID', 'TradeRequestType')    
    
    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()


    class NoDates(Group):
        __fields__ = ('TradeDate', 'TransactTime')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class TradeCaptureReportRequestAck(Message):
    __fields__ = ('TradeRequestID', 'TradeRequestType', 'SubscriptionRequestType', 'TotNumTradeReports', 'TradeRequestResult', 'TradeRequestStatus', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'NoUnderlyings', 'NoLegs', 'MultiLegReportingType', 'ResponseTransportType', 'ResponseDestination', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('TradeRequestID', 'TradeRequestType', 'TradeRequestResult', 'TradeRequestStatus')    
    
    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class TradeCaptureReport(Message):
    __fields__ = ('TradeReportID', 'TradeReportTransType', 'TradeReportType', 'TradeRequestID', 'TrdType', 'TrdSubType', 'SecondaryTrdType', 'TransferReason', 'ExecType', 'TotNumTradeReports', 'LastRptRequested', 'UnsolicitedIndicator', 'SubscriptionRequestType', 'TradeReportRefID', 'SecondaryTradeReportRefID', 'SecondaryTradeReportID', 'TradeLinkID', 'TrdMatchID', 'ExecID', 'OrdStatus', 'SecondaryExecID', 'ExecRestatementReason', 'PreviouslyReported', 'PriceType', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'OrderQty', 'CashOrderQty', 'OrderPercent', 'RoundingDirection', 'RoundingModulus', 'QtyType', 'YieldType', 'Yield', 'YieldCalcDate', 'YieldRedemptionDate', 'YieldRedemptionPrice', 'YieldRedemptionPriceType', 'NoUnderlyings', 'UnderlyingTradingSessionID', 'UnderlyingTradingSessionSubID', 'LastQty', 'LastPx', 'LastParPx', 'LastSpotRate', 'LastForwardPoints', 'LastMkt', 'TradeDate', 'ClearingBusinessDate', 'AvgPx', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'AvgPxIndicator', 'NoPosAmt', 'MultiLegReportingType', 'TradeLegRefID', 'NoLegs', 'TransactTime', 'NoTrdRegTimestamps', 'SettlType', 'SettlDate', 'MatchStatus', 'MatchType', 'NoSides', 'CopyMsgIndicator', 'PublishTrdIndicator', 'ShortSaleReason')
    __reqfields__ = ('TradeReportID', 'PreviouslyReported', 'Symbol', 'LastQty', 'LastPx', 'TradeDate', 'TransactTime', 'NoSides')    
    
    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'LegQty', 'LegSwapType', 'NoLegStipulations', 'LegPositionEffect', 'LegCoveredOrUncovered', 'NoNestedPartyIDs', 'LegRefID', 'LegPrice', 'LegSettlType', 'LegSettlDate', 'LegLastPx')
        __reqfields__ = ()


    class NoSides(Group):
        __fields__ = ('Side', 'OrderID', 'SecondaryOrderID', 'ClOrdID', 'SecondaryClOrdID', 'ListID', 'NoPartyIDs', 'Account', 'AcctIDSource', 'AccountType', 'ProcessCode', 'OddLot', 'NoClearingInstructions', 'ClearingFeeIndicator', 'TradeInputSource', 'TradeInputDevice', 'OrderInputDevice', 'Currency', 'ComplianceID', 'SolicitedFlag', 'OrderCapacity', 'OrderRestrictions', 'CustOrderCapacity', 'OrdType', 'ExecInst', 'TransBkdTime', 'TradingSessionID', 'TradingSessionSubID', 'TimeBracket', 'Commission', 'CommType', 'CommCurrency', 'FundRenewWaiv', 'GrossTradeAmt', 'NumDaysInterest', 'ExDate', 'AccruedInterestRate', 'AccruedInterestAmt', 'InterestAtMaturity', 'EndAccruedInterestAmt', 'StartCash', 'EndCash', 'Concession', 'TotalTakedown', 'NetMoney', 'SettlCurrAmt', 'SettlCurrency', 'SettlCurrFxRate', 'SettlCurrFxRateCalc', 'PositionEffect', 'Text', 'EncodedTextLen', 'EncodedText', 'SideMultiLegReportingType', 'NoContAmts', 'NoStipulations', 'NoMiscFees', 'ExchangeRule', 'TradeAllocIndicator', 'PreallocMethod', 'AllocID', 'NoAllocs')
        __reqfields__ = ('Side', 'OrderID')


    class NoClearingInstructions(Group):
        __fields__ = ('ClearingInstruction',)
        __reqfields__ = ()


    class NoContAmts(Group):
        __fields__ = ('ContAmtType', 'ContAmtValue', 'ContAmtCurr')
        __reqfields__ = ()


    class NoMiscFees(Group):
        __fields__ = ('MiscFeeAmt', 'MiscFeeCurr', 'MiscFeeType', 'MiscFeeBasis')
        __reqfields__ = ()


    class NoAllocs(Group):
        __fields__ = ('AllocAccount', 'AllocAcctIDSource', 'AllocSettlCurrency', 'IndividualAllocID', 'NoNested2PartyIDs', 'AllocQty')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class TradeCaptureReportAck(Message):
    __fields__ = ('TradeReportID', 'TradeReportTransType', 'TradeReportType', 'TrdType', 'TrdSubType', 'SecondaryTrdType', 'TransferReason', 'ExecType', 'TradeReportRefID', 'SecondaryTradeReportRefID', 'TrdRptStatus', 'TradeReportRejectReason', 'SecondaryTradeReportID', 'SubscriptionRequestType', 'TradeLinkID', 'TrdMatchID', 'ExecID', 'SecondaryExecID', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'TransactTime', 'NoTrdRegTimestamps', 'ResponseTransportType', 'ResponseDestination', 'Text', 'EncodedTextLen', 'EncodedText', 'NoLegs', 'ClearingFeeIndicator', 'OrderCapacity', 'OrderRestrictions', 'CustOrderCapacity', 'Account', 'AcctIDSource', 'AccountType', 'PositionEffect', 'PreallocMethod', 'NoAllocs')
    __reqfields__ = ('TradeReportID', 'ExecType', 'Symbol')    
    
    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'LegQty', 'LegSwapType', 'NoLegStipulations', 'LegPositionEffect', 'LegCoveredOrUncovered', 'NoNestedPartyIDs', 'LegRefID', 'LegPrice', 'LegSettlType', 'LegSettlDate', 'LegLastPx')
        __reqfields__ = ()


    class NoAllocs(Group):
        __fields__ = ('AllocAccount', 'AllocAcctIDSource', 'AllocSettlCurrency', 'IndividualAllocID', 'NoNested2PartyIDs', 'AllocQty')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class RegistrationInstructions(Message):
    __fields__ = ('RegistID', 'RegistTransType', 'RegistRefID', 'ClOrdID', 'NoPartyIDs', 'Account', 'AcctIDSource', 'RegistAcctType', 'TaxAdvantageType', 'OwnershipType', 'NoRegistDtls', 'NoDistribInsts')
    __reqfields__ = ('RegistID', 'RegistTransType', 'RegistRefID')    
    
    class NoRegistDtls(Group):
        __fields__ = ('RegistDtls', 'RegistEmail', 'MailingDtls', 'MailingInst', 'NoNestedPartyIDs', 'OwnerType', 'DateOfBirth', 'InvestorCountryOfResidence')
        __reqfields__ = ()


    class NoDistribInsts(Group):
        __fields__ = ('DistribPaymentMethod', 'DistribPercentage', 'CashDistribCurr', 'CashDistribAgentName', 'CashDistribAgentCode', 'CashDistribAgentAcctNumber', 'CashDistribPayRef', 'CashDistribAgentAcctName')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class RegistrationInstructionsResponse(Message):
    __fields__ = ('RegistID', 'RegistTransType', 'RegistRefID', 'ClOrdID', 'NoPartyIDs', 'Account', 'AcctIDSource', 'RegistStatus', 'RegistRejReasonCode', 'RegistRejReasonText')
    __reqfields__ = ('RegistID', 'RegistTransType', 'RegistRefID', 'RegistStatus')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class PositionMaintenanceRequest(Message):
    __fields__ = ('PosReqID', 'PosTransType', 'PosMaintAction', 'OrigPosReqRefID', 'PosMaintRptRefID', 'ClearingBusinessDate', 'SettlSessID', 'SettlSessSubID', 'NoPartyIDs', 'Account', 'AcctIDSource', 'AccountType', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'Currency', 'NoLegs', 'NoUnderlyings', 'NoTradingSessions', 'TransactTime', 'NoPositions', 'AdjustmentType', 'ContraryInstructionIndicator', 'PriorSpreadIndicator', 'ThresholdAmount', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('PosReqID', 'PosTransType', 'PosMaintAction', 'ClearingBusinessDate', 'Account', 'AccountType', 'Symbol', 'TransactTime', 'NoPositions')    
    
    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoTradingSessions(Group):
        __fields__ = ('TradingSessionID', 'TradingSessionSubID')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class PositionMaintenanceReport(Message):
    __fields__ = ('PosMaintRptID', 'PosTransType', 'PosReqID', 'PosMaintAction', 'OrigPosReqRefID', 'PosMaintStatus', 'PosMaintResult', 'ClearingBusinessDate', 'SettlSessID', 'SettlSessSubID', 'NoPartyIDs', 'Account', 'AcctIDSource', 'AccountType', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'Currency', 'NoLegs', 'NoUnderlyings', 'NoTradingSessions', 'TransactTime', 'NoPositions', 'NoPosAmt', 'AdjustmentType', 'ThresholdAmount', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('PosMaintRptID', 'PosTransType', 'PosMaintAction', 'OrigPosReqRefID', 'PosMaintStatus', 'ClearingBusinessDate', 'Account', 'AccountType', 'Symbol', 'TransactTime', 'NoPositions', 'NoPosAmt')    
    
    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoTradingSessions(Group):
        __fields__ = ('TradingSessionID', 'TradingSessionSubID')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class RequestForPositions(Message):
    __fields__ = ('PosReqID', 'PosReqType', 'MatchStatus', 'SubscriptionRequestType', 'NoPartyIDs', 'Account', 'AcctIDSource', 'AccountType', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'Currency', 'NoLegs', 'NoUnderlyings', 'ClearingBusinessDate', 'SettlSessID', 'SettlSessSubID', 'NoTradingSessions', 'TransactTime', 'ResponseTransportType', 'ResponseDestination', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('PosReqID', 'PosReqType', 'Account', 'AccountType', 'ClearingBusinessDate', 'TransactTime')    
    
    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoTradingSessions(Group):
        __fields__ = ('TradingSessionID', 'TradingSessionSubID')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class RequestForPositionsAck(Message):
    __fields__ = ('PosMaintRptID', 'PosReqID', 'TotalNumPosReports', 'UnsolicitedIndicator', 'PosReqResult', 'PosReqStatus', 'NoPartyIDs', 'Account', 'AcctIDSource', 'AccountType', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'Currency', 'NoLegs', 'NoUnderlyings', 'ResponseTransportType', 'ResponseDestination', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('PosMaintRptID', 'PosReqResult', 'PosReqStatus', 'Account', 'AccountType')    
    
    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class PositionReport(Message):
    __fields__ = ('PosMaintRptID', 'PosReqID', 'PosReqType', 'SubscriptionRequestType', 'TotalNumPosReports', 'UnsolicitedIndicator', 'PosReqResult', 'ClearingBusinessDate', 'SettlSessID', 'SettlSessSubID', 'NoPartyIDs', 'Account', 'AcctIDSource', 'AccountType', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'Currency', 'SettlPrice', 'SettlPriceType', 'PriorSettlPrice', 'NoLegs', 'NoUnderlyings', 'NoPositions', 'NoPosAmt', 'RegistStatus', 'DeliveryDate', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('PosMaintRptID', 'PosReqResult', 'ClearingBusinessDate', 'Account', 'AccountType', 'SettlPrice', 'SettlPriceType', 'PriorSettlPrice', 'NoPositions', 'NoPosAmt')    
    
    class NoLegs(Group):
        __fields__ = ('LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips', 'UnderlyingSettlPrice', 'UnderlyingSettlPriceType')
        __reqfields__ = ('UnderlyingSettlPrice', 'UnderlyingSettlPriceType')

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class AssignmentReport(Message):
    __fields__ = ('AsgnRptID', 'TotNumAssignmentReports', 'LastRptRequested', 'NoPartyIDs', 'Account', 'AccountType', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'Currency', 'NoLegs', 'LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'NoUnderlyings', 'NoPositions', 'NoPosAmt', 'ThresholdAmount', 'SettlPrice', 'SettlPriceType', 'UnderlyingSettlPrice', 'ExpireDate', 'AssignmentMethod', 'AssignmentUnit', 'OpenInterest', 'ExerciseMethod', 'SettlSessID', 'SettlSessSubID', 'ClearingBusinessDate', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('AsgnRptID', 'AccountType', 'NoPositions', 'NoPosAmt', 'SettlPrice', 'SettlPriceType', 'UnderlyingSettlPrice', 'AssignmentMethod', 'OpenInterest', 'ExerciseMethod', 'SettlSessID', 'SettlSessSubID', 'ClearingBusinessDate')    
    
    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class CollateralRequest(Message):
    __fields__ = ('CollReqID', 'CollAsgnReason', 'TransactTime', 'ExpireTime', 'NoPartyIDs', 'Account', 'AccountType', 'ClOrdID', 'OrderID', 'SecondaryOrderID', 'SecondaryClOrdID', 'NoExecs', 'NoTrades', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'SettlDate', 'Quantity', 'QtyType', 'Currency', 'NoLegs', 'LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'NoUnderlyings', 'MarginExcess', 'TotalNetValue', 'CashOutstanding', 'NoTrdRegTimestamps', 'Side', 'NoMiscFees', 'Price', 'PriceType', 'AccruedInterestAmt', 'EndAccruedInterestAmt', 'StartCash', 'EndCash', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'NoStipulations', 'TradingSessionID', 'TradingSessionSubID', 'SettlSessID', 'SettlSessSubID', 'ClearingBusinessDate', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('CollReqID', 'CollAsgnReason', 'TransactTime')    
    
    class NoExecs(Group):
        __fields__ = ('ExecID',)
        __reqfields__ = ()


    class NoTrades(Group):
        __fields__ = ('TradeReportID', 'SecondaryTradeReportID')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips', 'CollAction')
        __reqfields__ = ()


    class NoMiscFees(Group):
        __fields__ = ('MiscFeeAmt', 'MiscFeeCurr', 'MiscFeeType', 'MiscFeeBasis')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class CollateralAssignment(Message):
    __fields__ = ('CollAsgnID', 'CollReqID', 'CollAsgnReason', 'CollAsgnTransType', 'CollAsgnRefID', 'TransactTime', 'ExpireTime', 'NoPartyIDs', 'Account', 'AccountType', 'ClOrdID', 'OrderID', 'SecondaryOrderID', 'SecondaryClOrdID', 'NoExecs', 'NoTrades', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'SettlDate', 'Quantity', 'QtyType', 'Currency', 'NoLegs', 'LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'NoUnderlyings', 'MarginExcess', 'TotalNetValue', 'CashOutstanding', 'NoTrdRegTimestamps', 'Side', 'NoMiscFees', 'Price', 'PriceType', 'AccruedInterestAmt', 'EndAccruedInterestAmt', 'StartCash', 'EndCash', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'NoStipulations', 'SettlDeliveryType', 'StandInstDbType', 'StandInstDbName', 'StandInstDbID', 'NoDlvyInst', 'TradingSessionID', 'TradingSessionSubID', 'SettlSessID', 'SettlSessSubID', 'ClearingBusinessDate', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('CollAsgnID', 'CollAsgnReason', 'CollAsgnTransType', 'TransactTime')    
    
    class NoExecs(Group):
        __fields__ = ('ExecID',)
        __reqfields__ = ()


    class NoTrades(Group):
        __fields__ = ('TradeReportID', 'SecondaryTradeReportID')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips', 'CollAction')
        __reqfields__ = ()


    class NoMiscFees(Group):
        __fields__ = ('MiscFeeAmt', 'MiscFeeCurr', 'MiscFeeType', 'MiscFeeBasis')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class CollateralResponse(Message):
    __fields__ = ('CollRespID', 'CollAsgnID', 'CollReqID', 'CollAsgnReason', 'CollAsgnTransType', 'CollAsgnRespType', 'CollAsgnRejectReason', 'TransactTime', 'NoPartyIDs', 'Account', 'AccountType', 'ClOrdID', 'OrderID', 'SecondaryOrderID', 'SecondaryClOrdID', 'NoExecs', 'NoTrades', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'SettlDate', 'Quantity', 'QtyType', 'Currency', 'NoLegs', 'LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'NoUnderlyings', 'MarginExcess', 'TotalNetValue', 'CashOutstanding', 'NoTrdRegTimestamps', 'Side', 'NoMiscFees', 'Price', 'PriceType', 'AccruedInterestAmt', 'EndAccruedInterestAmt', 'StartCash', 'EndCash', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'NoStipulations', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('CollRespID', 'CollAsgnID', 'CollAsgnReason', 'CollAsgnRespType', 'TransactTime')    
    
    class NoExecs(Group):
        __fields__ = ('ExecID',)
        __reqfields__ = ()


    class NoTrades(Group):
        __fields__ = ('TradeReportID', 'SecondaryTradeReportID')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips', 'CollAction')
        __reqfields__ = ()


    class NoMiscFees(Group):
        __fields__ = ('MiscFeeAmt', 'MiscFeeCurr', 'MiscFeeType', 'MiscFeeBasis')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class CollateralReport(Message):
    __fields__ = ('CollRptID', 'CollInquiryID', 'CollStatus', 'TotNumReports', 'LastRptRequested', 'NoPartyIDs', 'Account', 'AccountType', 'ClOrdID', 'OrderID', 'SecondaryOrderID', 'SecondaryClOrdID', 'NoExecs', 'NoTrades', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'SettlDate', 'Quantity', 'QtyType', 'Currency', 'NoLegs', 'LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'NoUnderlyings', 'MarginExcess', 'TotalNetValue', 'CashOutstanding', 'NoTrdRegTimestamps', 'Side', 'NoMiscFees', 'Price', 'PriceType', 'AccruedInterestAmt', 'EndAccruedInterestAmt', 'StartCash', 'EndCash', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'NoStipulations', 'SettlDeliveryType', 'StandInstDbType', 'StandInstDbName', 'StandInstDbID', 'NoDlvyInst', 'TradingSessionID', 'TradingSessionSubID', 'SettlSessID', 'SettlSessSubID', 'ClearingBusinessDate', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('CollRptID', 'CollStatus')    
    
    class NoExecs(Group):
        __fields__ = ('ExecID',)
        __reqfields__ = ()


    class NoTrades(Group):
        __fields__ = ('TradeReportID', 'SecondaryTradeReportID')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()


    class NoMiscFees(Group):
        __fields__ = ('MiscFeeAmt', 'MiscFeeCurr', 'MiscFeeType', 'MiscFeeBasis')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class CollateralInquiry(Message):
    __fields__ = ('CollInquiryID', 'NoCollInquiryQualifier', 'SubscriptionRequestType', 'ResponseTransportType', 'ResponseDestination', 'NoPartyIDs', 'Account', 'AccountType', 'ClOrdID', 'OrderID', 'SecondaryOrderID', 'SecondaryClOrdID', 'NoExecs', 'NoTrades', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'SettlDate', 'Quantity', 'QtyType', 'Currency', 'NoLegs', 'LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'NoUnderlyings', 'MarginExcess', 'TotalNetValue', 'CashOutstanding', 'NoTrdRegTimestamps', 'Side', 'Price', 'PriceType', 'AccruedInterestAmt', 'EndAccruedInterestAmt', 'StartCash', 'EndCash', 'Spread', 'BenchmarkCurveCurrency', 'BenchmarkCurveName', 'BenchmarkCurvePoint', 'BenchmarkPrice', 'BenchmarkPriceType', 'BenchmarkSecurityID', 'BenchmarkSecurityIDSource', 'NoStipulations', 'SettlDeliveryType', 'StandInstDbType', 'StandInstDbName', 'StandInstDbID', 'NoDlvyInst', 'TradingSessionID', 'TradingSessionSubID', 'SettlSessID', 'SettlSessSubID', 'ClearingBusinessDate', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ()    
    
    class NoCollInquiryQualifier(Group):
        __fields__ = ('CollInquiryQualifier',)
        __reqfields__ = ()


    class NoExecs(Group):
        __fields__ = ('ExecID',)
        __reqfields__ = ()


    class NoTrades(Group):
        __fields__ = ('TradeReportID', 'SecondaryTradeReportID')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class CollateralInquiryAck(Message):
    __fields__ = ('CollInquiryID', 'CollInquiryStatus', 'CollInquiryResult', 'NoCollInquiryQualifier', 'TotNumReports', 'NoPartyIDs', 'Account', 'AccountType', 'ClOrdID', 'OrderID', 'SecondaryOrderID', 'SecondaryClOrdID', 'NoExecs', 'NoTrades', 'Symbol', 'SymbolSfx', 'SecurityID', 'SecurityIDSource', 'NoSecurityAltID', 'Product', 'CFICode', 'SecurityType', 'SecuritySubType', 'MaturityMonthYear', 'MaturityDate', 'CouponPaymentDate', 'IssueDate', 'RepoCollateralSecurityType', 'RepurchaseTerm', 'RepurchaseRate', 'Factor', 'CreditRating', 'InstrRegistry', 'CountryOfIssue', 'StateOrProvinceOfIssue', 'LocaleOfIssue', 'RedemptionDate', 'StrikePrice', 'StrikeCurrency', 'OptAttribute', 'ContractMultiplier', 'CouponRate', 'SecurityExchange', 'Issuer', 'EncodedIssuerLen', 'EncodedIssuer', 'SecurityDesc', 'EncodedSecurityDescLen', 'EncodedSecurityDesc', 'Pool', 'ContractSettlMonth', 'CPProgram', 'CPRegType', 'NoEvents', 'DatedDate', 'InterestAccrualDate', 'AgreementDesc', 'AgreementID', 'AgreementDate', 'AgreementCurrency', 'TerminationType', 'StartDate', 'EndDate', 'DeliveryType', 'MarginRatio', 'SettlDate', 'Quantity', 'QtyType', 'Currency', 'NoLegs', 'LegSymbol', 'LegSymbolSfx', 'LegSecurityID', 'LegSecurityIDSource', 'NoLegSecurityAltID', 'LegProduct', 'LegCFICode', 'LegSecurityType', 'LegSecuritySubType', 'LegMaturityMonthYear', 'LegMaturityDate', 'LegCouponPaymentDate', 'LegIssueDate', 'LegRepoCollateralSecurityType', 'LegRepurchaseTerm', 'LegRepurchaseRate', 'LegFactor', 'LegCreditRating', 'LegInstrRegistry', 'LegCountryOfIssue', 'LegStateOrProvinceOfIssue', 'LegLocaleOfIssue', 'LegRedemptionDate', 'LegStrikePrice', 'LegStrikeCurrency', 'LegOptAttribute', 'LegContractMultiplier', 'LegCouponRate', 'LegSecurityExchange', 'LegIssuer', 'EncodedLegIssuerLen', 'EncodedLegIssuer', 'LegSecurityDesc', 'EncodedLegSecurityDescLen', 'EncodedLegSecurityDesc', 'LegRatioQty', 'LegSide', 'LegCurrency', 'LegPool', 'LegDatedDate', 'LegContractSettlMonth', 'LegInterestAccrualDate', 'NoUnderlyings', 'TradingSessionID', 'TradingSessionSubID', 'SettlSessID', 'SettlSessSubID', 'ClearingBusinessDate', 'ResponseTransportType', 'ResponseDestination', 'Text', 'EncodedTextLen', 'EncodedText')
    __reqfields__ = ('CollInquiryID', 'CollInquiryStatus')    
    
    class NoCollInquiryQualifier(Group):
        __fields__ = ('CollInquiryQualifier',)
        __reqfields__ = ()


    class NoExecs(Group):
        __fields__ = ('ExecID',)
        __reqfields__ = ()


    class NoTrades(Group):
        __fields__ = ('TradeReportID', 'SecondaryTradeReportID')
        __reqfields__ = ()


    class NoUnderlyings(Group):
        __fields__ = ('UnderlyingSymbol', 'UnderlyingSymbolSfx', 'UnderlyingSecurityID', 'UnderlyingSecurityIDSource', 'NoUnderlyingSecurityAltID', 'UnderlyingProduct', 'UnderlyingCFICode', 'UnderlyingSecurityType', 'UnderlyingSecuritySubType', 'UnderlyingMaturityMonthYear', 'UnderlyingMaturityDate', 'UnderlyingCouponPaymentDate', 'UnderlyingIssueDate', 'UnderlyingRepoCollateralSecurityType', 'UnderlyingRepurchaseTerm', 'UnderlyingRepurchaseRate', 'UnderlyingFactor', 'UnderlyingCreditRating', 'UnderlyingInstrRegistry', 'UnderlyingCountryOfIssue', 'UnderlyingStateOrProvinceOfIssue', 'UnderlyingLocaleOfIssue', 'UnderlyingRedemptionDate', 'UnderlyingStrikePrice', 'UnderlyingStrikeCurrency', 'UnderlyingOptAttribute', 'UnderlyingContractMultiplier', 'UnderlyingCouponRate', 'UnderlyingSecurityExchange', 'UnderlyingIssuer', 'EncodedUnderlyingIssuerLen', 'EncodedUnderlyingIssuer', 'UnderlyingSecurityDesc', 'EncodedUnderlyingSecurityDescLen', 'EncodedUnderlyingSecurityDesc', 'UnderlyingCPProgram', 'UnderlyingCPRegType', 'UnderlyingCurrency', 'UnderlyingQty', 'UnderlyingPx', 'UnderlyingDirtyPrice', 'UnderlyingEndPrice', 'UnderlyingStartValue', 'UnderlyingCurrentValue', 'UnderlyingEndValue', 'NoUnderlyingStips')
        __reqfields__ = ()

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class Notification(Message):
    __fields__ = ('AccountName', 'NotifPriority', 'Text', 'Account')
    __reqfields__ = ('AccountName', 'NotifPriority', 'Text')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class AccountInfo(Message):
    __fields__ = ('AccountName', 'Currency', 'Leverage', 'UsableMargin', 'Equity', 'Account', 'TrdType')
    __reqfields__ = ('AccountName', 'Currency', 'Leverage', 'UsableMargin', 'Equity')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class InstrumentPositionInfo(Message):
    __fields__ = ('AccountName', 'Symbol', 'Amount', 'Price', 'Account')
    __reqfields__ = ('AccountName', 'Symbol', 'Amount', 'Price')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class OvernightReport(Message):
    __fields__ = ('Account', 'Symbol', 'Amount')
    __reqfields__ = ('Account', 'Symbol', 'Amount')    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class ActivationRequest(Message):
    __fields__ = ('Account',)
    __reqfields__ = ()    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        


class ActivationResponse(Message):
    __fields__ = ('UsersActivated',)
    __reqfields__ = ()    
    
    class UsersActivated(Group):
        __fields__ = ('Username', 'Account')
        __reqfields__ = ('Username', 'Account')

        
    def __init__(self, **data):
        Message.__init__(self, **data)        


class AccountInfoRequest(Message):
    __fields__ = ('Account',)
    __reqfields__ = ()    
            
    def __init__(self, **data):
        Message.__init__(self, **data)        

MSGTYPE = {
		PositionReport : "AP",
		QuoteResponse : "AJ",
		RFQRequest : "AH",
		CollateralReport : "BA",
		OrderMassCancelReport : "r",
		RegistrationInstructionsResponse : "p",
		ListExecute : "L",
		ConfirmationRequest : "BH",
		RequestForPositionsAck : "AO",
		NewOrderSingle : "D",
		TradingSessionStatusRequest : "g",
		News : "B",
		OrderCancelReject : "9",
		DerivativeSecurityListRequest : "z",
		BidResponse : "l",
		CollateralInquiry : "BB",
		PositionMaintenanceReport : "AM",
		Logon : "A",
		QuoteStatusRequest : "a",
		TestRequest : "1",
		ConfirmationAck : "AU",
		AccountInfoRequest : "U7",
		SecurityDefinition : "d",
		CollateralResponse : "AZ",
		ResendRequest : "2",
		SecurityListRequest : "x",
		QuoteCancel : "Z",
		RequestForPositions : "AN",
		TradeCaptureReport : "AE",
		Email : "C",
		DerivativeSecurityList : "AA",
		AllocationReportAck : "AT",
		TradeCaptureReportRequest : "AD",
		SettlementInstructions : "T",
		MarketDataIncrementalRefresh : "X",
		ExecutionReport : "8",
		MassQuoteAcknowledgement : "b",
		MarketDataSnapshotFullRefresh : "W",
		SecurityList : "y",
		OrderMassStatusRequest : "AF",
		IndicationOfInterest : "6",
		SettlementInstructionRequest : "AV",
		OrderCancelRequest : "F",
		AssignmentReport : "AW",
		ListCancelRequest : "K",
		MarketDataRequest : "V",
		UserResponse : "BF",
		ActivationResponse : "U6",
		RegistrationInstructions : "o",
		Notification : "U1",
		OrderStatusRequest : "H",
		Reject : "3",
		CollateralInquiryAck : "BG",
		QuoteRequestReject : "AG",
		CollateralRequest : "AX",
		Logout : "5",
		SecurityStatus : "f",
		DontKnowTrade : "Q",
		ActivationRequest : "U5",
		AllocationInstruction : "J",
		ListStatus : "N",
		OvernightReport : "U4",
		SecurityStatusRequest : "e",
		MarketDataRequestReject : "Y",
		CrossOrderCancelRequest : "u",
		SecurityTypes : "w",
		InstrumentPositionInfo : "U3",
		QuoteRequest : "R",
		MassQuote : "i",
		BidRequest : "k",
		NewOrderCross : "s",
		AllocationReport : "AS",
		NewOrderMultileg : "AB",
		TradingSessionStatus : "h",
		AccountInfo : "U2",
		OrderMassCancelRequest : "q",
		SecurityDefinitionRequest : "c",
		Confirmation : "AK",
		Quote : "S",
		NewOrderList : "E",
		Advertisement : "7",
		ListStatusRequest : "M",
		ListStrikePrice : "m",
		UserRequest : "BE",
		MultilegOrderCancelReplaceRequest : "AC",
		TradeCaptureReportAck : "AR",
		PositionMaintenanceRequest : "AL",
		TradeCaptureReportRequestAck : "AQ",
		SecurityTypeRequest : "v",
		AllocationInstructionAck : "P",
		QuoteStatusReport : "AI",
		Heartbeat : "0",
		CollateralAssignment : "AY",
		SequenceReset : "4",
		OrderCancelReplaceRequest : "G",
		BusinessMessageReject : "j",
		CrossOrderCancelReplaceRequest : "t",
}

for k in MSGTYPE.keys()[:]: MSGTYPE[MSGTYPE[k]] = k
