# -*- encoding: utf-8 -*- 
'''
Created on Jun 18, 2012

@author: gdtlive
'''
import os
import socket
import ssl
import logging
import traceback
import time
import threading
from datetime import datetime
from engine import FIXEngine
from gdtlive.utils import is_weekend
from gdtlive.core.constants import *
import gdtlive.config as config
import dukascopy.fix as fix 
from logging.handlers import RotatingFileHandler
from gdtlive.utils import OrderedDict
    
    
class FIXInitiator(FIXEngine):
    def __init__(self, host, port, sendercompid, targetcompid, message_handler=None, retries=5, use_ssl=True):
        FIXEngine.__init__(self, message_handler)                
        self._wait_between_reconnect = 20.0 
        self.host = host
        self.port = port
        self.use_ssl = use_ssl
        self.connection_retries = retries
        self.TargetCompID = targetcompid
        self.SenderCompID = sendercompid
        self.relogin_initiator = True
        self.username = None
        self.password = None
        self.logout_received_count = 0
        self.logout_received_max = 3
        #self.name = 'FIXInitiator-%d (%s:%d)' % (self.thread_num, self.host, self.port)
        self.name = 'FIXInitiator-%d %s' % (self.thread_num, sendercompid)
        self.thread_num += 1
        self.connect_lock = threading.Lock()
        self.logon_lock = threading.Lock()
        self.connectionloss_lock = threading.Lock()
        self._init_logging()
        
        
    def connect(self, retries = 0):        
        if self.connect_lock.acquire(False):
            self.log.info('connect_lock acquired')
            if self.connection_state != STATE_NOT_CONNECTED:
                self.log.warning('Connection state is %s, no need for reconnection' % CONNECTION_STATE[self.connection_state])
                return True
                                                
            self.last_testreqid = None
            self.last_in_time = 0            
            if retries: 
                self.connection_retries = retries
                
            if self.logout_received_count >= self.logout_received_max:
                self.logout_received_count = 0
                self.log.warn('Maximum number of logout messages received, going to sleep')
                time.sleep(60)
                self.log.warn('Dukascopy, wake the fuck up!!!!')
                
                #self.connect_lock.release()
                #self.log.info('connect_lock released') 
                #return False
            
            while self.connection_retries:
                try:                      
                    self.connection_state = STATE_NOT_CONNECTED
                    self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    if self.use_ssl:
                        self.socket = ssl.wrap_socket(self.socket)        
                    # A connect() örökké bennemarad, ha ezt nem állítjuk be
                    self.socket.settimeout(self._socket_timeout)
                                              
                    self.log.info('connecting')
                    self.socket.connect((self.host, self.port))                
                    self.log.info('connection made to %s:%d' % (self.host, self.port))
                    if self.sender_thread:
                        self.sender_thread.last_out_time = 0
                        self.sender_thread.socket = self.socket                    
                    self.connection_state = STATE_CONNECTED
                    self.read_buffer = ''
                    break
                except:
                    if self._shutdown :
                        break
                                        
                    if is_weekend():
                        self.log.warning('its weekend, the other side is down...')
                        break
                    if not self.connection_retries:
                        self.log.error('connection failed, no retries left')
                        break
                                                                        
                    if self.connection_retries > 0:
                        self.connection_retries -= 1                                        
                        if not self.connection_retries:
                            self.log.error('connection failed, no retries left')
                            break
                        else:
                            self.log.warn('connection failed, retries left: %d' % (self.connection_retries))
                        
                    if self.connection_retries < 0 :
                        self.log.warn('connection failed, retrying')
                        
                    time.sleep(self._wait_between_reconnect)
                    
            self.connect_lock.release()
            self.log.info('connect_lock released')
        else:
            self.log.warn('failed to acquire lock!')
        
        
        return self.connection_state == STATE_CONNECTED
        
   
    def login(self, username, password, wait=True):
        if self.logon_lock.acquire(False):
            if self.connection_state != STATE_CONNECTED:
                self.log.warning('Connection state %s is not appropriate for sending Logon message' % CONNECTION_STATE[self.connection_state])
                return True
            
            self.log.info('logon_lock acquired')                
            self.username = username if username else None
            self.password = password
            self.log.info('sending Logon message')
            self.sender_thread.store.reset()
            self.store.reset()
            self.connection_state = STATE_LOGON_INITIATED
            self.events['login_succeded'].clear() 
            self.login_sent = datetime.utcnow()
            self.send(fix.Logon(Username=self.username, Password=self.password, HeartBtInt=self.heartbeat_int, ResetSeqNumFlag=True, EncryptMethod=0))            
            
            if wait:
                self.events['login_succeded'].wait()
                try:
                    self.logon_lock.release()
                except:
                    pass
                self.log.info('logon_lock released')                
                return self.connection_state == STATE_SESSION_ESTABLISHED
            else:
                try:
                    self.logon_lock.release()
                except:
                    pass
                self.log.info('logon_lock released')
        else:
            self.log.debug('failed to acqurie logon_lock')
        

    def logout(self):
        self.log.info('starting logout process')
        self.connection_state = STATE_LOGOUT_INITIATED
        self.send_testrequest()
        self.log.info('waiting for TestRequest to be confirmed')
        self.events['test_request_confirmed'].wait()
        self.log.info('sending Logout' % self.name)
        self.events['logout_succeded'].clear()
        self.send(fix.Logout())
        self.log.info('waiting for Logout to be confirmed')
        self.events['logout_succeded'].wait()   


    def check_connection(self):                 
        if not is_weekend():        
            self.log.debug('Connection state is: %s' % CONNECTION_STATE[self.connection_state])
            if self.connection_state == STATE_NOT_CONNECTED:
                self.logout_received_count = 0                        
                self.connect(10)
                self.connection_retries = -1     
                                    
            if self.connection_state == STATE_CONNECTED:
                if not self.sender_thread:
                    self.start()
                    time.sleep(2)                    
                self.login(self.username, self.password, True)
                                    
            
    def shutdown(self):
        self.connection_retries = 0
        self.shutdown_reason = ENGINE_SHUTDOWN_CLIENT
        FIXEngine.shutdown(self)
        
                             
    def _handle_connection_loss(self):
        self.log.debug('entering _handle_connection_loss')
#        if self.connectionloss_lock.acquire(False):        
        if self._shutdown:
            return False
        
        self.connection_state = STATE_NOT_CONNECTED
        
        self.log.warn('connection lost, reconnecting')
        try:
            #Ha a Login folyamatban jon a mumus, akkor fel kell szabaditani a lock-ját, hogy újrakezdhessük a logint
            self.logon_lock.release()
        except:
            pass              
        
        if self.callbacks['on_disconnect']:
            try:
                self.callbacks['on_disconnect']()
            except:
                self.log.error(traceback.format_exc())
        
        if not is_weekend():
            time.sleep(1)                  
            if self.connect(10):              
                self.login(self.username, self.password, wait=False)
                return True
            else:
                self.log.warn('Connection failed, cant do relogin!')
        else:
            self.log.info('')
        try:
            self.connectionloss_lock.release()
        except:
            pass
        
        return False

#        else:
#            self.log.debug('failed to acgiure lock, reconnection is in progress')
    
 
    def _handle_logon(self, message):                        
        if self.connection_state == STATE_LOGON_INITIATED:
            self.log.info('logon succeded')            
            self.logout_received_count = 0                    
            self.heartbeat_int = message.HeartBtInt
            if message.ResetSeqNumFlag:                
                self.store += message
                self.store.processed(message)                
            self.log.info('FIX Session established')
            self.connection_state = STATE_SESSION_ESTABLISHED
            self.events['login_succeded'].set()
            if self.callbacks['on_logon']:
                try:
                    self.callbacks['on_logon']()
                except:
                    self.log.error(traceback.format_exc())
                
                
        elif self.connection_state == STATE_SESSION_ESTABLISHED:
            if message.ResetSeqNumFlag:                
                self.log.info('Logon with ResetSeqNumFlag==True received')
                self.store.reset()
                self.sender_thread.store.reset()
                self.store += message
                self.store.processed(message)
                self.heartbeat_int = message.HeartBtInt
                self.send(fix.Logon(HeartBtInt=message.HeartBtInt, ResetSeqNumFlag=True, EnrcyptMethod=0))
                if self.callbacks['on_logon']:
                    try:
                        self.callbacks['on_logon']()
                    except:
                        self.log.error(traceback.format_exc())
                    
            else:
                self.log.warn('Logon recived, dont know what to do with it... ')
                           
        return True
    
    
    def _handle_logout(self, message):        
        if self.connection_state == STATE_LOGOUT_INITIATED:
            self.connection_state = STATE_CONNECTED
            self.events['logout_succeded'].set()
            self.log.info('Logout confirmed')
        
        elif self.connection_state == STATE_SESSION_ESTABLISHED:
            self._terminate_session('', True, True)            
        
        elif self.connection_state == STATE_LOGON_INITIATED:
            self.connection_state = STATE_CONNECTED
            self.logout_received_count += 1
            self.log.info('%d. logout received' % self.logout_received_count)            
            self.events['login_succeded'].set()
            
        elif self.connection_state == STATE_CONNECTED:
            # ???? 
            pass
        

    def _send_resendrequest(self, begin, end):
        self.send(fix.ResendRequest(BeginSeqNo=begin, EndSeqNo=end))
        # ha a LOGOUT üzenetünk okoz GapFill-t az Acceptornál, 
        # akkor az újrakéri a kimaradt üzeneteket, 
        # és újra kell küldeni neki a Logout-ot megerősítésképpen
        if self.connection_state == STATE_LOGOUT_INITIATED:
            self.send(fix.Logout())
            

    def _init_logging(self):        
        name = 'gdtlive.core.engine.%s' % self.SenderCompID
        self.log = logging.getLogger(name)        
        self.log.setLevel(logging.DEBUG)
        self._remove_log_handlers()        
        self._add_log_handler(name, logging.INFO)
        self._add_log_handler(name, logging.WARN)
        self._add_log_handler(name, logging.DEBUG)        
    
    def _remove_log_handlers(self):
        handlers = self.log.handlers[:]
        for h in handlers:
            self.log.removeHandler(h)    
            
    def _add_log_handler(self, name, level):
        handler = RotatingFileHandler(config.LOG_PATH + os.sep + name + '.' + logging.getLevelName(level) + '.log','w+',20*1024*1024, LOG_KEEP_FILES)
        handler.setFormatter(logging.Formatter(LOG_FORMAT))
        handler.setLevel(level)        
        self.log.addHandler(handler)
    
    
    def get_engine_details(self):
        result = OrderedDict()
        result['Server'] = '%s:%d, ssl: %s' % (self.host, self.port, self.use_ssl)
        result['SenderCompID'] = self.SenderCompID 
        result['TagetCompID'] = self.TargetCompID
        
        result['Connection state'] = CONNECTION_STATE[self.connection_state]                
        
        result['HeartBeat interval'] = '%ds' % self.heartbeat_int
        
        result['Last incoming message time'] = "%s" % self.last_in_time
        try:
            result['Expected incoming MsgSeqNum'] = '%d' % self.store.expected_seqnum                         
            result['Last processed MsqSeqNum'] = "%d" % self.store.last_processed_seqnum        
        except:
            result['Expected incoming MsgSeqNum'] = '0'                         
            result['Last processed MsqSeqNum'] = "0"
                    
        try: 
            result['Last outgoing message time'] = "%s" % self.sender_thread.last_out_time
            result['Last outgoing MsgSeqNum'] = '%d' % self.store.expected_seqnum                                        
        except:
            result['Last outgoing message time'] = ""
            result['Last outgoing MsgSeqNum'] = '0'
        
        result['logout_received_count'] = "%d" % self.logout_received_count
        result['logout_received_max'] = "%d" % self.logout_received_max
                
        return result
            
    
    def get_last_messages(self):        
        incoming = {}
        outgoing = {}
        if self.sender_thread:
            outgoing = self.sender_thread.store.get_last(20)
        incoming = self.store.get_last(20)
        
        messages = {}
        for msg in incoming.values():
            messages[msg._received_time] = 'IN:%s' % msg
        for msg in outgoing.values():
            messages[msg.SendingTime] = 'OUT:%s' % msg        
        
        result = OrderedDict()
        times = sorted(messages.keys())
        for t in times:            
            p, c, m = messages[t].partition(':')
            result[t.strftime('%Y-%m-%d %H:%M:%S')+ '   '+p+':'] = m
                
        return result
    
    
    def get_engine_logs(self, logtype):
        result = ''    
        LTYPE = {0: 'DEBUG', 1:'INFO',2:'WARNING'}                 
        try:
            logfile = '/gdtlive.core.engine.%s.%s.log' % (self.SenderCompID, LTYPE[logtype])
            with open(config.LOG_PATH + logfile) as f:
                r = f.readlines()                        
                r = r[-20:]
                for line in r:
                    d, level, name, message = line.split(' - ', 3)
                    result += d + ' - ' + level + ' - ' + message
                #result = ''.join(r) 
        except:
            print traceback.format_exc()
            
        return result
        
        