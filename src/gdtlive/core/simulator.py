# -*- encoding: utf-8 -*- 
'''
Created on 2012.06.01.

@author: kulcsarb
'''
#from gdtlive.constants import *
import gdtlive.store.db as db
from threading import Thread
import logging
import historic
import numpy as np
from fixengine import FIXAcceptor, FIXAcceptorServer
from datetime import datetime, timedelta
from fix.messages import OrderSingleNewMessage, ExecutionReportMessage, OrderCancelRequestMessage, AccountInfoRequestMessage, AccountInfoMessage
from gdtlive.constants import PRICE_TIME, PRICE_ASKOPEN, PRICE_ASKHIGH, PRICE_ASKLOW, PRICE_ASKCLOSE, PRICE_BIDOPEN, PRICE_BIDHIGH, PRICE_BIDLOW, PRICE_BIDCLOSE, PRICE_VOLUME, PRICE_PIPVALUE, PIP_MULTIPLIER
from gdtlive.config import PRELOAD_DAYS
import time
import random
import gdtlive.historic.datarow as datarow

log = logging.getLogger('gdtlive.core.simulator')


class SampleFIXServer(FIXAcceptor):
    def __init__(self, socket, addr, message_handler = None):
        FIXAcceptor.__init__(self, socket, addr, message_handler)
        self.buy_stops = {}
        self.sell_stops = {}
        self.buy_limits = {}
        self.sell_limits = {}
        self.amounts = {}
        self.exposure = {}      
        self.base_equity = 10000
        self.leverage = 30
        self.use_of_leverage = 0
        
    def config(self, base_equity, leverage):
        self.base_equity = base_equity
        self.leverage = leverage
                        
    def get_useofleverage(self):
        return sum(self.exposure.values())

    def close_profit(self, symbol):
        sum_base = self.amounts[symbol][1]
        self.amounts[symbol][1] = 0
        if symbol.endswith('USD'):
            sum_usd = sum_base                
        elif symbol.startswith('USD'):
            sum_usd = sum_base / historic.CURRENT_PRICE[symbol]['ASK']
        else:
            symbol_3A = symbol[:3] + 'USD'
            symbol_3B = 'USD' + symbol[:3]
            
            if symbol_3A in historic.CURRENT_PRICE:                    
                sum_usd = sum_base * historic.CURRENT_PRICE[symbol_3A]['ASK']
            elif symbol_3B in historic.CURRENT_PRICE:
                sum_usd = sum_base / historic.CURRENT_PRICE[symbol_3B]['ASK']
            else:
                sum_usd = 0
                print 'FUCKUP'                                                            

        self.base_equity += sum_usd
                                

    def calc_floating_profit(self):
        total_floating = 0         
        for symbol in self.amounts:
            sum_base = self.amounts[symbol][1]                        
            if self.amounts[symbol][0] > 0:
                sum_base += self.amounts[symbol][0] * historic.CURRENT_PRICE[symbol]['BID']
            elif self.amounts[symbol][0] < 0:
                sum_base -= self.amounts[symbol][0] * historic.CURRENT_PRICE[symbol]['ASK']
                                
            if symbol.endswith('USD'):
                sum_usd = sum_base                
            elif symbol.startswith('USD'):
                sum_usd = sum_base / historic.CURRENT_PRICE[symbol]['ASK']
            else:
                symbol_3A = symbol[:3] + 'USD'
                symbol_3B = 'USD' + symbol[:3]
                
                if symbol_3A in historic.CURRENT_PRICE:                    
                    sum_usd = sum_base * historic.CURRENT_PRICE[symbol_3A]['ASK']
                elif symbol_3B in historic.CURRENT_PRICE:
                    sum_usd = sum_base / historic.CURRENT_PRICE[symbol_3B]['ASK']
                else:
                    sum_usd = 0
                    print 'FUCKUP'                                                            
            total_floating += sum_usd
                        
        
        self.calc_use_of_leverage(total_floating)
        return total_floating              
            
    def calc_use_of_leverage(self, floating_profit):
        total_leverage = (self.base_equity + floating_profit) * self.leverage
        exposure_sum = sum(self.exposure.values())
        self.use_of_leverage = (exposure_sum/total_leverage)*100
        #print self.base_equity, floating_profit, total_leverage, exposure_sum, '%.2f' % self.use_of_leverage
        
        
    def calc_exposure(self):
        for symbol in self.amounts:        
            exposure = self.amounts[symbol][0]
            if symbol.endswith('USD'):  #    last currency is USD
                exposure = exposure * historic.CURRENT_PRICE[symbol]['ASK']
            elif 'USD' not in symbol:   # neither currency is USD
                symbol3A = symbol[:3] + 'USD'
                symbol3B = 'USD' + symbol[3:]
                if symbol3A in historic.CURRENT_PRICE:                
                    baserate = historic.CURRENT_PRICE[symbol3A]['ASK']
                elif symbol3B in historic.CURRENT_PRICE:
                    baserate = historic.CURRENT_PRICE[symbol3B]['ASK']
                else:
                    baserate = 1.0 
                exposure = exposure * baserate
                
            self.exposure[symbol] = exposure
            
        return exposure
    
      
    def in_usd(self, symbol, amount, direction):
        if symbol.endswith('USD'):  #    last currency is USD
            if direction == 'Buy':
                exposure = amount * historic.CURRENT_PRICE[symbol]['ASK']
            else:
                exposure = amount * historic.CURRENT_PRICE[symbol]['BID']
        elif 'USD' not in symbol:   # neither currency is USD
            symbol3A = symbol[:3] + 'USD'
            symbol3B = 'USD' + symbol[3:]
            if symbol3A in historic.CURRENT_PRICE:                
                baserate = historic.CURRENT_PRICE[symbol3A]['ASK']
            elif symbol3B in historic.CURRENT_PRICE:
                baserate = historic.CURRENT_PRICE[symbol3B]['ASK']
            else:
                baserate = 1.0 
            exposure = amount * baserate
        return exposure
      
      
    def handle_application_message(self, message):
        if type(message) == OrderSingleNewMessage:
            #print 'NEW ORDER RECEIVED', message   
            if message.OrdType=='Market':
                reply = ExecutionReportMessage()
                reply.OrderID = time.time()
                reply.ClOrdID = message.ClOrdID
                reply.ExecID = random.randint(1,1000)
                reply.OrdStatus = 'Filled'
                reply.ExecType = 'Fill'
                reply.Symbol = message.Symbol
                reply.TimeInForce = message.TimeInForce
                reply.CumQty = message.OrderQty
                reply.LeavesQty = 0
                reply.OrderQty = message.OrderQty
                reply.Side = message.Side
                reply.OrdType = message.OrdType
                if message.Side == 'Buy':
                    reply.AvgPx = float(historic.CURRENT_PRICE[message.Symbol]['ASK'])
                else:
                    reply.AvgPx = float(historic.CURRENT_PRICE[message.Symbol]['BID'])              
                
                reply.TransactTime = historic.CURRENT_TIME + timedelta(seconds=random.randint(1,10))
                
                self.exposure.setdefault(message.Symbol, 0)
                self.amounts.setdefault(message.Symbol, [0,0])                
                if message.Side == 'Buy':
                    self.amounts[message.Symbol][0] += message.OrderQty
                    self.amounts[message.Symbol][1] -= message.OrderQty * reply.AvgPx                    
                else:
                    self.amounts[message.Symbol][0] -= message.OrderQty
                    self.amounts[message.Symbol][1] += message.OrderQty * reply.AvgPx

                self.calc_exposure()        
                if self.amounts[message.Symbol][0] == 0:
                    self.close_profit(message.Symbol)
                self.calc_floating_profit()                    
                      
                #print self.amounts
                #print self.exposure                                
                
                
                #print 'sending reply:', reply                
            if message.OrdType in ['Stop', 'Stop limit']:                
                reply = ExecutionReportMessage()
                reply.OrderID = time.time()
                reply.ClOrdID = message.ClOrdID
                reply.ExecID = random.randint(1,1000)
                reply.OrdStatus = 'Pending New'
                reply.ExecType = 'Fill'
                reply.Symbol = message.Symbol
                reply.TimeInForce = message.TimeInForce
                reply.CumQty = message.OrderQty
                reply.LeavesQty = 0
                reply.OrderQty = message.OrderQty
                reply.Side = message.Side
                reply.OrdType = message.OrdType
                reply.AvgPx = message.AvgPx
                reply.TransactTime = historic.CURRENT_TIME + timedelta(seconds=random.randint(1,10))
                if message.Side == 'Buy':        
                    if message.OrdType == 'Stop':
                        self.buy_stops[message.ClOrdID] = reply
                    else:
                        self.buy_limits[message.ClOrdID] = reply
                else:                    
                    if message.OrdType == 'Stop':
                        self.sell_stops[message.ClOrdID] = reply
                    else:
                        self.sell_limits[message.ClOrdID] = reply
        
                
        if type(message) == OrderCancelRequestMessage:
                
                if message.OrigClOrdID in self.buy_stops:
                    log.info('SIMULATOR: CANCEL Buy Stop ClOrdID: %s' % message.OrigClOrdID)
                    del self.buy_stops[message.OrigClOrdID]
                elif message.OrigClOrdID in self.sell_stops:
                    log.info('SIMULATOR: CANCEL Sell Stop ClOrdID: %s' % message.OrigClOrdID)
                    del self.sell_stops[message.OrigClOrdID]
                elif message.OrigClOrdID in self.buy_limits:
                    log.info('SIMULATOR: Cancel Buy Limit ClOrdID: %s' % message.OrigClOrdID)
                    del self.buy_limits[message.OrigClOrdID]
                elif message.OrigClOrdID in self.sell_limits:
                    log.info('SIMULATOR: Cancel Sell Limit ClOrdID: %s' % message.OrigClOrdID)
                    del self.sell_limits[message.OrigClOrdID]
                else:
                    print 'WTF???'
                                
                reply = ExecutionReportMessage()
                reply.OrderID = time.time()
                reply.ClOrdID = message.OrigClOrdID
                reply.ExecID = random.randint(1,1000)
                reply.OrdStatus = 'Canceled'
                reply.ExecType = 'Fill'
                reply.Symbol = message.Symbol                                                        
                reply.TransactTime = historic.CURRENT_TIME + timedelta(seconds=random.randint(1,10))            
            
        if type(message) == AccountInfoRequestMessage:
            reply = AccountInfoMessage()
            reply.Equity = self.base_equity + self.calc_floating_profit()
            reply.Leverage = self.leverage
            reply.UsableMargin = self.use_of_leverage
            
        self.send(reply)

    
    def _get_fill_message(self, message):
        reply = ExecutionReportMessage()
        reply.OrderID = time.time()
        reply.ClOrdID = message.ClOrdID
        reply.ExecID = random.randint(1,1000)
        reply.OrdStatus = 'Filled'
        reply.ExecType = 'Fill'
        reply.Symbol = message.Symbol
        reply.TimeInForce = message.TimeInForce
        reply.CumQty = message.OrderQty
        reply.LeavesQty = 0
        reply.OrderQty = message.OrderQty
        reply.Side = message.Side
        reply.OrdType = message.OrdType
        reply.AvgPx = message.AvgPx
        reply.TransactTime = historic.CURRENT_TIME + timedelta(seconds=random.randint(1,10))
        
        if message.Side == 'Buy':
            self.amounts[message.Symbol][0] += message.OrderQty
            self.amounts[message.Symbol][1] -= message.OrderQty * message.AvgPx
        else:
            self.amounts[message.Symbol][0] -= message.OrderQty
            self.amounts[message.Symbol][1] += message.OrderQty * message.AvgPx
        self.calc_exposure()
        if self.amounts[message.Symbol][0] == 0:
            self.close_profit(message.Symbol)
        self.calc_floating_profit()
        
#        print self.amounts
#        print self.exposure        
         
        return reply       


    def on_tick(self, candle_time):        
        self.calc_floating_profit()
        for ClOrdID in self.buy_stops.keys()[:]:
            message = self.buy_stops[ClOrdID]
            if message.AvgPx <= historic.CURRENT_PRICE[message.Symbol]['ASK']:
                log.info('SIMULATOR: FILL FOR BUY STOP  %s %.4f %.4f %s' % (message.ClOrdID, message.AvgPx, historic.CURRENT_PRICE[message.Symbol]['ASK'], historic.CURRENT_TIME))
                reply = self._get_fill_message(message)
                self.send(reply)
                reply.wait_until_processed()
                time.sleep(0.2)                                
                del self.buy_stops[ClOrdID]
                        
        for ClOrdID in self.buy_limits.keys()[:]:
            message = self.buy_limits[ClOrdID]
            if message.AvgPx >= historic.CURRENT_PRICE[message.Symbol]['ASK']:
                log.info('SIMULATOR: FILL FOR BUY LIMIT  %s %.4f %.4f %s' % (message.ClOrdID, message.AvgPx, historic.CURRENT_PRICE[message.Symbol]['ASK'], historic.CURRENT_TIME))
                reply = self._get_fill_message(message)
                self.send(reply)       
                reply.wait_until_processed()
                time.sleep(0.2)                         
                del self.buy_limits[ClOrdID]                
        
        for ClOrdID in self.sell_stops.keys()[:]:
            message = self.sell_stops[ClOrdID]
            if message.AvgPx >= historic.CURRENT_PRICE[message.Symbol]['ASK']:
                log.info('SIMULATOR: FILL FOR SELL STOP %s %.4f %.4f %s' % (message.ClOrdID, message.AvgPx, historic.CURRENT_PRICE[message.Symbol]['ASK'], historic.CURRENT_TIME))                
                reply = self._get_fill_message(message)
                self.send(reply)
                reply.wait_until_processed()          
                time.sleep(0.2)
                del self.sell_stops[ClOrdID]        
        
        for ClOrdID in self.sell_limits.keys()[:]:
            message = self.sell_limits[ClOrdID]
            if message.AvgPx <= historic.CURRENT_PRICE[message.Symbol]['ASK']:
                log.info('SIMULATOR: FILL FOR SELL LIMIT %s %.4f %.4f %s' % (message.ClOrdID, message.AvgPx, historic.CURRENT_PRICE[message.Symbol]['ASK'], historic.CURRENT_TIME))
                reply = self._get_fill_message(message)
                self.send(reply)       
                reply.wait_until_processed()
                time.sleep(0.2)                         
                del self.sell_limits[ClOrdID]
        

class ServerSimulator(Thread):

    def __init__(self, from_date, to_date):
        Thread.__init__(self)        
        self.onclose_handler = None
        self.ontick_handler = None        
        self.from_date = from_date
        self.to_date = to_date    
        
        # elinditjuk a szimulált FIX szervert
        self.server = FIXAcceptorServer(ENGINE_HOST, ENGINE_PORT, SampleFIXServer)
        self.server.start()
        
        self.m1candle_num = 0
        self.DATAROWS = {}
        self.m1_datarows = {}
        self.max_length = 0
        
        self.live_manager = None
        self.instruments = []
                        
                
    def add_handler(self, handler):
        self.live_manager = handler
                
        
    def register_instrument(self, symbol, timeframe, max_length):
        #self.max_length = max(self.max_length, max_length*2)
        #self.max_length = 1200
        session = db.Session()                        
        
        if 'USD' not in symbol:
            symbol_3A = symbol[:3] + 'USD'
            dr = session.query(db.DatarowDescriptor).filter(db.DatarowDescriptor.symbol == symbol_3A).filter(db.DatarowDescriptor.timeframe_num==1).first()
            if dr:
                log.info('Loading pipvalue datarow: %s' % symbol_3A)
                self.load_instrument(symbol_3A, timeframe)
            else:
                symbol_3B = 'USD' + symbol[:3] 
                dr = session.query(db.DatarowDescriptor).filter(db.DatarowDescriptor.symbol == symbol_3B).filter(db.DatarowDescriptor.timeframe_num==1).first()
                if dr:
                    log.info('Loading pipvalue datarow: %s' % symbol_3B)
                    self.load_instrument(symbol_3B, timeframe)
                else:
                    log.error('Cant load datarow for pipvalue calculation for %s' % symbol)
                    session.close()
                    return False
        
        if symbol not in self.m1_datarows:
            
            dr = session.query(db.DatarowDescriptor).filter(db.DatarowDescriptor.symbol == symbol).filter(db.DatarowDescriptor.timeframe_num==1).first()
            if dr:                
                #### JAJJ 
                #
                # TODO: adatsorszűrés, vagy mifasz kéne !!
                #preload_date = self.from_date - timedelta(seconds = 60 * timeframe * self.max_length)
                preload_date = self.from_date - timedelta(days=PRELOAD_DAYS)
                #preload_date = self.from_date
                self.m1_datarows[symbol] = datarow.load(dr.id, preload_date, self.to_date)
                self.m1candle_num = len(self.m1_datarows[symbol][PRICE_TIME])                                                                                                    
                log.info('M1 loaded for %s' % symbol)                      
            else:                
                log.error('M1 datarow not found for %s' % symbol)
                return False        
        
        
        if timeframe not in historic.DATAROWS:
            historic.DATAROWS[timeframe] = {}
            self.DATAROWS[timeframe] = {}
        
        if symbol not in historic.DATAROWS[timeframe]:
            historic.DATAROWS[timeframe][symbol] = {
                    PRICE_TIME : [],
                    PRICE_ASKOPEN : [],
                    PRICE_ASKHIGH : [],
                    PRICE_ASKLOW : [],
                    PRICE_ASKCLOSE : [],
                    PRICE_BIDOPEN : [],
                    PRICE_BIDHIGH : [],
                    PRICE_BIDLOW : [],
                    PRICE_BIDCLOSE : [],
                    PRICE_VOLUME : [],
                    PRICE_PIPVALUE : []
                    }
            self.DATAROWS[timeframe][symbol] = {
                    PRICE_TIME : [],
                    PRICE_ASKOPEN : [],
                    PRICE_ASKHIGH : [],
                    PRICE_ASKLOW : [],
                    PRICE_ASKCLOSE : [],
                    PRICE_BIDOPEN : [],
                    PRICE_BIDHIGH : [],
                    PRICE_BIDLOW : [],
                    PRICE_BIDCLOSE : [],
                    PRICE_VOLUME : [],
                    PRICE_PIPVALUE : []
                    }
                                
        session.close()    
        return True 
        
        
    def run(self):
        
        if not self.live_manager:
            log.error('handlers not defined! ')
            return                
                        
        for symbol in self.m1_datarows.keys():
            historic.CURRENT_PRICE[symbol] = {'ASK' : 0, 'BID':0,'PIPVALUE':0}

        candle_time = self.m1_datarows[self.m1_datarows.keys()[0]][PRICE_TIME]
                
        #print candle_time[start_index]
        
        current = {}
        for tf in self.DATAROWS.keys():
            current[tf] = {}
            for sym in self.DATAROWS[tf].keys():
                current[tf][sym] = {}
                current[tf][sym][PRICE_PIPVALUE] = 0
                for price in self.m1_datarows[sym].keys():                    
                    current[tf][sym][price] = self.m1_datarows[sym][price][0]                    
                    #print tf, sym, price, current[tf][sym][price]                                                                      
                                                                                 
        
        for i in xrange(1, self.m1candle_num):    
                            
            for timeframe in sorted(self.DATAROWS.keys()):
                                                                        
                if candle_time[i].minute % timeframe == 0:
                    for symbol in self.DATAROWS[timeframe].keys():
                        # Az összegyüjtött gyertyát hozzáadjuk a candle tömbhöz
                        for price in current[timeframe][symbol].keys():
                            self.DATAROWS[timeframe][symbol][price].append(current[timeframe][symbol][price])                            
                            if current[timeframe][symbol][PRICE_TIME] >= self.from_date:
                                del self.DATAROWS[timeframe][symbol][price][0]
                                                        
                            # Átalakitjuk numpy array-é a DNS-ek kiértékeléséhez                        
                            if price == PRICE_TIME:
                                historic.DATAROWS[timeframe][symbol][price] = self.DATAROWS[timeframe][symbol][price]
                            else:
                                historic.DATAROWS[timeframe][symbol][price] = np.array(self.DATAROWS[timeframe][symbol][price], dtype=np.double)                                
                        
                        #print symbol, timeframe, current[timeframe][symbol][PRICE_TIME], 
                        #print current[timeframe][symbol][PRICE_ASKOPEN], current[timeframe][symbol][PRICE_ASKHIGH], 
                        #print current[timeframe][symbol][PRICE_ASKLOW], current[timeframe][symbol][PRICE_ASKCLOSE]
                        
                        historic.CURRENT_PRICE[symbol]['ASK'] = current[timeframe][symbol][PRICE_ASKCLOSE]
                        historic.CURRENT_PRICE[symbol]['BID'] = current[timeframe][symbol][PRICE_BIDCLOSE]                                                
                        historic.CURRENT_TIME = current[timeframe][symbol][PRICE_TIME]
                        
                        # Reseteljük az aktuális gyertya állapotát
                        for price in self.m1_datarows[symbol].keys():                            
                            current[timeframe][symbol][price] = self.m1_datarows[symbol][price][i]                                                                
                    
                    # eseménykezelő meghivása
                      
                    log.info(' ------------ candle %s ------------- ' % current[timeframe][current[timeframe].keys()[0]][PRICE_TIME])                    
                    if self.live_manager and historic.CURRENT_TIME >= self.from_date:
                        self.live_manager.evaluate(timeframe)                                                                                                                      
                                                                                            
                else:
                    # minden regisztrált instrumentumon elvégezzük az adatok aggregálását
                    for symbol in self.DATAROWS[timeframe].keys():                                                
                        current[timeframe][symbol][PRICE_ASKCLOSE] = self.m1_datarows[symbol][PRICE_ASKCLOSE][i]
                        current[timeframe][symbol][PRICE_BIDCLOSE] = self.m1_datarows[symbol][PRICE_BIDCLOSE][i]              
                        current[timeframe][symbol][PRICE_ASKHIGH] = max(current[timeframe][symbol][PRICE_ASKHIGH], self.m1_datarows[symbol][PRICE_ASKHIGH][i])
                        current[timeframe][symbol][PRICE_ASKLOW] = min(current[timeframe][symbol][PRICE_ASKLOW], self.m1_datarows[symbol][PRICE_ASKLOW][i])
                        current[timeframe][symbol][PRICE_BIDHIGH] = max(current[timeframe][symbol][PRICE_BIDHIGH], self.m1_datarows[symbol][PRICE_BIDHIGH][i])
                        current[timeframe][symbol][PRICE_BIDLOW] = min(current[timeframe][symbol][PRICE_BIDLOW], self.m1_datarows[symbol][PRICE_BIDLOW][i])
                        current[timeframe][symbol][PRICE_VOLUME] += self.m1_datarows[symbol][PRICE_VOLUME][i]                    
                        #print current[timeframe][symbol]

                        historic.CURRENT_PRICE[symbol]['ASK'] = current[timeframe][symbol][PRICE_ASKCLOSE]
                        historic.CURRENT_PRICE[symbol]['BID'] = current[timeframe][symbol][PRICE_BIDCLOSE]
                    
                    for symbol in self.DATAROWS[timeframe].keys():
                        if symbol.endswith('USD'):
                            pipvalue = PIP_MULTIPLIER[symbol]
                        elif symbol.startswith('USD'):
                            pipvalue = PIP_MULTIPLIER[symbol] / historic.CURRENT_PRICE[self.symbol]['ASK']
                        else:
                            symbol_3B = 'USD' + self.symbol[:3]   # CASE 3.B : account currency as long 
                            symbol_3A = self.symbol[:3] + 'USD'    # CASE 3.A : account currency as short
                            if symbol_3A in historic.CURRENT_PRICE:
                                pipvalue = PIP_MULTIPLIER[symbol_3A] * historic.CURRENT_PRICE[symbol_3A]['ASK'] 
                            elif symbol_3B in historic.CURRENT_PRICE:
                                pipvalue = PIP_MULTIPLIER[symbol_3B] /  historic.CURRENT_PRICE[symbol_3B]['ASK']
                            else:
                                pipvalue = 0.0001
                                
                        historic.CURRENT_PRICE[symbol]['PIPVALUE'] = pipvalue
                        current[timeframe][symbol][PRICE_PIPVALUE] = pipvalue
                                                                    
            if self.live_manager and historic.CURRENT_TIME and historic.CURRENT_TIME >= self.from_date:
                for symbol in self.m1_datarows.keys():                    
                    historic.CURRENT_PRICE[symbol]['ASK'] = self.m1_datarows[symbol][PRICE_ASKCLOSE][i]
                    historic.CURRENT_PRICE[symbol]['BID'] = self.m1_datarows[symbol][PRICE_BIDCLOSE][i]                
                self.live_manager.on_tick()                    
                self.server.server_threads[0].on_tick(candle_time[i])
                        
            if candle_time[i].day != candle_time[i-1].day:
                if self.live_manager and historic.CURRENT_TIME and historic.CURRENT_TIME > self.from_date:                    
                    self.live_manager.on_new_day()                                             
        
        
    def shutdown(self):        
        self.server.shutdown()
        
        
if __name__ == '__main__':
    pass
    