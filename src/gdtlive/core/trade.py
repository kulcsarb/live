# -*- encoding: utf-8 -*- 
'''
Created on Jul 11, 2012

@author: gdtlive
'''
from gdtlive.utils import open_db_connection
from gdtlive.core.constants import *
from gdtlive.constants import DIR_BUY, DIR_SELL, DIR_STR, PIP_MULTIPLIER, PRICES, ORDERTYPE_CLOSE, ORDERTYPE_CLOSE_BY_TP, ORDERTYPE_CLOSE_BY_SL, ORDERTYPE_FORCE_CLOSE
import gdtlive.core.fix.dukascopy.fix as fix
import gdtlive.core.datafeed.historic as historic
from datetime import datetime, timedelta
import traceback
import calendar
import threading
import logging
import time
import copy
import gdtlive.store.db as db
import gdtlive.core.sqlupdater as sql


MARKETORDER_CREATED = 0 
MARKETORDER_OPEN_SENT = 1
MARKETORDER_OPEN_FAILED = 2
MARKETORDER_OPEN_REJECTED = 3 
MARKETORDER_PARTIALLY_FILLED = 4
MARKETORDER_FILLED = 5
MARKETORDER_CLOSE_SENT = 6 
MARKETORDER_CLOSE_FAILED = 7
MARKETORDER_CLOSE_REJECTED = 8
MARKETORDER_PARTIALLY_CLOSED = 9 
MARKETORDER_CLOSED = 10
MARKETORDER_CORRECTION_SENT = 11
MARKETORDER_CORRECTION_FAILED = 12
MARKETORDER_CORRECTION_REJECTED = 13
MARKETORDER_CORRECTION_FILLED = 14
MARKETORDER_ABORTED = 15 
MARKETORDER_UNKNOWN_CLOSED = 16


CONDORDER_NOT_USED = -1
CONDORDER_CREATED = 0
CONDORDER_ENTRY_SENT = 1  
CONDORDER_ENTRY_FAILED = 2
CONDORDER_ENTRY_REJECTED = 3
CONDORDER_PENDING_NEW = 4
CONDORDER_PARTIALLY_FILLED = 5
#CONDORDER_OVER_FILLED = 13  
CONDORDER_FILLED = 6
CONDORDER_CANCEL_SENT = 7 
CONDORDER_CANCEL_FAILED = 8
CONDORDER_CANCEL_REJECTED = 9
CONDORDER_CANCELED = 10
CONDORDER_UNKNOWN = 11
CONDORDER_UNKNOWN_CLOSED = 12



MARKETORDER_STATES = {
             MARKETORDER_CREATED : 'CREATED',
             MARKETORDER_OPEN_SENT : 'OPEN_SENT',
             MARKETORDER_OPEN_FAILED : 'OPEN_FAILED',
             MARKETORDER_OPEN_REJECTED : 'OPEN_REJECTED', 
             MARKETORDER_PARTIALLY_FILLED : 'PARTIALLY_FILLED',
             MARKETORDER_FILLED :'FILLED',
             MARKETORDER_CLOSE_SENT : 'CLOSE_SENT',
             MARKETORDER_CLOSE_FAILED : 'CLOSE_FAILED',
             MARKETORDER_CLOSE_REJECTED : 'CLOSE_REJECTED',
             MARKETORDER_PARTIALLY_CLOSED : 'PARTIALLY_CLOSED', 
             MARKETORDER_CLOSED : 'CLOSED',
             MARKETORDER_CORRECTION_SENT : 'CORRECTION_SENT',
             MARKETORDER_CORRECTION_FAILED : 'CORRECTION_FAILED',
             MARKETORDER_CORRECTION_REJECTED : 'CORRECTION_REJECTED',
             MARKETORDER_CORRECTION_FILLED : 'CORRECTION_FILLED',
             MARKETORDER_ABORTED : 'ABORTED',
             MARKETORDER_UNKNOWN_CLOSED : 'UNKNOWN, CLOSED'                           
             }

CONDORDER_STATES = {
              CONDORDER_NOT_USED : 'NOT_USED',
              CONDORDER_CREATED : 'CREATED',
              CONDORDER_ENTRY_SENT : 'ENTRY_SENT',  
              CONDORDER_ENTRY_FAILED : 'ENTRY_FAILED',
              CONDORDER_ENTRY_REJECTED : 'ENTRY_REJECTED',
              CONDORDER_PENDING_NEW : 'PENDING_NEW',
              CONDORDER_PARTIALLY_FILLED : 'PARTIALLY_FILLED',  
              CONDORDER_FILLED : 'FILLED',
              CONDORDER_CANCEL_SENT : 'CANCEL_SENT', 
              CONDORDER_CANCEL_FAILED : 'CANCEL_FAILED',
              CONDORDER_CANCEL_REJECTED : 'CANCEL_REJECTED',
              CONDORDER_CANCELED : 'CANCELED',
              CONDORDER_UNKNOWN :'UNKNOWN',
              CONDORDER_UNKNOWN_CLOSED : 'UNKNOWN, CLOSED'                                      
            }






class MarketOrder(object):
        
    def __init__(self, trade):
        self.id = 0
        self.state = MARKETORDER_CREATED
        self.trade = trade
        self.trade_id = trade.id
        self.symbol = trade.symbol 
        self.amount = trade.amount
        self.current_amount = 0
                
        self.open_direction = trade.direction
        self.open_price = 0
        self.open_send_time = None
        self.open_fill_time = None
        self.open_clordid = None
        self.open_reject_count = ORDER_RESEND_COUNT
        
        self.close_direction = DIR_SELL if self.open_direction==DIR_BUY else DIR_BUY
        self.close_price = 0
        self.close_send_time = None        
        self.close_fill_time = None
        self.close_clordid = None
        self.close_reject_count = ORDER_RESEND_COUNT
        
        self.correction_amount = 0
        self.correction_price = 0
        self.correction_send_time = None        
        self.correction_fill_time = None
        self.correction_clordid = None
        self.correction_reject_count = ORDER_RESEND_COUNT
        
        self._clordid = 0
    
    def get_data(self):
        data = copy.copy(self.__dict__)
        del data['_clordid']
        del data['trade']
        return data
    
    def reset_counters(self):
        self.open_reject_count = ORDER_RESEND_COUNT
        self.close_reject_count = ORDER_RESEND_COUNT
        self.correction_reject_count = ORDER_RESEND_COUNT
        
        
    def load(self, marketorder_id):
        table = db.MarketOrder.__table__
        result = db.engine.execute(table.select(whereclause=table.c.id==marketorder_id))
        r = result.fetchone()
        for k,v in r.items():            
            setattr(self, k, v)
            
            
    def save(self):
        table = db.MarketOrder.__table__
        
        if self.id:
            data = copy.copy(self.__dict__)
            del data["trade"]
            command = table.update().where(table.c.id==self.id).values(**data)
            sql.server.update(command)

        else:
            data = copy.copy(self.__dict__)
            del data["id"]
            del data["trade"]
            command = table.insert().values(**data).returning(table.c.id)            
            self.id = sql.server.insert(command)
            
                        
        
    def register_orders(self):
        if self.open_clordid:
            self.trade.fix_account.register_order(self.trade, self.open_clordid)
        if self.close_clordid:
            self.trade.fix_account.register_order(self.trade, self.close_clordid)
        if self.correction_clordid:
            self.trade.fix_account.register_order(self.trade, self.correction_clordid)
            
        
    def __str__(self):
        s = 'MarketOrder on %s, amount: %d, current_amount: %d\n' % (self.symbol, self.amount, self.current_amount)
        s += '\tstate: %s\n' % MARKETORDER_STATES[self.state]
        s += '\topen (%s, %s, %s, %s, %s)\n' % (DIR_STR[self.open_direction], self.open_price, self.open_send_time, self.open_fill_time, self.open_clordid)        
        s += '\tclose (%s, %s, %s, %s, %s)\n' % (DIR_STR[self.close_direction], self.close_price, self.close_send_time, self.close_fill_time, self.close_clordid)
        return s
                
    def next_clordid(self):
        self._clordid += 1
        return 'TRADE #%d, MO #%d %d' % (self.trade.id, self.id, calendar.timegm(datetime.utcnow().utctimetuple()))
        
        
    def _symbol(self):
        return self.symbol[:3] + '/' + self.symbol[3:]
                   
                                                            
    def open(self):
        if self.state == MARKETORDER_OPEN_SENT:
            possible_resend = True
            self.trade.log.info('Resending open order width ClOrdID: %s' % self.open_clordid)
        else:
            possible_resend = False
            self.open_clordid = self.next_clordid()
            self.open_send_time = datetime.utcnow()                                                         
            self.trade.log.info('Sending open order with ClOrdID: %s' % self.open_clordid)
                                           
        success = self.trade.fix_account.send_marketorder(self.trade, self.open_clordid, self.open_direction, self.amount, self._symbol(), possible_resend)                
        self.state = MARKETORDER_OPEN_SENT if success else MARKETORDER_OPEN_FAILED        


    def resend_open(self):
        if self.open_clordid:
            self.open_send_time = datetime.utcnow()                   
            success = self.trade.fix_account.send_marketorder(self.trade, self.open_clordid, self.open_direction, self.amount, self._symbol(), True)                
            self.state = MARKETORDER_OPEN_SENT if success else MARKETORDER_OPEN_FAILED
        else:            
            self.trade.log.info('Nothing to resend')
                                                                    
                                                    
    def close(self):                    
        if self.state in [MARKETORDER_FILLED, MARKETORDER_CLOSE_SENT, MARKETORDER_CLOSE_REJECTED, MARKETORDER_CLOSE_FAILED, MARKETORDER_PARTIALLY_CLOSED]:
            
            if self.state == MARKETORDER_CLOSE_SENT:
                possible_resend = True
            else:
                possible_resend = False                    
                self.close_clordid = self.next_clordid() 
                self.close_send_time = datetime.utcnow()                                   
            
            self.trade.log.info('Sending close order')                                                
            success = self.trade.fix_account.send_marketorder(self.trade, self.close_clordid, self.close_direction, self.current_amount, self._symbol(), resend=possible_resend)                                                            
            self.state = MARKETORDER_CLOSE_SENT if success else MARKETORDER_CLOSE_FAILED
            
                                    
    def corrigate_double_fill(self):
        self.correction_clordid = self.next_clordid()
        if not self.correction_send_time: 
            self.correction_send_time = datetime.utcnow()
        
        self.trade.log.info('Corrigating double fill')
        success = self.trade.fix_account.send_marketorder(self.trade, self.correction_clordid, self.open_direction, self.amount, self._symbol())
        self.state = MARKETORDER_CORRECTION_SENT if success else MARKETORDER_CORRECTION_FAILED
        
    
    def simulate_open(self, amount, price, time):
        self._open_fill(amount, price, time)
            
    def simulate_close(self, amount, price, time):
        self._close_fill(amount, price, time)


    def handle_filled(self, clordid, amount, price, transact_time):
        prev_state = self.state
        if clordid == self.open_clordid:
            self._open_fill(amount, price, transact_time)
        elif clordid == self.close_clordid:
            self._close_fill(amount, price, transact_time)
        elif clordid == self.correction_clordid:
            self._correction_fill(amount, price, transact_time)
        else:
            return False
        
        self.trade.log.debug('Marketorder state changed to %s -> %s ' % (MARKETORDER_STATES[prev_state], MARKETORDER_STATES[self.state]))
        return True
    
    
    def handle_rejected(self, clordid, message):
        prev_state = self.state     
        if clordid == self.open_clordid:
            self._open_rejected(message)
        elif clordid == self.close_clordid:
            self._close_rejected(message)
        elif clordid == self.correction_clordid:
            self._correction_rejected(message)
        else:
            return False
        
        self.trade.log.debug('Marketorder state changed to %s -> %s ' % (MARKETORDER_STATES[prev_state], MARKETORDER_STATES[self.state]))        
        return True
    
#    def handle_message(self, message):
#        if message.ClOrdID == self.open_clordid:
#            if message.OrdStatus == fix.OrdStatus.FILLED:
#                self._open_fill(message.CumQty, message.AvgPx, message.TransactTime or message.SendingTime)
#                
#            if message.OrdStatus == fix.OrdStatus.REJECTED:
#                self._open_rejected(message)
#
#                
#        elif message.ClOrdID == self.close_clordid:
#            if message.OrdStatus == fix.OrdStatus.FILLED:
#                self._close_fill(message.CumQty, message.AvgPx, message.TransactTime or message.SendingTime)
#                
#            if message.OrdStatus == fix.OrdStatus.REJECTED:
#                self._close_rejected(message)
#                
#        elif message.ClOrdID == self.correction_clordid:
#            if message.OrdStatus == fix.OrdStatus.FILLED:
#                self._correction_fill(message.CumQty, message.AvgPx, message.TransactTime or message.SendingTime)
#                
#            if message.OrdStatus == fix.OrdStatus.REJECTED:
#                self._correction_rejected(message)
#                
#        else:
#            return False
#
#        return True
                        
    def _open_fill(self, amount, price, time):
        self.current_amount += amount
        if not self.open_price:
            self.open_price = price
            self.open_fill_time = time    
        
        self.state = MARKETORDER_PARTIALLY_FILLED if self.current_amount < self.amount else MARKETORDER_FILLED
        
        if self.state == MARKETORDER_FILLED:
            self.trade.amount = self.current_amount 
        
             
    def _close_fill(self, amount, price, time):
        self.current_amount -= amount
        if not self.close_price:
            self.close_price = price
            self.close_fill_time = time

        self.state = MARKETORDER_PARTIALLY_CLOSED if self.current_amount > 0 else MARKETORDER_CLOSED 
    
    
    def _correction_fill(self, amount, price, time):
        self.correction_amount += amount
        if not self.correction_price:
            self.correction_price = price
            self.correction_fill_time = time
            
        if self.amount == self.correction_amount:
            self.state = MARKETORDER_CORRECTION_FILLED
        
            
    def _open_rejected(self, message):
        self.state = MARKETORDER_OPEN_REJECTED                                
        self.trade.log.error('OPEN REJECTED for %s  message: %s' % (self.trade, message))
        #TODO: pontosabb REJECT kezelés kell
        if self.open_reject_count:
            self.open_reject_count -= 1
            self.open()
        else:
            self.trade.log.error('Open resend limit reached, giving up...')
        
        
    def _close_rejected(self, message):
        self.state = MARKETORDER_CLOSE_REJECTED                
        self.trade.log.error('CLOSE REJECTED for %s  message: %s' % (self.trade, message))
        #TODO: pontosabb REJECT kezelés kell
        if self.close_reject_count:
            self.close_reject_count -= 1
            self.close()                
    
    
    def _correction_rejected(self, message):
        self.state = MARKETORDER_CORRECTION_REJECTED        
        self.trade.log.error('CORRECTION REJECTED for %s  message: %s' % (self.trade, message))
        #TODO: pontosabb REJECT kezelés kell
        if self.corrigate_reject_count:
            self.corrigate_reject_count -= 1
            self.corrigate_double_fill()        
    
    
#    def mark_aborted(self):
#        self.current_amount = 0        
#        self.state = MARKETORDER_ABORTED       
        
    
    def remove_trade_entries(self):
        self.trade.fix_account.unregister_order(self.open_clordid)
        self.trade.fix_account.unregister_order(self.close_clordid)
        self.trade.fix_account.unregister_order(self.correction_clordid)        
            
    


class ConditionalOrder(object):
    
    def __init__(self, trade):  
        self.id = 0
        self.trade = trade                  
        self.state = CONDORDER_CREATED                                       
        self.entry_clordid = None
        self.entry_orderid = None
        self.entry_sent_time = None
        self.entry_price = 0
        self.entry_retries_count = ORDER_RESEND_COUNT
        
        self.filled_amount = 0
        self.fill_price = 0
        self.fill_time = None
        
        self.cancel_clordid = None
        self.cancel_sent_time = None
        self.canceled_time = None
        self.cancel_retries_count = ORDER_RESEND_COUNT
        
        self._clordid = 0                
    
        
    def get_data(self):
        data = self._get_data()
        data['id'] = self.id
        return data
    
    
    def reset_counters(self):
        self.entry_retries_count = ORDER_RESEND_COUNT
        self.cancel_retries_count = ORDER_RESEND_COUNT
        
    
    def load(self, conditionalorder_id):                
        result = db.engine.execute(self._table.select(whereclause=self._table.c.id==conditionalorder_id))
        r = result.fetchone()
        for k,v in r.items():            
            setattr(self, k, v)            
    
    
    def _get_data(self):
        data = copy.copy(self.__dict__)
        for k in ["id","trade","_name","_table","_price"]:
            del data[k]                
        return data


    def save(self):
        try:                
            if self.id:     
                command = self._table.update().where(self._table.c.id==self.id).values(**self._get_data())
                sql.server.update(command)                                   
            else:            
                command = self._table.insert().returning(self._table.c.id).values(self._get_data())
                self.id = sql.server.insert(command)
        except:
            self.trade.log.error(traceback.format_exc())
            
                                                                    
    def register_orders(self):
        if self.entry_clordid:
            self.trade.fix_account.register_order(self.trade, self.entry_clordid)
        if self.cancel_clordid:
            self.trade.fix_account.register_order(self.trade, self.cancel_clordid)
            
                
    def _symbol(self):
        return self.trade.symbol[:3] + '/' + self.trade.symbol[3:]


    def next_clordid(self):
        self._clordid += 1
        return 'TRADE-%d, %s-%d %d' % (self.trade.id, self._name, self.id, calendar.timegm(datetime.utcnow().utctimetuple()))

                        
    def __str__(self):
        s = '%s(state: %s)\n' % (self._name, CONDORDER_STATES[self.state])
        s += 'entry: %s, %s, %s, %.5f\n' % (self.entry_clordid, self.entry_orderid, self.entry_sent_time, self.entry_price)
        s += 'fill: %d, %.5f, %s\n' % (self.filled_amount, self.fill_price, self.fill_time)
        s += 'cancel: %s, %s, %s\n' % (self.cancel_clordid, self.cancel_sent_time, self.canceled_time)
        return s
    
    
    def handle_pending_new(self, clordid, orderid, price):                
        if self.state == CONDORDER_NOT_USED:
            return False
        
        prev_state = self.state
        if clordid == self.entry_clordid:
            self._pending_new(orderid, price)
            self.trade.log.info('%s: pending new' % clordid)
            
        else:
            return False

        self.trade.log.debug('%s state changed to %s -> %s ' % (self._name, CONDORDER_STATES[prev_state], CONDORDER_STATES[self.state]))        
        return True     
        
        
    def handle_filled(self, clordid, amount, price, transacttime):
        if self.state == CONDORDER_NOT_USED:
            return False
        
        prev_state = self.state
        if clordid == self.entry_clordid:
            self._fill(amount, price, transacttime)
            self.trade.log.info('%s: filled' % clordid)
            
        else:
            return False
        
        self.trade.log.debug('%s state changed to %s -> %s ' % (self._name, CONDORDER_STATES[prev_state], CONDORDER_STATES[self.state]))
        return True
        
    
    def handle_rejected(self, clordid, message):
        if self.state == CONDORDER_NOT_USED:
            return False
        
        prev_state = self.state
        if clordid == self.entry_clordid and not self.entry_orderid:
            self._entry_rejected(message)
            self.trade.log.warning('%s: entry rejected' % clordid)
            
        if ( clordid == self.entry_clordid and self.entry_orderid == message.OrderID ) \
            or clordid == self.cancel_clordid:
            self._cancel_rejected(message)
            self.trade.log.warning('%s: cancel rejected' % clordid)
            
        else:
            return False
        
        self.trade.log.debug('%s state changed to %s -> %s ' % (self._name, CONDORDER_STATES[prev_state], CONDORDER_STATES[self.state]))
        return True
    
    
    def handle_canceled(self, clordid, cancel_time):
        if self.state == CONDORDER_NOT_USED:
            return False       
         
        prev_state = self.state        
        # a Dukas a cancel message clordid-jét küldi vissza, az adss meg az eredeti üzenet clordid-jét.... 
        if clordid == self.cancel_clordid or clordid == self.entry_clordid:
            self._canceled(cancel_time)
            self.trade.log.info('%s: canceled' % clordid)
        else:            
            return False

        self.trade.log.debug('%s state changed to %s -> %s ' % (self._name, CONDORDER_STATES[prev_state], CONDORDER_STATES[self.state]))        
        return True
    
                
#    def handle_message(self, message):
#        if self.state == CONDORDER_NOT_USED:
#            return False
#        
#        if message.ClOrdID == self.entry_clordid:             
#        
#            if message.OrdStatus == fix.OrdStatus.PENDING_NEW:
#                self._pending_new(message.OrderID, message.AvgPx or message.Price)
#                self.trade.log.info('%s: pending new' % message.ClOrdID)                  
#        
#            elif message.OrdStatus == fix.OrdStatus.FILLED:
#                self._fill(message.CumQty, message.AvgPx, message.TransactTime or message.SendingTime)
#                self.trade.log.info('%s: filled' % message.ClOrdID)                        
#                
#            elif message.OrdStatus == fix.OrdStatus.REJECTED:            
#                self._entry_rejected(message)
#                self.trade.log.warning('%s: entry reject' % message.ClOrdID)
#                                
#        elif message.ClOrdID == self.cancel_clordid:
#            if message.OrdStatus == fix.OrdStatus.REJECTED:
#                self._cancel_rejected(message)
#                self.trade.log.warning('%s: cancel rejected' % message.ClOrdID)
#        
#            elif message.OrdStatus == fix.OrdStatus.CANCELED:
#                self._canceled(message.TransactTime or message.SendingTime)    
#                self.trade.log.info('%s: canceled' % message.ClOrdID)
#    
#        else:
#            return False
#        
#        return True
#    
#    
    
    def _pending_new(self, orderid, price):        
        self.entry_orderid = orderid
        self.entry_price = price
        self.state = CONDORDER_PENDING_NEW
                    
                    
    def _fill(self, amount, price, time):
        self.filled_amount += amount
        if not self.fill_price:
            self.fill_price =  price
            self.fill_time = time
        
        if self.filled_amount >= self.trade.amount:
            self.state = CONDORDER_FILLED
        elif self.filled_amount < self.trade.amount:
            self.state = CONDORDER_PARTIALLY_FILLED                
        
                        
    def _canceled(self, cancel_time):
        self.canceled_time = cancel_time
        self.state = CONDORDER_CANCELED
        
                
    def _entry_rejected(self, message):
        self.state = CONDORDER_ENTRY_REJECTED
        self.trade.log.error('PLACING %s REJECTED FOR %s  message: %s' % (self._name, self.trade, message))
        if self.entry_retries_count:
            self.entry_retries_count -= 1
            self.place()
                
        
    def _cancel_rejected(self, message):
        if self.state == CONDORDER_CANCEL_SENT:
            self.state = CONDORDER_CANCEL_REJECTED
            self.trade.log.error('CANCELLING %s FOR %s REJECTED %s' % (self._name, self.trade, message))
            if self.cancel_retries_count:
                self.cancel_retries_count -= 1
                self.cancel()
                        
    def place(self, place_function):    
        if not self.entry_sent_time: 
            self.entry_sent_time = datetime.utcnow()
            
        self.entry_clordid = self.next_clordid()                 
        self.trade.log.info('placing %s' % self.entry_clordid)
        direction = DIR_BUY if self.trade.direction == DIR_SELL else DIR_SELL
        success = place_function(self.trade, self.entry_clordid, self._symbol(), direction, self.trade.amount, self._price, self.entry_sent_time)
                
        self.state = CONDORDER_ENTRY_SENT if success else CONDORDER_ENTRY_FAILED 


    
    def cancel(self, force_cancel=False):                 
        if self.state == CONDORDER_PENDING_NEW or force_cancel:            
            self.cancel_clordid = self.next_clordid()         
            self.trade.log.info('canceling %s' % self.entry_clordid)
            direction = DIR_BUY if self.trade.direction == DIR_SELL else DIR_SELL
            success = self.trade.fix_account.cancel_order(self.trade, self._symbol(), direction, self.entry_orderid, self.entry_clordid, self.cancel_clordid)            
        
            self.state = CONDORDER_CANCEL_SENT if success else CONDORDER_CANCEL_FAILED            
                        
    
    def remove_trade_entries(self):
        self.trade.fix_account.unregister_order(self.entry_clordid)
        self.trade.fix_account.unregister_order(self.cancel_clordid)        


class Takeprofit(ConditionalOrder):
    
    def __init__(self, trade):
        ConditionalOrder.__init__(self, trade)
        self._name = 'Takeprofit'        
        self._table = db.Takeprofit.__table__
        self._price = self.trade.tp_price        
        if not self.trade.tp_price :
            self.state = CONDORDER_NOT_USED                                                            
    
    def place(self):
        ConditionalOrder.place(self, self.trade.fix_account.place_takeprofit)
        
    
    
class Stoploss(ConditionalOrder):
    
    def __init__(self, trade):
        ConditionalOrder.__init__(self, trade)
        self._name = 'Stoploss'                
        self._table = db.Stoploss.__table__
        self._price = self.trade.sl_price        
        if not self.trade.sl_price :
            self.state = CONDORDER_NOT_USED 
            
    def place(self):
        ConditionalOrder.place(self, self.trade.fix_account.place_stoploss)


class Trade(object):
                
    def __init__(self, symbol=None, direction=None, open_time=None, price=None, amount=None, sl_price=None, tp_price=None, group_id=None, strategy_id=None, strategyrun_id=None, account_id=None, accountrun_id=None, portfolio_id=None, fix_account=None):    
        self.id = 0    
        self.account_id = account_id
        self.accountrun_id = accountrun_id
        self.portfolio_id = portfolio_id
        self.strategy_id = strategy_id
        self.strategyrun_id = strategyrun_id
        self.group_id = group_id                
        self.state = TRADE_CREATED        
        self.symbol = symbol
        self.direction = direction
        self.open_time = open_time
        self.req_price = price
        self.open_price = 0
        self.amount = amount
        self.sl_price = sl_price
        self.tp_price = tp_price        
        self.close_price = 0.0
        self.close_time = None
        self.close_type = 0
        self.floating_profit = 0.0
        self.profit = 0.0                    
        self.MAE = 0.0
        self.MFE = 0.0
        
        self.market_order = MarketOrder(self)                
        self.stoploss = Stoploss(self)        
        self.takeprofit = Takeprofit(self)
        
        self.market_order.register_orders()
        self.stoploss.register_orders()  
        self.takeprofit.register_orders()

        self.log = fix_account.log
        self.fix_account = fix_account
        self.execution_finished = threading.Event()
        self.lock = threading.Lock()      
    
    def get_data(self):
        data = copy.copy(self.__dict__)
        for k in data.keys():            
            if k in ["id", "stoploss","takeprofit","market_order","lock","log","fix_account","execution_finished"]:
                del data[k]            
            if k[0] == '_':
                del data[k]
        return data 
            
            
    def get_all_data(self):
        data = self.get_data()
        data['id'] = self.id
        data['market_order'] = self.market_order.get_data()
        data['stoploss'] = self.stoploss.get_data()
        data['takeprofit'] = self.takeprofit.get_data()
        return data
    
                        
    def load(self, trade_id):
        trade_table = db.Trade.__table__
        result = db.engine.execute(trade_table.select(whereclause=trade_table.c.id==trade_id))
        r = result.fetchone()
        for k,v in r.items():
            if k == 'marketorder_id':
                self.market_order.load(v)
                self.market_order.register_orders()
            elif k == 'stoploss_id':
                self.stoploss.load(v)
                self.stoploss.register_orders()
            elif k == 'takeprofit_id':
                self.takeprofit.load(v)
                self.takeprofit.register_orders()
            setattr(self, k, v)
                
        
    def save(self):
        trade_table = db.Trade.__table__            
        
        if not self.id:                                                
            self.market_order.save()
            self.stoploss.save()
            self.takeprofit.save()
            
            data = self.get_data()
                    
            data["marketorder_id"] = self.market_order.id
            data["stoploss_id"] = self.stoploss.id
            data["takeprofit_id"] = self.takeprofit.id
            
            command = trade_table.insert().values(**data).returning(trade_table.c.id)
            self.id = sql.server.insert(command)
                            
            self.market_order.trade_id = self.id
            self.stoploss.trade_id = self.id
            self.takeprofit.trade_id = self.id            
                                                
        else:
            try:
                self.log.debug('saving trade: %s' % self)
                command = trade_table.update().\
                                  where(trade_table.c.id==self.id).\
                                  values(state=self.state, amount=self.amount, open_price=self.open_price, floating_profit=self.floating_profit, profit=self.profit, MAE=self.MAE, MFE=self.MFE, 
                                         close_price=self.close_price, close_time=self.close_time, close_type=self.close_type)
                 
                sql.server.update(command)                                                                                  
            except:
                self.log.error(traceback.format_exc())
            
            self.log.debug('saving market order')     
            self.market_order.save()
            self.log.debug('saving takeprofit')
            self.takeprofit.save()
            self.log.debug('saving stoploss')
            self.stoploss.save()
            self.log.debug('save finished')
        
        
    def save_performance(self):
        try:
            trade_table = db.Trade.__table__ 
            #self.log.debug('saving trade performance: %s' % self)
            command = trade_table.update().\
                              where(trade_table.c.id==self.id).\
                              values(floating_profit=self.floating_profit, profit=self.profit, MAE=self.MAE, MFE=self.MFE)
             
            sql.server.update(command)                                                                                  
        except:
            self.log.error(traceback.format_exc())
        
        
    def __str__(self):
        sl = ''
        tp = ''
        if self.sl_price:
            sl = ' SL @ %.5f' % self.sl_price
        if self.tp_price:
            tp = ' TP @ %.5f' % self.tp_price        
        
        profit = self.profit or self.floating_profit
        
        return "Trade %s: %s %s %d @ %.5f close @ %.5f,%s%s profit: %.2f" % (self.id, self.symbol, DIR_STR[self.direction], self.amount, self.market_order.open_price, self.market_order.close_price, sl, tp, profit)

    def __repr__(self):
        s = """Trade %d (strategy_id: %d, group_id: %d) 
state:\t%s
symbol:\t%s
-------
%r
%r
%r
-------
floating profit: %.2f
profit: %.2f
MAE: %.2f
MFE: %.2f""" % (self.id, self.strategy_id, self.group_id, TRADE_STR[self.state], self.symbol, self.market_order, self.stoploss, self.takeprofit, self.floating_profit, self.profit, self.MAE, self.MFE)
        return s
        
                        
    def send_open(self):
        self.log.debug('---- Open started ----')
        with self.lock:            
            self.state = TRADE_OPENING
            self.execution_finished.clear()            
            self.market_order.open()            
            self.save()       
            
            if self.market_order.state == MARKETORDER_OPEN_FAILED:
                self.execution_finished.set()
                    
                    
#        self.execution_finished.wait()                                        
#        self.log.debug('---- Open finished ----')        
        
        
    def is_opened(self):
        return self.state == TRADE_OPENED                            
                
                
    def send_close(self):
        self.log.debug('---- Close started ----')
                
        with self.lock:
            if self.state == TRADE_OPENED:
                self.state = TRADE_CLOSING
                self.execution_finished.clear()
                
                if self.stoploss.state == CONDORDER_NOT_USED:                    
                    self.market_order.close()                                                                                    
                else:                    
                    self.stoploss.cancel()        
                    self.takeprofit.cancel()
                self.save()
                
                if self.market_order.state == MARKETORDER_CLOSE_FAILED or \
                    self.stoploss.state == CONDORDER_CANCEL_FAILED or \
                    self.takeprofit.state == CONDORDER_CANCEL_FAILED:
                    self.execution_finished.set()

                
        #self.execution_finished.wait()                                                
        #self.log.debug('---- Close finished ----')                                                                                                         
    

    def handle_filled(self, clordid, amount, price, transacttime):
        with self.lock:            
            self.market_order.handle_filled(clordid, amount, price, transacttime)            
            self.stoploss.handle_filled(clordid, amount, price, transacttime)
            self.takeprofit.handle_filled(clordid, amount, price, transacttime)
            
            self.update_state()        
    
    
    def handle_rejected(self, clordid, message):
        with self.lock:
            prev_state = self.market_order.state
            self.market_order.handle_rejected(clordid, message)
            
            self.stoploss.handle_rejected(clordid, message)
            self.takeprofit.handle_rejected(clordid, message)
            
            self.update_state()


    def handle_pending_new(self, clordid, orderid, price):
        with self.lock:
            prev_state = self.market_order.state
            self.stoploss.handle_pending_new(clordid, orderid, price)
            self.takeprofit.handle_pending_new(clordid, orderid, price)
            
            self.update_state()
            
            
    def handle_canceled(self, clordid, cancel_time):
        with self.lock:
            
            self.stoploss.handle_canceled(clordid, cancel_time)
            
            self.takeprofit.handle_canceled(clordid, cancel_time)
            
            self.update_state()
    
    
    def _corrigate_sl_tp(self):
        diff = self.open_price - self.req_price                
        if self.sl_price:            
            self.log.info('correcting SL price with %.1f pip from %.5f to %.5f' % (diff / PIP_MULTIPLIER[self.symbol], self.sl_price, self.sl_price+diff))
            self.sl_price += diff
            self.stoploss._price = self.sl_price
        if self.tp_price:            
            self.log.info('correcting TP price with %.1f pip from %.5f to %.5f' % (diff / PIP_MULTIPLIER[self.symbol], self.tp_price, self.tp_price+diff))
            self.tp_price += diff
            self.takeprofit._price = self.tp_price    
        
        if self.sl_price or self.tp_price:        
            trade_table = db.Trade.__table__
            command = trade_table.update().\
                            where(trade_table.c.id==self.id).\
                            values(sl_price = self.sl_price, tp_price = self.tp_price)
                                             
            sql.server.update(command)    
        

                        
    def update_state(self):
        prev_state = self.state
        prev_state_mo = self.market_order.state
        prev_state_sl = self.stoploss.state
        prev_state_tp = self.takeprofit.state
        
        if self.state == TRADE_OPENING:
            if self.market_order.state == MARKETORDER_FILLED:
                if not self.open_price:
                    self.open_price = self.market_order.open_price
                    self._corrigate_sl_tp()                     
                                                                                   
                if self.stoploss.state == CONDORDER_CREATED:                                                
                    self.stoploss.place()
                    
                if self.takeprofit.state == CONDORDER_CREATED:                                                        
                    self.takeprofit.place()
            
                if (self.stoploss.state == CONDORDER_NOT_USED and self.takeprofit.state == CONDORDER_NOT_USED) or \
                    (self.stoploss.state == CONDORDER_PENDING_NEW and self.takeprofit.state == CONDORDER_NOT_USED) or \
                    (self.stoploss.state == CONDORDER_PENDING_NEW and self.takeprofit.state == CONDORDER_PENDING_NEW): 
                    self.state = TRADE_OPENED                                       
                        
                if self.stoploss.state == CONDORDER_PARTIALLY_FILLED or self.stoploss.state == CONDORDER_FILLED or \
                    self.takeprofit.state == CONDORDER_PARTIALLY_FILLED or self.takeprofit.state == CONDORDER_FILLED:
                    self.state = TRADE_CLOSING
                
                if self.stoploss.state == CONDORDER_ENTRY_REJECTED and not self.stoploss.entry_retries_count:
                    self.log.warning('TRADE OPEN FAILED: %s' % (str(self)))  
                    self.execution_finished.set()
                    
                if self.takeprofit.state == CONDORDER_ENTRY_REJECTED and not self.takeprofit.entry_retries_count:
                    self.log.warning('TRADE OPEN FAILED: %s' % (str(self)))  
                    self.execution_finished.set()
                    
                    
            if self.market_order.state == MARKETORDER_OPEN_REJECTED and not self.market_order.open_reject_count:
                self.state = TRADE_CLOSED        
                    
            
        if self.state == TRADE_OPENED:        
            
            if self.stoploss.state == CONDORDER_FILLED:                
                self.state = TRADE_CLOSING
                if self.takeprofit.state == CONDORDER_NOT_USED:
                    self.state = TRADE_CLOSED                    
                if self.takeprofit.state == CONDORDER_PENDING_NEW:
                    self.takeprofit.cancel()
                if self.takeprofit.state == CONDORDER_CANCELED:
                    self.state = TRADE_CLOSED                    
                                        
            if self.takeprofit.state == CONDORDER_FILLED:                
                self.state  = TRADE_CLOSING
                if self.stoploss.state == CONDORDER_PENDING_NEW:
                    self.stoploss.cancel()
                    
                    
        if self.state == TRADE_CLOSING:
                                                
            # ha az SL vagy TP fill/cancel után kerülünk ide, és a market order még ép:
            if self.market_order.state == MARKETORDER_FILLED:
                if self.stoploss.state == CONDORDER_FILLED and self.takeprofit == CONDORDER_FILLED:                    
                    self.market_order.corrigate_double_fill()                    
                
                if self.stoploss.state == CONDORDER_FILLED and self.takeprofit.state == CONDORDER_PENDING_NEW:                    
                    self.takeprofit.cancel()
                    
                if self.takeprofit.state == CONDORDER_FILLED and self.stoploss.state == CONDORDER_PENDING_NEW:                    
                    self.stoploss.cancel()
                    
                # ez a két eset akkor van, ha  SL v. TP fillből jöttünk át ide
                
                if self.stoploss.state == CONDORDER_FILLED and self.takeprofit.state in (CONDORDER_CANCELED, CONDORDER_NOT_USED):
                    self.state = TRADE_CLOSED
                    
                if self.stoploss.state == CONDORDER_CANCELED and self.takeprofit.state == CONDORDER_FILLED:
                    self.state = TRADE_CLOSED
                
                # ezek pedig ha a .close()-t hivták meg
                if self.stoploss.state == CONDORDER_CANCELED and self.takeprofit.state == CONDORDER_NOT_USED:
                    self.market_order.close()
                        
                if self.stoploss.state == CONDORDER_CANCELED and self.takeprofit.state == CONDORDER_CANCELED:
                    self.market_order.close()
                
                                    
            # ha a market order zárásából kerülünk ide:
            if self.market_order.state == MARKETORDER_CLOSED:                    
                self.state = TRADE_CLOSED                
                        
            if self.market_order.state == MARKETORDER_CLOSE_REJECTED and not self.market_order.close_reject_count:
                self.log.warning('TRADE CLOSE FAILED: %s' % (str(self)))      
                self.execution_finished.set()
                
            if self.stoploss.state == CONDORDER_CANCEL_REJECTED and not self.stoploss.cancel_retries_count:
                self.log.warning('TRADE CLOSE FAILED: %s ' % (str(self)))
                self.execution_finished.set()
            
            if self.takeprofit.state == CONDORDER_CANCEL_REJECTED and not self.takeprofit.cancel_retries_count:
                self.log.warning('TRADE CLOSE FAILED: %s ' % (str(self)))
                self.execution_finished.set()
                

                
            if self.market_order.state == MARKETORDER_CORRECTION_FILLED:                
                self.state = TRADE_CLOSED
                
        
#        if prev_state != self.state:
        self.log.debug('Trade state changed to %s -> %s ' % (TRADE_STR[prev_state], TRADE_STR[self.state]))        
        self.log.debug('MarketOrder state changed to %s -> %s ' % (MARKETORDER_STATES[prev_state_mo], MARKETORDER_STATES[self.market_order.state]))
        self.log.debug('Stoploss state changed to %s -> %s ' % (CONDORDER_STATES[prev_state_sl], CONDORDER_STATES[self.stoploss.state]))
        self.log.debug('Takeprofit state changed to %s -> %s ' % (CONDORDER_STATES[prev_state_tp], CONDORDER_STATES[self.takeprofit.state]))
            
        if self.state == TRADE_CLOSED or self.state == TRADE_OPENED:                                                 
            
            if self.state == TRADE_OPENED and prev_state != self.state:
                self.log.info('**** TRADE OPENED: %s **** ' % (str(self)))
                
            if self.state == TRADE_CLOSED:                
                
                if self.stoploss.state == CONDORDER_FILLED:
                    self.close_price = self.stoploss.fill_price
                    self.close_time = self.stoploss.fill_time
                    self.close_type = ORDERTYPE_CLOSE_BY_SL
                if self.takeprofit.state == CONDORDER_FILLED:
                    self.close_price = self.takeprofit.fill_price
                    self.close_time = self.takeprofit.fill_time
                    self.close_type = ORDERTYPE_CLOSE_BY_TP
                if self.market_order.state == MARKETORDER_CLOSED:
                    self.close_price = self.market_order.close_price
                    self.close_time = self.market_order.close_fill_time
                    self.close_type = ORDERTYPE_CLOSE
                
                #print 'price_update', self.close_price, self.close_price, self.amount
                self.price_update(self.close_price, self.close_price)                
                self.profit = self.floating_profit
                self.floating_profit = 0
                #print 'profit:', self.profit
                if prev_state != self.state:
                    self.log.info('**** TRADE CLOSED: %s **** ' % (str(self)))                        
            
            self.execution_finished.set()
                                            
                                                    
        if self.state == TRADE_CLOSED:                            
            self.remove_trade_entries()
                
        self.save()
            
    
    
    def price_update(self, ask, bid):
        try:
            if self.state >= TRADE_OPENED and self.state <= TRADE_CLOSED:
                            
                if self.direction == DIR_BUY:
                    pip = (bid - self.market_order.open_price) / PIP_MULTIPLIER[self.symbol] 
                else:
                    pip = (self.market_order.open_price - ask)  / PIP_MULTIPLIER[self.symbol]
                                                          
                self.floating_profit = pip * historic.CURRENT_PRICE[self.symbol]['PIPVALUE'] * self.amount
                
                self.MAE = min(self.MAE, self.floating_profit)
                self.MFE = max(self.MFE, self.floating_profit)
        except:
            self.log.error(traceback.format_exc())
            
        return self.floating_profit
    
    
    def get_exposure(self):
        exposure = 0
        if self.market_order.state == MARKETORDER_FILLED:
            exposure = self.market_order.amount
            if self.direction == DIR_SELL:
                exposure *= -1            
        
        if self.stoploss.state == CONDORDER_FILLED or self.takeprofit.state == CONDORDER_FILLED:
            exposure = 0
        
        return exposure
                
            
    def remove_trade_entries(self):
        self.market_order.remove_trade_entries()
        if self.stoploss:
            self.stoploss.remove_trade_entries()
        if self.takeprofit:
            self.takeprofit.remove_trade_entries()
        
    
    
    def resend_failed_orders(self):
        self.log.info('Trade #%d state = %s' % (self.id, TRADE_STR[self.state]))
        self.log.info('   MarketOrder #%d state = %s' % (self.market_order.id, MARKETORDER_STATES[self.market_order.state]))
        self.log.info('   Stoploss #%d state = %s' % (self.stoploss.id, CONDORDER_STATES[self.stoploss.state]))
        self.log.info('   Takeprofit #%d state = %s' % (self.takeprofit.id, CONDORDER_STATES[self.takeprofit.state]))
                
        if self.market_order.state == MARKETORDER_FILLED:
            if self.stoploss.state == CONDORDER_CREATED:                
                self.log.info('Placing stoploss')
                self.stoploss.place()
            
            if self.takeprofit.state == CONDORDER_CREATED:
                self.log.info('Placing takeprofit')
                self.takeprofit.place()
                    
            if self.stoploss.state == CONDORDER_FILLED and self.takeprofit.state == CONDORDER_PENDING_NEW:                
                self.state = TRADE_CLOSING
                self.log.info('Trade set to %s, cancel takeprofit' % TRADE_STR[self.state])
                self.takeprofit.cancel()
            
            if self.takeprofit.state == CONDORDER_FILLED and self.stoploss.state == CONDORDER_PENDING_NEW:
                self.state = TRADE_CLOSING
                self.log.info('Trade set to %s, cancel stoploss' % TRADE_STR[self.state])
                self.stoploss.cancel()
                
        if self.market_order.state == MARKETORDER_OPEN_SENT:
            self.log.info('Resending open order')
            self.market_order(resend=True)
        
        
        if self.market_order.state == MARKETORDER_OPEN_FAILED:
            if self.market_order.open_send_time + timedelta(minutes = 15) > datetime.utcnow():
                self.log.info('Sending open order')
                self.market_order.open()
            else:                
                self.log.info('Open is too old, marking trade as CLOSED')
                self.state = TRADE_CLOSED
                
                            
        if self.market_order.state == MARKETORDER_OPEN_REJECTED:
            if self.market_order.open_send_time + timedelta(minutes = 15) > datetime.utcnow() and not self.market_order.open_reject_count:
                self.log.info('Sending open order')
                self.market_order.open()
            else:
                self.log.info('Open is too old, marking trade as CLOSED')
                self.state = TRADE_CLOSED
        
        if self.market_order.state == MARKETORDER_CLOSE_SENT:
            self.log.info('Resending close order')
            self.market_order.close(True)
        
            
        if self.market_order.state == MARKETORDER_CLOSE_FAILED:
            self.log.info('Resending close order')
            self.market_order.close()
            
            
        if self.market_order.state == MARKETORDER_CLOSE_REJECTED:
            self.market_order.reset_counters()
            self.log.info('Resending close order')
            self.market_order.close()
            
        
        if self.stoploss.state in (CONDORDER_ENTRY_REJECTED, CONDORDER_ENTRY_FAILED):
            self.log.info('Placing stoploss')
            self.stoploss.reset_counters()
            self.stoploss.place()
            
        if self.stoploss.state in (CONDORDER_CANCEL_REJECTED, CONDORDER_CANCEL_FAILED):
            self.log.info('Cancel stoploss')
            self.stoploss.reset_counters()
            self.stoploss.cancel()
            
        if self.takeprofit.state in (CONDORDER_ENTRY_REJECTED, CONDORDER_ENTRY_FAILED):
            self.log.info('Place takeprofit')
            self.takeprofit.reset_counters()
            self.takeprofit.place()
            
        if self.takeprofit.state in (CONDORDER_CANCEL_REJECTED, CONDORDER_CANCEL_FAILED):
            self.log.info('Cancel takeprofit')
            self.takeprofit.reset_counters()
            self.takeprofit.cancel()
            
        self.save()
        

    def mark_closed(self, price=None):
        #with self.lock:
        try:
            self.state = TRADE_CLOSED            
            self.close_price = price or historic.CURRENT_PRICE[self.symbol]['ASK' if self.direction==DIR_SELL else 'BID']
            self.close_time = datetime.utcnow()
            self.close_type = ORDERTYPE_FORCE_CLOSE            
            self.price_update(self.close_price, self.close_price)
            self.profit = self.floating_profit
            self.floating_profit = 0
            self.remove_trade_entries()
            self.save()            
            self.log.info('**** TRADE MARKED AS CLOSED: %s **** ' % (str(self)))
        except:
            self.log.critical(traceback.format_exc())

        
    def marketorder_open(self):
        #with self.lock:
        self.market_order.open()            
        self.market_order.save() 
                
    
    def marketorder_close(self):
        #with self.lock:
        self.market_order.close()
        self.market_order.save()        
        
        
    def takeprofit_place(self):
        #with self.lock:
        self.takeprofit.place()
        self.takeprofit.save()
        
        
    def takeprofit_cancel(self):
        #with self.lock: 
        self.takeprofit.cancel(True)
        self.takeprofit.save()
    
    
    def takeprofit_fill(self):
        #with self.lock: 
        self.takeprofit.handle_filled(self.takeprofit.entry_clordid, self.amount, self.takeprofit.entry_price, datetime.utcnow())           
        self.takeprofit.save()
        self.update_state()
    
    
    def stoploss_place(self):
        #with self.lock: 
        self.stoploss.place()
        self.stoploss.save()            
        
        
    def stoploss_cancel(self):
        #with self.lock: 
        self.stoploss.cancel(True)
        self.stoploss.save()
        
    
    def stoploss_fill(self):
        #with self.lock:
        self.stoploss.handle_filled(self.stoploss.entry_clordid, self.amount, self.stoploss.entry_price, datetime.utcnow())
        self.stoploss.save()
        self.update_state()
        
            
    def change_sltp_if_has_order(self, entry_orderid, new_price):
        if self.takeprofit.entry_orderid == entry_orderid:
            self.takeprofit.entry_price = new_price
            self.takeprofit.save()
            self.log.info('Trade #%d: TP changed by account owner to %f' % (self.id, new_price))
        if self.stoploss.entry_orderid == entry_orderid:
            self.stoploss.entry_price = new_price
            self.stoploss.save()
            self.log.info('Trade #%d: SL changed by account owner to %f' % (self.id, new_price))
            
    
    def cancel_sltp_if_has_order(self, entry_orderid):
        if self.takeprofit.entry_orderid == entry_orderid:
            self.takeprofit.handle_canceled(self.takeprofit.entry_clordid, datetime.utcnow())
            self.takeprofit.save()
            self.log.info('Trade #%d: TP cancelled by account owner ' % (self.id))
        if self.stoploss.entry_orderid == entry_orderid:
            self.stoploss.handle_canceled(self.stoploss.entry_clordid, datetime.utcnow())
            self.stoploss.save()
            self.log.info('Trade #%d: SL cancelled by account owner ' % (self.id))
    
    
        
        