'''
Created on Jan 23, 2013

@author: gdt
'''


from multiprocessing import Process, JoinableQueue, Lock, Value
from gdtlive.admin.system.log import initialize_loggers
import gdtlive.store.db as db
import traceback
import time
import logging 
from gdtlive.constants import STRATEGYRUN_LIVE
from gdtlive.core.constants import TRADE_CLOSED
from gdtlive.reporting.calculator import calculate_serial, calculate_performance


log = logging.getLogger('gdtlive.core.performance')

server = None


def start():
    global server
    server = PerformanceCalculator()
    server.start()
    
    
    
class PerformanceCalculator(Process):
        
    def __init__(self):        
        super(PerformanceCalculator, self).__init__()        
        self.lock = Lock()
        self.sql_queue = JoinableQueue()


    def _calc_strategy(self, strategyrun_id):
        session = None
        try:
            if not strategyrun_id:
                return 
            db.init()
            session = db.Session()
            
            q = session.query(db.StrategyRun.performanceIdCurrency, db.StrategyRun.performanceSerialId, db.LiveRunConfig.running_from, db.LiveRunConfig.running_to)
            q = q.filter(db.LiveRunConfig.id == db.StrategyRun.configId)
            q = q.filter(db.StrategyRun.id == strategyrun_id)             
            perf_id, serial_id, from_date, to_date,  = q.first()
            
            log.info('strategyrun %d : %d, %d, %s - %s' % (strategyrun_id, perf_id, serial_id, from_date, to_date))
            
            if perf_id and serial_id and from_date and to_date:                             
                #trades = session.query(db.Trade).filter(db.Trade.strategyrun_id == strategyrun_id).filter(db.Trade.state==TRADE_CLOSED).order_by(db.Trade.id).all()                
                trades = session.query(db.Trade).filter(db.Trade.strategyrun_id == strategyrun_id).order_by(db.Trade.id).all()
                
                if trades:
                    try:
                        perf_data = calculate_performance(trades, 0, from_date, to_date)
                        table = db.PerformanceData.__table__
                        db.engine.execute(table.update().where(table.c.id == perf_id).values(**perf_data))
                    except:            
                        log.error(traceback.format_exc())                
                    
                    try:                                                                        
                        serial_data = calculate_serial(trades, 0, from_date, to_date)
                        for k in serial_data.keys():
                            serial_data[k] = str(serial_data[k])
                        table = db.PerformanceStrategySerialData.__table__
                        db.engine.execute(table.update().where(table.c.id == serial_id).values(**serial_data))
                    except:
                        log.error(traceback.format_exc())
                                
        finally:
            if session:
                session.close()

    def _calc_account(self, accountrun_id):
        session = None
        try:
            db.init()
            session = db.Session()

            q = session.query(db.PortfolioAccountRun.performance_id,
                                 db.PortfolioAccountRun.performanceserial_id,
                                 db.PortfolioAccountRun.running_from,
                                 db.PortfolioAccountRun.running_to,
                                 db.PerformanceData.starting_balance).filter(db.PortfolioAccountRun.id == accountrun_id).filter(db.PortfolioAccountRun.performance_id == db.PerformanceData.id)
            
            perf_id, serial_id, from_date, to_date, starting_balance = q.first()                                                    
            
            log.info('accountrun %d : %d, %d, %f, %s - %s' % (accountrun_id, perf_id, serial_id, starting_balance, from_date, to_date))
            
            if perf_id and serial_id and from_date and to_date:                             
                #trades = session.query(db.Trade).filter(db.Trade.accountrun_id == accountrun_id).filter(db.Trade.state==TRADE_CLOSED).order_by(db.Trade.id).all()                
                trades = session.query(db.Trade).filter(db.Trade.accountrun_id == accountrun_id).order_by(db.Trade.id).all()
                
                if trades:
                    try:
                        perf_data = calculate_performance(trades, starting_balance, from_date, to_date)
                        del perf_data['min_floating']
                        table = db.PerformanceData.__table__
                        db.engine.execute(table.update().where(table.c.id == perf_id).values(**perf_data))
                    except:            
                        log.error(traceback.format_exc())                
                    
                    try:                                                                        
                        serial_data = calculate_serial(trades, starting_balance, from_date, to_date)
                        for k in serial_data.keys():
                            serial_data[k] = str(serial_data[k])
                        table = db.PerformanceStrategySerialData.__table__
                        db.engine.execute(table.update().where(table.c.id == serial_id).values(**serial_data))
                    except:
                        log.error(traceback.format_exc())
                                
        finally:
            if session:
                session.close()

    
    def run(self):
        while True:        
            run_type, run_id = self.sql_queue.get()
             
            if not run_type and not run_id:
                log.info('shutting down')
                break
            
            retries = 3
            while retries:
                if run_type == 1:
                    executor = Process(target=self._calc_strategy, args=(run_id,), name='Performance Calculator for strategyrun %d' % run_id)
                elif run_type == 2:
                    executor = Process(target=self._calc_account, args=(run_id,), name='Performance Calculator for accountrun %d' % run_id)
                executor.start()
                try:
                    executor.join(4)
                except:
                    pass
                                                
                try:                    
                    if executor.is_alive():
                        log.warning('terminating sql process')
                        executor.terminate()
                    else:
                        #log.info('command ok')
                        break 
                except:
                    pass
                
                log.warning('problems occured, retrying')
                retries -= 1
                                            
            self.sql_queue.task_done()
        
        
    def calculate_strategy(self, strategyrun_id):
        with self.lock:
            self.sql_queue.put((1, int(strategyrun_id)))
    
    def calculate_account(self, accountrun_id):
        with self.lock:
            self.sql_queue.put((2, int(accountrun_id)))
            
    def shutdown(self):
        with self.lock:
            self.sql_queue.put((None, None))
        
        
        
def recalculate_all():
    session = db.Session()
    strategy_runs = session.query(db.StrategyRun.id).filter(db.StrategyRun.type == STRATEGYRUN_LIVE).all()
    
    for run, in strategy_runs:
        server.calculate_strategy(run)
    
    account_runs = session.query(db.PortfolioAccountRun.id).all()
    for run, in account_runs:
        server.calculate_account(run)
        
    server.shutdown()
    
    
if __name__=='__main__':
    initialize_loggers()
    db.init()
    start()
    recalculate_all()    
        