# -*- encoding: utf-8 -*- 
'''
Created on May 29, 2012

@author: gdtlive
'''
import os
import ast
import glob
import copy
import logging
import traceback
import cPickle
from gdtlive.core.fix.messages import Message
import gdtlive.core.fix.dukascopy.fix as fix 

STORETYPE_IN = 0
STORETYPE_OUT = 1

STORE_TYPE = {
              STORETYPE_IN : 'in',
              STORETYPE_OUT : 'out'
            }


class MessageStore(object):
    
    
    def __init__(self, store_type, store_name):
        self.log = logging.getLogger('gdtlive.core.store.%s' % store_name)
        self.store = {}
        self.unprocessed = {}
        self.store_name = store_name
        self.store_type = store_type
        self.max_cache_size = 100
        self.expected_seqnum = 1
        self.last_processed_seqnum = 0
        #self.reset(0)
    
    
    def __str__(self):
        return '%s(%s, len:%d, processed: %d, expected_seqnum: %d)' % (self.__class__.__name__, STORE_TYPE[self.store_type], len(self), self.last_processed_seqnum, self.expected_seqnum)
    
    def show(self):
        print self
        for seqnum, message in self.store.iteritems():
            print seqnum, message._processed, message
    
    def reset(self, seqnum=0):
        self.store = {}
        self.last_processed_seqnum = seqnum
        self.expected_seqnum = seqnum+1        
    
    
    def get_last(self, number):
        seqnums = sorted(self.store.keys())[:number]
        result = {}
        for seqnum in seqnums:
            result[seqnum] = self.store[seqnum]
        
        return result
        
    
    def next_unprocessed(self):
        if self.last_processed_seqnum + 1 in self.store:
            i = self.last_processed_seqnum + 1
            while i+1 in self.store and self.store[i]._processed: i += 1            
            return self.store[i]        
        
        
    def processed(self, message, result=True):
        try:
            self.last_processed_seqnum = message.MsgSeqNum
            message.processed(result)                    
        except:
            self.log.error(traceback.format_exc())            
        
        
    def __len__(self):
        return len(self.store)        
    
    
    def __getitem__(self, seqnum):        
        try:            
            if seqnum in self.store: #self.store[seqnum]:
                return self.store[seqnum]
            else:
                return self._load(seqnum)            
        except:        
            raise KeyError
    
                
    def __setitem__(self, seqnum, message):
        if seqnum not in self.store:
            self.store[seqnum] = message            
            self.expected_seqnum = max(self.expected_seqnum, seqnum + 1)
            self._save(seqnum, message)
            
            if len(self.store) > self.max_cache_size:
                first_key = sorted(self.store.keys())[0]
                #self.store[first_key] = None
                del self.store[first_key]
            
            self.log.debug('%s(%s): message added: %d -> %d, %s' % (self.store_name, STORE_TYPE[self.store_type], seqnum, self.expected_seqnum, message))                
        else:
            self.log.debug('%s(%s): %d already in (%s)' % (self.store_name, STORE_TYPE[self.store_type], seqnum, message))
            
                                        
    def __contains__(self, seqnum):
        return seqnum in self.store

        
    def __iadd__(self, message):
        if message:
            self.__setitem__(message.MsgSeqNum, message)
            
        return self


    def _save(self, seqnum, message):
        pass
    
    
    def _load(self, seqnum):
        pass
    
        

class FileStore(MessageStore):
    
    base_path = '/var/spool/gdt/messagestore/'
    
    def __init__(self, store_type, store_name):
        super(FileStore, self).__init__(store_type, store_name)
        self.path = self.base_path + store_name
                
                
    def reset(self, seqnum=0):
        super(FileStore, self).reset(seqnum)
                
        self.path = self.base_path + self.store_name + os.sep + STORE_TYPE[self.store_type] + os.sep
                
        if not os.path.exists(self.path):
            try:
                os.makedirs(self.path)
            except:
                self.log.error(traceback.format_exc())
                return False
            
        messages = glob.glob(self.path + '*')
        for filename in messages:
            try:
                os.remove(filename)
            except:
                try:
                    os.rmdir(filename)
                except:
                    self.log.error(traceback.format_exc())
                        
        self.log.info(' ------- RESET -------- ')
        return True
                                                
    
    def _save(self, seqnum, message):
        if self.store_type == STORETYPE_IN:
            return         
        try:                            
            with open(self.path + str(seqnum),'wb+') as f:
                f.write(fix.encode(message))                        
        except:
            self.log.error(traceback.format_exc())


    def _load(self, seqnum):
        message = None
        try:
            with open(self.path + str(seqnum), 'rb') as f:
                data = f.read()                            
            message = fix.decode(data)            
        except:
            self.log.error(traceback.format_exc())
        return message
