'''
Created on Nov 19, 2012

@author: gdt
'''
from multiprocessing import Process, JoinableQueue, Lock, Value
import gdtlive.store.db as db
import traceback
import time
import logging 

log = logging.getLogger('gdtlive.sql')

server = None

def start():
    global server
    server = SQLUpdater()
    server.start()
    
    
    
class SQLUpdater(Process):
        
    def __init__(self):        
        super(SQLUpdater, self).__init__()        
        self.lock = Lock()
        self.sql_queue = JoinableQueue()
        self.insert_id = Value('i',0)
        
        
#    def connect(self):
#        retries = 10
#        connection = None
#        while retries:
#            try:
#                print 'opening connection'
#                connection = db.engine.connect()
#                break
#            except:
#                print traceback.format_exc()
#                pass
#            retries -= 1
#            
#        return connection
            
    
    def _execute(self, command, insert_id):
        #print 'executing:', command        
        while True:
            try:
                result = db.engine.execute(command)
                try:
                    insert_id.value = result.first()[0]
                except:
                    insert_id.value = 0
                break
            except:
                print traceback.format_exc()
                pass
            time.sleep(0.1)
                
#        while True:
#            try:
#                self.result = connection.execute(command)
#                break
#            except:
#                print 'reconnect', traceback.format_exc()
#                connection = self.connect()
#                pass         
    
    def _execute2(self, command, insert_id):
        while True:
            try:
                result = self.connection.execute(command)
                try:
                    insert_id.value = result.first()[0]
                except:
                    insert_id.value = 0
                break
            except:
                print traceback.format_exc()
                time.sleep(0.1)
                self.connection = db.engine.connect()
        
        
    def run(self):
        self.connection = db.engine.connect()
        while True:        
            command = self.sql_queue.get() 
            self._execute2(command, self.insert_id)
            
            #retries = 10
            #while retries:                
#                executor = Process(target=self._execute, args=(command, self.insert_id), name='SQL Executor')
#                executor.start()
#                try:
#                    executor.join(5)
#                except:
#                    pass
#                                                                
#                try:                    
#                    if executor.is_alive():
#                        log.warning('terminating sql process')
#                        executor.terminate()
#                    else:
#                        #log.info('command ok')
#                        break 
#                except:
#                    pass
                
                #log.warning('problems occured, retrying')
            #    retries -= 1
                                            
            self.sql_queue.task_done()
        
        
    def update(self, command):        
        with self.lock:       
            #log.info('UPDATE command received') 
            self.sql_queue.put(command)
            #self.sql_queue.join()
        
        
        
    def insert(self, command):        
        insertid = 0
        with self.lock:                       
            #log.info('INSERT command received, %s' % type(command))      
            self.sql_queue.put(command)
            self.sql_queue.join()
            insertid = self.insert_id.value                         
            #log.info('Insert id: %d' % self.insert_id.value)            
        
        return insertid
