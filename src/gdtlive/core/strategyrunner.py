# -*- encoding: utf-8 -*- 
'''
Created on Jul 3, 2012

@author: gdtlive
'''
import gdtlive.store.db as db
import gdtlive.config as config
from sqlalchemy import Column, Integer, String, Float, Time, Boolean, DateTime, Date, Text, create_engine, ForeignKey, Table, Sequence, LargeBinary, MetaData
from sqlalchemy.orm import relationship
from sqlalchemy.sql.expression import desc
from gdtlive.utils import open_db_connection
from gdtlive.core.constants import *
from gdtlive.core.accounting.accounts import live_accounts
from gdtlive.core.strategy import LiveStrategy
from gdtlive.constants import TIMEFRAME, LIVERUN_STATUS_FUCKEDUP, LIVERUN_STATUS_STOPPED, LIVERUN_STATUS_STR
from datetime import datetime, timedelta
import logging
import traceback
import ast
from logging.handlers import RotatingFileHandler
import os
import threading

#log = logging.getLogger('gdtlive.core.managers')


class StrategyRunner(object):
            
    def __init__(self, accountinfo):                
        self.account_id = accountinfo.id
        self.account_name = accountinfo.name
        self.account_number = accountinfo.account_number
        self.broker_id = accountinfo.brokerId
        self.username = accountinfo.username
        self.password = accountinfo.password        
        self.strategies = {}
        self.liverunconfigs = {}
        self.init_logging()
        self.session = None
        self.account = live_accounts[self.broker_id](self.account_id, self.account_name, accountinfo.account_number, self.username, self.password, accountinfo.server_url, accountinfo.server_port, accountinfo.use_ssl, accountinfo.trade_senderCompId, accountinfo.targetCompId, self.log)
        self.account.config_mm(accountinfo.mmpluginId, ast.literal_eval(accountinfo.mmplugin_parameters))
        self.lock = threading.Lock()                            
                    
    
    def init_logging(self, ):        
        name = 'gdtlive.core.account.%s' % self.account_name
        self.log = logging.getLogger(name)        
        self.log.setLevel(logging.DEBUG)
        self.add_log_handler(name, logging.INFO)
        self.add_log_handler(name, logging.WARN)
        self.add_log_handler(name, logging.DEBUG)
        
        
    def add_log_handler(self, name, level):
        handler = RotatingFileHandler(config.LOG_PATH + os.sep + name + '.' + logging.getLevelName(level) + '.log','w+',20*1024*1024, LOG_KEEP_FILES)
        handler.setFormatter(logging.Formatter(LOG_FORMAT))
        handler.setLevel(level)        
        self.log.addHandler(handler)        
        

    def load_strategies(self, session):
        from gdtlive.core.tradeengine import feedserver
        
        qry = session.query(db.LiveRunConfig.id).filter(db.LiveRunConfig.brokerAccountId==self.account_id).filter(db.LiveRunConfig.statusId < LIVERUN_STATUS_STOPPED).order_by(db.LiveRunConfig.strategyId)
        
        for liverunconfig_id, in qry.all():
            self.log.info('Loading liverunconfig %d' % liverunconfig_id)
            strategy = LiveStrategy(self.account, self.log)
            
            if not strategy.load_from(liverunconfig_id, session):
                self.log.error('Failed to load strategy from liverunconfig %d' % liverunconfig_id)
                return False
            
            for symbol in strategy.symbols: 
                feedserver.register_instrument(symbol, strategy.timeframe)                                                    
            
            feedserver.preload(strategy.timeframe)
                                        
            strategy.start()
            
            self.liverunconfigs[liverunconfig_id] = strategy                        
            self.strategies.setdefault(strategy.timeframe, []).append(strategy)                                    
            
            self.log.info('Strategy started: %d, strategyrun: %d' % (strategy.strategy_id, strategy.strategyrun_id))                                                                                                            
                                
        

    def connect(self):
        result = self.account.connect()        
        return result 
        
        
    def shutdown(self):        
        for liverunconfig_id in self.liverunconfigs.keys()[:]:
            self.abort_strategy(liverunconfig_id)
        
        self.account.shutdown()        
        self.strategies = {}
        self.liverunconfigs = {}
        
        #self.session.commit()            
        #self.session.close()
        
        
    def refresh_session(self):
        try:
            self.session.query(db.Strategy).get(1)
        except:
            self.session = db.Session()
                                                
        
    def start_strategy(self, liverunconfig_id, session):            
        from trademanager import feedserver
        try:                                                                         
            strategy = LiveStrategy(self.account, self.log)
            
            if not strategy.load_from(liverunconfig_id, session):
                self.log.error('Failed to load strategy from liverunconfig %d' % liverunconfig_id)
                return False
                                                                                                                               
            for symbol in strategy.symbols: 
                feedserver.register_instrument(symbol, strategy.timeframe)                                                                                                          
            
            feedserver.preload(strategy.timeframe)
                  
            strategy.start()
            
            self.liverunconfigs[liverunconfig_id] = strategy                        
            self.strategies.setdefault(strategy.timeframe, []).append(strategy)                                                
            
            self.log.info('Strategy started: %d, strategyrun: %d' % (strategy.strategy_id, strategy.strategyrun_id))                                                 
                                                                                                                                    
        except:
            self.log.error('Starting strategy failed !')
            self.log.error(traceback.format_exc())
            strategy.set_message(traceback.format_exc(), LIVERUN_STATUS_FUCKEDUP)
            return False        
        
        return True
    
    
    def evaluate(self, timeframe):
        try:                   
            if timeframe not in self.strategies:
                return 
            
            self.log.info('StrategyRunner.evaluate %s - start' % (TIMEFRAME[timeframe]))
            with self.lock:
                                                             
                self.account.before_evaluate(timeframe)            
                for strategy in self.strategies[timeframe]:
                    strategy.evaluate()
                
                for strategy in self.strategies[timeframe]:
                    strategy.dump_signals()
                
                for strategy in self.strategies[timeframe][:]:                                                                        
                    if strategy.is_stopped():
                        self.remove_strategy(strategy)
                                                                                                                                                                                                                                                    
        except:
            self.log.error(traceback.format_exc())
        finally:
            self.log.info('StrategyRunner.evaluate %s - end' % (TIMEFRAME[timeframe]))
        
        
    def evaluate_until_now(self):
        try:
            with self.lock:    
                self.log.info('StrategyRunner.evaluate_until_now - start')      
                
                for timeframe in self.strategies.iterkeys():            
                                                                                                 
                    self.account.before_evaluate(timeframe)
                                
                    for strategy in self.strategies[timeframe]:
                        strategy.evaluate_until_now()
                
        except:
            self.log.error(traceback.format_exc())
        finally:
            self.log.info('StrategyRunner.evaluate_until_now - end')
                                
        
    def stop_strategy(self, liverunconfig_id):
        try:
            with self.lock:
                                
                strategy = self.liverunconfigs[liverunconfig_id]
                                
                if strategy.stop():
                    self.remove_strategy(strategy)
                                            
        except:
            self.log.error(traceback.format_exc())
        
        
    def abort_strategy(self, liverunconfig_id):        
        try:
            with self.lock:
                
                strategy = self.liverunconfigs[liverunconfig_id]
                
                if strategy.abort():                                                                                                                                                                        
                    self.remove_strategy(strategy)                                                                                                                    
        
        except:
            self.log.error(traceback.format_exc())        
        
    
    def remove_strategy(self, strategy):                
        self.log.info('removing strategy %d' % strategy.strategy_id)            
        from tradeengine import feedserver        
        
        for symbol in strategy.symbols: 
            feedserver.unregister_instrument(symbol, strategy.timeframe)
                
        self.strategies[strategy.timeframe].remove(strategy)
        
        if not self.strategies[strategy.timeframe]:
            del self.strategies[strategy.timeframe]              
        
        del self.liverunconfigs[strategy.liverunconfig_id]                          
        self.log.info('strategy removed')
                                                      
                        
    def on_new_day(self):
        self.log.info('StrategyRunner.on_new_day')
        for strategy in self.liverunconfigs.values():
            strategy.on_new_day()
            
        self.account.onNewDay()
            
            
    def on_tick(self):
        self.log.debug('StrategyRunner.on_tick - start ------- ')
        with self.lock:
           
            for strategy in self.liverunconfigs.values():                
                strategy.on_tick()
                        
            self.account.onTick()                        
            
            self.log.debug('StrategyRunner.on_tick - end ------- ')
    
            
    def has_liverunconfig(self, liverunconfig_id):
        return liverunconfig_id in self.liverunconfigs        
    
    
    def has_strategies(self):
        return len(self.liverunconfigs) > 0 

    def __str__(self):
        str = "StrategyRunner for %s / %s\n" % (self.account_name, self.username)
        str += "Account summary:\n"
        str += "Strategy summary:\n"
        for tf in self.strategies:
            str += "on %s:\n" % TIMEFRAME[tf]
            for s in self.strategies[tf]:
                str += "\tID: %d, %s, state: %s, open trades: %d\n" % (s.strategy_id, s.symbols, LIVERUN_STATUS_STR[s.state].upper(), s.open_trades_num)
                
        return str
