# -*- encoding: utf-8 -*- 
'''
Created on Dec 19, 2012

@author: gdt
'''
import cherrypy
from gdtlive.control.router import ajax_wrapper
from gdtlive.languages.default import OK, EXCEPTION_OCCURED
from gdtlive.admin.system.log import errorlog, extra
import traceback
from gdtlive.core.constants import FIX_INCOMING, FIX_OUTGOING
from gdtlive.utils import OrderedDict
from ast import literal_eval
import gdtlive.store.db as db
import gdtlive.config as config
import gdtlive.core.tradeengine as engine
from sqlalchemy.sql import select



@cherrypy.expose
@ajax_wrapper
def start_portfolio(ids):
    '''Starts live portfolio

    @url:     /web/start_portfolio

    @rtype:    (bool, str) tuple
    @return:    Állapotjelzés, szöveges üzenet
    '''    
    ids = literal_eval(ids)
    try:
        import gdtlive.core.tradeengine as engine
        for portfolio_id in ids:        
            engine.trade_engine.start_portfolio(portfolio_id)
        
    except:
        errorlog.error('Exception raised during portfolio start', extra=extra(globals={}, locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    return True, OK


@cherrypy.expose
@ajax_wrapper
def stop_portfolio(ids):
    '''Stops live portfolio

    @url:     /web/stop_portfolio

    @rtype:    (bool, str) tuple
    @return:    Állapotjelzés, szöveges üzenet
    '''
    ids = literal_eval(ids)
    try:
        import gdtlive.core.tradeengine as engine        
        for portfolio_id in ids:
            engine.trade_engine.stop_portfolio(portfolio_id)
        
    except:
        errorlog.error('Exception raised during portfolio stop', extra=extra(globals={}, locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    return True, OK


@cherrypy.expose
@ajax_wrapper
def abort_portfolio(ids):
    '''Abprts live portfolio

    @url:     /web/abort_portfolio

    @rtype:    (bool, str) tuple
    @return:    Állapotjelzés, szöveges üzenet
    '''
    ids = literal_eval(ids)
    try:
        import gdtlive.core.tradeengine as engine       
        for portfolio_id in ids: 
            engine.trade_engine.abort_portfolio(portfolio_id)
        
    except:
        errorlog.error('Exception raised during portfolio abort', extra=extra(globals={}, locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    return True, OK


@cherrypy.expose
@ajax_wrapper
def start_strategy(portfolio_id, ids):
    '''Starts live strategies

    @url:     /web/start_strategy

    @rtype:    (bool, str) tuple
    @return:    Állapotjelzés, szöveges üzenet
    '''
    portfolio_id = int(portfolio_id)
    ids = literal_eval(ids)
    try:
        import gdtlive.core.tradeengine as engine
        
        for strategy_id in ids:
            engine.trade_engine.start_strategy(portfolio_id, strategy_id)
        
    except:
        errorlog.error('Exception raised during strategy start', extra=extra(globals={}, locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    return True, OK


@cherrypy.expose
@ajax_wrapper
def stop_strategy(portfolio_id, ids):
    '''Stops live strategies

    @url:     /web/stop_strategy

    @rtype:    (bool, str) tuple
    @return:    Állapotjelzés, szöveges üzenet
    '''
    portfolio_id = int(portfolio_id)
    ids = literal_eval(ids)
    try:
        import gdtlive.core.tradeengine as engine
        
        for strategy_id in ids:
            engine.trade_engine.stop_strategy(portfolio_id, strategy_id)
        
    except:
        errorlog.error('Exception raised during strategy stop', extra=extra(globals={}, locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    return True, OK


@cherrypy.expose
@ajax_wrapper
def abort_strategy(portfolio_id, ids):
    '''Aborts live strategies

    @url:     /web/abort_strategy

    @rtype:    (bool, str) tuple
    @return:    Állapotjelzés, szöveges üzenet
    '''
    portfolio_id = int(portfolio_id)
    ids = literal_eval(ids)
    try:
        import gdtlive.core.tradeengine as engine
        
        for strategy_id in ids:
            engine.trade_engine.abort_strategy(portfolio_id, strategy_id)
        
    except:
        errorlog.error('Exception raised during strategy abort', extra=extra(globals={}, locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    return True, OK


@cherrypy.expose
@ajax_wrapper
def start_account(portfolio_id, ids):
    '''Starts live accounts

    @url:     /web/start_account

    @rtype:    (bool, str) tuple
    @return:    Állapotjelzés, szöveges üzenet
    '''
    portfolio_id = int(portfolio_id)
    ids = literal_eval(ids)
    try:
        import gdtlive.core.tradeengine as engine
        
        for paccount_id in ids:
            engine.trade_engine.start_account(portfolio_id, paccount_id)
        
    except:
        errorlog.error('Exception raised during account start', extra=extra(globals={}, locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    return True, OK


@cherrypy.expose
@ajax_wrapper
def stop_account(portfolio_id, ids):
    '''Stops live accounts

    @url:     /web/stop_account

    @rtype:    (bool, str) tuple
    @return:    Állapotjelzés, szöveges üzenet
    '''
    portfolio_id = int(portfolio_id)
    ids = literal_eval(ids)
    try:
        import gdtlive.core.tradeengine as engine
        
        for paccount_id in ids:
            engine.trade_engine.stop_account(portfolio_id, paccount_id)
        
    except:
        errorlog.error('Exception raised during account stop', extra=extra(globals={}, locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    return True, OK


@cherrypy.expose
@ajax_wrapper
def abort_account(portfolio_id, ids):
    '''Aborts live accounts

    @url:     /web/abort_account

    @rtype:    (bool, str) tuple
    @return:    Állapotjelzés, szöveges üzenet
    '''
    portfolio_id = int(portfolio_id)
    ids = literal_eval(ids)
    try:
        import gdtlive.core.tradeengine as engine
        
        for paccount_id in ids:
            engine.trade_engine.abort_account(portfolio_id, paccount_id)
        
    except:
        errorlog.error('Exception raised during account abort', extra=extra(globals={}, locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    return True, OK

@cherrypy.expose
@ajax_wrapper
def test_connection(account_id):
    '''Tests if given live account can connect

    @url:     /web/test_connection

    @rtype:    (bool, str) tuple
    @return:    Állapotjelzés, szöveges üzenet
    '''
    import gdtlive.core.fix.dukascopy.fix as fix
    import socket
    import ssl
    from datetime import datetime
        

    def send(sock, message, sendercompid, targetcompid, msgseqnum):   
        global MsgSeqNum
        try:     
            message.MsgSeqNum = msgseqnum
            message.SendingTime = datetime.utcnow()        
            message.SenderCompID = sendercompid
            message.TargetCompID = targetcompid        
            encoded = fix.encode(message)        
            sock.send(encoded)            
            return True, ''
        except:
            return False, traceback.format_exc()
    
    def close(sock):
        try:
            sock.shutdown(socket.SHUT_RDWR)
        except:
            pass
        try:
            sock.close()
        except:
            pass
        
    session = db.Session()
    account = session.query(db.Account).get(account_id)
    if not account:
        return False, 'Account not found!'
    
    if account.benchmark:
        return False, 'Testing a Benchmark account is not possible'
    
    log = ''    
    log += 'creating network socket\n'
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except:
        log += 'socket creation failed: \n'
        log += traceback.format_exc()
        log += '------------------\nTEST FAILED'
        return True, log
    
    log += 'network socket created\n'
    if account.use_ssl:
        log += 'ssl wrap'
        sock = ssl.wrap_socket(sock)        
    sock.settimeout(1)
    try:
        sock.connect((account.server_url, account.server_port))
    except:
        log += 'network connection failed \n'
        log += traceback.format_exc()
        log += '------------------\nTEST FAILED'
        return True, log
    
    log += 'network connection OK\n'
    log += 'sending Logon message:\n'
    logon = fix.Logon(Password=account.password, Username=account.username, HeartBtInt=0, ResetSeqNumFlag=True, EncryptMethod=0)
    log += '%s\n' % logon    
    result, reason = send(sock, logon, account.sendercompid, account.targetcompid, 1)
    if result:
        log += 'Logon message sent\n'
    else:
        log += 'sending Logon failed:\n'
        log += reason
        return True, log
    
    try:    
        data = sock.recv(4096)        
    except:
        log += 'connection dropped: \n'
        log += traceback.format_exc()
        log += '------------------\nTEST FAILED'
        return True, log
    
    if not data:
        log += 'connection closed by the server'    
        log += '------------------\nTEST FAILED'    
        return True, log
    
    try:   
        reply = fix.decode(data)
        log += 'received: %s\n' % reply
    except:
        try:
            data += sock.recv(4096)        
            reply = fix.decode(data)
        except:                    
            log += 'error decoding message: %s\n' % data
            log += traceback.format_exc()
            log += '------------------\nTEST FAILED'
            close(sock)
            return True, log
    
    if type(reply == fix.Logon):
        log += 'Confirmation Logon received\n'
        log += '------------------\nTEST SUCCEEDED'
        send(sock, fix.Logout, account.sendercompid, account.targetcompid, 2)
        close(sock)
        return True, log
    else:
        log += '------------------\nTEST FAILED'
        close(sock)
        return True, log                    
    
    
    return True, 'Fasza'


@cherrypy.expose
@ajax_wrapper
def get_engine_details(portfolio_id, account_id):        
    portfolio_id = int(portfolio_id)
    account_id = int(account_id)
    result = engine.trade_engine.get_engine_details(portfolio_id, account_id)
    return True, OK, result


@cherrypy.expose
@ajax_wrapper
def get_engine_logs(portfolio_id, account_id, logtype):
    portfolio_id = int(portfolio_id)
    account_id = int(account_id)
    logtype = int(logtype)    
    result = engine.trade_engine.get_engine_logs(portfolio_id, account_id, logtype)        
    return True, result


@cherrypy.expose
@ajax_wrapper
def get_engine_messages(portfolio_id, account_id):   
    portfolio_id = int(portfolio_id)
    account_id = int(account_id)
    result = engine.trade_engine.get_engine_last_messages(portfolio_id, account_id)
    return True, OK, result


@cherrypy.expose
@ajax_wrapper
def engine_connect(portfolio_id, account_id):
    portfolio_id = int(portfolio_id)
    account_id = int(account_id)    
    return engine.trade_engine.check_connection(portfolio_id, account_id)


@cherrypy.expose
@ajax_wrapper
def engine_disconnect(portfolio_id, account_id):
    portfolio_id = int(portfolio_id)
    account_id = int(account_id)
    engine.trade_engine.shutdown_account(portfolio_id, account_id)    
    return True, OK


@cherrypy.expose
@ajax_wrapper
def trade_repair(portfolio_id, account_id, trade_id):
    engine.trade_engine.trade_repair(int(portfolio_id), int(account_id), int(trade_id))
    return True, OK

@cherrypy.expose
@ajax_wrapper
def trade_close(portfolio_id, account_id, trade_id):
    engine.trade_engine.trade_close(int(portfolio_id), int(account_id), int(trade_id))
    return True, OK

@cherrypy.expose
@ajax_wrapper
def trades_close(portfolio_id, account_id, ids):
    for trade_id in literal_eval(ids):
        engine.trade_engine.trade_close(int(portfolio_id), int(account_id), int(trade_id))
    return True, OK

@cherrypy.expose
@ajax_wrapper
def trade_markasclosed(portfolio_id, account_id, trade_id):
    engine.trade_engine.trade_markasclosed(int(portfolio_id), int(account_id), int(trade_id))
    return True, OK

@cherrypy.expose
@ajax_wrapper
def trade_fixmessages(portfolio_id, account_id, trade_id):        
    result = OrderedDict()    
    table = db.FIXOrderHistory.__table__
    messages = db.engine.execute(select([table.c.time, table.c.direction, table.c.message], table.c.trade_id == trade_id).order_by(table.c.id.asc()))        
    row = messages.fetchone()    
    while row:
        try:
            t = row[0]
            d = row[1]
            m = row[2]        
            d = 'IN' if d == FIX_INCOMING else 'OUT'
            result[t.strftime('%Y-%m-%d %H:%M:%S')+ '   '+d+':'] = m
        except:
            pass            
        row = messages.fetchone()                                                                    
    return True, OK, result

@cherrypy.expose
@ajax_wrapper
def trade_mo_sendopen(portfolio_id, account_id, trade_id):
    engine.trade_engine.trade_mo_sendopen(int(portfolio_id), int(account_id), int(trade_id))
    return True, OK

@cherrypy.expose
@ajax_wrapper
def trade_mo_sendclose(portfolio_id, account_id, trade_id):
    engine.trade_engine.trade_mo_sendclose(int(portfolio_id), int(account_id), int(trade_id))
    return True, OK

@cherrypy.expose
@ajax_wrapper
def trade_co_place(portfolio_id, account_id, trade_id, co_type):
    """
    co_type: 0: stoploss, 1 : takeprofit
    """
    engine.trade_engine.trade_co_place(int(portfolio_id), int(account_id), int(trade_id), int(co_type))
    return True, OK

@cherrypy.expose
@ajax_wrapper
def trade_co_cancel(portfolio_id, account_id, trade_id, co_type):
    """
    co_type: 0: stoploss, 1 : takeprofit
    """    
    engine.trade_engine.trade_co_cancel(int(portfolio_id), int(account_id), int(trade_id), int(co_type))
    return True, OK

@cherrypy.expose
@ajax_wrapper
def trade_co_filled(portfolio_id, account_id, trade_id, co_type):
    """
    co_type: 0: stoploss, 1 : takeprofit
    """    
    engine.trade_engine.trade_co_filled(int(portfolio_id), int(account_id), int(trade_id), int(co_type))
    return True, OK

