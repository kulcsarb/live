# -*- encoding: utf-8 -*- 
'''
Created on Jun 25, 2012

@author: gdtlive
'''
from gdtlive.constants import TIMEFRAME, PIP_MULTIPLIER, PRICE_PIPVALUE, PRICE_TIME, PRICE_ASKOPEN, PRICE_ASKHIGH, PRICE_ASKLOW, PRICE_ASKCLOSE, PRICE_BIDOPEN, PRICE_BIDHIGH, PRICE_BIDLOW, PRICE_BIDCLOSE, PRICE_VOLUME
from gdtlive.core.constants import *
from fix.initiator import FIXInitiator
import gdtlive.store.db as db
import gdtlive.config as config
import fix.dukascopy.fix as fix
import historic
import gdtlive.historic.datarow as h_datarow

from datetime import datetime, timedelta
from threading import Thread, Lock
from Queue import Queue, Empty
import time
import calendar
import random
import traceback
import copy
import logging
import subprocess
import StringIO
import numpy as np
import ast 
import os
import cPickle
from datetime import date


LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"

log = logging.getLogger('gdtlive.core.feed')

#handler = logging.FileHandler('feed-error.log')
#handler.setFormatter(logging.Formatter(LOG_FORMAT))
#handler.setLevel(logging.ERROR)
#log.addHandler(handler)
#
#handler = logging.FileHandler('feed-info.log')
#handler.setFormatter(logging.Formatter(LOG_FORMAT))
#handler.setLevel(logging.INFO)
#log.addHandler(handler)
#
#handler = logging.FileHandler('feed-debug.log')
#handler.setFormatter(logging.Formatter(LOG_FORMAT))
#handler.setLevel(logging.DEBUG)
#log.addHandler(handler)
 

DATAROW = {}

TRADABLE = {}


def load_candles(symbols, timeframe, from_date, to_date):
    global DATAROW
    try:        
        log.info('loading candles for : %s %s-%s %s' % (TIMEFRAME[timeframe], from_date, to_date, symbols))
        if type(symbols) == list:        
            _symbols = []
            for symbol in symbols:            
                _symbols.append(symbol[:3] + '/' + symbol[3:])
            _symbols = ' '.join(_symbols)
        else:
            _symbols = symbols[:3] + '/' + symbols[3:]
            
        log.info('%s %d %d %d %s' % (config.PRELOADER_JAR, timeframe, calendar.timegm(from_date.utctimetuple()), calendar.timegm(to_date.utctimetuple()), _symbols))
        
        tempfile = os.tmpfile()
        #process = subprocess.Popen('%s %d %d %s' % (config.PRELOADER_JAR, timeframe, length, _symbols), stdout=tempfile, shell=True)
        #process.wait()
        max_restart_count = 10
        restart_count = 0
        timeout = 30
        elapsed = 0
        process = subprocess.Popen('%s %d %d %d %s' % (config.PRELOADER_JAR, timeframe, calendar.timegm(from_date.utctimetuple()), calendar.timegm(to_date.utctimetuple()), _symbols), stdout=tempfile, shell=True)
        while True:            
            retcode = process.poll()
            if retcode is not None:
                log.info('preload.jar finished')
                break
            time.sleep(1)
            elapsed += 1
            log.info('waiting for preloader... %d' % elapsed)
            if elapsed >= timeout:
                if restart_count < max_restart_count:
                    log.info('restarting preloader, retries left: %d' % (max_restart_count - restart_count))
                    process.kill()
                    tempfile = os.tmpfile()
                    process = subprocess.Popen('%s %d %d %d %s' % (config.PRELOADER_JAR, timeframe, calendar.timegm(from_date.utctimetuple()), calendar.timegm(to_date.utctimetuple()), _symbols), stdout=tempfile, shell=True)
                    elapsed = 0
                    restart_count += 1
                else:
                    log.info('maximum restart count reached, giving up...')
                    log.error('Unable to use preloader! ')
                    return False
                
        
        tempfile.seek(0, os.SEEK_SET)
        output = tempfile.read()
        tempfile.close() 
        
        if process.returncode == 0:
            log.info('load finished')            
            log.debug(output)
            lines = output.split('\n')
            if not lines:
                log.error('Wrong output: %s' % output)
                return 
            
            while not lines[0].startswith('{'):                 
                del lines[0]
            while not lines[-1].startswith('}'):                 
                del lines[-1]                        
                        
            result = ''.join(lines)
            datarows = ast.literal_eval(result)                        
            for _symbol in datarows:
                symbol = _symbol[:3] + _symbol[4:]
                candles = datarows[_symbol]
                log.info('%s first candle: %s' % (symbol, datetime.utcfromtimestamp(candles[0][0])))
                log.info('%s last candle: %s' % (symbol, datetime.utcfromtimestamp(candles[-1][0])))                         
                for candle in reversed(candles):                
                    dt = datetime.utcfromtimestamp(candle[0])
                    if dt not in DATAROW[timeframe][symbol][PRICE_TIME]:                                                                     
                        if not DATAROW[timeframe][symbol][PRICE_TIME]:                                                                                                                 
                            position = 0                        
                        elif dt + timedelta(minutes=timeframe) in DATAROW[timeframe][symbol][PRICE_TIME]:
                            position = DATAROW[timeframe][symbol][PRICE_TIME].index(dt + timedelta(minutes=timeframe))
                        elif dt - timedelta(minutes=timeframe) in DATAROW[timeframe][symbol][PRICE_TIME]:
                            position = DATAROW[timeframe][symbol][PRICE_TIME].index(dt - timedelta(minutes=timeframe)) + 1
                        elif dt < DATAROW[timeframe][symbol][PRICE_TIME][0]:
                            position = 0
                        elif dt > DATAROW[timeframe][symbol][PRICE_TIME][-1]:
                            position = len(DATAROW[timeframe][symbol][PRICE_TIME])
                            
                        log.debug('add %s %s, pos: %d' % (symbol, dt, position))
                        DATAROW[timeframe][symbol][PRICE_TIME].insert(position, dt)                    
                        DATAROW[timeframe][symbol][PRICE_ASKOPEN].insert(position, candle[1])
                        DATAROW[timeframe][symbol][PRICE_ASKHIGH].insert(position, candle[2])
                        DATAROW[timeframe][symbol][PRICE_ASKLOW].insert(position, candle[3])
                        DATAROW[timeframe][symbol][PRICE_ASKCLOSE].insert(position, candle[4])
                        DATAROW[timeframe][symbol][PRICE_BIDOPEN].insert(position, candle[5])
                        DATAROW[timeframe][symbol][PRICE_BIDHIGH].insert(position, candle[6])
                        DATAROW[timeframe][symbol][PRICE_BIDLOW].insert(position, candle[7])
                        DATAROW[timeframe][symbol][PRICE_BIDCLOSE].insert(position, candle[8])
                        DATAROW[timeframe][symbol][PRICE_VOLUME].insert(position, candle[9])
                        DATAROW[timeframe][symbol][PRICE_PIPVALUE].insert(position, 0.0001)
            
                for price in DATAROW[timeframe][symbol]:
                    if price == PRICE_TIME:                                                                
                        historic.DATAROWS[timeframe][symbol][price] = DATAROW[timeframe][symbol][price]
                    else:
                        historic.DATAROWS[timeframe][symbol][price] = np.array(DATAROW[timeframe][symbol][price], dtype=np.double)
                                
                            
            log.info('candles loaded')
            log.info('DATAROW LENGTH: %d' % len(historic.DATAROWS[timeframe][symbol][PRICE_TIME]))
            log.info('CANDLES: %s' % str(historic.DATAROWS[timeframe][symbol][PRICE_TIME][-10:]))            
            return True
        
        else:                        
            log.error('load failed!')            
            log.error(output)
            return False
                
    except:        
        log.error(traceback.format_exc())
    
    return False


def load_wrapper(symbols, timeframe, length):
    success = False
    while not success:
        success = load_candles(symbols, timeframe, length)    
    


class DatarowBuilder:
    
    def __init__(self):
        global DATAROW
                
        historic.CURRENT_PRICE = {}
        DATAROW = {}
        self.current = {}                                                
        self.strategy_counter = {}
        self.lock = Lock()
        self.disconnect_time = None
        self.datarows_cleaned = False
        self.stop = False        
            
    
    def register(self, symbol, timeframe):                
        with self.lock:
            
            global DATAROW                    
            self.current.setdefault(timeframe, {})                            
            DATAROW.setdefault(timeframe, {})        
            historic.DATAROWS.setdefault(timeframe, {})
            self.strategy_counter.setdefault(timeframe, {})
                                    
            historic.CURRENT_PRICE.setdefault(symbol, {'ASK':0, 'BID':0, 'PIPVALUE':0.0001})
                                   
            self.strategy_counter[timeframe].setdefault(symbol, 0)
            DATAROW[timeframe].setdefault(symbol, {})
            historic.DATAROWS[timeframe].setdefault(symbol, {})
            
            if symbol not in self.current[timeframe]:                            
                self.current[timeframe][symbol] = {}
                for price in xrange(PRICE_TIME, PRICE_PIPVALUE+1):                    
                    self.current[timeframe][symbol][price] = 0
                    DATAROW[timeframe][symbol][price] = []                    
                    # numpy conversion
                    if price == PRICE_TIME:
                        historic.DATAROWS[timeframe][symbol][price] = []
                    else:
                        historic.DATAROWS[timeframe][symbol][price] = np.array(DATAROW[timeframe][symbol][price], dtype=np.double)
                        
                self.current[timeframe][symbol][PRICE_TIME] = None      
            
            else:
                self.strategy_counter[timeframe][symbol] += 1
                        
        
        
                        
    def unregister(self, symbol, timeframe):
        try:
            self.strategy_counter[timeframe][symbol] -= 1                        
        except:
            pass
        
        
    def has_consumer(self, symbol, timeframe):
        try:            
            return bool(self.strategy_counter[timeframe][symbol])
        except:
            return False
                
    
    def remove(self, symbol, timeframe):
        with self.lock:     
            try:       
                del self.current[timeframe][symbol]
            except:
                pass
            try:
                del DATAROW[timeframe][symbol]
            except:
                pass
            try:
                del historic.DATAROWS[timeframe][symbol]
            except:
                pass
            try:
                del self.strategy_counter[timeframe][symbol]
                if len(self.strategy_counter[timeframe]) == 0:
                    del self.strategy_counter[timeframe]
            except:
                pass
            log.info('datarow removed: %s_%s' % (symbol, TIMEFRAME[timeframe]))
        
    
    def preload(self, symbol, timeframe):     
        global DATAROW               
        session = db.Session()
        try:            
            datarow_id = session.query(db.DatarowDescriptor.id).filter(db.DatarowDescriptor.symbol==symbol).filter(db.DatarowDescriptor.timeframe_num == timeframe).first()[0]
            log.info('Loading datarow id %d, %s, %s' % (datarow_id, symbol, TIMEFRAME[timeframe]))
            
            result = h_datarow.load(datarow_id, date(2005,1,30), date.today())
            
            log.info('Loaded from %s to %s' % (result[PRICE_TIME][0], result[PRICE_TIME][-1]))
            
            DATAROW[timeframe][symbol] = result
                    
        except:
            log.error('Failed to load historic data from local store for %s' % symbol )
            log.error(traceback.format_exc())
        finally:
            session.close()


    def calc_pipvalue(self, timeframe):
        '''Kiszámolja az összes betöltött adatsor pip értékét'''
        global DATAROW
        log.info('Calculating pipvalues')
        try:
            for symbol in DATAROW[timeframe]:
                if PRICE_PIPVALUE not in DATAROW[timeframe][symbol]:
                    DATAROW[timeframe][symbol][PRICE_PIPVALUE] = []
                    
                    if symbol[3:] == 'USD':
                        self._pipvalue_calc_case1(timeframe, symbol)
                    elif symbol[:3] == 'USD':
                        self._pipvalue_calc_case2(timeframe, symbol)
                    else:
                        self._pipvalue_calc_case3(timeframe, symbol)
        except:
            log.error('Failed to compute pipvalue for historical data')
            log.error(traceback.format_exc())
            
        
    def _pipvalue_calc_case1(self, timeframe, symbol):
        global DATAROW
        
        '''Kiszámolja az USD-re végződő instrumentumok pip értékét minden gyertyára nézve.
        
        Az eredményt a `candles`[symbol][PRICE_PIPVALUE] tömbben tárolja
                
        
        :Parameters:
            symbol : str
                Az instrumentum azonosítója, ahol instrumentum = "*USD"  (AUDUSD, EURUSD, GBPUSD, ....)
        
        Algoritmus 
        ==========
        
        Determine your account’s currency (in about 90% cases it’s USD). If it’s the same currency 
        as the based currency of the pair (the second one, which goes after “/”) then you should 
        simply multiply the amount of currency units in your position (100,000 for 1 standard lot) 
        by the size of one pip. You’ll get a pip value of 10 currency units per 1 standard lot. 
        
        '''
        datarow_length = len(DATAROW[timeframe][symbol][PRICE_TIME])
        DATAROW[timeframe][symbol][PRICE_PIPVALUE] = [PIP_MULTIPLIER[symbol] for i in range(datarow_length)]        



    def _pipvalue_calc_case2(self, timeframe, symbol):
        global DATAROW
        '''Kiszámolja az USD-vel kezdődő instrumentumok pip értékét minden gyertyára nézve.
        
        Az eredményt a `candles`[symbol][PRICE_PIPVALUE] tömbben tárolja
                
        
        :Parameters:
            symbol : str
                Az instrumentum azonosítója, ahol instrumentum = "USD*"  (USDCAD, USDZAR, ....)
        
        Algoritmus 
        ==========
        
        If the account’s currency is different from the base currency but is the same as the currency 
        pair’s long currency (the first one, which goes before “/”) then check this currency pair’s 
        current Ask rate (the highest of the rates).
        You should multiply the amount of currency units in your position (100,000 for 1 standard lot) 
        by the size of one pip and then divide the result by the Ask rate 
        '''        
        datarow_length = len(DATAROW[timeframe][symbol][PRICE_TIME])
        DATAROW[timeframe][symbol][PRICE_PIPVALUE] = [0] * datarow_length        
        for i in xrange(len(DATAROW[timeframe][symbol][PRICE_TIME])):
            value = 1 * PIP_MULTIPLIER[symbol] / DATAROW[timeframe][symbol][PRICE_ASKCLOSE][i]
            DATAROW[timeframe][symbol][PRICE_PIPVALUE][i] = value


    def _pipvalue_calc_case3(self, timeframe, symbol):
        global DATAROW
        '''Kiszámolja az adott keresztárfolyam pip értékét minden gyertyára nézve.

        Az eredményt a `candles`[symbol][PRICE_PIPVALUE] tömbben tárolja

        :Parameters:
            symbol : str
                Az instrumentum azonosítója, ahol az instrumentumban nem szerepel az USD  (CADJPY, AUDNZD, ....)

        Algoritmus
        ==========

        If the account’s currency is different from any of the currencies from the pair of the position,
        you have to check the current rate of this currency relative to the base currency of the pair:
            3.A:
                If the currency pair combined of the account’s currency and the base currency of the position
                has the account’s currency as the base currency (second, after “/”) then you should check
                and remember its current Bid rate (the lowest of the rates).
            3.B
                If the currency pair combined of the account’s currency and the base currency of the position
                has the account’s currency as the long currency (first, before “/”) then you should check
                and remember its current Ask rate (the highest of the rates).

        You should multiply the amount of currency units in your position (100,000 for 1 standard lot) by
        the size of one pip and either multiply the result by the Bid rate received in step 3a or divide
        the result by the Ask rate received in step 3b.

        :TODO: unittest
        '''
        
        symbol_3B = 'USD' + symbol[:3]   # CASE 3.B : account currency as long 
        symbol_3A = symbol[:3] + 'USD'    # CASE 3.A : account currency as short    
        case = ''
        if symbol_3B in DATAROW[timeframe]:
            case = '3B'            
        elif symbol_3A in DATAROW[timeframe]:
            case = '3A'            

        datarow_length = len(DATAROW[timeframe][symbol][PRICE_TIME])
        DATAROW[timeframe][symbol][PRICE_PIPVALUE] = [0] * datarow_length        
        
        if case == '3A':
            for i in xrange(len(DATAROW[timeframe][symbol][PRICE_TIME])):                                      
                DATAROW[timeframe][symbol][PRICE_PIPVALUE][i] = PIP_MULTIPLIER[symbol_3A] * DATAROW[timeframe][symbol_3A][PRICE_ASKCLOSE][i]

        elif case == '3B':
            for i in xrange(len(DATAROW[timeframe][symbol][PRICE_TIME])):                                 
                DATAROW[timeframe][symbol][PRICE_PIPVALUE][i] = PIP_MULTIPLIER[symbol_3B] / DATAROW[timeframe][symbol_3B][PRICE_ASKCLOSE][i]

        else:
            # ha nincs meg az árfolyam, 1-nek vesszük.... 
            for i in xrange(len(DATAROW[timeframe][symbol][PRICE_TIME])):
                value = 0.0001
                DATAROW[timeframe][symbol][PRICE_PIPVALUE][i] = value                


    
    
    def gapfill(self, symbol, timeframe, gapfill_from, gapfill_to):
        result = load_candles(symbol, timeframe, gapfill_from, gapfill_to)                            
        self.calc_pipvalue(timeframe)
        return result
                    
                
    
    def on_disconnect(self):
        if not self.disconnect_time:
            self.disconnect_time = datetime.utcnow()        
    
    
    def on_connect(self):        
        if self.disconnect_time and self.disconnect_time + timedelta(seconds=30) < datetime.utcnow():            
            self.clean_current()   
        
        self.disconnect_time = None
                   
                   
    def clean_current(self):        
        log.info('Cleaning current candles')
        for timeframe in self.current:
            for symbol in self.current[timeframe]:
                for price in self.current[timeframe][symbol]:                    
                    if price == PRICE_TIME:
                        self.current[timeframe][symbol][PRICE_TIME] = None                        
                    else:
                        self.current[timeframe][symbol][price] = 0                         

                                                                                                
    def get_pipvalue(self, symbol):
        pipvalue = 0.0001
        try:
            if symbol.endswith('USD'):
                pipvalue = PIP_MULTIPLIER[symbol]
            elif symbol.startswith('USD'):
                pipvalue = PIP_MULTIPLIER[symbol] / historic.CURRENT_PRICE[symbol]['ASK']
            else:
                symbol_3B = 'USD' + symbol[:3]   # CASE 3.B : account currency as long 
                symbol_3A = symbol[:3] + 'USD'    # CASE 3.A : account currency as short                
                if symbol_3A in historic.CURRENT_PRICE:
                    pipvalue = PIP_MULTIPLIER[symbol_3A] * historic.CURRENT_PRICE[symbol_3A]['ASK'] 
                elif symbol_3B in historic.CURRENT_PRICE:
                    pipvalue = PIP_MULTIPLIER[symbol_3B] /  historic.CURRENT_PRICE[symbol_3B]['ASK']
                else:
                    pipvalue = 0.0001
        except:
            pass
            
        return pipvalue

    
    def add_tick(self, symbol, ask_price, ask_volume, bid_price, bid_volume):
        with self.lock:                                            
            if symbol not in historic.CURRENT_PRICE:
                return 
            
            historic.CURRENT_PRICE[symbol]['ASK'] = ask_price
            historic.CURRENT_PRICE[symbol]['BID'] = bid_price
            historic.CURRENT_TIME = datetime.utcnow()
            
            pipvalue = self.get_pipvalue(symbol)
            
            historic.CURRENT_PRICE[symbol]['PIPVALUE'] = pipvalue
            
            for timeframe in self.current:
                if symbol not in self.current[timeframe]:
                    continue
                
                if not self.current[timeframe][symbol][PRICE_TIME]:
                    continue
                
                candle = self.current[timeframe][symbol]
                                
                if not candle[PRICE_ASKOPEN]:                
                    candle[PRICE_ASKOPEN] = candle[PRICE_ASKCLOSE] = candle[PRICE_ASKLOW] = candle[PRICE_ASKHIGH] = ask_price
                    
                if not candle[PRICE_BIDOPEN]:                
                    candle[PRICE_BIDOPEN] = candle[PRICE_BIDCLOSE] = candle[PRICE_BIDLOW] = candle[PRICE_BIDHIGH] = bid_price
                    
                candle[PRICE_ASKCLOSE] = ask_price
                candle[PRICE_BIDCLOSE] = bid_price
                
                candle[PRICE_ASKLOW] = min(candle[PRICE_ASKLOW], ask_price)
                candle[PRICE_BIDLOW] = min(candle[PRICE_BIDLOW], bid_price)
                
                candle[PRICE_ASKHIGH] = max(candle[PRICE_ASKHIGH], ask_price)                                    
                candle[PRICE_BIDHIGH] = max(candle[PRICE_BIDHIGH], bid_price)
                
                candle[PRICE_VOLUME] += (ask_volume + bid_volume) / 1000000.0                                                    
                candle[PRICE_PIPVALUE] = pipvalue            
            
    
    def new_candle(self, current_time):        
        with self.lock:
            if self.stop:
                log.info('Datarow builder stopped, skip new_candle')
                return False
                        
            new_candle_added = False
            global DATAROW
            # DO NOT REMOVE IT, OR I WILL KILL YOU, MOTHERFUCKER!
            current_time = current_time.replace(second=0, microsecond=0)
            
            historic.CURRENT_TIME = current_time
            
            if self.disconnect_time and self.disconnect_time + timedelta(seconds=30) < datetime.utcnow():            
                self.clean_current()                   
                                
            for timeframe in sorted(self.current.keys()):
                preload = []
                gapfill_symbols = []
                gapfill_missing_candles = 0
                gapfill_from = None
                gapfill_to = None
                for symbol in sorted(self.current[timeframe]):
                    
                    if calendar.timegm(current_time.utctimetuple()) % (timeframe*60) == 0 :
                        
                        if not self.current[timeframe][symbol][PRICE_TIME]:
                            log.info('empty current[%s][%s], preparing for preload ' % (TIMEFRAME[timeframe], symbol))
                            if len(DATAROW[timeframe][symbol][PRICE_TIME]):
                                sym_from = DATAROW[timeframe][symbol][PRICE_TIME][-1] + timedelta(minutes = timeframe)
                                sym_to = current_time - timedelta(minutes = timeframe)
                                if (not gapfill_from or gapfill_from < sym_from):
                                    gapfill_from = sym_from
                                if (not gapfill_to or gapfill_to > sym_to):
                                    gapfill_to = sym_to                                                                
                                #missing_candles = (calendar.timegm(current_time.utctimetuple()) - calendar.timegm(last_candle_time.utctimetuple())) / (timeframe * 60)
                                gapfill_symbols.append(symbol)
                                #gapfill_missing_candles = missing_candles
                        
                        # nem elif!
                        if self.current[timeframe][symbol][PRICE_VOLUME] and self.current[timeframe][symbol][PRICE_TIME]:                     
                                                        
                            for price in DATAROW[timeframe][symbol]:                        
                                DATAROW[timeframe][symbol][price].append(self.current[timeframe][symbol][price])                        
                                
                                # numpy conversion
                                if price == PRICE_TIME:                                                                
                                    historic.DATAROWS[timeframe][symbol][price] = DATAROW[timeframe][symbol][price]
                                else:
                                    historic.DATAROWS[timeframe][symbol][price] = np.array(DATAROW[timeframe][symbol][price], dtype=np.double)
                                    
                                # set current price 
                                self.current[timeframe][symbol][price] = 0       
                            
                            new_candle_added = True
                            log.info('NEW CANDLE: %s_%s %s ASK: %.5f %.5f %.5f %.5f  BID: %.5f %.5f %.5f %.5f  VOL: %d, VALUE: %.7f' % (
                                symbol, TIMEFRAME[timeframe],                      
                                DATAROW[timeframe][symbol][PRICE_TIME][-1], 
                                DATAROW[timeframe][symbol][PRICE_ASKOPEN][-1], 
                                DATAROW[timeframe][symbol][PRICE_ASKHIGH][-1], 
                                DATAROW[timeframe][symbol][PRICE_ASKLOW][-1],
                                DATAROW[timeframe][symbol][PRICE_ASKCLOSE][-1],
                                DATAROW[timeframe][symbol][PRICE_BIDOPEN][-1], 
                                DATAROW[timeframe][symbol][PRICE_BIDHIGH][-1], 
                                DATAROW[timeframe][symbol][PRICE_BIDLOW][-1],
                                DATAROW[timeframe][symbol][PRICE_BIDCLOSE][-1],
                                DATAROW[timeframe][symbol][PRICE_VOLUME][-1],
                                DATAROW[timeframe][symbol][PRICE_PIPVALUE][-1]
                                ))
                            
                            log.info('DATAROW LENGTH: %d' % len(historic.DATAROWS[timeframe][symbol][PRICE_TIME]))
                            log.info('CANDLES: %s' % str(historic.DATAROWS[timeframe][symbol][PRICE_TIME][-10:]))
                            
                        self.current[timeframe][symbol][PRICE_TIME] = current_time 
                            
                       
                if gapfill_symbols:
                    log.info('starting gapfill')
                    result = self.gapfill(gapfill_symbols, timeframe, gapfill_from, gapfill_to)
                    if not result:
                        log.info('gapfill failed, stopping DatarowBuilder')
                        self.stop = True
                    else:
                        log.info('gapfill finished: %s ' % result)                    
                        new_candle_added = True    
                
            return new_candle_added                                                   
        
                    
                    
#    def integrity_check(self, timeframe):
#        global DATAROW
#        if timeframe not in DATAROW:
#            return False
#        try:        
#            result = True
#            last_dates = set()
#            for symbol in DATAROW[timeframe]:            
#                last_dates.add(DATAROW[timeframe][symbol][PRICE_TIME][-1])             
#            
#            last_dates = list(last_dates)
#            if len(last_dates) > 1:
#                log.warn('Datarows differ! Last candle times: %s' % last_dates)
#                log.warn('Removing candles...')
#                for d in sorted(last_dates[1:]):
#                    for symbol in DATAROW[timeframe]:                        
#                        try:
#                            position = DATAROW[timeframe][symbol][PRICE_TIME].index(d)
#                            for price in DATAROW[timeframe][symbol]:
#                                del DATAROW[timeframe][symbol][price][position]
#                            
#                            log.info('removed %s from %s' % (d, symbol))    
#                        except ValueError:                        
#                            pass
#                        except:
#                            log.error(traceback.format_exc())                        
#                
#                last_dates = set()
#                for symbol in DATAROW[timeframe]:            
#                    last_dates.add(DATAROW[timeframe][symbol][PRICE_TIME][-1])
#                last_dates = list(last_dates)
#                
#                log.warn('Last dates: %s' % last_dates)
#        except:
#            log.error(traceback.format_exc())
#            
#        return True
#                                                
                        

class DukascopyDatafeedReader:
    
    def __init__(self, feed_queue):
        #print 'INIT ! ', config.FEED_URL, config.FEED_PORT, config.FEED_SENDERCOMPID, config.FEED_TARGETCOMPID
        self.engine = FIXInitiator(config.FEED_URL, config.FEED_PORT, config.FEED_SENDERCOMPID, config.FEED_TARGETCOMPID, self.message_handler)            
        self.feed_queue = feed_queue
        self.subscribed_symbols = []
        self.tick_received_for = {}
        self.subscription_time = {}
        self.disconnected = True
        
    def connect(self):        
        if self.engine.connect():                        
            self.engine.connection_retries = -1
            self.engine.callbacks['on_logon'] = self.on_logon
            self.engine.callbacks['on_logout'] = self.on_logout
            self.engine.callbacks['on_connect'] = self.on_connect
            self.engine.callbacks['on_disconnect'] = self.on_disconnect
            
            self.engine.start()
            time.sleep(1)                                    
            if self.engine.login(config.FEED_USERNAME, config.FEED_PASSWORD):                       
                return True                     
            else:
                log.error('Logon failed to feed server !')
                return False
        else:
            log.error('Unable to connect to feed server !')
            return False
                            
    def shutdown(self):
        log.info('shutting down DatafeedReader!')
        self.engine.shutdown()
        
                            
    def on_logon(self):        
        self.disconnected = False
        log.info('Logon handler')      
        subscribed_symbols = self.subscribed_symbols[:]        
        self.subscribed_symbols = []
        self.tick_received_for = {}   
        for symbol in subscribed_symbols:
            self.subscribe(symbol)


    def on_logout(self):
        log.warn('Datafeed reader disconnected')
        self.disconnected = True        
    
    def on_connect(self):
        pass        
    
    def on_disconnect(self):
        log.warn('Datafeed reader disconnected')
        self.disconnected = True        
    
    
    def subscribe(self, symbol):
        if symbol in self.subscribed_symbols:
            return 
        
        self.subscribed_symbols.append(symbol)
        self.tick_received_for[symbol] = False
        self.subscription_time[symbol] = datetime.utcnow()
        symbol = symbol[:3] + '/' + symbol[3:]        
        log.info('Subscribing to %s' % symbol)
        if not self.disconnected:
            self.engine.send(fix.MarketDataRequest(MDReqID='GDT', 
                                               SubscriptionRequestType = fix.SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES,
                                               MarketDepth = 1,
                                               MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                               NoMDEntryTypes = [
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                                 ],
                                               NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol=symbol)]
                                               ))
    
    
    def unsubscribe(self, symbol):
        if symbol not in self.subscribed_symbols:
            return
        
        self.subscribed_symbols.remove(symbol)
        
        symbol = symbol[:3] + '/' + symbol[3:]        
        log.info('Unsubscribing from %s' % symbol)
        if not self.disconnected:
            self.engine.send(fix.MarketDataRequest(MDReqID='GDT', 
                                               SubscriptionRequestType = fix.SubscriptionRequestType.DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST,
                                               MarketDepth = 1,
                                               MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                               NoMDEntryTypes = [
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                                 ],
                                               NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol=symbol)]
                                               ))
        
    def check_subscription(self):
        try:
            for symbol, received in self.tick_received_for.iteritems():
                if not received and self.subscription_time[symbol] + timedelta(seconds=20) < datetime.utcnow():
                    log.info('no tick received for %s, resubscribing to it' % symbol)
                    self.subscribed_symbols.remove(symbol)
                    self.subscribe(symbol)
        except:
            log.error(traceback.format_exc())
    
    
    def message_handler(self, message):
        global TRADABLE
        if type(message) == fix.QuoteStatusReport:
            symbol = message.Symbol[:3] + message.Symbol[4:]
            TRADABLE[symbol] = True if message.QuoteType == fix.QuoteType.TRADEABLE else False                        
        
                                                                                            
        if type(message) == fix.MarketDataSnapshotFullRefresh:
            try:
                symbol = message.Symbol[:3]+message.Symbol[4:]  
                self.tick_received_for[symbol] = True              
                self.feed_queue.put((symbol, message.NoMDEntries[0].MDEntryPx, message.NoMDEntries[0].MDEntrySize, message.NoMDEntries[1].MDEntryPx, message.NoMDEntries[1].MDEntrySize))
            except:
                log.error(traceback.format_exc())
            
        
        


class DatafeedServer(Thread):
        
    def __init__(self, manager):
        Thread.__init__(self)
        log.info('DatafeedServer initializing')
        self.feedQ = Queue()
        self.reader = DukascopyDatafeedReader(self.feedQ)                
        self.datarows = DatarowBuilder()    
        self.current_day = None
        self.manager = manager
        self.unevaluated_candles = []
    
    
    def run(self):    
        log.info('DatafeedServer started')
        if self.reader.connect():
            previous = datetime.utcnow()
            while True:
                current = datetime.utcnow()
                                                                                                                
                if current.minute != previous.minute: 
                    
                    self.reader.check_subscription()
                    
                    try:                                            
                        Thread(target=self.on_new_minute, args=(datetime(current.year, current.month, current.day, current.hour, current.minute, 0, tzinfo=None),)).start()
                    except:
                        log.error(traceback.format_exc())

                    if self.datarows.stop :
                        log.info('DatarowBuilder stopped, shutting down DatafeedServer')
                        self.reader.shutdown()
                        break
                                                                    
                elif current.second > previous.second + 10:      
                    try:
                        log.debug('--ontick--')          
                        Thread(target=self.manager.on_tick, args=())                                                                
                    except:
                        log.error(traceback.format_exc())                    
                    
                previous = current
                
                time.sleep(0.01)    # 0.01-nel a CPU usage 1-2%, 0.001: 4-5%, printekkel , print nelkul 2% !
                
                if self.reader.disconnected:
                    self.datarows.on_disconnect()
                    
                if self.datarows.disconnect_time and not self.reader.disconnected:
                    self.datarows.on_connect()
                    
                try:                
                    while True:                                                
                        data = self.feedQ.get_nowait()
                        log.debug('TICK: %s' % str(data))
                        self.datarows.add_tick(data[0], data[1], data[2], data[3], data[4])                                                                    
                        self.feedQ.task_done()                                                             
                                                     
                except Empty:                
                    pass
                except:
                    log.error(traceback.format_exc())
        else:
            log.error('Cant connect to feed server, exiting..., ')         

        log.info('DatafeedServer exited')
        
        
    def on_new_minute(self, current):
        """
        
        current: datetime, ami NEM TARTALMAZHAT MÁSODPERC ÉRTÉKEKET !
            a másodperc gyakran nem :00 lesz, igy 
        """
        current = current.replace(second=0, microsecond=0)
        log.info(' ---- %s -----' % current)                
        
        has_new_candle = self.datarows.new_candle(current)
                                                                        
        if has_new_candle:       
            log.info('New candles received, evaluating')
            for timeframe in sorted(TIMEFRAME.keys()):                  
                if calendar.timegm(current.utctimetuple()) % (timeframe * 60) == 0 and timeframe in self.datarows.strategy_counter:                                                    
                    self.manager.evaluate(timeframe)
        
            self.save_datarows(current)
        else:
            log.info('No new candles, skip evaluating')
            if self.reader.disconnected:
                log.warn('network disconnected, registering candle for close processing')
                self.unevaluated_candles.append(current)
                return

                   
        if not self.current_day:
            self.current_day = current.day
                                 
        if self.current_day != current.day:
            self.current_day = current.day            
            self.manager.on_new_day()
            

    def save_datarows(self, current):
        try:
            path = '/opt/datarows/%s' % current.strftime('%Y%m%d_%H%M')
            if not os.path.exists(path):
                os.makedirs(path)
            for tf in historic.DATAROWS:
                for symbol in historic.DATAROWS[tf]:
                    for price in historic.DATAROWS[tf][symbol]:
                        if price != PRICE_TIME:
                            d = historic.DATAROWS[tf][symbol][price][-1 * config.DUMP_LENGTH:]
                            d.dump(path + os.sep + '%s_%s_%d.npy' % (symbol, TIMEFRAME[tf], price))
                        else:
                            with open(path + os.sep + '%s_%s_%d.dates' % (symbol, TIMEFRAME[tf], price),'wb+') as f:
                                cPickle.dump(historic.DATAROWS[tf][symbol][price][-1 * config.DUMP_LENGTH:], f, -1)
        except:
            log.warn('Could not save current historic data!')
            log.warn(traceback.format_exc())       

                                                              
    def register_instrument(self, symbol, timeframe):
        log.info('Registering instrument %s_%s' % (symbol, TIMEFRAME[timeframe]))        
        self.datarows.register(symbol, timeframe)                                                       
        self.reader.subscribe(symbol)
        
        if 'USD' not in symbol:
            if symbol[:3]+'USD' in PIP_MULTIPLIER:
                self.datarows.register(symbol[:3] + 'USD', timeframe)                
                self.reader.subscribe(symbol[:3] + 'USD')
            elif 'USD'+symbol[:3] in PIP_MULTIPLIER:
                self.datarows.register('USD' + symbol[:3], timeframe)                
                self.reader.subscribe('USD' + symbol[:3])
                
        return True

    
    def unregister_instrument(self, symbol, timeframe):
        self.datarows.unregister(symbol, timeframe)
        if not self.datarows.has_consumer(symbol, timeframe):
            self.reader.unsubscribe(symbol)
            self.datarows.remove(symbol, timeframe)            
            
            
    def preload(self, timeframe):
        global DATAROW
        symbols = []        
        for symbol in DATAROW[timeframe]:            
            if len(DATAROW[timeframe][symbol][PRICE_TIME]) == 0:                        
                symbols.append(symbol)
        
        for symbol in symbols:
            self.datarows.preload(symbol, timeframe)
        
        if symbols:
            self.datarows.calc_pipvalue(timeframe)                
                            
        

