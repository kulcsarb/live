# -*- encoding: utf-8 -*- 
'''
Created on Jun 25, 2012

@author: gdtlive
'''
from gdtlive.constants import TIMEFRAME, PIP_MULTIPLIER, PRICE_PIPVALUE, PRICE_TIME, PRICE_ASKOPEN, PRICE_ASKHIGH, PRICE_ASKLOW, PRICE_ASKCLOSE, PRICE_BIDOPEN, PRICE_BIDHIGH, PRICE_BIDLOW, PRICE_BIDCLOSE, PRICE_VOLUME
from gdtlive.core.constants import *
from gdtlive.core.fix.initiator import FIXInitiator
import gdtlive.store.db as db
import gdtlive.config as config
import gdtlive.core.fix.dukascopy.fix as fix
import historic as historic
import gdtlive.historic.datarow as h_datarow
from jforex import JForexLoader

from gdtlive.utils import is_weekend
 
from datetime import datetime, timedelta
from threading import Thread, Lock
import threading
from Queue import Queue, Empty
import time
import calendar
import random
import traceback
import copy
import logging
import subprocess
import StringIO
import numpy as np
import ast 
import os
import cPickle
from datetime import date


LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"

log = logging.getLogger('gdtlive.core.feed')

#handler = logging.FileHandler('feed-error.log')
#handler.setFormatter(logging.Formatter(LOG_FORMAT))
#handler.setLevel(logging.ERROR)
#log.addHandler(handler)
#
#handler = logging.FileHandler('feed-info.log')
#handler.setFormatter(logging.Formatter(LOG_FORMAT))
#handler.setLevel(logging.INFO)
#log.addHandler(handler)
#
#handler = logging.FileHandler('feed-debug.log')
#handler.setFormatter(logging.Formatter(LOG_FORMAT))
#handler.setLevel(logging.DEBUG)
#log.addHandler(handler)
 

#DATAROW = {}

#TRADABLE = {}

        

class DatarowBuilder:
    
    def __init__(self):                        
        historic.CURRENT_PRICE = {}
        historic.DATAROW = {}
        self.current = {}                                                
        self.strategy_counter = {}
        self.lock = Lock()
        self.disconnect_time = None
        self.datarows_cleaned = False
        self.stop = False        
            
    
    def register(self, symbol, timeframe):                
        with self.lock:
                        
            self.current.setdefault(timeframe, {})                            
            historic.DATAROW.setdefault(timeframe, {})        
            historic.DATAROWS.setdefault(timeframe, {})
            self.strategy_counter.setdefault(timeframe, {})
                                    
            historic.CURRENT_PRICE.setdefault(symbol, {'ASK':0, 'BID':0, 'PIPVALUE':0.0001})
                                   
            self.strategy_counter[timeframe].setdefault(symbol, 0)
            historic.DATAROW[timeframe].setdefault(symbol, {})
            historic.DATAROWS[timeframe].setdefault(symbol, {})
            
            if symbol not in self.current[timeframe]:                            
                self.current[timeframe][symbol] = {}
                for price in xrange(PRICE_TIME, PRICE_PIPVALUE+1):                    
                    self.current[timeframe][symbol][price] = 0
                    historic.DATAROW[timeframe][symbol][price] = []                    
                    # numpy conversion
                    if price == PRICE_TIME:
                        historic.DATAROWS[timeframe][symbol][price] = []
                    else:
                        historic.DATAROWS[timeframe][symbol][price] = np.array(historic.DATAROW[timeframe][symbol][price], dtype=np.double)
                        
                self.current[timeframe][symbol][PRICE_TIME] = None      
            
            else:
                self.strategy_counter[timeframe][symbol] += 1
                        
        
        
                        
    def unregister(self, symbol, timeframe):
        try:
            self.strategy_counter[timeframe][symbol] -= 1                        
        except:
            pass
        
        
    def has_consumer(self, symbol, timeframe):
        try:            
            return bool(self.strategy_counter[timeframe][symbol])
        except:
            return False
                
    
    def remove(self, symbol, timeframe):
        with self.lock:     
            try:       
                del self.current[timeframe][symbol]
            except:
                pass
            try:
                del historic.DATAROW[timeframe][symbol]
            except:
                pass
            try:
                del historic.DATAROWS[timeframe][symbol]
            except:
                pass
            try:
                del self.strategy_counter[timeframe][symbol]
                if len(self.strategy_counter[timeframe]) == 0:
                    del self.strategy_counter[timeframe]
            except:
                pass
            log.info('datarow removed: %s_%s' % (symbol, TIMEFRAME[timeframe]))
        
    
    def preload(self, symbol, timeframe):             
        session = db.Session()
        try:            
            datarow_id = session.query(db.DatarowDescriptor.id).filter(db.DatarowDescriptor.symbol==symbol).filter(db.DatarowDescriptor.timeframe_num == timeframe).first()[0]
            log.info('Loading datarow id %d, %s, %s' % (datarow_id, symbol, TIMEFRAME[timeframe]))
            
            result = h_datarow.load(datarow_id, date(2012,12,1), date.today())
            
            log.info('Loaded from %s to %s' % (result[PRICE_TIME][0], result[PRICE_TIME][-1]))
            
            historic.DATAROW[timeframe][symbol] = result
            historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE] = [0.0001] * len(historic.DATAROW[timeframe][symbol][PRICE_TIME])             
            
            from_time = historic.DATAROW[timeframe][symbol][PRICE_TIME][-1] + timedelta(minutes = timeframe)
            sec = calendar.timegm(datetime.utcnow().utctimetuple())
            sec = ((sec / (timeframe*60)) * (timeframe*60)) - (timeframe * 60)
            to_time = datetime.utcfromtimestamp(sec)
            if from_time <= to_time:
                loader = JForexLoader([symbol], timeframe, from_time, to_time)        
                result = loader.run()
            
        except:
            log.error('Failed to load historic data from local store for %s' % symbol )
            log.error(traceback.format_exc())
        finally:
            session.close()


    def export_datarow(self, symbol, timeframe):        
        for price in historic.DATAROW[timeframe][symbol]:
            if price == PRICE_TIME:                                                                
                historic.DATAROWS[timeframe][symbol][price] = historic.DATAROW[timeframe][symbol][price]
            else:
                historic.DATAROWS[timeframe][symbol][price] = np.array(historic.DATAROW[timeframe][symbol][price], dtype=np.double)
        
        historic.CURRENT_PRICE[symbol]['ASK'] = historic.DATAROW[timeframe][symbol][PRICE_ASKCLOSE][-1]
        historic.CURRENT_PRICE[symbol]['BID'] = historic.DATAROW[timeframe][symbol][PRICE_BIDCLOSE][-1]
        historic.CURRENT_TIME = datetime.utcnow()     
        
        

    def calc_pipvalue(self, timeframe):
        '''Kiszámolja az összes betöltött adatsor pip értékét'''        
        log.info('Calculating pipvalues')
        try:
            for symbol in historic.DATAROW[timeframe]:
                self._calc_pipvalue(timeframe, symbol)
        except:
            log.error('Failed to compute pipvalue for historical data')
            log.error(traceback.format_exc())
            
    def _calc_pipvalue(self, timeframe, symbol):
        
        if PRICE_PIPVALUE not in historic.DATAROW[timeframe][symbol]:
            historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE] = []
            
            if symbol[3:] == 'USD':
                self._pipvalue_calc_case1(timeframe, symbol)
            elif symbol[:3] == 'USD':
                self._pipvalue_calc_case2(timeframe, symbol)
            else:
                self._pipvalue_calc_case3(timeframe, symbol)
        
        
    def _pipvalue_calc_case1(self, timeframe, symbol):
        
        
        '''Kiszámolja az USD-re végződő instrumentumok pip értékét minden gyertyára nézve.
        
        Az eredményt a `candles`[symbol][PRICE_PIPVALUE] tömbben tárolja
                
        
        :Parameters:
            symbol : str
                Az instrumentum azonosítója, ahol instrumentum = "*USD"  (AUDUSD, EURUSD, GBPUSD, ....)
        
        Algoritmus 
        ==========
        
        Determine your account’s currency (in about 90% cases it’s USD). If it’s the same currency 
        as the based currency of the pair (the second one, which goes after “/”) then you should 
        simply multiply the amount of currency units in your position (100,000 for 1 standard lot) 
        by the size of one pip. You’ll get a pip value of 10 currency units per 1 standard lot. 
        
        '''
        datarow_length = len(historic.DATAROW[timeframe][symbol][PRICE_TIME])
        historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE] = [PIP_MULTIPLIER[symbol] for i in range(datarow_length)]        



    def _pipvalue_calc_case2(self, timeframe, symbol):
        
        '''Kiszámolja az USD-vel kezdődő instrumentumok pip értékét minden gyertyára nézve.
        
        Az eredményt a `candles`[symbol][PRICE_PIPVALUE] tömbben tárolja
                
        
        :Parameters:
            symbol : str
                Az instrumentum azonosítója, ahol instrumentum = "USD*"  (USDCAD, USDZAR, ....)
        
        Algoritmus 
        ==========
        
        If the account’s currency is different from the base currency but is the same as the currency 
        pair’s long currency (the first one, which goes before “/”) then check this currency pair’s 
        current Ask rate (the highest of the rates).
        You should multiply the amount of currency units in your position (100,000 for 1 standard lot) 
        by the size of one pip and then divide the result by the Ask rate 
        '''        
        datarow_length = len(historic.DATAROW[timeframe][symbol][PRICE_TIME])
        historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE] = [0] * datarow_length        
        for i in xrange(len(historic.DATAROW[timeframe][symbol][PRICE_TIME])):
            value = 1 * PIP_MULTIPLIER[symbol] / historic.DATAROW[timeframe][symbol][PRICE_ASKCLOSE][i]
            historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE][i] = value


    def _pipvalue_calc_case3(self, timeframe, symbol):
        
        '''Kiszámolja az adott keresztárfolyam pip értékét minden gyertyára nézve.

        Az eredményt a `candles`[symbol][PRICE_PIPVALUE] tömbben tárolja

        :Parameters:
            symbol : str
                Az instrumentum azonosítója, ahol az instrumentumban nem szerepel az USD  (CADJPY, AUDNZD, ....)

        Algoritmus
        ==========

        If the account’s currency is different from any of the currencies from the pair of the position,
        you have to check the current rate of this currency relative to the base currency of the pair:
            3.A:
                If the currency pair combined of the account’s currency and the base currency of the position
                has the account’s currency as the base currency (second, after “/”) then you should check
                and remember its current Bid rate (the lowest of the rates).
            3.B
                If the currency pair combined of the account’s currency and the base currency of the position
                has the account’s currency as the long currency (first, before “/”) then you should check
                and remember its current Ask rate (the highest of the rates).

        You should multiply the amount of currency units in your position (100,000 for 1 standard lot) by
        the size of one pip and either multiply the result by the Bid rate received in step 3a or divide
        the result by the Ask rate received in step 3b.

        :TODO: unittest
        '''
        
        symbol_3B = 'USD' + symbol[:3]   # CASE 3.B : account currency as long 
        symbol_3A = symbol[:3] + 'USD'    # CASE 3.A : account currency as short    
        case = ''
        if symbol_3B in historic.DATAROW[timeframe]:
            case = '3B'            
        elif symbol_3A in historic.DATAROW[timeframe]: 
            case = '3A'            

        datarow_length = len(historic.DATAROW[timeframe][symbol][PRICE_TIME])
        historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE] = [0] * datarow_length        
        
        if case == '3A':
            for i in xrange(len(historic.DATAROW[timeframe][symbol][PRICE_TIME])):                                      
                historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE][i] = PIP_MULTIPLIER[symbol_3A] * historic.DATAROW[timeframe][symbol_3A][PRICE_ASKCLOSE][i]

        elif case == '3B':
            for i in xrange(len(historic.DATAROW[timeframe][symbol][PRICE_TIME])):                                 
                historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE][i] = PIP_MULTIPLIER[symbol_3B] / historic.DATAROW[timeframe][symbol_3B][PRICE_ASKCLOSE][i]

        else:
            # ha nincs meg az árfolyam, 1-nek vesszük.... 
            for i in xrange(len(historic.DATAROW[timeframe][symbol][PRICE_TIME])):
                value = 0.0001
                historic.DATAROW[timeframe][symbol][PRICE_PIPVALUE][i] = value                


    
    
    def load_jforex(self, symbols, timeframe, from_date, to_date):
        
        loader = JForexLoader(symbols, timeframe, from_date, to_date)        
        result = loader.run()
        #result = load_candles(symbol, timeframe, gapfill_from, gapfill_to)                            
        self.calc_pipvalue(timeframe)
        return result                                       

                                                                                                
    def get_pipvalue(self, symbol):
        pipvalue = 0.0001
        try:
            if symbol.endswith('USD'):
                pipvalue = PIP_MULTIPLIER[symbol]
            elif symbol.startswith('USD'):
                pipvalue = PIP_MULTIPLIER[symbol] / historic.CURRENT_PRICE[symbol]['ASK']
            else:
                symbol_3B = 'USD' + symbol[:3]   # CASE 3.B : account currency as long 
                symbol_3A = symbol[:3] + 'USD'    # CASE 3.A : account currency as short                
                if symbol_3A in historic.CURRENT_PRICE:
                    pipvalue = PIP_MULTIPLIER[symbol_3A] * historic.CURRENT_PRICE[symbol_3A]['ASK'] 
                elif symbol_3B in historic.CURRENT_PRICE:
                    pipvalue = PIP_MULTIPLIER[symbol_3B] /  historic.CURRENT_PRICE[symbol_3B]['ASK']
                else:
                    pipvalue = 0.0001
        except:
            pass
            
        return pipvalue

    
            
    
    def new_candle(self, current_time):        
        with self.lock:
            if self.stop:
                log.info('Datarow builder stopped, skip new_candle')
                return False
                        
            new_candle_added = False
            
            # DO NOT REMOVE IT, OR I WILL KILL YOU, MOTHERFUCKER!
            current_time = current_time.replace(second=0, microsecond=0)
            
            historic.CURRENT_TIME = current_time                                           
                                
            for timeframe in sorted(self.current.keys()):                
                gapfill_symbols = []                
                gapfill_from = None
                gapfill_to = None
                for symbol in sorted(self.current[timeframe]):
                    
                    if calendar.timegm(current_time.utctimetuple()) % (timeframe*60) == 0 :
                        
                        if len(historic.DATAROW[timeframe][symbol][PRICE_TIME]):
                            sym_from = historic.DATAROW[timeframe][symbol][PRICE_TIME][-1] + timedelta(minutes = timeframe)
                            sym_to = current_time - timedelta(minutes = timeframe)
                            if (not gapfill_from or gapfill_from > sym_from):
                                gapfill_from = sym_from
                            if (not gapfill_to or gapfill_to < sym_to):
                                gapfill_to = sym_to                                                                

                            gapfill_symbols.append(symbol)

                                                                           
                                                                           
                if gapfill_symbols:
                    log.info('starting jforex loader')
                    result = self.load_jforex(gapfill_symbols, timeframe, gapfill_from, gapfill_to)
                    if not result:
                        log.info('loader failed!')
                        #self.stop = True
                    else:
                        log.info('loader finished: %s ' % result)                    
                        new_candle_added = True    
                
            return new_candle_added                                                   
        
                    

class DukascopyDatafeedReader:
    
    def __init__(self, feed_queue):                    
        self.feed_queue = feed_queue
        self.subscribed_symbols = []
        self.last_tick_time = {}
        self.subscription_time = {}
        self.disconnected = True
        
    def connect(self):
        self.engine = FIXInitiator(config.FEED_URL, config.FEED_PORT, config.FEED_SENDERCOMPID, config.FEED_TARGETCOMPID, self.message_handler, use_ssl=config.FEED_SSL)
                
        if self.engine.connect():                        
            self.engine.connection_retries = -1
            self.engine.callbacks['on_logon'] = self.on_logon
            self.engine.callbacks['on_logout'] = self.on_logout
            self.engine.callbacks['on_connect'] = self.on_connect
            self.engine.callbacks['on_disconnect'] = self.on_disconnect
            
            self.engine.start()
            time.sleep(1)                                    
            if self.engine.login(config.FEED_USERNAME, config.FEED_PASSWORD):                       
                return True                     
            else:
                log.error('Logon failed to feed server !')
                return False
        else:
            log.error('Unable to connect to feed server !')
            return False
                            
    def shutdown(self):
        log.info('shutting down DatafeedReader!')
        self.engine.shutdown()
        
                            
    def on_logon(self):        
        self.disconnected = False
        log.info('Logon handler')      
        subscribed_symbols = self.subscribed_symbols[:]        
        self.subscribed_symbols = []
        self.last_tick_time = {}   
        for symbol in subscribed_symbols:
            self.subscribe(symbol)


    def on_logout(self):
        log.warn('Datafeed reader disconnected')
        self.disconnected = True        
    
    def on_connect(self):
        pass        
    
    def on_disconnect(self):
        log.warn('Datafeed reader disconnected')
        self.disconnected = True        
    
    
    def subscribe(self, symbol):
        if symbol in self.subscribed_symbols:
            return 
        
        self.subscribed_symbols.append(symbol)
        self.last_tick_time[symbol] = None
        self.subscription_time[symbol] = datetime.utcnow()
        symbol = symbol[:3] + '/' + symbol[3:]        
        log.info('Subscribing to %s' % symbol)
        if not self.disconnected:
            self.engine.send(fix.MarketDataRequest(MDReqID='GDT', 
                                               SubscriptionRequestType = fix.SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES,
                                               MarketDepth = 1,
                                               MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                               NoMDEntryTypes = [
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                                 ],
                                               NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol=symbol)]
                                               ))
    
    
    def unsubscribe(self, symbol):
        if symbol not in self.subscribed_symbols:
            return
        
        self.subscribed_symbols.remove(symbol)
        
        symbol = symbol[:3] + '/' + symbol[3:]        
        log.info('Unsubscribing from %s' % symbol)
        if not self.disconnected:
            self.engine.send(fix.MarketDataRequest(MDReqID='GDT', 
                                               SubscriptionRequestType = fix.SubscriptionRequestType.DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST,
                                               MarketDepth = 1,
                                               MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                               NoMDEntryTypes = [
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                                 ],
                                               NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol=symbol)]
                                               ))
        
                
    def check_connection(self):
        try:
            restart = False
            
            if self.engine.connection_state == STATE_SESSION_ESTABLISHED:
                for symbol, received in self.last_tick_time.iteritems():                
                    if (not received and self.subscription_time[symbol] + timedelta(seconds=120) < datetime.utcnow()) \
                        or (received and received + timedelta(seconds=120) < datetime.utcnow()):
                        log.warning('no tick received for %s, restarting engine' % symbol)
                        restart = True
            
            elif self.engine.connection_state == STATE_SHUTDOWN:
                restart = True     

            else:
                self.engine.check_connection()
                                    
            if restart:
                self.shutdown()
                self.connect()
                
        except:
            log.error(traceback.format_exc())
    
    
    def message_handler(self, message):
        global TRADABLE                                                                                                    
        if type(message) == fix.MarketDataSnapshotFullRefresh:
            try:
                symbol = message.Symbol[:3]+message.Symbol[4:]  
                self.last_tick_time[symbol] = datetime.utcnow()             
                self.feed_queue.put((symbol, message.NoMDEntries[0].MDEntryPx, message.NoMDEntries[0].MDEntrySize, message.NoMDEntries[1].MDEntryPx, message.NoMDEntries[1].MDEntrySize))
                
#                data = self.feedQ.get_nowait()
#                log.debug('TICK: %s' % str(data))                             
#                ask_price = message.NoMDEntries[0].MDEntryPx                
#                bid_price = message.NoMDEntries[1].MDEntryPx                 
#                if symbol in historic.CURRENT_PRICE:
#                    historic.CURRENT_PRICE[symbol]['ASK'] = ask_price
#                    historic.CURRENT_PRICE[symbol]['BID'] = bid_price
#                    historic.CURRENT_TIME = datetime.utcnow()
                                                                                                                                                                                   
            except:
                log.error(traceback.format_exc())
            
        

                        
        


class DatafeedServer(Thread):
        
    def __init__(self, manager):
        Thread.__init__(self)
        log.info('DatafeedServer initializing')
        self.feedQ = Queue()
        self.reader = DukascopyDatafeedReader(self.feedQ)                    
        self.datarows = DatarowBuilder()    
        self.current_day = None
        self.manager = manager
        self.unevaluated_candles = []
        self.delta = 30
        self.lock = Lock()
        self.on_tick_thread = None
    
    def run(self):    
        log.info('DatafeedServer started')
        self.reader.connect()
        
        previous = datetime.utcnow()
        
        while True:
            time.sleep(0.01)    # 0.01-nel a CPU usage 1-2%, 0.001: 4-5%, printekkel , print nelkul 2% !            
            current = datetime.utcnow()
            weekend_status = is_weekend(current)                        
            current += timedelta(seconds=self.delta)                          
                            
            if is_weekend(current):
                if weekend_status == False:
                    log.info('on_weekend_start !')                    

                if current.hour != previous.hour:
                    log.info('-- WEEKEND PING -- ')
                    previous = current
                    previous += timedelta(minutes=1) #a hetvegek miatt, igy az alabbi if lefut                                         
            else:                                                                                        
                if current.minute != previous.minute:                                                 
                    try:
                        #log.info('active threads: %d' % threading.active_count())
#                        at = []
#                        for t in threading.enumerate():
#                            if 'HTTPServer' not in t.name and 'CP Server' not in t.name:
#                                at.append(t)                        
#                        log.info('threads: %s' % at)                                             
                        current_time = datetime(current.year, current.month, current.day, current.hour, current.minute, 0, tzinfo=None)
                        new_minute_thread = Thread(target=self.on_new_minute, args=(current_time,), name='on_new_minute %s' % current_time)
                        new_minute_thread.start()
                                            
                    except:
                        log.error(traceback.format_exc())
                                                                                    
                    previous = current
                                    
                    self.reader.check_connection()
                elif current.second > previous.second + 10:
                    if self.lock.acquire(False):                        
                        self.manager.on_tick()
                        self.lock.release()
                    else:
                        log.info('-- ontick lock failed --')
                    previous = current
#                try:
#                    if not self.on_tick_thread or not self.on_tick_thread.is_alive():                                                                        
#                        log.info('--ontick--')          
#                        self.on_tick_thread = Thread(target=self.manager.on_tick, args=(), name='ontick %s' % str(datetime.utcnow()))
#                        self.on_tick_thread.start()
#                    else:
#                        log.info('-- ontick lock failed --')                                                                                               
#                except:
#                    log.error(traceback.format_exc())
#                finally:                                
#                    pass                
                
                elif current.hour == 0 and current.minute == 5:                    
                    Thread(target=self.manager.on_new_day, args=()).start()                 
                else:                    
                    try:                
                        while True:                                                
                            data = self.feedQ.get_nowait()
                            log.debug('TICK: %s' % str(data))
                            
                            symbol, ask_price, ask_vol, bid_price, bid_vol = data
                            if symbol in historic.CURRENT_PRICE:
                                historic.CURRENT_PRICE[symbol]['ASK'] = ask_price
                                historic.CURRENT_PRICE[symbol]['BID'] = bid_price
                                historic.CURRENT_TIME = datetime.utcnow()
                                                                                                                            
                            self.feedQ.task_done()                                                             
                    except Empty:                
                        pass
                    except:
                        log.error(traceback.format_exc())
                                                                                                                                
            
        log.info('DatafeedServer exited')
        
        
    def on_new_minute(self, current):
        """
        
        current: datetime, ami NEM TARTALMAZHAT MÁSODPERC ÉRTÉKEKET !
            a másodperc gyakran nem :00 lesz, igy 
        """
        try:
            current = current.replace(second=0, microsecond=0)
            log.info(' ---- %s -----' % current)                
            
            has_new_candle = self.datarows.new_candle(current)
            
#            log.debug('entering lock')
#            with self.lock:                                
            if has_new_candle:       
                log.info('New candles received, evaluating')
                try:
                    self.lock.release()
                except:
                    pass
                self.lock.acquire()
                
                historic.CURRENT_CANDLE_TIME = current
                for timeframe in sorted(TIMEFRAME.keys()):                  
                    if calendar.timegm(current.utctimetuple()) % (timeframe * 60) == 0 and timeframe in self.datarows.strategy_counter:                                                                            
                        self.manager.evaluate(timeframe)
            
                #self.save_datarows(current)                
                self.lock.release()
            else:
                #log.info('No new candles, skip evaluating')
                
                if self.lock.acquire(False):
                    self.manager.on_tick()
                    
#                    if not self.on_tick_thread or not self.on_tick_thread.is_alive():                                                                        
#                        log.info('ontick ')          
#                        self.on_tick_thread = Thread(target=self.manager.on_tick, args=(), name='ontick %s' % str(datetime.utcnow()))
#                        self.on_tick_thread.start()
#                    else:
#                        log.info('ontick lock failed ')                                                                                               

                    self.lock.release()
            
            if current.isoweekday() == 5 and current.hour == 16 and current.minute == 1:                
                self.manager.on_weekend_start()
                
#            if not self.current_day:
#                self.current_day = current.day
#                                     
#            if self.current_day != current.day:
#                self.current_day = current.day            
#                self.manager.on_new_day()
        except:
            log.error(traceback.format_exc())

    
    def save_datarows(self, current):
        try:
            path = '/opt/datarows/%s' % current.strftime('%Y%m%d_%H%M')
            if not os.path.exists(path):
                os.makedirs(path)
            for tf in historic.DATAROWS:
                for symbol in historic.DATAROWS[tf]:
                    for price in historic.DATAROWS[tf][symbol]:
                        if price != PRICE_TIME:
                            d = historic.DATAROWS[tf][symbol][price][-1 * config.DUMP_LENGTH:]
                            d.dump(path + os.sep + '%s_%s_%d.npy' % (symbol, TIMEFRAME[tf], price))
                        else:
                            with open(path + os.sep + '%s_%s_%d.dates' % (symbol, TIMEFRAME[tf], price),'wb+') as f:
                                cPickle.dump(historic.DATAROWS[tf][symbol][price][-1 * config.DUMP_LENGTH:], f, -1)
        except:
            log.warn('Could not save current historic data!')
            log.warn(traceback.format_exc())       

                                                              
    def register_instrument(self, symbol, timeframe):
        log.info('Registering instrument %s_%s' % (symbol, TIMEFRAME[timeframe]))        
        self.datarows.register(symbol, timeframe)                                                               
        self.reader.subscribe(symbol)
        
        if 'USD' not in symbol:
            if symbol[:3]+'USD' in PIP_MULTIPLIER:
                self.datarows.register(symbol[:3] + 'USD', timeframe)
                self.reader.subscribe(symbol[:3] + 'USD')                
                
            elif 'USD'+symbol[:3] in PIP_MULTIPLIER:
                self.datarows.register('USD' + symbol[:3], timeframe)
                self.reader.subscribe('USD' + symbol[:3])                
                
                
        return True

    
    def unregister_instrument(self, symbol, timeframe):
        self.datarows.unregister(symbol, timeframe)
        if not self.datarows.has_consumer(symbol, timeframe):            
            self.datarows.remove(symbol, timeframe)            
            self.reader.unsubscribe(symbol)
            
            
    def preload(self, timeframe=None):
                
        if not timeframe:
            for timeframe in historic.DATAROW.keys():
                self.preload(timeframe)
        else:                
            self.preload_tf(timeframe)
    
                            
    def preload_tf(self, timeframe):
        symbols = []
        for symbol in historic.DATAROW[timeframe]:            
            if len(historic.DATAROW[timeframe][symbol][PRICE_TIME]) == 0:                        
                symbols.append(symbol)
    
        for symbol in symbols:
            self.datarows.preload(symbol, timeframe)
            self.datarows.export_datarow(symbol, timeframe)
    
        if symbols:
            self.datarows.calc_pipvalue(timeframe)

