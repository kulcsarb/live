'''
Created on Dec 4, 2012

@author: gdt
'''
from gdtlive.admin.system.log import initialize_loggers
import gdtlive.core.tradeengine
import time
import gdtlive.store.db as db
from threading import Thread 


def start():
    Thread(target=main, args=()).start()


def main():
    initialize_loggers()
    db.init()

    gdtlive.core.tradeengine.start()   
    while True:
        time.sleep(1)
        
        
if __name__ == '__main__':
    main()
else:
    start()