'''
Created on 2012.06.02.

@author: kulcsarb
'''
from gdtlive.store.db import PerformanceData, PerformanceStrategySerialData
from gdtlive.constants import PERFORMANCE_CURRENCY
import math


def standard_dev(data):
    avg = 0
    sum = 0
    diffFromAvg = 0
    i = 0
    length = len(data)
    if not length: return 0

    for i in xrange(length):
        avg += data[i]
    
    avg /= float(length)
    for i in xrange(length):
        diffFromAvg = data[i] - avg
        sum += diffFromAvg * diffFromAvg    
    sum /= float(length)
    return math.sqrt(sum)



class Performance:
    
    def __init__(self):
        self.account_balance = []
        self.drawdown = []
        self.trades_profit_loss = []
        self.daily_profit = []
        self.weekly_profit = []
        self.monthly_profit = []
        self.yearly_profit = []
        self.MAE = []
        self.MFE = []
        self.floating_profit = []
        self.use_of_leverage = []
        self.beta = []
                
        self._weekly_drawdown = []
        self._weekly_profitloss = []
        self._weekly_MAE = []
        self._weekly_MFE = []
        
        self.perf = PerformanceData(PERFORMANCE_CURRENCY, {})
        self.perf.clear()        
        self.serial = PerformanceStrategySerialData([], [], [], [], [], [], [], [], [], [], [], []) 
                                                             
        self._prev_candle_time = None
        self._max_balance = 0
        self._current_drawdown = 0
        self._account_balance = 0
        self._day_profit = 0
        self._day_floatmin = 0
        self._week_profit = 0
        self._month_profit = 0
        self._year_profit = 0
        self._months_num = 1
        self._days_num = 1
        self._winning_months = 0
        self._consec_loser = 0
        self._consec_winner = 0
        self._sum_time_in_market = None
        self._neg_floating_candle_num = 0
        self._pos_floating_candle_num = 0
        self._sum_neg_floating = 0
        self._sum_pos_floating = 0
        self._sum_neg_floating_p = 0
        self._sum_pos_floating_p = 0
        self._sum_use_of_leverage = 0
        self._candle_num = 0
        self._sum_MAE = 0
        self._sum_MFE = 0
        
    
    def get_performance(self):
        return self.perf
    
    def get_serial_performance(self):
        self.serial.update({
                            'net_profit' : self.account_balance,
                            'drawdown' : self.drawdown,
                            'trades_profit_loss' : self.trades_profit_loss,
                            'daily_profit' :  self.daily_profit,
                            'weekly_profit' : self.weekly_profit,
                            'monthly_profit' : self.monthly_profit,
                            'yearly_profit' : self.yearly_profit,
                            'MAE' : self.MAE,
                            'MFE' : self.MFE,
                            'floating_profit' : self.floating_profit,
                            'use_of_leverage' : self.use_of_leverage,
                            'beta' : []                             
                            })
        #print 'GET_SERIAL', self, self.monthly_profit, self.serial.monthly_profit
        return self.serial        
        
    def set_base_equity(self, equity):
        self._base_equity = equity
        self._account_balance = equity
        self._max_balance = equity
        
        
    def view(self):
        print 'SERIALS:'
        print 'Balance', self.account_balance
        print 'Drawdown', self.drawdown
        print 'Daily profit', self.daily_profit
        print 'Weekly profit', self.weekly_profit
        print 'Monthly profit', self.monthly_profit        
        print 'MAE', self.MAE
        print 'MFE', self.MFE
        print 'P/L', self.trades_profit_loss
        print 'Floating', self.floating_profit
        
        print 'PERFORMANCE'
        for k,v in self.perf.view().items():
            print k, v        
        
                
    def onNewDay(self, candle_time):           
        if not self._prev_candle_time:
            self._prev_candle_time = candle_time            
            
        if candle_time.day != self._prev_candle_time.day:                                    
            self.daily_profit.append(self._day_profit)            
            self.account_balance.append(self._account_balance)
            self.floating_profit.append(self._day_floatmin)  
            self._day_profit = 0
            self._day_floatmin = 0
            self._days_num += 1
            
        if candle_time.isoweekday() == 1:
            self.weekly_profit.append(self._week_profit)                        
            self.drawdown.append(self._weekly_drawdown)
            self.trades_profit_loss.append(self._weekly_profitloss)
            self.MAE.append(self._weekly_MAE)
            self.MFE.append(self._weekly_MFE)
            
            self._weekly_drawdown = []
            self._weekly_profitloss = []
            self._weekly_MAE = []
            self._weekly_MFE = []
            self._week_profit = 0
            
        if candle_time.month != self._prev_candle_time.month:            
            self.monthly_profit.append(self._month_profit)
            if self._month_profit > 0:
                self._winning_months += 1
            self._month_profit = 0
            self._months_num += 1            
            
        if candle_time.year != self._prev_candle_time.year:
            self.yearly_profit.append(self._year_profit)
            self._year_profit = 0
            #TODO: self.perf.min_yearly_profit_p
            
        self._prev_candle_time = candle_time
        
                
    def onStop(self, candle_time):
        #self._account_balance += self._day_profit
        self.account_balance.append(self._account_balance)
        self.daily_profit.append(self._day_profit)    
        self.weekly_profit.append(self._week_profit)                    
        self.monthly_profit.append(self._month_profit)
        self.yearly_profit.append(self._year_profit)
        
        
        self.drawdown.append(self._weekly_drawdown)
        self.trades_profit_loss.append(self._weekly_profitloss)
        self.MAE.append(self._weekly_MAE)
        self.MFE.append(self._weekly_MFE)
        
        #print 'ONSTOP', self, self.monthly_profit            
        if self._month_profit > 0:
            self._winning_months += 1

        if self._months_num-1 > 0:
            self.perf.chance1m = (float(self._winning_months) / ( self._months_num - 1)) * 100.0
        #TODO: chance3m
        
        self.perf.profit_variance = standard_dev(self.monthly_profit)         
        if self.perf.profit_variance:       
            self.perf.sharpe_ratio = self.perf.profit_per_month / self.perf.profit_variance                

        self.perf.open_trades = 0
        self.perf.floating_profit = 0
        
        #TODO: self.perf.min_yearly_profit_p
        
        
    def onCandleClose(self, floating_profit, use_of_leverage):        
        floating_percent = (floating_profit / (self._base_equity + self.perf.net_profit)) * 100.0
        if floating_profit > 0:
            self._sum_neg_floating += floating_profit
            self._sum_neg_floating_p += floating_percent
            self._neg_floating_candle_num += 1
        if floating_profit < 0:    
            self._sum_pos_floating += floating_profit
            self._sum_pos_floating_p += floating_percent
            self._pos_floating_candle_num += 1    

        self._candle_num += 1 
        self._sum_use_of_leverage += use_of_leverage
        
        if self._neg_floating_candle_num:
            self.perf.avg_neg_floating =  self._sum_neg_floating / self._neg_floating_candle_num
            self.perf.avg_neg_floating_p =  self._sum_neg_floating_p / self._neg_floating_candle_num
        if self._pos_floating_candle_num:
            self.perf.avg_pos_floating =  self._sum_pos_floating / self._pos_floating_candle_num
            self.perf.avg_pos_floating_p =  self._sum_pos_floating_p / self._pos_floating_candle_num
        if self._candle_num:
            self.perf.avg_use_of_lev = float(self._sum_use_of_leverage) / self._candle_num
        
        
    def onTick(self, floating_profit, use_of_leverage, open_trades):
        self.perf.open_trades = open_trades
        self.perf.floating_profit = floating_profit
        self.perf.max_use_of_lev = max(self.perf.max_use_of_lev, use_of_leverage)
        self.perf.min_floating = min(floating_profit, self.perf.min_floating)
        if self.perf.net_profit:
            self.perf.min_floating_p = min( (floating_profit / self._account_balance) * 100.0, self.perf.min_floating_p)
        self._day_floatmin = min(self._day_floatmin, floating_profit)
        
        #TODO: self.perf.avg_use_of_lev        
        
            
    def onTradeClose(self, trade):
        #print trade
        self._weekly_profitloss.append(trade.profit)
        self._weekly_MAE.append(trade.MAE)
        self._weekly_MFE.append(trade.MFE)
        self._sum_MAE += trade.MAE
        self._sum_MFE += trade.MFE
        
        self._account_balance += trade.profit
        self._max_balance = max(self._account_balance, self._max_balance)
        
        self.perf.number_of_trades += 1
        self.perf.net_profit += trade.profit
        if trade.profit > 0:
            self.perf.gross_profit += trade.profit
            self.perf.largest_winner = max(self.perf.largest_winner, trade.profit)
            self.perf.winning_trades += 1
            self.perf.max_conseq_losers = max(self.perf.max_conseq_losers, self._consec_loser)
            self._consec_loser = 0
            self._consec_winner += 1
        else:
            self.perf.max_conseq_winners = max(self.perf.max_conseq_winners, self._consec_winner)
            self.perf.largest_loser = min(self.perf.largest_loser, trade.profit)
            self._consec_loser += 1
            self._consec_winner = 0                        
            self.perf.gross_loss += trade.profit
            self.perf.losing_trades += 1
            
        if self.perf.gross_loss:
            self.perf.profit_factor = self.perf.gross_profit / self.perf.gross_loss 
            
        if self.perf.number_of_trades:
            self.perf.percent_profitable = (float(self.perf.winning_trades) / self.perf.number_of_trades) * 100.0
            self.perf.average_trade = self.perf.net_profit / self.perf.number_of_trades
            self.perf.avgMAE = self._sum_MAE / float(self.perf.number_of_trades)
            self.perf.avgMFE = self._sum_MFE / float(self.perf.number_of_trades)
        else:
            self.perf.avgMAE = 0.0
            self.perf.avgMFE = 0.0
            
        if self.perf.losing_trades:
            self.perf.average_loser = self.perf.gross_loss / float(self.perf.losing_trades)    
        if self.perf.winning_trades:
            self.perf.average_winner = self.perf.gross_profit / float(self.perf.winning_trades)        
        if self.perf.average_loser:
            self.perf.average_wl_ratio = -1 * self.perf.average_winner / self.perf.average_loser
        
        self.perf.profit_per_month = float(self.perf.net_profit) / self._months_num
            
        self._current_drawdown = -1 * (self._max_balance - self._account_balance)
        self._weekly_drawdown.append(self._current_drawdown)
        self.perf.maximum_drawdown = min(self._current_drawdown, self.perf.maximum_drawdown)
        if self._max_balance:                
            self.perf.max_drawdown_p = min( (self._current_drawdown / self._max_balance) * 100, self.perf.max_drawdown_p)
        
        if self._winning_months:
            self.perf.chance1m = (float(self._winning_months) / ( self._months_num - 1)) * 100.0  
        #if self._months_num - 1 > 3:
        #    self.perf.chance3m = 
            
        self.perf.profit_variance = standard_dev(self.monthly_profit)         
        if self.perf.profit_variance:       
            self.perf.sharpe_ratio = float(self.perf.profit_per_month) / self.perf.profit_variance                


        self.perf.trades_per_day = float(self.perf.number_of_trades) / self._days_num 
                
        if not self._sum_time_in_market:
            self._sum_time_in_market = trade.close_time - trade.open_time
        else:
            self._sum_time_in_market += trade.close_time - trade.open_time
        
        self.perf.avg_time_in_market = float(self._sum_time_in_market.seconds / 3600.0) / self.perf.number_of_trades         
        self.perf.avg_bars_in_trade = 0 
                        
        #self.perf.avg_neg_floating_p
        #self.perf.avg_pos_floating_p
        #self.perf.avg_neg_floating
        #self.perf.avg_pos_floating            
                
        self._day_profit += trade.profit
        self._week_profit += trade.profit
        self._month_profit += trade.profit
        self._year_profit += trade.profit
        
                