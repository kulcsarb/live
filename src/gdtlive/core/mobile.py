# -*- encoding: utf-8 -*-
'''
Created on 2013.09.19.

@author: kulcsarb
'''
import cherrypy
import cherrypy.lib
from gdtlive.config import RESOURCES_ROOT
from mako.template import Template
from mako.lookup import TemplateLookup
import gdtlive.core.tradeengine as live
from gdtlive.core.web import trade_fixmessages
import datetime
import re
import json


mako_lookup = TemplateLookup(RESOURCES_ROOT+'mobile/')

RELOAD_FREQUENCY = 5

def render(filename, data):
    template = mako_lookup.get_template(filename)
    return template.render(**data)


def process_logs(logtext):
    logs = []
    try:
        for entry in reversed(logtext.split('\n')):                       
            try:            
                e = entry.split(' - ')
                e[0] = re.search('(\d\d:\d\d:\d\d)', e[0]).groups(0)[0]                        
                logs.append(e)
            except:
                if entry:
                    logs.append(['','',entry.replace('/n','<br>')])
    except:
        pass            
    return logs


@cherrypy.expose
def fix(portfolio_id, account_id, logtype=2):
    portfolio_id = int(portfolio_id)
    account_id = int(account_id)
    logtype = int(logtype)
    data = {            
            'info' : live.trade_engine.get_engine_details(portfolio_id, account_id),
            'logs' : process_logs(live.trade_engine.get_engine_logs(portfolio_id, account_id, logtype)),
            'reload_freq' : RELOAD_FREQUENCY,
            'logtype' : logtype,
            'messages' : live.trade_engine.get_engine_last_messages(portfolio_id, account_id),
            'portfolio_id' : portfolio_id,
            'account_id' : account_id
            }    
    return render('fixaccount.html', data)
    

@cherrypy.expose
def account(portfolio_id, account_id, logtype=2):
    portfolio_id = int(portfolio_id)
    account_id = int(account_id)
    logtype = int(logtype)
    
    data = {
            'info' : live.trade_engine.get_account_data(portfolio_id, account_id),
            'trades' : live.trade_engine.get_trades(portfolio_id, account_id),
            'logs' : process_logs(live.trade_engine.get_account_logs(portfolio_id, account_id, logtype)),
            'reload_freq' : RELOAD_FREQUENCY,
            'logtype' : logtype,
            'portfolio_id' : portfolio_id,
            'account_id' : account_id            
            }            
    return render('account.html', data)



@cherrypy.expose
def trade(portfolio_id, account_id, trade_id):
    portfolio_id = int(portfolio_id)
    account_id = int(account_id)
    trade_id = int(trade_id)        
    
    data = {
            'account' : live.trade_engine.get_account_data(portfolio_id, account_id),
            'trade' : live.trade_engine.get_trade_data(portfolio_id, account_id, trade_id),            
            'reload_freq' : RELOAD_FREQUENCY,
            'portfolio_id' : portfolio_id,
            'account_id' : account_id            
            }
    return render('trade.html', data)


@cherrypy.expose
def trade_messages(portfolio_id, account_id, trade_id):
    portfolio_id = int(portfolio_id)
    account_id = int(account_id)
    trade_id = int(trade_id)        
    data = {            
            'messages' : json.loads(trade_fixmessages(portfolio_id, account_id, trade_id))['data'],
            'reload_freq' : RELOAD_FREQUENCY,
            'portfolio_id' : portfolio_id,
            'account_id' : account_id,
            'trade_id' : trade_id            
            }
    return render('trade_messages.html', data)


@cherrypy.expose
def index():    
    '''Visszaadja a böngészőnek a GUI kezdőoldalát

    @url: /
    @rtype: string
    @return: A kezdő html oldal
    '''        
   
    data = {
            'accounts': {},
            'portfolios' : {},             
            'trades' : {},
            'reload_freq' : RELOAD_FREQUENCY
            }
    
    portfolios = live.trade_engine.get_portfolios()    
    for portfolio_id in portfolios:
        data['trades'][portfolio_id] = {}
        accounts = live.trade_engine.get_accounts(portfolio_id)
        data['accounts'].update(accounts)
        for account_id in accounts.keys():
            trades = live.trade_engine.get_trades(portfolio_id, account_id)
            data['trades'][portfolio_id][account_id] = trades
                                
    data['portfolios'] = portfolios
    #print data
    return render('index.html', data)
