# -*- encoding: utf-8 -*- 
'''
Created on Nov 27, 2012

@author: gdt
'''
import gdtlive.store.db as db
from gdtlive.core.constants import *
from gdtlive.constants import *
from gdtlive.core.accounts.account import Account
import traceback
from threading import Lock
from datetime import datetime
from sqlalchemy import desc


class AccountManager(object):
    
    def __init__(self, portfolio):
        self.portfolio = portfolio
        self.log = self.portfolio.log
        self.accounts = {}
        self.lock = Lock()
        self.benchmark_account = None
        
#        self.open_allowed = True
#        self.playback = False        

    
    def load(self, session):
        if not self.portfolio.name or not self.portfolio.portfolio_id:
            self.log.warning('Portfolio is not initialized correctly, cant load accounts')
            return False
        
        try:
            self.log.info('Loading active accounts for portfolio "%s"' % self.portfolio.name)

            query = session.query(db.PortfolioAccountRun.id, db.PortfolioAccountRun.performance_id, db.PortfolioAccountRun.account_id, db.Account.broker_id, db.Account.name) #, db.PortfolioAccount.id, db.PortfolioAccount.account_id) # )
            query = query.filter(db.PortfolioAccountRun.state.in_((RUNNING, STOPPING, ABORTING)))
            query = query.filter(db.PortfolioAccountRun.portfoliorun_id == self.portfolio.portfoliorun_id)            
            query = query.filter(db.PortfolioAccountRun.account_id == db.Account.id)            
                                            
            for accountrun_id, performance_id, account_id, broker_id, name in query.all():
                #pfaccount_id = session.query(db.PortfolioAccount.id).filter(db.PortfolioAccount.account_id == account_id).filter(db.PortfolioAccount.portfolio_id == self.portfolio.portfolio_id).first()[0]                
                if account_id not in self.accounts:
                    self.log.info('Initializing account: "%s" at %s' % (name, LIVE_BROKERS[broker_id]))     
                    self.accounts[account_id] = Account(account_id, accountrun_id, performance_id, self.portfolio.portfolio_id, self)
                    self.accounts[account_id].load(session)
                    if self.accounts[account_id].benchmark:
                        self.benchmark_account = self.accounts[account_id]                                                 
            
            for account in self.accounts.itervalues():
                account.start()
        except:
            self.log.critical(traceback.format_exc())
                
        return True
    
    
    def start(self, session):
        if not self.portfolio.name or not self.portfolio.portfolio_id:
            self.log.warning('Portfolio is not initialized correctly, cant load accounts')
            return False
        
        self.log.info('Starting all accounts for portfolio "%s"' % self.portfolio.name)
        
        try:        
            query = session.query(db.PortfolioAccount.account_id, db.Account.broker_id, db.Account.name, db.Account.balance)
            query = query.filter(db.PortfolioAccount.account_id == db.Account.id)
            query = query.filter(db.PortfolioAccount.portfolio_id == self.portfolio.portfolio_id)          
            
            for account_id, broker_id, name, starting_balance in query.all():                
                if account_id not in self.accounts:
                    self.log.info('Initializing account: "%s" at %s' % (name, LIVE_BROKERS[broker_id]))
                    
                    accountrun = self._create_accountrun(account_id, starting_balance, session)
                    
                    self.accounts[account_id] = Account(account_id, accountrun.id, accountrun.performance_id, self.portfolio.portfolio_id, self) 
                    self.accounts[account_id].load(session)                                                    
                        
        except:
            self.log.critical(traceback.format_exc())

                    
        for account in self.accounts.itervalues():
            self.log.info('Starting account: "%s"' % account.account_name)
            account.start()
                
                
    def abort(self):
        self.log.info('Aborting all accounts')
        for account in self.accounts.values():     
            try:       
                account.abort()
            except:
                self.log.critical(traceback.format_exc())
        
        self.log.info('Waiting for accounts to finish aborting')
        for account in self.accounts.values():
            try:            
                account.wait_for_exit()
            except:
                self.log.critical(traceback.format_exc())
        
        self.log.info('Accounts aborted')
        self.accounts = {}
                        
    
    def stop(self):
        self.log.info('Stopping all accounts')
        for account in self.accounts.values():            
            try:
                account.stop()
            except:
                self.log.critical(traceback.format_exc())

    def _create_accountrun(self, account_id, starting_balance, session):
        accountrun = db.PortfolioAccountRun(self.portfolio.portfolio_id, self.portfolio.portfoliorun_id, account_id, datetime.utcnow(), datetime.utcnow())            
        session.add(accountrun)
        session.commit()          
        
        perf = db.PerformanceData(PERFORMANCE_CURRENCY, {})
        perf.clear()
        perf.starting_balance = starting_balance
        
        serial = db.PerformanceStrategySerialData([],[],[],[],[],[],[],[],[],[],[],[])
        session.add(perf)
        session.add(serial)
        session.commit()
                    
        accountrun.performance_id = perf.id
        accountrun.performanceserial_id = serial.id        
        session.commit()
        
        return accountrun


    def start_account(self, account_id):         
        try:
            session = db.Session()            
            
            query = session.query(db.PortfolioAccount.id, db.Account.broker_id, db.Account.name, db.Account.balance)
            query = query.filter(db.PortfolioAccount.account_id == db.Account.id)
            query = query.filter(db.PortfolioAccount.account_id == account_id)          
                                    
            pfaccount_id, broker_id, name, starting_balance = query.first()
            
            accountrun = self._create_accountrun(account_id, starting_balance, session)
            
            self.log.info('Initializing account: "%s" at %s' % (name, LIVE_BROKERS[broker_id]))            
            account = Account(account_id, accountrun.id, pfaccount_id, self.portfolio.portfolio_id, self)            
            account.load(session)                                                                
            account.start()
            
            self.accounts[account_id] = account
            
        except:
            self.log.critical(traceback.format_exc())
        finally:
            session.close()


    def stop_account(self, account_id):
        try:
            if account_id in self.accounts:            
                if not self.accounts[account_id].is_benchmark_account():
                    self.log.info('Stopping account "%s"' % self.accounts[account_id].account_name)
                    self.accounts[account_id].stop()
                else:
                    self.log.warning('Account "%s": stopping a benchmark account is not possible !')
        except:
            self.log.critical(traceback.format_exc())
        
    
    def abort_account(self, account_id):
        try:
            if account_id in self.accounts:            
                if not self.accounts[account_id].is_benchmark_account():
                    self.log.info('Aborting account "%s"' % self.accounts[account_id].account_name)
                    self.accounts[account_id].abort()
                else:
                    self.log.warning('Account "%s": aborting a benchmark account is not possible!')
        except:
            self.log.critical(traceback.format_exc())
            
                                
    def config_mm(self, mmplugin_id, mmplugin_parameters):
        for account in self.accounts.values()[:]:
            try:
                account.config_mm(mmplugin_id, mmplugin_parameters)
            except:
                self.log.critical(traceback.format_exc())


    def add_strategy(self, strategy, session):
        for account in self.accounts.values()[:]:
            try:
                account.add_strategy(strategy, session)
            except:
                self.log.critical(traceback.format_exc())    

    
    def remove_strategy(self, strategy_id):
        for account in self.accounts.values()[:]:
            try:
                account.remove_strategy(strategy_id)
            except:
                self.log.critical(traceback.format_exc())

    
    def before_evaluate(self, timeframe):        
        for account in self.accounts.values()[:]:
            try:
                account.before_evaluate(timeframe)
            except:
                self.log.critical(traceback.format_exc())
        
    def after_evaluate(self, timeframe):
        for account in self.accounts.values()[:]:
            try:
                account.after_evaluate(timeframe)
            except:
                self.log.critical(traceback.format_exc())

    def check_trades(self):
        for account in self.accounts.values()[:]:
            try:
                account.check_trades()
            except:
                self.log.critical(traceback.format_exc())
            

    def has_open_trades(self, strategy_id):
        return bool(sum((account.has_open_trades(strategy_id) for account in self.accounts.values())))
    
        
    def on_trade_open(self, strategy_id):        
        pass                        
                
                
    def on_trade_close(self, strategy_id):
        self.portfolio.on_trade_close(strategy_id)

    
    def on_account_stop(self, account_id):
        """
        lockolni kene hogy a commandokkal ne akadjon ossze
        """
        try:
            with self.lock:
                self.log.info('Account "%s" stopped, removing... ' % self.accounts[account_id].account_name)
                try:
                    del self.accounts[account_id]                
                except:
                    self.log.critical(traceback.format_exc())
                    pass
                self.portfolio.on_account_stop(account_id)
        except:
            self.log.critical(traceback.format_exc())
    
    
    def account_num(self):
        return len(self.accounts)
    
    
    def get_engine_details(self, account_id):
        #print self.accounts, account_id
        if account_id in self.accounts:
            return self.accounts[account_id].get_engine_details()
        return {}
    
    def get_engine_logs(self, account_id, logtype):
        if account_id in self.accounts:
            return self.accounts[account_id].get_engine_logs(logtype)
        else:
            return {}

    def get_account_logs(self, account_id, logtype):
        if account_id in self.accounts:
            return self.accounts[account_id].get_account_logs(logtype)
        else:
            return {}
    
    
    def get_engine_last_messages(self, account_id):
        if account_id in self.accounts:
            return self.accounts[account_id].get_engine_last_messages()
        return {}


    def shutdown(self, account_id):
        if account_id in self.accounts:
            return self.accounts[account_id].shutdown()
        
    def check_connection(self, account_id):
        if account_id in self.accounts:
            return self.accounts[account_id].check_connection()
        
    def has_account(self, account_id):
        return account_id in self.accounts
    
    def refresh_account(self, account_id):
        if account_id in self.accounts:
            self.accounts[account_id].reload_settings()
            
    def get_accounts(self):
        accounts = {}
        for account_id, account in self.accounts.items():
            accounts[account_id] = account.get_data()
        return accounts
    
    def get_account_data(self, account_id):      
        if account_id in self.accounts:
            return self.accounts[account_id].get_data()        
                            
    def get_trades(self, account_id):
        if account_id in self.accounts:
            return self.accounts[account_id].get_trades()
    
    def get_trade_data(self, account_id, trade_id):
        if account_id in self.accounts:
            return self.accounts[account_id].get_trade_data(trade_id)
        
    
    def get_benchmark_data(self):
        for account in self.accounts.values():
            if account.is_benchmark_account():
                return account.get_data()
        return {}
    
    
    def trade_repair(self, account_id, trade_id):
        if account_id in self.accounts:
            self.accounts[account_id].trade_repair(trade_id)
            
    def trade_close(self, account_id, trade_id):
        if account_id in self.accounts:
            self.accounts[account_id].trade_close(trade_id)
        
    def trade_markasclosed(self, account_id, trade_id):
        if account_id in self.accounts:
            self.accounts[account_id].trade_markasclosed(trade_id)
        
    def trade_mo_sendopen(self, account_id, trade_id):
        if account_id in self.accounts:
            self.accounts[account_id].trade_mo_sendopen(trade_id)
        
    def trade_mo_sendclose(self, account_id, trade_id):
        if account_id in self.accounts:
            self.accounts[account_id].trade_mo_sendclose(trade_id)
        
    def trade_co_place(self, account_id, trade_id, co_type):
        if account_id in self.accounts:
            self.accounts[account_id].trade_co_place(trade_id, co_type)
        
    def trade_co_cancel(self, account_id, trade_id, co_type):
        if account_id in self.accounts:
            self.accounts[account_id].trade_co_cancel(trade_id, co_type)
        
    def trade_co_filled(self, account_id, trade_id, co_type):
        if account_id in self.accounts:
            self.accounts[account_id].trade_co_filled(trade_id, co_type)


    

    # ----------------------------------------------------
    #        TRADING RELATED METHODS
    # ----------------------------------------------------
            
    def open(self, symbol, direction, percent, group_id, strategy_id):
        #print 'lock'
        with self.lock:
            try:
                for account in self.accounts.values():                    
                    account.open(symbol, direction, percent, group_id, strategy_id)
                    
            except:
                self.log.critical(traceback.format_exc())
    

    def openOrHold(self, symbol, direction, percent, group_id, strategy_id, canopen):
        #print 'lock'
        with self.lock:
            try:
                for account in self.accounts.values():                    
                    account.openOrHold(symbol, direction, percent, group_id, strategy_id, canopen)
                    
            except:
                self.log.critical(traceback.format_exc())
    
    
    def closeFirstTrade(self, symbol, group=0, strategy_id=0):
        """
        
        A symbol csak a CommandNode kompatibilitás miatt van benne
        """
        with self.lock:
            try:        
                for account in self.accounts.values():
                    account.closefirst(group, strategy_id)  
            except:
                self.log.critical(traceback.format_exc())                 
         
    
    def closeLastTrade(self, symbol, group=0, strategy_id=0):
        """
        
        A symbol csak a CommandNode kompatibilitás miatt van benne
        """
        with self.lock:                        
            try:            
                for account in self.accounts.values():
                    account.closelast(group, strategy_id)                    
            except:
                self.log.critical(traceback.format_exc())                
            

    def closeAll(self, group=0, strategy_id=0):        
        with self.lock:                                
            try:            
                for account in self.accounts.values():                    
                    account.closeall(group, strategy_id)                   
            except:                        
                self.log.critical(traceback.format_exc())                                                
        

    def closeAllSymbol(self, symbol, group=0, strategy_id=0):                        
        self.closeAll(group, strategy_id)     
        
                        
