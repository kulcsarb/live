# -*- encoding: utf-8 -*- 
'''
Created on Jul 3, 2012

@author: gdtlive
'''
import gdtlive.store.db as db
from sqlalchemy import Column, Integer, String, Float, Time, Boolean, DateTime, Date, Text, create_engine, ForeignKey, Table, Sequence, LargeBinary, MetaData
from sqlalchemy.orm import relationship

import gdtlive.core.sqlupdater as sql
from gdtlive.evol.gnodes.GenomeBase import GTree
from gdtlive.evol.gnets.NetBase import GNet
from gdtlive.evol.gnets.NetBaseEcho import GNetEcho
from gdtlive.evol.gnets.NetBaseEchoPredict import GNetEchoPredictMulti
from gdtlive.utils import OrderedDict, open_db_connection
from gdtlive.core.constants import *
from gdtlive.constants import * 
import gdtlive.core.datafeed.historic as historic
from gdtlive.core.trade import Trade

from datetime import datetime, timedelta
from multiprocessing import Process
import threading
import logging
import calendar
import traceback
import os.path
import os
import ast

#log = logging.getLogger('gdtlive.core.managers')



class Strategy(object):
        
    def __init__(self, accountmanager, log):
        self.log = log
        self.accountmanager = accountmanager        
        self.strategy_id = 0 
        self.portfolio_id = 0               
        self.portfoliorun_id = 0        
        self.portfstratrun_id = 0
        self.timeframe = 0
        self.state = 0
        self.symbols = []
        self.running_from = None
        self.running_to = None
        self.open_allowed = True 
        self.name = ""        
        self.playback = False
        self.last_processed_candle = None
        self.weekend = False
        self.weekend_start = 0
        self.stratplugin_id = 0        
        self.lock = threading.Lock()
        
        
    def load(self, session, portfolio_id, pfstratrun_id):
        from gdtlive.core.tradeengine import feedserver
        
        try:
            self.portfstratrun_id = pfstratrun_id
            
            run = session.query(db.PortfolioStrategyRun).get(pfstratrun_id)
            if not run:
                return False
            self.log.info('loading strategy %d  - %s' % (run.strategy_id, run.state))
            self.portfolio_id = portfolio_id    
            self.portfoliorun_id = run.portfoliorun_id        
            self.running_from = run.running_from
            self.running_to = run.running_to        
            self.strategy_id = run.strategy_id            
            self.state = run.state

            if self.state in [ABORTING, STOPPING]:
                self.open_allowed = False
                
                if not self.accountmanager.has_open_trades(self.strategy_id):
                    self.state = {'ABORTING': ABORTED, 'STOPPING': STOPPED}[self.state]
                    self._update_state(self.state)                    
                    return False
            
            strategy = session.query(db.Strategy).get(self.strategy_id)
            if not strategy:
                self.log.error('Strategy %d not found!' % self.strategy_id)
                return False
                        
            if not strategy.mmplugin_id:
                self.log.error('Strategy %d: no mplugin_id !' % self.strategy_id)
                return False
            
            self.timeframe = strategy.timeframe
            self.mmplugin_id = strategy.mmplugin_id
            self.weekend_start = strategy.weekend_start              
            self.stratplugin_id = strategy.stratplugin_id          
            #self._check_weekend()
            
            try:
                self.mmplugin_parameters = ast.literal_eval(strategy.mmplugin_parameters)
            except:
                self.log.error('Strategy %d: mmplugin parameters are invalid :  %s ' % (self.strategy_id, strategy.mmplugin_parameters))
                return False
            
            #self.log.info('DNS loaded : ' + strategy.dns)                       
            if self.stratplugin_id == 1:
                self.genome = GTree.decode(strategy.dns)
            elif self.stratplugin_id == 2:
                self.genome = GNet.decode(strategy.dns)
            elif self.stratplugin_id == 3:
                self.genome = GNetEcho.decode(strategy.dns)
            elif self.stratplugin_id == 4:
                self.genome = GNetEchoPredictMulti.decode(strategy.dns)
            else:
                self.log.error('Unknown stratplugin_id ! (%d) ' % self.stratplugin_id)
                return False
            
            self.genome.command_nodes = self.genome.getCommandNodes() 
            self.genome.log = self.log
            
            for command in self.genome.command_nodes:
                command.strategy_id = self.strategy_id
                command.group_id = 0
                
            self.symbols = self.genome.getSymbols()            
            # most tudjuk hogy 1 dns-ben csak 1 symbol lehet  
            self.symbol = list(self.symbols)[0]
                                                                                                                                                                       
            feedserver.register_instrument(self.symbol, self.timeframe)
            self.accountmanager.add_strategy(self, session)
        except:
            self.log.error(traceback.format_exc())
        finally:
            return True
                    
                
    def start(self, session, portfolio_id, portfolio_run_id, strategy_id):                        
        run = db.PortfolioStrategyRun(portfolio_run_id, strategy_id, None, None)
        session.add(run)
        session.commit()
                                        
        self.load(session, portfolio_id, run.id)
        
        self.open_allowed = True
        self._update_state(RUNNING)        
                                                  
                
    def abort(self):        
        self.open_allowed = False        
        if self.accountmanager.has_open_trades(self.strategy_id):     
            self.log.info('Strategy %d has open trades, starting the abort process' % self.strategy_id)                        
            self.closeAll(-1, self.strategy_id)
            self._update_state(ABORTING)        
        else:
            self.log.info('Strategy %d has no open trades, marking as ABORTED now' % self.strategy_id)
            self._update_state(ABORTED)
                            
        
    def stop(self):        
        self.open_allowed = False        
        if self.accountmanager.has_open_trades(self.strategy_id):
            self.log.info('Strategy %d has open trades, starting the stop process' % self.strategy_id)
            self._update_state(STOPPING)
        else:
            self.log.info('Strategy %d has no open trades, marking as STOPPED now' % self.strategy_id)
            self._update_state(STOPPED)                    


    def _check_weekend(self, candle_time):        
        if self.weekend_start:
            if self.timeframe == 1440:
                if candle_time.isoweekday() in [5,6]:                    
                    self.weekend = True                
                else:
                    self.weekend = False
            else:                                     
                if candle_time.isoweekday() == 5 and candle_time.hour >= self.weekend_start:
                    if not self.weekend:
                        self.log.info('strategy %d - weekend starts, closing all open trades' % self.strategy_id)
                        self.closeAll(-1, self.strategy_id)
                    self.weekend = True
                elif candle_time.isoweekday() > 5:
                    self.weekend = True
                else:
                    if self.weekend:
                        self.log.info('strategy %d - weekend ended.' % self.strategy_id)                    
                    self.weekend = False
        
        self.log.info('strategy %d - Weekend: %s' % (self.strategy_id, self.weekend))

        
    def evaluate(self):
        try:            
            if self.state in (STOPPED, ABORTED, ABORTING):                
                return
                                                                 
            self.log.info('strategy %d -- evaluating start --' % (self.strategy_id))            
                        
            
            if not self.last_processed_candle:
                self.last_processed_candle = historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME][-2]
                                    
            if self.last_processed_candle != historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME][-1]:                    
                
                last_proc_index = historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME].index(self.last_processed_candle)
                last_index = len(historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME]) - 1
                current_index = last_proc_index
                
                while current_index != last_index:
                    current_index += 1
                    self.log.debug('strategy %d  - last_processed: %d, %s, processing: %d, %s, final: %d, %s' % 
                              (self.strategy_id,
                               last_proc_index, historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME][last_proc_index], 
                               current_index, historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME][current_index],
                               last_index, historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME][last_index]                                   
                               )
                              )                        
                    
                    weekend_status = self.weekend
                    self._check_weekend(historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME][current_index])                                         
                    
                    self.playback = True if current_index < last_index else False                        
                    if self.playback:
                        self.log.warning('strategy %d - Playback mode is active, evaluating candle %s' % 
                                    (self.strategy_id, historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME][current_index]))
                    else:
                        self.log.info('strategy %d - evaluating candle: %s' % (self.strategy_id, historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME][current_index]))
                        
                    self.genome.prepareForBacktest(historic.DATAROWS[self.timeframe], self)
                    
                    for command in self.genome.command_nodes:                
                        self.log.info('strategy %d - checking: %s(group=%d) %s' % (self.strategy_id, str(command), command.group_id, command.signal[current_index-9:current_index+1]))
#                        if len(command.signal) and command.signal[current_index]:
                        self.log.info('strategy %d - Executing: %s(group=%d)' % (self.strategy_id, str(command), command.group_id))
                        try:                                       
                            command.execute(current_index)
                        except:
                            self.log.error(traceback.format_exc())       
                    
                    
                    self.last_processed_candle = historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME][current_index]
                    self.last_proc_index = current_index                        
                    self.running_to = historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME][current_index]                    
                    if not self.running_from:
                        self.running_from = self.running_to
                    self._update_state()
                                                                
            else:
                self.log.warning("strategy %d - datarow %s%s isn't updated, skip dns evaluating" % (self.strategy_id, self.symbol, TIMEFRAME[self.timeframe]))
                                                                                                                                                                                                                           
        except:
            self.log.error(traceback.format_exc())
            
        finally:            
            self.log.info('strategy %d -- evaluating end --' % (self.strategy_id))
        

    
    def evaluate_until_now(self):
        if not self.running_from or not self.running_to:
            return 
        
        try:
        
            self.last_processed_candle = self.running_to
                
            if self.running_to == historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME][-1]:            
                self.log.info('no need for playback')
                return 
                    
            self.log.info('strategy %d -- evaluate_until_now start --' % (self.strategy_id))
            self.genome.prepareForBacktest(historic.DATAROWS[self.timeframe], self)
            
            current_index = historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME].index(self.running_to)
            self.playback = True
            
            while self.running_to != historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME][-1]:
                current_index += 1
                self.log.info('evaluating candle: %s' % historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME][current_index])
                
                self._check_weekend(historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME][current_index])
                
                for command in self.genome.command_nodes:                
                    self.log.info('strategy %d - checking: %s(group=%d) %s' % (self.strategy_id, str(command), command.group_id, command.signal[current_index-9:current_index+1]))
                    if len(command.signal) and command.signal[current_index]:
                        self.log.info('strategy %d - Executing: %s(group=%d)' % (self.strategy_id, str(command), command.group_id))
                        try:                                       
                            command.execute(current_index)
                        except:
                            self.log.error(traceback.format_exc())                           
                                    
                    self.last_processed_candle = self.running_to = historic.DATAROWS[self.timeframe][self.symbol][PRICE_TIME][current_index]                         
                
            self._update_state()    
            self.playback = False
            self.log.info('strategy %d -- evaluate_until_now end --' % (self.strategy_id))
            
        except:
            self.log.critical(traceback.format_exc())
            self.log.critical(locals())
            
    
    def on_weekend_start(self):
        try:
            if self.weekend_start:
                self.closeAll(group=0, strategy_id=self.strategy_id)
        except:
            self.log.critical(traceback.format_exc())
                 
    
    def dump_signals(self):
        try:
            candle_time = historic.CURRENT_TIME
            path = '/opt/account_%d/strategy_%d/%s' % (self.account.account_id, self.strategy_id, candle_time.strftime('%Y%m%d_%H%M'))
            if not os.path.exists(path):                
                os.makedirs(path)                                        
            self.genome.dump_signals(path)
        except:
            self.log.warning('Could not create log files !')
            self.log.warning(traceback.format_exc())

    
    def _update_state(self, state=None):
        if state:
            self.log.info('Startegy %d: %s -> %s' % (self.strategy_id, self.state, state))
            self.state = state                                               
        try:            
            table = db.PortfolioStrategyRun.__table__
            command = table.update().where(table.c.id==self.portfstratrun_id).\
                  values(state = self.state,
                         running_from = self.running_from, 
                         running_to = self.running_to
                         )
            
            sql.server.update(command)                            
        except:                                
            self.log.warning("Strategy %d - SAVE FAILED"  % (self.strategy_id))
            self.log.warning(traceback.format_exc())


    def has_open_trades(self):
        return self.accountmanager.has_open_trades(self.strategy_id)
        
    
    def on_trade_close(self):                
        if self.state == STOPPING:            
            if not self.accountmanager.has_open_trades(self.strategy_id):                
                self._update_state(STOPPED)
                
        elif self.state == ABORTING:
            if not self.accountmanager.has_open_trades(self.strategy_id):                
                self._update_state(ABORTED)           
            
                                            
    def is_stopped(self):        
        return self.state == STOPPED                 
            
    def is_aborted(self):        
        return self.state == ABORTED
    
    
    
    # ----------------------------------------------------
    #        TRADING RELATED METHODS
    # ----------------------------------------------------
    
    def buy(self, symbol, percent=1.0, group=0, strategy_id=0):
        try:
            if self.open_allowed and not self.playback and not self.weekend:                                                        
                self.accountmanager.open(symbol, DIR_BUY, percent, group, strategy_id)
        except:
            self.log.critical(traceback.format_exc())

                                                            
    def sell(self, symbol, percent=1.0, group=0, strategy_id=0):
        try:            
            if self.open_allowed and not self.playback and not self.weekend:                        
                self.accountmanager.open(symbol, DIR_SELL, percent, group, strategy_id)
        except:
            self.log.critical(traceback.format_exc())
            
    
    def buyOrHold(self, symbol, percent=1.0, group=0, strategy_id=0):
        try:            
            if not self.weekend:                
                self.accountmanager.openOrHold(symbol, DIR_BUY, percent, group, strategy_id, self.open_allowed and not self.playback)
        except:
            self.log.critical(traceback.format_exc())
            
    def sellOrHold(self, symbol, percent=1.0, group=0, strategy_id=0):
        try:            
            if not self.weekend:                        
                self.accountmanager.openOrHold(symbol, DIR_SELL, percent, group, strategy_id, self.open_allowed and not self.playback)
        except:
            self.log.critical(traceback.format_exc())

    
    def closeFirstTrade(self, symbol, group=0, strategy_id=0):
        try:
            self.accountmanager.closeFirstTrade(symbol, group, strategy_id)
        except:
            self.log.critical(traceback.format_exc())

    
    def closeLastTrade(self, symbol, group=0, strategy_id=0):
        try:
            self.accountmanager.closeLastTrade(symbol, group, strategy_id)
        except:
            self.log.critical(traceback.format_exc())

        
    def closeAll(self, group=0, strategy_id=0):
        try:            
            self.accountmanager.closeAll(group, strategy_id)
        except:
            self.log.critical(traceback.format_exc())

    
    def closeAllSymbol(self, symbol, group=0, strategy_id=0):
        try:
            self.accountmanager.closeAll(group, strategy_id)                                
        except:
            self.log.critical(traceback.format_exc())
      
        