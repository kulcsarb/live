'''
Created on Oct 28, 2013

@author: gdt
'''
from fixaccountbase import FIXAccountBase
from gdtlive.core.accounts.benchmark import BenchmarkAccount
from gdtlive.core.trade import Trade
import gdtlive.core.datafeed.historic as historic
from datetime import datetime
import gdtlive.core.sqlupdater as sql
from gdtlive.core.constants import *
from gdtlive.constants import * 
import gdtlive.store.db as db
import traceback
import copy
import urllib
import calendar
import json
import time
from threading import Thread
from Queue import Queue


#class ZuluTrade(BenchmarkTrade):
#    
#    
#    def get_unique_id(self):
#        return 'Genome-T%d-%d' % (self.id, calendar.timegm(datetime.utcnow().utctimetuple()))
#    
#    def send_open(self):
#        self.state = TRADE_OPENING
#        self.save()
#        
#        message = {
#                   'currecyName': self.symbol,
#                   'lots': self.amount,
#                   'buy': 'true' if self.direction == DIR_BUY else 'false',
#                   'requestedPrice' : self.req_price,
#                   'uniqueId' : self.get_unique_id
#                   }
#        self.log.debug('message: %s' % str(message))
#        retries = 10 
#        while True:            
#            try:
#                response = urllib.urlopen(self.fix_account.server_url+'/open/market?%s' % urllib.urlencode(message))
#            except IOError:
#                self.load.error(traceback.format_exc())
#                retries -= 1
#                continue
#            
#            try:
#                response = json.loads(response.read())
#            except:
#                self.load.error(traceback.format_exc())
#                retries -= 1
#                continue
#            self.log.debug('response: %s' % str(response))
#            
#            if response['success']:                            
#                self.state = TRADE_OPENED    
#                self.open_price = response['entryRate']
#                            
#                self.fix_account.on_trade_open(self)
#                self.save()
#                self.log.info('**** TRADE OPENED: %s **** ' % (str(self)))     
#    
#    
#    def send_close(self):
#        
#        
#        
#        self.close(ORDERTYPE_CLOSE, historic.CURRENT_PRICE[self.symbol]['ASK' if self.direction == DIR_SELL else 'BID'])
        
class ZuluTransfer(Thread):
    
    def run(self):
        while True:
            path, message
        
        
    def send(self, path, message):                
        retries = 10
        while True:            
            try:
                response = urllib.urlopen(self.server_url+path+'?'+urllib.urlencode(message))
            except IOError:
                self.log.error(traceback.format_exc())
                retries -= 1
                time.sleep(1)
                continue
            
            try:
                response = json.loads(response.read())
            except:
                self.load.error(traceback.format_exc())
                retries -= 1
                continue
            self.log.debug('response: %s' % str(response))
            
            if response['success']:                            
                return True
            else:
                return False
            
            
            
    
    

        
    
    
class ZuluAccount(FIXAccountBase):
    broker_id = 5
    TradeClass = Trade
    
    def __init__(self, account, accountinfo):
        FIXAccountBase.__init__(self, account, accountinfo)
        self.server_url = "http://tradingserver.zulutrade.com"
        self.server_port = 80
        self.server_use_ssl = False
    
    
    def place_stoploss(self, trade, entry_clordid, symbol, direction, amount, price, entry_sent_time, resend=None):
        return self._place_conditionalorder(trade, entry_clordid, symbol, direction, amount, price, entry_sent_time, resend)
        
        
    def place_takeprofit(self, trade, entry_clordid, symbol, direction, amount, price, entry_sent_time, resend=None):
        return self._place_conditionalorder(trade, entry_clordid, symbol, direction, amount, price, entry_sent_time, resend)
            
        
    def _place_conditionalorder(self, trade, entry_clordid, symbol, direction, amount, price, entry_sent_time, resend):                
        
        self.register_order(trade, entry_clordid)
        message = {
                   'currencyName' : symbol,
                   'lots' : amount,
                   'buy' : 'true' if direction == DIR_SELL else 'false',
                   'requestedPrice' : price,
                   'uniqueId' : entry_clordid                   
                   }
            
        return self.send_message('/open/pending', message)
    
        
    def cancel_order(self, trade, symbol, direction, entry_orderid, entry_clordid, cancel_clordid, resend=None):
        
        self.register_order(trade, cancel_clordid)
        
        message = {
                   'currencyName' : symbol,
                   'lots' : trade.amount,
                   'buy' : 'true' if direction == DIR_SELL else 'false',
                   'uniqueId' : entry_clordid,
                   }         
        
        return self.send_message('/close/pending', message)
    
        
    
    def send_marketorder(self, trade, clordid, direction, amount, symbol, resend=None):                        
        if trade.state == TRADE_OPENING:
            return self.open_market(trade, clordid, direction, amount, symbol, resend)
        
        if trade.state == TRADE_CLOSING:
            return self.close_market(trade, clordid, direction, amount, symbol, resend)                                    
        
            
    def open_market(self, trade, clordid, direction, amount, symbol, resend=None):
        self.register_order(trade, clordid)
        message = {
                   'currencyName' : symbol,
                   'lots' : amount,
                   'buy' : 'true' if direction == DIR_BUY else 'false',
                   'requestedPrice' : trade.req_price,
                   'uniqueId' : clordid                   
                   }
        return self.send_message('/open/market', message)
    
    
    def close_market(self, trade, clordid, direction, amount, symbol, resend=None):
        message = {
                   'currencyName' : symbol,
                   'lots' : amount,
                   'buy' : 'true' if direction == DIR_BUY else 'false',
                   'requestedPrice' : 1.0, # lehet hogy itt be kell szerezni az aktualis arat....
                   'uniqueId' : trade.market_order.open_clordid                   
                   }
        return self.send_message('/close/market', message)
        
    
    
    def send_message(self, path, message):
        self.send_queue.put((path, message))
        