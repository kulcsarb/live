# -*- encoding: utf-8 -*- 
'''
Created on Dec 27, 2012

@author: gdt
'''
from gdtlive.c._backtest.simulatedliveaccount import SimulatedLiveAccount
from gdtlive.core.accounts import fix_accounts
from gdtlive.core.constants import *
from gdtlive.constants import * 
import gdtlive.core.datafeed.historic as historic
import gdtlive.store.db as db
import gdtlive.config as config 
from gdtlive.utils import is_weekend, add_log_handler, remove_log_handlers
from gdtlive.core.trade import CONDORDER_UNKNOWN
import time
import traceback
import copy
from datetime import datetime, timedelta
from Queue import Queue, Empty
from threading import Thread, Event, Lock
from sqlalchemy.sql import select
import gdtlive.core.sqlupdater as sql
import gdtlive.core.performance as performance

import logging

class Account(Thread):
    
    
    def __init__(self, account_id, accountrun_id, performance_id, portfolio_id, accountmanager):
        Thread.__init__(self)
         
        self.account_id = account_id
        self.accountrun_id = accountrun_id
        self.performance_id = performance_id        
        self.portfolio_id = portfolio_id
        self.accountmanager = accountmanager
        
        self.open_allowed = True
        self.online = True
        self.state = STOPPED
        self.symbols = []
        self.strategies = {}
        self.exposure = {}
        self.mm_plugins = {}
        
        self.equity = 0
        self.base_equity = 0
        self.leverage = 0        
        self.use_of_leverage = 0
        self.risk_multiplier = 1.0
        self.used_margin = 0
        self.usable_margin = 0       
        self.staring_balance = 0 
        self.balance = 0
        self.floating_profit = 0
        self.open_trades_num = 0
        self.open_trades = {}
        self.trades = {}
        self.running_from = None
        self.running_to = None
        self.benchmark = False
        self.min_floating = 0
        
        self.account_name = None
        self.command_queue = Queue()
        self.fix_account = None        
        self.thread_exit_event = Event()
        self.lock = Lock()
        self.log = accountmanager.log
        self.connection_check_interval = 60
        self.connection_state = STATE_NOT_CONNECTED
    
    
    def initialize_logging(self):                                                        
        name = 'gdtlive.core.portfolio.%s.account.%s' % (self.accountmanager.portfolio.name, self.account_name)                    
        self.log = logging.getLogger(name)                
        self.log.setLevel(logging.DEBUG)        
        remove_log_handlers(self.log)        
        add_log_handler(self.log, name, logging.INFO)
        add_log_handler(self.log, name, logging.WARN)
        add_log_handler(self.log, name, logging.DEBUG)                
    
            
    def load(self, session):
        try:
            account = session.query(db.Account).get(self.account_id)
            if not account:
                self.log.error("Account %d not found!" % self.account_id)
                return False
            
            self.account_name = account.name
            self.initialize_logging()
            
            q = session.query(db.PortfolioAccountRun.state, 
                              db.PortfolioAccountRun.running_from, 
                              db.PortfolioAccountRun.running_to, 
                              db.PerformanceData.starting_balance,                               
                              db.PerformanceData.net_profit,
                              db.PerformanceData.min_floating)            
            q = q.filter(db.PerformanceData.id == db.PortfolioAccountRun.performance_id)
            q = q.filter(db.PortfolioAccountRun.id == self.accountrun_id)
                        
            self.state, self.running_from, self.running_to, self.starting_balance, net_profit, self.min_floating = q.first()                        
                         
            self.log.info('state : %s' % (self.state))
            
            self.fix_account = fix_accounts[account.broker_id](self, account)
                                            
            self.online = account.online
            self.leverage = account.leverage
            self.base_equity = account.base_equity
            self.risk_multiplier = account.risk_multiplier                
            self.benchmark = account.benchmark
            self.balance = self.starting_balance + net_profit
            
            self.log.info('loading trades')
            
            self.open_trades_num = 0
            trades = session.query(db.Trade.id, db.Trade.strategy_id).filter(db.Trade.accountrun_id == self.accountrun_id).filter(db.Trade.state != TRADE_CLOSED).all()
            for trade_id, strategy_id in trades:
                if not strategy_id:
                    from gdtlive.core.trade import Trade
                    trade = Trade(fix_account = self.fix_account)
                else:                        
                    trade = self.fix_account.TradeClass(fix_account = self.fix_account)
                trade.load(trade_id)                            
                self.open_trades.setdefault(trade.strategy_id, {})[trade.id] = trade   
                self.trades[trade.id] = trade     
                self.open_trades_num += 1
                self.log.info('%s' % trade)
                self.exposure.setdefault(trade.symbol, 0)
                self.exposure[trade.symbol] += trade.get_exposure()
                
            self.log.info('loading finished. base equity: %.1f, balance:, %.1f, leverage: %.0f, risk_multiplier: %.1f' % (self.base_equity, self.balance, self.leverage, self.risk_multiplier))                    
            #self._update_account_exposure()
            #performance.server.calculate_account(self.accountrun_id)
                      
            if self.state in [ABORTING, STOPPING]:
                self.open_allowed = False
                            
        except:
            self.log.critical(traceback.format_exc())
        
    def abort(self):                     
        with self.lock:                            
            self.open_allowed = False
            self._update_state(ABORTING)
            self.thread_exit_event.clear()
            
            if self.open_trades_num:
                for trade in self.trades.values()[:]:
                    if trade.state == TRADE_OPENED:
                        self.log.info('Closing trade: %s' % trade)
                        trade.send_close()            
            
            
    def stop(self):    
        with self.lock:            
            self.open_allowed = False
            self._update_state(STOPPING)
        
        
    def wait_for_exit(self):                    
        #self.log.info('Waiting for account %s to exit' % self.account_name)
        self.thread_exit_event.wait()
                    
    
    def on_logon(self):
        self.log.info('Account logon handler')        
        self.resend_failed_trades()
        
    
    def resend_failed_trades(self):
        self.log.info('Resending failed orders')        
        for trade in self.trades.values()[:]:
            trade.resend_failed_orders()

    
    def on_trade_close(self, trade):        
        self.balance += trade.profit
        #self._update_performance()
        #self._update_trade_exposure(trade)
        self._remove_trade(trade)
        performance.server.calculate_strategy(trade.strategyrun_id)
        performance.server.calculate_account(self.accountrun_id)
        #self.accountmanager.on_trade_close(trade.strategy_id)
    
    
    def on_trade_open(self, trade):
        #self._update_performance()
        #self._update_trade_exposure(trade)
        self.accountmanager.on_trade_open(trade.strategy_id)
        
    
    def is_benchmark_account(self):
        return self.fix_account.is_benchmark_account()
    
    
    def check_trades(self):        
        self.fix_account.check_trades()                
        
            
    def run(self):
        if self.state not in (STOPPING, ABORTING):
            self._update_state(RUNNING)            
        
        self.thread_exit_event.clear()
        self.fix_account.connect()
        
        commands = {'open' : self._open,
                    'openOrHold' :  self._openOrHold,
                    'closeall' : self._closeall,
                    'closelast' : self._closelast,
                    'closefirst' : self._closefirst,
                    'after_evaluate' : self._after_evaluate}
        
        last_conn_check = datetime.utcnow()
        
        last_perf_update = datetime.utcnow()
        while True:
            try:
                command, params = self.command_queue.get(True, 1)    
                with self.lock:                        
                    commands[command](*params)            
            except Empty:
                try:
                    self.fix_account.on_tick()
                                    
                    # itt lehet figyelni a hálózat kimaradást, az orderek újraküldését, stb
                    if not is_weekend() and datetime.utcnow() > last_conn_check + timedelta(seconds=self.connection_check_interval):
                        last_conn_check = datetime.utcnow()
                        self.fix_account.check_connection()                                
                    
                    self._update_connection_state()
                    
                    if datetime.utcnow() > last_perf_update + timedelta(seconds=60):
                        last_perf_update = datetime.utcnow()
                        self._update_performance()                                                 
                    
                    if not self.open_trades_num:            
                        if self.state == ABORTING:
                            self.log.info('No more open trades left, abort complete')
                            self._update_state(ABORTED)
                            self.log.info('ABORTED, exiting from main loop')
                            break   
                                        
                        if self.state == STOPPING:
                            self.log.info('No more open trades left, stop complete')
                            self._update_state(STOPPED)
                            self.log.info('STOPPED, exiting from main loop')
                            break
                except:
                    self.log.critical(traceback.format_exc())
            except:
                self.log.error(traceback.format_exc())
                                
        self.fix_account.shutdown()                
        self._update_connection_state()                        
        self._update_performance()        
        self._update_lrconfigs_onexit()                        
        self.accountmanager.on_account_stop(self.account_id)                            
        self.log.info('EXIT')        
        self.thread_exit_event.set()        
    
                     
    def config_mm(self, strategy_id,  symbol, mmplugin_id, config):        
        config = copy.copy(config)                        
        self.log.info('Strategy %d: Configuring money management settings %s' % (strategy_id, str(config)))                        
        
        # Fix kötésméret esetén arányositjuk az aktuális account base_equityjéhez képest 
        if config['positionmode'] == 2:                                                             
            config['positionsize'] = int( config['positionsize'] * self.risk_multiplier )                                        
            self.log.info('Strategy %d: Position size is set to %d %%' % (strategy_id, config['positionsize']))            
                                       
                                            
        parameters = {
                      'equity' : self.base_equity,
                      'leverage' : self.leverage,
                      'parameters' : config
                      }
    
        self.mm_plugins[strategy_id] = SimulatedLiveAccount()                 
        self.mm_plugins[strategy_id].configMM(mmplugin_id, parameters)
        self.mm_plugins[strategy_id].setBaseEquity(self.base_equity)
        self.mm_plugins[strategy_id].setInstrumentNum(1)
        self.mm_plugins[strategy_id].setInstrument(0, PIP_MULTIPLIER[symbol], symbol)

    
    def _reconfigure_mm(self):
        self.log.info('Reconfiguring mmplugin settings')
        for strategy_id in self.mm_plugins:        
            try:                
                strategy = self.strategies[strategy_id]['object']
                config = copy.copy(strategy.mmplugin_parameters)   
                if config['positionmode'] == 2:                                                             
                    config['positionsize'] = int( config['positionsize'] * self.risk_multiplier )                                        
                    self.log.info('Strategy %d: Position size is set to %d %%' % (strategy_id, config['positionsize']))
                    
                parameters = {
                      'equity' : self.base_equity,
                      'leverage' : self.leverage,
                      'parameters' : config
                      }
                self.mm_plugins[strategy_id].configMM(strategy.mmplugin_id, parameters)
                self.mm_plugins[strategy_id].setBaseEquity(self.base_equity)
            except:
                self.log.error(traceback.format_exc())
                                                
                                                
    
    def add_strategy(self, strategy, session):
        try:
            if strategy.strategy_id in self.strategies:
                #self.log.warning('Strategy %d already added' % (strategy.strategy_id))
                return        
            
            self.log.info('Adding strategy: %d on %s %s' % (strategy.strategy_id, str(strategy.symbols), TIMEFRAME[strategy.timeframe] ))
            
            self.strategies[strategy.strategy_id] = {}
            self.strategies[strategy.strategy_id]['object'] = strategy
            if strategy.symbol not in self.symbols:
                self.symbols.append(strategy.symbol)
                self.exposure[strategy.symbol] = 0
                
            self.open_trades.setdefault(strategy.strategy_id, {})
            
            query = session.query(db.LiveRunConfig)
            query = query.filter(db.LiveRunConfig.portfstratrun_id ==strategy.portfstratrun_id)
            query = query.filter(db.LiveRunConfig.accountrun_id == self.accountrun_id)
            query = query.filter(db.LiveRunConfig.status_id < LIVERUN_STATUS_STOPPED)
            
            strategyrun_id = 0
            lrconfig = query.first()
            if not lrconfig:            
                self.log.info('Creating LiveRunConfig & StrategyRun for strategy %d' % strategy.strategy_id)
                lrconfig = db.LiveRunConfig(1, strategy.mmplugin_id, str(strategy.mmplugin_parameters), self.account_id, self.accountrun_id, strategy.portfstratrun_id, strategy.strategy_id, strategy.timeframe, 0, self.risk_multiplier)
                lrconfig.running_from = datetime.utcnow()
                lrconfig.running_to = datetime.utcnow()
                lrconfig.symbols = strategy.symbol
                lrconfig.status_id = LIVERUN_STATUS_RUNNING
                lrconfig.status_message = LIVERUN_STATUS_STR[LIVERUN_STATUS_RUNNING]
                
                datarow = session.query(db.DatarowDescriptor).filter(db.DatarowDescriptor.timeframe_num == strategy.timeframe).filter(db.DatarowDescriptor.symbol==strategy.symbol).first()
                if datarow:
                    lrconfig.datarows.append(datarow)
                else:
                    self.log.warning('DatarowDescriptor not found for %s%s' % (strategy.symbol, TIMEFRAME[strategy.timeframe]))
                session.add(lrconfig)
                session.commit()
                perf = db.PerformanceData(PERFORMANCE_CURRENCY, {})
                perf.clear()
                serial = db.PerformanceStrategySerialData([],[],[],[],[],[],[],[],[],[],[],[])
                session.add(perf)
                session.add(serial)
                session.commit()
                run = db.StrategyRun(strategy.strategy_id, STRATEGYRUN_LIVE, None, 0,perf.id,serial.id,lrconfig.id)
                session.add(run)
                session.commit()
                strategyrun_id = run.id
                perf_id = perf.id                                                 
            else:
                self.log.info('Liverunconfig %d found for strategy %d' % (lrconfig.id, strategy.strategy_id))
                strategyrun_id, perf_id = session.query(db.StrategyRun.id, db.StrategyRun.performanceIdCurrency).filter(db.StrategyRun.configId == lrconfig.id).filter(db.StrategyRun.type == STRATEGYRUN_LIVE).first()
            
            self.log.info('StrategyRunID: %d' % strategyrun_id)
            self.strategies[strategy.strategy_id]['lrconfig_id'] = lrconfig.id
            self.strategies[strategy.strategy_id]['strategyrun_id'] = strategyrun_id
            self.strategies[strategy.strategy_id]['perf_id'] = perf_id
            self.strategies[strategy.strategy_id]['exposure'] = 0
            self.strategies[strategy.strategy_id]['floating'] = 0
            
            #performance.server.calculate_strategy(strategyrun_id)
            self.config_mm(strategy.strategy_id, strategy.symbol, strategy.mmplugin_id, strategy.mmplugin_parameters)                        
        except:
            self.log.critical(traceback.format_exc())
                                        
                                        
    def remove_strategy(self, strategy_id):
        try:     
            self.log.info('removing strategy: %d' % (strategy_id))
            
            status_id = LIVERUN_STATUS_STOPPED if self.strategies[strategy_id]['object'].is_stopped() else LIVERUN_STATUS_ABORTED
            self._update_lrconfig_stop(strategy_id, status_id)
                    
            self.strategies.pop(strategy_id, None)
            self.mm_plugins.pop(strategy_id, None)
        except:
            self.log.critical(traceback.format_exc())


    def _remove_trade(self, trade):
        try:
            del self.open_trades[trade.strategy_id][trade.id]            
        except:
            pass
        try:
            del self.trades[trade.id]
        except:
            pass
        
        self.open_trades_num -= 1        
                
        self.accountmanager.on_trade_close(trade.strategy_id)


    def _update_lrconfigs_onexit(self):
        for strategy_id in self.strategies.keys()[:]:
            try:
                if self.strategies[strategy_id]['object'].is_stopped():                    
                    status_id = LIVERUN_STATUS_STOPPED
                elif self.strategies[strategy_id]['object'].is_aborted():                    
                    status_id = LIVERUN_STATUS_ABORTED
                else:
                    status_id = LIVERUN_STATUS_STOPPED if self.state == STOPPED else LIVERUN_STATUS_ABORTED
                    
                self._update_lrconfig_stop(strategy_id, status_id)
            except:
                self.log.error(traceback.format_exc())
        
    
    def _update_lrconfig_stop(self, strategy_id, status_id):
        table = db.LiveRunConfig.__table__
        try:
            command = table.update().\
                        where(table.c.id == self.strategies[strategy_id]['lrconfig_id']).\
                        values(running_to = datetime.utcnow(), status_id = status_id, status_message=LIVERUN_STATUS_STR[status_id])
            sql.server.update(command)
        except:
            self.log.error(traceback.format_exc())
    
    
    def _update_lrconfig_runningto(self, strategy_id):
        table = db.LiveRunConfig.__table__
        try:
            command = table.update().\
                        where(table.c.id == self.strategies[strategy_id]['lrconfig_id']).\
                        values(running_to = datetime.utcnow())
            sql.server.update(command)
        except:
            self.log.error(traceback.format_exc())

            
    def _update_state(self, state):
        try:
            self.log.info('%s -> %s' % (self.state, state))
            self.state = state
            table = db.PortfolioAccountRun.__table__
            command = table.update().where(table.c.id == self.accountrun_id).values(state=self.state)
            sql.server.update(command)
        except:
            self.log.critical(traceback.format_exc())        
    
        
    def _update_connection_state(self):
        try:
            connection_state = self.fix_account.get_connection_state()
            if self.connection_state != connection_state:
                self.log.info('%s -> %s' % (CONNECTION_STATE[self.connection_state], CONNECTION_STATE[connection_state]))
                self.connection_state = connection_state
                table = db.PortfolioAccountRun.__table__
                command = table.update().where(table.c.id == self.accountrun_id).values(connection_state=CONNECTION_STATE[connection_state])
                sql.server.update(command)
        except:
            self.log.critical(traceback.format_exc())
        
            
    def _update_running(self):
        try:
            table = db.PortfolioAccountRun.__table__
            command = table.update().where(table.c.id == self.accountrun_id).values(running_from = self.running_from, running_to = self.running_to)
            sql.server.update(command)            
        except:
            self.log.critical(traceback.format_exc())
            
    
    def _update_trade_exposure(self, trade):
        try:            
                                                            
            table = db.AccountExposure.__table__                                    
            command = table.insert().values(account_id = self.account_id,
                                            accountrun_id = self.accountrun_id,
                                            time = datetime.utcnow(),
                                            symbol = trade.symbol,
                                            exposure = self.exposure[trade.symbol])
            sql.server.insert(command)            
            
            table = db.StrategyExposure.__table__                        
            command = table.insert().values(strategy_id = trade.strategy_id,
                                            strategyrun_id = trade.strategyrun_id, 
                                            time = datetime.utcnow(),
                                            symbol = trade.symbol,
                                            exposure = self.strategies[trade.strategy_id]['exposure'])
             
        except:
            self.log.critical(traceback.format_exc())
    
    
    def _update_account_exposure(self):
        try:
            self._recalc_exposures()
            
            table = db.AccountExposure.__table__
            for symbol in self.exposure:                                    
                command = table.insert().values(account_id = self.account_id,
                                                accountrun_id = self.accountrun_id,
                                                time = datetime.utcnow(),
                                                symbol = symbol,
                                                exposure = self.exposure[symbol])
                sql.server.insert(command)
        except:
            self.log.critical(traceback.format_exc())
    


    def _recalc_exposures(self):        
        for symbol in self.exposure:
                self.exposure[symbol] = 0
                
        for strategy_id in self.strategies:
            try:             
                self.strategies[strategy_id]['exposure'] = 0
                for trade in self.open_trades[strategy_id].values():                                        
                    self.exposure[trade.symbol] += trade.get_exposure()
                    self.strategies[strategy_id]['exposure'] += trade.get_exposure()
            except:
                pass
            
                
    def before_evaluate(self, timeframe):        
        try:
            #self.fix_account.check_connection()
            
            if not timeframe:
                return 
            self._current_timeframe = timeframe
                                                        
            for strategy_id in self.strategies:
                symbol = self.strategies[strategy_id]['object'].symbol
                for price in [PRICE_ASKOPEN, PRICE_ASKHIGH, PRICE_ASKLOW, PRICE_ASKCLOSE, PRICE_BIDOPEN, PRICE_BIDHIGH, PRICE_BIDLOW, PRICE_BIDCLOSE, PRICE_PIPVALUE]:        
                    self.mm_plugins[strategy_id].setDatarow(0, price, historic.DATAROWS[timeframe][symbol][price])
                        
                self.mm_plugins[strategy_id].initPeakThrough(len(historic.DATAROWS[timeframe][symbol][PRICE_TIME]))
        except:
            self.log.critical(traceback.format_exc())
    
    
    def after_evaluate(self, timeframe):
        self.command_queue.put(("after_evaluate", (timeframe,)))
        #self.fix.account.shutdown()        
           
            
    def _after_evaluate(self, timeframe):
        try:
            self.running_to = historic.CURRENT_CANDLE_TIME
            if not self.running_from:
                self.running_from = self.running_to        
            
            self._update_running()
            
            for strategy_id in self.strategies.keys():
                self._update_lrconfig_runningto(strategy_id)
            
            self.fix_account.after_evaluate(timeframe)
        except:
            self.log.critical(traceback.format_exc())
    
    
    def open(self, symbol, direction, percent, group_id, strategy_id):
        self.command_queue.put(("open", (symbol, direction, percent, group_id, strategy_id)))
        

    def _open(self, symbol, direction, percent=1.0, group_id=0, strategy_id=0):
        
        if not self.open_allowed:
            return 
        
        # DO NOT REMOVE THIS ! 
        if strategy_id not in self.strategies:
            return 
        
        self.log.info('Strategy %d, OPEN ' % (strategy_id))    
                
#            try:
#                max_trade_num = self.strategies[strategy_id]['object'].mmplugin_parameters['maxnumoftrades']
#                if len(self.open_trades[strategy_id]) >= max_trade_num:
#                    self.log.info('Strategy %d: open rejected, no more trades allowed (max: %d)' % (strategy_id, max_trade_num))
#                    return
#            except:
#                self.log.critical(traceback.format_exc()) 
        
        
        #self._read_base_equity()
        
        #self.log.debug('Strategy %d, group %d : %s %s' % (strategy_id, group_id, DIR_STR[direction], symbol))
                                                    
        ask = float(historic.DATAROWS[self._current_timeframe][symbol][PRICE_ASKCLOSE][-1])
        bid = float(historic.DATAROWS[self._current_timeframe][symbol][PRICE_BIDCLOSE][-1])
        
        if direction == DIR_BUY:
            diff = abs(ask - historic.CURRENT_PRICE[symbol]['ASK']) / PIP_MULTIPLIER[symbol] 
            #self.log.debug('Strategy %d, group %d : slippage = %.1f' % (strategy_id, group_id, diff))
            if diff >= config.MAXIMUM_OPEN_SLIPPAGE:
                self.log.warning('Slippage is too large, skip opening')
                return False
                
        else:
            diff = abs(bid - historic.CURRENT_PRICE[symbol]['BID']) / PIP_MULTIPLIER[symbol] 
            #self.log.debug('Strategy %d, group %d : slippage = %.1f' % (strategy_id, group_id, diff))
            if diff >= config.MAXIMUM_OPEN_SLIPPAGE:
                self.log.warning('Slippage is too large, skip opening')
                return False
                            
                
        open_price = ask if direction == DIR_BUY else bid
        datarow_length = len(historic.DATAROWS[self._current_timeframe][symbol][PRICE_TIME])
        net_profit = self.balance - self.staring_balance
        
        #self.log.debug('MM parameters: datarow len: %d, %d, profit: %.1f direction: %s ask: %.5f, bid: %.5f, percent: %.2f' % (self.symbols.index(symbol), datarow_length-1, net_profit, DIR_STR[direction], ask, bid, percent))
        
        sl, tp, amount = self.mm_plugins[strategy_id].calc_amount(0, datarow_length-1, net_profit, direction, ask, bid, percent)        
        sl = round(sl, len(str(PIP_MULTIPLIER[symbol]))-1)
        tp = round(tp, len(str(PIP_MULTIPLIER[symbol]))-1)
                        
        #self.log.debug('MM Plugin results: SL: %.5f TP: %.5f,  Amount: %.4f' % (sl, tp, amount))                        
        if not amount:             
            self.log.warning('Trade rejected by MM! ')            
            return False            
            #return None, "Rejected by MM: %s" % reason
        if amount < 1000:
            amount = 1000
                
        trade = self.fix_account.TradeClass(symbol, direction, datetime.utcnow(), open_price, amount, sl, tp, 
                      group_id, strategy_id, self.strategies[strategy_id]['strategyrun_id'], self.account_id, self.accountrun_id, self.portfolio_id, self.fix_account)
        
        trade.save()                            
        self.open_trades[strategy_id][trade.id] = trade
        self.trades[trade.id] = trade
        self.open_trades_num += 1                                     
        #self.log.info(' **** TRADE CREATED: %s ****' % trade)        
            
        trade.send_open()                                                                                                 

    
    def openOrHold(self, symbol, direction, percent, group_id, strategy_id, canopen):
        self.command_queue.put(("openOrHold", (symbol, direction, percent, group_id, strategy_id, canopen)))
        

    def _openOrHold(self, symbol, direction, percent=1.0, group_id=0, strategy_id=0, canopen=True):
        #self.log.info('strategy %d %s - %s canopen: %s' % (strategy_id, symbol, DIR_STR[direction], canopen))
        
        if len(self.open_trades[strategy_id]):
            trade = self.open_trades[strategy_id].values()[0]
            if trade.direction == direction:
                if canopen:
                    #self.log.info('strategy %d has a trade in the same direction, skip opening (%s == %s)' % (strategy_id, DIR_STR[direction], DIR_STR[trade.direction]))
                    #self.log.info('%s' % self.open_trades[strategy_id])
                    pass
                else:
                    pass                
            else:
                #self.log.info('strategy %d has a trade in the opposite direction, closing trade (%s != %s)'% (strategy_id, DIR_STR[direction], DIR_STR[trade.direction]))
                #self.log.info('%s' % self.open_trades[strategy_id])
                trade.send_close()
                if canopen:
                    #self.log.info('strategy %d opening a new trade' % strategy_id)
                    self._open(symbol, direction, percent, group_id, strategy_id)
                else:
                    pass
                    #self.log.info('strategy %d opening a new trade is not allowed' % strategy_id)
        else:
            if canopen:
                #self.log.info('strategy %d has no open trade, opening one' % strategy_id)
                self._open(symbol, direction, percent, group_id, strategy_id)
            else:
                pass
                #self.log.info('strategy %d has no open trade, and opening a new one is not allowed' % strategy_id)
                                
    
    def closeall(self, group, strategy_id):
        self.command_queue.put(("closeall", (group, strategy_id)))
          
    
    def _closeall(self, group, strategy_id):
        try:                                    
            for trade in self.open_trades[strategy_id].values():
                if trade.state==TRADE_OPENED and (group == -1 or group == trade.group_id):
                    trade.send_close()                        
        except:                        
            self.log.error(traceback.format_exc())
        
    
    def closelast(self, group, strategy_id):
        self.command_queue.put(("closelast", (group, strategy_id)))   
        
        
    def _closelast(self, group, strategy_id):
        try:             
            for trade_id in reversed(self.open_trades[strategy_id].keys()):
                trade = self.open_trades[strategy_id][trade_id]                
                if trade.group_id == group and trade.state==TRADE_OPENED:            
                    trade.send_close()                    
        except:
            self.log.error(traceback.format_exc())      
    
    
    def closefirst(self, group, strategy_id):
        self.command_queue.put(("closefirst", (group, strategy_id)))
        
        
    def _closefirst(self, group, strategy_id):
        try:                    
            for trade in self.open_trades[strategy_id].values():                
                if trade.group_id == group and trade.state==TRADE_OPENED:                    
                    trade.send_close()                                                                               
        except:
            self.log.error(traceback.format_exc())    
    
            
    
    
    def has_open_trades(self, strategy_id):
        try:
            num = len(self.open_trades[strategy_id])
            self.log.info('strategy %d has %d open trades' % (strategy_id, num))
            return num
        except:
            return 0
       
        
    def _update_performance(self):
        try:
            self.floating_profit = 0
            self.open_trades_num = 0    
            for symbol in self.exposure:
                self.exposure[symbol] = 0
                
            for strategy_id in self.strategies:                                
                floating = 0
                open_trades = 0
                self.strategies[strategy_id]['exposure'] = 0
                                                
                for trade in self.open_trades[strategy_id].values():                    
                    if trade.state < TRADE_CLOSED:
                        trade.price_update(historic.CURRENT_PRICE[trade.symbol]['ASK'], historic.CURRENT_PRICE[trade.symbol]['BID'])
                        trade.save_performance()
                        
                        floating += trade.floating_profit
                        open_trades += 1
                        self.exposure[trade.symbol] += trade.get_exposure()
                        self.strategies[strategy_id]['exposure'] += trade.get_exposure()
                         
                self.strategies[strategy_id]['floating'] = floating
                #TODO: mentes az lrconfigba strategiankent ha kell                                            
                self.floating_profit += floating
                self.open_trades_num += open_trades
            
            self.min_floating = min(self.min_floating, self.floating_profit)
            self._save_performance()
        except:
            self.log.critical(traceback.format_exc())
            

    def _save_performance(self):
        table = db.AccountPerformance.__table__                        
        try:
#            self.log.debug('saving account performance - start')

            command = table.insert().values(
                                               account_id = self.account_id,
                                               accountrun_id = self.accountrun_id,
                                               timestamp = str(datetime.utcnow()),
                                               equity = self.balance + self.floating_profit,
                                               balance = self.balance,
                                               floating =  self.floating_profit,
                                               trade_num = self.open_trades_num,
                                               leverage = self.leverage,
                                               use_of_leverage = self.use_of_leverage,
                                               usable_margin =  self.usable_margin
                                               )
            sql.server.insert(command)
            
            
            table = db.PerformanceData.__table__
            for strategy_id in self.strategies.keys():
                command = table.update().where(table.c.id == self.strategies[strategy_id]['perf_id']).values(open_trades=len(self.open_trades[strategy_id]), floating_profit = self.strategies[strategy_id]['floating'], exposure = self.strategies[strategy_id]['exposure'])
                sql.server.update(command)
            
            command = table.update().where(table.c.id == self.performance_id).values(open_trades=self.open_trades_num, floating_profit = self.floating_profit, min_floating=self.min_floating)
            sql.server.update(command)
            
                
#            self.log.debug('saving account performance - end')
        except:
            pass


    
#
#    def _read_base_equity(self):
#        try:
#            table = db.Account.__table__
#            result = db.engine.execute(select([table.c.base_equity], table.c.id == self.account_id))
#            row = result.fetchone()
#            if row and self.base_equity != row[0]:       
#                balance =  self.balance + (row[0] - self.base_equity)                
#                base_eq = row[0]            
#                self.log.info('base equity: %d -> %d, balance: %d -> %d ' % (self.base_equity, base_eq, self.balance, balance))
#                self.base_equity = base_eq
#                self.balance = balance
#                for mm_plugin in self.mm_plugins.itervalues():
#                    mm_plugin.setBaseEquity(self.base_equity)
#        except:
#            self.log.critical(traceback.format_exc())
#            
         
            
        
#    def _read_balance(self):
#        try:
#            table = db.AccountPerformance.__table__
#            result = db.engine.execute(select([table.c.balance], table.c.account_id == self.account_id).order_by(table.c.id.desc()))
#            row = result.fetchone()
#            while row:                
#                if not row[0]:
#                    row = result.fetchone()
#                    continue 
#                self.balance = row[0]
#                break
#                #self.log.info('Balance read from db: %f' % (self.balance))            
#        except:
#            self.log.error(traceback.format_exc())
#        
    
    def get_engine_details(self):
        return self.fix_account.get_engine_details()
    
    def get_engine_logs(self, logtype):
        return self.fix_account.get_engine_logs(logtype)
    
    def get_engine_last_messages(self):
        return self.fix_account.get_engine_last_messages()
    
    def shutdown(self):
        self.fix_account.shutdown()
    
    def check_connection(self):
        self.fix_account.check_connection()
        return True, 'Connection state is: %s' % CONNECTION_STATE[self.fix_account.get_connection_state()] 

    def get_account_logs(self, logtype):         
        result = ''    
        LTYPE = {0: 'DEBUG', 1:'INFO',2:'WARNING'}                 
        try:
            logfile = '/gdtlive.core.portfolio.%s.account.%s.%s.log' % (self.accountmanager.portfolio.name, self.account_name, LTYPE[logtype])
            with open(config.LOG_PATH + logfile) as f:
                r = f.readlines()                        
                r = r[-20:]
                for line in r:
                    try:
                        d, level, name, message = line.split(' - ', 3)
                        result += d + ' - ' + level + ' - ' + message
                    except:
                        if line.strip():
                            result += line.strip()
                        pass
                #result = ''.join(r) 
        except:            
            print traceback.format_exc()
            
        return result
    
    def get_trade_data(self, trade_id): 
        try:       
            if trade_id in self.trades:
                return self.trades[trade_id].get_all_data()
        except:
            return {}
            
        
    def get_trades(self):
        result = []
        for trades in self.open_trades.values():
            for trade in trades.values():
                result.append(trade.get_all_data())
        return result    
    
    def get_data(self):
        return {'name': self.account_name, 
                'state': self.state, 
                'exposure':self.exposure,
                'connection_state': self.fix_account.get_connection_state(),
                'floating_profit': self.floating_profit,
                'open_trades': self.open_trades_num,
                'benchmark' :  self.benchmark
                }
    
    
    def reload_settings(self):
        self.log.info('Reloading settings')
        try:
            table = db.Account.__table__
            result = db.engine.execute(select([table.c.risk_multiplier, table.c.base_equity, table.c.leverage], table.c.id == self.account_id))
            row = result.fetchone()
            self.risk_multiplier = row[0]
            self.base_equity = row[1]
            self.leverage = row[2]
            self.log.info('Risk multiplier is set to %f' % self.risk_multiplier)
            self.log.info('Base equity is set to %f' % self.base_equity)
            self.log.info('Leverage is set to %f' % self.leverage)
            self._reconfigure_mm()
        except:
            self.log.error(traceback.format_exc())
    
    
    def trade_repair(self, trade_id):
        try:            
            if trade_id in self.trades:
                self.log.info('TRADE-%d: resend_failed_orderd initiated by user ' % trade_id)
                self.trades[trade_id].resend_failed_orders()            
        except:
            self.log.critical(traceback.format_exc())
        
        return True

        
    def trade_close(self, trade_id):
        try:
            #print 'trade close', self.account_name, trade_id, self.trades.keys()                        
            if trade_id in self.trades:
                self.log.info('TRADE-%d: close initiated by user' % trade_id)
                self.trades[trade_id].send_close()
        except:
            self.log.critical(traceback.format_exc())
            
        return True

                                
    def trade_markasclosed(self, trade_id):
        try:            
            if trade_id in self.trades:
                self.log.info('TRADE-%d: mark as closed initiated by user' % trade_id)
                self.trades[trade_id].mark_closed()
                self.on_trade_close(self.trades[trade_id])
        except:
            self.log.critical(traceback.format_exc())
            
        return True
    
    
    def trade_mo_sendopen(self, trade_id):
        try:            
            if trade_id in self.trades:
                self.log.info('TRADE-%d: marketorder_open initiated by user' % trade_id)
                self.trades[trade_id].marketorder_open()
        except:
            self.log.critical(traceback.format_exc())            
        return True
    
    
    def trade_mo_sendclose(self, trade_id):
        try:            
            if trade_id in self.trades:
                self.log.info('TRADE-%d: marketorder_close initiated by user' % trade_id)
                self.trades[trade_id].marketorder_close()
        except:
            self.log.critical(traceback.format_exc())            
        return True
         
    
    def trade_co_place(self, trade_id, co_type):
        try:            
            if trade_id in self.trades:                
                if co_type == 0:
                    self.log.info('TRADE-%d: stoploss place initiated by user' % trade_id)
                    self.trades[trade_id].stoploss_place()
                elif co_type == 1:
                    self.log.info('TRADE-%d: takeprofit place initiated by user' % trade_id)
                    self.trades[trade_id].takeprofit_place()                
        except:
            self.log.critical(traceback.format_exc())            
        return True
            
     
    def trade_co_cancel(self, trade_id, co_type):
        try:                        
            if trade_id in self.trades:
                if co_type == 0:
                    self.log.info('TRADE-%d: stoploss cancel initiated by user' % trade_id)
                    self.trades[trade_id].stoploss_cancel()
                elif co_type == 1:
                    self.log.info('TRADE-%d: takeprofit cancel initiated by user' % trade_id)
                    self.trades[trade_id].takeprofit_cancel()
        except:
            self.log.critical(traceback.format_exc())            
        return True
    
    
    def trade_co_filled(self, trade_id, co_type):
        try:            
            if trade_id in self.trades:
                if co_type == 0:
                    self.log.info('TRADE-%d: stoploss fill initiated by user' % trade_id)
                    self.trades[trade_id].stoploss_fill()
                elif co_type == 1:
                    self.log.info('TRADE-%d: takeprofit fill initiated by user' % trade_id)
                    self.trades[trade_id].takeprofit_fill()
        except:
            self.log.critical(traceback.format_exc())            
        return True
    