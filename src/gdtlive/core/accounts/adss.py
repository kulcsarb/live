# -*- encoding: utf-8 -*- 
'''
Created on Nov 27, 2012

@author: gdt
'''

import gdtlive.core.fix.dukascopy.fix as fix
from gdtlive.core.accounts.fixaccountbase import FIXAccountBase, FIXAccountSyncBase
from gdtlive.core.constants import *
from gdtlive.constants import * 
import gdtlive.config as config

from gdtlive.core.trade import CONDORDER_CANCEL_SENT, CONDORDER_CANCEL_FAILED, CONDORDER_CANCEL_REJECTED, \
    CONDORDER_PENDING_NEW, CONDORDER_UNKNOWN, CONDORDER_ENTRY_SENT, CONDORDER_UNKNOWN_CLOSED, \
    MARKETORDER_UNKNOWN_CLOSED

from datetime import datetime     
import traceback
from sqlalchemy.sql import select



class ADSSAccount(FIXAccountBase):
    
    broker_id = 2                                                

            
    def place_stoploss(self, trade, entry_clordid, symbol, direction, amount, price, entry_sent_time, resend=False):
        return self._place_conditionalorder(trade, entry_clordid, symbol, direction, amount, price, entry_sent_time, fix.OrdType.STOP, resend)
        
        
    def place_takeprofit(self, trade, entry_clordid, symbol, direction, amount, price, entry_sent_time, resend=False):
        return self._place_conditionalorder(trade, entry_clordid, symbol, direction, amount, price, entry_sent_time, fix.OrdType.LIMIT, resend)
            
        
    def _place_conditionalorder(self, trade, entry_clordid, symbol, direction, amount, price, entry_sent_time, order_type, resend):                
        
        self.register_order(trade, entry_clordid)
        
        message = fix.NewOrderSingle(ClOrdID = entry_clordid, 
                                            Currency = symbol[:3],
                                            Symbol = symbol, 
                                            Side = {DIR_BUY: fix.Side.BUY, DIR_SELL: fix.Side.SELL}[direction], 
                                            OrderQty = amount,
                                            Price = price,
                                            TransactTime = entry_sent_time,
                                            OrdType = order_type,
                                            TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL,
                                            DiscretionOffsetValue = config.CONDITIONALORDER_SLIPPAGE,
                                            PossResend = resend
                                            )                   

        return self.send_message(trade.id, message)
    
        
    def cancel_order(self, trade, symbol, direction, entry_orderid, entry_clordid, cancel_clordid, resend=False):
        
        self.register_order(trade, cancel_clordid)
        
        message = fix.OrderCancelRequest( 
                                      #OrderID = entry_orderid,
                                      ClOrdID = cancel_clordid,
                                      OrigClOrdID = entry_clordid,
                                      Account = self.account_number, 
                                      Side = {DIR_BUY: fix.Side.BUY, DIR_SELL: fix.Side.SELL}[direction],                                       
                                      TransactTime = datetime.utcnow(),
                                      Symbol = symbol,
                                      PossResend = resend                                      
                                      )
                
        
        return self.send_message(trade.id, message)
    
        
    
    def send_marketorder(self, trade, clordid, direction, amount, symbol, resend=False):                
        
        self.register_order(trade, clordid)
        
        message = fix.NewOrderSingle(ClOrdID = clordid, 
                                            Currency=symbol[:3],
                                            Symbol = symbol, 
                                            Side = {DIR_BUY: fix.Side.BUY, DIR_SELL: fix.Side.SELL}[direction], 
                                            OrderQty = amount,
                                            TransactTime = datetime.utcnow(),
                                            OrdType = fix.OrdType.MARKET,
                                            TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL,
                                            PossResend = resend
                                            )           
                    
        return self.send_message(trade.id, message)        
        


    def onMessage(self, message):
        self.log.debug('RECEIVED: %s' % message)
        
        if type(message) == fix.ExecutionReport:
            if message.ClOrdID in self.trades:                
                trade = self.trades[message.ClOrdID]      
                
                self._save_message(trade.id, FIX_INCOMING,  message)
                                          
                previous_state = trade.state                                
                try:
                    if message.OrdStatus == fix.OrdStatus.NEW:
                        trade.handle_pending_new(message.ClOrdID, message.OrderID, message.Price)
                    elif message.OrdStatus == fix.OrdStatus.FILLED:
                        trade.handle_filled(message.ClOrdID, message.CumQty, message.AvgPx, message.SendingTime)
                    elif message.OrdStatus == fix.OrdStatus.CANCELED:                        
                        trade.handle_canceled(message.ClOrdID, message.SendingTime)                    
                    elif message.OrdStatus == fix.OrdStatus.REJECTED:
                        trade.handle_rejected(message.ClOrdID, message)
                                            
                except:
                    self.log.error(traceback.format_exc())
                
                try:                                                                                                                     
                    if trade.state == TRADE_CLOSED: 
                        self.businessaccount.on_trade_close(trade)                                                                                                                                                       
                        
                    if trade.state == TRADE_OPENED and previous_state != TRADE_OPENED:
                        self.businessaccount.on_trade_open(trade)                                                                                                                    
                    
                except:
                    self.log.error(traceback.format_exc())
                    
            else:                
                self.log.warning('Trade not found for message: %s' % str(message))
                self.log.warning('ClOrdID: %s' % message.ClOrdID)
                self.log.warning('trades: %s' % self.trades.keys())
            
            
    def _synchronize_trades(self):
        sync = ADSSSync(self)
        sync.start()
        sync.join()               



class ADSSSync(FIXAccountSyncBase):

    
    def start_sync(self):        
        for trade in self.account.trades.values():
            if trade.stoploss.state in [CONDORDER_CANCEL_SENT, CONDORDER_CANCEL_FAILED, CONDORDER_CANCEL_REJECTED, CONDORDER_PENDING_NEW, CONDORDER_ENTRY_SENT]:
                trade.stoploss.state = CONDORDER_UNKNOWN
                self.account.send_message(fix.OrderStatusRequest(Symbol=trade.symbol[:3]+trade.symbol[4:], ClOrdID=trade.stoploss.entry_clordid, Side={DIR_BUY: fix.Side.SELL, DIR_SELL:fix.Side.BUY}[trade.direction]))
                self.unknown_count += 1
            
            if trade.takeprofit.state in [CONDORDER_CANCEL_SENT, CONDORDER_CANCEL_FAILED, CONDORDER_CANCEL_REJECTED, CONDORDER_PENDING_NEW, CONDORDER_ENTRY_SENT]:
                self.account.send_message(fix.OrderStatusRequest(Symbol=trade.symbol[:3]+trade.symbol[4:], ClOrdID=trade.takeprofit.entry_clordid, Side={DIR_BUY: fix.Side.SELL, DIR_SELL:fix.Side.BUY}[trade.direction]))
                trade.takeprofit.state = CONDORDER_UNKNOWN
                self.unknown_count += 1
        

