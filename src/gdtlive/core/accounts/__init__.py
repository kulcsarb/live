# -*- encoding: utf-8 -*- 

from benchmark import BenchmarkAccount
from dukascopy import DukascopyAccount
from dukascopy_aggregate import DukascopyAggregateAccount
from adss import ADSSAccount
from dukascopy_jforex import JForexAccount


fix_accounts = {
                 BenchmarkAccount.broker_id : BenchmarkAccount,
                 DukascopyAccount.broker_id : DukascopyAccount,                 
                 ADSSAccount.broker_id : ADSSAccount,
                 JForexAccount.broker_id : JForexAccount,
                 DukascopyAggregateAccount.broker_id : DukascopyAggregateAccount,
                 }

