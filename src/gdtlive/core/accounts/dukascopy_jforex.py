'''
Created on Oct 17, 2013

@author: gdt
'''
from benchmark import BenchmarkTrade, BenchmarkAccount, NullTrade
import os
import traceback
import gdtlive.config as config
import subprocess
from multiprocessing import Process
import time
import logging


class JForexAccount(BenchmarkAccount):
    
    broker_id = 3    
    TradeClass = NullTrade
    
    def __init__(self, account, accountinfo):
        BenchmarkAccount.__init__(self, account, accountinfo)
        self.transfer_parameters = ''
        self.max_restart_count = 10
        
    
    def after_evaluate(self, timeframe):
        positions = {}
        for symbol in self.businessaccount.symbols:
            positions[symbol] = 0
            
        position_size = 0
        for strategy_id in self.businessaccount.open_trades:
            for trade in self.businessaccount.open_trades[strategy_id].values():
                if not position_size:
                    position_size = abs(trade.get_exposure())                                   
                positions[trade.symbol] += trade.get_exposure()
        
        
        self.log.info('AFTER EVALUATE:' + str(positions))    
            
        transfer_process = JForexTransfer(self.username, self.password, positions)
        transfer_process.log = self.log
        transfer_process.run()
        
        

class JForexTransfer(Process):
    
    def __init__(self, username, password, positions):
        Process.__init__(self)
        self.username = username
        self.password = password
        args = []
        for symbol, exposure in positions.items():
            if exposure > 0:
                pos = "%s_%s_%d" % (symbol.upper(), 'LONG', exposure)
            elif exposure < 0:
                pos = "%s_%s_%d" % (symbol.upper(), 'SHORT', abs(exposure))
            else:
                pos = "%s_%s" % (symbol.upper(), 'CLOSE')
            args.append(pos)
                    
        self.transfer_parameters = ' '.join(args)                
        self.max_restart_count = 10
        self.restart_count = 0        
        self.start_timeout = 30
        self.wait_timeout = 60
        self.elapsed = 0
        self.started = False
        self.retcode = None
        self.log = logging
        
            
    def run(self):
        try:                     
            self.log.info('--- starting transfer --- ')   
            self.log.info('params: ' + self.transfer_parameters)
            self.start_transfer()
            while self.retcode is None:            
                self.retcode = self.process.poll()
                
                time.sleep(1)
                self.elapsed += 1
                self.log.info('checking transfer... %d, ' % self.elapsed)
                self.check_if_started()                
                
                if self.elapsed >= self.start_timeout and not self.started:                                                    
                    if not self.restart_transfer():
                        return False
                if self.elapsed >= self.wait_timeout and self.started:
                    if not self.restart_transfer():
                        return False
                                        
        except:
            self.log.error(traceback.format_exc())
            
        self.tempfile.seek(0, os.SEEK_SET)
        self.log.info('output:' + self.tempfile.read())
        self.tempfile.close()
        self.log.info('--- transfer finished ---')
        
    
    def check_if_started(self):        
        self.tempfile.seek(0, os.SEEK_SET)
        output = self.tempfile.read()            
        if 'connected' in output and not self.started:
            self.started = True
            self.log.info('transfer started')
            
    def start_transfer(self):
        self.started = False
        self.tempfile = os.tmpfile()        
        self.process = subprocess.Popen('%s %s %s %s' % (config.TRANSFER_JAR, self.username, self.password, self.transfer_parameters), stdout=self.tempfile, shell=True)        


    def stop_transfer(self):
        try:
            self.process.kill()
        except:
            self.log.error(traceback.format_exc())
    
    
    def restart_transfer(self):
        if self.restart_count < self.max_restart_count:
            self.log.info('restarting transfer, retries left: %d' % (self.max_restart_count - self.restart_count))
            if self.retcode is None:              
                self.stop_transfer()
            self.start_transfer()
            self.restart_count += 1
            self.elapsed = 0
            return True    
        else:
            self.log.info('maximum restart count reached, giving up...')
            self.log.error('Unable to transfer orders to dukascopy! ')
            return False
        
        
if __name__ == '__main__':    
    positions = {'EURUSD' : 0}
    logging.getLogger().setLevel(logging.DEBUG)    
    #transfer_process = JForexTransfer('DEMO3NxKxd', 'NxKxd', positions)
    transfer_process = JForexTransfer('Schoepf64EU', 'NihilInMachine', positions)
    transfer_process.start()
    transfer_process.join()
    
    