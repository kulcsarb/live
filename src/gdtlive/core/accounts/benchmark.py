'''
Created on Nov 29, 2012

@author: gdt
'''
from fixaccountbase import FIXAccountBase
import gdtlive.core.datafeed.historic as historic
from datetime import datetime
import gdtlive.core.sqlupdater as sql
from gdtlive.core.constants import *
from gdtlive.constants import * 
import gdtlive.store.db as db
import traceback
import copy



class BenchmarkTrade(object):
    
    
    def __init__(self, symbol=None, direction=None, open_time=None, price=None, amount=None, sl_price=None, tp_price=None, group_id=None, strategy_id=None, strategyrun_id=None, account_id=None, accountrun_id=None, portfolio_id=None, fix_account=None):    
        self.id = 0    
        self.account_id = account_id
        self.accountrun_id = accountrun_id
        self.portfolio_id = portfolio_id
        self.strategy_id = strategy_id
        self.strategyrun_id = strategyrun_id
        self.group_id = group_id                
        self.state = TRADE_CREATED        
        self.symbol = symbol
        self.direction = direction
        self.open_time = open_time
        self.req_price = price
        self.open_price = price
        self.amount = amount
        self.sl_price = sl_price
        self.tp_price = tp_price        
        self.close_price = 0.0
        self.close_time = None
        self.close_type = 0
        self.floating_profit = 0.0
        self.profit = 0.0                    
        self.MAE = 0.0
        self.MFE = 0.0
        self.fix_account = fix_account
        self.log = fix_account.log
    
    
    def get_data(self):
        return {'id': self.id, 'strategy_id':self.strategy_id, 'state': self.state, 'symbol': self.symbol, 'direction': self.direction, 'amount': self.amount, 'open_time': self.open_time, 'open_price': self.open_price, 'sl_price': self.sl_price, 'tp_price':self.tp_price, 'floating_profit': self.floating_profit, 'MAE': self.MAE, 'MFE': self.MFE}


    def get_all_data(self):
        return self.get_data()
    

    def load(self, trade_id):
        trade_table = db.Trade.__table__
        result = db.engine.execute(trade_table.select(whereclause=trade_table.c.id==trade_id))
        r = result.fetchone()
        for k,v in r.items():
            setattr(self, k, v)
        
        self.fix_account.register_trade(self)


    def save(self):
        trade_table = db.Trade.__table__            
        
        if not self.id:                                                            
            data = copy.copy(self.__dict__)            
            for k in data.keys():                
                if k in ['id','log','fix_account']:
                    del data[k]
                if k[0] == '_':
                    del data[k]
                                
            command = trade_table.insert().values(**data).returning(trade_table.c.id)
            self.id = sql.server.insert(command)                                                                                                    
        else:
            try:
                #self.log.debug('saving trade: %s' % self)
                command = trade_table.update().\
                                  where(trade_table.c.id==self.id).\
                                  values(state=self.state, open_price=self.open_price, floating_profit=self.floating_profit, profit=self.profit, MAE=self.MAE, MFE=self.MFE, 
                                         close_price=self.close_price, close_time=self.close_time, close_type=self.close_type)
                 
                sql.server.update(command)                                                                                  
            except:
                self.log.error(traceback.format_exc())    


    def save_performance(self):
        try:
            trade_table = db.Trade.__table__ 
            #self.log.debug('saving trade performance: %s' % self)
            command = trade_table.update().\
                              where(trade_table.c.id==self.id).\
                              values(floating_profit=self.floating_profit, profit=self.profit, MAE=self.MAE, MFE=self.MFE)
             
            sql.server.update(command)                                                                                  
        except:
            self.log.error(traceback.format_exc())
        
        
    def __str__(self):
        sl = ''
        tp = ''
        if self.sl_price:
            sl = ' SL @ %.5f' % self.sl_price
        if self.tp_price:
            tp = ' TP @ %.5f' % self.tp_price        
        
        profit = self.profit or self.floating_profit
        
        return "Trade %s: %s %s %d @ %.5f close @ %.5f,%s%s profit: %.2f" % (self.id, self.symbol, DIR_STR[self.direction], self.amount, self.open_price, self.close_price, sl, tp, profit)
        
                        
    def send_open(self):
        self.state = TRADE_OPENED
        self.fix_account.on_trade_open(self)
        self.save()
        #self.log.info('**** TRADE OPENED: %s **** ' % (str(self)))                
        
        
    def is_opened(self):
        return self.state == TRADE_OPENED                            
                
                
    def send_close(self):    
        self.close(ORDERTYPE_CLOSE, historic.CURRENT_PRICE[self.symbol]['ASK' if self.direction == DIR_SELL else 'BID'])
        
    
    def sl_hit(self):
        self.close(ORDERTYPE_CLOSE_BY_SL, self.sl_price)
    
    
    def tp_hit(self):
        self.close(ORDERTYPE_CLOSE_BY_TP, self.tp_price)
        
        
    def close(self, close_type, price):
        self.state = TRADE_CLOSED
        self.close_price = price
        self.close_type = close_type
        self.close_time = historic.CURRENT_TIME
        self.price_update(self.close_price, self.close_price)                
        self.profit = self.floating_profit
        self.floating_profit = 0
        self.save()
        #self.log.info('**** TRADE CLOSED: %s **** ' % (str(self)))
        self.fix_account.on_trade_close(self)
    
    
    def mark_closed(self):
        self.close(ORDERTYPE_FORCE_CLOSE, historic.CURRENT_PRICE[self.symbol]['ASK' if self.direction==DIR_SELL else 'BID'])
        
    
    def price_update(self, ask, bid):
        try:
            if self.state >= TRADE_OPENED and self.state <= TRADE_CLOSED:
                            
                if self.direction == DIR_BUY:
                    pip = (bid - self.open_price) / PIP_MULTIPLIER[self.symbol] 
                else:
                    pip = (self.open_price - ask)  / PIP_MULTIPLIER[self.symbol]
                                                          
                self.floating_profit = pip * historic.CURRENT_PRICE[self.symbol]['PIPVALUE'] * self.amount
                
                self.MAE = min(self.MAE, self.floating_profit)
                self.MFE = max(self.MFE, self.floating_profit)
        except:
            self.log.error(traceback.format_exc())
            
        return self.floating_profit
    
    
    def get_exposure(self):
        exposure = self.amount if self.state == TRADE_OPENED else 0
        if self.direction == DIR_SELL:
            exposure *= -1
        return exposure
        
        
    def remove_trade_entries(self):
        pass
    
    
    def resend_failed_orders(self):
        pass



class NullTrade(BenchmarkTrade):
    #pass
    #def save(self):
    #    pass
    
    def save_performance(self):
        pass
    
    

        

class BenchmarkAccount(FIXAccountBase):
    
    broker_id = 0    
    TradeClass = BenchmarkTrade
    
        
    def __init__(self, account, accountinfo):    
        FIXAccountBase.__init__(self, account, accountinfo)
        self.stoplosses = {DIR_BUY : {}, DIR_SELL: {}}
        self.takeprofits = {DIR_BUY : {}, DIR_SELL: {}}
        self._last_candle_time = None
    
    
    def connect(self):
        self._on_logon()
        return True
    
    def shutdown(self):
        pass
        
    def check_connection(self):
        pass    
    
    
    def after_evaluate(self, timeframe):
        pass        
    
    
    def _on_logon(self):
        self.businessaccount.on_logon()

    
    def check_trades(self):
        pass
    
        
        
    def on_tick(self):        
        #self.log.debug('SL: %s' % self.stoplosses)
        #self.log.debug('TP: %s' % self.takeprofits)
        for symbol in historic.CURRENT_PRICE:
            if not historic.CURRENT_PRICE[symbol]['ASK'] and not historic.CURRENT_PRICE[symbol]['BID']:
                continue
                        
            if symbol in self.stoplosses[DIR_BUY]:
                for sl, trade in self.stoplosses[DIR_BUY][symbol].items():
                    if historic.CURRENT_PRICE[symbol]['BID'] <= sl:
                        trade.sl_hit()                    
            
            if symbol in self.stoplosses[DIR_SELL]:
                for sl, trade in self.stoplosses[DIR_SELL][symbol].items():
                    if historic.CURRENT_PRICE[symbol]['ASK'] >= sl:
                        trade.sl_hit()

            if symbol in self.takeprofits[DIR_BUY]:                                
                for sl, trade in self.takeprofits[DIR_BUY][symbol].items():
                    if historic.CURRENT_PRICE[symbol]['BID'] >= sl:
                        trade.tp_hit()                    
            
            if symbol in self.takeprofits[DIR_SELL]:
                for sl, trade in self.takeprofits[DIR_SELL][symbol].items():
                    if historic.CURRENT_PRICE[symbol]['ASK'] <= sl:
                        trade.tp_hit() 


    
    def register_trade(self, trade):
        #self.log.info('registering trade: %s' % trade)
        if trade.sl_price:
            self.stoplosses[trade.direction].setdefault(trade.symbol, {})
            self.stoplosses[trade.direction][trade.symbol][trade.sl_price] = trade         
            
        if trade.tp_price:
            self.takeprofits[trade.direction].setdefault(trade.symbol, {})
            self.takeprofits[trade.direction][trade.symbol][trade.tp_price] = trade


    def on_trade_open(self, trade):
        self.register_trade(trade)
        self.businessaccount.on_trade_open(trade)                        

    
    def on_trade_close(self, trade):
        try:
            if trade.sl_price:
                del self.stoplosses[trade.direction][trade.symbol][trade.sl_price]            
        except:
            pass
        
        try:
            if trade.tp_price:
                del self.takeprofits[trade.direction][trade.symbol][trade.tp_price]            
        except:
            pass
        
        self.businessaccount.on_trade_close(trade)   
        
        
               

        
