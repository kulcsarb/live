# -*- encoding: utf-8 -*- 
'''
Created on Nov 27, 2012

@author: gdt
'''

from gdtlive.core.accounts.fixaccountbase import FIXAccountBase, FIXAccountSyncBase
from gdtlive.core.constants import *
from gdtlive.constants import *
import gdtlive.core.fix.dukascopy.fix as fix
import gdtlive.store.db as db
from datetime import datetime
import re

from gdtlive.core.trade import CONDORDER_CANCEL_SENT, CONDORDER_CANCEL_FAILED, CONDORDER_CANCEL_REJECTED, \
    CONDORDER_PENDING_NEW, CONDORDER_UNKNOWN, CONDORDER_ENTRY_SENT, CONDORDER_UNKNOWN_CLOSED, \
    MARKETORDER_UNKNOWN_CLOSED
      
import traceback
from sqlalchemy.sql import select
import gdtlive.core.sqlupdater as sql


class DukascopyAccount(FIXAccountBase):
    
    broker_id = 1
            
    def place_stoploss(self, trade, entry_clordid, symbol, direction, amount, price, entry_sent_time, resend=None):
        return self._place_conditionalorder(trade, entry_clordid, symbol, direction, amount, price, entry_sent_time, fix.OrdType.STOP, resend)
        
        
    def place_takeprofit(self, trade, entry_clordid, symbol, direction, amount, price, entry_sent_time, resend=None):
        return self._place_conditionalorder(trade, entry_clordid, symbol, direction, amount, price, entry_sent_time, fix.OrdType.STOP_LIMIT, resend)
            
        
    def _place_conditionalorder(self, trade, entry_clordid, symbol, direction, amount, price, entry_sent_time, order_type, resend):                
        
        self.register_order(trade, entry_clordid)
        
        message = fix.NewOrderSingle(ClOrdID = entry_clordid, 
                                            Symbol = symbol, 
                                            Side = {DIR_BUY: fix.Side.BUY, DIR_SELL: fix.Side.SELL}[direction], 
                                            OrderQty = amount,
                                            Price = price,
                                            TransactTime = entry_sent_time,
                                            OrdType = order_type,
                                            TimeInForce = fix.TimeInForce.GOOD_TILL_CANCEL,
                                            PossResend = resend
                                            )       

        return self.send_message(trade.id, message)
    
        
    def cancel_order(self, trade, symbol, direction, entry_orderid, entry_clordid, cancel_clordid, resend=None):
        
        self.register_order(trade, cancel_clordid)
        
        message = fix.OrderCancelRequest(Symbol = symbol, 
                                      OrderID = entry_orderid, 
                                      OrigClOrdID = entry_clordid,
                                      ClOrdID = cancel_clordid,
                                      PossResend = resend
                                      )
        
        
        return self.send_message(trade.id, message)
    
        
    
    def send_marketorder(self, trade, clordid, direction, amount, symbol, resend=None):                
        
        self.register_order(trade, clordid)
        
        message = fix.NewOrderSingle(ClOrdID = clordid, 
                                            Symbol = symbol, 
                                            Side = {DIR_BUY: fix.Side.BUY, DIR_SELL: fix.Side.SELL}[direction], 
                                            OrderQty = amount,
                                            TransactTime = datetime.utcnow(),
                                            OrdType = fix.OrdType.MARKET,
                                            TimeInForce = fix.TimeInForce.FILL_OR_KILL,
                                            PossResend = resend
                                            )
                    
        return self.send_message(trade.id, message)        
        


    def onMessage(self, message):
        self.log.debug('RECEIVED: %s' % message)
        self._last_message_received = datetime.utcnow()
        
        if type(message) == fix.ExecutionReport:
            if message.ClOrdID in self.trades:
                #FIXME: ez az azonositás nem kezeli le a kliensból bezárt ordereket, megbizhatóbb azonositást kell csinálni
                                                        
                trade = self.trades[message.ClOrdID]
                
                self._save_message(trade.id, FIX_INCOMING,  message)
                                    
                previous_state = trade.state                                
                try:
                    if message.OrdStatus == fix.OrdStatus.PENDING_NEW:
                        trade.handle_pending_new(message.ClOrdID, message.OrderID, message.AvgPx)
                    elif message.OrdStatus == fix.OrdStatus.FILLED:
                        trade.handle_filled(message.ClOrdID, message.CumQty, message.AvgPx, message.TransactTime)
                    elif message.OrdStatus == fix.OrdStatus.CANCELED:
                        trade.handle_canceled(message.ClOrdID, message.TransactTime)                    
                    elif message.OrdStatus == fix.OrdStatus.REJECTED:
                        trade.handle_rejected(message.ClOrdID, message)
                                                                                                    
                except:
                    self.log.error(traceback.format_exc())
                                                
                try:                                                                                                                                     
                    if trade.state == TRADE_CLOSED: 
                        self.businessaccount.on_trade_close(trade)                                                                                  
                        
                    if trade.state == TRADE_OPENED and previous_state != TRADE_OPENED:                                                                        
                        self.businessaccount.on_trade_open(trade)                        
                
                except:
                    self.log.error(traceback.format_exc())
            
            elif message.ClOrdID == 0:
                self.log.warning('Account owner is messing with our positions')
                
                if message.OrdStatus == fix.OrdStatus.FILLED and message.OrdType == fix.OrdType.MARKET and message.LeavesQty == 0.0:
                    symbol = message.Symbol[:3] + message.Symbol[4:]
                    for trade in self.trades.values():
                        if trade.symbol == symbol:
                            trade.mark_closed(message.AvgPx)
                    
                                          
            else:                
                self.log.warning('Trade not found for message: %s' % str(message))
                self.log.warning('ClOrdID: %s' % message.ClOrdID)
                self.log.warning('trades: %s' % self.trades.keys())
                
        
#        elif type(message) == fix.AccountInfo:            
#            self.handle_account_info_message(message)
#        
        #elif type(message) == fix.Notification:                                                    
        #    self._handle_notification(message)
#        
#        elif type(message) == fix.InstrumentPositionInfo:
#            self.handle_instrumentpositioninfo(message)
                    
        self.log.debug('message processed')
                
    
    def _handle_notification(self, message):
        try:
            sltp_modify_filter = "Position \d+[A-Z]{3}/[A-Z]{3} ENTRY #(\d+) price changed from ([\d\.]+) [A-Z]{3} to ([\d\.]+) [A-Z]{3}"
            match = re.search(sltp_modify_filter, message.Text)                    
            if match:
                self.log.info(message)
                self.log.info('SL or TP changed by account owner! %s' % str(message.Text))
                order_entry_id, old_price, new_price = match.groups()
                for trade in self.trades.itervalues():
                    trade.change_sltp_if_has_order(order_entry_id, new_price)
                                      
                return 
            
            sltp_cancel_filter ='Order CANCELLED: #(\d+)'
            match = re.search(sltp_cancel_filter,message.Text)
            if match:
                order_entry_id, = match.groups()
                self.log.info('SL or TP cancelled by account owner! %s' % str(message.Text))
                for trade in self.trades.itervalues():
                    trade.cancel_sltp_if_has_order(order_entry_id)            
                
                return                                                       
            
        except:
            self.log.error(traceback.format_exc())
        

#    def handle_instrumentpositioninfo(self, message):
#        try:
#            symbol = message.Symbol[:3] + message.Symbol[4:]
#            self.exposure[symbol] = message.Amount
#            table = db.InstrumentInfo.__table__
#            db.engine.execute(table.insert(), {'account_name': message.AccountName,
#                                               'sending_time': message.SendingTime, 
#                                               'amount' : message.Amount,
#                                               'price' : message.Price,
#                                               'symbol' : symbol
#                                               })
#        except:
#            self.log.error(traceback.format_exc())
#            
#
#    def update_account_info(self, async):
#        self.log.debug('Updating account info...')
#        self.last_info_request = datetime.now()    
#        self.events['accountinfo_updated'].clear()        
#        message = fix.AccountInfoRequest()        
#        self.engine.send(message)
#        sent = message.wait_until_processed()
#        if sent:
#            if not async:                
#                self.events['accountinfo_updated'].wait()
#        else:
#            self.log.warning('Failed to send AccountInfo message')                    
#                                    
#                                    
#    def handle_account_info_message(self, message):
#        self.equity = message.Equity
#        self.leverage = message.Leverage
#        self.usable_margin = message.UsableMargin
#        self.used_margin = message.Equity - message.UsableMargin
#        try:
#            self.use_of_leverage = (self.used_margin / self.equity) * 100.0
#        except:
#            pass 
#        
#        for mm_plugin in self.mm_plugins.values():
#            mm_plugin.setLeverage(self.leverage)
#        
#        self._update_performance()
#                                        
#        self.log.debug('open_trades: %d, floating profit: $%.1f, balance: $%.1f' % (self.open_trade_num, self.floating_profit, self.balance))        
#        self.log.debug('equity: $%.2f  use of leverage: %.2f%%, usable margin: $%.2f, used margin: $%.2f, leverage: %.1f )' % \
#                 (self.equity, self.use_of_leverage, self.usable_margin, self.used_margin, self.leverage))               
#        self.events['accountinfo_updated'].set() 
#    
                    
    
    def _synchronize_trades(self):
        sync = DukascopySync(self)
        sync.start()
        sync.join()                    
    


class DukascopySync(FIXAccountSyncBase):
    
    def start_sync(self):        
        for trade in self.account.trades.values():
            if trade.stoploss.state in [CONDORDER_CANCEL_SENT, CONDORDER_CANCEL_FAILED, CONDORDER_CANCEL_REJECTED, CONDORDER_PENDING_NEW, CONDORDER_ENTRY_SENT]:
                trade.stoploss.state = CONDORDER_UNKNOWN
                self.unknown_count += 1
            
            if trade.takeprofit.state in [CONDORDER_CANCEL_SENT, CONDORDER_CANCEL_FAILED, CONDORDER_CANCEL_REJECTED, CONDORDER_PENDING_NEW, CONDORDER_ENTRY_SENT]:
                trade.takeprofit.state = CONDORDER_UNKNOWN
                self.unknown_count += 1                
        
        if self.unknown_count:
            self.account.log.info('Sending OrderStatusRequest')
            message = fix.OrderMassStatusRequest(MassStatusReqID='GDT', MassStatusReqType=fix.MassStatusReqType.STATUS_FOR_ALL_ORDERS)
            self.account.send_message(message)