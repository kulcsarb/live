# -*- encoding: utf-8 -*- 
'''
Created on Nov 27, 2012

@author: gdt
'''
from gdtlive.c._backtest.simulatedliveaccount import SimulatedLiveAccount
from gdtlive.core.fix.initiator import FIXInitiator
from gdtlive.core.constants import *
from gdtlive.constants import * 
import gdtlive.core.datafeed.historic as historic
import gdtlive.store.db as db
import gdtlive.config as config 
from gdtlive.utils import is_weekend
from gdtlive.core.trade import Trade, CONDORDER_UNKNOWN
import time
import traceback
import copy
from datetime import datetime, timedelta
from Queue import Queue, Empty
from threading import Thread, Event, Lock
from sqlalchemy.sql import select
import gdtlive.core.sqlupdater as sql



class FIXAccountBase(object):
    
    broker_id = 0
    TradeClass = Trade

    def __init__(self, account, accountinfo):
                
        self.businessaccount = account                
        self.account_name = ""
        self.account_number = "" 
        self.username = ""
        self.password = ""
        self.server_url = ""
        self.server_port = 0
        self.server_use_ssl = True
        self.sendercomp_id = ""
        self.targetcomp_id = ""       
        self.clordid = 0
        self.trade_id = 0
        
                                
        self._last_message_received = None
        self.last_accperf_saved = None
        self.last_info_request = None
        self.last_reject_corrigate = None        
                
        self.log = self.businessaccount.log        
        self.check_connection_lock = Lock()
        
        self.engine = None                
        self.trades = {}
        
        self.account_name = accountinfo.name
        self.account_number = accountinfo.account_number
        self.username = accountinfo.username
        self.password = accountinfo.password
        self.server_url = accountinfo.server_url
        self.server_port = accountinfo.server_port
        self.server_use_ssl = accountinfo.use_ssl
        self.sendercomp_id = accountinfo.sendercompid
        self.targetcomp_id = accountinfo.targetcompid
            
                                                                                                                                                                                           
    def connect(self):        
        self.engine = FIXInitiator(self.server_url, self.server_port, self.sendercomp_id, self.targetcomp_id, self.onMessage, use_ssl = self.server_use_ssl)
        if self.engine.connect():            
            self.engine.callbacks['on_logon'] = self._on_logon
#            self.engine.callbacks['on_logout'] = self._on_logout
#            self.engine.callbacks['on_connect'] = self._on_connect
#            self.engine.callbacks['on_disconnect'] = self._on_disconnect
            self.engine.start() 
            time.sleep(2)                        
            if self.engine.login(self.username, self.password, wait=False):                                                                               
                return True
            
            # ha hétvége van, visszatérünk sikerrel, hogy elindulhasson az alkalmazás, és majd konnektál ha hétfő lesz
            if is_weekend():
                return True
    
        return False
            
    
    def _on_logon(self):
        self.businessaccount.on_logon()
                
#    def _on_logout(self):
#        self.businessaccount.on_logon()
#        
#    def _on_connect(self):
#        self.businessaccount.on_connect()
#        
#    def _on_disconnect(self):
#        self.businessaccount.on_disconnect()
    
    
    def on_tick(self):
        pass
    
    def is_benchmark_account(self):
        return self.broker_id == 0
    
    def shutdown(self):
        try:
            self.engine.shutdown()
        except:
            self.log.critical(traceback.format_exc())        


    def after_evaluate(self, timeframe):
        pass
    
    
    def check_trades(self):
        pass
    
        
    def get_connection_state(self):
        if self.engine:
            return self.engine.connection_state
        else:
            return STATE_NOT_CONNECTED
    
    
    def check_connection(self):
        with self.check_connection_lock:
            try:                
                if self.engine.connection_state == STATE_SHUTDOWN:                    
                    self.log.info('FIX Engine is down, restarting...')
                    self.connect()
                else:                                   
                    self.engine.check_connection()
            except:
                self.log.critical(traceback.format_exc())


    def _save_message(self, trade_id, direction, message, success=True):        
        try:
            table = db.FIXOrderHistory.__table__
            command = table.insert().values(trade_id = trade_id, 
                                            time=datetime.utcnow(),
                                            direction = direction,
                                            message=str(message),
                                            success=success)
            sql.server.insert(command)                        
        except:
            self.log.error(traceback.format_exc())
            
    
    def send_message(self, trade_id, message):
        waiting_time = 10        
        while self.engine.connection_state != STATE_SESSION_ESTABLISHED:
            self.log.warning('FIX Session is not established, waiting...')
            time.sleep(2)
            waiting_time -= 1
            if not waiting_time :
                self.log.warning('network is down, message is not sent: %s' % message)
                break
                                
        if self.engine.connection_state == STATE_SESSION_ESTABLISHED:
            self.log.debug('Sending: %s' % message)        
            self.engine.send(message)        
            success = message.wait_until_processed()
        
            self._save_message(trade_id, FIX_OUTGOING, message, success)
            
            if success:
                self.log.debug('Order sended: %s' % message)                
            else:            
                self.log.warning('Order send failed: %s' % message)
        else:
            success = False
            
        return success
            
    
    def _synchronize_trades(self):
        pass        

    
    
    def register_order(self, trade, clordid):
        if clordid not in self.trades:
            #self.log.debug('registering trade: %s -> %s' % (clordid, trade))
            self.trades[clordid] = trade
        #else:
            #self.log.warning('clordid already registered !  %s -> %s' % (clordid, self.trades[clordid]))
    
    def unregister_order(self, clordid):
        if clordid in self.trades:
            del self.trades[clordid]

    
    def get_engine_details(self):
        try:
            return self.engine.get_engine_details()
        except:
            return {}
    
    def get_engine_last_messages(self):
        try:
            return self.engine.get_last_messages()
        except:
            return {}
        
    def get_engine_logs(self, logtype):
        return self.engine.get_engine_logs(logtype)            
            

class FIXAccountSyncBase(Thread):
    
    def __init__(self, account):
        self.timeout = 10
        self.account = account
        self.unknown_count = 0
        super(FIXAccountSyncBase, self).__init__()
        
        
    def start_sync(self):
        pass
                           
        
    def count_unknowns(self):
        count = 0
        trades = list(set(self.account.trades.values()))
        for trade in trades:
            if trade.stoploss.state == CONDORDER_UNKNOWN:
                count += 1
            if trade.takeprofit.state == CONDORDER_UNKNOWN:
                count += 1
        #print 'current unknowns:', count
        return count
        
        
    def close_unknowns(self):
        for trade in self.account.trades.values():
            if trade.stoploss.state == CONDORDER_UNKNOWN and trade.takeprofit.state == CONDORDER_UNKNOWN:
                pass
            
            elif trade.stoploss.state == CONDORDER_UNKNOWN:
                self.account.log.info('Closing trade with SL hit: %s' % trade)
                trade.handle_filled(trade.stoploss.entry_clordid, trade.amount, trade.stoploss.entry_price, datetime.utcnow())
                
            elif trade.takeprofit.state == CONDORDER_UNKNOWN:
                self.account.log.info('Closing trade with TP hit: %s' % trade)
                trade.handle_filled(trade.takeprofit.entry_clordid, trade.amount, trade.takeprofit.entry_price, datetime.utcnow())                                    
    
    
    def send(self, message):
        self.account.engine.send(message)
             
    
    def run(self):
        self.account.log.info('Account synchronization started')
        retries = 3
        self.unknown_count = 0
        
        while retries:
            self.start_sync()
            if not self.unknown_count:
                break
            
            self.account.log.info('sleeping')
            time.sleep(10)
            if self.unknown_count != self.count_unknowns():
                break
            
            retries -= 1
            self.account.log.info('retries left: %d' % retries)
                
        self.close_unknowns()        
        
        self.account.log.info('Account synchronization ended')
