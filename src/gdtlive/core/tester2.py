'''
Created on Oct 9, 2012

@author: gdt
'''
'''
Created on Jun 11, 2012

@author: gdtlive
'''
import gdtlive.core.fix.dukascopy.fix as fix
#from fix.dukascopy.servertests import ServerTester
#from gdtlive.core.fix.messages import *
from gdtlive.core.fix.initiator import FIXInitiator
from logging.handlers import RotatingFileHandler 
import logging
import traceback
import time
from datetime import datetime


print datetime.utcfromtimestamp(1349653500)
print datetime.utcfromtimestamp(1349654400)
quit()



URL = 'demo-api.dukascopy.com' 
TRADE_PORT = 10443
FEED_PORT = 9443

FEED_SENDERCOMPID = 'FEED_DEMOPHRcOEU_DEMOFIX'
#SENDERCOMPID = 'DEMO3NxKxd_DEMOFIX'
SENDERCOMPID = 'DEMOPHRcOEU_DEMOFIX'
#FEED_SENDERCOMPID = 'FEED_DEMO3NxKxd_DEMOFIX' 
TARGETCOMPID = 'DUKASCOPYFIX'
#USERNAME = 'DEMO3NxKxd'
#PASSWORD = 'NxKxd'
USERNAME = 'DEMOPHRcOEU'
PASSWORD = 'Y2eGD'

LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"

log = logging.getLogger('gdtlive')
log.setLevel(logging.DEBUG)

handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter(LOG_FORMAT))
handler.setLevel(logging.DEBUG)
log.addHandler(handler)

#handler = RotatingFileHandler('dukastester.log','a',100*1024*1024, 2)
#handler.setFormatter(logging.Formatter(LOG_FORMAT))    
#log.addHandler(handler)


def message_handler(message):
    print message


if __name__ == '__main__':
    
    engine = FIXInitiator(URL, FEED_PORT, FEED_SENDERCOMPID, TARGETCOMPID, message_handler)
    if engine.connect():
        engine.start()
        if engine.login(USERNAME, PASSWORD):
            print 'Login OK'
        else:
            print 'Login Failed'
        
        #time.sleep(5)
        message = fix.MarketDataRequest(MDReqID='GDT', 
                                               SubscriptionRequestType = fix.SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES,
                                               MarketDepth = 1,
                                               MDUpdateType = fix.MDUpdateType.FULL_REFRESH, 
                                               NoMDEntryTypes = [
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.BID),
                                                                 fix.MarketDataRequest.NoMDEntryTypes(MDEntryType= fix.MDEntryType.OFFER)
                                                                 ],
                                               NoRelatedSym = [fix.MarketDataRequest.NoRelatedSym(Symbol='EUR/USD')]
                                               )
        engine.send(message)
        message.wait_until_processed()
        
#        time.sleep(2)
#        message = fix.AccountInfoRequest()        
#        engine.send(message)
#        sent = message.wait_until_processed()
#        time.sleep(2)
#        message = fix.AccountInfoRequest()        
#        engine.send(message)
#        sent = message.wait_until_processed()


    
