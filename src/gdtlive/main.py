# -*- encoding: utf-8 -*-
'''
OK Fő programvezérlő funkciók

@status: Ellenőrizve, rendben.
@author: kulcsarb
'''
#import gdtlive.languages
from gdtlive.control.server import HTTPServer
from gdtlive.admin.system.log import initialize_loggers, extra
import gdtlive.store.db as db 
import gdtlive.historic.plugin.dukascopy_fs
from gdtlive.config import CHECK_DATAROWS
import gdtlive.config
import os.path
import time
import sys
import logging
import traceback
import gdtlive.core.tradeengine
import stacktrace
import multiprocessing
import os
from threading import Thread
from optparse import OptionParser
import fcntl


errorlog = logging.getLogger('errors.main')
log = logging.getLogger('gdtlive.main')    


def parse_commandline():
    parser = OptionParser()
    parser.add_option("-c", "--clean", action='store_true', dest="clean_start", help="does not start the trade engine")
    
    (options, args) = parser.parse_args()    
    if options.clean_start == True:
        gdtlive.config.LIVE_AUTOSTART = False
                    
PID_FILE = None

def save_pid():
    global PID_FILE
    try:
        path = os.path.abspath(gdtlive.config.LOG_PATH)            
        PID_FILE = open(path + os.sep +'gdtlive.pid', 'w')
        fcntl.lockf(PID_FILE, fcntl.LOCK_EX | fcntl.LOCK_NB)
        PID_FILE.write("%d\n" % multiprocessing.current_process().pid)                                
        return True
    except:        
        log.error('creating pid file failed, maybe another instance is running' + str(traceback.format_exc()))
        return False
        
    
def main():
    '''A parancssorból indított alkalmazás belépőpontja       
    '''
    
    try:
        initialize_loggers()
        if not save_pid():            
            quit()
        parse_commandline()      
                                     
        #stacktrace.trace_start('/opt/', 5)                                    
        if db.init():                   
            gdtlive.historic.plugin.dukascopy_fs.initialize_datarow_descriptors()            
            gdtlive.historic.plugin.dukascopy_fs.cleanup_db()            
                                                                            
            Thread(target = gdtlive.core.tradeengine.start, args=()).start()                                         
            HTTPServer.start()            
    except Exception:        
        errorlog.error('Exception raised during application startup', extra=extra('main', globals=globals(), locals=locals()), exc_info=sys.exc_info())
        HTTPServer.stop()        


if __name__ == '__main__':    
    main()
