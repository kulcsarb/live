# -*- encoding: utf-8 -*- 
'''
Created on Nov 27, 2012

@author: gdt
'''
import gdtlive.store.db as db 
from threading import Lock, Thread
import gdtlive.config as config
from gdtlive.constants import *
from gdtlive.core.constants import *
from gdtlive.core2.strategy import Strategy
from gdtlive.core2.accountmanager import AccountManager
from gdtlive.utils import add_log_handler, remove_log_handlers
from datetime import datetime
import gdtlive.core.datafeed.historic as historic
import gdtlive.core.sqlupdater as sql
import os
import ast
import logging
import traceback


class Portfolio(object):
        
    
    def __init__(self, trade_engine):
        self.trade_engine = trade_engine
        self.name = ''    
        self.portfolio_id = 0
        self.portfoliorun_id = 0    
        self.strategies = {}        
        self.log = logging.getLogger('gdtlive.core.portfolio')     
        self.accountmanager = AccountManager(self)
        self.state = STOPPED
        self.online = True
        self.initialize_logging()
        self.running_from = None
        self.running_to = None
        
    
    def initialize_logging(self):
        if self.name:
            name = 'gdtlive.core.portfolio.%s' % self.name
        else:
            name = 'gdtlive.core.portfolio'
        self.log = logging.getLogger(name)                
        self.log.setLevel(logging.DEBUG)        
        remove_log_handlers(self.log)        
        add_log_handler(self.log, name, logging.INFO)
        add_log_handler(self.log, name, logging.WARN)
        add_log_handler(self.log, name, logging.DEBUG)        
        self.accountmanager.log = self.log
    
    
    def load(self, portfoliorun_id):                                                
        self.portfoliorun_id = portfoliorun_id
        session = None
        try:
            session = db.Session()
            
            query = session.query(db.Portfolio.name, db.Portfolio.id, db.PortfolioRun.state, db.PortfolioRun.running_from, db.PortfolioRun.running_to)
            query = query.filter(db.Portfolio.id == db.PortfolioRun.portfolio_id)
            query = query.filter(db.PortfolioRun.id == portfoliorun_id)
                            
            self.name, self.portfolio_id, self.state, self.running_from, self.running_to = query.first()
                                            
            self.log.info('Loading portfolio: %d. %s - %s, %.2f%%' % (self.portfolio_id, self.name, self.state))
            
            self.initialize_logging()
                                                            
            self.accountmanager.load(session)                
                                                
            query = session.query(db.PortfolioStrategyRun.id)
            query = query.filter(db.PortfolioStrategyRun.portfoliorun_id == self.portfoliorun_id)
            query = query.filter(db.PortfolioStrategyRun.state.in_((RUNNING, STOPPING, ABORTING)))
            
            self.log.info('Loading active strategies for portfolio %s' % self.name)
            
            for pfstratrun_id, in query.all():                
                strategy = Strategy(self.accountmanager, self.log)
                success = strategy.load(session, self.portfolio_id, pfstratrun_id)
                if success:                                                         
                    self.strategies[strategy.strategy_id] = strategy
                
                                                    
        except:
            self.log.error(traceback.format_exc())
        finally:
            if session:
                session.close()                                                     
                                                               
    
    def bring_online(self):
        self.online = True
                
    def take_offline(self):
        self.offline = False
        
    
    def start(self, portfolio_id):
        import gdtlive.core.tradeengine
                                
        self.portfolio_id = portfolio_id
        session = None
        try:
            session = db.Session()
            
            portfolio = session.query(db.Portfolio).get(self.portfolio_id)                 
            self.name = portfolio.name                                
            
            self.log.info('Starting portfolio: %s' % self.name)                                
            
            self.initialize_logging()
            
            portfrun = db.PortfolioRun(self.portfolio_id, datetime.utcnow())                
            session.add(portfrun)
            session.commit()                
            
            self.portfoliorun_id = portfrun.id 
            
            self.accountmanager.start(session)                
                                                
            query = session.query(db.PortfolioStrategy.strategy_id)
            query = query.filter(db.PortfolioStrategy.portfolio_id == self.portfolio_id)                
            
            self.log.info('Starting strategies for portfolio %s' % self.name)
            
            for strategy_id, in query.all():                
                strategy = Strategy(self.accountmanager, self.log)
                strategy.start(session, self.portfolio_id, self.portfoliorun_id, strategy_id)  
                  
                self._register_startegy(strategy)                                
            
            self._update_state(RUNNING)                 
            
            gdtlive.core.tradeengine.feedserver.preload() 
        except:
            self.log.error(traceback.format_exc())
        finally:
            if session:
                session.close()
        
    
    def stop(self):        
        session = None
        try:
            self.log.info('portfolio %s : STOPPING' % self.name)
            self._update_state(STOPPING)
            
            for strategy in self.strategies.values()[:]:                    
                strategy.stop()                
                self._remove_strategy(strategy)                                
            
            self.accountmanager.stop()
                        
            self._update_state(STOPPED)                                                        
        except:
            self.log.error(traceback.format_exc())
        finally:
            if session:
                session.close()
        
    
    def abort(self):
        session = None
        try:
            self.log.info('portfolio %s : ABORTING' % self.name)
            self._update_state(ABORTING)                
            
            for strategy in self.strategies.values()[:]:                    
                strategy.abort()                                                            
                self._remove_strategy(strategy)
                
            self.accountmanager.abort()
                                                            
            self._update_state(ABORTED)                                
        except:
            self.log.error(traceback.format_exc())
        finally:
            if session:
                session.close()
                            
        
    def start_strategy(self, strategy_id):
        import gdtlive.core.tradeengine        
                
        session = None
        try:
            session = db.Session()
            
            strategy = Strategy(self.accountmanager, self.log)
            strategy.start(session, self.portfolio_id, self.portfoliorun_id, strategy_id)                 
            
            gdtlive.core.tradeengine.feedserver.preload()
                    
            self._register_startegy(strategy)                                                
        except:
            self.log.critical(traceback.format_exc())
        finally:
            if session:
                session.close()
                
    
    def stop_strategy(self, strategy_id):
        try:                        
            if strategy_id in self.strategies:
                strategy = self.strategies[strategy_id]
                strategy.stop()                                     
                self._remove_strategy(strategy)
            else:
                self.log.warning('Strategy %d not found in portfolio %s' % (strategy_id, self.name))                        
        except:
            self.log.critical(traceback.format_exc())
                
    
    def abort_strategy(self, strategy_id):
        try:                        
            if strategy_id in self.strategies:
                strategy = self.strategies[strategy_id] 
                strategy.abort()
                self._remove_strategy(strategy)
            else:
                self.log.warning('Strategy %d not found in portfolio %s' % (strategy_id, self.name))                                                       
        except:
            self.log.critical(traceback.format_exc())
        
        
    def start_account(self, portfaccount_id):                
        session = None
        try:
            session = db.Session()
            
            self.accountmanager.start_account(portfaccount_id)
            for strategy in self.strategies.itervalues():
                self.accountmanager.add_strategy(strategy, session)
                
        except:
            self.log.error(traceback.format_exc())
        finally:
            if session: session.close() 
    
    
    def stop_account(self, paccount_id):
        with self.lock:
            self.accountmanager.stop_account(paccount_id)    
    
    
    def abort_account(self, paccount_id):
        with self.lock:
            self.accountmanager.abort_account(paccount_id)
        
            
    def _register_startegy(self, strategy):        
        self.strategies[strategy.strategy_id] = strategy
    
    
    def _remove_strategy(self, strategy):
        try:
            self.log.info('Removing strategy %d from portfolio %s' % (strategy.strategy_id, self.name))
            
            del self.strategies[strategy.strategy_id]                        
        except:
            self.log.error(traceback.format_exc())            
        
    
    def on_trade_close(self, strategy_id):
        
        # Lock kell, mert ezt több futó Account hivja meg mindig, több szálból 
        # ha az egyik törli a stratégiát, akkor a másiknak már nem lesz meg a stratégia
        
        with self.ontradeclose_lock:
            if strategy_id in self.strategies:
                strategy = self.strategies[strategy_id]
                strategy.on_trade_close()
                if strategy.is_stopped() or strategy.is_aborted():            
                    self._remove_strategy(strategy)                                    
                                
        self._check_stop_abort()
                            
            
    def on_account_stop(self, paccount_id):
        self.log.info('on_account_stop')
        self.log.info('strategies: ' + str(self.strategies))
        self.log.info('accounts: %d' % self.accountmanager.account_num())
        self._check_stop_abort()        
            
        
    def _check_stop_abort(self):
        with self.stoplock:
            if self.state == STOPPING and not len(self.strategies) and not self.accountmanager.account_num():
                self.log.info('Portfolio is stopped')
                self._update_state(STOPPED)
                self.trade_engine.on_portfolio_stop(self.portfolio_id)
            
            if self.state == ABORTING and not len(self.strategies) and not self.accountmanager.account_num():
                self.log.info('portfolio %s : ABORTED' % self.name)
                self._update_state(ABORTED)                                                             
                self.trade_engine.on_portfolio_stop(self.portfolio_id)            
        
    
                                            
    def evaluate(self, timeframe):        
        try:                                 
            self.log.info('Portfolio.evaluate %s - start' % (TIMEFRAME[timeframe]))
            with self.lock:                                                                                
                                    
                self.accountmanager.before_evaluate(timeframe)    
                        
                for strategy in self.strategies.values()[:]:
                    try:
                        trade = strategy.evaluate()
                    except:
                        self.log.critical(traceback.format_exc())
                
                
                self.accountmanager.after_evaluate(timeframe)
                
                self._update_running_fromto()
                                                                                                                                                                                                                                                                        
        except:
            self.log.error(traceback.format_exc())
        finally:
            self.log.info('Portfolio.evaluate %s - end' % (TIMEFRAME[timeframe]))
                        
    
    def _update_running_fromto(self):
        try:
            if not self.running_from:
                self.running_from = historic.CURRENT_TIME
            self.running_to = historic.CURRENT_TIME
            
            table = db.PortfolioRun.__table__
            command = table.update().where(table.c.id==self.portfoliorun_id).\
                     values(running_from = self.running_from, running_to = self.running_to)
            
            sql.server.update(command)                            
        except:                                
            self.log.warning("Portfolio %s - status update failed"  % (self.portfolio_id))
            self.log.warning(traceback.format_exc())
                     
                     
    def _update_state(self, state=None):
        if state:            
            self.log.info('Portfolio %s : %s -> %s' % (self.name, self.state, state))
            self.state = state
        try:            
            table = db.PortfolioRun.__table__
            if self.state in ('STOPPED', 'ABORTED') :
                command = table.update().where(table.c.id==self.portfoliorun_id).\
                     values(state = self.state, end_time=datetime.utcnow())
            else:
                command = table.update().where(table.c.id==self.portfoliorun_id).\
                     values(state = self.state)
            
            sql.server.update(command)                            
        except:                                
            self.log.warning("Portfolio %s - status update failed"  % (self.portfolio_id))
            self.log.warning(traceback.format_exc())
    
    
    def get_engine_details(self, account_id):        
        return self.accountmanager.get_engine_details(account_id)
    
    def get_engine_logs(self, account_id, logtype):
        return self.accountmanager.get_engine_logs(account_id, logtype)
    
    def get_account_logs(self, account_id, logtype):
        return self.accountmanager.get_account_logs(account_id, logtype)
    
    def get_engine_last_messages(self, account_id):
        return self.accountmanager.get_engine_last_messages(account_id)
    
    def shutdown_account(self, account_id):
        return self.accountmanager.shutdown(account_id)
    
    def check_connection(self, account_id):
        return self.accountmanager.check_connection(account_id)
    
    def has_account(self, account_id):
        return self.accountmanager.has_account(account_id)
    
    def refresh_account(self, account_id):
        self.accountmanager.refresh_account(account_id)
        
    def trade_repair(self, account_id, trade_id):
        self.accountmanager.trade_repair(account_id, trade_id)
            
    def trade_close(self, account_id, trade_id):
        self.accountmanager.trade_close(account_id, trade_id)
        
    def trade_markasclosed(self, account_id, trade_id):
        self.accountmanager.trade_markasclosed(account_id, trade_id)
        
    def trade_mo_sendopen(self, account_id, trade_id):
        self.accountmanager.trade_mo_sendopen(account_id, trade_id)
        
    def trade_mo_sendclose(self, account_id, trade_id):
        self.accountmanager.trade_mo_sendclose(account_id, trade_id)
        
    def trade_co_place(self, account_id, trade_id, co_type):
        self.accountmanager.trade_co_place(account_id, trade_id, co_type)
        
    def trade_co_cancel(self, account_id, trade_id, co_type):
        self.accountmanager.trade_co_cancel(account_id, trade_id, co_type)
        
    def trade_co_filled(self, account_id, trade_id, co_type):
        self.accountmanager.trade_co_filled(account_id, trade_id, co_type)

    def get_accounts(self):
        return self.accountmanager.get_accounts()
    
    def get_account_data(self, account_id):
        return self.accountmanager.get_account_data(account_id)
    
    def get_trades(self, account_id):
        return self.accountmanager.get_trades(account_id)
    
    def get_trade_data(self, account_id, trade_id):
        return self.accountmanager.get_trade_data(account_id, trade_id)
    
    def get_data(self):
        return {'name': self.name, 'state': self.state, 'perf': self.accountmanager.get_benchmark_data()}
        
        