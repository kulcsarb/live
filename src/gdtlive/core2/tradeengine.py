# -*- encoding: utf-8 -*- 
'''
Created on 2012.05.23.

@author: kulcsarb
'''
from gdtlive.constants import *
from gdtlive.core.constants import *
#from gdtlive.core.datafeed import DatafeedServer
from gdtlive.core.datafeed import DatafeedServer
from gdtlive.core2.portfolio import Portfolio
from logging.handlers import RotatingFileHandler
from threading import Thread
import gdtlive.core2.sqlupdater
import gdtlive.core.performance
import gdtlive.config as config
import gdtlive.store.db as db
import logging
import traceback
import gdtlive.c.talib as talib
import os

try:
    from gdtlive.rfoo.utils import rconsole    
except:
    print traceback.format_exc()
    pass

talib.Initialize()


log = logging.getLogger('gdtlive.core.managers')

    
trade_engine = None
feedserver = None



def init_logger(name):
    log = logging.getLogger(name)
    log.setLevel(logging.DEBUG)
    
    handler = RotatingFileHandler(config.LOG_PATH + os.sep + name + '.info.log','w+',20*1024*1024, 10)
    handler.setFormatter(logging.Formatter(LOG_FORMAT))
    handler.setLevel(logging.INFO)        
    log.addHandler(handler)
    
    handler = RotatingFileHandler(config.LOG_PATH + os.sep + name + '.error.log','w+',20*1024*1024, 10)
    handler.setFormatter(logging.Formatter(LOG_FORMAT))
    handler.setLevel(logging.WARN)        
    log.addHandler(handler)
    
    handler = RotatingFileHandler(config.LOG_PATH + os.sep + name + '.debug.log','w+',20*1024*1024, 10)
    handler.setFormatter(logging.Formatter(LOG_FORMAT))
    handler.setLevel(logging.DEBUG)        
    log.addHandler(handler)
    

def initialize_loggers():
    
    for logger_name in ['gdtlive.core','gdtlive.core.feed', 'gdtlive.core.managers']:
        init_logger(logger_name)
    
        
    log = logging.getLogger('gdtlive.evol.gnodes')
    log.setLevel(logging.DEBUG)
    handler = RotatingFileHandler(config.LOG_PATH + os.sep + 'gdtlive.evol.gnodes.log','w+',20*1024*1024, 10)
    handler.setFormatter(logging.Formatter("%(asctime)s %(message)s"))
    handler.setLevel(logging.DEBUG)        
    log.addHandler(handler)
    
    
        
    
def start():
    global trade_engine, feedserver    
                
    initialize_loggers()
    
    try:
        rconsole.spawn_server()
    except:
        print traceback.format_exc()
        pass
                
    # legelsonek ezt kell startolni
    gdtlive.core.sqlupdater.start()
    gdtlive.core.performance.start()        
    
    
    # A sorrend szándékos, függési viszonyban vannak egymástól
    trade_engine = TradeEngine()
    feedserver = DatafeedServer(trade_engine)        
    trade_engine.load_all()
    feedserver.start()    
    
    


class TradeEngine():
    
    def __init__(self):        
        self.portfolios = {}
        self.online = True
        log.info('Trade Engine started')        
    
    
    def __str__(self):
        s = "TradeEngine(" + self.name + ')\n'
        for portfolio in self.portfolios:
            s += '%s' % portfolio
        return s        
        
        
    def load_all(self):
        global feedserver
        try:
            session = db.Session()
            
            portf_ids = session.query(db.PortfolioRun.id, db.PortfolioRun.portfolio_id).filter(db.PortfolioRun.state.in_((RUNNING, ABORTING, STOPPING))).all()
    
            for pfrun_id, portf_id, in portf_ids:
                self.portfolios[portf_id] = Portfolio(self)
                self.portfolios[portf_id].load(pfrun_id)                 
                                    
            feedserver.preload()                               
        except:
            log.error(traceback.format_exc())
        finally:
            if session:
                session.close()
    
    
    def stop_all(self):
        for portfolio in self.portfolios.itervalues():
            portfolio.stop()
            
    
    def abort_all(self):
        for portfolio in self.portfolios.itervalues():
            portfolio.abort()
    
        
    def bring_online(self):
        self.online = True
        
        
    def take_offline(self):
        self.offline = False
    
            
    def start_portfolio(self, portfolio_id):
        if portfolio_id not in self.portfolios:             
            self.portfolios[portfolio_id] = Portfolio(self)
            self.portfolios[portfolio_id].start(portfolio_id)                                            
         
         
    def stop_portfolio(self, portfolio_id):
        if portfolio_id in self.portfolios:
            self.portfolios[portfolio_id].stop()   
            del self.portfolios[portfolio_id]
         
                  
    def abort_portfolio(self, portfolio_id):
        if portfolio_id in self.portfolios:
            self.portfolios[portfolio_id].abort()
            del self.portfolios[portfolio_id]

        
#        
#    def connect(self):        
#        for runner in self.strategy_runners:
#            log.info('connecting to account %s' % runner.account_name)
#            result = runner.connect()            
#            log.info('connection succeded' if result else 'connection failed')                        
#                
#           


    def start_strategy(self, portfolio_id, portfstrat_id):
        if portfolio_id in self.portfolios:
            Thread(target=self.portfolios[portfolio_id].start_strategy, args=(portfstrat_id,)).start()
            
    
    def stop_strategy(self, portfolio_id, strategy_id):
        if portfolio_id in self.portfolios:
            self.portfolios[portfolio_id].stop_strategy(strategy_id)
    
    
    def abort_strategy(self, portfolio_id, strategy_id):
        if portfolio_id in self.portfolios:
            self.portfolios[portfolio_id].abort_strategy(strategy_id)


    def start_account(self, portfolio_id, paccount_id):
        if portfolio_id in self.portfolios:
            self.portfolios[portfolio_id].start_account(paccount_id)

    def stop_account(self, portfolio_id, paccount_id):
        if portfolio_id in self.portfolios:
            self.portfolios[portfolio_id].stop_account(paccount_id)
            
    def abort_account(self, portfolio_id, paccount_id):
        if portfolio_id in self.portfolios:
            self.portfolios[portfolio_id].abort_account(paccount_id)
    

    def get_engine_details(self, portfolio_id, account_id):        
        if portfolio_id in self.portfolios:            
            return self.portfolios[portfolio_id].get_engine_details(account_id)
        return {}
        
    def get_engine_logs(self, portfolio_id, account_id, logtype):        
        if portfolio_id in self.portfolios:            
            return self.portfolios[portfolio_id].get_engine_logs(account_id, logtype)
        return {}
    
    def get_account_logs(self, portfolio_id, account_id, logtype):        
        if portfolio_id in self.portfolios:            
            return self.portfolios[portfolio_id].get_account_logs(account_id, logtype)
        return {}
        
    def get_engine_last_messages(self, portfolio_id, account_id):
        if portfolio_id in self.portfolios:
            return self.portfolios[portfolio_id].get_engine_last_messages(account_id)
        return {}

    def shutdown_account(self, portfolio_id, account_id):
        if portfolio_id in self.portfolios:
            return self.portfolios[portfolio_id].shutdown_account(account_id)
    
    def check_connection(self, portfolio_id, account_id):
        if portfolio_id in self.portfolios:
            return self.portfolios[portfolio_id].check_connection(account_id)
    
    def refresh_account(self, account_id):
        for portfolio in self.portfolios.itervalues():
            if portfolio.has_account(account_id):
                portfolio.refresh_account(account_id)
                
    def trade_repair(self, portfolio_id, account_id, trade_id):
        if portfolio_id in self.portfolios:
            self.portfolios[portfolio_id].trade_repair(account_id, trade_id)            
    
    def trade_close(self, portfolio_id, account_id, trade_id):
        if portfolio_id in self.portfolios:
            self.portfolios[portfolio_id].trade_close(account_id, trade_id)
                
    def trade_markasclosed(self, portfolio_id, account_id, trade_id):
        if portfolio_id in self.portfolios:
            self.portfolios[portfolio_id].trade_markasclosed(account_id, trade_id)        
    
    def trade_mo_sendopen(self, portfolio_id, account_id, trade_id):
        if portfolio_id in self.portfolios:
            self.portfolios[portfolio_id].trade_mo_sendopen(account_id, trade_id)            
    
    def trade_mo_sendclose(self, portfolio_id, account_id, trade_id):
        if portfolio_id in self.portfolios:
            self.portfolios[portfolio_id].trade_mo_sendclose(account_id, trade_id)        
    
    def trade_co_place(self, portfolio_id, account_id, trade_id, co_type):
        if portfolio_id in self.portfolios:
            self.portfolios[portfolio_id].trade_co_place(account_id, trade_id, co_type)        
    
    def trade_co_cancel(self, portfolio_id, account_id, trade_id, co_type):
        if portfolio_id in self.portfolios:
            self.portfolios[portfolio_id].trade_co_cancel(account_id, trade_id, co_type)
            
    def trade_co_filled(self, portfolio_id, account_id, trade_id, co_type):
        if portfolio_id in self.portfolios:
            self.portfolios[portfolio_id].trade_co_filled(account_id, trade_id, co_type)
        
        
#-------------------------------------------------------------------------------------------
#
#                            EVENT HANDLERS
#
#-------------------------------------------------------------------------------------------


     
    def evaluate(self, timeframe):
        if self.online:
            log.info('TradeEngine.evaluate %s - start' % TIMEFRAME[timeframe])                                  
            for portfolio in self.portfolios.itervalues():
                log.debug('TradeEngine.evaluate %s - evaluating Portfolio %s' % (TIMEFRAME[timeframe], portfolio.name))
                try:    
                    portfolio.evaluate(timeframe)                    
                except:
                    log.error(traceback.format_exc())                                            
                
            log.info('TradeEngine.evaluate %s - end' % TIMEFRAME[timeframe])
        else:
            log.info('TradeEngine is offline, skip evaluating portfolios')
                    

    def on_portfolio_stop(self, portf_id):
        try:
            
        except:
            pass
    
    
    def get_portfolios(self):
        portfolios = {}
        for id, portfolio in self.portfolios.items():
            portfolios[id] = portfolio.get_data()
        return portfolios
    
    def get_account_data(self, portfolio_id, account_id):
        try:
            return self.portfolios[portfolio_id].get_account_data(account_id)
        except:
            return {}    
    
    def get_accounts(self, portfolio_id):
        try:
            return self.portfolios[portfolio_id].get_accounts()
        except:            
            return {}
        
    def get_trades(self, portfolio_id, account_id):
        try:
            return self.portfolios[portfolio_id].get_trades(account_id)
        except:            
            return []
        
    def get_trade_data(self, portfolio_id, account_id, trade_id):
        try:
            return self.portfolios[portfolio_id].get_trade_data(account_id, trade_id)
        except:            
            return {}
    