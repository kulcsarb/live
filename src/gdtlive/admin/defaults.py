# -*- encoding: utf-8 -*- 
'''
REVIEW Különféle alapértelmezett adatokat szolgáltató funkciók:
 - currency list
 - timeframe list
 - symbol list
 - ...
 
@status: fejlesztés alatt
@author: kulcsarb
@newfield url: URL
'''

from gdtlive.constants import CURRENCY, INSTRUMENT, TIMEFRAME
from gdtlive.languages.default import OK
import cherrypy
from gdtlive.control.router import ajax_wrapper

@cherrypy.expose
@ajax_wrapper
def currencyList():
    '''
    Visszaadja a pénznemek listáját
    
    @url:    /default/currencyList
    @rtype:     dict
    @return:    A használható pénznemek név-érték szótára
    '''
    return True, OK, [{'currency': curr} for curr in CURRENCY]

@cherrypy.expose
@ajax_wrapper
def symbolList():
    '''
    Visszaadja az összes elérhető szimbólum nevet
    
    @url:    /default/symbolList 
    @rtype:     dict
    @return:    Az elérhető szimbólum nevek név-érték szótára
    '''
    return True, OK, [{'symbol': intr} for intr in INSTRUMENT]

@cherrypy.expose
@ajax_wrapper
def timeframeList():
    '''
    Visszaadja az összes elérhető időtáv nevét

    @url:    /default/timeframeList
    @rtype:     dict
    @return:    Az elérhető időtávok név-érték szótára
    '''
    data = []
    for key in sorted(TIMEFRAME.keys()):
        data.append({'timeframe': key, 'timeframe_str': TIMEFRAME[key]})
    return True, OK, data
