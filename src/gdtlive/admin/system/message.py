# -*- encoding: utf-8 -*- 
'''
REF Üzenetküldésre szolgáló funkciók

@status: refaktorálás szükséges a compose_mailben
@author: kulcsarb
@newfield url: URL


'''

from gdtlive.languages.default import GDTLIVE_SYS_MSG, WHILE_SENDING_EMAIL_TO_RECIPIENT, EXCEPTION_OCCURED, OK
import smtplib
import gdtlive.store.db as db
from gdtlive.config import SKIP_SENDING_MAIL, SMTP_SERVER#, MAIL_FROM
#import ast
import traceback
#from gdtlive.store import ormObj
from gdtlive.utils import datetime2str#, first0
from gdtlive.admin.system.log import errorlog, extra
import cherrypy
from gdtlive.control.router import ajax_wrapper

#@cherrypy.expose
#@ajax_wrapper
#def send_message(users, message, subject = GDTLIVE_SYS_MSG):
#    '''Üzenetet küld a megadott felhasználóknak
#    
#    A funkció a megadott felhasználók email címére elküldi a C{message} üzenetet
#    
#    @url: /message/send_message
#    
#    @type users:     list
#    @param users:    a felhasználók userID-jeinek listája
#    @type message:   string 
#    @param message:  a küldendő üzenet
#    
#    @rtype: (bool, str) tuple
#    @return: A művelet sikerességét jelző boolean és szöveges érték
#    ''' 
#    session = db.Session()
#    errMsgs = ""
#    try:
#        users = ast.literal_eval(users)
#        emails = session.query(db.User.email).filter(db.User.id.in_(users)).all()        
#        while (len(emails)):
#            email = emails.pop()[0]            
#            result = send_mail(MAIL_FROM, email, subject, message)    
#            if not result[0]:           
#                errMsgs += result[1] + WHILE_SENDING_EMAIL_TO_RECIPIENT + str(email) + "  "
#        return (not errMsgs, errMsgs)
#    except:
#        errorlog.error('Exception raised during admin system send_message', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
#        return False, EXCEPTION_OCCURED
#    finally:
#        session.close()
#    return True, OK


def send_mail(from_addr, to_addr, subject, body, filename=""):    
    '''E-mailt küld a modulban meghatározott L{smtp_server} szerveren keresztül
    
    @type from_addr: string
    @param from_addr: küldő e-mail címe
    @type to_addr: string
    @param to_addr: fogadó e-mail címe
    @type subject: string
    @param subject: a levél tárgya
    @type body: string
    @param body: a levél tartalma
    
    @return: a művelet sikerességét jelző tuple, ahol az első tag a művelet siketét jelzi (boolean), 
    a második pedig a hibaüzenetet tartalmazza (string)
    @rtype: tuple(boolean, string)
    '''    

    result = (True, OK)        
    if not SKIP_SENDING_MAIL:
        mail = compose_mail(from_addr, to_addr, subject, body,filename)                  
        try:
            server = smtplib.SMTP(SMTP_SERVER)
            server.sendmail(from_addr, to_addr, mail)
            server.quit()
        except (smtplib.SMTPConnectError, IOError,
               smtplib.SMTPRecipientsRefused,
               smtplib.SMTPHeloError,
               smtplib.SMTPSenderRefused,
               smtplib.SMTPDataError) as e:          
            result = (False, str(e))    
    return result


def compose_mail(fromaddr, toaddr, subject, body, filename=""):
    '''A megadott paraméterek szerint összeállít egy szabványos mail üzenetet

    @type from_addr: string
    @param from_addr: küldő e-mail címe
    @type to_addr: string
    @param to_addr: fogadó e-mail címe
    @type subject: string
    @param subject a levél tárgya
    @type body: string
    @param body: a levél tartalma

    @return: szabványos mail üzenet
    @rtype: string

    @todo: refaktorálni, ez a funkció nem tölthet be fileokat
    '''
    import base64
    if filename:
        encodedcontent = ""
        try:
            fo = open(filename, "r")
            filecontent = fo.read()
            encodedcontent = base64.b64encode(filecontent)  # base64
            fo.close()
        except:            
            pass        
        marker = "AUNIQUEMARKER"
        # main headers
        part1 = """From: %s
        To: %s
        Subject: %s
        MIME-Version: 1.0
        Content-Type: multipart/mixed; boundary=%s
        --%s
        """ % (fromaddr, toaddr, subject, marker, marker)
        
        # message body
        part2 = """Content-Type: text/plain
        Content-Transfer-Encoding:8bit
        
        %s
        --%s
        """ % (body,marker)
        
        # attachment section
        part3 = """Content-Type: multipart/mixed; name=\"%s\"
        Content-Transfer-Encoding:base64
        Content-Disposition: attachment; filename=%s
        
        %s
        --%s--
        """ %(filename, filename, encodedcontent, marker)
        message = part1 + part2 + part3
#        message = """From: %s
#        To: %s
#        Subject: %s
#        MIME-Version: 1.0
#        Content-Type: multipart/mixed; boundary=%s
#        --%s
#        Content-Type: text/plain
#        Content-Transfer-Encoding:8bit
#        
#        %s
#        --%s
#        Content-Type: multipart/mixed; name=\"%s\"
#        Content-Transfer-Encoding:base64
#        Content-Disposition: attachment; filename=%s
#        
#        %s
#        --%s--
#        """ %(fromaddr, toaddr, subject, marker, marker,body,marker,filename, filename, encodedcontent, marker)
    else:
        message = 'From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n%s' % (fromaddr, toaddr, subject, body)

    return message

#def save(userId, message):
#    '''
#    Resturns all recent messages for show them in popup.
#
#    @type  userId: int
#    @param userId: the user's id
#
#    @rtype:    (bool, str, list) tuple
#    @return:    Success, Message, The list of all popup messages that have not been shown yet
#    '''
#    session = db.Session()
#    try:
#        if not first0(session.query(db.User.popup_notify).filter(db.User.id==userId)):
#            return True
#        res = ormObj.save(db.Message, **{'userId': userId, 'userId': userId, 'message': message, 'id': 0})
#        return res[0]
#    except:
#        errorlog.error('Exception raised during admin system message save', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
#        return False
#    finally:
#        session.close()

@cherrypy.expose
@ajax_wrapper
def getRecents(userId):
    '''
    Resturns all recent messages for show them in popup.
    @url:     /message/getRecents

    @type  userId: int
    @param userId: the user's id

    @rtype:    (bool, str, list) tuple
    @return:    Success, Message, The list of all popup messages that have not been shown yet
    '''
    session = db.Session()
    try:
        res = []
        ids = []
        for messageId, time, message in session.query(db.Message.id, db.Message.time, db.Message.message).filter(db.Message.userId==userId).order_by(db.Message.time).all():
            res.append({'time': datetime2str(time), 'message':message})
            ids.append(messageId)
        if len(ids):
            session.query(db.Message).filter(db.Message.id.in_(ids)).delete('fetch')
            session.commit()
        return True, OK, res
    except:
        errorlog.error('Exception raised during admin system getRecents', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()
