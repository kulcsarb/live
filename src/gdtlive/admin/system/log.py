# -*- encoding: utf-8 -*- 
'''
OK Rendszernaplózással kapcsolatos függvények

@status: Ellenőrizve, rendben.
@author: kulcsarb
@newfield url: URL

'''
from gdtlive.languages.default import GDTLIVE_SYS_LOG
from logging.handlers import SMTPHandler, RotatingFileHandler, SysLogHandler
from gdtlive.config import LOG_MAIL_FROM, LOG_TO_TRAC, SMTP_SERVER, LOG_TRAC_EMAIL, LOG_TO_CONSOLE, SYSLOG_FACILITY, LOG_TO_SYSLOG
import logging
import pprint
import os.path
import cherrypy
from gdtlive.control.router import ajax_wrapper


log = logging.getLogger('gdtlive')
'''Normál szoftvertevékenység naplózására kijelölt csatorna. Az egyszerűbb, L{LOG_FORMAT}-ot használja. 
Fileba és konzolra küldi a naplókat'''

errorlog = logging.getLogger('errors')
'''Hibanaplózásra kijelölt csatorna. Az ide küldött hibák levélen elküldve bekerülnek a Trac-ba. 
A csatorna a L{TRAC_TICKET_FORMAT}, és a L{LOG_ERROR_FORMAT} formátumokat használja.'''

TRAC_TICKET_FORMAT = """
@type: bug
@milestone: Bugfix
@component: %(component)s
@priority: critical
@reporter: %(reporter)s
@version: %(version)s

 Time::
   %(asctime)s  
 Funtion::
   %(module)s.%(funcName)s
 Line::
   %(lineno)s

----
%(variables)s
----
EXCEPTION:
{{{
"""
'''A levélben küldött hibajelentés formátuma. A jelentés automatikusan bekerül a Trac rendszerbe'''

LOG_ERROR_FORMAT = "%(asctime)s - %(levelname)s - %(module)s.%(funcName)s:%(lineno)s - %(message)s%(variables)s"
'''Hibajelentés rövid formátuma, filba való mentésre'''
#LOG_FORMAT = "%(asctime)s - %(levelname)s - %(name)s - %(module)s.%(funcName)s:%(lineno)s - %(message)s"
LOG_FORMAT = "%(asctime)s - %(levelname)s - %(module)s.%(funcName)s:%(lineno)s - %(message)s"
SYSLOG_FORMAT = "%(levelname)s - %(name)s - %(module)s.%(funcName)s:%(lineno)s - %(message)s"
#LOG_FORMAT = "%(asctime)s - %(levelname)s - %(name)s - %(message)s"
'''Naplózás alapértelmezett formátuma'''
 
 
LOG_PATH = os.path.normpath(os.path.dirname(__file__) + '/../../gdtlive.log')
'''Az alapértelmezett naplófile helye és neve'''


class TracSMTPHandler(SMTPHandler):
    '''Az SMTPHandler leszármaztatása, hogy a küldött levélben beállítsunk a subjectet.'''
    def getSubject(self, record):
        '''A log rekord üzenet mezőjét adja vissza, ami így a levél subject-je lesz'''
        return record.getMessage()    

@cherrypy.expose
@ajax_wrapper
def get():
    '''Rendszernapló lekérése
    
    @url: /log/get
    @rtype:  text
    @return: rendszernapló
    '''
    content = ''
    fo = None    
    try: 
        fo = open(LOG_PATH, "r")
        content = fo.read() 
    except IOError: 
        pass
    finally:
        if fo:
            fo.close()
    return content 

@cherrypy.expose
@ajax_wrapper
def delete():
    '''Rendszernapló törlése

    @url: /log/delete

    @return: A művelet sikeresége, és a felhasználónak szánt üzenet
    @rtype: (bool, str) tuple
    '''
    result = (True, 'OK') 
    try:         
        os.remove(LOG_PATH)    
    except OSError, e:         
        if e.errno != 2:
            result = (False, str(e))    
    except Exception, e:
        result =  (False, str(e))    
    return result 

@cherrypy.expose
@ajax_wrapper
def email(email):
    '''Rendszernapló elküldése a megadott email címre

    @url: /log/email

    @type email:  string
    @param email: Ide küldi a program a rendszernaplót
    
    @return: A művelet sikeresége, és a felhasználónak szánt üzenet
    @rtype: (bool, str) tuple
    
    @todo: a log csatolmányként való küldését szebben megoldani
    '''
    from gdtlive.admin.system.message import send_mail 
    return send_mail(LOG_MAIL_FROM, email, GDTLIVE_SYS_LOG, "", LOG_PATH)


def initialize_loggers():
    '''
    Inicializálja a loggereket. 
    
    
    A GDTLIVE szoftver inditásakor ez a legelsőnek lefutó inicializálás.
    '''
    global errorlog, log    
    
    path = os.path.dirname(__file__) + '/../../'
    path = os.path.normpath(path)

    if LOG_TO_TRAC:
        # levélküldő handler a trac tickettel     
        trac_handler = TracSMTPHandler(SMTP_SERVER, LOG_MAIL_FROM, LOG_TRAC_EMAIL, '')    
        trac_handler.setFormatter(logging.Formatter(TRAC_TICKET_FORMAT))
        errorlog.addHandler(trac_handler)
    
    if LOG_TO_CONSOLE:
        # konzolra történő logolás
        console_handler = logging.StreamHandler()    
        console_handler.setFormatter(logging.Formatter(LOG_ERROR_FORMAT))
        errorlog.addHandler(console_handler)
    
    # fileba történő logolás
    file_handler = RotatingFileHandler(path+'/gdtlive_errors.log','a',10*1024*1024, 2)
    file_handler.setFormatter(logging.Formatter(LOG_ERROR_FORMAT))                    
    errorlog.addHandler(file_handler)
    
    errorlog.setLevel(logging.ERROR)
    
    log.setLevel(logging.INFO)
    
    # konzol logger
    if LOG_TO_CONSOLE:        
        handler = logging.StreamHandler()
        handler.setFormatter(logging.Formatter(LOG_FORMAT))
        handler.setLevel(logging.INFO)
        log.addHandler(handler)
        
    # file logger        
    handler = RotatingFileHandler(path+'/gdtlive.log','a',10*1024*1024, 2)
    handler.setFormatter(logging.Formatter(LOG_FORMAT))    
    log.addHandler(handler)
    
    if LOG_TO_SYSLOG:
        handler = SysLogHandler(address='/dev/log', facility=SYSLOG_FACILITY)
        handler.setFormatter(logging.Formatter(SYSLOG_FORMAT))
        log.addHandler(handler)
        
        handler = SysLogHandler(address='/dev/log', facility=SYSLOG_FACILITY)
        handler.setFormatter(logging.Formatter(LOG_ERROR_FORMAT))
        errorlog.addHandler(handler)
    

def extra(component='GDTLIVE szoftver', **kwargs):
    '''
    Az errorlog logger objektum extra paramétereit összeállító funkció.
    
    A funkció a megadott paraméterekből állítja össze a trac ticket végső formátumát. 
    
    @type component: str
    @param component:  A GDTLIVE Trac-ban definiált komponens. A bug ticket ehhez a komponenshez kerül bejegyzésre
    @type kwargs: mixed
    @param kwargs: tetszőleges számú kulcsszó paraméter. Céljuk, hogy a fejlesztőknek információt szolgáltassanak a megadott
    objektumok, változók értékéről a hiba bekövetkeztekor. Az itt megadott paraméterek név-érték párban bekerülnek a ticket szövegrészébe. 
    
    @rtype: dict
    @return: behelyettesítési értékek a L{TRAC_TICKET_FORMAT} számára
    '''
    variables = '\r\n'
    if kwargs:
        for key in kwargs.keys():
            variables += key.upper()+':\r\n'
            variables += '{{{\r\n' + pprint.pformat(kwargs[key], 1, 80)+'\r\n}}}\r\n'    
    
    result = {'component':component, 
              'version':'1.0',
              'reporter':'kulcsarb',
              'variables':variables}
    return result
