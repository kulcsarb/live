# -*- encoding: utf-8 -*- 
'''
REVIEW Spread és jutalék konfigurációk kezelése 

@status: fejlesztés alatt
@author: kulcsarb
@newfield url: URL
'''
#import gdtlive.store.db as db
from gdtlive.store.db import SpreadConfig#, SpreadConfigsRow
#from gdtlive.languages.default import SPREAD_IN_EVOL_CONFIG, GUI_REFRESH_NEEDED, OK, MISSING_DATA, EXCEPTION_OCCURED
import gdtlive.store.ormObj as ormObj 
#from gdtlive.admin.system.log import errorlog, extra
#import traceback
#import sqlalchemy as sa
import cherrypy
from gdtlive.control.router import ajax_wrapper#, store_wrapper
 
@cherrypy.expose
@ajax_wrapper
def listConfigs():
    '''Visszaadja az összes spread konfiguráció összes adatát
    
    @url:     /spread/listConfigs
    @rtype:    (bool, str, list) tuple
    @return:    Állapotjelzés, szöveges üzenet, a spread konfigurációk név-érték szótárának listája
    
    '''
    return ormObj.list(SpreadConfig)
    
#@cherrypy.expose
#@store_wrapper
#def saveConfig(**configData):
#    '''Eltárolja a megadott spread konfiguráció adatait.
#    
#    A funkció az adatokat kulcsszó paraméterekkel megadva várja. Amennyiben a configID=0, akkor új konfiguráció 
#    kerül létrehozásra, ellenben a meglévő konfig adatai módosulnak.
#    
#    @url:         /spread/saveConfig
#    @type configData:    dict
#    @param configData:   A spread konfiguráció adatainak név-érték szótára
#    
#    @rtype:    (bool, str, list) tuple
#    @return:    Állapotjelzés, szöveges üzenet, az elmentett spread konfiguráció adatai
#    '''
#    return ormObj.save(SpreadConfig,**configData) 
#
#@cherrypy.expose
#@store_wrapper
#def deleteConfig(id):
#    '''Törli a megadott spread konfigurációt a rendszerből. A
#    
#    @url:     /spread/deleteConfig
#    @type id: int
#    @param id: A spread konfiguráció azonosítója
#    
#    @rtype:    (bool, str, list) tuple
#    @return:    Állapotjelzés, szöveges üzenet, a törölt spread konfiguráció id-je
#    '''
#    if db.session.query(sa.func.count(db.EvolConfig.id)).filter(db.EvolConfig.spreadconfigId==id).scalar():
#        return False, SPREAD_IN_EVOL_CONFIG
#    return ormObj.delete(SpreadConfig,id)
#
#@cherrypy.expose
#@ajax_wrapper
#def listRows(id):
#    '''Viszaadja az adott spread konfigurációhoz tartozó instrumentum beállításokat.
#    
#    @url:     /spread/listRows
#    @type id: int
#    @param id: spread config azonositó L{SpreadConfig}.id
#    
#    @rtype:    (bool, str, list) tuple
#    @return:    Állapotjelzés, szöveges üzenet, az instrumentumok beállításainak listája
#    
#    @todo: unittest
#    '''
#    return ormObj.listByFilters(SpreadConfigsRow, [SpreadConfigsRow.configId==id])                
#
#@cherrypy.expose
#@store_wrapper
#def saveRow(**data): 
#    '''Elmenti egy spread konfigurációhoz tartozó instrumentum beállítás adatait. 
#    
#    @url:     /spread/saveRow
#    @type data: dict
#    @param data: egy instrumentumra vonatkozó spread beállítások
#    
#    @rtype:    (bool, str, dict) tuple
#    @return:    Állapotjelzés, szöveges üzenet, sikeres mentés esetén mentett spread beállítás dict-je  
#    
#    @todo: unittestek megirása   
#    '''    
#    if data['id'] != 0:
#        if data['exception_spread'] == '':
#            data['exception_spread'] = None
#            data['exception_from'] = None
#            data['exception_to'] = None
#            
#        return ormObj.save(SpreadConfigsRow, **data)
#    else:
#        session = db.Session()
#        try:
#            configId = data['configId']
#            spread_config = session.query(SpreadConfig).get(configId)
#            if not spread_config:
#                return False, GUI_REFRESH_NEEDED
#            row = db.SpreadConfigsRow(data['symbol'], data['spread'], data['exception_from'], data['exception_to'], data['exception_spread'])
#            spread_config.rows.append(row)
#            session.commit()                
#            return True, OK, dict(row)
#        except KeyError, e:
#            errorlog.error('Exception raised during spread saveRow', extra=extra(globals=globals(), locals=locals()), exc_info=str(e))
#            return False, MISSING_DATA + str(e)
#        except Exception, e:
#            errorlog.error('Exception raised during spread saveRow', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
#            return False, EXCEPTION_OCCURED
#        finally:
#            session.close()
#
#@cherrypy.expose
#@store_wrapper
#def deleteRow(id):
#    '''Törli a megadott spread konfiguráció sort. 
#    
#    @url:     /spread/deleteRow
#    @type id: int
#    @param id: az adott spread konfiguráció sor azonositója ( L{gdtlive.store.db.SpreadConfigsRow}.id )
#    
#    @rtype:    (bool, str, dict) tuple
#    @return:    Állapotjelzés, szöveges üzenet, a törölt beállítás ID-je
#    
#    @todo: unittestek megirása   
#    '''
#    return ormObj.delete(SpreadConfigsRow, id)
#    