# -*- encoding: utf-8 -*- 
'''
REVIEW Rollover konfigurációk kezelése

@status: fejlesztés alatt
@author: kulcsarb
@newfield url: URL
'''

from gdtlive.store.db import RolloverConfig#, RolloverConfigRow
#import gdtlive.store.db as db
#from gdtlive.languages.default import ROLLOVER_IN_EVOL_CONFIG, GUI_REFRESH_NEEDED, OK, MISSING_DATA, EXCEPTION_OCCURED
import gdtlive.store.ormObj as ormObj 
#from gdtlive.admin.system.log import errorlog, extra
#import traceback
#import sqlalchemy as sa
import cherrypy
from gdtlive.control.router import ajax_wrapper#, store_wrapper

@cherrypy.expose
@ajax_wrapper
def listConfigs():
    '''
    Visszaadja az összes rollover konfiguráció összes adatát
    
    @url:    /rollover/listConfigs
    @rtype:    (bool, str, list) tuple
    @return:    Állapotjelzés, szöveges üzenet, a rollover konfigurációk név-érték szótárának listája
    
    '''
    return ormObj.list(RolloverConfig)

#@cherrypy.expose
#@store_wrapper
#def saveConfig(**configData):
#    '''
#    Eltárolja a megadott rollover konfiguráció adatait.
#    
#    A funkció az adatokat kulcsszó paraméterekkel megadva várja. Amennyiben a configID=0, akkor új konfiguráció 
#    kerül létrehozásra, ellenben a meglévő konfig adatai módosulnak.
#    
#    @url: /rollover/saveConfig
#    @type configData:    dict
#    @param configData:   A rollover konfiguráció adatai
#    
#    @rtype:    (bool, str, list) tuple
#    @return:    Állapotjelzés, szöveges üzenet, az elmentett rollover konfiguráció adatai
#    '''
#    return ormObj.save(RolloverConfig,**configData)
#
#@cherrypy.expose
#@store_wrapper
#def deleteConfig(id):
#    '''
#    Törli a megadott rollover konfigurációt a rendszerből
#    
#    @url: /rollover/deleteConfig    
#    @type id: int
#    @param id: A konfiguráció azonosítója
#    @rtype:    (bool, str, list) tuple
#    @return:    Állapotjelzés, szöveges üzenet, a törölt rollover konfiguráció id-je 
#    '''
#    if db.session.query(sa.func.count(db.EvolConfig.id)).filter(db.EvolConfig.rolloverconfigId==id).scalar():
#        return False, ROLLOVER_IN_EVOL_CONFIG
#    return ormObj.delete(RolloverConfig,id)
#
#@cherrypy.expose
#@ajax_wrapper
#def listRows(id):
#    '''
#    Visszaadja az adott rollover konfiguráció összes kapcsolódó kamatláb beállítását
#    
#    @url:    /rollover/listRows
#    
#    @param id: Rollover konfiguráció azonosítója
#    @type id: int    
#    
#    @rtype:     list    
#    @return:    A kamatláb beállítások név-érték szótárának listája
#    
#    @todo: unittestek megirása 
#    '''
#    return ormObj.listByFilters(RolloverConfigRow, [RolloverConfigRow.configId==id])
#
#@cherrypy.expose
#@store_wrapper
#def saveRow(**data):
#    '''
#    Eltárol egy rollover konfigurációhoz tartozó kamatláb beállítást.
#    
#    A funkció az adatokat kulcsszó paraméterekkel megadva várja. Amennyiben a id=0, akkor új konfiguráció 
#    kerül létrehozásra, ellenben a meglévő konfig adatai módosulnak.
#    
#    @url: /rollover/saveRow
#    @type data:    dict
#    @param data:   A kamatláb beállítás adatai
#    
#    @rtype:    (bool, str, dict) tuple
#    @return:    Állapotjelzés, szöveges üzenet, sikeres mentés esetén mentett kamatláb beállítás dict-je 
#    
#    @todo: unittestek megirása  
#    '''
#    if data['id'] != 0:
#        return ormObj.save(RolloverConfigRow, **data)
#    else:
#        session = db.Session()
#        try:
#            configId = data['configId']
#            rollover_config = session.query(RolloverConfig).get(configId)
#            if not rollover_config:
#                return False, GUI_REFRESH_NEEDED
#            row = db.RolloverConfigRow(data['currency'], data['interest'])
#            rollover_config.rows.append(row)
#            session.commit()            
#            return True, OK, dict(row)
#        except KeyError, e:
#            errorlog.error('Exception raised during rollover saveRow', extra=extra(globals=globals(), locals=locals()), exc_info=str(e))
#            return False, MISSING_DATA + str(e)
#        except Exception, e:
#            errorlog.error('Exception raised during rollover saveRow', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
#            return False, EXCEPTION_OCCURED
#        finally:
#            session.close()
#
#@cherrypy.expose
#@store_wrapper
#def deleteRow(id):
#    '''
#    Törli a megadott kamatláb beállítást. Egy kamatláb megfelel egy L{gdtlive.db.store.RolloverConfigRow} objektumnak.
#    
#    @url: /rollover/deleteRow
#    
#    @type id: int
#    @param id: A kamatláb bállítás objektum azonositója ( L{gdtlive.db.store.RolloverConfigRow}.id )
#    
#    @rtype:    (bool, str, dict) tuple
#    @return:    Állapotjelzés, szöveges üzenet, a törölt beállítás ID-je
#    
#     @todo: unittestek megirása 
#    '''
#    return ormObj.delete(RolloverConfigRow, id)
