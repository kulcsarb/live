'''
Created on Jan 22, 2013

@author: gdt
'''
from datetime import timedelta, datetime, date
from gdtlive.core.constants import TRADE_OPENED, TRADE_CLOSING
import math
import logging
import traceback

log = logging.getLogger('gdtlive.reporting')

def get_week_num(from_date, to_date):
    fy, fw, fd =  from_date.isocalendar()
    ty, tw, td = to_date.isocalendar()
    week_num = 0    
    if ty == fy:
        week_num = tw - fw + 1
    else:
        week_num = (52 - fw) + 1
        week_num += ((ty-fy)-1) * 52
        week_num += tw
         
    return week_num
    

def get_month_num(from_date, to_date):
    month_num = 0
    if from_date.year == to_date.year:
        month_num = (to_date.month - from_date.month) + 1
    else:
        month_num = ((to_date.year - from_date.year) - 1 ) * 12
        month_num += (12 - from_date.month) + 1
        month_num += to_date.month
        
    return month_num


def get_day_num(from_date, to_date):
    return ((to_date.date() - from_date.date()) + timedelta(days = 1)).days    


def get_year_num(from_date, to_date):
    return (to_date.year - from_date.year) + 1
    

def standard_dev(data):
    length = len(data)
    avg = 0
    sum = 0
    diffFromAvg = 0 
    if not length: return 0

    for i in xrange(0, length):
        avg += data[i];
    
    avg /= float(length);
    for i in xrange(0, length):    
        diffFromAvg = data[i] - avg
        sum += diffFromAvg * diffFromAvg
    
    sum /= float(length);
    return math.sqrt(sum);


def calculate_performance(trades, starting_balance, from_date, to_date):
    r = {
         'starting_balance' : starting_balance,
         'net_profit' : 0,  #
         'gross_profit' : 0,    #
         'gross_loss' : 0,      #
         'cumulated_profit' : 0,    #
         'commission' : 0,          #
         'profit_factor' : 0,       #
         'maximum_drawdown' : 0,    #
         'sharpe_ratio' : 0,        #
         'number_of_trades' : 0,    #
         'winning_trades' : 0,      #
         'losing_trades' : 0,       #
         'average_trade' : 0,       #
         'percent_profitable' : 0,  #
         'average_winner' : 0,      #
         'average_loser' : 0,       #
         'average_wl_ratio' : 0,    #
         'max_conseq_winners' : 0,      #
         'max_conseq_losers' : 0,       # 
         'largest_winner' : 0,          #
         'largest_loser' : 0,           #
         'trades_per_day' : 0,          #
         'avg_time_in_market' : 0,      #
         'avg_bars_in_trade' : 0,
         'profit_per_month' : 0,        #
         'avgMAE' : 0,                  #
         'avgMFE' : 0,                  #
         'profit_variance' : 0,         #
         'open_trades' : 0,             #
         'floating_profit' : 0,         #
         'min_yearly_profit_p' : 0,
         'max_drawdown_p' : 0,          #
         'min_floating_p' : 0,          #
         'min_floating' : 0,            #
         'avg_neg_floating_p' : 0,
         'avg_pos_floating_p' : 0,
         'avg_neg_floating' : 0,
         'avg_pos_floating' : 0,
         'max_use_of_lev' : 0,
         'avg_use_of_lev' : 0,
         'chance1m' : 0,
         'chance3m' : 0,
         'score' : 0
         }
    try:
        avg_time_in_market = timedelta(seconds=0)
        trade_num = len(trades)
        if not trade_num:
            return r
        
        num_of_days = get_day_num(from_date, to_date)     
        num_of_months = get_month_num(from_date, to_date)
        
        monthly_profit = [0] * num_of_months
        r['number_of_trades'] = trade_num
        max_balance = balance = starting_balance
        sumMAE = 0
        sumMFE = 0
        conseq_winners = 0
        conseq_losers = 0
        for i, trade in enumerate(trades):
            try:
                if not trade.open_time or not trade.open_price:
                    continue
                
                r['net_profit'] += trade.profit
                r['gross_loss'] += trade.profit if trade.profit < 0 else 0
                r['gross_profit'] += trade.profit if trade.profit > 0 else 0
                r['cumulated_profit'] += trade.profit if trade.profit > 0 else 0
                r['winning_trades'] += 1 if trade.profit > 0 else 0
                r['losing_trades'] += 1 if trade.profit < 0 else 0
                r['largest_winner'] = max(r['largest_winner'], trade.profit)
                r['largest_loser'] = min(r['largest_loser'], trade.profit)
                r['open_trades'] += 1 if trade.state in (TRADE_OPENED, TRADE_CLOSING) else 0
                r['floating_profit'] += trade.floating_profit
                if trade.close_time:                
                    avg_time_in_market += trade.close_time - trade.open_time 
                                    
                sumMAE += trade.MAE
                sumMFE += trade.MFE        
                
                balance += trade.profit
                max_balance = max(balance, max_balance)
                r['maximum_drawdown'] = min(r['maximum_drawdown'], balance - max_balance)
                try:
                    r['max_drawdown_p'] = min(r['max_drawdown_p'],  (r['maximum_drawdown'] / balance) * 100.0)
                except:
                    r['max_drawdown_p'] = 0
                    
                r['min_floating'] = min(r['min_floating'], trade.MAE)
                                
                try:
                    r['min_floating_p'] = min(r['min_floating_p'], (r['min_floating_p'] / balance) * 100.0)
                except:
                    r['min_floating_p'] = 0
                
                if trade.profit > 0:
                    conseq_winners += 1
                    conseq_losers = 0
                elif trade.profit < 0:
                    conseq_winners = 0
                    conseq_losers += 1
                else:
                    conseq_winners = 0
                    conseq_losers = 0            
                r['max_conseq_winners'] = max(r['max_conseq_winners'], conseq_winners)
                r['max_conseq_losers'] = max(r['max_conseq_losers'], conseq_losers)
                
                if trade.close_time:
                    mi = get_month_num(from_date, trade.close_time)-1
                    monthly_profit[mi] += trade.profit
            except:
                log.critical(traceback.format_exc())
                
        if r['gross_loss']:
            r['profit_factor'] = r['gross_profit'] / abs(r['gross_loss']) 
        
        if num_of_days:
            r['trades_per_day'] = float(trade_num) / float(num_of_days)
            
        r['avgMAE'] = sumMAE / trade_num
        r['avgMFE'] = sumMFE / trade_num
        r['percent_profitable'] = (float(r['winning_trades']) / trade_num) * 100
        r['average_trade'] = r['net_profit'] / trade_num    
        r['average_loser'] = 0 if not r['losing_trades'] else r['gross_loss'] / float(r['losing_trades'])
        r['average_winner'] = 0 if not r['winning_trades'] else r['gross_profit'] / float(r['winning_trades'])
        r['average_wl_ratio'] = 0 if not r['average_loser'] else r['average_winner'] / r['average_loser']
        r['profit_per_month'] = r['net_profit'] / num_of_months
    
        r['avg_time_in_market'] = avg_time_in_market.seconds / (3600.0 * trade_num)
        
        r['profit_variance'] = standard_dev(monthly_profit);
        r['sharpe_ratio'] = 0 if not r['profit_variance'] else r['profit_per_month'] / r['profit_variance']
    except:
        log.critical(traceback.format_exc())
        
    return r


def calculate_serial(trades, starting_balance, from_date, to_date):
    try:
        
        result = {'net_profit' : [0],
                  'drawdown' : [[]],
                  'trades_profit_loss' : [[]],
                  'daily_profit' : [0],
                  'weekly_profit' : [0],
                  'monthly_profit' : [0],
                  'yearly_profit' : [0],
                  'MAE' : [[]],
                  'MFE' : [[]],
                  'floating_profit' : [],
                  'use_of_leverage' : [],
                  'beta' : []
                }
        
        num_of_days = get_day_num(from_date, to_date)     
        num_of_months = get_month_num(from_date, to_date)
        num_of_weeks = get_week_num(from_date, to_date)
        num_of_years = get_year_num(from_date, to_date)    
        
        account_balance = [0] * num_of_days
        drawdown_w = [[]] * num_of_weeks
        trades_profit_loss_w = [[]] * num_of_weeks
        daily_profit = [0] * num_of_days
        weekly_profit = [0] * num_of_weeks
        monthly_profit = [0] * num_of_months
        yearly_profit = [0] * num_of_years
        MAE_w = [[]] * num_of_weeks
        MFE_w = [[]] * num_of_weeks
            
        trades = filter(lambda x: x.close_time and x.open_time and x.open_price and x.close_price and x.close_type, trades)                
        trades = sorted(trades, key = lambda x: x.close_time)
        
        balance = starting_balance
        max_balance = balance
        for t in trades:
            di = get_day_num(from_date, t.close_time)-1
            wi = get_week_num(from_date, t.close_time)-1
            mi = get_month_num(from_date, t.close_time)-1
            yi = get_year_num(from_date, t.close_time)-1
            daily_profit[di] += t.profit
            weekly_profit[wi] += t.profit
            monthly_profit[mi] += t.profit
            yearly_profit[yi] += t.profit
            trades_profit_loss_w[wi].append(t.profit)
            MAE_w[wi].append(t.MAE)
            MFE_w[wi].append(t.MFE)
            balance += t.profit
            if balance > max_balance:
                max_balance = balance
            drawdown_w[wi].append(balance - max_balance)
        
        balance = starting_balance
        for i in xrange(num_of_days):
            balance += daily_profit[i]
            account_balance[i] = balance
                    
    
        result = {'net_profit' : account_balance,
                  'drawdown' : drawdown_w,
                  'trades_profit_loss' : trades_profit_loss_w,
                  'daily_profit' : daily_profit,
                  'weekly_profit' : weekly_profit,
                  'monthly_profit' : monthly_profit,
                  'yearly_profit' : yearly_profit,
                  'MAE' : MAE_w,
                  'MFE' : MFE_w,
                  'floating_profit' : [],
                  'use_of_leverage' : [],
                  'beta' : []
                } 
    except:
        log.critical(traceback.format_exc())
        
    return result
    