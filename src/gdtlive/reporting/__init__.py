import gdtlive.store.db as db 
from gdtlive.core.constants import TRADE_CLOSED
from calculator import calculate_serial
from datetime import timedelta


if __name__ == '__main__':
    db.init()
    
    session = db.Session()
    
    runs = session.query(db.StrategyRun.id, db.StrategyRun.strategyId, db.LiveRunConfig.running_from, db.LiveRunConfig.running_to).filter(db.StrategyRun.configId == db.LiveRunConfig.id).filter(db.StrategyRun.id==315).all()
    
    
    for id, sid, running_from, running_to in runs:
        print id, sid, running_from, running_to
        trades = session.query(db.Trade).filter(db.Trade.strategyrun_id == id).filter(db.Trade.state==TRADE_CLOSED).order_by(db.Trade.id).all()
    #    for trade in trades:
    #        print trade.open_time, trade.profit
        
        #d = (running_to.date() - running_from.date()) + timedelta(days = 1)
       
        #print d.week
        
        print calculate_serial(trades, 10000, running_from, running_to)
        
        
