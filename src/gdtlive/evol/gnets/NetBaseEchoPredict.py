# -*- encoding: utf-8 -*-
'''
Created on Nov 06, 2012

@author: gdtlive
'''
import gdtlive.c.signal as signal
from gdtlive.c.transform import difference as tr_diff
import numpy as np
import random
from gdtlive.constants import PIP_MULTIPLIER
from NetBase import NetPrices, createThreshold, sigmoid#, createLayer, sigmoidInvert

np.set_printoptions(threshold = 'nan', precision = 6)
# np.seterr(all = 'raise')

def signum(num):
    if num > 0:
        return 1
    if num < 0:
        return -1
    return 0

def sigmoidInvert(nparray):
    '''
    The inverted sigmoid function results a numpy array from [0, 1) interval to (-inf, inf)
    '''
    out = np.array(nparray)
    np.reciprocal(out, out = out)
    out = np.nan_to_num(out)
    out -= 1
    np.log(out, out = out)
    return out

def createLayer(minValue, maxValue, *size):
    '''
    Creates a random [minValue, maxValue) numpy array given by its' @param size
    '''
    out = np.random.rand(*size)
    out *= maxValue - minValue
    out += minValue
    return np.float32(out)

efoLimit = 0.85
def createEfo(*size):
    '''
    Creates a random [-efoLimit, efoLimit) numpy array given by its' @param size.
    '''
    return createLayer(-efoLimit, efoLimit, *size)

def getMinMax(mult, noConnection):
    maxValue = mult + noConnection
    minValue = -maxValue
    return (minValue, maxValue)

PredictCommands = ('CloseAllOpenDir', 'OpenWaitOrClose')
PredictHLCCommands = ('HLCDummy', )
classMap = {}
classMapRev = {}

class GNetEchoPredictNet(object):
    '''
    One predict net.
    @type layers:  list/None
    @param layers: layer node counts from bottom to up
    @type width:  tuple/None
    @param width: how many nodes can be on a layer
    @type height:  tuple/None
    @param width: how many layers can be in the net
    @type noConnection:  float
    @param noConnection: the probability that 2 nodes are not connected is noConnection:1
    @type connMult:  number
    @param connMult: the connection size range (without noConnection modifier)
    '''
    def __init__(self, echoFalloff = 0, priceNodes = None, noInit = False, width = None, height = None, noConnection = 0.5, connMult = 10, needEfo = True):
        if noInit:
            return
        self.needEfo = needEfo
        self.account = None
        self.layers = []
        self.thresholds = []
        if needEfo == True:
            self.efos = []
        else:
            self.echoFalloff = echoFalloff
        connMult *= 0.5
        noConnection *= connMult
        self.connMult = connMult
        self.noConnection = noConnection

        self.minWidth, self.maxWidth = width
        self.minHeight, self.maxHeight = height

        minValue, maxValue = getMinMax(connMult, noConnection)
        xSize = len(priceNodes)
        for layerIndex in xrange(random.randint(self.minHeight, self.maxHeight)):
            xSize, ySize = self.addLayer(xSize, minValue = minValue, maxValue = maxValue)
            xSize = ySize
        self.outputLayer = createLayer(minValue, maxValue, 1, xSize)

    def setDatarow(self, datarow):
        self.datarow = datarow
        self.clean()
        if hasattr(self, 'states'):
            states = True
            signalInputLength = self.lastSignalLength
            signalLength = self.signal.shape[0]
        else:
            self.states = []
            states = False
            signalInputLength = 0
            signalLength = 0
        datarowLength = self.datarow.shape[1]
        if states:
            if signalLength <= datarowLength:
                signalTmp = self.signal[0:signalInputLength]
                self.signal = np.zeros(max(signalLength + 100, datarowLength), dtype = np.double)
                self.signal[0:signalInputLength] = signalTmp
        else:
            for layer in self.layersCleaned:   #hidden state
                self.states.append(np.zeros(layer.shape[0]))
            self.states.append(np.zeros(1))
            self.signal = np.zeros(datarowLength, dtype = np.double)
        data = [np.zeros(len(self.priceNodesCleaned))]
        for layer in self.layersCleaned:
            data.append(np.zeros(layer.shape[0], dtype = np.int))
        for datarowIndex in xrange(signalInputLength, datarowLength):
            data[0] = self.datarow[self.priceNodesCleaned, datarowIndex]
            for index2, layer in enumerate(self.layersCleaned):
                if self.needEfo == True:
                    self.states[index2][:] = self.states[index2] * self.efosCleaned[index2] + np.dot(layer, data[index2])
                else:
                    self.states[index2][:] = self.states[index2] * self.echoFalloff + np.dot(layer, data[index2])
                data[index2 + 1][:] = self.states[index2] > self.thresholdsCleaned[index2]
            self.signal[datarowIndex] = np.dot(self.outputLayerCleaned, data[index2 + 1])
        self.lastSignalLength = datarowLength

    def encode(self, cleaned = False):
        '''Creates string representation of a GNet object.
        @rtype: str
        '''
        toEncode = ['minWidth', 'maxWidth', 'minHeight', 'maxHeight', 'noConnection', 'connMult', 'needEfo']
        toEncodeNp = ['outputLayer']
        toEncodeListNp = ['thresholds', 'layers']
        if self.needEfo == True:
            toEncodeListNp.append('efos')
        else:
            toEncode.append('echoFalloff')
        if cleaned:
            self.clean()
            layers = self.layers
            self.layers = self.layersCleaned
            outputLayer = self.outputLayer
            self.outputLayer = self.outputLayerCleaned
            thresholds = self.thresholds
            self.thresholds = [sigmoid(threshold) for threshold in self.thresholdsCleaned]
            if self.needEfo == True:
                efos = self.efos
                self.efos = [sigmoid(efo) for efo in self.efosCleaned]
            noConnection = self.noConnection
            self.noConnection = 0
        result = [{}, {}, {}]
        for i in toEncode:
            result[0][i] = getattr(self, i)
        for i in toEncodeNp:
            result[1][i] = getattr(self, i).tolist()
        for i in toEncodeListNp:
            result[2][i] = []
            obj = getattr(self, i)
            for j in obj:
                result[2][i].append(j.tolist())
        if cleaned:
            self.noConnection = noConnection
            self.layers = layers
            self.outputLayer = outputLayer
            self.thresholds = thresholds
            if self.needEfo == True:
                self.efos = efos
        return str(result)

    @classmethod
    def decode(myClass, net_str):
        '''Rebuilds net from string representation.
        @type genome_str : str
        @param genome_str : the string representation created by encode methode
        @rtype: myClass
        '''
        from ast import literal_eval
        result = myClass(noInit = True)
        toEncode = literal_eval(net_str)
        for key, value in toEncode[0].iteritems():
            setattr(result, key, value)
        for key, value in toEncode[1].iteritems():
            setattr(result, key, np.array(value).astype(np.float32))
        for key, value in toEncode[2].iteritems():
            obj = []
            for j in value:
                obj.append(np.array(j).astype(np.float32))
            setattr(result, key, obj)
        return result

    def addLayer(self, xSize = None, ySize = None, insert = None, minValue = None, maxValue = None):
        if insert is not None and insert > len(self.layers):
            raise Exception('layer cannot be inserted over output layer')
        if minValue is None:
            minValue, maxValue = getMinMax(self.connMult, self.noConnection)
        if not ySize:
            ySize = random.randint(self.minWidth, self.maxWidth)

        if not xSize:
            if len(self.layers) > insert:
                xSize = self.layers[insert].shape[1]
            else:
                xSize = self.outputLayer.shape[1]

        newLayer = createLayer(minValue, maxValue, ySize, xSize)
        newThresholds = createThreshold(ySize)
        if self.needEfo == True:
            newEfos = createEfo(ySize)

        if insert is None:
            self.layers.append(newLayer)
            self.thresholds.append(newThresholds)
            if self.needEfo == True:
                self.efos.append(newEfos)
        else:
            self.layers.insert(insert, newLayer)
            self.thresholds.insert(insert, newThresholds)
            if self.needEfo == True:
                self.efos.insert(insert, newEfos)
            if len(self.layers) > insert + 1:
                self.layers[insert + 1] = createLayer(minValue, maxValue, self.layers[insert + 1].shape[0], ySize)
            else:
                self.outputLayer = createLayer(minValue, maxValue, 1, ySize)

        return xSize, ySize

    def removeLayer(self, layerIndex, minValue = None, maxValue = None):
        if layerIndex >= len(self.layers):
            raise Exception('non existing layer cannot be removed')
        xSize = self.layers[layerIndex].shape[1]
        del self.layers[layerIndex]
        del self.thresholds[layerIndex]
        if self.needEfo == True:
            del self.efos[layerIndex]
        if minValue is None:
            minValue, maxValue = getMinMax(self.connMult, self.noConnection)
        if layerIndex == len(self.layers):
            self.outputLayer = createLayer(minValue, maxValue, 1, xSize)
        else:
            self.layers[layerIndex] = createLayer(minValue, maxValue, self.layers[layerIndex].shape[0], xSize)

    def addNode(self, layerIndex, nodeIndex, minValue = None, maxValue = None):
        if layerIndex >= len(self.layers):
            raise Exception('node cannot be inserted on output layer or over it')
        if nodeIndex > self.layers[layerIndex].shape[0]:
            raise Exception('node can only be inserted from before the first until after the last one')
        if minValue is None:
            minValue, maxValue = getMinMax(self.connMult, self.noConnection)
        self.thresholds[layerIndex] = np.insert(self.thresholds[layerIndex], nodeIndex, createThreshold(), axis=0)
        if self.needEfo == True:
            self.efos[layerIndex] = np.insert(self.efos[layerIndex], nodeIndex, createEfo(), axis=0)
        self.layers[layerIndex] = np.insert(self.layers[layerIndex], nodeIndex, createLayer(minValue, maxValue, 1, self.layers[layerIndex].shape[1]), axis=0)
        if layerIndex == len(self.layers) - 1:
            self.outputLayer = np.insert(self.outputLayer, nodeIndex, createLayer(minValue, maxValue), axis=1)
        else:
            self.layers[layerIndex + 1] = np.insert(self.layers[layerIndex + 1], nodeIndex, createLayer(minValue, maxValue, self.layers[layerIndex + 1].shape[0]), axis=1)

    def removeNode(self, layerIndex, nodeIndex, cleaned = False):
        if type(nodeIndex) == np.ndarray:
            maxNodeIndex = np.max(nodeIndex)
        else:
            maxNodeIndex = nodeIndex
        if cleaned == True:
            if layerIndex > len(self.layersCleaned):
                raise Exception('node cannot be removed over output layer')
            if layerIndex > -1:
                if maxNodeIndex >= self.layersCleaned[layerIndex].shape[0]:
                    raise Exception('node cannot be removed after the last one of the layer ')
        else:
            if layerIndex >= len(self.layers):
                raise Exception('node cannot be removed from output layer or over it')
            if maxNodeIndex >= self.layers[layerIndex].shape[0]:
                raise Exception('node cannot be removed after the last one of the layer')
        if cleaned:
            if layerIndex == -1:
                self.priceNodesCleaned = np.delete(self.priceNodesCleaned, nodeIndex, axis=0)
            elif layerIndex == len(self.layers):
                self.thresholdsCleaned[layerIndex] = np.delete(self.thresholdsCleaned[layerIndex], nodeIndex, axis=0)
                self.outputLayerCleaned = np.delete(self.outputLayerCleaned, nodeIndex, axis=0)
            else:
                self.thresholdsCleaned[layerIndex] = np.delete(self.thresholdsCleaned[layerIndex], nodeIndex, axis=0)
                if self.needEfo == True:
                    self.efosCleaned[layerIndex] = np.delete(self.efosCleaned[layerIndex], nodeIndex, axis=0)
                self.layersCleaned[layerIndex] = np.delete(self.layersCleaned[layerIndex], nodeIndex, axis=0)
        else:
            self.thresholds[layerIndex] = np.delete(self.thresholds[layerIndex], nodeIndex, axis=0)
            if self.needEfo == True:
                self.efos[layerIndex] = np.delete(self.efos[layerIndex], nodeIndex, axis=0)
            self.layers[layerIndex] = np.delete(self.layers[layerIndex], nodeIndex, axis=0)
        index = layerIndex + 1
        if cleaned:
            if index == len(self.layersCleaned):
                self.outputLayerCleaned = np.delete(self.outputLayerCleaned, nodeIndex, axis=1)
            else:
                self.layersCleaned[index] = np.delete(self.layersCleaned[index], nodeIndex, axis=1)
        else:
            if index == len(self.layers):
                self.outputLayer = np.delete(self.outputLayer, nodeIndex, axis=1)
            else:
                self.layers[index] = np.delete(self.layers[index], nodeIndex, axis=1)

    def clean(self):
        if hasattr(self, 'layersCleaned'):
            return
        self.priceNodesCleaned = range(self.layers[0].shape[1])
        self.layersCleaned = []
        self.thresholdsCleaned = []
        if self.needEfo == True:
            self.efosCleaned = []
        for index, layer in enumerate(self.layers):
            self.layersCleaned.append(np.array(layer))
            self.layersCleaned[index][np.abs(self.layersCleaned[index]) > self.connMult] = 0
            self.thresholdsCleaned.append(sigmoidInvert(self.thresholds[index]))
            if self.needEfo == True:
                self.efosCleaned.append(self.efos[index])
        self.outputLayerCleaned = np.array(self.outputLayer)
        self.outputLayerCleaned[np.abs(self.outputLayerCleaned) > self.connMult] = 0
        downSuccess = True
        upSuccess = True
        while downSuccess or upSuccess:
            downSuccess = self.cleanDown()
            upSuccess = self.cleanUp()

    def cleanDown(self):
        success = False
        for index in xrange(len(self.layersCleaned), -1, -1):
            if index == len(self.layersCleaned):
                layer = self.outputLayerCleaned
            else:
                layer = self.layersCleaned[index]
            places = np.where(np.sum(np.abs(layer), 0) == 0)[0]
            if places.shape[0] > 0:
                self.removeNode(index - 1, places, True)
                success = True
        return success

    def cleanUp(self):
        success = False
        for index in xrange(len(self.layersCleaned)):
            layer = self.layersCleaned[index]
            places = np.where(np.sum(np.abs(layer), 1) == 0)[0]
            if places.shape[0] > 0:
                places = places[self.thresholdsCleaned[index][places] >= 0]
                if places.shape[0] > 0:
                    self.removeNode(index, places, True)
                    success = True
        return success

    def __repr__(self):
        """String representation of Genome."""
        result = ''
        for index, layer in enumerate(self.layers):
            layer2 = np.array(layer)
            if type(layer2) != float:
                layer2 = layer2.astype(np.float32)    #no connection if abs(value) is over self.connMult
            layer2[abs(layer2) > self.connMult] = 0
            result += 'layer' + str(index) + ' connections:\n' + np.array_str(layer2, 160, 5) + '\n'
            result += 'layer' + str(index) + ' thresholds:\n' + np.array_str(self.thresholds[index], 160, 5) + '\n'
            if self.needEfo == True:
                result += 'layer' + str(index) + ' efos:\n' + np.array_str(self.efos[index], 160, 5) + '\n'
        result += 'output layer connections:\n' + np.array_str(self.outputLayer, 160, 5)
        return result

    def printStructure(self):
        """String representation of Genome structure"""
        print 'datarow layer has ' + str(self.layers[0].shape[1]) + ' nodes'
        print '\n'.join(['layer' + str(index) + ' has ' + str(layer.shape[0]) + ' nodes' for index, layer in enumerate(self.layers)])
        print 'output layer has ' + str(self.outputLayer.shape[0]) + ' nodes'

    def printStructureCleaned(self):
        """String representation of cleaned Genome structure"""
        self.clean()
        print 'datarow layer has ' + str(len(self.priceNodesCleaned)) + ' nodes'
        print '\n'.join(['layer' + str(index) + ' has ' + str(layer.shape[0]) + ' nodes' for index, layer in enumerate(self.layersCleaned)])
        print 'output layer has ' + str(self.outputLayerCleaned.shape[0]) + ' nodes'

    def exportStructure(self):
        self.clean()
        conn_count = []
        layers = []
        layer_count = 0
        node_complexity = np.zeros((3, self.layersCleaned[0].shape[1]))#, np.int)
        node_complexity[:2, :] = 1
        node_complexity[2:, :] = 2
        for layerIndex in xrange(len(self.layersCleaned) + 1):
            if layerIndex < len(self.layersCleaned):
                layer = self.layersCleaned[layerIndex]
                layers.append(layer.shape[0])
            else:
                layer = self.outputLayerCleaned
            conns = (layer != 0)
            node_complexity2 = np.zeros((3, layer.shape[0]))#, np.int)
            for node in xrange(layer.shape[0]):
                layerNode = conns[node, :]
                node_complexity2[0, node] = np.sum(node_complexity[0, :][layerNode])
                node_complexity2[1, node] = np.sum(node_complexity[1, :][layerNode]) + 1
                node_complexity2[2, node] = np.sum(node_complexity[2, :][layerNode]) + 1
            node_complexity = node_complexity2
            conn_count.append(np.sum(conns))
            if layer.shape[0] > 0:
                layer_count += 1
        res = [0] * 10
        res[0] = sum(layers) + len(self.priceNodesCleaned) + 1 #node count
        res[1] = min(layers)    #min(node per layer)
        res[2] = max(layers)    #max(node per layer)
        res[3] = sum(conn_count)    #connection count
        res[4] = np.std(conn_count) #standard deviation(connection per layer)
        res[5] = layer_count + 1    #layer count
        res[6] = np.std(layers)     #standard deviation(node per layer)
        res[7] = int(node_complexity[0, -1])    #node sum complexity no. 1
        res[8] = int(node_complexity[1, -1])    #node sum complexity no. 2
        res[9] = int(node_complexity[2, -1])    #node sum complexity no. 3
#         res[10] = node_complexity[2, -1] #node product complexity no. 1
#         res[11] = node_complexity[4, -1] #node product complexity no. 2
        return res

    def getNodeNum(self):
        self.clean()
        layers = []
        for layerIndex in xrange(len(self.layersCleaned)):
            layers.append(self.layersCleaned[layerIndex].shape[0])
        return sum(layers)

    def mutate(self, mutator, frequency, intensity, conn_freq, conn_int, thres_freq, thres_int, echo_freq, echo_int):
        """
        Called to mutate the genome.
        1. add/remove layer/node with frequency and intensity
        2. connection_count = the number of all values in layer matrices
            fr = conn_freq / 2000
            int = conn_int / 100
            a) generates (fr * connection_count) places where values will be modified
            b) changes values at the given places by the given mutator and intensity
        3. node_count = the number of all nodes
            fr = thres_freq / 2000
            int = thres_int / 100
            a) generates (fr * node_count) places where values will be modified
            b) changes values at the given places by the given mutator and intensity
        """
        if hasattr(self, 'layersCleaned'):
            del self.layersCleaned
        self.mutateStructure(frequency * 0.01, intensity * 0.01)
        self.mutateConnections(mutator, conn_freq * 0.001, conn_int * 0.001)
        self.mutateThresholds(mutator, thres_freq * 0.002, thres_int * 0.01)
        self.mutateEcho(mutator, echo_freq * 0.002, echo_int * 0.002)

    def mutateStructure(self, freq, intens):
        '''
        Mutate structure: add/remove node/layer
        a) if [0, 1) random number >= fr -> skip add/remove layer/node
        b) if [0, 1) random number >= int/2 -> node add/remove
            else -> layer add/remove
        c) if r = [0, 1) random number >= 0.5 -> remove
            else -> add
        '''
        if random.random() >= freq:         #the action may not be needed at all
            return

        if random.random() * 2 < intens:    #add/remove layer
            target = 'layer'
        else:                               #add/remove node
            target = 'node'

        #choosing action
        #choosing where to add or which layer/node to remove
        layerCount = len(self.layers)
        if random.random() < 0.5:           #add layer/node
            action = 'add'
            if target == 'node':
                random_layer = random.randint(0, layerCount - 1)
                nodeCount = self.layers[random_layer].shape[0]
                if nodeCount == self.maxWidth:      #maximum node count on the layer
                    return
                random_node = random.randint(0, self.layers[random_layer].shape[0])
            else:
                if layerCount == self.maxHeight:    #maximum layer count
                    return
                random_layer = random.randint(0, layerCount)
        else:                               #remove layer/node
            action = 'remove'
            if target == 'node':
                random_layer = random.randint(0, layerCount - 1)
                node_count = self.layers[random_layer].shape[0]
                if node_count == self.minWidth:     #minimum node count on the layer
                    return
                random_node = random.randint(0, node_count - 1)
            else:
                if layerCount == self.minHeight:    #minimum layer count
                    return
                random_layer = random.randint(0, layerCount - 1)

        #do the action
        minValue, maxValue = getMinMax(self.connMult, self.noConnection)
        if action == 'add':
            if target == 'layer':
                self.addLayer(insert = random_layer, minValue = minValue, maxValue = maxValue)
            else:
                self.addNode(random_layer, random_node, minValue = minValue, maxValue = maxValue)
        else:
            if target == 'layer':
                self.removeLayer(random_layer, minValue = minValue, maxValue = maxValue)
            else:
                self.removeNode(random_layer, random_node)

    def mutateConnections(self, mutator, freq, intens):
        '''
        Selects @param freq rate count of all connections' weight and changes them
        with the given mutator and the available min and max possible values.
        '''
        minValue, maxValue = getMinMax(self.connMult, self.noConnection)
        for layer in self.layers:
            toChange = np.random.rand(*layer.shape) < freq
            layer[toChange] = mutator(layer[toChange], minValue, maxValue, intens)

    def mutateThresholds(self, mutator, freq, intens):
        '''
        Selects @param freq rate count of all thresholds and changes them
        with the given mutator and the available min and max possible values.
        '''
        for threshold in self.thresholds:
            toChange = np.random.rand(*threshold.shape) < freq
            if np.any(threshold[toChange]):
                threshold[toChange] = mutator(threshold[toChange], 0.0, 0.9999, intens)

    def mutateEcho(self, mutator, freq, intens):
        if self.needEfo == True:
            for efo in self.efos:
                toChange = np.random.rand(*efo.shape) < freq
                if np.any(efo[toChange]):
                    efo[toChange] = mutator(efo[toChange], -efoLimit, efoLimit, intens)
        else:
            self.echoFalloff = mutator(self.echoFalloff, -efoLimit, efoLimit, intens)

    def checkConsistency(self):
        '''
        Checks consistency of the net's matrices.
        '''
        ySize, xSize = self.layers[0].shape
        if hasattr(self, 'datarow') and self.datarow.shape[0] != xSize:
            raise Exception('historic datarow count and layer0 do not fit')
        if ySize != len(self.thresholds[0]):
            raise Exception('layer0 connections and thresholds count are not the same')
        if self.needEfo == True:
            if ySize != len(self.efos[0]):
                raise Exception('layer0 connections and efos count are not the same')
        for index in xrange(1, len(self.layers)):
            xSize = ySize
            lShape = self.layers[index].shape
            ySize = lShape[0]
            if lShape[1] != xSize:
                raise Exception('layer' + str(index - 1) + ' and layer' + str(index) + ' do not fit')
            if ySize != len(self.thresholds[index]):
                raise Exception('layer' + str(index - 1) + ' connections and thresholds count are not the same')
            if self.needEfo == True:
                if ySize != len(self.efos[index]):
                    raise Exception('layer' + str(index - 1) + ' connections and efos count are not the same')
        xSize = ySize
        if self.outputLayer.shape[1] != xSize:
            raise Exception('layer' + str(index) + ' and output layer do not fit')
        return True

    def checkConsistencyCleaned(self):
        '''
        Checks consistency of the cleaned net's matrices.
        '''
        self.clean()
        ySize, xSize = self.layersCleaned[0].shape
        if len(self.priceNodesCleaned) != xSize:
            raise Exception('historic datarow count and layer0 do not fit')
        if ySize != len(self.thresholdsCleaned[0]):
            raise Exception('layer0 connections and thresholds count are not the same')
        if self.needEfo == True:
            if ySize != len(self.efosCleaned[0]):
                raise Exception('layer0 connections and efos count are not the same')
        for index in xrange(1, len(self.layersCleaned)):
            xSize = ySize
            lShape = self.layersCleaned[index].shape
            ySize = lShape[0]
            if lShape[1] != xSize:
                raise Exception('layer' + str(index - 1) + ' and layer' + str(index) + ' do not fit')
            if ySize != len(self.thresholdsCleaned[index]):
                raise Exception('layer' + str(index - 1) + ' connections and thresholds count are not the same')
            if self.needEfo == True:
                if ySize != len(self.efosCleaned[index]):
                    raise Exception('layer' + str(index - 1) + ' connections and efos count are not the same')
        xSize = ySize
        if self.outputLayerCleaned.shape[1] != xSize:
            raise Exception('layer' + str(index) + ' and output layer do not fit')
        return True

    def draw(self, filename, edgeLabels = False, thresholdLabels = True, efoLabels = True, cleaned = True):
        import pydot
        graph = pydot.Dot(graph_type = 'graph')
        nodes = []
        nodes2 = nodes
        if cleaned == True:
            self.clean()
            layers = self.layersCleaned
            thresholds = self.thresholdsCleaned
            outputLayer = self.outputLayerCleaned
            efos = self.efosCleaned
        else:
            layers = self.layers
            thresholds = self.thresholds
            outputLayer = self.outputLayer
            efos = self.efos
        for j in xrange(layers[0].shape[1]):
            node = pydot.Node("datarow %i" % j, )
            nodes.append(node)
            graph.add_node(node)
        for i, layer in enumerate(layers):
            nodes2 = nodes
            nodes = []
            for j in xrange(layer.shape[0]):
                args = {}
                label = ''
                if thresholdLabels == True:
                    label += str(thresholds[i][j].round(3))
                if efoLabels == True:
                    if len(label) != 0:
                        label += "; "
                    label += str(efos[i][j].round(3))
                if len(label) != 0:
                    args['label'] = label
                node = pydot.Node("layer %i, node %i" % (i, j), **args)
                nodes.append(node)
                graph.add_node(node)
                for k in xrange(layer.shape[1]):
                    if layer[j, k] != 0:
                        args = {}
                        if edgeLabels:
                            args['label'] = layer[j, k].round(3)
                        graph.add_edge(pydot.Edge(nodes2[k], node, **args))
        node = pydot.Node("output")
        nodes.append(node)
        graph.add_node(node)
        nodes2 = nodes
        nodes = []
        for k in xrange(outputLayer.shape[1]):
            if np.abs(outputLayer[0, k]) > 0:
                args = {}
                if edgeLabels:
                    args['label'] = outputLayer[0, k].round(3)
#                 print k, nodes2[k].get_name()
                graph.add_edge(pydot.Edge(nodes2[k], node, **args))
        if cleaned == False:
            filename += '_'
        graph.write_png('%s.png' % filename)

    def clone(self):
        """
        Clone this net.
        """
        return GNetEchoPredictNet.decode(self.encode())


class GNetEchoPredict(object):
    description = 'Default strategy without predefined structure'
    '''
    Net based model for genome.
    @type netSize:  list/tuple/None
    @param netSize: the minimum and maximum number of net nodes
    Other parameters are added to net nodes
    '''
    def __init__(self, echoFalloff = 0.9, symbols = None, noInit = False, layerSizes = None, nodeTypes = None, commandParams = None, *args, **kw):
        self.class_type = classMapRev[type(self)]
        self.evaluated = False
        self.fitness = 0.0
        self.vote_threshold = 0.0
        if noInit:
            return
        if symbols is None:
            raise Exception('Symbols must be set!')
        if type(symbols) == str or type(symbols) == unicode:
            symbols = (symbols, )
        self.symbol = random.choice(symbols)
        if layerSizes:
            height = (layerSizes[0][0], layerSizes[1][0])
            width = (layerSizes[0][1], layerSizes[1][1])
        else:
            height = None
            width = None

        self.priceNodes = []
        commandNodes = []
        for nodeType, value in nodeTypes.iteritems():
            if not value:
                continue
            if nodeType in PredictCommands:
                commandNodes.append(nodeType)
            elif nodeType in NetPrices.keys():
                self.priceNodes.append(NetPrices[nodeType])
        self.nets = {self.class_type: GNetEchoPredictNet(echoFalloff = echoFalloff, height = height, width = width, priceNodes = self.priceNodes, *args, **kw)}
        self.commandFunction = random.choice(commandNodes)
        self.commandParams = commandParams

    def prepareForBacktest(self, datarows, account):
        self.account = account
        datarows = datarows[self.symbol]
        pip_m = PIP_MULTIPLIER[self.symbol]
        firstNet = self.nets.keys()[0]
        if hasattr(self.nets[firstNet], 'datarow'):
            datarowLength = getattr(self.nets[firstNet], 'datarow').shape[1]
            if datarows[1].shape[0] <= datarowLength:
                return
            datarowLength -= 1
        else:
            datarowLength = 0
        datarows = np.array([tr_diff(datarow[datarowLength:], 1, pip_m) for price, datarow in datarows.iteritems() if price in self.priceNodes])
        if datarowLength > 0:
            datarows = np.hstack((self.nets[firstNet].datarow, datarows))

        for net in self.nets.itervalues():
            net.setDatarow(datarows)
        self.signal = self.nets[firstNet].signal
        self.execute = getattr(self, '_execute' + self.commandFunction)

    def executeParams(self):
        result = {}
        if hasattr(self, 'strategy_id'):
            result['strategy_id'] = self.strategy_id
        return result

    def _executeCloseAllOpenDir(self, index):
        self.account.closeAll(**self.executeParams())
        if self.nets[self.class_type].signal[index] > 0:
            self.account.buy(self.symbol, **self.executeParams())
        elif self.nets[self.class_type].signal[index] < 0:
            self.account.sell(self.symbol, **self.executeParams())

    def _executeOpenWaitOrClose(self, index):
        net = self.nets[self.class_type]
        curr_dir = signum(net.signal[index])
        if curr_dir == 0:
            self.account.closeAll(**self.executeParams())
        elif curr_dir == 1:
            self.account.buyOrHold(self.symbol, **self.executeParams())
        else:
            self.account.sellOrHold(self.symbol, **self.executeParams())

    def __repr__(self):
        result = ''
        for index, net in self.nets.iteritems():
            result += 'net' + str(index) + ':\n' + net.__repr__() + '\n'
        return result

    def printStructure(self):
        """String representation of Genome structure"""
        for net in self.nets.itervalues():
            net.printStructure()

    def mutate(self, *args, **kw):
        """
        Called to mutate the genome.
        """
        for net in self.nets.itervalues():
            net.mutate(*args, **kw)

    def checkConsistency(self):
        '''
        Checks consistency of the net's matrices.
        '''
        for net in self.nets.itervalues():
            net.checkConsistency()
        return True

    def encode(self, cleaned = False):
        '''Creates string representation of a GNet object.
        @rtype: str
        '''
        toEncode = ['symbol', 'priceNodes', 'fitness', 'evaluated', 'commandFunction', 'commandParams', 'vote_threshold']
        result = [{}, [], self.class_type]
        for i in toEncode:
            result[0][i] = getattr(self, i)
        for key, value in self.nets.iteritems():
            result[1].append(key)
            result[1].append(value.encode(cleaned))
        return str(result)

    @staticmethod
    def decode(genome_str):
        '''Rebuilds net from string representation.
        @type genome_str : str
        @param genome_str : the string representation created by GNet.encode methode
        @rtype: GNet
        '''
        from ast import literal_eval
        toEncode = literal_eval(genome_str)
        result = classMap[toEncode[2]](noInit = True)
        for key, value in toEncode[0].iteritems():
            setattr(result, key, value)
        result.nets = {}
        for index in xrange(0, len(toEncode[1]), 2):
            result.nets[toEncode[1][index]] = GNetEchoPredictNet.decode(toEncode[1][index + 1])
        return result

    def getNodeNum(self):
        res = 0
        for net in self.nets.itervalues():
            res += net.getNodeNum()
        return res

    def getNets(self):
        return self.nets

    def getCommandNodes(self):
        return [self]

    def getSymbols(self):
        return set((self.symbol, ))

    def clone(self):
        cloned = GNetEchoPredict.decode(self.encode())
        cloned.fitness = 0.0
        cloned.evaluated = False
        return cloned

    def mergeWith(self, high, low, checkTypes = True):
        if checkTypes:
            predicts = {'high': 0, 'low': 0, 'close': 0}
            for predict in (self, high, low):
                predicts[predict.class_type] = predict
            if predicts['high'] == 0 or predicts['low'] == 0 or predicts['close'] == 0:
                raise Exception('Not all of "close", "high" and "low" are given!')
        else:
            predicts = [high, low, self]   #HLC
        result = GNetEchoPredictHLC(noInit = True)
        result.nets = predicts
        result.symbol = self.symbol
        return result

    def getSymbol(self):
        return self.symbol

    def __str__(self):
        return str(self.commandFunction)



class GNetEchoPredictClose(GNetEchoPredict):
    pass

class GNetEchoPredictHigh(GNetEchoPredict):
    pass

class GNetEchoPredictLow(GNetEchoPredict):
    pass


class GNetEchoPredictMulti(GNetEchoPredict):
    def prepareForBacktest(self, datarows, account):
        self.account = account
        datarows = datarows[self.symbol]
        pip_m = PIP_MULTIPLIER[self.symbol]
        #if hasattr(self, 'log'): self.log.info('strategy %d datarow shape: %d' % (self.strategy_id, str(datarows[1].shape)))
        datarowLength = datarows[1].shape[0]

        firstNet = self.nets[self.nets.keys()[0]]

        if hasattr(self, 'signal'):
            signalInputLength = firstNet.datarow.shape[1]
            signalLength = self.signal.shape[0]
            if signalLength <= datarowLength:
                signalTmp = self.signal[0:signalInputLength]
                self.signal = np.zeros(max(signalLength + 1, datarowLength), dtype = np.double)
                self.signal[0:signalInputLength] = signalTmp
            datarowLength = signalInputLength
            if datarows[1].shape[0] <= datarowLength:
                return
            datarowLength -= 1
            later = True
        else:
            signalInputLength = 0
            signalLength = 0
            self.signal = np.zeros(datarowLength, dtype = np.double)
            datarowLength = 0
            later = False

        datarows = np.array([tr_diff(datarow[datarowLength:], 1, pip_m) for price, datarow in datarows.iteritems() if price in self.priceNodes])
        if datarowLength > 0:
            if later == True:
                datarows = np.hstack((firstNet.datarow, datarows[:, 1:]))
            else:
                datarows = np.hstack((firstNet.datarow, datarows))

        datarowLength = datarows[1].shape[0]
        for net in self.nets.itervalues():
            net.setDatarow(datarows)
        for i in xrange(signalInputLength, datarowLength):
            if hasattr(self, 'vote_threshold') and self.vote_threshold:
                signals = {1: 0, -1: 0, 0:0}
                for net in self.nets.itervalues():
                    signals[signum(net.signal[i])] += 1
                long_count = float(signals[1])
                short_count = float(signals[-1])
                sum = long_count + short_count
                if sum and long_count / sum > short_count / sum + self.vote_threshold:
                    self.signal[i] = 1                
                elif sum and short_count / sum > long_count / sum + self.vote_threshold:
                    self.signal[i] = -1                 
                else:
                    self.signal[i] = 0
            else:
                signal = 0
                for net in self.nets.itervalues():
                    signal += signum(net.signal[i])
                self.signal[i] = signum(signal)
        self.execute = getattr(self, '_execute' + self.commandFunction)

    def _executeCloseAllOpenDir(self, index):
        self.account.closeAll(**self.executeParams())

        currentDir = self.signal[index]
        if currentDir == 1:
            self.account.buy(self.symbol, **self.executeParams())
        elif currentDir == -1:
            self.account.sell(self.symbol, **self.executeParams())

    def _executeCloseAllOpenDirInvert(self, index):
        self.account.closeAll(**self.executeParams())

        currentDir = self.signal[index]
        if currentDir == -1:
            self.account.buy(self.symbol, **self.executeParams())
        elif currentDir == 1:
            self.account.sell(self.symbol, **self.executeParams())

    def _executeOpenOrHold(self, index):
        direction = self.signal[index]
        if hasattr(self, 'log'): self.log.info('strategy %d - Vote: %d' % (self.strategy_id, direction))

        if direction == 1:
            self.account.buyOrHold(self.symbol, **self.executeParams())
        elif direction == -1:
            self.account.sellOrHold(self.symbol, **self.executeParams())
        else:
            self.account.closeAll(**self.executeParams())


    def _executeOpenOrHoldInvert(self, index):
        direction = self.signal[index]
        if hasattr(self, 'log'): self.log.info('strategy %d - Vote: %d' % (self.strategy_id, direction))

        if direction == -1:
            self.account.buyOrHold(self.symbol, **self.executeParams())
        elif direction == 1:
            self.account.sellOrHold(self.symbol, **self.executeParams())
        else:
            self.account.closeAll(**self.executeParams())


class GNetEchoPredictHLC(GNetEchoPredict):
    def _executeHLCDummy(self, index):  #TODO: ide lehet új commandokat hegeszteni
        self.account.closeAll(**self.executeParams())
        #close signal: self.nets['close'].signal
        #high signal: self.nets['high'].signal
        #low signal: self.nets['low'].signal
        if self.nets['close'].signal > 0:
            self.account.buy(self.symbol, **self.executeParams())
        elif self.nets['close'].signal < 0:
            self.account.sell(self.symbol, **self.executeParams())


classMap[''] = GNetEchoPredict
classMap['close'] = GNetEchoPredictClose
classMap['high'] = GNetEchoPredictHigh
classMap['low'] = GNetEchoPredictLow
classMap['hlc'] = GNetEchoPredictHLC
classMap['multi_close'] = GNetEchoPredictMulti
classMap['multi_high'] = GNetEchoPredictMulti
classMap['multi_low'] = GNetEchoPredictMulti
for key, value in classMap.iteritems():
    classMapRev[value] = key

def mergePredict(idList, commandFunction, vote_threshold=0.0):
    import gdtlive.store.db as db
    from gdtlive.store.db import Strategy
    db.init()
    session = db.Session()
    newStrat = None
    for index, dns in enumerate(session.query(Strategy.dns).filter(Strategy.id.in_(idList))):
        if index == 0:
            newStrat = GNetEchoPredict.decode(dns[0])
            class_type = 'multi_' + newStrat.class_type
            newStrat.class_type = class_type
            newStrat = newStrat.clone()
            newStrat.class_type = class_type
            newStrat.nets = {index: newStrat.nets[newStrat.nets.keys()[0]]}
        else:
            tmpStrat = GNetEchoPredict.decode(dns[0])
            newStrat.nets[index] = tmpStrat.nets[tmpStrat.nets.keys()[0]]
    newStrat.commandFunction = commandFunction
    newStrat.vote_threshold = vote_threshold
    return newStrat
