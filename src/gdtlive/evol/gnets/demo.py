import gdtlive.store.db as db
from gdtlive.historic.datarow import load
from NetBase import GNet, NetSignals, NetOnOffSignalCommands, NetOnAndOffSignalCommands, NetPrices
from gdtlive.constants import PRICE_ASKOPEN, PRICE_ASKHIGH, PRICE_ASKLOW, PRICE_ASKCLOSE
import numpy
from datetime import datetime

#print thresholdFill(0.5, 10, 1)
#exit()

sql_url = 'localhost/gnet_test' #to /etc/gdtlive.conf
symbol = "EURUSD", 
layers = [[1, 4, 12], [4, 6, 12]]
timeframe_num = 1440
strategies = range(10)

#from gdt.evol.plugin import mutator_np_list

command_class = 'OpenOrHold'
invert = False
from gdtlive.evol.gnets.NetBaseEchoPredict import mergePredict
gnet = mergePredict(strategies, command_class + ('Invert' if invert else ''))

#gnet = GNet(symbols = symbol, layerSizes = layers, nodeTypes = list(NetSignals) + list(NetOnOffSignalCommands) + list(NetOnAndOffSignalCommands) + NetPrices.keys())
#print dumps(gnet)
#exit()
#print 'gnet:'
#gnet.printStructure()
#print 'gnet.commandGroups', gnet.commandGroups
#gnet2 = GNet()
#print 'gnet2:'
#gnet2.printStructure()
#print 'gnet2.commandGroups', gnet2.commandGroups
#gnet.mergeWith(gnet2)
#print 'merged:'
#gnet.printStructure()
#print 'merged.commandGroups', gnet.commandGroups
##gnet.printStructure()
#for i in xrange(2):
##    gnet.mutateStructure(1, 1)
#    gnet.mutate(mutator_np_list[3], 20, 50, 20, 50, 20, 50)
##    gnet.printStructure()
#    gnet.checkConsistency()
#    print gnet
##gnet.printStructure()
#exit()

sql_url = sql_url
db.init()
session = db.Session()
for datarow in session.query(db.DatarowDescriptor).all():
    row = dict(datarow)
    datarows_np = {}
    if str(row['symbol']) in symbol and row['timeframe_num'] == timeframe_num:
        break

from_date = datetime(2011, 1, 1)
to_date = datetime(2011, 3, 1)
datarows = load(row['id'], from_date, to_date)
datarows_np[row['symbol']] = {}
for i in xrange(len(datarows[0]) - 5, len(datarows[0])):
    for price in (PRICE_ASKOPEN, PRICE_ASKHIGH, PRICE_ASKLOW, PRICE_ASKCLOSE):
        datarows_np[row['symbol']][price] = numpy.array(datarows[price][:i], dtype=numpy.double)
    gnet.prepareForBacktest(datarows_np, None)
    print gnet.signal[40:50]
exit()
commands = gnet.getCommandNodes()
for c in commands:
    res = c()
    print res
#    c.symbol = self.instruments.index(c.symbol)
#    print float(numpy.sum(res)) / res.shape[1]
gnet.fitness = -0.2
print gnet.fitness