# -*- encoding: utf-8 -*-
'''
Created on Nov 06, 2012

@author: gdt
'''
import gdtlive.c.signal as signal
from gdtlive.c.transform import difference as tr_diff
import numpy as np
import random
from gdtlive.constants import PIP_MULTIPLIER
from NetBase import createThreshold, createLayer, NetSignals, NetOnOffSignalCommands, NetOnAndOffSignalCommands, NetPrices, sigmoidInvert#, sigmoid

np.set_printoptions(threshold = 'nan', precision = 6)
#np.seterr(all = 'raise')

class GNetEchoCommand(object):
    '''
    One command layer and all below.
    @type command:  string/None
    @param command: the command layer's type
    @type signal:  string/None
    @param signal: the signal layer's type
    @type layers:  list/None
    @param layers: layer node counts from bottom to up
    @type width:  tuple/None
    @param width: how many nodes can be on a layer
    @type height:  tuple/None
    @param width: how many layers can be in the net
    @type noConnection:  float/None
    @param noConnection: the probability that 2 nodes are not connected is noConnection:1
    @type outputBinary:  list/None
    @param noConnection: a value for each layer, if the output is needed in binary format
    '''
    def __init__(self, echoFalloff = 0, group_id = 0, symbol = None, priceNodes = None, signalFunctions = None, commandFunctions = None, noInit = False, width = None, height = None, noConnection = 0.5, outputBinary = None):
        if noInit:
            return
        self.echoFalloff = echoFalloff
        self.account = None
        self.group_id = group_id
        self.layers = []
        self.thresholds = []
        self.noConnection = noConnection
        self.symbol = symbol

        self.priceNodes = priceNodes
        self.datarowCount = len(priceNodes)                   #number of historic sources

        if width:
            self.minWidth, self.maxWidth = width
        else:
            self.minWidth = 4
            self.maxWidth = 12

        if height:
            self.minHeight, self.maxHeight = height
        else:
            self.minHeight = 6
            self.maxHeight = 12

        xSize = self.datarowCount
        for layerIndex in xrange(random.randint(self.minHeight, self.maxHeight)):
            xSize, ySize = self.addLayer(xSize)
            xSize += ySize
        if not outputBinary:
            outputBinary = [True] * len(self.layers)
        self.outputBinary = outputBinary
        self.outputLayer = createLayer(noConnection, 1, xSize)
        self.thresholds.append(createThreshold(1))

        self.signalFunction = random.choice(signalFunctions)
#        if type(self.signalFunction) == str or type(self.signalFunction) == unicode:
#            self.signalFunction = getattr(signal, self.signalFunction)

        self.commandFunction = random.choice(commandFunctions)

    def setDatarow(self, datarow):
        self.datarow = datarow

    def encode(self):
        '''Creates string representation of a GNet object.
        @rtype: str
        '''
        toEncode = ['symbol', 'group_id', 'minWidth', 'maxWidth', 'minHeight', 'maxHeight', 'noConnection', 'outputBinary', 'datarowCount', 'commandFunction', 'signalFunction', 'echoFalloff']
        toEncodeNp = ['outputLayer']
        toEncodeListNp = ['thresholds', 'layers']
        result = [{}, {}, {}]
        for i in toEncode:
            result[0][i] = getattr(self, i)
        for i in toEncodeNp:
            result[1][i] = getattr(self, i).tolist()
        for i in toEncodeListNp:
            result[2][i] = []
            obj = getattr(self, i)
            for j in obj:
                result[2][i].append(j.tolist())
        return str(result)

    @staticmethod
    def decode(command_str):
        '''Rebuilds net from string representation.
        @type genome_str : str
        @param genome_str : the string representation created by GNet.encode methode
        @rtype: GNet
        '''
        from ast import literal_eval
        result = GNetEchoCommand(noInit = True)
        toEncode = literal_eval(command_str)
        for key, value in toEncode[0].iteritems():
            setattr(result, key, value)
        for key, value in toEncode[1].iteritems():
            setattr(result, key, np.array(value).astype(np.float32))
        for key, value in toEncode[2].iteritems():
            obj = []
            for j in value:
                obj.append(np.array(j).astype(np.float32))
            setattr(result, key, obj)
        return result

    def addLayer(self, xSize = None, ySize = None, insert = None):
        if not ySize:
            ySize = random.randint(self.minWidth, self.maxWidth)

        if not xSize:
            if len(self.layers) > insert:
                xSize = self.layers[insert].shape[1]
            else:
                xSize = self.outputLayer.shape[1]

        newLayer = createLayer(self.noConnection, ySize, xSize)
        newThresholds = createThreshold(ySize)

        if insert is None:
            self.layers.append(newLayer)
            self.thresholds.append(newThresholds)
        else:
            self.layers.insert(insert, newLayer)
            self.thresholds.insert(insert, newThresholds)

        return xSize, ySize

    def __call__(self):
        if hasattr(self, 'states'):
            states = True
            signalInputLength = self.lastDatarowLength
            signalLength = self.signal.shape[0]
        else:
            self.states = []
            states = False
            signalInputLength = 0
            signalLength = 0
        datarowLength = self.datarow.shape[1]
        thresholds = []
        layer2s = []
        for index, layer in enumerate(self.layers):   #hidden state
            if not states:
                self.states.append(np.zeros(layer.shape[0]))
            thresholds.append(sigmoidInvert(self.thresholds[index]))
            layer2 = np.array(layer)
            layer2[layer2 < -0.5] = 0   #no connection if value is under -0.5
            layer2s.append(layer2)
        thresholds.append(sigmoidInvert(self.thresholds[len(self.layers)]))
        if states:
            if signalLength <= datarowLength:
                signalTmp = self.signal[0:signalInputLength]
                self.signal = np.zeros(max(signalLength + 100, datarowLength), dtype=np.double)
                self.signal[0:signalInputLength] = signalTmp
        else:
            self.signal = np.zeros(datarowLength, dtype=np.double)
        signalInput = np.zeros(datarowLength - signalInputLength)
        data = np.zeros(self.outputLayer.shape[1])

        candleSize = self.datarow.shape[0]
        for index, datarowIndex in enumerate(xrange(signalInputLength, datarowLength)):
            data[0:candleSize] = self.datarow[:, datarowIndex]
            startIndex = candleSize
            for index2, layer in enumerate(layer2s):
                newdata = self.states[index2] * self.echoFalloff + np.dot(layer, data[0:startIndex])
                self.states[index2] = newdata
                if self.outputBinary[index2]:
                    newdata = (newdata > thresholds[index2]).astype('int')
                data[startIndex:startIndex + newdata.shape[0]] = newdata
                startIndex += newdata.shape[0]
            signalInput[index] = np.dot(self.outputLayer, data)
        signalInput = (signalInput > thresholds[-1]).astype('int')
#        print signalInput
        getattr(signal, self.signalFunction)(signalInput, self.signal[signalInputLength:datarowLength], 1, 1, 0)
        if not states:
            self.execute = getattr(self, '_execute' + self.commandFunction)
        self.lastDatarowLength = datarowLength

    def executeParams(self):
        result = {}
        if hasattr(self, 'strategy_id'):
            result['strategy_id'] = self.strategy_id
        return result

    def _executeBuyFix(self, index):
        return self.account.buy(self.symbol, group = self.group_id, **self.executeParams())

    def _executeSellFix(self, index):
        return self.account.sell(self.symbol, group = self.group_id, **self.executeParams())

    def _executeBuyClose(self, index):
        if self.signal[index] > 0:
            return self.account.buy(self.symbol, group = self.group_id, **self.executeParams())
        if self.signal[index] < 0:
            return self.account.closeLastTrade(self.symbol, group = self.group_id, **self.executeParams())

    def _executeSellClose(self, index):
        if self.signal[index] > 0:
            return self.account.sell(self.symbol, group = self.group_id, **self.executeParams())
        if self.signal[index] < 0:
            return self.account.closeLastTrade(self.symbol, group = self.group_id, **self.executeParams())

    def _executeBuySell(self, index):
        if self.signal[index] > 0:
            self.account.closeLastTrade(self.symbol, group = self.group_id, **self.executeParams())
            return self.account.buy(self.symbol, group = self.group_id, **self.executeParams())
        if self.signal[index] < 0:
            self.account.closeLastTrade(self.symbol, group = self.group_id, *self.executeParams())
            return self.account.sell(self.symbol, group = self.group_id, **self.executeParams())

    def __repr__(self):
        """String representation of Genome."""
        result = ''
        for index, layer in enumerate(self.layers):
            layer2 = np.array(layer)
            if type(layer2) != float:
                layer2 = layer2.astype(np.float32)    #no connection if value is under 0.5
            layer2[layer2 < -0.5] = 0
            result += 'layer' + str(index) + ' connections:\n' + np.array_str(layer2, 160, 5) + '\n'
            result += 'layer' + str(index) + ' thresholds:\n' + np.array_str(self.thresholds[index], 160, 5) + '\n'
        result += 'output layer connections:\n' + np.array_str(self.outputLayer, 160, 5) + '\n'
        result += 'output layer thresholds:\n' + np.array_str(self.thresholds[-1], 160, 5) + '\n'
        result += 'signal layer: ' + str(self.signalFunction) + '\n'
        result += 'command layer: ' + str(self.commandFunction)
        return result

    def __str__(self):
        return str(self.commandFunction)

    def printStructure(self):
        """String representation of Genome structure"""
        print 'datarow layer has ' + str(self.datarowCount) + ' nodes'
        print '\n'.join(['layer' + str(index) + ' has ' + str(layer.shape[0]) + ' nodes' for index, layer in enumerate(self.layers)])
        print 'output layer has ' + str(self.outputLayer.shape[0]) + ' nodes'

    def mutate(self, mutator, frequency, intensity, conn_freq, conn_int, thres_freq, thres_int, echo_int):
        """
        Called to mutate the genome.
        1. add/remove layer/node with frequency and intensity
        2. connection_count = the number of all values in layer matrices
            fr = conn_freq / 2000
            int = conn_int / 100
            a) generates (fr * connection_count) places where values will be modified
            b) changes values at the given places by the given mutator and intensity
        3. node_count = the number of all nodes
            fr = thres_freq / 2000
            int = thres_int / 100
            a) generates (fr * node_count) places where values will be modified
            b) changes values at the given places by the given mutator and intensity
        """
        self.mutateStructure(frequency * 0.002, intensity * 0.01)
        self.mutateConnections(mutator, conn_freq * 0.002, conn_int * 0.01)
        self.mutateThresholds(mutator, thres_freq * 0.002, thres_int * 0.01)
#        self.mutateEcho(mutator, echo_int)TODO: mutate echo param

    def mutateStructure(self, freq, intens):
        '''
        Mutate structure: add/remove node/layer
        a) if [0, 1) random number >= fr -> skip add/remove layer/node
        b) if [0, 1) random number >= int/2 -> node add/remove
            else -> layer add/remove
        c) if r = [0, 1) random number >= 0.5 -> remove
            else -> add
        '''
        if random.random() >= freq:         #the action may not be needed at all
            return

        if random.random() * 2 < intens:    #add/remove layer
            target = 'layer'
        else:                               #add/remove node
            target = 'node'

        #choosing action
        #choosing where to add or which layer/node to remove
        layerCount = len(self.layers)
        if random.random() < 0.5:           #add layer/node
            action = 'add'
            if target == 'node':
                random_layer = random.randint(0, layerCount - 1)
                nodeCount = self.layers[random_layer].shape[0]
                if nodeCount == self.maxWidth:      #maximum node count on the layer
                    return
                random_node = random.randint(0, self.layers[random_layer].shape[0])
            else:
                if layerCount == self.maxHeight:    #maximum layer count
                    return
                random_layer = random.randint(0, layerCount)
        else:                               #remove layer/node
            action = 'remove'
            if target == 'node':
                random_layer = random.randint(0, layerCount - 1)
                node_count = self.layers[random_layer].shape[0]
                if node_count == self.minWidth:     #minimum node count on the layer
                    return
                random_node = random.randint(0, node_count - 1)
            else:
                if layerCount == self.minHeight:    #minimum layer count
                    return
                random_layer = random.randint(0, layerCount - 1)

        #do the action
        if action == 'add':
            if target == 'layer':
                xSize, ySize = self.addLayer(insert = random_layer)
                for index in xrange(random_layer + 1, len(self.layers)):#creating connections over the inserted layer
                    self.layers[index] = np.insert(self.layers[index], np.ones(ySize) * xSize, createLayer(self.noConnection, self.layers[index].shape[0], ySize), axis=1)
                self.outputLayer = np.insert(self.outputLayer, np.ones(ySize) * xSize, createLayer(self.noConnection, 1, ySize), axis=1)
                if random_layer:    #copy the outputBinary from the layer below
                    value = self.outputBinary[random_layer - 1]
                else:               #copy the outputBinary from the layer beyond
                    value = self.outputBinary[random_layer]
                self.outputBinary.insert(random_layer, value)
            else:
                if random_layer == 0:   #the node comes to the 0th layer
                    xSize = 0
                else:   #the new node's index in the highest layers is the sum of nodes under + before
                    xSize = self.layers[random_layer - 1].shape[0]
                xSize += random_node    #the nodes before
                self.thresholds[random_layer] = np.insert(self.thresholds[random_layer], random_node, createThreshold(), axis=0)
                self.layers[random_layer] = np.insert(self.layers[random_layer], random_node, createLayer(self.noConnection, 1, self.layers[random_layer].shape[1]), axis=0)
                for index in xrange(random_layer + 1, len(self.layers)):#creating connections over the inserted node
                    self.layers[index] = np.insert(self.layers[index], xSize, createLayer(self.noConnection, self.layers[index].shape[0]), axis=1)
                self.outputLayer = np.insert(self.outputLayer, xSize, createLayer(self.noConnection), axis=1)
        else:
            if target == 'layer':
                ySize, xSize = self.layers[random_layer].shape
                del self.layers[random_layer]
                del self.thresholds[random_layer]
                for index in xrange(random_layer, len(self.layers)):#removing connections over the deleted layer
                    self.layers[index] = np.delete(self.layers[index], np.arange(xSize, xSize + ySize), axis=1)
                self.outputLayer = np.delete(self.outputLayer, np.arange(xSize, xSize + ySize), axis=1)
                del self.outputBinary[random_layer]
            else:
                if random_layer == 0:   #the node is to be removed from the 0th layer
                    xSize = 0
                else:   #the deleted node's index in the highest layers is the sum of nodes under + before
                    xSize = self.layers[random_layer - 1].shape[0]
                xSize += random_node    #the nodes before
                self.thresholds[random_layer] = np.delete(self.thresholds[random_layer], random_node, axis=0)
                self.layers[random_layer] = np.delete(self.layers[random_layer], random_node, axis=0)
                for index in xrange(random_layer + 1, len(self.layers)):#removing connections over the deleted node
                    self.layers[index] = np.delete(self.layers[index], xSize, axis=1)
                self.outputLayer = np.delete(self.outputLayer, xSize, axis=1)

    def mutateConnections(self, mutator, freq, intens):
        '''
        Selects @param freq rate count of all connections' weight and changes them
        with the given mutator and the available min and max possible values.
        '''
        for layer in self.layers:
            toChange = np.random.rand(*layer.shape) < freq
            layer[toChange] = mutator(layer[toChange], -0.5 - self.noConnection, 0.5, intens)

    def mutateThresholds(self, mutator, freq, intens):
        '''
        Selects @param freq rate count of all thresholds and changes them
        with the given mutator and the available min and max possible values.
        '''
        for threshold in self.thresholds:
            toChange = np.random.rand(*threshold.shape) < freq
            if np.any(threshold[toChange]):
                threshold[toChange] = mutator(threshold[toChange], 0.0, 0.9999, intens)

    def mutateEcho(self, mutator, intens):
        self.echoFalloff = mutator(self.echoFalloff, 0.0, 0.9999, intens)

    def checkConsistency(self):
        '''
        Checks consistency of the command net's matrices.
        '''
        if len(self.outputBinary) != len(self.layers):
            raise Exception('layer count and output binary filter count are not the same')
        ySize, xSize = self.layers[0].shape
        if self.datarowCount != xSize:
            raise Exception('historic datarow count and layer0 do not fit')
        if ySize != len(self.thresholds[0]):
            raise Exception('layer0 connections and thresholds count are not the same')
        xSize += ySize
        for index in xrange(1, len(self.layers)):
            layer = self.layers[index]
            lShape = layer.shape
            if lShape[1] != xSize:
                raise Exception('layer' + str(index - 1) + ' and layer' + str(index) + ' do not fit')
            if lShape[0] != len(self.thresholds[index]):
                raise Exception('layer' + str(index - 1) + ' connections and thresholds count are not the same')
            xSize += lShape[0]
        if self.outputLayer.shape[1] != xSize:
            raise Exception('layer' + str(index) + ' and output layer do not fit')
        return True

    def getHeight(self):
        """
        Returns the net height.
        """
        return len(self.layers)

    def clone(self):
        """
        Clone this GNetEchoCommand.
        """
        return GNetEchoCommand.decode(self.encode())

    def getSymbol(self):
        return self.symbol


class GNetEcho(object):
    description = 'Default strategy without predefined structure'
    '''
    Net based model for genome.
    @type commandSize:  list/tuple/None
    @param commandSize: the minimum and maximum number of command nodes
    Other parameters are added to command nodes
    '''
    def __init__(self, echoFalloff = 0.9, symbols = None, noInit = False, layerSizes = None, nodeTypes = None, *args, **kw):
        if noInit:
            return
        self.evaluated = False
        self.fitness = 0.0
        if symbols is None:
            raise Exception('Symbols must be set!')
        if type(symbols) == str or type(symbols) == unicode:
            symbols = (symbols, )
        self.symbol = random.choice(symbols)
        if layerSizes:
            commandSize = (layerSizes[0][0], layerSizes[1][0])
            height = (layerSizes[0][1], layerSizes[1][1])
            width = (layerSizes[0][2], layerSizes[1][2])
        else:
            commandSize = (1, 4)
            height = None
            width = None

        self.priceNodes = []
        signalNodes = []
        commandNodes = []
        for nodeType, value in nodeTypes.iteritems():
            if not value:
                continue
            if nodeType in NetOnOffSignalCommands + NetOnAndOffSignalCommands:
                commandNodes.append(nodeType)
            elif nodeType in NetSignals:
                signalNodes.append(nodeType)
            elif nodeType in NetPrices.keys():
                self.priceNodes.append(NetPrices[nodeType])
        self.commands = []
        self.commandGroups = []
        commandGroup = []
        for i in xrange(random.randint(*commandSize)):
            self.commands.append(GNetEchoCommand(echoFalloff = echoFalloff, group_id = 0, symbol = self.symbol, height = height, width = width, commandFunctions = commandNodes, signalFunctions = signalNodes, priceNodes = self.priceNodes, *args, **kw))
            commandGroup.append(i)
        self.commandGroups.append(commandGroup)

    def prepareForBacktest(self, datarows, account):
        datarows = datarows[self.symbol]
        pip_m = PIP_MULTIPLIER[self.symbol]
        if hasattr(self.commands[0], 'datarow'):
            datarowLength = getattr(self.commands[0], 'datarow').shape[1]
            if datarows[1].shape[0] <= datarowLength:
                return
            datarowLength -= 1
        else:
            datarowLength = 0
        datarows = np.array([tr_diff(datarow[datarowLength:], 1, pip_m) for price, datarow in datarows.iteritems() if price in self.priceNodes])
        if datarowLength > 0:
            datarows = np.hstack((self.commands[0].datarow, datarows))

        for command in self.commands:
            command.setDatarow(datarows)
            command.account = account
            command()

    def __repr__(self):
        result = ''
        for index, command in enumerate(self.commands):
            result += 'command' + str(index) + ':\n' + command.__repr__() + '\n'
        return result

    def printStructure(self):
        """String representation of Genome structure"""
        for command in self.commands:
            command.printStructure()

    def mutate(self, *args, **kw):
        """
        Called to mutate the genome.
        """
        for command in self.commands:
            command.mutate(*args, **kw)

    def checkConsistency(self):
        '''
        Checks consistency of the command net's matrices.
        '''
        for command in self.commands:
            command.checkConsistency()
        return True

    def getHeight(self):
        """
        Returns the maximum of the child command net's height.
        """
        return max([command.getHeight() for command in self.commands])

    def encode(self):
        '''Creates string representation of a GNet object.
        @rtype: str
        '''
        toEncode = ['commandGroups', 'symbol', 'priceNodes', 'fitness', 'evaluated']
        toEncodeListEncode = ['commands']
        result = [{}, {}]
        for i in toEncode:
            result[0][i] = getattr(self, i)
        for i in toEncodeListEncode:
            obj = getattr(self, i)
            result[1][i] = []
            for j in obj:
                result[1][i].append(j.encode())
        return str(result)

    @staticmethod
    def decode(genome_str):
        '''Rebuilds net from string representation.
        @type genome_str : str
        @param genome_str : the string representation created by GNet.encode methode
        @rtype: GNet
        '''
        from ast import literal_eval
        result = GNetEcho(noInit = True)
        toEncode = literal_eval(genome_str)
        for key, value in toEncode[0].iteritems():
            setattr(result, key, value)
        for key, value in toEncode[1].iteritems():
            obj = []
            for j in value:
                obj.append(GNetEchoCommand.decode(j))
            setattr(result, key, obj)
        return result

    def mergeWith(self, genome):
        group_id = len(self.commandGroups)
        groupTable = {}
        for group_index in xrange(len(genome.commandGroups)):
            self.commandGroups.append([])
            groupTable[group_index] = group_id
            group_id += 1
        for command in genome.commands:
            newCommand = command.clone()
            newGroup = groupTable[newCommand.group_id]
            self.commandGroups[newGroup].append(len(self.commands))
            self.commands.append(newCommand)
            newCommand.group_id = newGroup

    def getCommandNodes(self):
        return self.commands

    def getSymbols(self):
        symbols = set()
        for command in self.commands:
            symbols.add(command.getSymbol())
        return symbols

    def clone(self):
        cloned = GNetEcho.decode(self.encode())
        cloned.fitness = 0.0
        cloned.evaluated = False
        return cloned

#    def afterBacktest(self):
#        for command in self.commands:
#            delattr(command, 'execute')

