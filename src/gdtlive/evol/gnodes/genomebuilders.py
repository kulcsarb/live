# -*- encoding: utf-8 -*- 
'''
Created on Mar 22, 2011

@author: gdtlive
'''
#import traceback
from gdtlive.evol.gnodes import *
from gdtlive.evol.gnodes.NodeSelector import getDefaultNodeSelector#, NodeSelector, build_selector
from gdtlive.evol.gnodes.GenomeBase import GTree
import time 



def simpleGenomeBuilder(instruments, selector, parent=None, depth=0):
    if parent == None:
        parent = RootNode(None)  #ha nincs megadva, akkor randomolunk egy root nodeot
        depth = 0

    child_num = random.randint(parent.min_child, parent.max_child)
    child_type = parent.getChildType()
    try:
        child_classes = [selector.get(child_type, depth, parent.__class__) for i in range(child_num)]
    except:
        print child_type
        print depth
        print parent.__class__
        print traceback.format_exc()
    
#    if child_type == NODETYPE_COMMAND:
#        child_classes = []
#        child_classes.append(random.choice(Command.open_nodes))
#        child_classes.append(random.choice(Command.close_nodes))
#        for i in xrange(2, child_num):
#            child_classes.append(random.choice(Command.nodes))
        
    for child_class in child_classes:
        child = child_class(parent)        
        #parent.addChild(child)            
        child.create(instruments)                            
        if not child.isLeaf():            
            simpleGenomeBuilder(instruments, selector, child, depth+1)    

    return parent


def defaultGenomeBuilder(instruments, selector, parent=None, depth=0):

    if parent == None:
        parent = RootNode(None)  #ha nincs megadva, akkor randomolunk egy root nodeot
        depth = 0
    if depth == 0:
        defaultGenomeBuilder.linked_commands = []
        defaultGenomeBuilder.signals = []
        defaultGenomeBuilder.commands = 0
        
    child_num = random.randint(parent.min_child, parent.max_child)
    child_type = parent.getChildType()    
    for i in range(child_num):    
        child_class = selector.get(child_type, depth, parent.__class__)
        child = child_class(parent)        
        #parent.addChild(child)            
        child.create(instruments)        
                    
        if not child.isLeaf():            
            if child.type == NODETYPE_SIGNAL:
                defaultGenomeBuilder.signals.append(child)
            if child.type == NODETYPE_COMMAND:
                if defaultGenomeBuilder.commands > 0:
                    if random.random() < 0.5:
                        defaultGenomeBuilder.linked_commands.append(child)
                    else:
                        defaultGenomeBuilder(instruments, selector, child, depth+1)
                else:
                    defaultGenomeBuilder(instruments, selector, child, depth+1)
                defaultGenomeBuilder.commands += 1                                       
            else:
                defaultGenomeBuilder(instruments, selector, child, depth+1)    
        
    if depth == 0:        
        for command in defaultGenomeBuilder.linked_commands:
            signal = random.choice(defaultGenomeBuilder.signals)
            signal.addParent(command)
            #command.addChild(signal)
            #print 'command', command,' linked to: ',  signal
        
    return parent






if __name__ == '__main__':
        
    
    
    #print selector                
    #quit()
    
#    selector.add(*Price.nodes)
#    selector.add(*Binary.nodes)
#    selector.add(*Comparison.nodes)
#    selector.add(*Numeric.nodes)
#    selector.add(*Indicator.nodes)
#    selector.add(*Logical.nodes)
#    selector.add(*DateTime.nodes)
#    selector.add(*Weekday.nodes)
#    selector.add(*Command.nodes)
#    selector.add(*Signal.nodes)
    #selector.add(*Math.nodes)
    #selector.addLevel(1, AND)
    #selector.addLevel(2, GT, EQ, Open, MA)
    #selector.addLevel(2, *Comparison.nodes)
    #selector.addLevel(5, *Indicator.nodes)
    #selector.addLevel(3, OnOffSignal, OnSignal, OffSignal)
    #selector.addLevel(5, *Comparison.nodes)
    #selector.addLevel(9, *Numeric.nodes)
    #selector.addLevel(9, *Price.nodes)
    #selector.addLevel(3, Open, MA)
    #selector.addLevel(6, Open)
    #selector.addChildren(RSI, Open)
    #selector.addChildrenLevel(AND, 2, GT)    
    #selector.addChildrenLevel(GT, 6, MA)
    #selector = build_selector({})
    #print selector
    selector = getDefaultNodeSelector()
    s = time.time()
    for i in range(1):
        root_node = simpleGenomeBuilder(('EURUSD','GBPJPY','USDCHF'), selector)
        g = GTree(root_node)
        g.processNodes()
    print 'generation time: %.5f' % (time.time()-s)
    print g.getCommandNodes()
    #print 'node count: ', len(g.nodes_list)
    
    #print 
    #print g.nodes_list        
    #print g.getHeight()
    #node = g.getRandomNode()
    #print node
    #print g.getNodesCount(node), g.getNodeDepth(node), g.getNodeHeight(node)

