# -*- encoding: utf-8 -*- 
'''
Created on Mar 7, 2011

@author: kulcsarb
'''
#from gdtlive.evol.gnodes.GenomeBase import GTreeNode
#from gdtlive.constants import *
#import random
#import numpy as np
#
#
#class Binary(GTreeNode):           
#    type = NODETYPE_INT
#    child_type = NODETYPE_INT
#    min_child = 2
#    max_child = 2 
#    classID = 100
#    
#    np_type = np.int
#    
#    def _child(self, index):
#        np_array = self.childs[index]()
#        new_array =  np_array if np_array.dtype==self.np_type else np_array.astype(self.np_type)
#        return new_array
#    
#    def is_valid(self):
#        #print self.childs[0], self.childs[1]        
#        assert isinstance(self.childs[0], GTreeNode)
#        assert isinstance(self.childs[1], GTreeNode)
#        return True
#    
#class LeftShift(Binary):
#    ''' left << right'''
#    classID = 101
#            
#    def __call__(self):
#        return np.left_shift(self._child(0), self._child(1))
#
#class RightShift(Binary):  
#    ''' left >> right'''
#    classID = 102
#        
#    def __call__(self):
#        return np.right_shift(self._child(0), self._child(1))
#    
#class BinaryAND(Binary):
#    ''' left & right '''
#    classID = 103
#        
#    def __call__(self):
#        return np.bitwise_and(self._child(0), self._child(1))
#
#class BinaryOR(Binary):
#    ''' left | right '''
#    classID = 104
#        
#    def __call__(self):
#        return np.bitwise_or(self._child(0), self._child(1))
#
#class BinaryXOR(Binary):
#    ''' left ^ right '''
#    classID = 105
#    
#    def __call__(self):
#        return np.bitwise_xor(self._child(0), self._child(1))
#
#
#class BinaryNOT(Binary):    
#    type = NODETYPE_INT
#    child_type = NODETYPE_INT
#    min_child = 1
#    max_child = 1
#    classID = 106
#    
#    def __call__(self):
#        return np.invert(self._child(0))
#         
#    def is_valid(self):                
#        assert isinstance(self.childs[0], GTreeNode)        
#        return True
#        
#Binary.nodes = [LeftShift, RightShift, BinaryAND, BinaryOR, BinaryXOR, BinaryNOT]
