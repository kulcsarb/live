# -*- encoding: utf-8 -*- 
'''
Created on Mar 4, 2011

@author: kulcsarb
'''
from gdtlive.evol.gnodes.GenomeBase import GTreeNode,  ParameterDescriptor
from gdtlive.constants import NODETYPE_FLOAT
#import random
import numpy as np
import logging

log = logging.getLogger('gdtlive.evol.gnodes')


class Numeric(GTreeNode):         
    child_type = None
    min_child = 0
    max_child = 0
    classID = 401
                                        
class Int(Numeric):
    classID = 402    
    type = NODETYPE_FLOAT      
    parameters_descriptor = [ParameterDescriptor(int, -1000, 1000,'value')]
        
    def __call__(self):        
        self.signal = np.array([self.parameters[0] for i in xrange(self.datarow_length)], dtype=np.double)
        log.debug('%s : %s' % (self.name, self.signal[-10:])) 
        return self.signal
    
    def is_valid(self):
        #assert self.datarow_length
        assert self.parameters[0]
        
class Float(Numeric):
    classID = 403    
    type = NODETYPE_FLOAT
    parameters_descriptor = [ParameterDescriptor(float, -1000.0, 1000.0,'value')]    
                    
    def __call__(self):
        self.signal = np.array([self.parameters[0] for i in xrange(self.datarow_length)], dtype=np.double)
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    
    def is_valid(self):
        #assert self.datarow_length
        assert self.parameters[0]
        
Numeric.nodes = [Int, Float]
