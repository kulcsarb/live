# -*- encoding: utf-8 -*- 
'''
Created on Mar 22, 2011

@author: gdtlive
'''
#from gdtlive.evol.gnodes.GenomeBase import GTreeNode, GTree
#import random
#from gdtlive.constants import *
from gdtlive.evol.gnodes import *


def getDefaultNodeSelector():
    selector = NodeSelector()
    selector.add(*All.nodes)
    selector.addLevel(3, OnOffSignal, OnSignal, OffSignal)
    selector.addLevel(5, *Comparison.nodes)
    selector.addLevel(7, *Numeric.nodes)
    selector.addLevel(7, *Price.nodes)
    return selector



def make_selectorparams(arr):
    params = {}
    try:
        params = {
              'command_min' : arr[0][0], 'command_max' : arr[1][0], 'rootchild_min': arr[2][0], 'rootchild_max' : arr[3][0],
              'signal_min' : arr[0][1], 'signal_max' : arr[1][1], 'sigchild_min': arr[2][1], 'sigchild_max' : arr[3][1],                            
              'logical_min' : arr[0][2], 'logical_max' : arr[1][2], 'logchild_min': arr[2][2], 'logchild_max' : arr[3][2],
              'numeric_min' : arr[0][3], 'numeric_max' : arr[1][3], 'numchild_min': arr[2][3], 'numchild_max' : arr[3][3],              
              }
    except:
        pass
    return params


def build_selector(params = None, allowed_nodes=None):
    if not params:
        params = {}
    if not allowed_nodes:
        allowed_nodes = {}        
    if type(params) == list:
        params = make_selectorparams(params)
    elif type(params) != dict:
        raise Exception('dictet adj meg wazzze!')
        
    siglayer_min = params.get('signal_min',1)
    siglayer_max = params.get('signal_max',4)
    loglayer_min = params.get('logical_min',1)
    loglayer_max = params.get('logical_max',4)
    numlayer_min = params.get('numeric_min',1)
    numlayer_max = params.get('numeric_max',5)
    
    rootchild_min = params.get('rootchild_min', 1)
    rootchild_max = params.get('rootchild_max', 4)
    numchild_min = params.get('numchild_min', 2)
    numchild_max = params.get('numchild_max', 4)
    logchild_min = params.get('logchild_min', 2)
    logchild_max = params.get('logchild_max', 4)
    sigchild_min = params.get('sigchild_min', 2)
    sigchild_max = params.get('sigchild_max', 4)
                
    siglayer_num = random.randint(siglayer_min, siglayer_max)
    loglayer_num = random.randint(loglayer_min, loglayer_max)
    numlayer_num = random.randint(numlayer_min, numlayer_max)
    
    RootNode.min_child = rootchild_min
    RootNode.max_child = rootchild_max
    SignalAggregator.min_child = sigchild_min
    SignalAggregator.max_child = sigchild_max
    OnOffSignalAggregator.min_child = sigchild_min
    OnOffSignalAggregator.max_child = sigchild_max
    Math.min_child = numchild_min
    Math.max_child = numchild_max
    Logical.min_child = logchild_min
    Logical.max_child = logchild_max
    
           
    command_nodes = Command.nodes[:]
    signal_nodes = Signal.signal_nodes[:]
    sigaggreg_nodes = Signal.aggreg_nodes[:]
    logical_nodes = Logical.nodes[:]
    comparison_nodes = Comparison.nodes[:]
    weekday_nodes = Weekday.nodes[:]
    price_nodes = Price.nodes[:]
    numeric_nodes = Numeric.nodes[:]
    math_nodes = Math.nodes[:]
    indicator_nodes = Indicator.nodes[:]
    datetime_nodes = DateTime.nodes[:]
    
    locator = {}
    for nodes in (command_nodes, signal_nodes, sigaggreg_nodes, logical_nodes, comparison_nodes, weekday_nodes, price_nodes, numeric_nodes, math_nodes, indicator_nodes, datetime_nodes) :
        for node in nodes:
            locator[node.classID] = nodes
    
    #print locator
     
    #print command_nodes
    #print signal_nodes 
    
    #    nem engedélyezett nodeok kiszűrése
    for node_id, allowed in allowed_nodes.iteritems():
        if not allowed:
            try:
                classID = int(node_id)                
                node_class = All.node_ids[classID] 
                while True:
                    try:               
                        locator[classID].remove(node_class)
                    except:
                        break            
                print node_class.__name__, 'removed'                
            except:
                print traceback.format_exc()
                pass
            
    #print command_nodes
    #print signal_nodes
       
    # ha túl sokat szűrt ki a kedves user, visszarakunk néhányat
    if not command_nodes:   command_nodes = [Buy, CloseAll]
    if not signal_nodes:    signal_nodes = Signal.signal_nodes[:]
    if not sigaggreg_nodes: sigaggreg_nodes = Signal.aggreg_nodes[:]
    if not logical_nodes:   logical_nodes = [AND]
    if not comparison_nodes: comparison_nodes = [GT]
    if not numeric_nodes:   numeric_nodes = Numeric.nodes[:]
    if not price_nodes:     price_nodes = Price.nodes[:]
                                        
    selector = NodeSelector()    
    selector.addLevel(0, *command_nodes)    
                
    for layer_num in xrange(1, siglayer_num):        
        selector.addLevel(layer_num, *sigaggreg_nodes)    
    selector.addLevel(siglayer_num, *signal_nodes)
        
    for layer_num in xrange(1, loglayer_num):
        selector.addLevel(siglayer_num+layer_num, *logical_nodes)
    selector.addLevel(siglayer_num+loglayer_num, *comparison_nodes + comparison_nodes + weekday_nodes)

    leaf_nodes = price_nodes + price_nodes + numeric_nodes
    numeric_nodes = math_nodes + math_nodes + math_nodes + indicator_nodes + datetime_nodes + [Time] + price_nodes + numeric_nodes
    for layer_num in xrange(1, numlayer_num):
        selector.addLevel(siglayer_num+loglayer_num+layer_num, *numeric_nodes)    
    selector.addLevel(siglayer_num+loglayer_num+numlayer_num, *leaf_nodes)
    
    #print selector
    return selector    




class NodeSelector:
    
    def __init__(self):
        self.levels = {}
        self.children = {}
            
    def add(self, *args):                    
        self.addLevel(-1, *args)

    def addLevel(self, level, *args):
        for cls in args:
            try:
                if not cls.type in self.levels:
                    self.levels[cls.type] = {}
                if not level in self.levels[cls.type]:
                    self.levels[cls.type][level] = []
            except:
                print traceback.format_exc()
                print level 
                print cls
                raise Exception()
            #if cls not in self.levels[cls.type][level]:
            self.levels[cls.type][level].append(cls)        
    
    
    def addChildren(self, cls, *children):
        self.addChildrenLevel(cls, -1, *children)
        
    def addChildrenLevel(self, cls, level, *children):
        if cls not in self.children:
            self.children[cls] = {-1: []}
        if level not in self.children[cls]:
            self.children[cls][level] = []
        for child in children:
            if child.type != cls.child_type:
                raise Exception('Invalid child "%s" for node %s' % (child.__name__, cls.__name__))
            if child not in self.children[cls][level]:
                self.children[cls][level].append(child)
                
    def __repr__(self):
        r = 'Level matrix: \n'
        r += 'TYPE\tLEVEL\tCLASSES\n'
        for type in self.levels.iterkeys(): 
#            r += '%s\t\t' % (TYPE_NAMES[type])
#            for cls in self.types[type]:
#                r += cls.__name__ +', '            
#            r += '\n'            
            for depth in sorted(self.levels[type].iterkeys()):
                if depth == -1: 
                    r += '%s\tdefault\t' % NODETYPE_NAMES[type]             
                else:
                    r += '%s\t>=%d\t' % (NODETYPE_NAMES[type], depth)
                for cls in self.levels[type][depth]:
                    r += cls.__name__ +', '
                r += '\n'
        
        r += '\nChildren matrix\n'
        r += 'Parent\tLevel\tAllowed children\n'
        for parent in self.children.iterkeys():             
            for level in sorted(self.children[parent].iterkeys()):
                if level == -1:
                    r += '%s\tdefault\t' % parent.__name__
                else:
                    r += '%s\t>=%s\t' % (parent.__name__, level)
                for child in self.children[parent][level]:
                    r += child.__name__ + ',' 
                r += '\n'    
        return r
    
        
    def get(self, node_type, depth, cls):
        if cls in self.children:
            levels = sorted(self.children[cls].keys(), reverse=True)
            for level in levels:
                if level <= depth:                                    
                    try:                                         
                        node = random.choice(self.children[cls][level])
                        #print 'selecting from children %d, from:' % level
                        #print self.children[cls][level]
                        return node
                    except:                        
                        pass

        if node_type not in self.levels:
            print cls
            print node_type
            print self.levels
            raise Exception('No "%s" type nodes in current NodeSelector, please add some before running me...' % NODETYPE_NAMES[node_type])
        
        levels = sorted(self.levels[node_type].keys(), reverse=True)
        for level in levels:
            if level <= depth:
                #print 'selecting from levels %d, from:' % level
                #print self.levels[node_type][level]                                                         
                return random.choice(self.levels[node_type][level])
        
        raise Exception('No selectable class for type: %s, depth: %d, parent: %s' % (NODETYPE_NAMES[node_type], depth, cls))        




