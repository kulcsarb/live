# -*- encoding: utf-8 -*-
'''
Created on Mar 24, 2011

@author: gdtlive
'''
from gdtlive.evol.gnodes import *
from gdtlive.evol.gnodes.GenomeBase import GTree
from gdtlive.evol.gnodes.genomebuilders import simpleGenomeBuilder, getDefaultNodeSelector#, defaultGenomeBuilder
from gdtlive.evol.gnodes.NodeSelector import build_selector#, NodeSelector


class Individual(GTree):
    selector = None
    
    description = 'Default strategy without predefined structure'
    
    def __init__(self, root_node=None):
        super(Individual, self).__init__(root_node)
        self.initializator = simpleGenomeBuilder                                   
        
    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        Individual.selector = build_selector(builder_params, allowed_nodes)
        
        
    def build_genome(self, symbols):
        if not Individual.selector:
            Individual.selector = self.get_selector()
        
        self.root_node = self.initializator(symbols, Individual.selector, self.root_node)
        return self
            
            
    @staticmethod
    def get_selector():        
        return getDefaultNodeSelector()
        
        
        
class IBuyAndClose(Individual):
                  
    description = ''
    
    def __init__(self):
        super(IBuyAndClose, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = BuyCloseCommand(self.root_node)        
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command]
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.command.create(symbols)                        
        self.initializator(symbols, selector, self.command)                
        return self
        
                   
    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IBuyAndClose.get_selector()
        
                                                           
    @staticmethod
    def get_selector():
        if not IBuyAndClose.selector:
            IBuyAndClose.selector = NodeSelector()
            IBuyAndClose.selector.add(*All.nodes)            
            IBuyAndClose.selector.addLevel(0, OnOffSignal)
            IBuyAndClose.selector.addLevel(1, *Logical.nodes)
            IBuyAndClose.selector.addLevel(2, *Comparison.nodes)
            IBuyAndClose.selector.addLevel(3, *Indicator.nodes)
            IBuyAndClose.selector.addLevel(3, *Math.nodes)
            IBuyAndClose.selector.addLevel(6, *Numeric.nodes)
            IBuyAndClose.selector.addLevel(6, *Price.nodes)            
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 2
            Logical.max_child = 4            
        return IBuyAndClose.selector



class ISellAndClose(Individual):
                  
    description = ''
    
    def __init__(self):
        super(ISellAndClose, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = SellCloseCommand(self.root_node)
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command]        
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.command.create(symbols)                        
        self.initializator(symbols, selector, self.command)                
        return self

    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        ISellAndClose.get_selector()
                                                                                                    
    @staticmethod
    def get_selector():
        if not ISellAndClose.selector:
            ISellAndClose.selector = IBuyAndClose.get_selector()
            #print ISellAndClose.selector
        return ISellAndClose.selector



#class IBuyAndCloseComplex(Individual):
#                  
#    description = ''
#    
#    def __init__(self):
#        super(IBuyAndCloseComplex, self).__init__(RootNode())
#        #self.initializator = simpleGenomeBuilder                        
#        self.root_node.min_child = 1
#        self.root_node.max_child = 1
#        self.command = BuyCloseCommand(self.root_node)        
#        self.root_node.addChild(self.command)
#        self.fixed_nodes = [self.command]
#        
#
#    def build_genome(self, symbols, selector=None):
#        if not selector:
#            selector = self.get_selector()            
#        self.command.create(symbols)                        
#        self.initializator(symbols, selector, self.command)                
#        return self
#    
#    @staticmethod
#    def build_selector(builder_params, allowed_nodes):
#        IBuyAndCloseComplex.get_selector()
#                                                                                                   
#    @staticmethod
#    def get_selector():
#        if not IBuyAndCloseComplex.selector:
#            IBuyAndCloseComplex.selector = NodeSelector()
#            IBuyAndCloseComplex.selector.add(*All.nodes)            
#            IBuyAndCloseComplex.selector.addLevel(0, AddOnOffSignals)
#            IBuyAndCloseComplex.selector.addLevel(1, AddOnOffSignals)
#            IBuyAndCloseComplex.selector.addLevel(2, OnOffSignal)
#            IBuyAndCloseComplex.selector.addLevel(3, *Logical.nodes)
#            IBuyAndCloseComplex.selector.addLevel(4, *Logical.nodes)
#            IBuyAndCloseComplex.selector.addLevel(5, *Comparison.nodes)
#            IBuyAndCloseComplex.selector.addLevel(6, *Indicator.nodes)
#            IBuyAndCloseComplex.selector.addLevel(6, *Math.nodes)
#            IBuyAndCloseComplex.selector.addLevel(8, *Numeric.nodes)
#            IBuyAndCloseComplex.selector.addLevel(8, *Price.nodes)            
#            Math.min_child = 3
#            Math.max_child = 5
#            Logical.min_child = 3
#            Logical.max_child = 5            
#        return IBuyAndCloseComplex.selector
#
#
#
#class ISellAndCloseComplex(Individual):
#                  
#    description = ''
#    
#    def __init__(self):
#        super(ISellAndCloseComplex, self).__init__(RootNode())
#        #self.initializator = simpleGenomeBuilder                        
#        self.root_node.min_child = 1
#        self.root_node.max_child = 1
#        self.command = SellCloseCommand(self.root_node)
#        self.root_node.addChild(self.command)
#        self.fixed_nodes = [self.command]        
#        
#
#    def build_genome(self, symbols, selector=None):
#        if not selector:
#            selector = self.get_selector()            
#        self.command.create(symbols)                        
#        self.initializator(symbols, selector, self.command)                
#        return self
#    
#    @staticmethod
#    def build_selector(builder_params, allowed_nodes):
#        ISellAndCloseComplex.get_selector()
#                                                                                                            
#    @staticmethod
#    def get_selector():
#        if not ISellAndCloseComplex.selector:
#            ISellAndCloseComplex.selector = IBuyAndCloseComplex.get_selector()
#        return ISellAndCloseComplex.selector


class IBuySell(Individual):        
    
    description = ''
        
    def __init__(self):
        super(IBuySell, self).__init__(RootNode())                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = BuySellCommand(self.root_node)        
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command]
        
    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()
                        
        symbol = random.choice(symbols) if hasattr(symbols, '__iter__') else symbols 
        self.command.create(symbol)                                                     
        self.initializator(symbol, selector, self.command)                        
        return self

    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IBuySell.get_selector()
    
        
    @staticmethod
    def get_selector():
        if not IBuySell.selector:
            IBuySell.selector = NodeSelector()
            IBuySell.selector.add(*All.nodes)            
            IBuySell.selector.addLevel(0, OnOffSignal)
            IBuySell.selector.addLevel(1, *Logical.nodes)
            IBuySell.selector.addLevel(2, *Comparison.nodes)
            IBuySell.selector.addLevel(3, *Indicator.nodes)
            IBuySell.selector.addLevel(3, *Math.nodes)
            IBuySell.selector.addLevel(6, *Numeric.nodes)
            IBuySell.selector.addLevel(6, *Price.nodes)            
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 2
            Logical.max_child = 4            
        return IBuySell.selector
        

#class IBuySellComplex(Individual):        
#    
#    description = ''
#        
#    def __init__(self):
#        super(IBuySellComplex, self).__init__(RootNode())                        
#        self.root_node.min_child = 1
#        self.root_node.max_child = 1
#        self.command = BuySellCommand(self.root_node)        
#        self.root_node.addChild(self.command)
#        self.fixed_nodes = [self.command]
#        
#    def build_genome(self, symbols, selector=None):
#        if not selector:
#            selector = self.get_selector()
#                        
#        symbol = random.choice(symbols) if hasattr(symbols, '__iter__') else symbols 
#        self.command.create(symbol)                                                     
#        self.initializator(symbol, selector, self.command)                        
#        return self
#
#    @staticmethod
#    def build_selector(builder_params, allowed_nodes):
#        IBuySellComplex.get_selector()
#    
#        
#    @staticmethod
#    def get_selector():
#        if not IBuySellComplex.selector:
#            IBuySellComplex.selector = NodeSelector()
#            IBuySellComplex.selector.add(*All.nodes)            
#            IBuySellComplex.selector.addLevel(0, AddOnOffSignals)
#            IBuySellComplex.selector.addLevel(1, AddOnOffSignals)
#            IBuySellComplex.selector.addLevel(2, OnOffSignal)
#            IBuySellComplex.selector.addLevel(3, *Logical.nodes)
#            IBuySellComplex.selector.addLevel(4, *Logical.nodes)
#            IBuySellComplex.selector.addLevel(5, *Comparison.nodes)
#            IBuySellComplex.selector.addLevel(6, *Indicator.nodes)
#            IBuySellComplex.selector.addLevel(6, *Math.nodes)
#            IBuySellComplex.selector.addLevel(8, *Numeric.nodes)
#            IBuySellComplex.selector.addLevel(8, *Price.nodes)            
#            Math.min_child = 3
#            Math.max_child = 5
#            Logical.min_child = 3
#            Logical.max_child = 5
#        return IBuySellComplex.selector
#        


class IBuyCloseAll(Individual):
                  
    description = ''
    
    def __init__(self):
        super(IBuyCloseAll, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 2
        self.root_node.max_child = 2
        self.buy_command = BuyFix(self.root_node)
        self.close_command = CloseAll(self.root_node)        
        self.root_node.addChild(self.buy_command)
        self.root_node.addChild(self.close_command)
        self.fixed_nodes = [self.buy_command, self.close_command]
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.buy_command.create(symbols)                        
        self.close_command.create(symbols)
        self.initializator(symbols, selector, self.buy_command)                
        self.initializator(symbols, selector, self.close_command)
        return self
    
    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IBuyCloseAll.get_selector()
                                                                                        
    @staticmethod
    def get_selector():
        if not IBuyCloseAll.selector:
            IBuyCloseAll.selector = NodeSelector()
            IBuyCloseAll.selector.add(*All.nodes)            
            IBuyCloseAll.selector.addLevel(0, OnSignal, OffSignal)
            IBuyCloseAll.selector.addLevel(1, *Logical.nodes)
            IBuyCloseAll.selector.addLevel(2, *Comparison.nodes)
            IBuyCloseAll.selector.addLevel(3, *Indicator.nodes)
            IBuyCloseAll.selector.addLevel(3, *Math.nodes)
            IBuyCloseAll.selector.addLevel(6, *Numeric.nodes)
            IBuyCloseAll.selector.addLevel(6, *Price.nodes)            
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 2
            Logical.max_child = 4            
        return IBuyCloseAll.selector



class ISellCloseAll(Individual):
                  
    description = ''
    
    def __init__(self):
        super(ISellCloseAll, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 2
        self.root_node.max_child = 2
        self.sell_command = SellFix(self.root_node)
        self.close_command = CloseAll(self.root_node)
        self.root_node.addChild(self.sell_command)
        self.root_node.addChild(self.close_command)
        self.fixed_nodes = [self.sell_command, self.close_command]        
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.sell_command.create(symbols)                                
        self.close_command.create(symbols)
        self.initializator(symbols, selector, self.sell_command)                
        self.initializator(symbols, selector, self.close_command)            
        return self

    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        ISellCloseAll.get_selector()
                                                                                                        
    @staticmethod
    def get_selector():
        if not ISellCloseAll.selector:
            ISellCloseAll.selector = IBuyCloseAll.get_selector()
        return ISellCloseAll.selector


#class IBuyCloseAllComplex(Individual):
#                  
#    description = ''
#    
#    def __init__(self):
#        super(IBuyCloseAllComplex, self).__init__(RootNode())
#        #self.initializator = simpleGenomeBuilder                        
#        self.root_node.min_child = 2
#        self.root_node.max_child = 2
#        self.buy_command = BuyFix(self.root_node)
#        self.close_command = CloseAll(self.root_node)        
#        self.root_node.addChild(self.buy_command)
#        self.root_node.addChild(self.close_command)
#        self.fixed_nodes = [self.buy_command, self.close_command]
#        
#
#    def build_genome(self, symbols, selector=None):
#        if not selector:
#            selector = self.get_selector()            
#        self.buy_command.create(symbols)                        
#        self.close_command.create(symbols)
#        self.initializator(symbols, selector, self.buy_command)                
#        self.initializator(symbols, selector, self.close_command)
#        return self
#    
#    @staticmethod
#    def build_selector(builder_params, allowed_nodes):
#        IBuyCloseAllComplex.get_selector()
#                                                                                                        
#    @staticmethod
#    def get_selector():
#        if not IBuyCloseAllComplex.selector:
#            IBuyCloseAllComplex.selector = NodeSelector()
#            IBuyCloseAllComplex.selector.add(*All.nodes)      
#            IBuyCloseAllComplex.selector.addLevel(0, AddSignals)
#            IBuyCloseAllComplex.selector.addLevel(1, AddSignals)
#            IBuyCloseAllComplex.selector.addLevel(2, OnSignal, OffSignal)
#            IBuyCloseAllComplex.selector.addLevel(3, *Logical.nodes)
#            IBuyCloseAllComplex.selector.addLevel(4, *Logical.nodes)
#            IBuyCloseAllComplex.selector.addLevel(5, *Comparison.nodes)
#            IBuyCloseAllComplex.selector.addLevel(6, *Indicator.nodes)
#            IBuyCloseAllComplex.selector.addLevel(6, *Math.nodes)
#            IBuyCloseAllComplex.selector.addLevel(8, *Numeric.nodes)
#            IBuyCloseAllComplex.selector.addLevel(8, *Price.nodes)                                
#            Math.min_child = 3
#            Math.max_child = 5
#            Logical.min_child = 3
#            Logical.max_child = 5            
#        return IBuyCloseAllComplex.selector
#
#
#
#class ISellCloseAllComplex(Individual):
#                  
#    description = ''
#    
#    def __init__(self):
#        super(ISellCloseAllComplex, self).__init__(RootNode())
#        #self.initializator = simpleGenomeBuilder                        
#        self.root_node.min_child = 2
#        self.root_node.max_child = 2
#        self.sell_command = SellFix(self.root_node)
#        self.close_command = CloseAll(self.root_node)
#        self.root_node.addChild(self.sell_command)
#        self.root_node.addChild(self.close_command)
#        self.fixed_nodes = [self.sell_command, self.close_command]        
#        
#
#    def build_genome(self, symbols, selector=None):
#        if not selector:
#            selector = self.get_selector()            
#        self.sell_command.create(symbols)                                
#        self.close_command.create(symbols)
#        self.initializator(symbols, selector, self.sell_command)                
#        self.initializator(symbols, selector, self.close_command)            
#        return self
#    
#    @staticmethod
#    def build_selector(builder_params, allowed_nodes):
#        ISellCloseAllComplex.get_selector()
#                                                                                                        
#    @staticmethod
#    def get_selector():
#        if not ISellCloseAllComplex.selector:
#            ISellCloseAllComplex.selector = IBuyCloseAllComplex.get_selector()
#        return ISellCloseAllComplex.selector


class IBuyAndClose_SellAndClose_BuySell(Individual):
                  
    description = ''
    
    def __init__(self):
        super(IBuyAndClose_SellAndClose_BuySell, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = random.choice([BuyCloseCommand, SellCloseCommand, BuySellCommand])(self.root_node)        
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command]
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.command.create(symbols)                        
        self.initializator(symbols, selector, self.command)                
        return self
        
                   
    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IBuyAndClose_SellAndClose_BuySell.get_selector()
        
                                                           
    @staticmethod
    def get_selector():
        if not IBuyAndClose_SellAndClose_BuySell.selector:
            IBuyAndClose_SellAndClose_BuySell.selector = NodeSelector()
            IBuyAndClose_SellAndClose_BuySell.selector.add(*All.nodes)            
            IBuyAndClose_SellAndClose_BuySell.selector.addLevel(0, OnOffSignal)
            IBuyAndClose_SellAndClose_BuySell.selector.addLevel(1, *Logical.nodes)
            IBuyAndClose_SellAndClose_BuySell.selector.addLevel(2, *Comparison.nodes)
            IBuyAndClose_SellAndClose_BuySell.selector.addLevel(3, *Indicator.nodes)
            IBuyAndClose_SellAndClose_BuySell.selector.addLevel(3, *Math.nodes)
            IBuyAndClose_SellAndClose_BuySell.selector.addLevel(6, *Numeric.nodes)
            IBuyAndClose_SellAndClose_BuySell.selector.addLevel(6, *Price.nodes)            
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 2
            Logical.max_child = 4            
        return IBuyAndClose_SellAndClose_BuySell.selector



#****************************************************************************************************
#
#                Individuals using Difference price nodes
#
#****************************************************************************************************



class IBuyAndCloseDIFF(Individual):
                  
    description = ''
    
    def __init__(self):
        super(IBuyAndCloseDIFF, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = BuyCloseCommand(self.root_node)        
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command]
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.command.create(symbols)                        
        self.initializator(symbols, selector, self.command)                
        return self
        
                   
    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IBuyAndCloseDIFF.get_selector()
        
                                                           
    @staticmethod
    def get_selector():
        if not IBuyAndCloseDIFF.selector:
            IBuyAndCloseDIFF.selector = NodeSelector()
            IBuyAndCloseDIFF.selector.add(*All.nodes)            
            IBuyAndCloseDIFF.selector.addLevel(0, OnOffSignal)
            IBuyAndCloseDIFF.selector.addLevel(1, *Logical.nodes)
            IBuyAndCloseDIFF.selector.addLevel(2, *Comparison.nodes)
            IBuyAndCloseDIFF.selector.addLevel(3, *Indicator.non_price_specific_nodes)
            IBuyAndCloseDIFF.selector.addLevel(3, *Math.nodes)
            IBuyAndCloseDIFF.selector.addLevel(6, *Numeric.nodes)
            IBuyAndCloseDIFF.selector.addLevel(6, *Price.diff_nodes)            
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 2
            Logical.max_child = 4            
        return IBuyAndCloseDIFF.selector




class ISellAndCloseDIFF(Individual):
                  
    description = ''
    
    def __init__(self):
        super(ISellAndCloseDIFF, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = SellCloseCommand(self.root_node)
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command]        
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.command.create(symbols)                        
        self.initializator(symbols, selector, self.command)                
        return self

    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        ISellAndCloseDIFF.get_selector()
                                                                                                    
    @staticmethod
    def get_selector():
        if not ISellAndCloseDIFF.selector:
            ISellAndCloseDIFF.selector = IBuyAndCloseDIFF.get_selector()
            #print ISellAndClose.selector
        return ISellAndCloseDIFF.selector


class IBuySellDIFF(Individual):        
    
    description = ''
        
    def __init__(self):
        super(IBuySellDIFF, self).__init__(RootNode())                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = BuySellCommand(self.root_node)        
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command]
        
    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()
                        
        symbol = random.choice(symbols) if hasattr(symbols, '__iter__') else symbols 
        self.command.create(symbol)                                                     
        self.initializator(symbol, selector, self.command)                        
        return self

    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IBuySellDIFF.get_selector()
    
        
    @staticmethod
    def get_selector():
        if not IBuySellDIFF.selector:
            IBuySellDIFF.selector = NodeSelector()
            IBuySellDIFF.selector.add(*All.nodes)            
            IBuySellDIFF.selector.addLevel(0, OnOffSignal)
            IBuySellDIFF.selector.addLevel(1, *Logical.nodes)
            IBuySellDIFF.selector.addLevel(2, *Comparison.nodes)
            IBuySellDIFF.selector.addLevel(3, *Indicator.non_price_specific_nodes)
            IBuySellDIFF.selector.addLevel(3, *Math.nodes)
            IBuySellDIFF.selector.addLevel(6, *Numeric.nodes)
            IBuySellDIFF.selector.addLevel(6, *Price.diff_nodes)            
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 2
            Logical.max_child = 4            
        return IBuySellDIFF.selector



class IBuyAndClose_SellAndClose_BuySellDIFF(Individual):
                  
    description = ''
    
    def __init__(self):
        super(IBuyAndClose_SellAndClose_BuySellDIFF, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = random.choice([BuyCloseCommand, SellCloseCommand, BuySellCommand])(self.root_node)        
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command]
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.command.create(symbols)                        
        self.initializator(symbols, selector, self.command)                
        return self
        
                   
    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IBuyAndClose_SellAndClose_BuySellDIFF.get_selector()
        
                                                           
    @staticmethod
    def get_selector():
        if not IBuyAndClose_SellAndClose_BuySellDIFF.selector:
            IBuyAndClose_SellAndClose_BuySellDIFF.selector = NodeSelector()
            IBuyAndClose_SellAndClose_BuySellDIFF.selector.add(*All.nodes)            
            IBuyAndClose_SellAndClose_BuySellDIFF.selector.addLevel(0, OnOffSignal)
            IBuyAndClose_SellAndClose_BuySellDIFF.selector.addLevel(1, *Logical.nodes)
            IBuyAndClose_SellAndClose_BuySellDIFF.selector.addLevel(2, *Comparison.nodes)
            IBuyAndClose_SellAndClose_BuySellDIFF.selector.addLevel(3, *Indicator.non_price_specific_nodes)
            IBuyAndClose_SellAndClose_BuySellDIFF.selector.addLevel(3, *Math.nodes)
            IBuyAndClose_SellAndClose_BuySellDIFF.selector.addLevel(6, *Numeric.nodes)
            IBuyAndClose_SellAndClose_BuySellDIFF.selector.addLevel(6, *Price.diff_nodes)            
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 2
            Logical.max_child = 4            
        return IBuyAndClose_SellAndClose_BuySellDIFF.selector


class IBuyCloseAllDIFF(Individual):
                  
    description = ''
    
    def __init__(self):
        super(IBuyCloseAllDIFF, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 2
        self.root_node.max_child = 2
        self.buy_command = BuyFix(self.root_node)
        self.close_command = CloseAll(self.root_node)        
        self.root_node.addChild(self.buy_command)
        self.root_node.addChild(self.close_command)
        self.fixed_nodes = [self.buy_command, self.close_command]
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.buy_command.create(symbols)                        
        self.close_command.create(symbols)
        self.initializator(symbols, selector, self.buy_command)                
        self.initializator(symbols, selector, self.close_command)
        return self
    
    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IBuyCloseAllDIFF.get_selector()
                                                                                        
    @staticmethod
    def get_selector():
        if not IBuyCloseAllDIFF.selector:
            IBuyCloseAllDIFF.selector = NodeSelector()
            IBuyCloseAllDIFF.selector.add(*All.nodes)            
            IBuyCloseAllDIFF.selector.addLevel(0, OnSignal, OffSignal)
            IBuyCloseAllDIFF.selector.addLevel(1, *Logical.nodes)
            IBuyCloseAllDIFF.selector.addLevel(2, *Comparison.nodes)
            IBuyCloseAllDIFF.selector.addLevel(3, *Indicator.non_price_specific_nodes)
            IBuyCloseAllDIFF.selector.addLevel(3, *Math.nodes)
            IBuyCloseAllDIFF.selector.addLevel(6, *Numeric.nodes)
            IBuyCloseAllDIFF.selector.addLevel(6, *Price.diff_nodes)            
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 2
            Logical.max_child = 4            
        return IBuyCloseAllDIFF.selector



class ISellCloseAllDIFF(Individual):
                  
    description = ''
    
    def __init__(self):
        super(ISellCloseAllDIFF, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 2
        self.root_node.max_child = 2
        self.sell_command = SellFix(self.root_node)
        self.close_command = CloseAll(self.root_node)
        self.root_node.addChild(self.sell_command)
        self.root_node.addChild(self.close_command)
        self.fixed_nodes = [self.sell_command, self.close_command]        
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.sell_command.create(symbols)                                
        self.close_command.create(symbols)
        self.initializator(symbols, selector, self.sell_command)                
        self.initializator(symbols, selector, self.close_command)            
        return self

    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        ISellCloseAllDIFF.get_selector()
                                                                                                        
    @staticmethod
    def get_selector():
        if not ISellCloseAllDIFF.selector:
            ISellCloseAllDIFF.selector = IBuyCloseAllDIFF.get_selector()
        return ISellCloseAllDIFF.selector


#****************************************************************************************************
#
#                Individuals using UpDown price nodes
#
#****************************************************************************************************



class IBuyAndCloseUpDown(Individual):
                  
    description = ''
    
    def __init__(self):
        super(IBuyAndCloseUpDown, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = BuyCloseCommand(self.root_node)        
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command]
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.command.create(symbols)                        
        self.initializator(symbols, selector, self.command)                
        return self
        
                   
    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IBuyAndCloseUpDown.get_selector()
        
                                                           
    @staticmethod
    def get_selector():
        if not IBuyAndCloseUpDown.selector:
            IBuyAndCloseUpDown.selector = NodeSelector()
            IBuyAndCloseUpDown.selector.add(*All.nodes)            
            IBuyAndCloseUpDown.selector.addLevel(0, OnOffSignal)
            IBuyAndCloseUpDown.selector.addLevel(1, *Logical.nodes)
            IBuyAndCloseUpDown.selector.addLevel(2, *Comparison.nodes)
            IBuyAndCloseUpDown.selector.addLevel(3, *Indicator.non_price_specific_nodes)
            IBuyAndCloseUpDown.selector.addLevel(3, *Math.nodes)
            IBuyAndCloseUpDown.selector.addLevel(6, *Numeric.nodes)
            IBuyAndCloseUpDown.selector.addLevel(6, *Price.ud_nodes)            
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 2
            Logical.max_child = 4            
        return IBuyAndCloseUpDown.selector




class ISellAndCloseUpDown(Individual):
                  
    description = ''
    
    def __init__(self):
        super(ISellAndCloseUpDown, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = SellCloseCommand(self.root_node)
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command]        
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.command.create(symbols)                        
        self.initializator(symbols, selector, self.command)                
        return self

    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        ISellAndCloseUpDown.get_selector()
                                                                                                    
    @staticmethod
    def get_selector():
        if not ISellAndCloseUpDown.selector:
            ISellAndCloseUpDown.selector = IBuyAndCloseUpDown.get_selector()
        return ISellAndCloseUpDown.selector


class IBuySellUpDown(Individual):        
    
    description = ''
        
    def __init__(self):
        super(IBuySellUpDown, self).__init__(RootNode())                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = BuySellCommand(self.root_node)        
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command]
        
    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()
                        
        symbol = random.choice(symbols) if hasattr(symbols, '__iter__') else symbols 
        self.command.create(symbol)                                                     
        self.initializator(symbol, selector, self.command)                        
        return self

    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IBuySellUpDown.get_selector()
    
        
    @staticmethod
    def get_selector():
        if not IBuySellUpDown.selector:
            IBuySellUpDown.selector = NodeSelector()
            IBuySellUpDown.selector.add(*All.nodes)            
            IBuySellUpDown.selector.addLevel(0, OnOffSignal)
            IBuySellUpDown.selector.addLevel(1, *Logical.nodes)
            IBuySellUpDown.selector.addLevel(2, *Comparison.nodes)
            IBuySellUpDown.selector.addLevel(3, *Indicator.non_price_specific_nodes)
            IBuySellUpDown.selector.addLevel(3, *Math.nodes)
            IBuySellUpDown.selector.addLevel(6, *Numeric.nodes)
            IBuySellUpDown.selector.addLevel(6, *Price.ud_nodes)            
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 2
            Logical.max_child = 4            
        return IBuySellUpDown.selector



class IBuyAndClose_SellAndClose_BuySellUpDown(Individual):
                  
    description = ''
    
    def __init__(self):
        super(IBuyAndClose_SellAndClose_BuySellUpDown, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = random.choice([BuyCloseCommand, SellCloseCommand, BuySellCommand])(self.root_node)        
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command]
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.command.create(symbols)                        
        self.initializator(symbols, selector, self.command)                
        return self
        
                   
    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IBuyAndClose_SellAndClose_BuySellUpDown.get_selector()
        
                                                           
    @staticmethod
    def get_selector():
        if not IBuyAndClose_SellAndClose_BuySellUpDown.selector:
            IBuyAndClose_SellAndClose_BuySellUpDown.selector = NodeSelector()
            IBuyAndClose_SellAndClose_BuySellUpDown.selector.add(*All.nodes)            
            IBuyAndClose_SellAndClose_BuySellUpDown.selector.addLevel(0, OnOffSignal)
            IBuyAndClose_SellAndClose_BuySellUpDown.selector.addLevel(1, *Logical.nodes)
            IBuyAndClose_SellAndClose_BuySellUpDown.selector.addLevel(2, *Comparison.nodes)
            IBuyAndClose_SellAndClose_BuySellUpDown.selector.addLevel(3, *Indicator.non_price_specific_nodes)
            IBuyAndClose_SellAndClose_BuySellUpDown.selector.addLevel(3, *Math.nodes)
            IBuyAndClose_SellAndClose_BuySellUpDown.selector.addLevel(6, *Numeric.nodes)
            IBuyAndClose_SellAndClose_BuySellUpDown.selector.addLevel(6, *Price.ud_nodes)            
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 2
            Logical.max_child = 4            
        return IBuyAndClose_SellAndClose_BuySellUpDown.selector


class IBuyCloseAllUpDown(Individual):
                  
    description = ''
    
    def __init__(self):
        super(IBuyCloseAllUpDown, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 2
        self.root_node.max_child = 2
        self.buy_command = BuyFix(self.root_node)
        self.close_command = CloseAll(self.root_node)        
        self.root_node.addChild(self.buy_command)
        self.root_node.addChild(self.close_command)
        self.fixed_nodes = [self.buy_command, self.close_command]
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.buy_command.create(symbols)                        
        self.close_command.create(symbols)
        self.initializator(symbols, selector, self.buy_command)                
        self.initializator(symbols, selector, self.close_command)
        return self
    
    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IBuyCloseAllUpDown.get_selector()
                                                                                        
    @staticmethod
    def get_selector():
        if not IBuyCloseAllUpDown.selector:
            IBuyCloseAllUpDown.selector = NodeSelector()
            IBuyCloseAllUpDown.selector.add(*All.nodes)            
            IBuyCloseAllUpDown.selector.addLevel(0, OnSignal, OffSignal)
            IBuyCloseAllUpDown.selector.addLevel(1, *Logical.nodes)
            IBuyCloseAllUpDown.selector.addLevel(2, *Comparison.nodes)
            IBuyCloseAllUpDown.selector.addLevel(3, *Indicator.non_price_specific_nodes)
            IBuyCloseAllUpDown.selector.addLevel(3, *Math.nodes)
            IBuyCloseAllUpDown.selector.addLevel(6, *Numeric.nodes)
            IBuyCloseAllUpDown.selector.addLevel(6, *Price.ud_nodes)            
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 2
            Logical.max_child = 4            
        return IBuyCloseAllUpDown.selector



class ISellCloseAllUpDown(Individual):
                  
    description = ''
    
    def __init__(self):
        super(ISellCloseAllUpDown, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 2
        self.root_node.max_child = 2
        self.sell_command = SellFix(self.root_node)
        self.close_command = CloseAll(self.root_node)
        self.root_node.addChild(self.sell_command)
        self.root_node.addChild(self.close_command)
        self.fixed_nodes = [self.sell_command, self.close_command]        
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.sell_command.create(symbols)                                
        self.close_command.create(symbols)
        self.initializator(symbols, selector, self.sell_command)                
        self.initializator(symbols, selector, self.close_command)            
        return self

    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        ISellCloseAllUpDown.get_selector()
                                                                                                        
    @staticmethod
    def get_selector():
        if not ISellCloseAllUpDown.selector:
            ISellCloseAllUpDown.selector = IBuyCloseAllUpDown.get_selector()
        return ISellCloseAllUpDown.selector



# ---------------------------------------------------------------------------
#            NO LOGICALS
# ----------------------------------------------------------------------------

class IBuyAndClose_SellAndClose_BuySellDIFFNoLogicals(Individual):
                  
    description = ''
    
    def __init__(self):
        super(IBuyAndClose_SellAndClose_BuySellDIFFNoLogicals, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = random.choice([BuyCloseCommand, SellCloseCommand, BuySellCommand])(self.root_node)        
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command]
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.command.create(symbols)                        
        self.initializator(symbols, selector, self.command)                
        return self
        
                   
    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IBuyAndClose_SellAndClose_BuySellDIFFNoLogicals.get_selector()
        
                                                           
    @staticmethod
    def get_selector():
        if not IBuyAndClose_SellAndClose_BuySellDIFFNoLogicals.selector:
            IBuyAndClose_SellAndClose_BuySellDIFFNoLogicals.selector = NodeSelector()
            IBuyAndClose_SellAndClose_BuySellDIFFNoLogicals.selector.add(*All.nodes)            
            IBuyAndClose_SellAndClose_BuySellDIFFNoLogicals.selector.addLevel(0, OnOffSignal)
            IBuyAndClose_SellAndClose_BuySellDIFFNoLogicals.selector.addLevel(1, *Comparison.nodes)
            IBuyAndClose_SellAndClose_BuySellDIFFNoLogicals.selector.addLevel(2, *Indicator.non_price_specific_nodes)
            IBuyAndClose_SellAndClose_BuySellDIFFNoLogicals.selector.addLevel(5, *Price.diff_nodes)            
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 2
            Logical.max_child = 4            
        return IBuyAndClose_SellAndClose_BuySellDIFFNoLogicals.selector

class IOnlyOpenDIFFNoLogicals(Individual):
                  
    description = ''
    
    def __init__(self):
        super(IOnlyOpenDIFFNoLogicals, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = random.choice([BuyFix,SellFix])(self.root_node)        
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command]
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.command.create(symbols)                        
        self.initializator(symbols, selector, self.command)                
        return self
        
                   
    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IOnlyOpenDIFFNoLogicals.get_selector()
        
                                                           
    @staticmethod
    def get_selector():
        if not IOnlyOpenDIFFNoLogicals.selector:
            IOnlyOpenDIFFNoLogicals.selector = NodeSelector()
            IOnlyOpenDIFFNoLogicals.selector.add(*All.nodes)            
            IOnlyOpenDIFFNoLogicals.selector.addLevel(0, OnSignal,OffSignal)
            IOnlyOpenDIFFNoLogicals.selector.addLevel(1, *Comparison.nodes)
            IOnlyOpenDIFFNoLogicals.selector.addLevel(2, *Indicator.non_price_specific_nodes)
            IOnlyOpenDIFFNoLogicals.selector.addLevel(5, *Price.diff_nodes)            
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 2
            Logical.max_child = 4            
        return IOnlyOpenDIFFNoLogicals.selector

class IBuyAndClose_SellAndClose_BuySellUpDownNoLogicals(Individual):
                  
    description = ''
    
    def __init__(self):
        super(IBuyAndClose_SellAndClose_BuySellUpDownNoLogicals, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = random.choice([BuyCloseCommand, SellCloseCommand, BuySellCommand])(self.root_node)        
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command]
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.command.create(symbols)                        
        self.initializator(symbols, selector, self.command)                
        return self
        
                   
    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IBuyAndClose_SellAndClose_BuySellUpDownNoLogicals.get_selector()
        
                                                           
    @staticmethod
    def get_selector():
        if not IBuyAndClose_SellAndClose_BuySellUpDownNoLogicals.selector:
            IBuyAndClose_SellAndClose_BuySellUpDownNoLogicals.selector = NodeSelector()
            IBuyAndClose_SellAndClose_BuySellUpDownNoLogicals.selector.add(*All.nodes)            
            IBuyAndClose_SellAndClose_BuySellUpDownNoLogicals.selector.addLevel(0, OnOffSignal)            
            IBuyAndClose_SellAndClose_BuySellUpDownNoLogicals.selector.addLevel(1, *Comparison.nodes)
            IBuyAndClose_SellAndClose_BuySellUpDownNoLogicals.selector.addLevel(2, *Indicator.non_price_specific_nodes)
            IBuyAndClose_SellAndClose_BuySellUpDownNoLogicals.selector.addLevel(3, *Price.ud_nodes)            
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 2
            Logical.max_child = 4            
        return IBuyAndClose_SellAndClose_BuySellUpDownNoLogicals.selector
    
    
    
class IUptrenderBuyClose(Individual):
                  
    description = ''
    
    def __init__(self):
        super(IUptrenderBuyClose, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = random.choice([BuyCloseCommand])(self.root_node)
        self.onoffsignal = OnOffSignal(self.command)
        self.onoffsignal.parameters[0] = 1
        self.onoffsignal.parameters[1] = 1
        self.onoffsignal.parameters[2] = 0
        self.AND = AND(self.onoffsignal)        
        self.GE = GE(self.AND)
        self.P1 = Close(self.GE)        
        self.MA = MA(self.GE)        
        self.MA.parameters[0] = 1
        self.MA.parameters[1] = 7200     
        self.P2 = Close(self.MA)   
        
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command, self.onoffsignal, self.AND, self.GE, self.P1, self.MA, self.P2]
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.command.create(symbols)                        
        self.initializator(symbols, selector, self.AND)     
        self.P1.create(symbols)               
        self.P2.create(symbols)
        self.P1.shift = 0
        self.P2.shift = 0
        return self
        
                   
    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IUptrenderBuyClose.get_selector()
        
                                                           
    @staticmethod
    def get_selector():
        if not IUptrenderBuyClose.selector:
            IUptrenderBuyClose.selector = NodeSelector()            
            IUptrenderBuyClose.selector.add(*All.nodes)
            IUptrenderBuyClose.selector.addLevel(0, *Comparison.nodes)
            IUptrenderBuyClose.selector.addLevel(1, *Indicator.non_price_specific_nodes)
            IUptrenderBuyClose.selector.addLevel(2, *Price.ud_nodes)
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 1
            Logical.max_child = 1            
        return IUptrenderBuyClose.selector


class IUptrenderSellClose(Individual):
                  
    description = ''
    
    def __init__(self):
        super(IUptrenderSellClose, self).__init__(RootNode())
        #self.initializator = simpleGenomeBuilder                        
        self.root_node.min_child = 1
        self.root_node.max_child = 1
        self.command = random.choice([SellCloseCommand])(self.root_node)
        self.onoffsignal = OnOffSignal(self.command)
        self.onoffsignal.parameters[0] = 1
        self.onoffsignal.parameters[1] = 1
        self.onoffsignal.parameters[2] = 0
        self.AND = AND(self.onoffsignal)        
        self.GE = GE(self.AND)        
        self.MA = MA(self.GE)
        self.MA.parameters[0] = 1
        self.MA.parameters[1] = 7200     
        self.P2 = Close(self.MA)   
        self.P1 = Close(self.GE)
        self.root_node.addChild(self.command)
        self.fixed_nodes = [self.command, self.onoffsignal, self.AND, self.GE, self.P1, self.MA, self.P2]
        

    def build_genome(self, symbols, selector=None):
        if not selector:
            selector = self.get_selector()            
        self.command.create(symbols)                        
        self.initializator(symbols, selector, self.AND)     
        self.P1.create(symbols)               
        self.P2.create(symbols)
        self.P1.shift = 0
        self.P2.shift = 0
        return self
        
                   
    @staticmethod
    def build_selector(builder_params, allowed_nodes):
        IUptrenderSellClose.get_selector()
        
                                                           
    @staticmethod
    def get_selector():
        if not IUptrenderSellClose.selector:
            IUptrenderSellClose.selector = NodeSelector()            
            IUptrenderSellClose.selector.add(*All.nodes)
            IUptrenderSellClose.selector.addLevel(0, *Comparison.nodes)
            IUptrenderSellClose.selector.addLevel(1, *Indicator.non_price_specific_nodes)
            IUptrenderSellClose.selector.addLevel(2, *Price.ud_nodes)
            Math.min_child = 2
            Math.max_child = 4
            Logical.min_child = 1
            Logical.max_child = 1            
        return IUptrenderSellClose.selector

