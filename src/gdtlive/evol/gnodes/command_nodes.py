# -*- encoding: utf-8 -*-
'''
Created on Mar 9, 2011

@author: kulcsarb
'''
from gdtlive.evol.gnodes.GenomeBase import GTreeNode, ParameterDescriptor
from gdtlive.evol.gnodes.indicator_nodes import MA, EMA
from gdtlive.evol.gnodes.price_nodes import Open
from gdtlive.c.signal import transform_signal
from gdtlive.constants import NODETYPE_COMMANDGROUP, NODETYPE_COMMAND, NODETYPE_SIGNAL, NODETYPE_ONOFFSIGNAL
import random#, traceback
import numpy as np
import logging

log = logging.getLogger('gdtlive.evol.gnodes')

class CommandGroup(GTreeNode):
    type = NODETYPE_COMMANDGROUP
    child_type=NODETYPE_COMMAND
    min_child = 1
    max_child = 4
    classID = 199
        
    parameters_descriptor = [
                            ParameterDescriptor(int, 0, 2000,'groupId'),
                            ParameterDescriptor(float, 0.1, 1.0,'weight')
                            ]      
        
    def create(self, symbol):
        self.parameters[0] = 0
        self.parameters[1] = 1.0        
        
    def __call__(self):
        log.debug('%r --- ', self)
        for child in self.childs:
            child.group_id =  self.parameters[0]
            child()
    
    def is_valid(self):
        return True    
    

        
class Command(GTreeNode):
    type = NODETYPE_COMMAND
    child_type = NODETYPE_SIGNAL
    min_child = 1
    max_child = 1 
    classID = 200
    
    def __init__(self, parent=None, childs=None):   
        self.group_id = 0
        self.strategy_id = 0
        super(Command, self).__init__(parent, childs)
        
    def __call__(self):
        self.signal = self.childs[0]()
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    
    def create(self, symbol):
        self.symbol = random.choice(symbol) if hasattr(symbol, '__iter__') else symbol         
        return self        
    
    def execute(self, index):                         
        return self._execute(index)      
        
    def copy(self, g):
        g.symbol = self.symbol
        
    def encode(self):
        parameters = [self.symbol]
        if hasattr(self, 'parameters'):
            parameters.extend(self.parameters)                                    
        childs = [child.id for child in self.childs]
        parents = [parent.id for parent in self.parents]                        
        return [self.classID, parameters, parents, childs]    
    
    def decodeParameters(self, parameters):
        self.symbol = str(parameters[0])
        del parameters[0]
        if len(parameters):
            self.parameters = parameters[:]
        
    def is_valid(self):
        assert self.symbol
        assert self.childs[0]
        
                


class BuyFix(Command):
    classID = 201      
    
    def _execute(self, index):
        return self.account.buy(self.symbol, percent = 1.0, group=self.group_id, strategy_id=self.strategy_id)        


class SellFix(Command):        
    classID = 202
    
    def _execute(self, index):        
        return self.account.sell(self.symbol, percent = 1.0, group=self.group_id, strategy_id=self.strategy_id)        

    
class CloseAll(Command):
    classID = 203
        
    def _execute(self, index):        
        return self.account.closeAll(group=self.group_id, strategy_id=self.strategy_id)    

class CloseAllInstrument(Command):
    classID = 204
        
    def _execute(self, index):        
        return self.account.closeAllSymbol(self.symbol, group=self.group_id, strategy_id=self.strategy_id)

class CloseFirst(Command):
    classID = 205
        
    def _execute(self, index):        
        return self.account.closeFirstTrade(self.symbol, group=self.group_id, strategy_id=self.strategy_id)

class CloseLast(Command):
    classID = 206
        
    def _execute(self, index):        
        return self.account.closeLastTrade(self.symbol, group=self.group_id, strategy_id=self.strategy_id)
               
                
class ClosePercent(Command):    
    classID = 207
    
    parameters_descriptor = [
                             ParameterDescriptor(int,1,100,'percent')
                             ]
    
    def create(self, symbol):
        self.symbol = random.choice(symbol) if hasattr(symbol, '__iter__') else symbol        
        super(Command, self).create()
        return self   
    
    def _execute(self, index):        
        return self.account.closeAmountPercent(self.symbol, percent=self.parameters[0], group=self.group_id, strategy_id=self.strategy_id)

    def is_valid(self):
        assert self.symbol
        assert self.parameters[0] >= 0
        assert self.childs[0]
        
        
class SlTpBase(Command):
    classID = 208
        
    def create(self, symbol):        
        self.symbol = random.choice(symbol) if hasattr(symbol, '__iter__') else symbol
        
        i_class = random.choice((MA, EMA))
        indicator = i_class(self).create(symbol)
        price = Open(indicator).create(self.symbol)
        
        indicator.addChild(price)        
        self.addChild(indicator)        
                                
        return self
    
    def __call__(self):
        self.price_cache = self.childs[0]()
        self.signal = self.childs[1]()        
        return self.signal
    
    def is_valid(self):
        assert self.symbol
        assert self.childs[0]
        assert self.childs[1]
        
    def execute(self, index):
        try:
            if self.signal[index]:
                return self._execute(self.price_cache[index])
        except:
            pass


class Takeprofit(SlTpBase):
    classID = 209
        
    def _execute(self, price, index):        
        return self.account.setTakeprofitForAll(self.symbol, price, group=self.group_id, strategy_id=self.strategy_id)

    
class Stoploss(SlTpBase):
    classID = 210
            
    def _execute(self, price, index):        
        return self.account.setStoplossForAll(self.symbol, price, group=self.group_id, strategy_id=self.strategy_id)
        


class BuyCloseCommand(Command):
    classID = 211    
    type = NODETYPE_COMMAND
    child_type = NODETYPE_ONOFFSIGNAL
    min_child = 1
    max_child = 1 
    
    def __init__(self, parent=None, childs=None):
        super(BuyCloseCommand, self).__init__(parent, childs)
        self.trade = None
            
    def _execute(self, index):                
        if self.signal[index] > 0:            
            return self.account.buy(self.symbol, percent = 1.0, group=self.group_id, strategy_id=self.strategy_id)
        if self.signal[index] < 0:
            return self.account.closeLastTrade(self.symbol, group=self.group_id, strategy_id=self.strategy_id)                        
    
    
class Buy(Command):
    classID = 212        
    
    parameters_descriptor = [
                             ParameterDescriptor(int,1,5,'divider'),
                             ParameterDescriptor(int,1,3,'step'),                             
                             ]
    
    def create(self, symbol):
        self.symbol = random.choice(symbol) if hasattr(symbol, '__iter__') else symbol  
        self.parameters = [] 
        for index in range(len(self.parameters_descriptor)):
            self.parameters.append(self.createParameter(index))       
        return self        
    
    
    def __call__(self):
        if not self.parameters[0]: self.parameters[0] = 1
        if not self.parameters[1]: self.parameters[1] = 1         
        input = self.childs[0]()
        log.debug('%s input: %s' % (self.name, input[-10:]))        
        self.signal = np.zeros((len(input)), dtype=np.double)
        transform_signal(input, self.signal, self.parameters[0], self.parameters[1])
        log.debug('%s signal: %s' % (self.name, self.signal[-10:]))                        
        return self.signal
    
    def _execute(self, index):
        return self.account.buy(self.symbol, percent = self.signal[index], group=self.group_id, strategy_id=self.strategy_id)        


class Sell(Command):        
    classID = 213    
    
    parameters_descriptor = [
                             ParameterDescriptor(int,1,5,'divider'),
                             ParameterDescriptor(int,1,3,'step'),                             
                             ]
    
    def create(self, symbol):
        self.symbol = random.choice(symbol) if hasattr(symbol, '__iter__') else symbol    
        self.parameters = [] 
        for index in range(len(self.parameters_descriptor)):
            self.parameters.append(self.createParameter(index))     
        return self        
    
    
    def __call__(self):
        if not self.parameters[0]: self.parameters[0] = 1
        if not self.parameters[1]: self.parameters[1] = 1
        input = self.childs[0]()
        log.debug('%s input: %s' % (self.name, input[-10:]))        
        self.signal = np.zeros((len(input)), dtype=np.double)         
        transform_signal(input, self.signal, self.parameters[0], self.parameters[1])  
        log.debug('%s signal: %s' % (self.name, self.signal[-10:]))      
        return self.signal
        
    
    def _execute(self, index):        
        return self.account.sell(self.symbol, percent = self.signal[index], group=self.group_id, strategy_id=self.strategy_id)        



class SellCloseCommand(Command):
    classID = 214   
    type = NODETYPE_COMMAND
    child_type = NODETYPE_ONOFFSIGNAL
    min_child = 1
    max_child = 1 
    
    def __init__(self, parent=None, childs=None):
        super(SellCloseCommand, self).__init__(parent, childs)
        self.trade = None
            
    def _execute(self, index):                
        if self.signal[index] > 0:            
            return self.account.sell(self.symbol, percent = 1.0, group=self.group_id, strategy_id=self.strategy_id)
        if self.signal[index] < 0:
            return self.account.closeLastTrade(self.symbol, group=self.group_id, strategy_id=self.strategy_id)                        
    


class BuySellCommand(Command):
    classID = 215   
    type = NODETYPE_COMMAND
    child_type = NODETYPE_ONOFFSIGNAL
    min_child = 1
    max_child = 1 
    
    def __init__(self, parent=None, childs=None):
        super(BuySellCommand, self).__init__(parent, childs)
        self.trade = None
            
    def _execute(self, index):                      
        if self.signal[index] > 0:                        
            self.account.closeLastTrade(self.symbol, group=self.group_id, strategy_id=self.strategy_id)            
            return self.account.buy(self.symbol, percent = 1.0, group=self.group_id, strategy_id=self.strategy_id)
        if self.signal[index] < 0:                
            self.account.closeLastTrade(self.symbol, group=self.group_id, strategy_id=self.strategy_id)            
            return self.account.sell(self.symbol, percent = 1.0, group=self.group_id, strategy_id=self.strategy_id)            
            
        

Command.open_nodes = [Buy, Sell, BuyFix, SellFix, BuyCloseCommand, SellCloseCommand, BuySellCommand]
#Command.open_nodes = [BuyF, SellF]
#Command.open_nodes = [Buy, Sell]
Command.close_nodes = [CloseAll, CloseAllInstrument, CloseFirst, CloseLast]
Command.nodes = Command.close_nodes + [Takeprofit, Stoploss] + Command.open_nodes * 4
