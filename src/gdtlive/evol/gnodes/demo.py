# -*- encoding: utf-8 -*-
from gdtlive.evol.gnodes import *
from gdtlive.evol.gnodes.genomebuilders import defaultGenomeBuilder#, simpleGenomeBuilder
#from gdtlive.evol.gnodes.NodeSelector import NodeSelector
from gdtlive.evol.gnodes.GenomeBase import GTree
#from gdtlive.constants import *

####
#
#  Default értékek testreszabása  (opcionális)
#
####

RootNode.max_child = 2



####
#
#  Selector összeállítása
#
####

selector = NodeSelector()
#print selector
selector.add(*All.nodes)
#selector.addLevel(5, LE, LT, GT, GE, EQ, NE)
#selector.addLevel(10, Open, Low, Close, High)
#selector.addLevel(10, Int, Volume, Hour)

#quit()
selector.addLevel(3, OnOffSignal, OnSignal, OffSignal)
selector.addLevel(5, *Comparison.nodes)
selector.addLevel(9, *Numeric.nodes)
selector.addLevel(9, *Price.nodes)
#print selector

####
#
#  Strategia / Fa / Genom létrehozás    
#
####

root_node = defaultGenomeBuilder(('EURUSD','GBPJPY','USDCHF'), selector)
g = GTree(root_node)
g.processNodes()
print g






