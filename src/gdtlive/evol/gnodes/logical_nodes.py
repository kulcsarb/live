'''
Created on Mar 7, 2011

@author: kulcsarb
'''
from gdtlive.evol.gnodes.GenomeBase import GTreeNode
from gdtlive.constants import NODETYPE_BOOL
#import random
import numpy as np
import logging

log = logging.getLogger('gdtlive.evol.gnodes')


class Logical(GTreeNode):
    classID = 500        
    type = NODETYPE_BOOL 
    child_type = NODETYPE_BOOL   
    min_child = 2
    max_child = 4

    def calc(self, function):        
        show = False #True if str(self) == 'XOR' else False        
        result = self.childs[0]()
        if show:            
            print '=' * 50
            print self
            print self.childs[0].__class__.__name__            
            print result[-100:-1]
        for i in range(1, len(self.childs)):           
            t =  self.childs[i]()
            if show:
                print self.childs[i].__class__.__name__
                print t[-100:-1]
            result = function(result, t)
            if show:
                print self                      
                print result[-100:-1]
            
        return result  

    def is_valid(self):
        assert len(self.childs) >= 2
        
class AND(Logical):
    classID = 501
    def __call__(self):
        self.signal = self.calc(np.logical_and)
        log.debug('%s : %s' % (self.name, self.signal[-10:]))        
        return self.signal         

class OR(Logical):
    classID = 502
    def __call__(self):
        self.signal = self.calc(np.logical_or) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))        
        return self.signal

class XOR(Logical):
    classID = 503
    def __call__(self):
        self.signal = self.calc(np.logical_xor) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))        
        return self.signal

class NOT(Logical):
    classID = 504
    min_child = 1
    max_child = 1    
    def __call__(self):
        self.signal = self.calc(np.logical_not) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))        
        return self.signal
    
    def is_valid(self):
        assert len(self.childs) == 1 


Logical.nodes = [AND, OR, XOR, NOT]
