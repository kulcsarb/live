# -*- encoding: utf-8 -*-
'''
Created on Mar 22, 2011

@author: gdtlive
'''
import inspect
import random#, traceback
from collections import namedtuple
#from gdtlive.utils import R
from gdtlive.constants import NODETYPE_COMMAND, NODETYPE_COMMANDGROUP
from gdtlive.config import DUMP_LENGTH
import logging
import traceback
import os


log = logging.getLogger('gdtlive.evol.gnodes')

try:
#    import pydot
    HAVE_PYDOT = True
except ImportError:
    HAVE_PYDOT = False

ParameterDescriptor = namedtuple('ParameterDescriptor', 'type,min,max,name')


class GTree(object):

#    initializator = None


    def __init__(self, root_node):
        self.root_node = root_node
        self.tree_height = 0
        self.nodes_list = None
        self.score = 0.0
        self.fitness = 0.0
        self.evaluated = False
        self.node_count = 0
        self.fixed_nodes = []
        self._structure_hash = None
        self.command_groups = 0

    def __repr__(self):
        """String representation of Genome"""
#        ret = "Score:\t\t%.6f\n" % (self.score,)
#        ret = "Fitness:\t%.6f\n" % (self.fitness,)
#        ret += "Height:\t\t%d\n" % self.getHeight()
#        ret += "Nodes:\t\t%d\n" % self.getNodesCount()
        ret = self.getTraversalString()
        return ret

    def resetStats(self):
        """ Clear score and fitness of genome """
        self.score = 0.0
        self.fitness = 0.0

#    def initialize(self, **args):
#        """ Called to initialize genome
#        
#        :param args: this parameters will be passed to the initializator
#        
#        """
#        self.initializator(self, **args)


    def mutate(self, mutator, frequency, intensity):
        """ Called to mutate the genome
        
        :param args: this parameters will be passed to the mutator
        :rtype: the number of mutations returned by mutation operator
        
        """
        GTreeNode.mutator = mutator
        mutable_nodes = self.getMutableNodes()
        mutated_num = int(len(mutable_nodes) * frequency * 0.01)
        mutated_num = min(mutated_num, len(mutable_nodes))
        if mutated_num:
            mutated_nodes = random.sample(mutable_nodes, mutated_num)
            for node in mutated_nodes:
                node.mutate(mutator, intensity)


    def getMutableNodes(self):
        return [node for node in self.nodes_list if node.isMutable()]


    def copy(self, g, node = None, node_parent = None):
        """ Copy the current contents GTree to 'g'
        
        :param g: the destination GTree tree
        
        .. note:: If you are planning to create a new chromosome representation, you
                  **must** implement this method on your class.
        """
        g.score = self.score
        g.fitness = self.fitness

        if node is None:
            g.tree_height = self.tree_height
            node = self.root_node

        if node is None: return None

        newnode = node.clone()

        if node_parent is None:
            g.setRoot(newnode)
        else:
            newnode.setParent(node_parent)
            node_parent.replaceChild(node, newnode)

        for ci in xrange(len(newnode)):
            GTree.copy(self, g, newnode.getChild(ci), newnode)

        return newnode

    def copy2(self, genome):
        genome.score = self.score
        genome.fitness = self.fitness
        genome.tree_height = self.tree_height
        genome.node_count = self.node_count
        genome.fixed_nodes = []                
        node_list = []
        
        for n in self.nodes_list:
            g = n.clone()
            if n in self.fixed_nodes:
                genome.fixed_nodes.append(g)                
            node_list.append(g)
        
        #print '----'
        #print node_list
        #print genome.fixed_nodes
                            
        #assert len(self.nodes_list), len(node_list)
        #print 'c2',
#        for i, n in enumerate(self.nodes_list):
#            assert node_list[i].__class__ == self.nodes_list[i].__class__
#            assert i == self.nodes_list[i].id
        #print 'c3',
        
        for i, node in enumerate(self.nodes_list):
            for parent in node.parents:
                #try:
                    #assert parent.id < len(self.nodes_list)
                node_list[i].addParent(node_list[parent.id])
                #except:
                #    print 'parent Assertion error:', parent.id
            for child in node.childs:
                #try:
                    #assert child.id < len(self.nodes_list)
                node_list[i].addChild(node_list[child.id])
#                except:
#                    print 'child Assertion error:', child.id
#                    print child
#                    print node
#                    print self.nodes_list
#                    print node_list
#                    raise Exception()

        genome.node_list = node_list
        genome.root_node = node_list[0]


    def clone(self):
        """ Clone this GenomeBase
        
        :rtype: the clone genome   
        
        .. note:: If you are planning to create a new chromosome representation, you
                  **must** implement this method on your class.
        """
        newcopy = GTree(None)
        self.copy2(newcopy)                        
        #print 'newcopy fixed:', newcopy.fixed_nodes
        newcopy.processNodes()
        #print 'after process:', newcopy.fixed_nodes        
        return newcopy

    def processNodes(self, cloning = False):
        """ Creates a *cache* on the tree, this method must be called
        every time you change the shape of the tree. It updates the
        internal nodes list and the internal nodes properties such as
        depth and height.
        """
        #print 'p1',
        if self.root_node is None: return
        
        self._structure_hash = None
        self.nodes_list = self.getAllNodes()
        #print 'p2',
        #self.checkConsistency()

        #self.nodes_leaf = filter(lambda n: n.isLeaf(), self.nodes_list)
        #self.nodes_branch = filter(lambda n: n.isLeaf() == False, self.nodes_list)
        self.node_count = len(self.nodes_list)

        for id, node in enumerate(self.nodes_list):
            node.id = id
        #print 'p3',
        #if not cloning:
        #    self.tree_height = self.getNodeHeight(self.getRoot())
        
        swappables = self.nodes_list[:]
        swappables.remove(self.root_node)
        try:        
            for fix_node in self.fixed_nodes:
                #print 'removing', fix_node
                swappables.remove(fix_node)
                
        except:
            pass
            print '--------'
            print self.nodes_list
            print swappables
            print fix_node
            print self.fixed_nodes
            print '-------------'
        
        self.swappables = {}
        for node in swappables:
            if node.type in self.swappables:
                self.swappables[node.type].append(node)
            else:
                self.swappables[node.type] = [node]
        self.swappableNodeTypes = set(self.swappables.keys())


    @property
    def structure_hash(self):
        if not self._structure_hash:
            structure = []
            for node in self.nodes_list:
                structure.append(node.classID)
            self._structure_hash = hash(tuple(structure))
        return self._structure_hash


    def checkConsistency(self):
        if not self.nodes_list:
            return
        for node in self.nodes_list:
            for child in node.childs:
                if node not in child.parents:
                    #print 'node not in parents!'
                    #print node, node.childs, child.parents,  
                    for parent in child.parents:
                        if parent not in self.nodes_list:
                            print 'invalid parent link removed'
                            child.parents.remove(parent)
                    child.addParent(node)
                    #print child.parents


    def getRoot(self):
        """ Return the tree root node 
        
        :rtype: the tree root node
        """
        return self.root_node

    def setRoot(self, root):
        """ Sets the root of the tree
        
        :param root: the tree root node
        """
        if not isinstance(root, GTreeNode):
            raise Exception("The root must be a node")
        self.root_node = root

    def getNodeDepth(self, node):
        """ Returns the depth of a node
        
        :rtype: the depth of the node, the depth of root node is 0
        """
        if node == self.getRoot(): return 0
        else:                    return 1 + self.getNodeDepth(node.getParent())

    def getNodeHeight(self, node):
        """ Returns the height of a node
        
        .. note:: If the node has no childs, the height will be 0.
        
        :rtype: the height of the node
        """
        height = 0
        if len(node) <= 0:
            return 0
        for child in node.getChilds():
            h_inner = self.getNodeHeight(child) + 1
            if h_inner > height:
                height = h_inner
        return height

    def getHeight(self):
        """ Return the tree height
        
        :rtype: the tree height
        """
        return self.tree_height

    def getNodesCount(self, start_node = None):
        """ Return the number of the nodes on the tree
        starting at the *start_node*, if *start_node* is None,
        then the method will count all the tree nodes.
        
        :rtype: the number of nodes
        """
        count = 1
        if start_node is None:
            start_node = self.getRoot()
        if start_node:
            for i in start_node.getChilds():
                count += self.getNodesCount(i)
        return count

    def getTraversalString(self, start_node = None, spc = 0):
        """ Returns a tree-formated string of the tree. This
        method is used by the __repr__ method of the tree
        for p in population:
        p.status = 0
        p.worker = -1
        p.id = -1

    print population
        :rtype: a string representing the tree
        """
        str_buff = ""
        if start_node is None:
            start_node = self.getRoot()
            str_buff += "%s\n" % start_node
        if start_node is None:
            return ''
        spaces = spc + 2
        for child_node in start_node.getChilds():
            str_buff += "%s%s\n" % (" " * spaces, child_node)
            str_buff += self.getTraversalString(child_node, spaces)
        return str_buff

    def traversal(self, callback, start_node = None):
        """ Traversal the tree, this method will call the
        user-defined callback function for each node on the tree
        
        :param callback: a function
        :param start_node: the start node to begin the traversal
        """
        if not inspect.isfunction(callback):
            raise Exception("The callback for the tree traversal must be a function")

        if start_node is None:
            start_node = self.getRoot()
            callback(start_node)
        for child_node in start_node.getChilds():
            callback(child_node)
            self.traversal(callback, child_node)

    def getRandomNode(self, node_type = 0):
        """ Returns a random node from the Tree
    
       :param node_type: 0 = Any, 1 = Leaf, 2 = Branch
       :rtype: random node
       """
        lists = (self.nodes_list, self.nodes_leaf, self.nodes_branch)
        cho = lists[node_type]
        if len(cho) <= 0:
            return None
        return random.choice(cho)


    def getAllNodes(self):
        """ Return a new list with all nodes
        
        :rtype: the list with all nodes
        """
        node_stack = []
        all_nodes = []
        tmp = None

        node_stack.append(self.getRoot())
        while len(node_stack) > 0:
            tmp = node_stack.pop()
            all_nodes.append(tmp)
            childs = tmp.getChilds()
            node_stack.extend(childs)

        return all_nodes


    def getCommandNodes(self):
        command_nodes = [node for node in self.nodes_list if node.type == NODETYPE_COMMAND ]
        return command_nodes

    def getCommandGroups(self):
        command_groups = [node for node in self.nodes_list if node.type == NODETYPE_COMMANDGROUP ]
        return command_groups


    def getSwappableNodes(self):
        swappables = self.nodes_list[:]
        swappables.remove(self.root_node)
        for fix_node in self.fixed_nodes:
            swappables.remove(fix_node)
        return swappables


    def getSwappableNodeTypes(self):
        return self.swappableNodeTypes


    def getCommonNodeTypesWith(self, other):
        return self.swappableNodeTypes & other.swappableNodeTypes


    def canMateWith(self, individual):
        return True if len(self.getCommonNodeTypesWith(individual)) else False


    def getSwappableNodeByType(self, type):
        return random.choice(self.swappables[type])


    def replaceNode(self, old_node, new_node):
        if not old_node in self.nodes_list:
            return False

        new_node.parents = []
        for parent in old_node.parents:
            parent.replaceChild(old_node, new_node)


    def prepareForBacktest(self, datarows, account):
        from gdtlive.evol.gnodes.price_nodes import Price
        from gdtlive.evol.gnodes.constant_nodes import Numeric
        from gdtlive.constants import PRICE_TIME
        #import time                        
        #self.processNodes()                
        for node in self.nodes_list:
            if node.type == NODETYPE_COMMAND:
                node.account = account
            if isinstance(node, Price):
                node.datarow = datarows
            if isinstance(node, Numeric):
                node.datarow_length = len(datarows.itervalues().next()[PRICE_TIME])
            if hasattr(node,'cache'):
                node.cache = None

        #s = time.time()
        log.debug('--- prepareForBacktest ---')
        self.precalc()
        #print 'precalc: %.5f' % (time.time() - s)


    def getLongestPeriod(self):
        return self.root_node.getMaxPeriod()


    @staticmethod
    def decode(genome_str):
        '''A node fa string reprezentációjából újjáépíti a node-fát
        
        @type genome_str : str
        @param genome_str : a GTree.encode metódusával létrehozott reprezentációs string  
        
        @rtype: GTree
        '''
        import ast
        from gdtlive.evol.gnodes import All
        try:
            encoded_genomes = ast.literal_eval(genome_str)
        except:
            print genome_str

        node_list = {}
        for genome in encoded_genomes:
            id = genome[0]
            node_list[id] = All.get(genome[1][0])

        for genome in encoded_genomes:
            node = node_list[genome[0]]
            parameters = genome[1][1]
            parents = genome[1][2]
            childs = genome[1][3]
            node.decodeParameters(parameters)
            for id in parents:
                node.addParent(node_list[id])
            for id in childs:
                node.addChild(node_list[id])

        genome = GTree(node_list[0])
        genome.processNodes()
        return genome


    def encode(self):
        '''Előállítja a node-fa string reprezentációját
        
        @rtype: str
        '''
        if not self.nodes_list:
            return str([])
        #self.checkConsistency()
        encoded = []
        for node in self.nodes_list:
            encoded.append([node.id, node.encode()])
        return str(encoded)


    def precalc(self):
        self.root_node()


    def __len__(self):
        return len(self.nodes_list)

    def __getitem__(self, index):
        return self.nodes_list[index]

    def __iter__(self):
        return iter(self.nodes_list)

    def getSymbols(self):
        from gdtlive.evol.gnodes.price_nodes import Price
        symbols = set()
        for node in self.nodes_list:
            if hasattr(node, 'symbol'):
                symbols.add(node.symbol)
        return symbols


    def getField(self, name):
        try:
            return self.__dict__[name]
        except:
            try:
                return self.performance['currency'][name]
            except:
                return 0


    def mergeWith(self, genome):
        from command_nodes import CommandGroup
        if not self.command_groups:
            commandgroup = CommandGroup()
            commandgroup.parameters[0] = self.command_groups
            commandgroup.parameters[1] = 1.0
            for command in self.root_node.childs:
                command.setParent(commandgroup, 0)
                commandgroup.addChild(command)

            commandgroup.addParent(self.root_node)
            self.root_node.childs = []
            self.root_node.addChild(commandgroup)
            self.command_groups += 1


        commandgroup = CommandGroup()
        commandgroup.parameters[0] = self.command_groups
        commandgroup.parameters[1] = 1.0
        for command in genome.root_node.childs:
            command.parents = []
            commandgroup.addChild(command)
        self.command_groups += 1
        commandgroup.addParent(self.root_node)

        self.processNodes()


    def splitPortfolio(self):
        from gdtlive.evol.gnodes import RootNode
        command_groups = self.getCommandGroups()
        print command_groups
        individuals = []
        for command_group in command_groups:
            root = RootNode()
            command_group.setParent(root, 0)
            root.addChild(command_group)
            genome = GTree(root)
            genome.processNodes()
            individuals.append(genome)
        return individuals


    def dump_signals(self, path):
        for node in self.nodes_list:
            node.dump_signal(path)
            
        

class GTreeNode(object):
    """ GTreeNodeBase Class - The base class for the node tree genomes
    
    :param parent: the parent node of the node
    :param childs: the childs of the node, must be a list of nodes   
    
    .. versionadded:: 0.6
       Added te *GTreeNode* class
    """

    child_type = None
    parameters_descriptor = []

    def __init__(self, parent = None, childs = None):
        self.parents = []
        self.childs = []
        self.id = 0

        if len(self.parameters_descriptor):
            self.parameters = []
            for index in range(len(self.parameters_descriptor)):
                self.parameters.append(0)

        if parent is not None:
            self.addParent(parent)

        if childs is not None:
            self.addChild(childs)


    def getChildType(self):
        if hasattr(self.child_type, '__iter__'):
            return random.choice(self.child_type)
        else:
            return self.child_type


    def isLeaf(self):
        """ Return True if the node is a leaf
        
        :rtype: True or False
        """
        return self.child_type == None

    def isCommand(self):
        return self.type == NODETYPE_COMMAND

    def getChild(self, index):
        """ Returns the index-child of the node
        
        :rtype: child node
        """
        return self.childs[index]

    def getChilds(self):
        """ Return the childs of the node
        
        .. warning :: use .getChilds()[:] if you'll change the list itself, like using childs.reverse(),
                      otherwise the original genome child order will be changed.
        
        :rtype: a list of nodes
        """
        return self.childs

    def addChild(self, child):
        """ Adds a child to the node
        
        :param child: the node to be added   
        """
        if type(child) == list:
            typecheck_list = filter(lambda x: not isinstance(x, GTreeNode), child)
            if len(typecheck_list) > 0:
                raise Exception("Childs must be a list of nodes")
            self.childs.extend(child)
        else:
            if child not in self.childs:
                self.childs.append(child)
                if self not in child.parents:
                    child.parents.append(self)


    def replaceChild(self, older, newer):
        """ Replaces a child of the node
        
        :param older: the child to be replaces
        :param newer: the new child which replaces the older
        """
        index = self.childs.index(older)
        self.childs[index] = newer
        if self not in newer.parents:
            newer.parents.append(self)


    def setParent(self, parent, index):
        """ Sets the parent of the node
        
        :param parent: the parent node
        """
        #if not isinstance(parent, GTreeNodeBase):
        #   Util.raiseException("The parent must be a node", TypeError)
        self.parents[index] = parent


    def getParent(self, index):
        """ Get the parent node of the node
        
        :rtype: the parent node
        """
        return self.parents[index]


    def getParentsCount(self):
        return len(self.parents)


    def addParent(self, parent):
        if parent not in self.parents:
            self.parents.append(parent)
            if self not in parent.childs:
                parent.childs.append(self)


    def removeParents(self):
        self.parents = []

    def getParameterCount(self):
        return len(self.parameters_descriptor)

    def getParameterDescriptor(self, index):
        return self.parameters_descriptor[index]

    def getParameterType(self, index):
        return self.parameters_descriptor[index].type

    def getParameterMin(self, index):
        return self.parameters_descriptor[index].min

    def getParameterMax(self, index):
        return self.parameters_descriptor[index].max

    def setParameter(self, index, value):
        self.parameters[index] = value

    def getParameter(self, index):
        return self.parameters[index]

    def setParameters(self, parameters):
        if type(parameters) == list:
            if parameters and len(parameters):
                self.parameters = [0] * len(self.parameters_descriptor)
                for i in range(len(parameters)):
                    self.parameters[i] = parameters[i]
            else:
                self.parameters = []
        elif parameters == None:
            self.parameters = parameters
        else:
            print '?????????????', parameters

    def createParameter(self, index):
        descriptor = self.parameters_descriptor[index]
        try:
            if descriptor.type == int:
                return random.randint(descriptor.min, descriptor.max)
            elif descriptor.type == float:
                return random.uniform(descriptor.min, descriptor.max)
            elif descriptor.type == list:
                return random.choice((descriptor.min, descriptor.max))
        except:
            print self.name
            print self.parameters_descriptor
            raise
        return 0


    def create(self, *args):
        self.parameters = []
        for index in range(len(self.parameters_descriptor)):
            self.parameters.append(self.createParameter(index))
        return self


    def mutate(self, mutator, intensity):

        for i, descriptor in enumerate(self.parameters_descriptor):
            if descriptor.type != list and descriptor.min != descriptor.max:
                self.parameters[i] = mutator(self.parameters[i], descriptor.min, descriptor.max, intensity)


    def isMutable(self):
        return bool(self.parameters_descriptor)

#    
#    def __getattr__(self, name):        
#        for index, descriptor in enumerate(self.parameters_descriptor):
#            if name == descriptor.name:
#                return self.parameters[index]        
#        
#    
#    def __setattr__(self, name, value):
#        for index, descriptor in enumerate(self.parameters_descriptor):
#            if name == descriptor.name:
#                self.parameters[index] = value
#                return 
#        super(GTreeNode, self).__setattr__(name, value)        
#    
#    def __getstate__(self): 
#        return self.__dict__
#    
#    def __setstate__(self, d): 
#        self.__dict__.update(d)


    def encode(self):
        parameters = None
        if hasattr(self, 'parameters'):
            parameters = self.parameters
        childs = [child.id for child in self.childs]
        parents = [parent.id for parent in self.parents]

        return [self.classID, parameters, parents, childs]

    def decodeParameters(self, parameters):
        self.setParameters(parameters)

    def __repr__(self):
        str_repr = "%s" % (self.name)
        if hasattr(self, 'parameters'):
            params = []
            for i in range(len(self.parameters_descriptor)):
                params.append(self.parameters_descriptor[i].name + '=' + str(self.parameters[i]))
            str_repr += '(' + ', '.join(params) + ')'
        return str_repr

    def __len__(self):
        return len(self.childs)


    @property
    def name(self):
        return self.__class__.__name__


    def copy(self, g):
        """ Copy the current contents GTreeNodeBase to 'g'
        
        :param g: the destination node      
        
        .. note:: If you are planning to create a new chromosome representation, you
                  **must** implement this method on your class.
        """
        #g.parents = self.parents[:]
        #g.childs = self.childs[:]
        if hasattr(self, 'parameters'):
            if self.parameters:
                g.parameters = self.parameters[:]
            else:                
                g.parameters = self.parameters


    def clone(self):
        """ Clone this GenomeBase
        
        :rtype: the clone genome   
        
        .. note:: If you are planning to create a new chromosome representation, you
                  **must** implement this method on your class.
        """
        newcopy = self.__class__()
        self.copy(newcopy)
#        if hasattr(self, 'parameters'):
#            if self.parameters:
#                newcopy.parameters = self.parameters[:]
#            else:                
#                newcopy.parameters = self.parameters

        return newcopy


    def cloneWithChildren(self):
        newcopy = self.__class__()
        self.copy(newcopy)

        for child in self.childs:
            nc = child.cloneWithChildren()
            newcopy.addChild(nc)
            
        return newcopy
    
    def print_subtree(self,depth=0):
        print " "*depth, self
        for child in self.childs:
            assert self in child.parents
            child.print_subtree(depth+1)

    def getMaxPeriod(self):
        if len(self.childs) == 0:
            return 0
        return max([child.getMaxPeriod() for child in self.childs])

    def dump_signal(self, path):
        try:
            if hasattr(self, 'signal'):
                fname = '%d_%s.npy' % (self.id, self.name)
                signal = self.signal[-1 * DUMP_LENGTH:]
                signal.dump(path + os.sep + fname)
        except:            
            log.error(traceback.format_exc())
