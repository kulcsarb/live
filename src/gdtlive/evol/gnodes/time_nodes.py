'''
Created on Mar 7, 2011

@author: kulcsarb
'''
from gdtlive.evol.gnodes.GenomeBase import GTreeNode
from gdtlive.constants import NODETYPE_TIME, NODETYPE_FLOAT, NODETYPE_BOOL
import numpy as np
#import calendar
import logging

log = logging.getLogger('gdtlive.evol.gnodes')


class DateTime(GTreeNode):
    classID = 900
    cache = None
    child_type = NODETYPE_TIME
    #type = NODETYPE_INT
    type = NODETYPE_FLOAT
    min_child = 1
    max_child = 1

    def is_valid(self):
        assert len(self.childs) == 1


class Hour(DateTime):
    classID = 901
    def __call__(self):        
        self.signal = np.array([d.hour for d in self.childs[0]()], dtype = np.double)
        log.debug('%s self.signal: %s' % (self.name, self.signal[-10:]))
        return self.signal

class Minute(DateTime):
    classID = 902
    def __call__(self):        
        self.signal = np.array([d.minute for d in self.childs[0]()], dtype = np.double)
        log.debug('%s self.signal: %s' % (self.name, self.signal[-10:]))
        return self.signal

class Day(DateTime):
    classID = 903
    def __call__(self):        
        self.signal = np.array([d.day for d in self.childs[0]()], dtype = np.double)
        log.debug('%s self.signal: %s' % (self.name, self.signal[-10:]))
        return self.signal

class Month(DateTime):
    classID = 904
    def __call__(self):        
        self.signal = np.array([d.month for d in self.childs[0]()], dtype = np.double)
        log.debug('%s self.signal: %s' % (self.name, self.signal[-10:]))
        return self.signal


class Weekday(DateTime):
    classID = 905
    child_type = NODETYPE_TIME
    type = NODETYPE_BOOL
    min_child = 1
    max_child = 1
    day = 0
    def __call__(self):
        self.signal = np.array([True if d.isoweekday() == self.day else False for d in self.childs[0]()], dtype = np.bool)
        log.debug('%s self.signal: %s' % (self.name, self.signal[-10:]))
        return self.signal

class Monday(Weekday):
    classID = 906
    day = 1

class Thuesday(Weekday):
    classID = 907
    day = 2

class Wednesday(Weekday):
    classID = 908
    day = 3

class Thursday(Weekday):
    classID = 909
    day = 4

class Friday(Weekday):
    classID = 910
    day = 5

class Sunday(Weekday):
    classID = 911
    day = 7

DateTime.nodes = [Hour, Minute, Day, Month]
Weekday.nodes = [Monday, Thuesday, Wednesday, Thursday, Friday, Sunday]


