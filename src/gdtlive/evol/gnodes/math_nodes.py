# -*- encoding: utf-8 -*- 
'''
Created on Mar 7, 2011

@author: kulcsarb
'''
from gdtlive.evol.gnodes.GenomeBase import GTreeNode
from gdtlive.constants import NODETYPE_FLOAT
#import random
import numpy as np
import logging

log = logging.getLogger('gdtlive.evol.gnodes')


class Math(GTreeNode):
    classID = 600
    type = NODETYPE_FLOAT 
    child_type = NODETYPE_FLOAT
    min_child = 2
    max_child = 4
    
    def calc(self, function):
        result = self.childs[0]()
        try:
            for i in range(1, len(self.childs)):            
                result = function(result, self.childs[i]())
        except:
            print self
            print self.childs
            print self.parents                
        return result                           

    def is_valid(self):
        assert len(self.childs) >= 2 and len(self.childs) <= 4
        
        
class Mul(Math):
    classID = 601    
    ''' left * right '''                   
    def __call__(self):
        self.signal = self.calc(np.multiply) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))    
        return self.signal

class Div(Math):
    classID = 602
    ''' left / right '''
    def __call__(self):
        self.signal = self.calc(np.divide)
        log.debug('%s : %s' % (self.name, self.signal[-10:]))    
        return self.signal 

class Add(Math):
    classID = 603
    ''' left + right '''
    def __call__(self):
        self.signal = self.calc(np.add)
        log.debug('%s : %s' % (self.name, self.signal[-10:]))    
        return self.signal

class Sub(Math):
    classID = 604
    ''' left - right '''
    def __call__(self):
        self.signal = self.calc(np.subtract) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))    
        return self.signal

#class Mod(Math):
#    classID = 605    
#    ''' left % right '''
#    max_child = 2
#    def __call__(self):
#        return self.calc(np.fmod)
#    
#    def is_valid(self):
#        assert len(self.childs) == 2        
#
#class Remainder(Math):
#    classID = 606
#    ''' left % right '''
#    max_child = 2
#    def __call__(self):
#        return self.calc(np.remainder)        
#    
#    def is_valid(self):
#        assert len(self.childs) == 2
#
#class Power(Math):
#    classID = 607
#    max_child = 2
#    def __call__(self):
#        return self.calc(np.power)
#    
#    def is_valid(self):
#        assert len(self.childs) == 2
#        
#
#class Negative(Math):
#    classID = 608
#    min_child = 1    
#    max_child = 1
#    def __call__(self):
#        return np.negative(self.childs[0]())
#    
#    def is_valid(self):
#        assert len(self.childs) == 1
#    
#class Reciprocal(Math):
#    classID = 609
#    min_child = 1    
#    max_child = 1
#    def __call__(self):
#        return np.reciprocal(self.childs[0]())
#
#    def is_valid(self):
#        assert len(self.childs) == 1
    
#Math.nodes = [Mul, Div, Add, Sub, Mod, Remainder, Power, Negative, Reciprocal]
Math.nodes = [Mul, Div, Add, Sub]

