#from gdtlive.evol.gnodes.binary_nodes import *
from gdtlive.evol.gnodes.command_nodes import *
from gdtlive.evol.gnodes.compare_nodes import *
from gdtlive.evol.gnodes.constant_nodes import *
from gdtlive.evol.gnodes.indicator_nodes import *
from gdtlive.evol.gnodes.logical_nodes import *
from gdtlive.evol.gnodes.math_nodes import *
from gdtlive.evol.gnodes.price_nodes import *
from gdtlive.evol.gnodes.time_nodes import *
from gdtlive.evol.gnodes.signal_nodes import *

from gdtlive.constants import NODETYPE_COMMAND, NODETYPE_ROOT


class RootNode(GTreeNode):
    classID = 0
    type = NODETYPE_ROOT
    child_type = NODETYPE_COMMAND
    min_child = 1
    max_child = 4
    
    def __call__(self):
        for child in self.childs:
            child()
    
    def is_valid(self):
        return True    
        

class All:
    nodes = Price.all_nodes + Price.bool_nodes + Comparison.nodes + Numeric.nodes + Indicator.nodes + \
        Logical.nodes + DateTime.nodes + Weekday.nodes  + Command.nodes + [CommandGroup] + Signal.nodes + Math.nodes
    node_ids = None

    @staticmethod
    def init():        
        if not All.node_ids:
            All.node_ids = {}
            All.node_ids[0] = RootNode
            for node in All.nodes:
                All.node_ids[node.classID] = node 
    
    @staticmethod
    def get(id):        
        return All.node_ids[id]()
        
All.init()
