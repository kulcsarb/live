# -*- encoding: utf-8 -*-
'''
Created on Mar 23, 2011

@author: gdtlive
'''
from gdtlive.evol.gnodes.GenomeBase import GTreeNode, ParameterDescriptor
from gdtlive.constants import NODETYPE_SIGNAL, NODETYPE_BOOL, NODETYPE_ONOFFSIGNAL
from gdtlive.config import MAXIMUM_SHIFT
#import random, traceback
import numpy as np
from gdtlive.c.signal import on_signal, off_signal, onoff_signal
import gdtlive.c.transform as transform
import logging

log = logging.getLogger('gdtlive.evol.gnodes')


class Signal(GTreeNode):
    classID = 800
    type = NODETYPE_SIGNAL
    child_type = NODETYPE_BOOL
    min_child = 1
    max_child = 1 
    
    parameters_descriptor = [
                             ParameterDescriptor(list,-1,1,'invert'),
                             ParameterDescriptor(float,0.1,1,'weight'),
                             ParameterDescriptor(int,0,MAXIMUM_SHIFT,'shift')
                             ]

    def is_valid(self):
        if not self.parameters[0]:
            self.parameters[0] = 1
        assert len(self.childs) == 1
        assert self.parameters[0]
        assert self.parameters[1]
        
    def getMaxPeriod(self):
        if len(self.childs) == 0:
            return 0
        shift = 0
        try:
            shift = self.parameters[2]
        except:
            pass
        return max([child.getMaxPeriod()+shift for child in self.childs])
    
        
class OnSignal(Signal):
    classID = 801
    def __call__(self):
        input = self.childs[0]()
        self.signal = np.zeros((len(input)), dtype=np.double)
        invert = self.parameters[0]
        weight = self.parameters[1]
                
        validFrom = self.childs[0].getMaxPeriod()       #TODO: may be reduced in the future for possible earlier data
        
        on_signal(input, self.signal, weight, invert, validFrom)
        log.debug('%s signal: %s' % (self.name, self.signal[-10:]))
        
        transform.shift(self.signal, self.parameters[2])
        log.debug('%s shift: %s' % (self.name, self.signal[-10:]))
        
        return self.signal

class OffSignal(Signal):
    classID = 802
    def __call__(self):
        input = self.childs[0]()
        self.signal = np.zeros((len(input)), dtype=np.double)
        invert = self.parameters[0]
        weight = self.parameters[1]        
        
        validFrom = self.childs[0].getMaxPeriod()       #TODO: may be reduced in the future for possible earlier data
        
        off_signal(input, self.signal, weight, invert, validFrom)                  
        log.debug('%s signal: %s' % (self.name, self.signal[-10:]))
        
        transform.shift(self.signal, self.parameters[2])        
        log.debug('%s shift: %s' % (self.name, self.signal[-10:]))
        
        return self.signal


class OnOffSignal(GTreeNode):
    classID = 803
    type = NODETYPE_ONOFFSIGNAL
    child_type = NODETYPE_BOOL
    min_child = 1
    max_child = 1
    
    parameters_descriptor = [
                             ParameterDescriptor(list,-1,1,'invert'),
                             ParameterDescriptor(float,0.1,1,'weight'),
                             ParameterDescriptor(int,0,MAXIMUM_SHIFT,'shift')
                             ]
    
    def __call__(self):
        '''majd kitoltjuk'''
        input = self.childs[0]()
        self.signal = np.zeros((len(input)), dtype=np.double)
        invert = self.parameters[0]
        weight = self.parameters[1]        
        
        validFrom = self.childs[0].getMaxPeriod()       #TODO: may be reduced in the future for possible earlier data
        
        onoff_signal(input, self.signal, weight, invert, validFrom)
        log.debug('%s signal: %s' % (self.name, self.signal[-10:]))
        
        transform.shift(self.signal, self.parameters[2])        
        log.debug('%s shift: %s' % (self.name, self.signal[-10:]))
        
        return self.signal

    def is_valid(self):
        assert len(self.childs) == 1
        assert self.parameters[0] == 1 or self.parameters[0] == -1
        assert self.parameters[1] >= -1 and self.parameters[0] <= -1
    
    
    def getMaxPeriod(self):
        if len(self.childs) == 0:
            return 0
        shift = 0
        try:
            shift = self.parameters[2]
        except:
            pass
        return max([child.getMaxPeriod()+shift for child in self.childs])
        
        
class SignalAggregator(Signal):
    classID = 804
    type = NODETYPE_SIGNAL
    child_type = NODETYPE_SIGNAL
    min_child = 2
    max_child = 4

    def is_valid(self):
        assert len(self.childs) >= 2 and len(self.childs) <= 4
        
class AddSignals(SignalAggregator):
    classID = 805
    def __call__(self):
        return self.calc(np.add)
            
    def calc(self, function):
        self.signal = self.childs[0]()
        for i in range(1, len(self.childs)):            
            self.signal = function(self.signal, self.childs[i]())   
        log.debug('%s signal: %s' % (self.name, self.signal[-10:]))             
        return self.signal
    
            
class OnOffSignalAggregator(OnOffSignal):
    classID = 806
    type = NODETYPE_ONOFFSIGNAL
    child_type = NODETYPE_ONOFFSIGNAL
    min_child = 2
    max_child = 4

class AddOnOffSignals(OnOffSignalAggregator):
    classID = 807
    def __call__(self):
        return self.calc(np.add)        

    def calc(self, function):
        self.signal = self.childs[0]()
        for i in range(1, len(self.childs)):            
            self.signal = function(self.signal, self.childs[i]())
        
        log.debug('%s signal: %s' % (self.name, self.signal[-10:]))                
        return self.signal

    
Signal.nodes = [OnSignal, OffSignal, OnOffSignal, AddSignals, AddOnOffSignals]
Signal.signal_nodes = [OnSignal, OffSignal, OnOffSignal]
Signal.aggreg_nodes = [AddSignals, AddOnOffSignals]
    
    
    
