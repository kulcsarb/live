# -*- encoding: utf-8 -*-
'''
Created on Mar 4, 2011

@author: kulcsarb
'''
from gdtlive.evol.gnodes.GenomeBase import GTreeNode
from gdtlive.constants import NODETYPE_FLOAT, NODETYPE_BOOL, PRICE_ASK, PRICE_BID, NODETYPE_TIME, PRICE_TIME, PRICE_VOLUME, PRICE_OPEN, PRICE_CLOSE, PRICE_LOW, PRICE_HIGH, PIP_MULTIPLIER
from gdtlive.c.transform import shift, shift_n
from gdtlive.config import MAXIMUM_SHIFT
import random, traceback
import gdtlive.c.transform as transform
import logging
import numpy as np

log = logging.getLogger('gdtlive.evol.gnodes')


class Price(GTreeNode):
    classID = 700
    type = NODETYPE_FLOAT
    child_type = None
    min_child = 0
    max_child = 0

    price = None
    available_types = (PRICE_ASK, PRICE_BID)

    def __init__(self, parent = None, childs = None):
        super(Price, self).__init__(parent, childs)
        self.symbol = ''
        self.price_type = None
        self.shift = 0

    def create(self, symbol, price_type = None, shift = None):
        self.symbol = random.choice(symbol) if hasattr(symbol, '__iter__') else symbol
        if self.available_types:
            self.price_type = price_type if price_type is not None else random.choice(self.available_types)
        if shift is None:
            self.shift = random.randint(0, MAXIMUM_SHIFT)
        else:
            self.shift = shift
        return self

    def __repr__(self):
        if self.price_type is not None:
            return "%s[%s][%s][%d]" % (self.symbol, 'ASK' if self.price_type == PRICE_ASK else 'BID', self.name, self.shift)
        else:
            return "%s[%s][%d]" % (self.symbol, self.name, self.shift)

    def __call__(self):
        try:           
            data = self.datarow[self.symbol][self.price_type * 4 + self.price]   
            log.debug('%s data: %s' % (self.name, data[-10:]))         
            data = shift_n(data, self.shift)
            log.debug('%s data shifted: %s' % (self.name, data[-10:]))            
            return data            
        except:
            print traceback.format_exc()

    def encode(self):
        childs = [child.id for child in self.childs]
        parents = [parent.id for parent in self.parents]
        return [self.classID, [self.symbol, self.price_type, self.shift], parents, childs]

    def setParameters(self, parameters):
        self.symbol = str(parameters[0])
        self.price_type = parameters[1]
        try:
            self.shift = parameters[2]
        except:
            pass

    def copy(self, g):
        g.symbol = self.symbol
        g.price_type = self.price_type
        g.shift = self.shift

    def is_valid(self):
        assert self.symbol
        assert self.price_type == PRICE_ASK or self.price_type == PRICE_BID
        assert self.price
        assert self.shift >= 0 
        
    def getMaxPeriod(self):
        return self.shift
    

class Time(Price):
    classID = 701
    type = NODETYPE_TIME
    price = PRICE_TIME
    available_types = None

    def __call__(self):
        signal = self.datarow[self.symbol][self.price]
        log.debug('%s : %s' % (self.name, signal[-10:])) 
        return signal

    def is_valid(self):
        assert self.symbol
        assert self.price == PRICE_TIME


class Volume(Price):
    classID = 702
    type = NODETYPE_FLOAT
    price = PRICE_VOLUME
    available_types = None

    def is_valid(self):
        assert self.price == PRICE_VOLUME

    def __call__(self):
        data = self.datarow[self.symbol][self.price]
        log.debug('%s data: %s' % (self.name, data[-10:]))
        data = shift_n(data, self.shift)        
        log.debug('%s data shifted: %s' % (self.name, data[-10:]))
        return data


class Open(Price):
    classID = 703
    price = PRICE_OPEN

class Close(Price):
    classID = 704
    price = PRICE_CLOSE

class Low(Price):
    classID = 705
    price = PRICE_LOW

class High(Price):
    classID = 706
    price = PRICE_HIGH



class PriceDiff(Price):

    def __call__(self):
        try:
            #log.debug('%s, %d, %d, %d, %d' % (self.symbol, self.shift, self.price_type, self.price, self.price_type * 4 + self.price))
            #log.debug('%s price: %s' % (self.name, self.datarow[self.symbol][self.price_type * 4 + self.price][-10:]))
            self.signal = transform.difference(self.datarow[self.symbol][self.price_type * 4 + self.price], 1, PIP_MULTIPLIER[self.symbol])
            log.debug('%s data: %s' % (self.name, self.signal[-10:]))
            self.signal = shift(self.signal, self.shift)           
            log.debug('%s data shifted: %s' % (self.name, self.signal[-10:]))             
            return self.signal
        except:
            print traceback.format_exc()

            
class OpenDiff(PriceDiff):
    classID = 707
    price = PRICE_OPEN
        
class CloseDiff(PriceDiff):
    classID = 708
    price = PRICE_CLOSE
        
class LowDiff(PriceDiff):
    classID = 709
    price = PRICE_LOW

class HighDiff(PriceDiff):
    classID = 710
    price = PRICE_HIGH


class VolumeDiff(PriceDiff):
    classID = 711
    type = NODETYPE_FLOAT
    price = PRICE_VOLUME
    available_types = None

    def is_valid(self):
        assert self.price == PRICE_VOLUME

    def __call__(self):
        self.signal = transform.difference(self.datarow[self.symbol][self.price], 1, PIP_MULTIPLIER[self.symbol])
        log.debug('%s data: %s' % (self.name, self.signal[-10:]))
        self.signal = shift(self.signal, self.shift)
        log.debug('%s data shifted: %s' % (self.name, self.signal[-10:]))
        return self.signal



class PriceUD(Price):

    def __call__(self):
        try:            
            self.signal = transform.updown(self.datarow[self.symbol][self.price_type * 4 + self.price], 1, PIP_MULTIPLIER[self.symbol])
            log.debug('%s data: %s' % (self.name, self.signal[-10:]))
            self.signal = shift(self.signal, self.shift)
            log.debug('%s data shifted: %s' % (self.name, self.signal[-10:]))
            #print self, data[:30]
            return self.signal
        except:
            print traceback.format_exc()


class OpenUD(PriceUD):
    classID = 712
    price = PRICE_OPEN
        
class CloseUD(PriceUD):
    classID = 713
    price = PRICE_CLOSE
        
class LowUD(PriceUD):
    classID = 714
    price = PRICE_LOW

class HighUD(PriceUD):
    classID = 715
    price = PRICE_HIGH

class VolumeUD(PriceUD):
    classID = 716
    type = NODETYPE_FLOAT
    price = PRICE_VOLUME
    available_types = None

    def is_valid(self):
        assert self.price == PRICE_VOLUME

    def __call__(self):        
        self.signal = transform.updown(self.datarow[self.symbol][self.price], 1, PIP_MULTIPLIER[self.symbol])
        log.debug('%s data: %s' % (self.name, self.signal[-10:]))
        self.signal = shift(self.signal, self.shift)
        log.debug('%s data shifted: %s' % (self.name, self.signal[-10:]))
        return self.signal



class PriceBool(Price):
    
    type = NODETYPE_BOOL
    
    def __call__(self):
        try:            
            data = transform.updown(self.datarow[self.symbol][self.price_type * 4 + self.price], 1, PIP_MULTIPLIER[self.symbol])
            data = shift(data, self.shift)            
            data = data.astype(np.bool)
            return data
        except:
            print traceback.format_exc()

class OpenBool(PriceBool):
    classID = 717
    price = PRICE_OPEN
        
class CloseBool(PriceBool):
    classID = 718
    price = PRICE_CLOSE
        
class LowBool(PriceBool):
    classID = 719
    price = PRICE_LOW

class HighBool(PriceBool):
    classID = 720
    price = PRICE_HIGH



Price.nodes = [Time, Open, High, Low, Close, Volume]
Price.diff_nodes = [Time, OpenDiff, HighDiff, LowDiff, CloseDiff, VolumeDiff]
Price.ud_nodes = [Time, OpenUD, HighUD, LowUD, CloseUD, VolumeUD]
Price.bool_nodes = [OpenBool, CloseBool, LowBool, HighBool]
Price.all_nodes = [Time, Open, High, Low, Close, Volume, OpenDiff, HighDiff, LowDiff, CloseDiff, VolumeDiff, OpenUD, HighUD, LowUD, CloseUD, VolumeUD]
