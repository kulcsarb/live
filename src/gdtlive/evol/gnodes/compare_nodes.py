# -*- encoding: utf-8 -*- 
'''
Created on Mar 7, 2011

@author: kulcsarb
'''
from gdtlive.evol.gnodes.GenomeBase import GTreeNode
from gdtlive.constants import NODETYPE_BOOL, NODETYPE_FLOAT
import numpy as np
import logging

log = logging.getLogger('gdtlive.evol.gnodes')


class Comparison(GTreeNode):    
    classID = 301
    type = NODETYPE_BOOL    
    child_type = NODETYPE_FLOAT
    min_child = 2
    max_child = 2
        
    def __init__(self, parent=None, childs=None):
        if self.__class__ == Comparison:
            raise Exception('%s is a base class, do not use it directly!' % self.name)
        super(Comparison, self).__init__(parent, childs)
    
    def is_valid(self):
        assert isinstance(self.childs[0], GTreeNode)  
        assert isinstance(self.childs[1], GTreeNode)  
    
    
class LE(Comparison):
    classID = 302       
    
    def __call__(self):  
        try:
            self.signal = np.less_equal(self.childs[0](), self.childs[1]())
            log.debug('%s : %s' % (self.name, self.signal[-10:]))
            return self.signal 
        except Exception as e:
            print self.childs[0]
            print self.childs[0]()
            print self.childs[0]().shape
            print self.childs[1]
            print self.childs[1]()
            print self.childs[1]().shape
            raise e
        
class LT(Comparison):
    classID = 303    
    
    def __call__(self):        
        try:
            self.signal = np.less(self.childs[0](), self.childs[1]())
            log.debug('%s : %s' % (self.name, self.signal[-10:]))
            return self.signal
        except Exception as e:
            print self.childs[0]
            print self.childs[0]()
            print self.childs[1]
            print self.childs[1]()
            raise e
    
class GT(Comparison):
    classID = 304
    
    def __call__(self):        
        try:
            self.signal = np.greater(self.childs[0](), self.childs[1]())
            log.debug('%s : %s' % (self.name, self.signal[-10:]))
            return self.signal
        except Exception as e:
            print self.childs[0]
            print self.childs[0]().shape
            print self.childs[0]()
            print self.childs[1]
            print self.childs[1]().shape
            print self.childs[1]()            
            raise e
        
class GE(Comparison):
    classID = 305
    
    def __call__(self):        
        try:
            self.signal = np.greater_equal(self.childs[0](), self.childs[1]())
            log.debug('%s : %s' % (self.name, self.signal[-10:])) 
            return self.signal
        except Exception as e:
            print self.childs[0]
            print self.childs[0]()
            print self.childs[1]
            print self.childs[1]()
            raise e
    
class EQ(Comparison):
    classID = 306
    
    def __call__(self):
        try:
            self.signal = np.equal(self.childs[0](), self.childs[1]())
            log.debug('%s : %s' % (self.name, self.signal[-10:]))
            return self.signal
        except Exception as e:
            print self.childs[0]
            print self.childs[0]()
            print self.childs[1]
            print self.childs[1]()
            raise e

class NE(Comparison):
    classID = 307
    
    def __call__(self):
        try:
            self.signal = np.not_equal(self.childs[0](), self.childs[1]())
            log.debug('%s : %s' % (self.name, self.signal[-10:]))
            return self.signal
        except Exception as e:
            print self.childs[0]
            print self.childs[0]()
            print self.childs[1]
            print self.childs[1]()
            raise e
    

Comparison.nodes = [LE, LT, GT, GE] #[EQ, NE]
