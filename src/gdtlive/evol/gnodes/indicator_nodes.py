# -*- encoding: utf-8 -*-
'''
@by_kincso:  szeretem a sütit!!!!

Created on Mar 4, 2011

@author: kulcsarb
'''
from gdtlive.evol.gnodes.GenomeBase import GTreeNode, ParameterDescriptor
from gdtlive.evol.gnodes.price_nodes import Open, High, Low, Close, Volume
from gdtlive.constants import NODETYPE_FLOAT, PRICE_ASK, PRICE_BID
import random, traceback
import gdtlive.c.talib as talib
import numpy as np
import logging

log = logging.getLogger('gdtlive.evol.gnodes')

class Indicator(GTreeNode):        
    child_type = NODETYPE_FLOAT
    type = NODETYPE_FLOAT
    min_child = 1
    max_child = 1 
    np_type = np.double
    classID = 1000
    
    def _child(self, index):
        try:
            np_array = self.childs[index]()
            new_array =  np_array if np_array.dtype==self.np_type else np_array.astype(self.np_type)
        except:
            print index
            print self.childs
            print self.childs[index]
            print self.childs[index]()
            raise Exception('')
        return new_array
    
#    def copy(self, g):        
#        super(Indicator, self).copy(g)
        #custom_attrs = ['high','low','close','open','volume']
        
        #for attr in custom_attrs:
        #    if hasattr(self, attr):                
        #        node = getattr(self,attr)
        #        setattr(g, attr, node.clone())

    def is_valid(self, childs=None):
        if hasattr(self, 'parameters_descriptor'):
            for i in range(len(self.parameters_descriptor)):                
                assert self.parameters[i] >= self.parameters_descriptor[i].min and self.parameters[i] <= self.parameters_descriptor[i].max
        if childs:
            for i, cls in enumerate(childs):
                assert isinstance(self.childs[i], cls)

    def getMaxPeriod(self):
        if len(self.childs) == 0:
            return 1
        try:
            maxperiod = max(self.parameters)+1
        except:
            maxperiod = 1
        return max([child.getMaxPeriod()+maxperiod for child in self.childs])


class AD(Indicator):
    classID = 1001
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 

    def create(self, instruments):        
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))
        self.childs = [                       
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                       
                       Close().create(instrument, price_type),
                       Volume().create(instrument)
                       ]
        for child in self.childs:
            child.addParent(self)                     
        
    def is_valid(self):
        super(AD, self).is_valid([High, Low, Close, Volume])    
                         
        
    def __call__(self):
        try:
            self.signal = talib.AD(self._child(0), self._child(1), self._child(2), self._child(3)) 
            log.debug('%s : %s' % (self.name, self.signal[-10:]))             
            return self.signal
        except:
            log.critical(traceback.format_exc()) 



class ADOSC(Indicator):
    classID = 1002
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 

    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'fastPeriod'),
                            ParameterDescriptor(int, 2, 200,'slowPeriod')
                            ]      
    
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
                    
        price_type = random.choice((PRICE_ASK, PRICE_BID))
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),
                       Volume().create(instrument)
                       ]
        for child in self.childs:
            child.addParent(self)          

    def is_valid(self):
        super(ADOSC, self).is_valid([High, Low, Close, Volume])
        
        
    def __call__(self):
        try:  
            self.signal = talib.ADOSC(self._child(0), self._child(1), self._child(2), self._child(3), self.parameters[0], self.parameters[1] )           
            log.debug('%s : %s' % (self.name, self.signal[-10:]))
            return self.signal 
        except:
            log.critical( traceback.format_exc() )
            

class ADX(Indicator):
    classID = 1003
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 

    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'period'),                            
                            ]      
    
    def create(self, instruments):        
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),                       
                       ]
        for child in self.childs:
            child.addParent(self)          

                         
    def is_valid(self):
        super(ADX, self).is_valid([High, Low, Close])

        
    def __call__(self):
        try:
            self.signal = talib.ADX(self._child(0), self._child(1), self._child(2), self.parameters[0])
            log.debug('%s : %s' % (self.name, self.signal[-10:]))            
            return self.signal 
        except:
            log.critical( traceback.format_exc() )


class ADXR(Indicator):
    classID = 1004
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 

    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'period'),                            
                            ]      
    
    def create(self, instruments):        
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))     
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),                       
                       ]
        for child in self.childs:
            child.addParent(self)          
     
    
    def is_valid(self):
        super(ADXR, self).is_valid([High, Low, Close])
                                 
        
    def __call__(self):
        try:
            self.signal = talib.ADXR(self._child(0), self._child(1), self._child(2), self.parameters[0]) 
            log.debug('%s : %s' % (self.name, self.signal[-10:]))            
            return self.signal
        except:
            log.critical( traceback.format_exc() )


class APO(Indicator):
    classID = 1005
    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'fastPeriod'),
                            ParameterDescriptor(int, 2, 200,'slowPeriod'),
                            ParameterDescriptor(int, 0, 7,'maType')
                            ]                        
                            
    def __call__(self):  
        self.signal = talib.APO(self._child(0), self.parameters[0], self.parameters[1], self.parameters[2])       
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
                                         

class AROON_UP(Indicator):
    classID = 1006
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 

    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'period'),                            
                            ]      
    
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))   
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type)                       
                       ]            
        for child in self.childs:
            child.addParent(self)          

                         
    def is_valid(self):
        super(AROON_UP, self).is_valid([High, Low])
                                 
    def __call__(self):
        try:
            self.signal = talib.AROON_UP(self._child(0), self._child(1), self.parameters[0]) 
            log.debug('%s : %s' % (self.name, self.signal[-10:]))            
            return self.signal
        except:
            log.critical( traceback.format_exc() )
            

class AROON_DOWN(Indicator):
    classID = 1007
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 

    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'period'),                            
                            ]      
    
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))       
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type)                       
                       ]       
        for child in self.childs:
            child.addParent(self)          

                         
    def is_valid(self):
        super(AROON_DOWN, self).is_valid([High, Low])
        
    def __call__(self):
        try:
            self.signal = talib.AROON_DOWN(self._child(0), self._child(1), self.parameters[0])
            log.debug('%s : %s' % (self.name, self.signal[-10:]))            
            return self.signal 
        except:
            log.critical( traceback.format_exc() )
            

class AROONOSC(Indicator):
    classID = 1008
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 

    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'period'),                            
                            ]      
    
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))    
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type)                       
                       ]          
        for child in self.childs:
            child.addParent(self)          

                         
    def is_valid(self):
        super(AROONOSC, self).is_valid([High, Low])
                
    def __call__(self):
        try:
            self.signal = talib.AROONOSC(self._child(0), self._child(1), self.parameters[0]) 
            log.debug('%s : %s' % (self.name, self.signal[-10:]))            
            return self.signal
        except:
            log.critical( traceback.format_exc() )
            
            

class ATR(Indicator):
    classID = 1009
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 

    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'period'),                            
                            ]      
    
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))    
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),
                       Close().create(instrument, price_type)
                       ]       
        for child in self.childs:
            child.addParent(self)          

                         
    def is_valid(self):
        super(ATR, self).is_valid([High, Low, Close])
                
    def __call__(self):
        try:
            self.signal = talib.ATR(self._child(0), self._child(1), self._child(2), self.parameters[0]) 
            log.debug('%s : %s' % (self.name, self.signal[-10:]))            
            return self.signal
        except:
            log.critical( traceback.format_exc() )


class AVGPRICE(Indicator):
    classID = 1010
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 

    
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))
        self.childs = [
                       Open().create(instrument, price_type),                       
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                       
                       Close().create(instrument, price_type),                       
                       ]       
        for child in self.childs:
            child.addParent(self)          
 
                         
    def is_valid(self):
        super(AVGPRICE, self).is_valid([Open, High, Low, Close])
                
    def __call__(self):
        try:
            self.signal = talib.AVGPRICE(self._child(0), self._child(1), self._child(2), self._child(3)) 
            log.debug('%s : %s' % (self.name, self.signal[-10:]))            
            return self.signal
        except:
            log.critical( traceback.format_exc() )
            

class BBANDS_UPPER(Indicator):
    classID = 1011
    
    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'period'),
                            ParameterDescriptor(float, 0.1, 3,'devUp'),
                            ParameterDescriptor(float, 0.1, 3,'devDown'),
                            ParameterDescriptor(int, 0, 7,'maType')
                            ]                        
                            
    def __call__(self):
        self.signal = talib.BBANDS_UPPER(self._child(0), self.parameters[0], self.parameters[1], self.parameters[2], self.parameters[3]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))        
        return self.signal
    
    
class BBANDS_MIDDLE(Indicator):
    classID = 1012
    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'period'),
                            ParameterDescriptor(float, 0.1, 3,'devUp'),
                            ParameterDescriptor(float, 0.1, 3,'devDown'),
                            ParameterDescriptor(int, 0, 7,'maType')
                            ]                        
                            
    def __call__(self):
        self.signal = talib.BBANDS_MIDDLE(self._child(0), self.parameters[0], self.parameters[1], self.parameters[2], self.parameters[3])
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    
           
class BBANDS_LOWER(Indicator):
    classID = 1013
    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'period'),
                            ParameterDescriptor(float, 0.1, 3,'devUp'),
                            ParameterDescriptor(float, 0.1, 3,'devDown'),
                            ParameterDescriptor(int, 0, 7,'maType')
                            ]                        
                            
    def __call__(self):
        self.signal = talib.BBANDS_LOWER(self._child(0), self.parameters[0], self.parameters[1], self.parameters[2], self.parameters[3]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))        
        return self.signal
           


class BETA(Indicator):
    classID = 1014
    min_child = 2
    max_child = 2
     
    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'period')
                            ]                               
    def __call__(self):
        self.signal = talib.BETA(self._child(0), self._child(1), self.parameters[0])         
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal 
                                   

class BOP(Indicator):
    classID = 1015
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 

    
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))
        self.childs = [
                       Open().create(instrument, price_type),                       
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                       
                       Close().create(instrument, price_type),                       
                       ]  
        for child in self.childs:
            child.addParent(self)          

                         
    def is_valid(self):
        super(BOP, self).is_valid([Open, High, Low, Close])
                
    def __call__(self):
        try:
            self.signal = talib.BOP(self._child(0), self._child(1), self._child(2), self._child(3)) 
            log.debug('%s : %s' % (self.name, self.signal[-10:]))            
            return self.signal
        except:
            log.critical( traceback.format_exc() )                                    
            


class CCI(Indicator):
    classID = 1016
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 

    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'period')
                            ]
    
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))          
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),                       
                       ]
        for child in self.childs:
            child.addParent(self)          

                         
    def is_valid(self):
        super(CCI, self).is_valid([High, Low, Close])
                
    def __call__(self):
        try:
            self.signal = talib.CCI(self._child(0), self._child(1), self._child(2), self.parameters[0]) 
            log.debug('%s : %s' % (self.name, self.signal[-10:]))            
            return self.signal
        except:
            log.critical( traceback.format_exc() )
            

class CMO(Indicator):
    classID = 1017
    parameters_descriptor = [    
                            ParameterDescriptor(int, 2, 200,'period')
                            ]                        
                            
    def __call__(self):
        self.signal = talib.CMO(self._child(0), self.parameters[0])
        log.debug('%s : %s' % (self.name, self.signal[-10:]))        
        return self.signal
    

class CORREL(Indicator):
    classID = 1018
    min_child = 2
    max_child = 2
     
    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'period')
                            ]                               
    def __call__(self):
        self.signal = talib.CORREL(self._child(0), self._child(1), self.parameters[0]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))        
        return self.signal
      

class DEMA(Indicator):
    classID = 1019
    parameters_descriptor = [    
                            ParameterDescriptor(int, 2, 200,'period')
                            ]                        
                            
    def __call__(self):        
        self.signal = talib.DEMA(self._child(0), self.parameters[0]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal

      
class DX(Indicator):
    classID = 1020
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 

    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'period')
                            ]
    
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))          
        
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),                       
                       ]
        for child in self.childs:
            child.addParent(self)          

                         
    def is_valid(self):
        super(DX, self).is_valid([High, Low, Close])
        
    def __call__(self):
        try:
            self.signal = talib.DX(self._child(0), self._child(1), self._child(2), self.parameters[0]) 
            log.debug('%s : %s' % (self.name, self.signal[-10:]))            
            return self.signal
        except:
            log.critical( traceback.format_exc() )


class EMA(Indicator):
    classID = 1021
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period')
                            ]
                                
    def __call__(self):    
        self.signal = talib.EMA(self._child(0), self.parameters[0])        
        log.debug('%s : %s' % (self.name, self.signal[-10:]))      
        return self.signal
            

class KAMA(Indicator):
    classID = 1022
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period')
                            ]
                                
    def __call__(self):          
        self.signal = talib.KAMA(self._child(0), self.parameters[0])
        log.debug('%s : %s' % (self.name, self.signal[-10:]))      
        return self.signal
            

class LINEARREG(Indicator):
    classID = 1023
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period')
                            ]
                                
    def __call__(self):   
        self.signal = talib.LINEARREG(self._child(0), self.parameters[0]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))              
        return self.signal
            

class LINEARREG_ANGLE(Indicator):
    classID = 1024
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period')
                            ]
                                
    def __call__(self):
        self.signal = talib.LINEARREG_ANGLE(self._child(0), self.parameters[0]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))                 
        return self.signal
            
            
class LINEARREG_INTERCEPT(Indicator):
    classID = 1025
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period')
                            ]
                                
    def __call__(self):      
        self.signal = talib.LINEARREG_INTERCEPT(self._child(0), self.parameters[0])            
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
               

class LINEARREG_SLOPE(Indicator):
    classID = 1026
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period')
                            ]
                                
    def __call__(self):           
        self.signal = talib.LINEARREG_SLOPE(self._child(0), self.parameters[0])       
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
            

class MA(Indicator):
    classID = 1027
    parameters_descriptor = [
                            ParameterDescriptor(int, 0, 7,'ma_type'),
                            ParameterDescriptor(int, 2, 200,'period')
                            ]                        
                            
    def __call__(self):        
        self.signal = talib.MA(self._child(0), self.parameters[1], self.parameters[0])
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal


class MACD(Indicator):
    classID = 1028
    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'fastPeriod'),
                            ParameterDescriptor(int, 2, 200,'slowPeriod'),
                            ParameterDescriptor(int, 2, 200,'signal'),                            
                            ]                        
                            
    def __call__(self):
        self.signal = talib.MACD(self._child(0), self.parameters[0], self.parameters[1], self.parameters[2])[0]         
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal


class MACD_SIGNAL(Indicator):
    classID = 1029
    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'fastPeriod'),
                            ParameterDescriptor(int, 2, 200,'slowPeriod'),
                            ParameterDescriptor(int, 2, 200,'signal')                         
                            ]                        
                            
    def __call__(self):        
        self.signal = talib.MACD(self._child(0), self.parameters[0], self.parameters[1], self.parameters[2])[1] 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    
class MACD_HIST(Indicator):
    classID = 1030
    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'fastPeriod'),
                            ParameterDescriptor(int, 2, 200,'slowPeriod'),
                            ParameterDescriptor(int, 2, 200,'signal')                        
                            ]                        
                            
    def __call__(self):        
        self.signal = talib.MACD(self._child(0), self.parameters[0], self.parameters[1], self.parameters[2])[2] 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
         


class MACDEXT(Indicator):
    classID = 1031
    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'fastPeriod'),
                            ParameterDescriptor(int, 0, 7,'fastMaType'),
                            ParameterDescriptor(int, 2, 200,'slowPeriod'),
                            ParameterDescriptor(int, 0, 7,'slowMaType'),
                            ParameterDescriptor(int, 2, 200,'signal'),
                            ParameterDescriptor(int, 0, 7,'signalMaType')                            
                            ]                        
                            
    def __call__(self):        
        self.signal = talib.MACDEXT(self._child(0), self.parameters[0], self.parameters[1], self.parameters[2], \
                    self.parameters[3], self.parameters[4], self.parameters[5])[0] 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    

class MACDEXT_SIGNAL(Indicator):
    classID = 1032
    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'fastPeriod'),
                            ParameterDescriptor(int, 0, 7,'fastMaType'),
                            ParameterDescriptor(int, 2, 200,'slowPeriod'),
                            ParameterDescriptor(int, 0, 7,'slowMaType'),
                            ParameterDescriptor(int, 2, 200,'signal'),
                            ParameterDescriptor(int, 0, 7,'signalMaType')                            
                            ]                        
                            
    def __call__(self):        
        self.signal = talib.MACDEXT(self._child(0), self.parameters[0], self.parameters[1], self.parameters[2], \
                    self.parameters[3], self.parameters[4], self.parameters[5])[1] 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal


class MACDEXT_HIST(Indicator):
    classID = 1033
    parameters_descriptor = [
                            ParameterDescriptor(int, 2, 200,'fastPeriod'),
                            ParameterDescriptor(int, 0, 7,'fastMaType'),
                            ParameterDescriptor(int, 2, 200,'slowPeriod'),
                            ParameterDescriptor(int, 0, 7,'slowMaType'),
                            ParameterDescriptor(int, 2, 200,'signal'),
                            ParameterDescriptor(int, 0, 7,'signalMaType')                            
                            ]                        
                            
    def __call__(self):        
        self.signal = talib.MACDEXT(self._child(0), self.parameters[0], self.parameters[1], self.parameters[2], \
                    self.parameters[3], self.parameters[4], self.parameters[5])[2] 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal

                                                                

class MACDFIX(Indicator):
    classID = 1034
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'signal'),                            
                            ]                        
                            
    def __call__(self):        
        self.signal = talib.MACDFIX(self._child(0), self.parameters[0])[0] 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal


class MACDFIX_SIGNAL(Indicator):
    classID = 1035
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'signal'),                            
                            ]                        
                            
    def __call__(self):
        self.signal = talib.MACDFIX(self._child(0), self.parameters[0])[1] 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))        
        return self.signal


class MACDFIX_HIST(Indicator):
    classID = 1036
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'signal'),                            
                            ]                        
                            
    def __call__(self):
        self.signal = talib.MACDFIX(self._child(0), self.parameters[0])[2]         
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal

                                                                

class MAMA_MAMA(Indicator):
    classID = 1037
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'fastPeriod'),                            
                            ParameterDescriptor(int, 2, 200,'slowPeriod'),
                            ]                        
                            
    def __call__(self):        
        self.signal = talib.MAMA(self._child(0), self.parameters[0], self.parameters[1])[0] 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal


class MAMA_FAMA(Indicator):
    classID = 1038
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'fastPeriod'),                            
                            ParameterDescriptor(int, 2, 200,'slowPeriod'),
                            ]                        
                            
    def __call__(self):        
        self.signal = talib.MAMA(self._child(0), self.parameters[0], self.parameters[1])[1] 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    
    
class MAX(Indicator):
    classID = 1039
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                
                            ]                        
                            
    def __call__(self):     
        self.signal = talib.MAX(self._child(0), self.parameters[0])   
        log.debug('%s : %s' % (self.name, self.signal[-10:])) 
        return self.signal


class MEDPRICE(Indicator):
    classID = 1040
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
    
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))     
        
        self.childs = [
                High().create(instrument, price_type),
                Low().create(instrument,price_type)                       
                ]       
        for child in self.childs:
            child.addParent(self)          

    
    def is_valid(self):
        super(MEDPRICE, self).is_valid([High, Low])
        
    def __call__(self):
        try:
            self.signal = talib.MEDPRICE(self._child(0), self._child(1)) 
            log.debug('%s : %s' % (self.name, self.signal[-10:]))            
            return self.signal
        except:
            log.critical( traceback.format_exc()      )


class MFI(Indicator):
    classID = 1041
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
    
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                
                            ]      
    
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))   
        self.childs = [                       
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                       
                       Close().create(instrument, price_type),
                       Volume().create(instrument)
                       ]        
        for child in self.childs:
            child.addParent(self)          

        
    def is_valid(self):
        super(MFI, self).is_valid([High, Low, Close, Volume])
                
    def __call__(self):
        try:  
            self.signal = talib.MFI(self._child(0), self._child(1), self._child(2), self._child(3), self.parameters[0])           
            log.debug('%s : %s' % (self.name, self.signal[-10:]))
            return self.signal 
        except:
            log.critical( traceback.format_exc() )



class MIDPOINT(Indicator):
    classID = 1042
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                
                            ]                        
                            
    def __call__(self):        
        self.signal = talib.MIDPOINT(self._child(0), self.parameters[0]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal



class MIDPRICE(Indicator):
    classID = 1043
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
    
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                
                            ]                        
                            
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))        
        self.childs = [
                High().create(instrument, price_type),
                Low().create(instrument,price_type)                       
                ]   
        for child in self.childs:
            child.addParent(self)          

    
    def is_valid(self):
        super(MIDPRICE, self).is_valid([High, Low])
        
    def __call__(self):    
        self.signal = talib.MIDPRICE(self._child(0), self._child(1), self.parameters[0])         
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    
    

class MIN(Indicator):
    classID = 1044
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                
                            ]                        
                            
    def __call__(self): 
        self.signal = talib.MIN(self._child(0), self.parameters[0])        
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal



class MINUS_DI(Indicator):
    classID = 1045
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
    
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                
                            ]                        
                            
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))    
        
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),                       
                       ]      
        for child in self.childs:
            child.addParent(self)          

    
    def is_valid(self):
        super(MINUS_DI, self).is_valid([High, Low, Close])
        
    def __call__(self):
        self.signal = talib.MINUS_DI(self._child(0), self._child(1), self._child(2), self.parameters[0]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))        
        return self.signal
    

class MINUS_DM(Indicator):
    classID = 1046
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
    
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                
                            ]                        
                            
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))          
        self.childs = [
                High().create(instrument, price_type),
                Low().create(instrument,price_type)                       
                ] 
        for child in self.childs:
            child.addParent(self)          

        
    def is_valid(self):
        super(MINUS_DM, self).is_valid([High, Low])
        
    def __call__(self): 
        self.signal = talib.MINUS_DM(self._child(0), self._child(1), self.parameters[0])        
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    


class MOM(Indicator):
    classID = 1047
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                
                            ]                        
                            
    def __call__(self):     
        self.signal = talib.MOM(self._child(0), self.parameters[0])    
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal



class NATR(Indicator):
    classID = 1048
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
    
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                
                            ]                        
                            
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))          
        
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),                       
                       ]
        for child in self.childs:
            child.addParent(self)          

        
    def is_valid(self):
        super(NATR, self).is_valid([High, Low, Close])
                                        
    def __call__(self):     
        self.signal = talib.NATR(self._child(0), self._child(1), self._child(2), self.parameters[0])    
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    


class OBV(Indicator):
    classID = 1049
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 1
    max_child = 1 
                               
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_class = random.choice([Open, High, Low, Close])
        price_type = random.choice((PRICE_ASK, PRICE_BID))
        self.childs = [
                       price_class().create(instrument, price_type),
                       Volume().create(instrument)
                       ]                                              
        
        for child in self.childs:
            child.addParent(self)          
                                
    def __call__(self):     
        self.signal = talib.OBV(self._child(0), self._child(1))    
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    


class PLUS_DI(Indicator):
    classID = 1050
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
    
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                
                            ]                        
                            
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))      
        
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),                       
                       ]    
        for child in self.childs:
            child.addParent(self)          


    def is_valid(self):
        super(PLUS_DI, self).is_valid([High, Low, Close])
                                        
    def __call__(self):      
        self.signal = talib.PLUS_DI(self._child(0), self._child(1), self._child(2), self.parameters[0])   
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    

class PLUS_DM(Indicator):
    classID = 1051
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
    
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                
                            ]                        
                            
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))    
        self.childs = [
                High().create(instrument, price_type),
                Low().create(instrument,price_type)                       
                ]        
        for child in self.childs:
            child.addParent(self)          

        
    def is_valid(self):
        super(PLUS_DM, self).is_valid([High, Low])
        
    def __call__(self):    
        self.signal = talib.PLUS_DM(self._child(0), self._child(1), self.parameters[0])     
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    

class PPO(Indicator):
    classID = 1052
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'fastPeriod'),                                
                            ParameterDescriptor(int, 2, 200,'slowPeriod'),
                            ParameterDescriptor(int, 0, 7,'maType'),
                            ]                        
                            
    def __call__(self):      
        self.signal = talib.PPO(self._child(0), self.parameters[0], self.parameters[1], self.parameters[2])   
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal



class ROC(Indicator):
    classID = 1053
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                         
                            ]                        
                            
    def __call__(self):       
        self.signal = talib.ROC(self._child(0), self.parameters[0])  
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal


class ROCP(Indicator):
    classID = 1054
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                         
                            ]                        
                            
    def __call__(self):       
        self.signal = talib.ROCP(self._child(0), self.parameters[0])  
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal


class ROCR(Indicator):
    classID = 1055
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                         
                            ]                        
                            
    def __call__(self):        
        self.signal = talib.ROCR(self._child(0), self.parameters[0]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal


class ROCR100(Indicator):
    classID = 1056
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                         
                            ]                        
                            
    def __call__(self):     
        self.signal = talib.ROCR100(self._child(0), self.parameters[0]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))   
        return self.signal
                                                                   
                                                                   
class RSI(Indicator):
    classID = 1057
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                         
                            ]                        
                            
    def __call__(self):        
        self.signal = talib.RSI(self._child(0), self.parameters[0]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal

                                                                   

class SAR(Indicator):
    classID = 1058
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
    
    parameters_descriptor = [                            
                            ParameterDescriptor(float, 0.1, 3,'acceleration'),                                
                            ParameterDescriptor(float, 0.1, 3,'maximum'),
                            ]
                            
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))          
        
        self.childs = [
                High().create(instrument, price_type),
                Low().create(instrument,price_type)                       
                ]  
        for child in self.childs:
            child.addParent(self)          

                            
    def is_valid(self):
        super(SAR, self).is_valid([High, Low])
                                    
    def __call__(self):        
        self.signal = talib.SAR(self._child(0), self._child(1), self.parameters[0], self.parameters[1]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
                                                                   

class SAREXT(Indicator):
    classID = 1059
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
    
    parameters_descriptor = [                            
                            ParameterDescriptor(float, 0.1, 3,'startValue'),                                
                            ParameterDescriptor(float, 0.1, 3,'offsetOnReverse'),
                            ParameterDescriptor(float, 0.1, 3,'accInitLong'),
                            ParameterDescriptor(float, 0.1, 3,'accLong'),
                            ParameterDescriptor(float, 0.1, 3,'accMaxLong'),
                            ParameterDescriptor(float, 0.1, 3,'accInitShort'),
                            ParameterDescriptor(float, 0.1, 3,'accShort'),
                            ParameterDescriptor(float, 0.1, 3,'accMaxShort')
                            ]
                            
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))     
        self.childs = [
                High().create(instrument, price_type),
                Low().create(instrument,price_type)                       
                ]      
        for child in self.childs:
            child.addParent(self)          

    def is_valid(self):
        super(SAREXT, self).is_valid([High, Low])
                                    
    def __call__(self):
        self.signal = talib.SAREXT(self._child(0), self._child(1), self.parameters[0], self.parameters[1], self.parameters[2], \
                   self.parameters[3], self.parameters[4], self.parameters[5], self.parameters[6], self.parameters[7]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal                                                                 

                                                                                                                   
class SMA(Indicator):
    classID = 1060
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                                         
                            ]                        
                            
    def __call__(self):     
        self.signal = talib.SMA(self._child(0), self.parameters[0])    
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal                                                                    

                            
class STDDEV(Indicator):
    classID = 1061
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),
                            ParameterDescriptor(float, 0.1, 2,'dev'),
                            ]                        
                            
    def __call__(self):        
        self.signal = talib.STDDEV(self._child(0), self.parameters[0], self.parameters[1]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    
                                                                                    
class STOCH_SLOWK(Indicator):
    classID = 1062
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
    
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 100,'fastKPeriod'),                                
                            ParameterDescriptor(int, 2, 200,'slowKPeriod'),
                            ParameterDescriptor(int, 0, 7,'slowKMaType'),
                            ParameterDescriptor(int, 2, 200,'slowDPeriod'),
                            ParameterDescriptor(int, 0, 7,'slowDMaType')                            
                            ]
                            
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))  
        
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),                       
                       ]        
        for child in self.childs:
            child.addParent(self)          
    
    def is_valid(self):
        super(STOCH_SLOWK, self).is_valid([High, Low, Close])
                                    
    def __call__(self):    
        self.signal = talib.STOCH_SLOWK(self._child(0), self._child(1), self._child(2), self.parameters[0], self.parameters[1], self.parameters[2], \
                   self.parameters[3], self.parameters[4]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))    
        return self.signal                                                                 


class STOCH_SLOWD(Indicator):
    classID = 1063
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
    
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 100,'fastKPeriod'),                                
                            ParameterDescriptor(int, 2, 200,'slowKPeriod'),
                            ParameterDescriptor(int, 0, 7,'slowKMaType'),
                            ParameterDescriptor(int, 2, 200,'slowDPeriod'),
                            ParameterDescriptor(int, 0, 7,'slowDMaType')                            
                            ]
                            
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))     
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),                       
                       ]     
        for child in self.childs:
            child.addParent(self)          

    def is_valid(self):
        super(STOCH_SLOWD, self).is_valid([High, Low, Close])
                                    
    def __call__(self):     
        self.signal = talib.STOCH_SLOWD(self._child(0), self._child(1), self._child(2), self.parameters[0], self.parameters[1], self.parameters[2], \
                   self.parameters[3], self.parameters[4]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))   
        return self.signal                                                                 


class STOCHF_FASTK(Indicator):
    """ A TALIB FUNKCIÓ RANDOM EREDMÉNYEKED AD VISSZA, TOVÁBBI VIZSGÁLATIG NEM SZABAD HASZNÁLNI"""
    
    classID = 1064
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
    
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 100,'fastKPeriod'),                                                            
                            ParameterDescriptor(int, 2, 100,'fastDPeriod'),
                            ParameterDescriptor(int, 0, 7,'fastDMaType')                                                        
                            ]
                            
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))       
        
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),                       
                       ]   
        for child in self.childs:
            child.addParent(self)          

                            
    def is_valid(self):
        super(STOCHF_FASTK, self).is_valid([High, Low, Close])
                                    
    def __call__(self):        
        self.signal = talib.STOCHF_FASTK(self._child(0), self._child(1), self._child(2), self.parameters[0], self.parameters[1], self.parameters[2])    
        log.debug('%s : %s' % (self.name, self.signal[-10:]))                         
        return self.signal


class STOCHF_FASTD(Indicator):
    classID = 1065
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
    
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 100,'fastKPeriod'),                                                            
                            ParameterDescriptor(int, 2, 100,'fastDPeriod'),
                            ParameterDescriptor(int, 0, 7,'fastDMaType')                                                        
                            ]
                            
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))        
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),                       
                       ]  
        for child in self.childs:
            child.addParent(self)          

        
    def is_valid(self):
        super(STOCHF_FASTD, self).is_valid([High, Low, Close])
                                    
    def __call__(self):
        self.signal = talib.STOCHF_FASTD(self._child(0), self._child(1), self._child(2), self.parameters[0], self.parameters[1], self.parameters[2])         
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal

                
class STOCHRSI_FASTD(Indicator):
    classID = 1066
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),
                            ParameterDescriptor(int, 2, 100,'fastKPeriod'),                                                            
                            ParameterDescriptor(int, 2, 100,'fastDPeriod'),
                            ParameterDescriptor(int, 0, 7,'fastDMaType')                                                        
                            ]
                            
    def __call__(self):     
        self.signal = talib.STOCHRSI_FASTD(self._child(0), self.parameters[0], self.parameters[1], self.parameters[2], self.parameters[3])    
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal


class STOCHRSI_FASTK(Indicator):
    classID = 1067
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),
                            ParameterDescriptor(int, 2, 100,'fastKPeriod'),                                                            
                            ParameterDescriptor(int, 2, 100,'fastDPeriod'),
                            ParameterDescriptor(int, 0, 7,'fastDMaType')                                                        
                            ]
                            
    def __call__(self):        
        self.signal = talib.STOCHRSI_FASTK(self._child(0), self.parameters[0], self.parameters[1], self.parameters[2], self.parameters[3]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal



class SUM(Indicator):
    classID = 1068
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period')
                            ]
                            
    def __call__(self):     
        self.signal = talib.SUM(self._child(0), self.parameters[0]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))   
        return self.signal


class T3(Indicator):
    classID = 1069
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),
                            ParameterDescriptor(float, 0.1, 1,'vFactor')
                            ]
                            
    def __call__(self):    
        self.signal = talib.T3(self._child(0), self.parameters[0], self.parameters[1])     
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    
    
class TEMA(Indicator):
    classID = 1070     
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                            
                            ]
                            
    def __call__(self):     
        self.signal = talib.TEMA(self._child(0), self.parameters[0])
        log.debug('%s : %s' % (self.name, self.signal[-10:]))   
        return self.signal
    


class TRANGE(Indicator):
    classID = 1071
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
                                
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))   
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),                       
                       ]       
        for child in self.childs:
            child.addParent(self)          

        
    def is_valid(self):
        super(TRANGE, self).is_valid([High, Low, Close])
                                    
    def __call__(self):    
        self.signal = talib.TRANGE(self._child(0), self._child(1), self._child(2)) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))    
        return self.signal


class TRIMA(Indicator):
    classID = 1072
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                            
                            ]
                            
    def __call__(self):     
        self.signal = talib.TRIMA(self._child(0), self.parameters[0]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))   
        return self.signal
    

class TRIX(Indicator):
    classID = 1073
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                            
                            ]
                            
    def __call__(self):  
        self.signal = talib.TRIX(self._child(0), self.parameters[0]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))      
        return self.signal



class TSF(Indicator):
    classID = 1074
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),                            
                            ]
                            
    def __call__(self):        
        self.signal = talib.TSF(self._child(0), self.parameters[0])
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal


class TYPPRICE(Indicator):
    classID = 1075
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
                                
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))    
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),                       
                       ]      
        for child in self.childs:
            child.addParent(self)          

    
    def is_valid(self):
        super(TYPPRICE, self).is_valid([High, Low, Close])
                                    
    def __call__(self):      
        self.signal = talib.TYPPRICE(self._child(0), self._child(1), self._child(2))   
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    


class ULTOSC(Indicator):
    classID = 1076
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
    
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period1'),                                                            
                            ParameterDescriptor(int, 2, 200,'period2'),
                            ParameterDescriptor(int, 1, 200,'period3')                                                        
                            ]
                            
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))     
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),                       
                       ]     
        for child in self.childs:
            child.addParent(self)          


    def is_valid(self):
        super(ULTOSC, self).is_valid([High, Low, Close])
                                    
    def __call__(self):       
        self.signal = talib.ULTOSC(self._child(0), self._child(1), self._child(2), self.parameters[0], self.parameters[1], self.parameters[2])  
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal


class VAR(Indicator):
    classID = 1077
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period'),
                            ParameterDescriptor(float, 0.1, 2.0,'dev')                           
                            ]
                            
    def __call__(self):        
        self.signal = talib.VAR(self._child(0), self.parameters[0], self.parameters[1]) 
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    

class WCLPRICE(Indicator):
    classID = 1078
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
                                
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))          
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),                       
                       ]
        for child in self.childs:
            child.addParent(self)          

    
    def is_valid(self):
        super(WCLPRICE, self).is_valid([High, Low, Close])
                                    
    def __call__(self):     
        self.signal = talib.WCLPRICE(self._child(0), self._child(1), self._child(2))    
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    


class WILLR(Indicator):
    classID = 1079
    child_type = None
    type = NODETYPE_FLOAT
    min_child = 0
    max_child = 0 
    
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period1')                                                                                    
                            ]
                            
    def create(self, instruments):
        super(self.__class__, self).create()
        if hasattr(instruments, '__iter__'):
            instrument = random.choice(instruments)
        else:
            instrument = instruments
        
        price_type = random.choice((PRICE_ASK, PRICE_BID))    
        self.childs = [
                       High().create(instrument, price_type),
                       Low().create(instrument, price_type),                                                                
                       Close().create(instrument, price_type),                       
                       ]      
        for child in self.childs:
            child.addParent(self)          

    
    def is_valid(self):
        super(WILLR, self).is_valid([High, Low, Close])                                    
    
    def __call__(self):     
        self.signal = talib.WILLR(self._child(0), self._child(1), self._child(2), self.parameters[0])    
        log.debug('%s : %s' % (self.name, self.signal[-10:]))
        return self.signal
    


class WMA(Indicator):
    classID = 1080
    parameters_descriptor = [                            
                            ParameterDescriptor(int, 2, 200,'period')                                             
                            ]
                            
    def __call__(self):      
        self.signal = talib.WMA(self._child(0), self.parameters[0])
        log.debug('%s : %s' % (self.name, self.signal[-10:]))  
        return self.signal
        
    
                                                                                                                                                                                            
Indicator.nodes = [AD, ADOSC, ADX, ADXR, APO, AROON_UP, AROON_DOWN, AROONOSC, ATR, AVGPRICE,
                   BBANDS_UPPER, BBANDS_LOWER, BBANDS_MIDDLE, BETA, BOP,
                   CCI, CMO, CORREL,
                   DEMA, DX, EMA, KAMA, 
                   LINEARREG, LINEARREG_ANGLE, LINEARREG_INTERCEPT, LINEARREG_SLOPE,
                   MA, #MACD,
                   #MACD_SIGNAL, MACD_HIST, 
                   #MACDEXT, MACDEXT_SIGNAL, MACDEXT_HIST, MACDFIX, MACDFIX_SIGNAL, MACDFIX_HIST,
                   MAMA_MAMA, MAMA_FAMA, MAX, MEDPRICE, MFI, MIDPOINT, MIDPRICE, MIN, MINUS_DI, MINUS_DM, MOM,
                   NATR, OBV,
                   PLUS_DI, PLUS_DM, PPO, 
                   ROC, ROCP, ROCR, ROCR100, RSI, 
                   SAR, SAREXT, SMA, STDDEV, SUM,STOCH_SLOWK, STOCH_SLOWD, STOCHF_FASTK, STOCHF_FASTD, STOCHRSI_FASTD, STOCHRSI_FASTK,
                   T3, TEMA, TRANGE, TRIMA, TRIX, TSF, TYPPRICE,
                   ULTOSC, VAR, WCLPRICE, WILLR, WMA                    
                    ]

Indicator.non_price_specific_nodes = [APO, BBANDS_UPPER, BBANDS_MIDDLE, BBANDS_LOWER,
                                      BETA, CMO, CORREL, DEMA, EMA, KAMA, LINEARREG, LINEARREG_ANGLE, LINEARREG_INTERCEPT,
                                      LINEARREG_SLOPE, MA, MAMA_MAMA, MAMA_FAMA, MAX, MIDPOINT, MIN, MOM, PPO, ROC, ROCP, 
                                      ROCR, ROCR100, RSI, SMA, STDDEV, STOCHRSI_FASTD, STOCHRSI_FASTK, SUM, T3, TEMA, 
                                      TRIMA, TRIX, TSF, VAR, WMA
                                      ]

    
#Indicator.nodes = [WMA, MA]