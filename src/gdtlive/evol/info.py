# -*- encoding: utf-8 -*- 
'''
Adatszolgáltató funkciók a GUI felé

A modul feladata, hogy ellássa adatokkal az evolúciós konfiguráció létrehozó/beállító képernyőjét

Created on 2010.12.11.

@author: kulcsarb
'''
#import gdtlive.store.db as db
#import gdtlive.store.ormObj as ormObj
from gdtlive.languages.default import OK
#from gdtlive.historic import prepareDescriptor
#from gdtlive.constants import *
#from gdtlive.utils import date2str
#import json
#from gdtlive.evol.gnodes import *
import cherrypy
from gdtlive.control.router import ajax_wrapper

#@cherrypy.expose
#@ajax_wrapper
#def genometree():
#    '''Az evolúció genetikai anyagának részletei választhatók ki egy fában, az ehhez szükséges részleteket adja vissza.
#
#    @url:     /info/genometree
#
#    @rtype:    list
#    @return:   a genom node-ok listája
#    '''
#    commands = []
#    signals = []
#    logicals = []
#    compares = []
#    maths = []
#    indicators = []
#    prices = []
#    times = []
#    
#    for node in Command.open_nodes + Command.close_nodes + [Takeprofit, Stoploss]:
#        commands.append({'text': node.__name__, 'id': node.classID})
#    for node in Signal.nodes:
#        signals.append({'text': node.__name__, 'id': node.classID})
#    for node in Logical.nodes:
#        logicals.append({'text': node.__name__, 'id': node.classID})
#    for node in Comparison.nodes:
#        compares.append({'text': node.__name__, 'id': node.classID})
#    for node in Math.nodes:
#        maths.append({'text': node.__name__, 'id': node.classID})
#    for node in Numeric.nodes:
#        maths.append({'text': node.__name__, 'id': node.classID})
#    for node in Indicator.nodes:
#        indicators.append({'text': node.__name__, 'id': node.classID})
#    for node in Price.all_nodes:
#        prices.append({'text': node.__name__, 'id': node.classID})
#    for node in DateTime.nodes:
#        times.append({'text': node.__name__, 'id': node.classID})
#    for node in Weekday.nodes:
#        times.append({'text': node.__name__, 'id': node.classID})
#        
#    result = [
#              {'text': 'Command', 'children' : commands},
#              {'text': 'Signal', 'children' : signals},
#              {'text': 'Logical', 'children' : [
#                        {'text': 'Logical', 'children' : logicals},
#                        {'text': 'Compare', 'children' : compares},
#                        ]},
#              {'text': 'Numeric', 'children' : [
#                        {'text': 'Math', 'children' : maths},
#                        {'text': 'Indicator', 'children' : indicators},
#                        {'text': 'Price', 'children' : prices},
#                        {'text': 'Time', 'children' : times}
#                        ]}                              
#               ]
#        
#    return json.dumps(result)
##                      [
##        {'text': 'Command', 'children' : [
##            {'text': 'Buy', 'id': 'command-buy'},
##            {'text': 'Sell', 'id': 'command-sell'},
##            {'text': 'CloseAll', 'id': 'command-closeall'},
##            {'text': 'Close', 'id': 'command-close'},
##            {'text': 'SetSL', 'id': 'command-setsl'},
##            {'text': 'SetTP', 'id': 'command-settp'}]},
##        {'text': 'Signal', 'children' : [
##            {'text': 'OnOffSignal', 'id': 'signal-onoffsignal'}]},
##        {'text': 'Logical', 'children' : [
##            {'text': 'Logical', 'children' : [
##                {'text': 'AND', 'id': 'logical-logical-and'},
##                {'text': 'OR', 'id': 'logical-logical-or'}]},
##            {'text': 'Compare', 'children' : [
##                {'text': 'GT', 'id': 'logical-compare-gt'},
##                {'text': 'LT', 'id': 'logical-compare-lt'}]}]},
##        {'text': 'Numeric', 'children' : [
##            {'text': 'Math', 'children' : [
##                {'text': '+', 'id': 'numeric-math-plus'},
##                {'text': '-', 'id': 'numeric-math-minus'},
##                {'text': '*', 'id': 'numeric-math-mult'},
##                {'text': '/', 'id': 'numeric-math-divide'}]},
##            {'text': 'Indicator', 'children' : [
##                {'text': 'MA', 'id': 'numeric-indicator-ma'},
##                {'text': 'RSI', 'id': 'numeric-indicator-rsi'},
##                {'text': 'WPR', 'id': 'numeric-indicator-wpr'}]},
##            {'text': 'Price', 'children' : [
##                {'text': 'Open', 'id': 'numeric-price-open'},
##                {'text': 'Close', 'id': 'numeric-price-close'}]},
##            {'text': 'Time', 'children' : [
##                {'text': 'Wednesday', 'id': 'numeric-time-wednesday'},
##                {'text': 'Tuesday', 'id': 'numeric-time-tuesday'}]}]}])

#@cherrypy.expose
#@ajax_wrapper
#def evolplugins():
#    '''Az evolúciós konfiguráció összeállitásához szükséges evol plugin adatokat adja vissza
#    Visszaadott adatok: evol plugin id, név  szótár lista
#
#    @url:     /info/evolplugins
#
#    @rtype:    (bool, str, list) tuple
#    @return:    Állapotjelzés, szöveges üzenet, az adatok név-érték szótárának listája
#    '''
#    from gdtlive.evol.plugin import evolplugins_list
#
#    evolPlugins = []
#    for k,v in evolplugins_list.iteritems():
#        evolPlugins.append({"id":k,"name":str(v).rpartition('.')[2].rstrip("'>")})
#                                            #v.__class__,__name__
#    return True, OK, evolPlugins

#@cherrypy.expose
#@ajax_wrapper
#def selectorplugins():
#    '''Az evolúciós konfiguráció összeállitásához szükséges money management plugin adatokat adja vissza
#    
#    Visszaadott adatok: money management plugin adatok szótár lista  
#     
#    @url:     /info/selectorplugins
#    
#    @rtype:    (bool, str, list) tuple
#    @return:    Állapotjelzés, szöveges üzenet, az adatok név-érték szótárának listája
#    '''
#    from gdtlive.evol.plugin import selector_list
#    
#    selectorPlugins = []
#    for k,v in selector_list.iteritems():
#        selectorPlugins.append({"id":k,"name":v.__name__})
#
#    return True, OK, selectorPlugins

#@cherrypy.expose
#@ajax_wrapper
#def mutatorplugins():
#    '''Az evolúciós konfiguráció összeállitásához szükséges money management plugin adatokat adja vissza
#    Visszaadott adatok: money management plugin adatok szótár lista  
#
#    @url:     /info/mutatorplugins
#
#    @rtype:    (bool, str, list) tuple
#    @return:    Állapotjelzés, szöveges üzenet, az adatok név-érték szótárának listája
#    '''
#    from gdtlive.evol.plugin import mutator_list
#    
#    mutatorPlugins = []
#    for k,v in mutator_list.iteritems():
#        mutatorPlugins.append({"id":k,"name":v.__name__})
#
#    return True, OK, mutatorPlugins

#@cherrypy.expose
#@ajax_wrapper
#def individualplugins():
#    '''Az evolúciós konfiguráció összeállitásához szükséges Individual osztályok listáját adja vissza
#        
#     
#    @url:     /info/individualplugins
#    
#    @rtype:    (bool, str, list) tuple
#    @return:    Állapotjelzés, szöveges üzenet, az adatok név-érték szótárának listája
#    '''
#    from gdtlive.evol.plugin import individual_list
#    
#    individualPlugins = []
#    for k,v in individual_list.iteritems():
#        individualPlugins.append({"id":k,"name":v.__name__,"info":v.description})
#
#    return True, OK, individualPlugins

@cherrypy.expose
@ajax_wrapper
def mmplugins():
    '''Az evolúciós konfiguráció összeállitásához szükséges money management plugin adatokat adja vissza
    Visszaadott adatok: money management plugin adatok szótár lista

    @url:     /info/mmplugins

    @rtype:    (bool, str, list) tuple
    @return:    Állapotjelzés, szöveges üzenet, az adatok név-érték szótárának listája
    '''
    from gdtlive.backtest import mm_plugins
    
    MMPlugins = []
    for k,v in mm_plugins.iteritems():
        MMPlugins.append({"id":k,"name":v})#str(v).rpartition('.')[2].rstrip("'>")})

    return True, OK, MMPlugins

@cherrypy.expose
@ajax_wrapper
def peakthroughplugins():
    '''Returns the list of the available peak-through plugins

    @url:     /info/peakthroughplugins

    @rtype:    (bool, str, list) tuple
    @return:    Állapotjelzés, szöveges üzenet, az adatok név-érték szótárának listája
    '''
    from gdtlive.backtest import peakthrough_plugins, peakthrough_plugins_pseudo
    
    PeakthroughPlugins = []
    for k,v in peakthrough_plugins.iteritems():
        PeakthroughPlugins.append({"id":k,"name":v})
    for k, v in peakthrough_plugins_pseudo.iteritems():
        PeakthroughPlugins.append({"id": k,"name": v, "type": 1})

    return True, OK, PeakthroughPlugins

#@cherrypy.expose
#@ajax_wrapper
#def fitnessplugins():
#    '''A fitness pluginok adatait adja vissza
#    Visszaadott adatok: fitness pluginok azonosítói és nevei   
#
#    @url:     /info/fitnessplugins
#
#    @rtype:    (bool, str, list) tuple
#    @return:    Állapotjelzés, szöveges üzenet, az adatok név-érték szótárának listája
#    '''
#
#    from gdtlive.evol.fitness import fitness_plugins
#    
#    fPlugins = []
#    for k,v in fitness_plugins.iteritems():
#        fPlugins.append({"id":k,"name":v.__name__})#str(v).rpartition('.')[2].rstrip("'>")})
#    return True, OK, fPlugins


#def getEvolDefaultData():
#    '''Az evolúciós konfiguráció összeállitásához szükséges összes adatot visszaadja
#          
#    
#    Visszaadott adatok:
#     - historikus adatok leirói 
#     - az összes felhasználó elmentett stratégia listái 
#     - az összes felhasználó elmentett keresései 
#     - evol plugin lista 
#     - feladatelosztási pluginek listája 
#     - spread,jutalék lista 
#     - money management plugin lista
#     
#    @url:     /evol/default
#    
#    @rtype:    (bool, str, dict) tuple
#    @return:    Állapotjelzés, szöveges üzenet, az adatok név-érték szótára
#    '''
#    d = db.DatarowDescriptor
##    id, symbol, from_date, to_date, timeframe, pluginId, download_date
##    id, symbol, from_date, to_date, timeframe_num, pluginId, download_date
#    o = [{"id":a,"symbol":b,"from_date":date2str(c),"to_date":date2str(d),"timeframe":TIMEFRAME.get(e),"pluginId":f,"download_date":date2str(g)} for a,b,c,d,e,f,g in db.session.query(d.id, d.symbol, d.from_date, d.to_date, d.timeframe_num, d.pluginId, d.download_date).all() ] 
#    evolPlugins = ["plugin01","plugin02","plugin03"]
#    spreadData = [{"id":id,"name":name} for id,name in db.session.query(db.SpreadConfig.id,db.SpreadConfig.name).all()]            
#
#    return True, OK, (o,evolPlugins,spreadData)

#if __name__ == '__main__':
#    print getGenomeTree()

#def get_evolscreen_data():
#    '''Az evolúciós konfiguráció összeállitásához szükséges összes adatot visszaadja
#    
#    Visszaadott adatok:
#     - historikus adatok leirói 
#     - az összes felhasználó elmentett stratégia listái 
#     - az összes felhasználó elmentett keresései 
#     - evol plugin lista 
#     - feladatelosztási pluginek listája 
#     - spread,jutalék lista 
#     - money management plugin lista
#     
#    @rtype: dict    
#    
#    '''
#    pass
#
#def get_plugin_params(evolPluginID):
#    '''Egy adott evolúciós plugin egyedi paramétereit adja vissza
#    
#    @type evolPluginID:  int
#    @param evolPluginID: az evolúciós plugin azonositója
#    ''' 
#    pass
#
#def get_mm_params(mmPluginID):
#    '''Adott money management plugin egyedi paramétereit adja vissza
#    
#    @type mmPluginID:  int
#    @param mmPluginID: a money management plugin azonositója
#    '''
#    pass
#
#def get_fitness_params(fitnessPluginID):
#    '''Adott fitness plugin bemeneti paramétereit adja vissza
#    
#    @type fitnessPluginID:  int
#    @param fitnessPluginID: a fitness plugin azonositója
#    '''
#    pass
    