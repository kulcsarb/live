# -*- encoding: utf-8 -*- 
'''
Stratégiák árfolyamadatokon való tesztelésére szolgáló modulok  
'''
mm_plugins = {1: "StandardMM"}
peakthrough_plugins = {1: "MinMax", 2: "Vendelin"}
peakthrough_plugins_pseudo = {3: "TrailingStop"}