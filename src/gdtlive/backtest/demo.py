# -*- encoding: utf-8 -*-
'''
Created on Mar 24, 2011

@author: gdt
'''
from backtester import Backtester, StratPlugins
#from accelerated.backtester import Backtester
import gdtlive.store.db as db
from gdtlive.evol.gnodes.GenomeBase import GTree
#from gdtlive.evol.gnodes.genomebuilders import *
#from gdtlive.evol.gnodes.NodeSelector import NodeSelector
import gdtlive.admin.system.log
from datetime import datetime#, timedelta
from gdtlive.constants import ORDERTYPE_STR, DIR_STR#, TIMEFRAME
#from gdtlive.config POSTLOAD_DAYS
#import cProfile, pstats
#import gzip, cStringIO, cPickle, timeit, time, os
#from gdtlive.evol.gnodes import *
#from gdtlive.evol.gnodes.individuals import Individual, IBuySell, IBuySellComplex, IBuyAndClose, ISellAndClose, IBuyAndCloseComplex, ISellAndCloseComplex, ISellCloseAll, IBuyCloseAll, ISellCloseAllComplex, IBuyCloseAllComplex
from gdtlive.utils import R, R2
import ast#, traceback
from gdtlive.constants import STRATEGYRUN_BACKTEST
from sqlalchemy.sql.expression import desc
#from gdtlive.evol.fitness import fitness_plugins
from sets import Set

gdtlive.admin.system.log.LOG_FORMAT = "%(levelname)s - %(message)s"
gdtlive.admin.system.log.initialize_loggers()

def dataToFile(path, data, jsonConvert = True, wrap = False):
    import json
    fp = open('/home/gdt/Desktop/gdt/' + path, 'w')
    if wrap:
        result = ''
        if type(data) == dict:
            if len(data.keys()) == 1:
                data = data[data.keys()[0]]
        if type(data) == dict:
            for key, value in data.iteritems():
                result = result + str(key) + ': ' + str(value) + '\n'
        else:
            for i in data:
                result = result + str(i) + '\n'
        data = result[0:-1]
    elif jsonConvert:
        data = json.dumps(data)
    fp.write(data)
    fp.close()    

def dataFromFile(path, jsonConvert = True):
    fp = open('/home/gdt/Desktop/gdt/' + path, 'r')
    data = fp.read()
    fp.close()    
    if (jsonConvert):
        import json
        data = json.loads(data)
    return data

def readLog(path):
    fp = open(path, 'r')
    log = []
    logline = fp.readline()
    while logline:
        log.append(ast.literal_eval(logline))
        logline = fp.readline()
    fp.close()
    return log

#def dictCompare(obj1, obj2):
#    success = True
#    if type(obj1) == str or type(obj1) == unicode:
#        obj1 = ast.literal_eval(obj1)
#    elif type(obj1) != dict:
#        obj1 = dict(obj1)
#    if type(obj2) == str or type(obj2) == unicode:
#        obj2 = ast.literal_eval(obj2)
#    elif type(obj2) != dict:
#        obj2 = dict(obj2)
#    for key, value in obj1.iteritems():
#        if key not in obj2:
#            print key, value
#        elif obj2[key] != value:
#            print key, value, obj2[key]
#            success = False
#    return success

#def checkPerfConsistence(stratId):
#    db.init()
#    backtest = Backtester()
#    session = db.Session()
#    strategy = session.query(db.Strategy).get(stratId)
#    genome = GTree.decode(strategy.dns)
#    backtest.configure(strategy.evolconfId)
#    backtest.use_orderlog(True)
#
#    from gdtlive.constants import STRATEGYRUN_BACKTEST
#    backtestStrategyRun = session.query(db.StrategyRun).filter(db.StrategyRun.strategyId==stratId).filter(db.StrategyRun.type==STRATEGYRUN_BACKTEST).first()
#    backtestConfig = session.query(db.BacktestConfig).get(backtestStrategyRun.configId)
#    evolConfig = session.query(db.EvolConfig).get(strategy.evolconfId)
#    if dictCompare(backtestConfig.mmplugin_parameters, evolConfig.mmplugin_parameters):
#        print 'mmparams OK'
#    else:
#        print 'mmparams differ:'
#        print backtestConfig.mmplugin_parameters
#        print evolConfig.mmplugin_parameters
#    backtest.from_date = backtestConfig.from_date
#    backtest.perf_from_date = backtestConfig.perf_from_date
#    backtest.to_date = backtestConfig.to_date
#    backtest.perf_to_date = backtestConfig.perf_to_date
#
#    savedPerformance = session.query(db.PerformanceData).get(backtestStrategyRun.performanceIdCurrency)
#    performance = backtest.evaluate(genome)
#    if dictCompare(savedPerformance, performance):
#        print 'performance OK'
#    else:
#        print 'performance differ:'
#        print savedPerformance
#        print performance
#
#    savedSerial = session.query(db.PerformanceStrategySerialData).filter(db.PerformanceStrategySerialData.id==backtestStrategyRun.performanceSerialId).first()
#    serial = backtest.strategy.get_serial_performance()
#    if dictCompare(savedSerial, serial):
#        print 'serial OK'
#    else:
#        print 'serial differ:'
#        print savedSerial
#        print serial
#
#    savedOrderlog = backtestStrategyRun.orderlog
#    orderlog = backtest.strategy.get_orderlog()
#    if savedOrderlog == orderlog:
#        print 'orderlog OK'
#    else:
#        print 'orderlog differ:'
#        print savedOrderlog
#        print orderlog
#
#    savedTradelog = backtestStrategyRun.tradelog
#    tradelog = backtest.strategy.get_tradelog()
#    if savedTradelog == tradelog:
#        print 'tradelog OK'
#    else:
#        print 'tradelog differ:'
#        print savedTradelog
#        print tradelog
#checkPerfConsistence(9139)

#def compReq(obj1, obj2):
#    if type(obj1) != type(obj2):
#        return 'type(obj1) = ' + str(type(obj1)) + ' != ' + str(type(obj2)) + ' = type(obj2)'
#    if type(obj1) == dict:
#        if len(obj1.keys()) != len(obj2.keys()):
#            return 'len(obj1.keys()) = ' + str(len(obj1.keys())) + ' != ' + str(len(obj2.keys())) + ' = len(obj2.keys())'
#        for prop in obj1.keys():
#            if prop not in obj2:
#                return str(prop) + ' not in obj2'
#            result = compReq(obj1[prop], obj2[prop])
#            if type(result) == 'str':
#                return result
#        return True
#    elif type(obj1) == list or type(obj1) == tuple:
#        if len(obj1) != len(obj2):
#            return 'len(obj1) = ' + str(len(obj1)) + ' != ' + str(len(obj2)) + ' = len(obj2)'
#        for i in xrange(len(obj1)):
#            result = compReq(obj1[i], obj2[i])
#            if type(result) == 'str':
#                return result
#        return True
#    elif type(obj1) == float or type(obj1) == int:
#        if obj1 != obj2:
#            return 'obj1 = ' + str(obj1) + ' != ' + str(obj2) + ' = obj2'
#        return True
#    else:
#        return 'unknown type: ' + str(type(obj1))
#    return True

#def compare(obj1, obj2):
#    result = compReq(obj1, obj2)
#    if type(result) == 'str':
#        print 'NOT EQUAL!!!'
#        print obj1
#        print obj2
#        print result
#        quit()

#def get_perf_from_date(backtest):
#    if backtest.perf_from_date:
#        return backtest.perf_from_date
#    return backtest.from_date
#
#def get_perf_to_date(backtest):
#    if backtest.perf_to_date:
#        return backtest.perf_to_date
#    return backtest.to_date + timedelta(days = POSTLOAD_DAYS)
#

#TODO: top
def timeKey(logType):
    if logType == 'orderlog':
        return 'time'
    if logType == 'tradelog':
        return 'opentime'
    if logType == 'plog':
        return 'time'
    if logType == 'plog':
        return 'time'
    if logType == 'slog':
        return 'time'
    raise Exception('unknown log type:' + str(logType))

def sumLog(sumlog, logType, valueKey):
    time = timeKey(logType)
    sumlog = sorted(sumlog, key=lambda log: log[time])
    currentTime = 0
    result = []
    for i in sumlog:
        if i[time] == currentTime:
            if valueKey in i:
                result[-1][valueKey] += i[valueKey]
        else:
            currentTime = i[time]
            result.append(i)
    dataToFile(logType + '_summed', result, wrap = True)
    return result

#def compareSaved():
#    logType = 'plog'
#    joinlist = (1, 2, 3, 4)
#    path = '/home/gdt/Desktop/gdt/'
#    mergedLog = readLog(path + logType + '0')
#    joinedLog = {}
#    for toJoin in joinlist:
#        joinedLog[str(toJoin)] = readLog(path + logType + str(toJoin))
#    joinedLog = joinLog(joinedLog, joinlist, logType)
#    compareLog(joinedLog, mergedLog, logType)
#    dataToFile(logType + '_merged', mergedLog, wrap = True)

def compareSaved2():
    logType = 'slog'
    path = '/home/gdt/Desktop/gdt/'
    valueKey = 'c'
    mergedLog = readLog(path + logType + '0')
    oldMergedLog = mergedLog
    mergedLog = []
    for log in oldMergedLog:
        if valueKey in log:
            mergedLog.append(log)
    joinedLog = readLog(path + logType + '1')
    joinedLog = sumLog(joinedLog, logType, valueKey)
    compareSumLog(joinedLog, mergedLog, logType, valueKey)

def joinLog(joinlistlog, joinlist, logType):
    result = []
    for index, strId in enumerate(joinlist):
        for entry in joinlistlog[strId]:
            entry['group'] = index
            result.append(entry)
    result = sorted(result, key=lambda log: log[timeKey(logType)])
    dataToFile(logType + '_joined', result, wrap = True)
    return result

def compareLog(log0, log1, logType):
    time = timeKey(logType)
    if len(log0) != len(log1):
        print 'len(log0) =', len(log0), '!=', len(log1), '= len(log1)'
    index2 = -1
    for index in xrange(len(log0)):
        if index < index2:
#            print index, '<', index2
            continue
        if log0[index] != log1[index]:
            currentTime = log0[index][time]
            if currentTime != log1[index][time]:
                print 'first diff at index', index, 'time differ:', currentTime, '!=', log1[index][time]
                return False
            index2 = index + 1
            if len(log0) < index2 + 1:
                print 'ended at index', index, 'but last one was not ok'
                return False
            while log0[index2][time] == currentTime:
                index2 += 1
                if len(log0) < index2 + 1:
                    break
            if index2 < index + 1:
                print 'first diff at index', index, logType, 'differ:', log0[index], '!=', log1[index]
                return False
            if Set([str(logstring) for logstring in log0[index:index2]]) != Set([str(logstring) for logstring in log1[index:index2]]):
                print 'first diff at set of indexes', index, index2, log0[index:index2], '!=', log1[index:index2]
                return False
            else:
                log1[index:index2] = log0[index:index2]
#                print index, ':', index2, 'OK'
#        else:
#            print index, 'OK'
    print 'the logs are the same'
    return True

def compareSumLog(log0, log1, logType, valueKey):
    time = timeKey(logType)
    limit = 5
    if len(log0) != len(log1):
        print 'len(log0) =', len(log0), '!=', len(log1), '= len(log1)'
    for index in xrange(len(log0)):
        if log0[index][time] != log1[index][time]:
            print 'first diff at index', index, 'time differ:', log0[index][time], '!=', log1[index][time]
            return False
        if abs(log0[index][valueKey] - log1[index][valueKey]) > limit:
            print 'first diff at index', index, 'value differ:', log0[index][valueKey], '!=', log1[index][valueKey]
            return False
    print 'the logs are the same'
    return True

def decodeOrderlog(log, group = -1):
    if log == '()' or log == '':
        return []
    try:
        log = ast.literal_eval(log)
    except Exception as e:
        print e
        print log
        raise
    res = []
    for r in log:
        tmp = dict(zip(["orderId", "time", "apicall", "executed", "symbols", "action", "affectedtrades", "group"], r))
        tmp["time"] = datetime.utcfromtimestamp(int(tmp["time"])).strftime('%Y-%m-%d %H:%M:%S')
        del tmp["orderId"]
        tmp["affectedtrades"] = len(ast.literal_eval(tmp["affectedtrades"]))
        if group == -1:
            tmp["group"] = int(tmp["group"])
        else:
            tmp["group"] = group
        res.append(tmp)
    return res

def decodeTradelog(log):
    result = []
    log = ast.literal_eval(log)
    if type(log[0][0]) != tuple:
        log = (log,)
    for r in log:
        fr = dict(zip(["id", "symbol", "direction", "opentime", "openprice", "openamount", "sl", "tp", "opencommission"], r[0]))
#        symbol = fr['symbol']
#        if symbol not in result:
#            result[symbol] = {}
#        del fr['symbol']
        fr["opentime"] = datetime.utcfromtimestamp(int(fr["opentime"])).strftime('%Y-%m-%d %H:%M:%S')
#        opentime = fr["opentime"]
#        del fr["opentime"]
#        del fr["openamount"]
        fr["direction"] = DIR_STR[fr["direction"]]
        fr["opencommission"] = round(fr["opencommission"], 2)
        fr["openprice"] = R(fr["openprice"])
        #res.append(fr)
#        tmp = dict(zip(["type", "closetime", "closeprice", "closeamount", "pip", "profit", "group", "closecommission", "rollover"], [0, 0, 0, 0, 0, 0, 0, 0, 0]))
        tmp = dict(zip(["type", "closetime", "closeprice", "pip", "profit", "closecommission", "rollover", "group"], r[1]))
        fr["closetime"] = datetime.utcfromtimestamp(int(tmp["closetime"])).strftime('%Y-%m-%d %H:%M:%S')
        fr["type"] = ORDERTYPE_STR[tmp["type"]]
        fr["closeprice"] = R(tmp["closeprice"])
        fr["closecommission"] = R2(tmp["closecommission"])
        fr["profit"] = R2(tmp["profit"])
        fr["group"] = tmp["group"]
        del fr["id"]
        result.append(fr)
    return result

def updateDict(oldO, newO):
    newOCopy = dict(newO)
    for name, value in newO.iteritems():
        if type(value) == dict:
            updateDict(oldO[name], value)
            del newOCopy[name]
        elif (type(value) == str or type(value) == unicode) and type(oldO[name]) != str and type(oldO[name]) != unicode:
            if value[0] == '*':
                newOCopy[name] = int(value[1:]) * oldO[name]
            elif value[0] == '/':
                newOCopy[name] = oldO[name] / float(value[1:])
    oldO.update(newOCopy)

def updateMM(backtest, mmPluginId, mmPluginParams, extraMM):
    if extraMM:
        updateDict(mmPluginParams, extraMM)
    backtest.strategy.configMM(mmPluginId, mmPluginParams)

def loadAndMerge(stratIds):
    db.init()
    session = db.Session()
    if type(stratIds) != tuple and type(stratIds) != list:
        stratIds = [stratIds]
    mergedDns = None
    for dns in session.query(db.Strategy.dns).filter(db.Strategy.id.in_(stratIds)).all():
        if not mergedDns:
            mergedDns = GTree.decode(dns[0])
        else:
            mergedDns.mergeWith(GTree.decode(dns[0]))
    return mergedDns.encode()

def evaluate(backtest, genome, result, returnResult = None, index = None, stratIds = None):
    performance = backtest.evaluate(genome)
    if stratIds:
        stratId = stratIds[index]
    else:
        stratId = index

    if returnResult['perf']:
        result['perf'][stratId] = performance

    if returnResult['serial']:
        serial_data = backtest.strategy.get_serial_performance()
        if type(returnResult['serial']) == list or type(returnResult['serial']) == tuple:
            result['serial'][stratId] = {}
            for i in returnResult['serial']:
                result['serial'][stratId][i] = serial_data[i]
        elif type(returnResult['serial']) == str:
            result['serial'][stratId] = {returnResult['serial']: serial_data[returnResult['serial']]}
        else:
            result['serial'][stratId] = serial_data

    if returnResult['orderlog']:
        result['orderlog'][stratId] = decodeOrderlog(backtest.strategy.get_orderlog())

    if returnResult['tradelog']:
        result['tradelog'][stratId] = decodeTradelog(backtest.strategy.get_tradelog())

def load_live(backtest, confId):
    '''Betölti adatbázisból a megadott evolúciós konfigurációt, és bekonfigurálja önmagát
    :Parameters:
        confId : int
            A használni kívánt LiveRunConfig objektum azonosítója
    '''
    backtest.evol_config = backtest.session.query(db.LiveRunConfig).get(confId)
    if not backtest.evol_config:
        raise OSError('Live config %d not found!' % confId)

    backtest.strategy = StratPlugins[1](backtest.session, 1, backtest.evol_config.mmplugin_id, 'gdt')
    backtest.strategy.load_spread(1)
    backtest.strategy.load_rollover(1)

    backtest.from_date = backtest.evol_config.running_from
#    backtest.from_date = datetime.strptime('2013-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
    backtest.to_date = backtest.evol_config.running_to
#    backtest.to_date = datetime.strptime('2013-02-18 23:58:00', '%Y-%m-%d %H:%M:%S')

    #itt feltételezzük, hogy az adatsorok megfelelőek
    for datarow in backtest.evol_config.datarows:
        backtest.strategy.datarows[datarow.symbol.encode()] = datarow
        backtest.timeframe = datarow.timeframe_num

    instruments = [symbol for symbol in sorted(backtest.strategy.datarows.keys())]
    backtest.strategy.add_instruments(instruments)

    backtest.strategy.configMM(backtest.evol_config.mmplugin_id, {
        'parameters' : ast.literal_eval(backtest.evol_config.mmplugin_parameters),
        'equity' : 10000,
        'leverage' : 30
    })
    backtest.strategy.slippage = 0
    backtest.strategy.failed_trades = 0

#TODO: middle
def doDaWholeThing(stratIds = 0, dns = None, returnResult = None, backtestConfId = 0, loadLiveConfigId = 0):
    db.init()
    session = db.Session()

    tmp = {'perf': False, 'serial': False, 'orderlog': False, 'tradelog': False}
    if returnResult:
        tmp.update(returnResult)
    returnResult = tmp
    result = {}
    if returnResult['perf']:
        result['perf'] = {}
    if returnResult['serial']:
        result['serial'] = {}
    if returnResult['orderlog']:
        result['orderlog'] = {}
    if returnResult['tradelog']:
        result['tradelog'] = {}

    if stratIds:
        if type(stratIds) != list and type(stratIds) != tuple:
            stratIds = [stratIds]
        dns = []
        for dbStrat in session.query(db.Strategy).filter(db.Strategy.id.in_(stratIds)).all():
            dns.append(dbStrat.dns)
    elif dns:
        if type(dns) != list and type(dns) != tuple:
            dns = [dns]
    else:
        dns = ["[[0, [0, [], [], [1, 13, 26, 40, 56, 64, 76, 85, 97, 109, 120, 131, 143, 150, 421, 438, 453, 466, 480, 497]]], [497, [199, [19, 1.0], [0], [498]]], [498, [214, [u'GBPUSD'], [497], [499]]], [499, [803, [1, 0.4491644900000572, 16], [498], [500]]], [500, [303, [], [499], [501, 506]]], [506, [1019, [64], [500], [507]]], [507, [1052, [163, 112, 4], [506], [508]]], [508, [1077, [90, 0.57844676727926969], [507], [509]]], [509, [710, [u'GBPUSD', 0, 6], [508], []]], [501, [1080, [173], [500], [502]]], [502, [1014, [118], [501], [503, 505]]], [505, [709, [u'GBPUSD', 1, 8], [502], []]], [503, [1077, [88, 0.59341974473476744], [502], [504]]], [504, [710, [u'GBPUSD', 0, 6], [503], []]], [480, [199, [18, 1.0], [0], [481]]], [481, [214, [u'USDCAD'], [480], [482]]], [482, [803, [-1, 0.32144876469250816, 9], [481], [483]]], [483, [305, [], [482], [484, 489]]], [489, [1069, [197, 0.29435299604505966], [483], [490]]], [490, [1012, [49, 1.6323540914274937, 0.42017578460660338, 1], [489], [491]]], [491, [1042, [143], [490], [492]]], [492, [1026, [175], [491], [493]]], [493, [1074, [107], [492], [494]]], [494, [1039, [188], [493], [495]]], [495, [1013, [192, 0.11788749054239525, 0.63959289201372471, 0], [494], [496]]], [496, [708, [u'USDCAD', 1, 16], [495], []]], [484, [1026, [178], [483], [485]]], [485, [1074, [107], [484], [486]]], [486, [1039, [188], [485], [487]]], [487, [1013, [193, 0.1555519451055869, 0.66655045188205853, 0], [486], [488]]], [488, [708, [u'USDCAD', 1, 16], [487], []]], [466, [199, [17, 1.0], [0], [467]]], [467, [214, [u'USDCAD'], [466], [468]]], [468, [803, [1, 0.49716581360720896, 2], [467], [469]]], [469, [304, [], [468], [470, 474]]], [474, [1066, [68, 16, 77, 2], [469], [475]]], [475, [1005, [135, 133, 3], [474], [476]]], [476, [1018, [118], [475], [477, 479]]], [479, [711, [u'USDCAD', None, 12], [476], []]], [477, [1070, [76], [476], [478]]], [478, [709, [u'USDCAD', 1, 1], [477], []]], [470, [1012, [191, 0.36485501590425484, 0.42744545340837542, 6], [469], [471]]], [471, [1053, [95], [470], [472]]], [472, [1026, [61], [471], [473]]], [473, [709, [u'USDCAD', 1, 3], [472], []]], [453, [199, [16, 1.0], [0], [454]]], [454, [215, [u'USDCAD'], [453], [455]]], [455, [803, [1, 0.34166326068805197, 10], [454], [456]]], [456, [303, [], [455], [457, 461]]], [461, [1068, [178], [456], [462]]], [462, [1069, [138, 0.18317808324568152], [461], [463]]], [463, [1018, [34], [462], [464, 465]]], [465, [711, [u'USDCAD', None, 7], [463], []]], [464, [708, [u'USDCAD', 1, 9], [463], []]], [457, [1037, [91, 21], [456], [458]]], [458, [1057, [109], [457], [459]]], [459, [1023, [175], [458], [460]]], [460, [708, [u'USDCAD', 1, 3], [459], []]], [438, [199, [15, 1.0], [0], [439]]], [439, [214, [u'USDCAD'], [438], [440]]], [440, [803, [1, 0.18054438285585689, 4], [439], [441]]], [441, [302, [], [440], [442, 446]]], [446, [1014, [197], [441], [447, 450]]], [450, [1056, [89], [446], [451]]], [451, [1072, [27], [450], [452]]], [452, [708, [u'USDCAD', 1, 3], [451], []]], [447, [1037, [149, 174], [446], [448]]], [448, [1026, [27], [447], [449]]], [449, [709, [u'USDCAD', 1, 11], [448], []]], [442, [1022, [146], [441], [443]]], [443, [1068, [16], [442], [444]]], [444, [1022, [151], [443], [445]]], [445, [711, [u'USDCAD', None, 0], [444], []]], [421, [199, [14, 1.0], [0], [422]]], [422, [211, [u'GBPJPY'], [421], [423]]], [423, [803, [1, 0.57777969837272913, 1], [422], [424]]], [424, [303, [], [423], [425, 434]]], [434, [1038, [61, 4], [424], [435]]], [435, [1057, [85], [434], [436]]], [436, [1013, [178, 2.269306409615738, 1.7341768470451864, 0], [435], [437]]], [437, [709, [u'GBPJPY', 1, 17], [436], []]], [425, [1069, [171, 0.99527302038976373], [424], [426]]], [426, [1027, [6, 143], [425], [427]]], [427, [1021, [136], [426], [428]]], [428, [1061, [194, 0.77764149122638149], [427], [429]]], [429, [1077, [5, 1.3264928751041942], [428], [430]]], [430, [1072, [102], [429], [431]]], [431, [1013, [161, 0.31574537554445647, 2.6176532225416382, 7], [430], [432]]], [432, [1022, [160], [431], [433]]], [433, [708, [u'GBPJPY', 1, 16], [432], []]], [150, [199, [13, 1.0], [0], [151]]], [151, [215, [u'AUDJPY'], [150], [152]]], [152, [803, [-1, 0.59067748560691469, 0], [151], [153]]], [153, [501, [], [152], [154, 195, 210]]], [210, [501, [], [153], [211, 228, 409]]], [409, [304, [], [210], [410, 420]]], [420, [708, [u'AUDJPY', 1, 12], [409], []]], [410, [1066, [51, 2, 95, 2], [409], [411]]], [411, [601, [], [410], [412, 414, 416, 418]]], [418, [1060, [146], [411], [419]]], [419, [710, [u'AUDJPY', 1, 5], [418], []]], [416, [1080, [95], [411], [417]]], [417, [710, [u'AUDJPY', 0, 8], [416], []]], [414, [1080, [22], [411], [415]]], [415, [403, [764.14085756768941], [414], []]], [412, [1019, [53], [411], [413]]], [413, [708, [u'AUDJPY', 0, 17], [412], []]], [228, [501, [], [210], [229, 243, 397]]], [397, [304, [], [228], [398, 408]]], [408, [708, [u'AUDJPY', 1, 12], [397], []]], [398, [1066, [50, 2, 95, 1], [397], [399]]], [399, [601, [], [398], [400, 402, 404, 406]]], [406, [1060, [142], [399], [407]]], [407, [710, [u'AUDJPY', 1, 5], [406], []]], [404, [1080, [97], [399], [405]]], [405, [710, [u'AUDJPY', 0, 8], [404], []]], [402, [1080, [19], [399], [403]]], [403, [403, [766.63604967105096], [402], []]], [400, [1019, [53], [399], [401]]], [401, [708, [u'AUDJPY', 0, 17], [400], []]], [243, [501, [], [228], [244, 259, 270]]], [270, [501, [], [243], [271, 302, 319]]], [319, [501, [], [270], [320, 337, 353]]], [353, [501, [], [319], [354, 370, 382]]], [382, [504, [], [353], [383]]], [383, [504, [], [382], [384]]], [384, [304, [], [383], [385, 393]]], [393, [1069, [56, 0.72567718753454613], [384], [394]]], [394, [1024, [109], [393], [395]]], [395, [1024, [24], [394], [396]]], [396, [709, [u'AUDJPY', 0, 8], [395], []]], [385, [601, [], [384], [386, 389, 390]]], [390, [1005, [127, 200, 2], [385], [391]]], [391, [1037, [107, 87], [390], [392]]], [392, [707, [u'AUDJPY', 0, 0], [391], []]], [389, [403, [-713.24312872396342], [385], []]], [386, [1077, [150, 1.278593987277566], [385], [387]]], [387, [1060, [164], [386], [388]]], [388, [711, [u'AUDJPY', None, 10], [387], []]], [370, [304, [], [353], [371, 381]]], [381, [708, [u'AUDJPY', 1, 12], [370], []]], [371, [1066, [51, 2, 93, 1], [370], [372]]], [372, [601, [], [371], [373, 375, 377, 379]]], [379, [1060, [146], [372], [380]]], [380, [710, [u'AUDJPY', 1, 5], [379], []]], [377, [1080, [94], [372], [378]]], [378, [710, [u'AUDJPY', 0, 8], [377], []]], [375, [1080, [20], [372], [376]]], [376, [403, [784.97707412681177], [375], []]], [373, [1019, [51], [372], [374]]], [374, [708, [u'AUDJPY', 0, 17], [373], []]], [354, [504, [], [353], [355]]], [355, [304, [], [354], [356, 366]]], [366, [1069, [65, 0.71595360884860215], [355], [367]]], [367, [1024, [102], [366], [368]]], [368, [1024, [19], [367], [369]]], [369, [709, [u'AUDJPY', 0, 8], [368], []]], [356, [601, [], [355], [357, 359, 364]]], [364, [1005, [124, 198, 1], [356], [365]]], [365, [708, [u'AUDJPY', 1, 20], [364], []]], [359, [1019, [7], [356], [360]]], [360, [601, [], [359], [361, 362, 363]]], [363, [403, [-968.88285026552398], [360], []]], [362, [711, [u'AUDJPY', None, 10], [360], []]], [361, [708, [u'AUDJPY', 1, 12], [360], []]], [357, [1069, [44, 0.43474988162106099], [356], [358]]], [358, [402, [82], [357], []]], [337, [304, [], [319], [338, 340]]], [340, [1044, [190], [337], [341]]], [341, [1018, [75], [340], [342, 351]]], [351, [1073, [132], [341], [352]]], [352, [711, [u'AUDJPY', None, 17], [351], []]], [342, [604, [], [341], [343, 344, 345, 350]]], [350, [709, [u'AUDJPY', 0, 3], [342], []]], [345, [604, [], [342], [346, 347, 348, 349]]], [349, [709, [u'AUDJPY', 0, 3], [345], []]], [348, [708, [u'AUDJPY', 0, 4], [345], []]], [347, [711, [u'AUDJPY', None, 10], [345], []]], [346, [711, [u'AUDJPY', None, 20], [345], []]], [344, [402, [-286], [342], []]], [343, [711, [u'AUDJPY', None, 6], [342], []]], [338, [1039, [170], [337], [339]]], [339, [708, [u'AUDJPY', 0, 8], [338], []]], [320, [304, [], [319], [321, 333]]], [333, [1069, [62, 0.71196354557813479], [320], [334]]], [334, [1024, [107], [333], [335]]], [335, [1024, [31], [334], [336]]], [336, [709, [u'AUDJPY', 0, 8], [335], []]], [321, [601, [], [320], [322, 325, 328]]], [328, [1005, [128, 193, 2], [321], [329]]], [329, [1037, [113, 89], [328], [330]]], [330, [1054, [101], [329], [331]]], [331, [1005, [40, 133, 3], [330], [332]]], [332, [708, [u'AUDJPY', 1, 4], [331], []]], [325, [1019, [7], [321], [326]]], [326, [1037, [114, 90], [325], [327]]], [327, [711, [u'AUDJPY', None, 2], [326], []]], [322, [1077, [157, 1.2375178503901723], [321], [323]]], [323, [1060, [181], [322], [324]]], [324, [711, [u'AUDJPY', None, 10], [323], []]], [302, [304, [], [270], [303, 315]]], [315, [1069, [66, 0.66691143185900803], [302], [316]]], [316, [1024, [101], [315], [317]]], [317, [1024, [24], [316], [318]]], [318, [709, [u'AUDJPY', 0, 8], [317], []]], [303, [601, [], [302], [304, 306, 313]]], [313, [1005, [128, 197, 4], [303], [314]]], [314, [708, [u'AUDJPY', 1, 20], [313], []]], [306, [1019, [5], [303], [307]]], [307, [601, [], [306], [308, 309, 310]]], [310, [1024, [104], [307], [311]]], [311, [1024, [33], [310], [312]]], [312, [709, [u'AUDJPY', 0, 8], [311], []]], [309, [711, [u'AUDJPY', None, 10], [307], []]], [308, [708, [u'AUDJPY', 1, 12], [307], []]], [304, [1069, [42, 0.41522763506989252], [303], [305]]], [305, [402, [61], [304], []]], [271, [304, [], [270], [272, 298]]], [298, [1069, [64, 0.70633697312716182], [271], [299]]], [299, [1024, [102], [298], [300]]], [300, [1024, [32], [299], [301]]], [301, [709, [u'AUDJPY', 0, 8], [300], []]], [272, [601, [], [271], [273, 290, 295]]], [295, [1005, [127, 191, 1], [272], [296]]], [296, [1037, [106, 86], [295], [297]]], [297, [711, [u'AUDJPY', None, 2], [296], []]], [290, [1019, [2], [272], [291]]], [291, [601, [], [290], [292, 293, 294]]], [294, [403, [-963.84188130925668], [291], []]], [293, [711, [u'AUDJPY', None, 10], [291], []]], [292, [708, [u'AUDJPY', 1, 12], [291], []]], [273, [1069, [45, 0.38688067788321212], [272], [274]]], [274, [1054, [111], [273], [275]]], [275, [1005, [38, 140, 2], [274], [276]]], [276, [601, [], [275], [277, 281, 287]]], [287, [1005, [129, 200, 4], [276], [288]]], [288, [1037, [100, 83], [287], [289]]], [289, [711, [u'AUDJPY', None, 2], [288], []]], [281, [1019, [12], [276], [282]]], [282, [601, [], [281], [283, 285, 286]]], [286, [403, [-998.91947338001512], [282], []]], [285, [711, [u'AUDJPY', None, 10], [282], []]], [283, [1068, [161], [282], [284]]], [284, [710, [u'AUDJPY', 1, 5], [283], []]], [277, [1069, [39, 0.41611718489131816], [276], [278]]], [278, [1054, [99], [277], [279]]], [279, [1005, [38, 130, 3], [278], [280]]], [280, [708, [u'AUDJPY', 1, 4], [279], []]], [259, [304, [], [243], [260, 269]]], [269, [403, [-923.55385041157047], [259], []]], [260, [1066, [47, 3, 91, 1], [259], [261]]], [261, [601, [], [260], [262, 264, 265, 267]]], [267, [1060, [143], [261], [268]]], [268, [711, [u'AUDJPY', None, 18], [267], []]], [265, [1080, [96], [261], [266]]], [266, [710, [u'AUDJPY', 0, 8], [265], []]], [264, [709, [u'AUDJPY', 0, 3], [261], []]], [262, [1019, [57], [261], [263]]], [263, [708, [u'AUDJPY', 0, 17], [262], []]], [244, [304, [], [243], [245, 255]]], [255, [1069, [67, 0.65999368671584613], [244], [256]]], [256, [1024, [94], [255], [257]]], [257, [1024, [20], [256], [258]]], [258, [709, [u'AUDJPY', 0, 8], [257], []]], [245, [601, [], [244], [246, 248, 253]]], [253, [1005, [135, 197, 3], [245], [254]]], [254, [708, [u'AUDJPY', 1, 20], [253], []]], [248, [1019, [2], [245], [249]]], [249, [601, [], [248], [250, 251, 252]]], [252, [403, [-998.42552717410592], [249], []]], [251, [711, [u'AUDJPY', None, 10], [249], []]], [250, [402, [66], [249], []]], [246, [1069, [42, 0.41086300007521354], [245], [247]]], [247, [402, [95], [246], []]], [229, [304, [], [228], [230, 239]]], [239, [1069, [59, 0.70607346002168603], [229], [240]]], [240, [1024, [105], [239], [241]]], [241, [1024, [29], [240], [242]]], [242, [709, [u'AUDJPY', 0, 8], [241], []]], [230, [601, [], [229], [231, 234, 237]]], [237, [1060, [181], [230], [238]]], [238, [711, [u'AUDJPY', None, 10], [237], []]], [234, [1019, [10], [230], [235]]], [235, [1037, [116, 91], [234], [236]]], [236, [711, [u'AUDJPY', None, 2], [235], []]], [231, [1077, [154, 1.2769427861406297], [230], [232]]], [232, [1060, [181], [231], [233]]], [233, [711, [u'AUDJPY', None, 10], [232], []]], [211, [504, [], [210], [212]]], [212, [304, [], [211], [213, 224]]], [224, [1069, [65, 0.66922581406074078], [212], [225]]], [225, [1024, [106], [224], [226]]], [226, [1024, [20], [225], [227]]], [227, [709, [u'AUDJPY', 0, 8], [226], []]], [213, [601, [], [212], [214, 216, 222]]], [222, [1005, [125, 198, 5], [213], [223]]], [223, [708, [u'AUDJPY', 1, 20], [222], []]], [216, [1019, [5], [213], [217]]], [217, [601, [], [216], [218, 220, 221]]], [221, [403, [-1000.0], [217], []]], [220, [711, [u'AUDJPY', None, 10], [217], []]], [218, [1024, [22], [217], [219]]], [219, [709, [u'AUDJPY', 0, 8], [218], []]], [214, [1069, [44, 0.42476885636286649], [213], [215]]], [215, [402, [35], [214], []]], [195, [304, [], [153], [196, 206]]], [206, [1069, [61, 0.68960514600105938], [195], [207]]], [207, [1024, [99], [206], [208]]], [208, [1024, [25], [207], [209]]], [209, [709, [u'AUDJPY', 0, 8], [208], []]], [196, [601, [], [195], [197, 199, 204]]], [204, [1005, [130, 195, 4], [196], [205]]], [205, [708, [u'AUDJPY', 1, 20], [204], []]], [199, [1019, [2], [196], [200]]], [200, [601, [], [199], [201, 202, 203]]], [203, [403, [-968.79050977375221], [200], []]], [202, [711, [u'AUDJPY', None, 10], [200], []]], [201, [708, [u'AUDJPY', 1, 12], [200], []]], [197, [1069, [46, 0.40348694192920453], [196], [198]]], [198, [402, [57], [197], []]], [154, [501, [], [153], [155, 170, 176]]], [176, [504, [], [154], [177]]], [177, [304, [], [176], [178, 191]]], [191, [1069, [61, 0.67742224115326466], [177], [192]]], [192, [1024, [104], [191], [193]]], [193, [1024, [32], [192], [194]]], [194, [709, [u'AUDJPY', 0, 8], [193], []]], [178, [601, [], [177], [179, 183, 188]]], [188, [1005, [126, 198, 2], [178], [189]]], [189, [1037, [108, 84], [188], [190]]], [190, [711, [u'AUDJPY', None, 2], [189], []]], [183, [1019, [3], [178], [184]]], [184, [601, [], [183], [185, 186, 187]]], [187, [403, [-990.86124921794635], [184], []]], [186, [711, [u'AUDJPY', None, 10], [184], []]], [185, [708, [u'AUDJPY', 1, 12], [184], []]], [179, [1069, [44, 0.41169714441985117], [178], [180]]], [180, [1054, [105], [179], [181]]], [181, [1005, [38, 133, 2], [180], [182]]], [182, [708, [u'AUDJPY', 1, 4], [181], []]], [170, [304, [], [154], [171, 175]]], [175, [403, [-907.71782328282711], [170], []]], [171, [1069, [67, 0.69303350787975759], [170], [172]]], [172, [1024, [98], [171], [173]]], [173, [1024, [21], [172], [174]]], [174, [709, [u'AUDJPY', 0, 8], [173], []]], [155, [504, [], [154], [156]]], [156, [304, [], [155], [157, 167]]], [167, [1060, [169], [156], [168]]], [168, [1060, [150], [167], [169]]], [169, [710, [u'AUDJPY', 1, 5], [168], []]], [157, [601, [], [156], [158, 160, 165]]], [165, [1005, [135, 200, 3], [157], [166]]], [166, [708, [u'AUDJPY', 1, 20], [165], []]], [160, [1019, [4], [157], [161]]], [161, [601, [], [160], [162, 163, 164]]], [164, [403, [-988.4723312716294], [161], []]], [163, [711, [u'AUDJPY', None, 10], [161], []]], [162, [708, [u'AUDJPY', 1, 12], [161], []]], [158, [1069, [45, 0.38083483770458731], [157], [159]]], [159, [402, [78], [158], []]], [143, [199, [12, 1.0], [0], [144]]], [144, [214, [u'GBPUSD'], [143], [145]]], [145, [803, [1, 0.41896016504855127, 1], [144], [146]]], [146, [304, [], [145], [147, 149]]], [149, [710, [u'GBPUSD', 1, 2], [146], []]], [147, [1066, [126, 3, 35, 1], [146], [148]]], [148, [709, [u'GBPUSD', 1, 16], [147], []]], [131, [199, [11, 1.0], [0], [132]]], [132, [215, [u'EURUSD'], [131], [133]]], [133, [803, [-1, 0.3193901039674249, 0], [132], [134]]], [134, [303, [], [133], [135, 139]]], [139, [1068, [134], [134], [140]]], [140, [1066, [165, 94, 50, 0], [139], [141]]], [141, [1054, [97], [140], [142]]], [142, [708, [u'EURUSD', 1, 17], [141], []]], [135, [1039, [136], [134], [136]]], [136, [1039, [88], [135], [137]]], [137, [1052, [172, 80, 4], [136], [138]]], [138, [708, [u'EURUSD', 1, 17], [137], []]], [120, [199, [10, 1.0], [0], [121]]], [121, [215, [u'EURUSD'], [120], [122]]], [122, [803, [1, 0.92626159092425397, 8], [121], [123]]], [123, [304, [], [122], [124, 127]]], [127, [1066, [97, 93, 88, 0], [123], [128]]], [128, [1069, [158, 0.73324360113878562], [127], [129]]], [129, [1067, [195, 63, 9, 2], [128], [130]]], [130, [710, [u'EURUSD', 0, 12], [129], []]], [124, [1066, [31, 32, 69, 0], [123], [125]]], [125, [1068, [174], [124], [126]]], [126, [707, [u'EURUSD', 0, 14], [125], []]], [109, [199, [9, 1.0], [0], [110]]], [110, [215, [u'EURUSD'], [109], [111]]], [111, [803, [-1, 0.7627414233505232, 8], [110], [112]]], [112, [303, [], [111], [113, 117]]], [117, [1068, [134], [112], [118]]], [118, [1066, [164, 95, 51, 0], [117], [119]]], [119, [711, [u'EURUSD', None, 8], [118], []]], [113, [1039, [144], [112], [114]]], [114, [1039, [82], [113], [115]]], [115, [1052, [171, 81, 4], [114], [116]]], [116, [708, [u'EURUSD', 1, 3], [115], []]], [97, [199, [8, 1.0], [0], [98]]], [98, [215, [u'EURUSD'], [97], [99]]], [99, [803, [-1, 0.78284917448085067, 8], [98], [100]]], [100, [303, [], [99], [101, 105]]], [105, [1053, [80], [100], [106]]], [106, [1069, [151, 0.71912829885976259], [105], [107]]], [107, [1067, [200, 64, 9, 2], [106], [108]]], [108, [710, [u'EURUSD', 0, 12], [107], []]], [101, [1023, [191], [100], [102]]], [102, [1025, [143], [101], [103]]], [103, [1017, [191], [102], [104]]], [104, [707, [u'EURUSD', 0, 14], [103], []]], [85, [199, [7, 1.0], [0], [86]]], [86, [211, [u'EURUSD'], [85], [87]]], [87, [803, [1, 0.5552549309774677, 15], [86], [88]]], [88, [304, [], [87], [89, 93]]], [93, [1074, [130], [88], [94]]], [94, [1068, [93], [93], [95]]], [95, [1053, [15], [94], [96]]], [96, [709, [u'EURUSD', 0, 14], [95], []]], [89, [1080, [23], [88], [90]]], [90, [1073, [195], [89], [91]]], [91, [1070, [138], [90], [92]]], [92, [711, [u'EURUSD', None, 8], [91], []]], [76, [199, [6, 1.0], [0], [77]]], [77, [214, [u'GBPJPY'], [76], [78]]], [78, [803, [1, 0.24631855074555137, 12], [77], [79]]], [79, [305, [], [78], [80, 82]]], [82, [1013, [178, 0.72120456214934636, 2.7474626047671609, 0], [79], [83]]], [83, [1042, [178], [82], [84]]], [84, [711, [u'GBPJPY', None, 9], [83], []]], [80, [1017, [22], [79], [81]]], [81, [709, [u'GBPJPY', 1, 19], [80], []]], [64, [199, [5, 1.0], [0], [65]]], [65, [211, [u'GBPJPY'], [64], [66]]], [66, [803, [1, 0.52621082287845589, 4], [65], [67]]], [67, [304, [], [66], [68, 70]]], [70, [1014, [198], [67], [71, 74]]], [74, [1072, [97], [70], [75]]], [75, [707, [u'GBPJPY', 1, 3], [74], []]], [71, [1014, [200], [70], [72, 73]]], [73, [707, [u'GBPJPY', 1, 14], [71], []]], [72, [708, [u'GBPJPY', 1, 13], [71], []]], [68, [1072, [97], [67], [69]]], [69, [710, [u'GBPJPY', 1, 4], [68], []]], [56, [199, [4, 1.0], [0], [57]]], [57, [211, [u'EURUSD'], [56], [58]]], [58, [803, [-1, 0.17046325042942961, 17], [57], [59]]], [59, [305, [], [58], [60, 62]]], [62, [1077, [24, 1.1537756845789695], [59], [63]]], [63, [708, [u'EURUSD', 0, 0], [62], []]], [60, [1055, [66], [59], [61]]], [61, [708, [u'EURUSD', 0, 19], [60], []]], [40, [199, [3, 1.0], [0], [41]]], [41, [211, [u'EURUSD'], [40], [42]]], [42, [803, [-1, 0.59767578294342438, 6], [41], [43]]], [43, [304, [], [42], [44, 49]]], [49, [1011, [113, 1.5005852755052216, 2.0812895858392011, 0], [43], [50]]], [50, [1011, [119, 1.5140350072804067, 1.9152848820045216, 0], [49], [51]]], [51, [1017, [49], [50], [52]]], [52, [1019, [22], [51], [53]]], [53, [1019, [36], [52], [54]]], [54, [1077, [90, 1.6362032311238925], [53], [55]]], [55, [711, [u'EURUSD', None, 20], [54], []]], [44, [1019, [35], [43], [45]]], [45, [1017, [46], [44], [46]]], [46, [1019, [38], [45], [47]]], [47, [1019, [31], [46], [48]]], [48, [711, [u'EURUSD', None, 20], [47], []]], [26, [199, [2, 1.0], [0], [27]]], [27, [215, [u'AUDJPY'], [26], [28]]], [28, [803, [-1, 0.84145229371890418, 12], [27], [29]]], [29, [305, [], [28], [30, 35]]], [35, [1057, [54], [29], [36]]], [36, [1053, [158], [35], [37]]], [37, [1013, [175, 0.88320125347300305, 2.744043154030825, 4], [36], [38]]], [38, [1023, [188], [37], [39]]], [39, [710, [u'AUDJPY', 1, 3], [38], []]], [30, [1068, [85], [29], [31]]], [31, [1073, [129], [30], [32]]], [32, [1013, [171, 0.95710618092358679, 2.7409179991035222, 4], [31], [33]]], [33, [1023, [190], [32], [34]]], [34, [707, [u'AUDJPY', 0, 1], [33], []]], [13, [199, [1, 1.0], [0], [14]]], [14, [215, [u'AUDJPY'], [13], [15]]], [15, [803, [1, 0.90635155397881728, 0], [14], [16]]], [16, [305, [], [15], [17, 22]]], [22, [1080, [63], [16], [23]]], [23, [1026, [178], [22], [24]]], [24, [1022, [61], [23], [25]]], [25, [711, [u'AUDJPY', None, 8], [24], []]], [17, [1077, [129, 1.4088260255494076], [16], [18]]], [18, [1027, [5, 26], [17], [19]]], [19, [1070, [22], [18], [20]]], [20, [1026, [197], [19], [21]]], [21, [710, [u'AUDJPY', 1, 19], [20], []]], [1, [199, [0, 1.0], [0], [2]]], [2, [211, [u'AUDJPY'], [1], [3]]], [3, [803, [-1, 0.91530040983349481, 18], [2], [4]]], [4, [303, [], [3], [5, 9]]], [9, [1013, [140, 1.8900194399672616, 1.0666649416595928, 2], [4], [10]]], [10, [1027, [3, 108], [9], [11]]], [11, [1039, [172], [10], [12]]], [12, [709, [u'AUDJPY', 1, 4], [11], []]], [5, [1039, [85], [4], [6]]], [6, [1017, [111], [5], [7]]], [7, [1052, [146, 28, 2], [6], [8]]], [8, [707, [u'AUDJPY', 0, 17], [7], []]]]"]

    genomes = []
    for i in dns:
        genomes.append(GTree.decode(i))

    backtest = Backtester()
    mmPluginParams = None
    mmPluginId = None

    if loadLiveConfigId:
        load_live(backtest, loadLiveConfigId)
        backtest.strategy.load_historic()

    elif stratIds:
        if backtestConfId:
            btc = session.query(db.BacktestConfig).get(backtestConfId)
        else:
            btc = session.query(db.BacktestConfig).filter(db.BacktestConfig.id==db.StrategyRun.configId).filter(db.StrategyRun.type==STRATEGYRUN_BACKTEST).filter(db.StrategyRun.strategyId==stratIds[0]).order_by(desc(db.BacktestConfig.id)).first()
            if not btc:
                for btc in session.query(db.BacktestConfig).all():
                    if str(stratIds[0]) in btc.strategies:
                        break
            print "backtestConfigId:", btc.id
        backtest.setStratPlugin(btc.stratpluginId, btc.accpluginId, btc.mmpluginId)
        for key, value in btc:
            try:
                if type(value) == str:
                    try:
                        value = datetime.strptime(value, "%Y-%m-%d %H:%M:%S")
                    except:
                        pass
                setattr(backtest, key, value)
            except Exception as e:
                print 'Exception in setattr(backtest, ' + key + ', ' + value + ')', e
        backtest.strategy.load_spread(backtest.spreadconfigId)
        backtest.strategy.load_rollover(backtest.rolloverconfigId)
        for datarow in btc.datarows:
            backtest.add_datarow(datarow.id)
        params = dict(btc)
        params.update(backtest.__dict__)
        updateMM(backtest, btc.mmpluginId, {'parameters': ast.literal_eval(btc.mmplugin_parameters), 'equity': btc.base_equity, 'leverage': btc.leverage}, {})
#        print 'backtest params:', params
        backtest.strategy.load_historic()
    else:
        loadDatarowId = 13
        stratPuginId = 1
        accPuginId = 1
        mmPluginId = 1
        backtest.setStratPlugin(stratPuginId, accPuginId, mmPluginId)
        if type(loadDatarowId) != tuple and type(loadDatarowId) != list:
            loadDatarowId = [loadDatarowId]
        for datarow in loadDatarowId:
            backtest.add_datarow(datarow)
#        backtest.from_date = "2011/01/01"
#        backtest.to_date = "2011/09/30"
    #    backtest.perf_from_date = "2010/03/01"
    #    backtest.perf_to_date = "2011/08/31"
#        mmPluginParams = {'equity': 100000,
#            'leverage' : 30,
#            'parameters': {
#                'positionsize' : 20000,
#                'positionmode' : 2,
#                'riskmanagement' : True,
#                'setsl' : 1,
#                'maxrisk' : 2,
#                'riskreward_rate' : 0.0,
#                'maxnumoftrades': 1,
#                'margincall_rate' : 50
#            }
#        }
        backtest.from_date = "2008/01/01"
        backtest.to_date = "2011/12/31"
        mmPluginParams = {'equity': 10000,
            'leverage' : 30,
            'parameters': {
                "positionsize":200,
                "positionmode":2,
                "sltype":3,
                "riskmanagement":1,
                "modifysl":0,
                "maxrisk":50,
                "setsl":1,
                "be_threshold":25,
                "riskreward_rate":2,
                "maxnumoftrades":1000,
                "margincall_rate":50
            }
        }
        backtest.strategy.load_spread(1)
        backtest.strategy.load_rollover(1)
        updateMM(backtest, mmPluginId, mmPluginParams, {})
        backtest.strategy.load_historic()

    backtest.use_orderlog(True)
    for index, genome in enumerate(genomes):
        evaluate(backtest, genome, result, returnResult, index, stratIds)

    return result

def comparePortfLogs(joinlist, logType = None, differentSource = False):
    returnResult = {}
    if logType:
        returnResult[logType] = 1
    joinedLog = doDaWholeThing(stratIds = joinlist, returnResult = returnResult, merge = True, differentSource = differentSource)
    if not logType:
        return
    joinedLog = joinedLog[logType]
    mergedLog = joinedLog.pop('merged')
    joinedLog = joinLog(joinedLog, joinlist, logType)
    compareLog(joinedLog, mergedLog, logType)
    dataToFile(logType + '_merged', mergedLog, wrap = True)
    if not logType:
        return

def comparePortfSerial(joinlist, serialType = None, differentSource = False):
    from math import sqrt
    if not serialType:
        serialType = 'monthly_profit'
    joinedSerial = doDaWholeThing(stratIds = joinlist, returnResult = {'serial': serialType}, merge = True, differentSource = differentSource)
    joinedSerial = joinedSerial['serial']
    mergedSerial = joinedSerial.pop('merged')
    mergedSerial = mergedSerial[serialType]
    limit = 0.0001
    joinedSum = []
    for strId in joinlist:
        if not len(joinedSum):
            joinedSum = joinedSerial[strId][serialType]
        else:
            for index, monthly in enumerate(joinedSerial[strId][serialType]):
                joinedSum[index] += monthly
    print joinedSum
    print len([i for i in joinedSum if i <= 0]), 'losing months in joined'
    print mergedSerial
    print len([i for i in mergedSerial if i <= 0]), 'losing months in merged'
    squareDiff = 0
    for index, monthly in enumerate(joinedSum):
        diff = monthly - mergedSerial[index]
        if abs(diff) > limit:
            print index, diff, mergedSerial[index], joinedSum[index]
        squareDiff += diff*diff
    print len(joinedSum), 'months'
    print 'square error', sqrt(squareDiff)

#compareSaved2()
#print "done"
#quit()

#TODO: bottom
#comparePortfLogs((149411, 150450, 162478, 229665, 229864, 229959, 236595, 237878, 261169, 274201, 321054, 323958, 358215, 382057, 417350, 423415, 424051, 486774, 493494, 98060), differentSource = 1, logType = 'orderlog')#
#print "done"
#quit()

#comparePortfSerial((149411, 150450, 162478, 229665, 229864, 229959, 236595, 237878, 261169, 274201, 321054, 323958, 358215, 382057, 417350, 423415, 424051, 486774, 493494, 98060), differentSource = 2)
#print "done"
#quit()

result = doDaWholeThing(stratIds = 1, loadLiveConfigId = 286, returnResult = {'perf': 1})
print result
print "done"
quit()

#import os, os.path, datetime
#import ast
#from gdtlive.constants import DIR_STR, ORDERTYPE_STR
#from gdtlive.utils import R, R2
#
#audjpy = [1, 2, 3]
#eurusd = [4, 5, 7, 8]
#gbpjpy = [11, 12, 13]
#usdcad = [14]
#gbpusd = [15, 16]
#allSymbol = audjpy + eurusd + gbpjpy + usdcad + gbpusd

#fp = open('/home/gdt/merged', 'r')
#dns = fp.read()
#fp.close()
#genome = GTree.decode(dns)
#print genome
#quit()
#
##db.init()
##backtest = Backtester()
##backtest.configure(2)
##backtest.use_orderlog(True)
#dnsPath = '/home/gdt/dns'
#merged = None
#dnsFiles = os.listdir(dnsPath)
##orderlogs = []
#group = 0
##sumNetProfit = 0
#for dnsFile in dnsFiles:
#    fp = open(os.path.join(dnsPath, str(dnsFile)), 'r')
#    dns = fp.read()
#    fp.close()
#    genome = GTree.decode(dns)
#    if not merged:
#        merged = genome
#    else:
#        merged.mergeWith(genome)
##    perf = backtest.evaluate(genome)
##    sumNetProfit += perf['net_profit']
##    orderlog = backtest.strategy.get_orderlog()
##    orderlog = backtest.strategy.get_tradelog()
##    orderlogs.append(decodeTradelog(orderlog, group))
#    group += 1
#print merged

#mergedOrderlog = []
#mergedPerf = backtest.evaluate(merged)
#print sumNetProfit, mergedPerf['net_profit']
#mergedOrderlog = decodeTradelog(backtest.strategy.get_orderlog())
#mergedOrderlog = decodeTradelog(backtest.strategy.get_tradelog())
#for i in mergedOrderlog:
#    print i
#quit()
#print 'len(orderlogs)', len(orderlogs), len(orderlogs[0]) + len(orderlogs[1]) + len(orderlogs[2])
#print 'len(mergedOrderlog)', len(mergedOrderlog)
#quit()
#
#doneCounter = 0
#for merged in mergedOrderlog:
#    seps = []
#    success = False
#    for sepList in orderlogs:
#        if not len(sepList):
#            continue
#        seps.append(sepList[0])
##        if merged == orderlogs[merged['group']][0]:
#        if merged == sepList[0]:
#            doneCounter += 1
##            del orderlogs[merged['group']][0]
#            del sepList[0]
#            success = True
#            break
#    if not success:
#        print merged
##        print seps
#        print orderlogs[0][0]
#        print orderlogs[1][0]
#        print orderlogs[2][0]
#        break
#
#print doneCounter

#print 'len(orderlogs)', len(orderlogs)
#print 'len(mergedOrderlog)', len(mergedOrderlog)
#    fp = open('/home/gdt/tradelogs/' + dnsFile, 'w')
#    fp.write(backtest.strategy.get_tradelog())
#    fp.close()

#def eval():
#    backtest.evaluate(genome)
#    backtest.strategy.get_serial_performance()
#
#def profile():
#    cProfile.run('eval()', 'backtest')
#    p = pstats.Stats('backtest')
#    p.strip_dirs()
#    p.sort_stats('time')
#    p.reverse_order()
#    p.print_stats()
#
#profile()
#quit()

#startTime = time.time()

#print "finished in", time.time() - startTime, "seconds"

#loadDatarowSymbols = 'AUDUSD'
#for i in xrange(10000):
#    genome = Individual().build_genome(loadDatarowSymbols)
#    genome.processNodes()
#    print i, genome.encode()
#    performance = backtest.evaluate(genome)
#    print performance['net_profit']
#    s = backtest.get_serial_performance()
#    print len(s['monthly_profit'])
#    print s['yearly_profit']

#quit()

#node = AD()
#node.create('EURUSD')
#print node, node.high, node.low, node.volume, node.close
#print node
#node2 = node.clone()
#print node2, node2.high, node2.low, node2.volume, node2.close
#
#quit()





#i = IBuySellComplex().build_genome('EURUSD')
#i.processNodes()
#print i

#quit()

#strategy = session.query(db.Strategy).get(1)
#perf = session.query(db.PerformanceData).filter(db.PerformanceData.id == db.StrategyRun.performanceIdCurrency).filter(db.StrategyRun.strategyId == strategy.id).first()
#serial = session.query(db.PerformanceStrategySerialData).filter(db.PerformanceStrategySerialData.id == db.StrategyRun.performanceSerialId).filter(db.StrategyRun.strategyId == strategy.id).first()
#print perf.net_profit, perf.min_yearly_profit_p
#print serial.monthly_profit
#print strategy
#print serial
#quit()

#backtest.configure(strategy.evolconfId)
#performance = backtest.evaluate(strategy.dns)
#s = backtest.get_serial_performance()
#print performance['net_profit'], performance['min_yearly_profit_p']
#print s['monthly_profit']
#quit()

#
#strategy = session.query(db.Strategy).get(175955)
#serial = session.query(db.PerformanceStrategySerialData).filter(db.PerformanceStrategySerialData.id==db.StrategyRun.performanceSerialId).filter(db.StrategyRun.strategyId==strategy.id).first()
#print serial.monthly_profit
##print strategy
##print serial
##quit()
#
#backtest.configure(strategy.evolconfId)
#performance = backtest.evaluate(strategy.dns)
#s = backtest.get_serial_performance()
#print s['monthly_profit']   
#quit()


#backtest.configure()

#backtest.use_orderlog(True)

#for i in xrange(10000):
#    genome = Individual().build_genome('EURGBP')
#    genome.processNodes()
#    performance = backtest.evaluate(genome)
#    dns = genome.encode()
#    genome2 = GTree.decode(dns)
#    dns2 = genome2.encode()
    #if dns != dns2:
    #    print 'FUCKEDUP DNS'
    #    print dns
    #    print dns2
#    performance2 = backtest.evaluate(genome2)
#    if performance != performance2:
#        print 'Fucked up performance:'
#        print dns
#        print dns2
#        print performance
#        print performance2
#    else:
#        print i
#
#
#quit()

#GENOMES = [Individual().build_genome('EURUSD') for i in xrange(1000)]
#print 'procssing...'
#for g in GENOMES:
#    g.processNodes()

#genome = Individual().build_genome('EURUSD')
#genome.processNodes()
#genome = GTree.decode("[[0, [0, None, [], [27, 23, 18, 1]]], [1, [202, ['EURUSD'], [0], [2]]], [2, [802, [1, 0.48231710154986596], [1], [3]]], [3, [504, [], [2], [4]]], [4, [304, [], [3], [14, 5]]], [5, [1014, [25], [4], [10, 6]]], [6, [1020, [162], [5], [9, 8, 7]]], [7, [704, ['EURUSD', 0], [6], []]], [8, [705, ['EURUSD', 0], [6], []]], [9, [706, ['EURUSD', 0], [6], []]], [10, [1078, [], [5], [13, 12, 11]]], [11, [704, ['EURUSD', 1], [10], []]], [12, [705, ['EURUSD', 1], [10], []]], [13, [706, ['EURUSD', 1], [10], []]], [14, [1050, [83], [4], [17, 16, 15]]], [15, [704, ['EURUSD', 0], [14], []]], [16, [705, ['EURUSD', 0], [14], []]], [17, [706, ['EURUSD', 0], [14], []]], [18, [205, ['EURUSD'], [0], [19]]], [19, [802, [-1, 0.84480906414752843], [18], [20]]], [20, [304, [], [19], [22, 21]]], [21, [402, [314], [20], []]], [22, [706, ['EURUSD', 1], [20], []]], [23, [206, ['EURUSD'], [0], [24]]], [24, [801, [1, 0.85116589719646818], [23], [25]]], [25, [911, [], [24], [26]]], [26, [701, ['EURUSD', None], [25], []]], [27, [202, ['EURUSD'], [0], [28]]], [28, [801, [1, 0.23007927757596597], [27], [29]]], [29, [502, [], [28], [48, 30]]], [30, [303, [], [29], [44, 31]]], [31, [602, [], [30], [41, 38, 32]]], [32, [1014, [9], [31], [37, 33]]], [33, [1079, [135], [32], [36, 35, 34]]], [34, [704, ['EURUSD', 1], [33], []]], [35, [705, ['EURUSD', 1], [33], []]], [36, [706, ['EURUSD', 1], [33], []]], [37, [705, ['EURUSD', 1], [32], []]], [38, [1042, [196], [31], [39]]], [39, [1017, [109], [38], [40]]], [40, [702, ['EURUSD', None], [39], []]], [41, [1059, [0.10600212633356851, 2.4201137985223919, 2.6622502729898057, 0.69341402588483037, 1.5856160954680767, 2.3536124354717871, 1.10253043579229, 2.6637148618854432], [31], [43, 42]]], [42, [705, ['EURUSD', 1], [41], []]], [43, [706, ['EURUSD', 1], [41], []]], [44, [1017, [28], [30], [45]]], [45, [1040, [], [44], [47, 46]]], [46, [705, ['EURUSD', 0], [45], []]], [47, [706, ['EURUSD', 0], [45], []]], [48, [910, [], [29], [49]]], [49, [701, ['EURUSD', None], [48], []]]]")
#print genome
#GENOMES = [genome]
#s = time.time()
#for genome in GENOMES:
#    performance = backtest.evaluate(genome)
    #orderlog = backtest.get_orderlog()
    #tradelog = backtest.get_tradelog()
    #print performance

#    if performance['min_floating_p'] < -100:
        #print performance['min_floating_p'], '%    ', performance['min_floating']
        #print genome.encode()
        #quit()
#        pass
#    else:
#        print '.'

#print "time : %.4f" % (time.time() - s)
#quit()
#serial = backtest.get_serial_performance()

#print 'cumulated profit:', performance['cumulated_profit']
#print 'net_profit : ', R(performance['net_profit']), ' $'
#print 'commission: ', performance['commission'],'$'
#print performance['chance1m'], '%'
#print performance['chance3m'], '%'
#print performance['min_floating_p'], '%'
#print performance['max_drawdown_p'], '%'
#print serial['monthly_profit']
#print performance
#quit()

#print performance['min_floating']
#print performance['avg_pos_floating_p'], '%'
#print performance['avg_neg_floating_p'], '%'
#print performance['avg_pos_floating']
#print performance['avg_neg_floating']
#print
#print performance['avg_margin_usage']
#print performance['avg_margin_usage_p'], '%'
#print performance['max_margin_usage']
#print performance['max_margin_usage_p'], '%'
#print performance['sharpe_ratio']
#
#print serial['yearly_profit']
#print serial['margin_usage']

#for order in ast.literal_eval(orderlog):
#    print order
#quit()

#for fl in serial['floating_profit']:
#    print fl 




#print tradelog

#quit()
#print performance['percent']
#quit()


#backtest.add_datarow(2)
#backtest.from_date ="2007/10/01"
#backtest.to_date = "2009/09/30"
#backtest.load_historic()
#backtest.use_orderlog(True)
#backtest.account.instrument_value('EURUSD', 0.0001)

#print "%s, %.10f" % (backtest.candles['EURCHF'][PRICE_TIME][0], backtest.candles['EURCHF'][PRICE_PIPVALUE][0])
#for i in range(len(backtest.candles['EURCHF'][PRICE_PIPVALUE])):
#    print "%s, %.10f" % (backtest.candles['EURCHF'][PRICE_TIME][i], backtest.candles['EURCHF'][PRICE_PIPVALUE][i])
#
#import ast
#

#genome = GTree.decode("[[0, [0, None, [], [14, 9, 5, 1]]], [1, [205, [u'USDCAD'], [0], [2]]], [2, [801, [-1, 0.32784064578135141], [1], [3]]], [3, [910, [], [2], [4]]], [4, [701, [u'USDCAD', None], [3], []]], [5, [206, [u'USDCAD'], [0], [6]]], [6, [801, [1, 0.33905093923547491], [5], [7]]], [7, [909, [], [6], [8]]], [8, [701, [u'USDCAD', None], [7], []]], [9, [205, [u'USDCAD'], [0], [10]]], [10, [801, [-1, 0.44141719729550666], [9], [11]]], [11, [305, [], [10], [13, 12]]], [12, [705, [u'USDCAD', 0], [11], []]], [13, [402, [-650], [11], []]], [14, [202, [u'USDCAD'], [0], [15]]], [15, [802, [-1, 0.93270454182394269], [14], [16]]], [16, [911, [], [15], [17]]], [17, [701, [u'USDCAD', None], [16], []]]]")
#genome = Individual().build_genome('USDCAD')
#genome.processNodes()
#performance = backtest.evaluate(genome)
#serial = backtest.get_serial_performance()
#print performance
#print serial
#
#print serial['trades_profit_loss']
#
#quit()

#
#print 'net profit', performance['currency']['net_profit']
#print 'avg profit/month', performance['currency']['profit_per_month']
#print 'open trades', performance['currency']['open_trades']
#print 'floating profit', performance['currency']['floating_profit']
#print 'variance', performance['currency']['profit_variance']
#print 'sharpe', performance['currency']['sharpe_ratio']
#print serial['MAE']
#print serial['MFE']
#print serial['drawdown']
#print serial['daily_profit']
#print serial['weekly_profit']
#print serial['monthly_profit']
#print serial['yearly_profit']
#print serial['net_profit']
#print performance['pips_ratio']
#
#quit()

#def save_genome(genome):
#    f = open('genome', 'wt+')
#    if f:
#        #f.write(genome)
#        cPickle.dump(genome, f, -1)
#        f.close()
#    else:
#        print 'file open error'

#print 'creating...'
#GENOMES = [Individual().build_genome('EURUSD') for i in xrange(10)]
#print 'saving...'
#save_genome(GENOMES)
#GENOMES = load_genome()
#print 'procssing...'
#sum = 0.0
#for g in GENOMES:
#    g.processNodes()
#    sum += len(g.encode()) 
#sum /= 1000.0

#print 'average dns length:', sum


#times = [];
#for c in range(10):
#print 'evaluating..'
#s = time.time();
#sum = 0
#for genome in GENOMES:
    #genome = GTree.decode("[[0, [0, None, [], [9, 1]]], [1, [205, ['EURUSD'], [0], [2]]], [2, [801, [1, 0.45662617259893401], [1], [3]]], [3, [302, [], [2], [6, 4]]], [4, [903, [], [3], [5]]], [5, [701, ['EURUSD', None], [4], []]], [6, [1068, [51], [3], [7]]], [7, [904, [], [6], [8]]], [8, [701, ['EURUSD', None], [7], []]], [9, [202, ['EURUSD'], [0], [10]]], [10, [805, [-1, 0.11414321147902243], [9], [85, 74, 53, 11]]], [11, [805, [-1, 0.46133777976152968], [10], [50, 37, 12]]], [12, [801, [1, 0.67269756058394536], [11], [13]]], [13, [503, [], [12], [30, 23, 14]]], [14, [305, [], [13], [19, 15]]], [15, [1071, [], [14], [18, 17, 16]]], [16, [704, ['EURUSD', 0], [15], []]], [17, [705, ['EURUSD', 0], [15], []]], [18, [706, ['EURUSD', 0], [15], []]], [19, [1016, [48], [14], [22, 21, 20]]], [20, [704, ['EURUSD', 1], [19], []]], [21, [705, ['EURUSD', 1], [19], []]], [22, [706, ['EURUSD', 1], [19], []]], [23, [303, [], [13], [28, 24]]], [24, [1009, [118], [23], [27, 26, 25]]], [25, [704, ['EURUSD', 0], [24], []]], [26, [705, ['EURUSD', 0], [24], []]], [27, [706, ['EURUSD', 0], [24], []]], [28, [1052, [102, 192, 3], [23], [29]]], [29, [704, ['EURUSD', 1], [28], []]], [30, [304, [], [13], [35, 31]]], [31, [1063, [27, 157, 4, 59, 7], [30], [34, 33, 32]]], [32, [704, ['EURUSD', 1], [31], []]], [33, [705, ['EURUSD', 1], [31], []]], [34, [706, ['EURUSD', 1], [31], []]], [35, [1070, [183], [30], [36]]], [36, [705, ['EURUSD', 1], [35], []]], [37, [801, [1, 0.11154371420095965], [11], [38]]], [38, [501, [], [37], [45, 39]]], [39, [305, [], [38], [41, 40]]], [40, [402, [-789], [39], []]], [41, [1071, [], [39], [44, 43, 42]]], [42, [704, ['EURUSD', 1], [41], []]], [43, [705, ['EURUSD', 1], [41], []]], [44, [706, ['EURUSD', 1], [41], []]], [45, [305, [], [38], [48, 46]]], [46, [1023, [48], [45], [47]]], [47, [703, ['EURUSD', 0], [46], []]], [48, [1055, [55], [45], [49]]], [49, [706, ['EURUSD', 0], [48], []]], [50, [802, [-1, 0.19467690236160839], [11], [51]]], [51, [909, [], [50], [52]]], [52, [701, ['EURUSD', None], [51], []]], [53, [805, [1, 0.69173287023199648], [10], [71, 66, 54]]], [54, [802, [-1, 0.74238759537513865], [53], [55]]], [55, [305, [], [54], [63, 56]]], [56, [1014, [105], [55], [59, 57]]], [57, [1044, [118], [56], [58]]], [58, [703, ['EURUSD', 1], [57], []]], [59, [1050, [59], [56], [62, 61, 60]]], [60, [704, ['EURUSD', 0], [59], []]], [61, [705, ['EURUSD', 0], [59], []]], [62, [706, ['EURUSD', 0], [59], []]], [63, [1005, [23, 131, 3], [55], [64]]], [64, [1019, [177], [63], [65]]], [65, [705, ['EURUSD', 0], [64], []]], [66, [802, [-1, 0.90306972785519957], [53], [67]]], [67, [305, [], [66], [70, 68]]], [68, [1073, [140], [67], [69]]], [69, [703, ['EURUSD', 1], [68], []]], [70, [402, [1], [67], []]], [71, [802, [1, 0.93700088517302949], [53], [72]]], [72, [907, [], [71], [73]]], [73, [701, ['EURUSD', None], [72], []]], [74, [801, [-1, 0.96739955810002731], [10], [75]]], [75, [303, [], [74], [80, 76]]], [76, [1062, [83, 116, 5, 86, 6], [75], [79, 78, 77]]], [77, [704, ['EURUSD', 0], [76], []]], [78, [705, ['EURUSD', 0], [76], []]], [79, [706, ['EURUSD', 0], [76], []]], [80, [1056, [16], [75], [81]]], [81, [1065, [65, 63, 4], [80], [84, 83, 82]]], [82, [704, ['EURUSD', 1], [81], []]], [83, [705, ['EURUSD', 1], [81], []]], [84, [706, ['EURUSD', 1], [81], []]], [85, [802, [1, 0.3739900178501091], [10], [86]]], [86, [907, [], [85], [87]]], [87, [701, ['EURUSD', None], [86], []]]]") 
#print genome.encode()
#    backtest.use_orderlog(True)
#print 'first eval'
#    print genome.encode()
#    performance = backtest.evaluate(genome)
#    serial = backtest.get_serial_performance()
#    print performance['percent']
#    print serial['monthly_profit']
#    print serial['yearly_profit']
#    print
    #print performance['currency']['open_trades'], performance['currency']['floating_profit'], performance['currency']['profit_variance'] 
    #serial = backtest.get_serial_performance()
    #sum += len(str(performance))
#    fucked = False 
#    for r in serial['drawdown']:
#        if r > 0:
#            print r
#            fucked = True
#    if fucked:  
#        print genome.encode()
#        print serial['drawdown']
#        die()

#quit()


#sum /= 1000

#print 'average result length: ', sum
    #quit()

    #print '.'    
#print performance['currency']['number_of_trades']
#serial = backtest.get_serial_performance()
#print serial['net_profit'][:10]
    #if i % 100 == 0: print '.'
        #else: print '.', #performance['currency']['net_profit']
    #    if performance['currency']['net_profit'] > 1000000:        
    #        print backtest.get_tradelog()            
    #        out = ast.literal_eval(backtest.get_orderlog())
    #        for log in out:
    #            print log
    #        print performance
    #        quit()
    #    #print 'max:', 
#    times.append(time.time()-s)

#print '%.5f' % (time.time() - s)
#    
#print 'Average : ', sum(times) / 100.0;
#print 'OK'
#quit()


#    print i,
#quit()
#    #if i % 10 == 0:
#     #   pass
#        #print    
#    result = backtest.get_serial_performance()
#    tradelog = backtest.get_tradelog()

#            
#    try:
#        ast.literal_eval(tradelog)        
#    except:
#        print 'Python tradelog:'.center(100,'-')
#        print tradelog
#        print traceback.format_exc()
#    
#    print result['mae']
#    print result['mfe']
#    print result['cumulated_profit']
#    print result['drawdown']
#    print result['trades_profit_loss']
#    print result['daily_profit']
#    print result['yearly_profit']
#    print result['monthly_profit']
#    print result['weekly_profit']
#quit()

#print backtest
#genome = Individual().build_genome('EURUSD')
#genome.processNodes()
#print genome.encode()
#print genome
#genome = genome.encode()
#print genome2
#quit()
#quit()
#genome = IBuyClose().build_genome('EURUSD')
#genome = IBuySell().build_genome('EURUSD')
#
#selector = NodeSelector()
#selector.add(*All.nodes)
#selector.addLevel(3, OnOffSignal, OnSignal, OffSignal)
#selector.addLevel(5, *Comparison.nodes)
#selector.addLevel(7, *Numeric.nodes)
#selector.addLevel(7, *Price.nodes)

#genome = GTree(simpleGenomeBuilder('EURUSD', selector))

#def load_genome():
#    #f = open('genome','rt')
#    #encoded = f.read()
#    #print encoded
#    #f.close()
#    #return encoded
#    return cPickle.load(open('genome'))
#
#genome = load_genome()

#def zip(string):
#    buffer = cStringIO.StringIO()
#    z = gzip.GzipFile('genome', 'wb', 6, buffer)
#    z.write(string)
#    z.close()
#    return len(buffer.getvalue())

#print zip(genome)
#print GTree.decode(genome)
#
#e1 = genome.encode()
#
#g2 = genome.clone()
#g2.processNodes()
#e2 = g2.encode()
#
#print e1 == e2
#quit()

#encoded = genome.encode()
#print len(encoded)
#print zip(encoded)

#genome2 = GTree.decode(genome.encode())

#print genome2
#quit()
#print 'starting....'

#def eval():
#    backtest.evaluate(genome)
#
#def profile():
#    cProfile.run('eval()', 'backtest')
#    p = pstats.Stats('backtest')
#    p.strip_dirs()
#    p.sort_stats('cum')
#    p.reverse_order()
#    p.print_stats()
#
#s = time.time()
#profile()
#while True:
#    genome = IBuyClose().build_genome('EURUSD')
#eval()
#    perf = backtest.account.get_performance()
#    if perf['currency']['sharpe_ratio'] > 1:
#        break
#    print '.',
#s = time.time() - s
#print backtest.account.show()
#orderlog = backtest.get_orderlog()
#orderlog = ast.literal_eval(orderlog)
#for order in orderlog:
#    print order


#print genome
#print backtest.strategy.account.show();
#print backtest.strategy.get_performance()
#serial = backtest.get_serial_performance()
#for mfe in serial['MAE']:
#    print mfe, 
#print serial['mae']
#print serial['mfe']
#print 'backtest finished in %.5f' % s
#print backtest.account

#save_genome(genome.encode())
#backtest.strategy.destroy()




