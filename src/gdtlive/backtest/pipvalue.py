# -*- encoding: utf-8 -*- 
'''

Az egyes keresztárfolyamok pip értékének kiszámítását végző osztályok és funkciók

Created on Mar 25, 2011

@author: kulcsarb
'''
__docformat__ = 'restructuredtext en'

from os.path import dirname
from datetime import datetime, timedelta, date 
from gdtlive.store.db import PipValue
from sqlalchemy.sql.expression import desc
import urllib, os, logging, zipfile, traceback
import gdtlive.store.db as db
#from gdtlive.constants import TEMP_DIRECTORY

log = logging.getLogger('gdtlive.backtest.pipvalue')

class FxHistoricalDataDownloader(object):
    '''Árfolyamadatokat tölt le, és importál az adatbázisba a www.fxhistoricaldata.com-ról
    
    Használata:
        >>> p = FxHistoricalDataDownloader()
        >>> p.update('USDCHF')
        True
        
    '''
    
    log = logging.getLogger('gdtlive.FxHistoricalDataDownloader')
  
    def update(self, symbol):
        '''Frissiti az adott instrumentumhoz tartozó napi árfolyamadatokat
        
        - Ellenőrzi, hogy szükséges-e az adatok frissitése
        - Letölti az adatokat
        - Beolvassa az adatokat az adatbázisba
        
        :Parameters:
            symbol : str
                az árfolyam megnevezése
        
        :return: Ha a művelet sikerült, True, ha hiba lépett fel a folyamat során akkor False
        :rtype: bool
        '''
                        
        if self._is_db_current(symbol): return True
                
        if not self._download(symbol):  return False
        
        if not self._load_content():    return False
        
        if not self._save_content():    return False
        
        return True
    
    def _is_db_current(self, symbol):
        '''Ellenőrzi, hogy a megadott instrumentum adatai naprakészek-e vagy sem
        
        :Parameters:
            symbol : str
                az instrumentum neve
        
        :return: Sikeres volt-e a művelet, vagy sem
        :rtype: bool
        '''
        session = None
        result = False
        try:
            session = db.Session()                        
            self.count = session.query(PipValue).filter(PipValue.symbol == symbol).count()
            if self.count:                
                last = session.query(PipValue).order_by(desc(PipValue.date)).filter(PipValue.symbol == symbol).first()
                self.last_date = last.date
                result = True if last.date == date.today() else False                
                self.log.info('last entry in db : %s' % (last.date))
                self.log.info('update needed : %s' % ('No' if result else 'Yes'))                
            else:
                self.last_date = None            
                self.log.info('no data in db for %s' % symbol)                                         
        except:
            self.log.error(traceback.format_exc())            
        finally:
            if session:
                session.close()
        return result
            
    def _download(self, symbol):        
        """Letölti az instrumentum összes elérhető napi árfolyamadatát.
        
        :Parameters:           
            symbol : str
                az instrumentum neve
        
        :return: Sikeres volt-e a művelet, vagy sem 
        :rtype: bool
        """                
        result = False        
        try:       
            self.filename = None   
            target_file = dirname(__file__) + os.sep + '%s-daily.zip' % symbol
            url = 'http://www.fxhistoricaldata.com/download/%s?t=day' % (symbol.upper())  
            self.log.info('downloading %s -> %s' % (url, target_file))
            urllib.urlretrieve(url, target_file)                
            if os.path.exists(target_file) and zipfile.is_zipfile(target_file):
                self.filename = target_file
                self.log.info('download OK.')                        
                result = True
            else:
                self.log.error('downloaded file doesnt exists, or isnt zip archive!')
                try:
                    if os.path.exists(target_file):        
                        os.remove(target_file)                        
                except OSError, e:
                    self.log.error(traceback.format_exc())           
        except :            
            self.log.error(traceback.format_exc())     
        return result
                
    def _load_content(self):
        '''Beolvassa egy előzőleg letöltött file tartalmát
        
        A filet a `_download` metódus tölti le. Sikeres beolvasás esetén törli a filet a lemezről.
        
        :return: Sikeres volt-e a művelet, vagy sem 
        :rtype: bool
        '''
        try:
            if zipfile.is_zipfile(self.filename):
                zip = zipfile.ZipFile(self.filename, 'r')
                f = zip.open(zip.namelist()[0],'r')
                self.content = f.read()
                zip.close()  
                self.log.info('content OK')              
                try:        
                    os.remove(self.filename)
                except OSError, e:
                    self.log.error(traceback.format_exc())           
                return True                                        
        except:
            self.log.error(traceback.format_exc())            
        return False

    def _save_content(self):   
        '''Elmenti az adatbázisba a beolvasott árfolyamadatokat.
        
        A metódus az előzőleg a _load_content segitségével beolvasott adatokat irja ki adatbázisba. 
        Az adatbázisba csak azokat az adatokat viszi fel, amelyek a már benne lévőknél újabbak.        
        
        :return: Sikeres volt-e a művelet, vagy sem 
        :rtype: bool
        '''                 
        value_table = PipValue.__table__
        insert_data = []        
        for line in self.content.split('\n')[1:]:
            try:
                line = line.strip()
                if line:
                    symbol, date_str, time_str, open, low, high, close = line.split(',')                
                    open = float(open)
                    close = float(close)
                    date_str = datetime.strptime(date_str, '%Y%m%d').date()
                    median = abs(close - open) + open                    
                    if (not self.last_date) or (self.last_date and date_str > self.last_date):
                        insert_data.append({'symbol':symbol, 'date': date_str, 'price':median}) 
            except:                
                self.log.error(traceback.format_exc())

        self.log.info('We have %d new price data' % (len(insert_data)))                                 
        try:                    
            if len(insert_data) :                
                db.engine.execute(value_table.insert(), insert_data)
                self.log.info('New data saved succesfully')                
        except:
            self.log.error(traceback.format_exc()) 
            return False
        return True
        

class PriceValue:
    '''
    Az osztály feladata, hogy a backtest réteg számára könnyen kezelhetővé tegye a pip érték számitás alapját képező árfolyamadatokat. 
    
    '''
    def __init__(self, symbol, from_date, to_date):
        '''Betölti a megadott instrumentumhoz tartozó adatokat 
        
        :Parameters:
            symbol : str
                az instrumentum neve
            from_date : date
                kezdő időpont
            to_date : date
                végidőpont
        '''                        
        session = db.Session()
        query = session.query(PipValue).order_by(PipValue.date).filter(PipValue.symbol == symbol).filter(PipValue.date >= from_date).filter(PipValue.date <= to_date)
        self.price = {}
        for value in query:
            self.price[value.date] = value.price
        session.close()
        
        self.price_keys = sorted(self.price.keys())
        
    def __len__(self):
        '''Visszaadja az adatsor hosszát'''
        return len(self.price)
    
    def __getitem__(self, key):
        '''
        Tömb indexelés, mely a kipótolja a hiányzó adatokat a meglévőek alapján. 
        
        Célja, hogy a `Backtester` számára mindig szolgáltasson valamilyen adatot, 
        mégha csak közelítőt is.  
        '''
        if len(self.price) == 0:    return 0
        
                
        if key in self.price:       return self.price[key]
        if key < self.price_keys[0]:     return self.price[self.price_keys[0]]
        if key > self.price_keys[-1]:    return self.price[self.price_keys[-1]]
        
        delta = timedelta(days=1)
        while key >= self.price_keys[0]:
            if key in self.price:   return self.price[key]
            key -= delta            
        return self.price[self.price_keys[0]]


pipvalue_plugins = [FxHistoricalDataDownloader]

def update(symbol):
    '''
    A funkció végigmegy az elérhető adatletöltő plugineken, és addig próbálkozik 
    az adatok frissitésével, amíg valamelyik pluginnek nem sikerül.
    
    :Parameters:
        symbol : str
            az instrumentum neve
            
    :return: sikerült-e a frissités vagy nem
    :rtype: bool
    '''
    try:
        for cls in pipvalue_plugins:
            plugin = cls()
            if plugin.update(symbol):   return True
    except:
        log.error(traceback.format_exc())            
    return False

def has_symbol(symbol):
    '''Lekérdezi, hogy az adott instrumentum szerepel-e az adatbázisban
    
    :rtype: bool
    '''
    session = None
    result = False
    try:
        session = db.Session()
        result = session.query(PipValue).filter(PipValue.symbol == symbol).count() > 0
    except:
        log.error(traceback.format_exc())        
    finally:
        if session: session.close()
    return result


def get_price(symbol, from_date, to_date):    
    '''Visszaad egy inicializált `PriceValue` objektumot
    
    :Parameters:
        symbol : str
            az instrumentum neve
        from_date : date
            kezdő időpont
        to_date : date
            végidőpont
    '''
    return PriceValue(symbol, from_date, to_date)


if __name__ == '__main__':
    import gdtlive.admin.system.log
    gdtlive.admin.system.log.LOG_FORMAT = "%(levelname)s - %(message)s"
    gdtlive.admin.system.log.initialize_loggers()   
     
    db.init()    
    p = FxHistoricalDataDownloader()
    p.update('USDCHF')
