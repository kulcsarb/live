'''
Created on 2013.02.22.

@author: gdt
'''

from gdtlive.admin.system.log import initialize_loggers
from gdtlive.backtest.backtester import Backtester, StratPlugins
from gdtlive.constants import STRATEGYRUN_BACKTEST, PERFORMANCE_CURRENCY
import gdtlive.store.db as db
from ast import literal_eval
from datetime import date, datetime

db.init()
initialize_loggers()

stratPuginId = 1
accPuginId = 1    

session = db.Session()
strategy = session.query(db.Strategy).get(1)

backtest = Backtester()    
backtest.setStratPlugin(stratPuginId, accPuginId, strategy.mmplugin_id)
datarow = session.query(db.DatarowDescriptor).filter(db.DatarowDescriptor.symbol == strategy.symbol).filter(db.DatarowDescriptor.timeframe_num == strategy.timeframe).first()
print datarow.to_date
backtest.add_datarow(datarow.id)

backtest.from_date = datetime(2013,2,1)
backtest.to_date = datetime(2013,2,7)
mmplugin_params = {'equity' : 10000,
                   'leverage' : 30,
                   'parameters' : literal_eval(strategy.mmplugin_parameters)
                   }
backtest.strategy.configMM(strategy.mmplugin_id, mmplugin_params)
backtest.use_orderlog(True)
backtest.strategy.load_spread(1)
backtest.strategy.load_rollover(1)
backtest.strategy.load_historic()
print 1
performance = backtest.evaluate(strategy.dns)        
serial = backtest.get_serial_performance()
tradelog = backtest.get_tradelog()
orderlog = backtest.get_orderlog()        
print performance