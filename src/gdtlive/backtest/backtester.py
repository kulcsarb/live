# -*- encoding: utf-8 -*- 
'''
Az evolúció által létrehozott stratégiák leteszteléséért felelős modul.

Created on 2010.12.11.

@author: kulcsarb

'''
__docformat__ = 'restructuredtext en'
import gdtlive.c.strategies as strategies
from gdtlive.evol.gnodes.GenomeBase import GTree
from gdtlive.admin.system.log import extra
from gdtlive.constants import TIMEFRAME, PRICES, PRICE_TIME
import gdtlive.store.db as db
import logging
import traceback
import ast
import sys
import gdtlive.c.talib as talib

log = logging.getLogger('gdtlive.Backtester')
errorlog = logging.getLogger('errors.Backtest')

talib.Initialize()

StratPlugins = {1: strategies.Strategy1, 4: strategies.Strategy4}

class Backtester(object):
    '''
    A stratégiák leteszteléséért felelős osztály.
    :author: kulcsarb
    '''

    def __init__(self, Session = None):
        '''A backtester inicializálása.

        Az inicializálás folyamán alapszinű konfiguráció is történik, 
        igy az objektum az adatsorok betöltése után már használható is.

        :Parameters:
            Session : SqlAlchemy Session object
                Az aktuálisan használni kívánt Session objektum. Amennyiben
                nincs megadva, akkor a `gdt.store.db.Session`-t használja.
        '''
        self.timeframe_num = 0
        self.fitness = None
        """:type: dict
        A devizák kamatlábainak tárolására szolgáló változó
        """
        self.return_logs = False
        try:
            if Session:
                self.session = Session()
            else:
                self.session = db.Session()
        except:
            raise Exception('Database connection not initialized! Call gdt.store.db.init() first, or supply a valid Session object in the constructor!')

    def __del__(self):
        del self.strategy

#---------------------------------------------------------------------
#                Konfigurációs metódusok 
#---------------------------------------------------------------------


    def configure(self, config_id):
        '''Inicializálja a megadott stratégia osztályt a kiértékelési folyamatokhoz'''
        self.load_evol(config_id)
        self.strategy.load_historic()

    def loadMMFromEvol(self):
        return {
            'parameters' : ast.literal_eval(self.evol_config.mmplugin_parameters),
            'equity' : self.evol_config.base_equity,
            'leverage' : self.evol_config.leverage
        }

    def load_evol(self, id):
        '''Betölti adatbázisból a megadott evolúciós konfigurációt, és bekonfigurálja önmagát
        :Parameters:
            id : int
                A használni kívánt EvolConfig objektum azonosítója
        '''
        evol_conf = self.session.query(db.EvolConfig).get(id)
        if not evol_conf:
            raise OSError('Evol config %d not found!' % id)
        self.evol_config = evol_conf

        self.strategy = StratPlugins[self.evol_config.stratpluginId](self.session, self.evol_config.accpluginId, self.evol_config.mmpluginId, 'gdt')
        self.config_fitnessplugin()
        self.strategy.load_spread(self.evol_config.spreadconfigId)
        self.strategy.load_rollover(self.evol_config.rolloverconfigId)

        self.from_date = self.evol_config.from_date
        self.to_date = self.evol_config.to_date

        #itt feltételezzük, hogy az adatsorok megfelelőek
        for datarow in self.evol_config.datarows:
            self.strategy.datarows[datarow.symbol.encode()] = datarow
            self.timeframe = datarow.timeframe_num

        instruments = [symbol for symbol in sorted(self.strategy.datarows.keys())]
        self.strategy.add_instruments(instruments)

        self.strategy.configMM(self.evol_config.mmpluginId, self.loadMMFromEvol())
        self.strategy.slippage = self.evol_config.slippage
        self.strategy.failed_trades = self.evol_config.failed_trades

    def config_fitnessplugin(self):
        try:
            from gdt.evol.fitness import fitness_plugins
            config = ast.literal_eval(self.evol_config.fitnessplugin_parameters)
            self.fitness = fitness_plugins[self.evol_config.fitnesspluginId]()
            self.fitness.configure(config)
        except :
            log.error('Error occured while configuring fitness plugin')
            log.error(traceback.format_exc())
            return False
        return True

    def setStratPlugin(self, stratpluginId, accpluginId, mmPuginId):
        self.strategy = StratPlugins[stratpluginId](self.session, accpluginId, mmPuginId, 'gdt')

#----------------------------------------------------------------
#    A kiértékelő metódus
#----------------------------------------------------------------    


    def evaluate(self, genome):
        '''
        Az adott genomot kiértékelteti, majd visszaadja a kötési naplót és a teljesítmény adatokat.

        Feladatai:
         - felkészíti a stratégiát a kiértékelésre
         - lefuttatja a tesztelési ciklust:
         - lekéri a stratégia kötési naplóját és performancia adatait
         - ezeket visszaadja a hivó félnek

        :Parameters:
             genome : `GTree`
                a kiértékelendő genome
        
        :rtype: (str, str, dict)
        :return: orderlog, tradelog, és perfomancia adatok 
        '''
        try:
#            if (isinstance(genome, GTree)):
#                genome = genome.encode()

            #log.info('evaluating genome: %s' % (str(genome)))
            self.strategy.evaluate(genome)
        except Exception as e:
            log.error('Cant evaluate %s! ' % genome)
            errorlog.error('Error occured during evaluation %s' % genome, extra = extra('backtest', globals = globals(), locals = locals()), exc_info = sys.exc_info())
            raise e

        performance = self.strategy.get_performance()

        if self.fitness:
            try:
                if self.fitness.need_serial_data:
                    serial = self.strategy.get_serial_performance()
                    fitness = self.fitness.calculate(performance, serial)
                else:
                    fitness = self.fitness.calculate(performance)
            except:
                log.error(traceback.format_exc())
                #fitness = 0
        else:            
            fitness = 0

        #performance['point']['fitness'] = fitness
        performance['fitness'] = fitness

        return performance

#-----------------------------------------------------------------
#        Segédfüggvények
#-----------------------------------------------------------------


    def add_datarow(self, datarow_id):
        '''A megadott adatsort felveszi a startégia által elérhető adatsorok közé
                
        :Parameters:
            id : int
                A DatarowDescriptor objektum azonosítója
        '''

        datarow = self.session.query(db.DatarowDescriptor).get(datarow_id)
        if not datarow:
            raise Exception('Datarow %d not found!' % datarow_id)

        if not self.timeframe_num:
            self.timeframe = datarow.timeframe_num

        if self.timeframe_num != datarow.timeframe_num:
            raise Exception('Incompatible timeframe! (%s %s != BacktestController\'s %s' % (datarow.symbol, TIMEFRAME[datarow.timeframe_num], self.timeframe))

        if self.strategy.available_from and self.to_date <= datarow.from_date:
            raise Exception('Datarow %d starts after our current to_date (%s < %s)! Datarow not loaded.' % (datarow.id, self.to_date, datarow.from_date))

        if self.strategy.available_to and self.from_date >= datarow.to_date:
            raise Exception('Datarow %d ends before our current from_date (%s > %s)! Datarow not loaded.' % (datarow.id, self.from_date, datarow.to_date))

        self.strategy.datarows[datarow.symbol.encode()] = datarow
        self.strategy.add_instrument(datarow.symbol.encode())
        #print datarow.symbol

        for datarow in self.strategy.datarows.itervalues():
            if not self.strategy.available_from or datarow.from_date > self.strategy.available_from:
                self.strategy.available_from = datarow.from_date

            if not self.strategy.available_to or datarow.to_date < self.strategy.available_to:
                self.strategy.available_to = datarow.to_date

    @property
    def timeframe(self):
        '''Az aktuálisan használt időtáv lekérése'''
        return TIMEFRAME.get(self.timeframe_num, 'None')


    @timeframe.setter
    def timeframe(self, value):
        '''Az aktuálisan használni kívánt időtáv beállítása'''
        if type(value) == str:
            for tf_num, tf_str in TIMEFRAME.iteritems():
                if tf_str == value:
                    self.timeframe_num = tf_num
                    self.strategy.timeframe = self.timeframe_num

        elif type(value) == int and value in TIMEFRAME:
            self.timeframe_num = value
            self.strategy.timeframe = self.timeframe_num


    @property
    def from_date(self):
        return self.strategy.from_date


    @from_date.setter
    def from_date(self, value):
        self.strategy.from_date = value


    @property
    def perf_from_date(self):
        return self.strategy.perf_from_date


    @perf_from_date.setter
    def perf_from_date(self, value):
        self.strategy.perf_from_date = value


    @property
    def perf_to_date(self):
        return self.strategy.perf_to_date


    @perf_to_date.setter
    def perf_to_date(self, value):
        self.strategy.perf_to_date = value


    @property
    def to_date(self):
        return self.strategy.to_date

    @to_date.setter
    def to_date(self, value):
        self.strategy.to_date = value

    @property
    def candle_num(self):
        if len(self.strategy.candles) == 0:
            return 0
        symbol = self.strategy.candles.__iter__().next()
        return len(self.strategy.candles[symbol][PRICE_TIME])

    def use_orderlog(self, value):
        self.strategy.use_orderlog(value)

#    def get_serial_performance(self):
#        return self.strategy.get_serial_performance()

    def get_orderlog(self):
        return self.strategy.get_orderlog()

    def get_tradelog(self):
        return self.strategy.get_tradelog()

    def get_serial_performance(self):
        return self.strategy.get_serial_performance()

    def __repr__(self):
        s = 'Timeframe : %s\n' % TIMEFRAME.get(self.timeframe_num, 'Not set')
        s += 'Max usable time period: %s - %s\n' % (self.strategy.available_from, self.strategy.available_to)
        s += 'Backtesting interval: %s - %s\n' % (self.from_date, self.to_date)
        s += 'Datarows'.center(30, '-') + '\n'
        for d in self.strategy.datarows.itervalues():
            s += '%d. %s%s %s, %s - %s\n' % (d.id, d.symbol, TIMEFRAME[d.timeframe_num], PRICES[d.price_type], d.from_date.date(), d.to_date.date())
        s += 'Spread config'.center(30, '-') + '\n'
        s += '%r\n' % self.strategy.spread_config
        #s += 'Account'.center(30,'-') + '\n'
        #s += '%r\n' % self.account                
        return s
