# -*- encoding: utf-8 -*- 
'''
OK Konfigurációs változókat tároló modul.

A modul feladata a szoftver eltérő területein használt beállítások, változók, konstansok
egységes tárolása, valamint a szoftver inditásakor azok értékeinek automatikus betöltése
a konfigurációs fileból. 

@status: Ellenőrzive, rendben.
@author: kulcsarb
'''
from os.path import dirname, sep
#import re
import sys
from ConfigParser import ConfigParser
global configured


configured = False
config_files = [dirname(__file__) + sep + 'etc' + sep + 'gdtlive.conf', '/etc/gdtlive/gdtlive.conf'] 

RESOURCES_ROOT = dirname(__file__)+sep+'resources'+sep


def load_config(files):    
    parser = ConfigParser()
    parser.read(files)    

    for section in parser.sections():
        #print '[', section,']'
        for option in parser.options(section):
            key = option.upper()
            if key in ('MAX_DOWNLOAD_RETRIES', 'BROWSE_MAX_TABS', 'EVOL_STOP_AFTER', 'EVOL_MAX_STRATEGY_PER_RUN', 'EXPORT_STRATEGY_MAX', 
                       'MAX_LOCAL_WORKERS', 'MAX_EVOL_WORKERS', 'MOVING_SHARP_PERIOD', 'BETA_PERIOD', 'POSTLOAD_DAYS', 'PRELOAD_DAYS', 'HOLIDAY_LIMIT_HOURS', 'MAXIMUM_SHIFT',
                       'TRADESERVER_PORT', 'FEED_PORT', 'DUMP_LENGTH','WEEKEND_STARTDAY','WEEKEND_STARTHOUR','WEEKEND_ENDDAY','WEEKEND_ENDHOUR',
                       'MAXIMUM_OPEN_SLIPPAGE', 'CONDITIONALORDER_SLIPPAGE'):
                sys.modules['gdtlive.config'].__dict__[key] = parser.getint(section, option)

            elif key in ('EVOL_FILTER_MAXDD', 'EVOL_FILTER_YEARLY_PROFIT','EVOL_FILTER_FLOATING_PROFIT', 'EVOL_FILTER_SHARPE', 'EVOL_STOP_PERCENT'):
                sys.modules['gdtlive.config'].__dict__[key] = parser.getfloat(section, option)

            elif key in ('JFOREX_JARS'):
                sys.modules['gdtlive.config'].__dict__[key] = parser.get(section, option).split(',')

            else:
                value = parser.get(section, option)
                if value.upper() in ['TRUE','FALSE']:
                    value = parser.getboolean(section, option)

                sys.modules['gdtlive.config'].__dict__[key] = value

            #print key, sys.modules['gdtlive.config'].__dict__[key]

if not configured:
    load_config(config_files)
    configured = True
