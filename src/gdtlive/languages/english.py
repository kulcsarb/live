# -*- encoding: utf-8 -*- 
'''
Created on Jan 14, 2011

@author: kulcsarb
'''

OK = 'OK'
RENEW_EMAIL_SENT_SUCCESSFULLY = 'Renew send successfully'
EMAIL_SENDING_FAILED = 'Email sending failed!'
DIFFERRING_PASSWORDS = 'Passwords are different!'
WRONG_PASSWORD = 'Wrong password!'
WEAK_PASSWORD = 'Password is not strong enough!'
MISSING_DATA = 'Missing data: '
NOT_FOUND = 'Not found: '
EXCEPTION_OCCURED = 'Unexpected exception occured during run. Please consult the system log!'
GUI_REFRESH_NEEDED = 'The element does not exists (probably has possibly been deleted). Please refresh the Browser window'
RENEW_TIME_EXPIRED = 'Renew time expired!'
FORGOT_PASSW_EMAIL_TEMPLATE = 'To renew your password please click to : ' 
GDTLIVE_SYS_MSG = 'GDTLIVE system message'
GDTLIVE_SYS_LOG = 'GDTLIVE system log'
WHILE_SENDING_EMAIL_TO_RECIPIENT = ' while sending email to recipient: '
FOR_THIS_ID = ' for this id!'
WAS_SAVED_SUCCESFULLY = ' was saved succesfully!'
WAS_DELETED_SUCCESFULLY = ' probably was deleted succesfully!'
INVALID_DATE_FORMAT = 'A gui által adott dátumformátum nem értelmezhető'
NAME_CONFLICT = 'Name conflict error, already existing name!'
TYPE_CONFLICT = 'Type conflict error, the given types are not identical!'
EVOL_CONFIG_IN_WORKFLOW_NOT_DELETED = "This Evol Config is used in a Workflow, it cannot be deleted!"
EVOL_CONFIG_NOT_FOUND = "Evol config not found for this strategy."
SPREAD_IN_EVOL_CONFIG = "This Spread Config cannot be deleted because it is used by an Evol Config."
ROLLOVER_IN_EVOL_CONFIG = "This ROLLOVER Config cannot be deleted because it is used by an Evol Config."
CANNOT_DELETE_OWN_ACCOUNT = "The current account cannot be deleted!"