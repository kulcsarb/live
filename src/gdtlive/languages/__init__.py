import sys

DEFAULT_LANGUAGE = 'english'

def set_default_language(language):
    lang_name = 'gdtlive.languages.'+language
    def_name = 'gdtlive.languages.default'
    try:
        __import__(lang_name)
        lang_module = sys.modules[lang_name]
        constants = [k for k in lang_module.__dict__.keys() if not k.startswith('__')]
        __import__(def_name)
        default_module = sys.modules[def_name]        
        for k in constants:                        
            default_module.__dict__[k] = lang_module.__dict__[k]
        
    except:
        pass
    
set_default_language(DEFAULT_LANGUAGE)