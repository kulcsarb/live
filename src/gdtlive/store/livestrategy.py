# -*- encoding: utf-8 -*- 
'''
Éles futási stratégiákkal kapcsolatos web service-ek

Created on 2012.07.12.

@author: vendelin8
@newfield url: Web service, Web services
'''
from sqlalchemy import desc, asc, func
import gdtlive.store.db as db
from gdtlive.store.db import Strategy, EntityName, PortfolioStrategyRun, PortfolioRun, PortfolioStrategy, LiveRunConfig, StrategyRun, PerformanceData, Account, BacktestConfig
#import gdtlive.store.ormObj as ormObj
from gdtlive.constants import ELEMENT_TYPE_STRATEGY, TIMEFRAME, STRATEGYRUN_LIVE, STRATEGYRUN_BACKTEST#, HISTORY_ACTION_IMPORTED, HISTORY_ACTION_VIEWED, STRATEGYRUN_STR
from gdtlive.control.router import paging_wrapper#, ajax_wrapper, store_wrapper
import cherrypy, traceback
from gdtlive.store.strategy import getSymbols, getNumofInstruments
#from sqlalchemy.sql.expression import or_
from gdtlive.utils import datetime2str, date2str
from gdtlive.languages.default import OK, EXCEPTION_OCCURED
from gdtlive.admin.system.log import errorlog, extra

@cherrypy.expose
@paging_wrapper
def activegridlist(start, limit, sort, dir, id = 0):
    '''
    A GUI számára visszaadja az elmentett stratégiák rendezett listáját 

    @url: /livestrategy/activegridlist

    @type  id: int
    @param id: broker account id, szűkíti a keresést. Ha 0, akkor az összes stratégia között keres.
    @type  start:  int
    @param start: az első visszaadandó elem sorszáma
    @type  limit:  int
    @param limit: megadja hogy maximum hány elemet tartalmazzon a válasz
    @type  sort:  int
    @param sort: rendezési mező (ált. db object attribútum név) 
    @type  dir:  string
    @param dir: rendezés iránya (ASC, DESC)

    @rtype: (bool, str, list, int) tuple
    @return: sikeresség, szöveges üzenet, az adat-szótárak listája, és a stratégiák teljes száma

    @rtype: list
    @return:  listája  
    '''
    session = db.Session()

    sort = 'strategys.' + sort
    if dir == "DESC":
        direction = desc
    else:
        direction = asc


    currPfrun = session.query(PortfolioRun.id).filter(PortfolioRun.portfolio_id == id).order_by(desc(PortfolioRun.id)).first()

    if not currPfrun:
        return gridlist(start, limit, sort, dir, id)

    res = []
    resDict = {}
    resIds = []
    try:
        qry = session.query(Strategy.id, Strategy.timeframe, Strategy.symbol, Strategy.individual)
        qry2 = session.query(func.count(Strategy.id))

        filters = [Strategy.id == PortfolioStrategy.strategy_id, PortfolioStrategy.portfolio_id == id]

        for fltr in filters:
            qry = qry.filter(fltr)
            qry2 = qry2.filter(fltr)

        for r in qry.order_by(direction(sort).nullslast()).slice(start, start + limit):
            resItem = {}
            itemId = r[0]
            resItem['id'] = itemId
            resItem['timeframe_str'] = TIMEFRAME[r[1]]
            resItem['instruments'] = r[2]
            resItem['number_of_instruments'] = r[2].count(',') + 1
            resItem['individual'] = r[3]
            res.append(resItem)
            resDict[itemId] = resItem
            resIds.append(itemId)

        if len(res):
            for r in session.query(EntityName.element_id, EntityName.element_name).filter(EntityName.element_id.in_(resIds)).filter(EntityName.type==ELEMENT_TYPE_STRATEGY).all():
                resDict[r[0]]['name'] = r[1]
    
            psrunMax = session.query(PortfolioStrategyRun.strategy_id.label('strategy_id'), func.max(PortfolioStrategyRun.id).label('id')).filter(PortfolioStrategyRun.portfoliorun_id == currPfrun[0]).filter(PortfolioStrategyRun.strategy_id.in_(resIds)).group_by(PortfolioStrategyRun.strategy_id).subquery('psrunMax')
            for r in session.query(PortfolioStrategyRun.strategy_id, PortfolioStrategyRun.state, PortfolioStrategyRun.running_from, PortfolioStrategyRun.running_to).filter(PortfolioStrategyRun.id == psrunMax.c.id).all():
                resDict[r[0]]['state'] = r[1]
                resDict[r[0]]['running_from'] = datetime2str(r[2])
                resDict[r[0]]['running_to'] = datetime2str(r[3])                
    
            for r in session.query(psrunMax.c.strategy_id, PerformanceData.winning_trades, PerformanceData.losing_trades, PerformanceData.open_trades, PerformanceData.net_profit, PerformanceData.floating_profit, PerformanceData.exposure).filter(PerformanceData.id == StrategyRun.performanceIdCurrency).filter(StrategyRun.type == STRATEGYRUN_LIVE).filter(StrategyRun.configId == LiveRunConfig.id).filter(LiveRunConfig.portfstratrun_id == psrunMax.c.id).filter(LiveRunConfig.account_id == Account.id).filter(Account.benchmark == True):                
                resDict[r[0]]['benchmark_winning_trades'] = r[1]
                resDict[r[0]]['benchmark_losing_trades'] = r[2]
                resDict[r[0]]['benchmark_open_trades'] = r[3]
                resDict[r[0]]['benchmark_net_profit'] = r[4]
                resDict[r[0]]['benchmark_floating_profit'] = r[5]
                resDict[r[0]]['benchmark_exposure'] = r[6]
                

        return True, OK, res, qry2.scalar()
    except:
        errorlog.error('Exception raised during personal account list', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

#    strNames = aliased(EntityName, session.query(EntityName).filter(EntityName.type==ELEMENT_TYPE_STRATEGY).subquery())    
#    psrunMax = session.query(PortfolioStrategyRun.strategy_id, func.max(PortfolioStrategyRun.id).label('id')).filter(PortfolioStrategyRun.portfoliorun_id == currPfrun[0]).group_by(PortfolioStrategyRun.strategy_id).subquery('psrunMax')
#    psrun = session.query(PortfolioStrategyRun).filter(PortfolioStrategyRun.id == psrunMax.c.id).subquery('psrun')
#    outerjoins = [(strNames, strNames.element_id == Strategy.id), (PortfolioRun, PortfolioRun.portfolio_id == id), (psrun, psrun.c.strategy_id == Strategy.id)]
#    idFilters = [Strategy.id == PortfolioStrategy.strategy_id, PortfolioStrategy.portfolio_id == id, or_(psrun.c.portfoliorun_id == None, psrun.c.portfoliorun_id == currPfrun[0]), PortfolioRun.id == currPfrun[0]]
#
#    query = session.query(Strategy.id.label('strat_id'),
#                          strNames.element_name.label('name'),
#                          Strategy.timeframe.label('timeframe'),
#                          PortfolioRun.id,
#                          psrun.c.portfoliorun_id,
#                          psrun.c.state,
#                          psrun.c.running_from,
#                          psrun.c.running_to,
#                          psrun.c.open_trades)
#
#    return ormObj.grid_list(start, limit, sort, dir, id, [], activeGetDict, session,
#        query,
#        Strategy.id,
#        outerjoins,
#        [],
#        [],
#        idFilters)


def activeGetDict(q):
    d = {}
    d["id"] = q.strat_id
    d["name"] = q.name
    if q.timeframe:
        d["timeframe_str"] = TIMEFRAME[q.timeframe]
    d["instruments"] = getSymbols(d["id"])
    d["number_of_instruments"] = getNumofInstruments(d["id"])
    prunid = q[3]
    psrun_prunid = q[4]
    if prunid and psrun_prunid and prunid == psrun_prunid:
        d['state'] = q[5]
        d['running_from'] = datetime2str(q[6])
        d['running_to'] = datetime2str(q[7])
        d['open_trades'] = q[8]
    return d

@cherrypy.expose
@paging_wrapper
def gridlist(start, limit, sort, dir, id = 0):
    '''
    A GUI számára visszaadja az elmentett stratégiák rendezett listáját 

    @url: /livestrategy/gridlist

    @type  id: int
    @param id: portfolio id
    @type  start:  int
    @param start: az első visszaadandó elem sorszáma
    @type  limit:  int
    @param limit: megadja hogy maximum hány elemet tartalmazzon a válasz
    @type  sort:  int
    @param sort: rendezési mező (ált. db object attribútum név) 
    @type  dir:  string
    @param dir: rendezés iránya (ASC, DESC)

    @rtype: (bool, str, list, int) tuple
    @return: sikeresség, szöveges üzenet, az adat-szótárak listája, és a stratégiák teljes száma

    @rtype: list
    @return:  listája  
    '''
    session = db.Session()

    #sort = 'strategys.' + sort
    if dir == "DESC":
        direction = desc
    else:
        direction = asc

    res = []
    resIds = []
    resDict = {}
    qry = session.query(Strategy.id, Strategy.timeframe, Strategy.symbol, Strategy.individual, Strategy.evolved_from, Strategy.evolved_to)
    if id:
        qry = qry.filter(PortfolioStrategy.portfolio_id == id).filter(PortfolioStrategy.strategy_id == Strategy.id)
    try:
        for r in qry.order_by(direction(sort).nullslast()).slice(start, start + limit):
            resItem = {}
            itemId = r[0]
            resItem['id'] = itemId
            resItem['timeframe_str'] = TIMEFRAME[r[1]]
            resItem['instruments'] = r[2]
            resItem['number_of_instruments'] = r[2].count(',') + 1
            resItem['individual'] = r[3]
            resItem['evolved_from'] = date2str(r[4])
            resItem['evolved_to'] = date2str(r[5])
            resItem['has_live_runs'] = False
            res.append(resItem)
            resDict[itemId] = resItem
            resIds.append(itemId)

        for r in session.query(EntityName.element_id, EntityName.element_name).filter(EntityName.element_id.in_(resIds)).filter(EntityName.type == ELEMENT_TYPE_STRATEGY).all():
            resDict[r[0]]['name'] = r[1]

        for r in session.query(StrategyRun.strategyId, func.count(StrategyRun.id)).filter(StrategyRun.strategyId.in_(resIds)).filter(StrategyRun.type == STRATEGYRUN_LIVE).group_by(StrategyRun.strategyId).all():
            resDict[r[0]]['has_live_runs'] = r[1] != 0

        if not id:
            for r in session.query(Strategy.id, PerformanceData.winning_trades, PerformanceData.losing_trades, PerformanceData.net_profit).filter(Strategy.id.in_(resIds)).filter(StrategyRun.strategyId == Strategy.id).filter(StrategyRun.performanceIdCurrency == PerformanceData.id).filter(StrategyRun.type == STRATEGYRUN_BACKTEST).filter(StrategyRun.configId == BacktestConfig.id).filter(BacktestConfig.from_date == Strategy.evolved_to).all():
                resDict[r[0]]['winning_trades'] = r[1]
                resDict[r[0]]['losing_trades'] = r[2]
                resDict[r[0]]['net_profit'] = r[3]

        return True, OK, res, session.query(func.count(Strategy.id)).scalar()
    except:
        errorlog.error('Exception raised during personal account list', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

#    filters = []
#    n  = aliased(EntityName)
#
#    strNames = aliased(n, session.query(n).filter(n.type==ELEMENT_TYPE_STRATEGY).subquery())    
#    query = session.query(Strategy.id.label('strat_id'),
#                          strNames.element_name.label('name'),
#                          Strategy.timeframe.label('timeframe'))
#
#    outerjoins = [(strNames, strNames.element_id == Strategy.id)]
#    return ormObj.grid_list(start, limit, sort, dir, id, filters, getDict, session,
#        query,
#        Strategy.id,
#        outerjoins,
#        [],
#        [],
#        [])


#def getDict(q):
#    d = {}
#    d["id"] = q.strat_id
#    d["name"] = q.name
#    if q.timeframe:
#        d["timeframe_str"] = TIMEFRAME[q.timeframe]
#    d["instruments"] = getSymbols(d["id"])
#    d["number_of_instruments"] = getNumofInstruments(d["id"])
#    return d
