# -*- encoding: utf-8 -*- 
'''
Created on May 24, 2011

@author: gdtlive
'''
import gdtlive.store.db as db
import logging, traceback, subprocess, os, time
#from os.path import dirname
from gdtlive.constants import LIVERUN_STATUS_FUCKEDUP, ELEMENT_TYPE_STRATEGY, TIMEFRAME, LIVERUN_STATUS_RUNNING, LIVERUN_STATUS_STARTING, LIVERUN_STATUS_STOPPING, LIVERUN_STATUS_ABORTING, LIVE_API_JFOREX, LIVE_API_FIX, LIVERUN_STATUS_STR
from gdtlive.evol.gnodes.GenomeBase import GTree
from gdtlive.config import JAVA, JCLIENT, JCLIENT_PATH, JYTHON, JFOREX_PATH, JFOREX_JARS
from gdtlive.config import SQL_URL, SQL_USER, SQL_PASS
from gdtlive.utils import datetime2str, first0
from gdtlive.languages.default import OK, EXCEPTION_OCCURED, GUI_REFRESH_NEEDED, NOT_FOUND, FOR_THIS_ID
from threading import Thread
from sqlalchemy import and_, desc
from gdtlive.admin.system.log import errorlog, extra
import cherrypy
from gdtlive.control.router import ajax_wrapper

import gdtlive.core.tradeengine
import ast


def run_client(config_id):    
    log = logging.getLogger('gdtlive.core')
    try:
        session = db.Session()
        conf = session.query(db.LiveRunConfig).get(config_id)
        if not conf:
            #WTF??? 
            return 
        
        python_path = [JYTHON, JCLIENT_PATH]
        for jar in JFOREX_JARS:            
            python_path.append(JFOREX_PATH+os.sep+jar.strip())    
        python_path = ':'.join(python_path)            
        parameters = [JAVA,'-Dlog4j.configuration=file:////%s/log4j.properties' % JFOREX_PATH, '-classpath', python_path,'-Dpython.path='+python_path, 'org.python.util.jython',JCLIENT, '--sqlurl', SQL_URL, '--sqluser', SQL_USER, '--sqlpass', SQL_PASS, '--liverunconfigid', str(config_id)]
        
        log.info(' '.join(parameters))
        client = subprocess.Popen(parameters, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        #client = subprocess.Popen(parameters)
        
        i = 60
        while i:
            log.debug('waiting for client')
            time.sleep(1)
            log.debug('check client status')
            client.poll()                
            if client.returncode:
                log.debug('error while starting client')                                
                session.refresh(conf)
                if conf.statusId != LIVERUN_STATUS_FUCKEDUP:
                    conf.statusId = LIVERUN_STATUS_FUCKEDUP
                    conf.status_message = "Can't start client!\n"
                    stdout = client.stdout.read()
                    stderr = client.stderr.read()
                    conf.status_message += 'return code = %d\n' % client.returncode
                    conf.status_message += 'stdout: %s\n' % stdout 
                    conf.status_message += 'stderr: %sn' % stderr
                    session.commit()
                    session.close()
                    log.error("Can't start broker client binary!")
                    log.error('return code : %d' % client.returncode)
                    log.error('stdout : %s' % stdout)
                    log.error('stderr : %s' % stderr)
#                    db.saveToHistory(conf.userId, ELEMENT_TYPE_STRATEGY, conf.strategyId, HISTORY_ACTION_FAILED)
                break
            i -= 1
    except:       
        conf.statusId = LIVERUN_STATUS_FUCKEDUP
        conf.status_message = traceback.format_exc()
        session.commit()

#@cherrypy.expose
#@ajax_wrapper
#def timeframes(strategyId):
#    ''' Ha létezik a stratégiához kapcsolt konfig, akkor visszaadja a 
#    stratégia timeframe-jét számként, ha nincs, akkor visszaadja az elérhető összes timeframe-t
#
#    @url: /liverun/timeframes
#
#    @type strategyId: int
#    @param strategyId: stratégia azonosítója  
#
#    @rtype: dict
#    @return: Sikeresség jelzés, timframe tuple-k listája (timeframenum, timeframestr)
#    '''
#    session=db.Session()
#    try:        
#        res=[]    
#        ecid=first0(session.query(db.Strategy.evolconfId).filter(db.Strategy.id==strategyId), 0)
#        if ecid:
#            ec=session.query(db.EvolConfig).get(ecid)
#            if ec:
#                r={}
#                r["timeframenum"]=ec.datarows[0].timeframe_num
#                r["timeframestr"]=TIMEFRAME[ec.datarows[0].timeframe_num]
#                res.append(r)
#            else:
#                for t in sorted(TIMEFRAME.keys()):                    
#                    res.append({"timeframenum":t,"timeframestr":TIMEFRAME[t]})
#        else:
#            timeframe=first0(session.query(db.LiveRunConfig.timeframe).filter(db.LiveRunConfig.strategyId==strategyId), 0)
#            if timeframe:
#                r={}
#                r["timeframenum"]=timeframe
#                r["timeframestr"]=TIMEFRAME[timeframe]
#                res.append(r)
#            else:
#                for t in sorted(TIMEFRAME.keys()):                    
#                    res.append({"timeframenum":t,"timeframestr":TIMEFRAME[t]})
#        return True, OK, res
#    except:
#        errorlog.error('Exception raised during liverun timeframes', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
#        return False, EXCEPTION_OCCURED
#    finally:
#        session.close()

@cherrypy.expose
@ajax_wrapper
def brokeraccounts(userId):
    ''' Visszaadja a user által elérhető brókerszámlák neveit és azonosítóit tartalmazó listát, 
    melyben NEM szerepelhet olyan számla, melyen éppen élesen futó stratégia van. 

    @url: /liverun/brokeraccounts

    @type userId: int
    @param userId: user azonosítója  

    @rtype: dict
    @return: Sikeresség jelzés, brokeraccount tuple-k listája (name, accountId)
    '''
    session=db.Session()
    try:        
        res=[]
        #ezzel nem látszik a frissen létrehozott broker account        
        #acc=session.query(db.AccountData).filter(db.AccountData.userId==userId).filter(db.AccountData.id.in_(session.query(db.LiveRunConfig.brokerAccountId).filter(db.LiveRunConfig.userId==userId).filter(db.LiveRunConfig.statusId!=4).filter(db.LiveRunConfig.statusId!=5).all())).all()
        acc=session.query(db.AccountData).filter(db.AccountData.userId==userId)
        for a in acc:
            res.append({"name":a.name,"id":a.id})
        return True, OK, res
    except:
        errorlog.error('Exception raised during liverun brokeraccounts', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@ajax_wrapper
def start(userId, strategyIds, accountId, timeframe):
    '''Elindítja a megadott stratégia éles futását

    @url: /liverun/start

    @type  userId:  int
    @param userId:  user azonosító
    @type  strategyIds:  int
    @param strategyIds:  stratégia azonosítók
    @type  accountId:  int
    @param accountId:  éles számla azonosító    
    @type  mmpluginId:  int
    @param mmplugind:  mmplugin azonosító
    @type  mmpluginsettings:  string
    @param mmpluginsettings:  money management beállításokat tartalmazó dict

    @rtype:    (bool, str) tuple
    @return:    Állapotjelzés, szöveges üzenet 
    '''        
    session = db.Session()
    errors = []
    try:
        user = session.query(db.User).get(userId)
        for strategyId in strategyIds:
            count = session.query(db.LiveRunConfig).filter(and_(db.LiveRunConfig.userId==userId, db.LiveRunConfig.brokerAccountId==accountId, db.LiveRunConfig.strategyId==strategyId, db.LiveRunConfig.statusId.in_([LIVERUN_STATUS_RUNNING, LIVERUN_STATUS_STARTING, LIVERUN_STATUS_STOPPING, LIVERUN_STATUS_ABORTING]))).count()
            if count > 0:
                errors.append("Strategy no. " + str(strategyId) + " is already running on this account!")
                continue

            conf = db.LiveRunConfig(userId, 0, '', accountId, strategyId, 0, timeframe)        
            conf.statusId = LIVERUN_STATUS_STARTING        
            session.add(conf)
            session.commit()
            
            strategy = session.query(db.Strategy).get(strategyId)
            if not strategy:
                errors.append(NOT_FOUND + "Strategy no. " + str(strategyId))
                continue
            
            genome = GTree.decode(strategy.dns)
            symbols = genome.getSymbols()        
            conf.symbols = ','.join(symbols)        
            
            for symbol in symbols:
                datarow_descriptor = session.query(db.DatarowDescriptor).filter(db.DatarowDescriptor.symbol==symbol).filter(db.DatarowDescriptor.timeframe_num==timeframe).first()                            
                conf.datarows.append(datarow_descriptor)        
            session.commit()
            
    #        db.saveToHistory(userId, ELEMENT_TYPE_STRATEGY, strategyId, HISTORY_ACTION_STARTED)
            
            account = session.query(db.AccountData).get(accountId)
            conf.mmplugin_id = account.mmpluginId
            conf.mmplugin_parameters = account.mmplugin_parameters
            session.commit()

            # JFOREX 
            if account.api == LIVE_API_JFOREX:        
                try:            
                    Thread(None, run_client, None, (conf.id,)).start()
                except:
                    conf.statusId = LIVERUN_STATUS_FUCKEDUP
                    conf.status_message = traceback.format_exc()
                session.commit()
                
            if account.api == LIVE_API_FIX:
                gdtlive.core.tradeengine.trade_engine.start_strategy(conf.id)
                
        if len(errors):
            return False, "\n".join(errors)
        return True, OK
    except:
#        db.saveToHistory(userId, ELEMENT_TYPE_STRATEGY, strategyId, HISTORY_ACTION_FAILED)
        errorlog.error('Exception raised during liverun start', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@ajax_wrapper
def list(userId):
    '''Visszaadja a felhasználó élesen futó stratégia listájának adatait

    @url: /liverun/list

    @type  userId:  int
    @param userId:  user azonosító

    @return: az élesen futó stratégia lista összes adata
    @rtype:    (bool, str, dict) tuple
    @return:    Állapotjelzés, szöveges üzenet, az élesen futó stratégia lista adat-szótára 
    '''
    session=db.Session()
    try:        
        result=[]
        #qry=session.query(LiveRunConfig).filter(LiveRunConfig.statusId!=4).filter(LiveRunConfig.statusId!=5).filter(LiveRunConfig.userId==userId).order_by(LiveRunConfig.id).all()
        qry=session.query(db.LiveRunConfig).filter(and_(db.LiveRunConfig.statusId!=4, db.LiveRunConfig.statusId!=5, db.LiveRunConfig.userId==userId)).order_by(desc(db.LiveRunConfig.id)).all()
        for ent in qry:
            res={}
            res["stratId"]=ent.strategyId
            res["liverunId"]=ent.strategyRunId
            res["liverunconfigId"]=ent.id
            res["stratname"]=first0(session.query(db.EntityName.element_name).filter(db.EntityName.element_id==ent.strategyId).filter(db.EntityName.type==ELEMENT_TYPE_STRATEGY), '')
            res["accountname"]=first0(session.query(db.AccountData.name).filter(db.AccountData.id==ent.brokerAccountId), '')
            res["runfrom"]=datetime2str(ent.running_from)
            res["runstatus"]=LIVERUN_STATUS_STR[ent.statusId]
            res["statmsg"]=ent.status_message
            res["instruments"]=ent.symbols
            res["number_of_instruments"]=len(ent.datarows)
            res["timeframe"]=TIMEFRAME[ent.timeframe]
            res["opennum"]=ent.opentrades            
            res["floating_profit"] = ent.floating_profit
            res["drawdown"] = ent.drawdown
            res["use_of_leverage"] = ent.use_of_leverage
            #ez csak akkor mukodik, ha mar elindult egy strategia, addig nem. 
            #van némi időeltolódás a liverunconfig, és az strategyrun lekerülése között
            res["realized"]=first0(session.query(db.PerformanceData.net_profit).filter(db.PerformanceData.id==db.StrategyRun.performanceIdCurrency).filter(db.StrategyRun.id==ent.strategyRunId), 0)            
            result.append(res)
        return True, OK, result
    except:
        errorlog.error('Exception raised during liverun list', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@ajax_wrapper
def adminList():
    '''Visszaadja az összes élesen futó stratégia lista adatokat

    @url: /liverun/adminList

    @return: az élesen futó stratégia lista összes adata
    @rtype:    (bool, str, dict) tuple
    @return:    Állapotjelzés, szöveges üzenet, az élesen futó stratégia lista adat-szótára 
    '''
    session=db.Session()
    try:        
        result=[]
        qry=session.query(db.LiveRunConfig).filter(db.LiveRunConfig.statusId!=4).filter(db.LiveRunConfig.statusId!=5).all()
        for ent in qry:
            res={}
            res["userId"]=ent.userId
            res["stratId"]=ent.strategyId
            res["liverunId"]=ent.strategyRunId
            res["liverunconfigId"]=ent.id
            res["stratname"]=first0(session.query(db.EntityName.element_name).filter(db.EntityName.element_id==ent.strategyId).filter(db.EntityName.type==ELEMENT_TYPE_STRATEGY), '')
            res["accountname"]=first0(session.query(db.AccountData.name).filter(db.AccountData.id==ent.brokerAccountId), '')
            res["runfrom"]=datetime2str(ent.running_from)
            res["runstatus"]=LIVERUN_STATUS_STR[ent.statusId]
            res["statmsg"]=ent.status_message
            res["instruments"]=ent.symbols
            res["timeframe"]=TIMEFRAME[ent.timeframe]
            res["openbool"]=bool(ent.opentrades)            
            result.append(res)
        return True, OK, result
    except:
        errorlog.error('Exception raised during liverun adminList', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

#@cherrypy.expose
#@ajax_wrapper
#def log(configId):
#    '''Visszaadja a felhasználó éles futási listájának adatait
#
#    @url: /liverun/log
#
#    @type  configId:  int
#    @param configId:  liverunconfig azonosító
#
#    @return: az éles futási napló összes adata
#    @rtype:    (bool, str, dict) tuple
#    @return:    Állapotjelzés, szöveges üzenet, az éles futási napló adat-szótára 
#    '''
#    session=db.Session()
#    try:        
#        result = ""
#        qry=session.query(db.LiveLogEntry).filter(db.LiveLogEntry.configId==configId).all()
#        for q in qry:
#            result += "%s - %s - %s\n" % (datetime2str(q.time), logging.getLevelName(q.level), q.message) 
##            r={}
##            r["configId"]=q.configId
##            r["time"]=
##            r["type"]=
##            r["message"]=q.message
##            res.append(r)
#        return True, OK, result
#    except:
#        errorlog.error('Exception raised during liverun log', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
#        return False, EXCEPTION_OCCURED
#    finally:
#        session.close()

@cherrypy.expose
@ajax_wrapper
def stop(liverunconfigIds):
    '''Leállítja a felhasználó adott éles futását

    @url: /liverun/stop

    @type  liverunconfigId:  int
    @param liverunconfigId:  liverunconfig azonosító

    @rtype:    (bool, str) tuple
    @return:    Állapotjelzés, szöveges üzenet 
    '''
    liverunconfigIds = ast.literal_eval(liverunconfigIds)
    session = db.Session()
    error = []
    liverunconfigIdsCopy = liverunconfigIds[:]
    try:
        for config in session.query(db.LiveRunConfig).filter(db.LiveRunConfig.id.in_(liverunconfigIds)).all():
            account = session.query(db.AccountData).get(config.brokerAccountId)
            if not account:
                error.append(NOT_FOUND + 'AccountData' + FOR_THIS_ID + ' (' + config.id + ')')
                continue

            if account.api == LIVE_API_JFOREX:
                config.command = 'STOP'
                config.status_message = 'Stop command sent'
                session.commit()

            if account.api == LIVE_API_FIX:
                gdtlive.core.tradeengine.trade_engine.stop_strategy(config.id)

#            db.saveToHistory(config.userId, ELEMENT_TYPE_STRATEGY, config.strategyId, HISTORY_ACTION_STOPPED)
            liverunconfigIdsCopy.remove(config.id)
        if len(liverunconfigIdsCopy):
            error.append(NOT_FOUND + 'LiveRunConfig' + FOR_THIS_ID + '(' + ','.join(str(x) for x in liverunconfigIdsCopy) + ')')
    except:
#        db.saveToHistory(config.userId, ELEMENT_TYPE_STRATEGY, config.strategyId, HISTORY_ACTION_FAILED)
        errorlog.error('Exception raised during liverun stop', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()
    if len(error):
        return False, '\n'.join(error)
    return True, OK

@cherrypy.expose
@ajax_wrapper
def abort(liverunconfigIds):
    '''megszakítja a felhasználó adott éles futását

    @url: /liverun/abort

    @type  liverunconfigId:  int
    @param liverunconfigId:  liverunconfig azonosító

    @rtype:    (bool, str) tuple
    @return:    Állapotjelzés, szöveges üzenet 
    '''
    session = db.Session()
    error = []
    liverunconfigIds = ast.literal_eval(liverunconfigIds)
    liverunconfigIdsCopy = liverunconfigIds[:]
    try:
        for config in session.query(db.LiveRunConfig).filter(db.LiveRunConfig.id.in_(liverunconfigIds)).all():
            account = session.query(db.AccountData).get(config.brokerAccountId)
            if not account:
                error.append(NOT_FOUND + 'AccountData' + FOR_THIS_ID + ' (' + config.id + ')')
                continue

            if account.api == LIVE_API_JFOREX:
                config.command = 'ABORT'
                config.status_message = 'Abort command sent'
                session.commit()

            if account.api == LIVE_API_FIX:
                gdtlive.core.tradeengine.trade_engine.abort_strategy(config.id)
            liverunconfigIdsCopy.remove(config.id)
        if len(liverunconfigIdsCopy):
            error.append(NOT_FOUND + 'LiveRunConfig' + FOR_THIS_ID + '(' + ','.join(str(x) for x in liverunconfigIdsCopy) + ')')
    except:
        errorlog.error('Exception raised during liverun abort', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()
    if len(error):
        return False, '\n'.join(error)
    return True, OK

@cherrypy.expose
@ajax_wrapper
def delete(liverunconfigId):
    '''Törli a felhasználó adott éles futását

    @url: /liverun/delete

    @type  liverunconfigId:  int
    @param liverunconfigId:  liverunconfig azonosító

    @rtype:    (bool, str) tuple
    @return:    Állapotjelzés, szöveges üzenet 
    '''
    session = db.Session()
    try:    
        config = session.query(db.LiveRunConfig).get(liverunconfigId)
        if not config:
            return False, GUI_REFRESH_NEEDED
        if config.statusId == LIVERUN_STATUS_FUCKEDUP:
            session.delete(config)
            session.commit()
            return True, OK
        else:
            return False, 'Only rows with status "fuckedup" can be deleted!' 
    except:
        errorlog.error('Exception raised during liverun delete', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()


#if __name__ == '__main__':    
#    python_path = [JCLIENT_PATH]
#    for jar in JFOREX_JARS:
#        python_path.append(JFOREX_PATH+os.sep+jar)    
#    python_path = ':'.join(python_path)    
#    parameters = [JYTHON,'-Dpython.path='+python_path, '-m','gdtlive.jclient.main','--sqlurl', JCLIENT_SQLURL, '--sqluser', JCLIENT_SQLUSER, '--sqlpass', JCLIENT_SQLPASSWORD, '--liverunconfigid', '1']
#    
#    client = subprocess.Popen(parameters, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
#    client.wait()
#    if client.returncode:
#        print 'FAIL!'
#        print client.returncode
#        print client.stdout.read()
#        print client.stderr.read()
