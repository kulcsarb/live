# -*- encoding: utf-8 -*- 
'''
Created on Jan 20, 2011

Az adatbázis objektumok általános kezelését végző modul
 
@author: elekes.arpad
'''
#from sqlalchemy.orm import sessionmaker, Session
#from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import func
import sqlalchemy as sa
import gdtlive.store.db as db
from gdtlive.languages.default import OK, EXCEPTION_OCCURED, NOT_FOUND, FOR_THIS_ID, WAS_SAVED_SUCCESFULLY, WAS_DELETED_SUCCESFULLY
#from gdtlive.constants import ELEMENT_TYPE, ELEMENT_TYPE_STRATEGY, STRATEGYRUN_LIVE, STRATEGYRUN_EVOL
import traceback 
from gdtlive.admin.system.log import errorlog, extra


def list(OrmClass):
    '''Visszaadja az adott típusú összes objektum összes adatát dict változók tömbjeként.
    
    A visszaadott lista elemei dictionary típusúak, ahol az egyes dictionary-k kulcs-érték párjai 
    az adott típusú objektum attribútum-érték párjainak felenek meg. A funkció célja, hogy az ExtJs Store objektuma 
    által generált adatletöltő kéréseket kiszolgáló webservice függvények feladatait absztrahálja. A funkció visszatérésként 
    az ExtJs Store által értelmezhető (success, message, data) formátumú választ állít elő.           
    
    @type OrmClass: declarative_base()
    @param OrmClass: bármilyen perzisztens osztály, mely a declarative_base()-ből van származtatva. Általában a gdtlivs.store.db modulban szereplő 
                     osztályok valamelyike  
    
    @rtype:     (bool, string, list) tuple 
    @return:    a sikerességet jelző boolean érték, szöveges üzenet a művelet eredményével kapcsolatban, az adatokat tartalmazó lista   
    
    '''
    session = db.Session()    
    try:        
        return True, OK, [dict(i) for i in session.query(OrmClass).all()] 
    except:
        errorlog.error('Exception raised during ormObj list', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()


#def listByFilters(OrmClass,filters):
#    '''Visszaadja az első paraméterrel (OrmClass) megadott típusú és a második paraméterrel (filters) megadott feltételeknek 
#    megfelelő objektumok összes adatát dict változók tömbjeként.
#    
#    A visszaadott lista elemei dictionary típusúak, ahol az egyes dictionary-k kulcs-érték párjai 
#    az adott típusú objektum attribútum-érték párjainak felenek meg. A funkció célja, hogy az ExtJs Store objektuma 
#    által generált adatletöltő kéréseket kiszolgáló webservice függvények feladatait absztrahálja. A funkció visszatérésként 
#    az ExtJs Store által értelmezhető (success, message, data) formátumú választ állít elő.           
#    
#    @type OrmClass: declarative_base()
#    @param OrmClass: bármilyen perzisztens osztály, mely a declarative_base()-ből van származtatva. Általában a gdtlive.store.db modulban szereplő 
#                     osztályok valamelyike  
#
#    @type:     filters: list
#    @param     filters: A feltételek listája, ahol a feltételek logikai állítás formájában adottak 
#
#    @rtype:     (bool, string, list) tuple 
#    @return:    sikeresség, szöveges üzenet, adatlista   
#    '''
#    session = db.Session()    
#    try:        
#        qry = session.query(OrmClass)
#        while filters :
#            qry = qry .\
#             filter(filters.pop())
#        return True, OK, [dict(i) for i in qry.all()] 
#    except:
#        errorlog.error('Exception raised during ormObj listByFilters', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
#        return False, EXCEPTION_OCCURED
#    finally:
#        session.close()


def save(OrmClass, **configData):
    '''Eltárolja a megadott objektum adatait.
    
    A funkció az adatokat kulcsszó paraméterekkel megadva várja. Amennyiben a id=0, akkor új  
    objektum kerül létrehozásra, ellenben a meglévő objektum adatai módosulnak. A funkció célja, hogy az ExtJs Store objektuma 
    által generált adattároló kéréseket kiszolgáló webservice függvények feladatait absztrahálja. A funkció visszatérésként 
    az ExtJs Store által értelmezhető (success, message, data) formátumú választ állít elő, ahol a data elem az objektum ID-jét 
    tartalmazza
    
    @type OrmClass: declarative_base()
    @param OrmClass: bármilyen perzisztens osztály, mely a declarative_base()-ből van származtatva. Általában a gdtlive.store.db modulban szereplő 
                     osztályok valamelyike
                       
    @type configData:    dict
    @param configData:   Az objektum adatai, ahol a kulcs-érték párok megfelelnek az OrmClass objektum attribútum-érték párjainak
    
    @rtype:     (bool, string, list) tuple 
    @return:    sikeresség, szöveges üzenet, adatlista  
    '''
    session = db.Session()    
    confId = int(configData.pop("id"))
    try:        
        if confId :
            obj = session.query(OrmClass).filter(OrmClass.id==confId).first()   
            if not obj:   
                return (False, NOT_FOUND + str(OrmClass) + FOR_THIS_ID, {"id": confId})   
            for attrName in configData :
                setattr(obj, attrName,configData[attrName])
        else :
            session.add(OrmClass(**configData))
            confId = session.query(func.max(OrmClass.id)).first()[0]
        session.commit()
        configData['id'] = confId
        return (True, str(OrmClass)+ WAS_SAVED_SUCCESFULLY, configData)
    except:
        session.rollback()
        errorlog.error('Exception raised during ormObj save', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()


def delete(OrmClass,id):
    '''Törli a megadott objektumot a rendszerből
    
    A funkció célja, hogy az ExtJs Store objektuma által generált adattörlő kéréseket 
    kiszolgáló webservice függvények feladatait absztrahálja. A funkció visszatérésként az ExtJs 
    Store által értelmezhető (success, message, data) formátumú választ állít elő, ahol a data elem 
    az objektum id-jét tartalmazza
    
    @type  OrmClass: declarative_base()
    @param OrmClass: bármilyen perzisztens osztály, mely a declarative_base()-ből van származtatva. 
                     Általában a gdtlive.store.db modulban szereplő osztályok valamelyike
                                          
    @type id: int
    @param id: Az objektum azonosítója
    
    @rtype:     (bool, string, list) tuple 
    @return:   sikeresség, szöveges üzenet, adatlista 
    '''
    session = db.Session()    
    try:        
        # session.query(OrmClass).filter(OrmClass.id==id).delete()
        # Ezzel a módszerrel az objektum gyerekeit is le lehet törölni. A feni megoldás nem törli a kapcsolt objektumait 
        obj = session.query(OrmClass).get(id)
        if not obj: 
            return (False, NOT_FOUND+str(OrmClass)+FOR_THIS_ID, {"id":id})
        session.delete(obj)
        session.commit()
        return (True,str(OrmClass)+ WAS_DELETED_SUCCESFULLY,{"id":id})
    except:
        session.rollback()
        errorlog.error('Exception raised during ormObj delete', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()
    

#def deleteByQrs(session, qrs):
#    '''Törli a db lekérdezések eredmény objektumait a rendszerből
#    
#    A funkció célja, hogy a DeleteRecursive típusú szolgáltatások feladatait absztrahálja. A funkció visszatérésként az ExtJs 
#    Store által értelmezhető (success, message) formátumú választ állít elő
#    
#    @type  session: session
#    @param session: db session
#    @type  qrs: list
#    @param qrs: A lekérdezések listája
#    
#    @rtype:     (bool, string) tuple 
#    @return:    sikeresség, szöveges üzenet
#    '''
#    try: 
#        for qry in qrs:       
#            for o in qry.all():
#                session.delete(o)
#        session.commit()
#        return True, OK
#    except :
#        session.rollback()
#        errorlog.error('Exception raised during ormObj deleteByQrs', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
#        return False, EXCEPTION_OCCURED
#    finally:
#        session.close()


#def get(OrmClass,id):
#    '''Lekéri egy store objektum adatait + a nevét, ha van 
#    
#    @type  OrmClass: declarative_base()
#    @param OrmClass: A gdtlive.store csomagban szereplő moduloknak megfelelő osztályok valamelyike
#    @type  id: int
#    @param id: Az objektum azonosítója
#    
#    @rtype:  (bool, string, dict) tuple 
#    @return: sikeresség, szöveges üzenet, objektum adat-szótára
#    '''
#    session = db.Session()
#    try:
#        obj = session.query(OrmClass).get(id)
#        if not obj:
#            return (False, NOT_FOUND+str(OrmClass)+FOR_THIS_ID, {"id":id})
#        d = dict(obj)
#        name = session.query(db.EntityName.element_name).filter(db.EntityName.element_id==id).filter(db.EntityName.type==ELEMENT_TYPE[OrmClass]).first()
#        if name:
#            d["name"]=name[0]
#        return True, OK, d
#    except:
#        errorlog.error('Exception raised during ormObj get', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
#        return False, EXCEPTION_OCCURED
#    finally:
#        session.close()


def tree_list(session, qry):#, getSize, showSize = 0, showCount = False, elementName=""
    '''A GUI treelist feltöltését kiszolgáló (store/.../treelist) webservicek általánosítása,
    visszaadja az összes, vagy az id-vel megadott szülőhöz tartozó adott típusú elem (azonosító,név) szótárak listáját, 
    és a showSize-nak megfelelően az összes kapcsolódó adat méretét a névben 
    
    @type  showSize: int
    @param showSize: 0 ha nem kell az adatok mérete, 1 ha kell
    
    @rtype: (bool, str, list) tuple
    @return: sikeresség, szöveges üzenet, adat-szótárak listája
    '''
    try:        
        res = []
        size = ""
        count = 1
        for elementId, name in qry.all():
#            if showSize:
#                size = getSize(elementId)
#            if showCount:
#                name = "%s #%d (%d)" % (elementName, count, elementId)
            res.append({"id": elementId,"name": str(name) + str(size)})
            count += 1
            
        return True, OK, res 
    except:
        errorlog.error('Exception raised during ormObj tree_list', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()


#def deleteStrats(session, fltr):
#    '''Törli a fltr-el meghatározott ormClass-hoz tartozó stratégiák közül azokat és CSAK azokat, 
#            amelyeknek nincs neve vagy 
#            nem szerepelnek mentett listában vagy 
#            nem volt éles futásuk.         
#
#    Ha egy stratégia szülői törlődnek, de maga a stratégia nem, akkor a stratégiák szülő mezői 0-ra állnak. 
#    Emellett azoknak a stratégiáknak az esetében, ahol a szülő evol config (és csak az evolconfig) törlődnének, 
#    még a szülők törlése előtt egy backtest-en kell átesniük a törlendő szülő config beállításaival. (backtest.backtestrunner.run_strategy(evolconfigId)) 
#    
#    @type  session: session
#    @param session: session
#    @type  fltr: filter
#    @param fltr: szűrő
#    
#    @rtype:  
#    @return: sikeresség, szöveges üzenet
#    '''
#    import gdtlive.backtest.backtestrunner as backtestrunner
#
#    qry = session.query(db.Strategy).filter(fltr)    
#    ss = set(qry.all())
#    ss -= set(qry.filter(db.EntityName.element_id==db.Strategy.id).filter(db.EntityName.type==ELEMENT_TYPE_STRATEGY).all())
#    ss -= set(qry.filter(db.ListsEntry.elementId==db.Strategy.id).all())
#    ss -= set(qry.filter(db.StrategyRun.strategyId==db.Strategy.id).filter(db.StrategyRun.type==STRATEGYRUN_LIVE).all())
#
#    for s in ss:
#        if str(fltr) == str(db.Strategy.evolconfId==id):# a szülő evol config
#            orderlog, tradelog, performance = backtestrunner.run_strategy(s.id)
#            pssd = db.PerformanceStrategySerialData(**performance)
#            session.add(pssd)
#            session.commit()
#            sr = session.query(db.StrategyRun).filter(db.StrategyRun.strategyId==s.id).filter(db.StrategyRun.type==STRATEGYRUN_EVOL).first() 
#            sr.orderlog=orderlog
#            sr.tradelog=tradelog
#            sr.performanceSerialId = pssd.id
#            session.commit()
#            
#        session.delete(s)
#    session.commit()
#
#    for s in qry.all():
#        if str(fltr) == str(db.Strategy.evolcycleId==id):
#            s.evolcycleId = 0
#        elif str(fltr) == str(db.Strategy.evolrunId==id):
#            s.evolrunId = 0
#        else: #str(fltr) == str(db.Strategy.evolconfId==id)
#            s.evolconfId = 0
#    session.commit()


def deleteFromGridList(OrmClass, ids, deleteRow):
    '''Törli az id-kkel megadott ormClass-okat, és a kapcsolódó objektumokat
    
    A GUI gridlist-jéből történő törlést kiszolgáló webservicek általánosítása 
    
    @type  OrmClass: declarative_base()
    @param OrmClass: osztályok, amelyek megjelenhetnek a gridlistben: EvolConfig, EvolCycle, EvolRun, Strategy, Workflow, WorkflowRun
    @type  ids:   list 
    @param ids:   a törlendő ormClass azonosítók
    @type  deleteRow:   metódus 
    @param deleteRow:   az OrmClass típusának megfelelően a törléseket elvégző metódus
    
    @rtype:  (bool, string, list) tuple 
    @return: sikeresség, szöveges üzenet, a törölt (EvolConfig esetében esetleg a nem törölhető) ormClass azonosítók listája
    '''
    if isinstance(ids, int):
        ids = [ids]
    session = db.Session()    
    try:  
        failedIds = []
        for elementId in ids:
            if deleteRow(session, elementId):
                failedIds.append(elementId) #csak EvolConfig esetében lehet 
  
        session.commit()
        if failedIds == []:
            return (True,str(OrmClass)+WAS_DELETED_SUCCESFULLY, {"ids": ids})
        else:
            return (False,"The following EvolConfigs cannot be deleted (they are used in workflows) :", {"ids":failedIds})
    except:
        session.rollback()
        errorlog.error('Exception raised during ormObj deleteFromGridList', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()


#def perf_summary(ormClass, id):
#    '''
#    Lekéri egy ormClass (start/evol) típusú lefutás összesített performancia adatait
#
#    @type  OrmClass: StrategyRun vagy EvolRun
#    @param OrmClass: lefutást reprezentáló perzisztens osztály
#
#    @type  id: int
#    @param id: Az objektum azonosítója
#
#    @rtype:  (bool, str, dict) tuple
#    @return: sikeresség, szöveges üzenet, performancia adatok
#    '''
#    session = db.Session()    
#    try:        
#        o = session.query(ormClass).get(id).first()[0]   
#        #pp = session.query(db.PerformanceData).get(o.performanceIdPoints) 
#        pc = session.query(db.PerformanceData).get(o.performanceIdCurrency) 
#        return True, OK, [dict(pc)]
#    except:
#        errorlog.error('Exception raised during ormObj perf_summary', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
#        return False, EXCEPTION_OCCURED
#    finally:
#        session.close()

def grid_list(start, limit, sort, dir, id, filters, getDict, session, qry, field, outerjoins, group_bies, group_bies2, idFilters):
    '''A GUI gridlist feltöltését kiszolgáló (store/.../gridlist) webservicek általánosítása, visszaadja a meghívó osztályok rendezett listáját 
    
    @type  start:  int
    @param start: az első visszaadandó elem sorszáma
    @type  limit:  int
    @param limit: megadja hogy maximum hány elemet tartalmazzon a válasz
    @type  sort:  int
    @param sort: rendezési mező (ált. db object attribútum név) 
    @type  dir:  string
    @param dir: rendezés iránya (ASC, DESC) 
    @type  id: int
    @param id: szűkíti a keresést, ha 0, akkor az összes elem között keres.
    @type  filters: list
    @param filters: plusz filterek
    @type  getDict: metódus
    @param getDict: a meghívó modul getDict metódusa, ami szótárba tölti a lekérdezés egy sorát az adott osztálynak megfelelően
    @type  session: session
    @param session: db session
    @type  qry: query
    @param qry: adott típusú osztálynak megfelelő lekérdezés
    @type  field: db mező
    @param field: a mező, aminek a számát le kell kérdezni a total number-hez, tipikusan az adott db osztály id-je
    @type  outerjoins: list
    @param outerjoins: az outer join feltételek
    @type  group_bies: list
    @param group_bies: a group by csoportosítások
    @type  group_bies2: list
    @param group_bies2: a group by csoportosítások a total number lekérdezéshez
    @type  idFilters: list
    @param idFilters: azon filterek listája, amire szűrni kell, ha a paraméterként kapott id nem 0
    
    @rtype:  (bool, str, list, int) tuple
    @return: sikeresség, szöveges üzenet, evolúciós konfiguráció adat-szótárak listája, az evolúciós konfigurációk teljes száma
    '''    

    try:  
        if dir == "DESC":   direction = sa.desc
        else:               direction = sa.asc

        qry2 = session.query(sa.func.count(field))        
        if id:
            for fltr in idFilters:
                qry = qry.filter(fltr)
                qry2 = qry2.filter(fltr)
        if filters:
            for fltr in filters:
                qry = qry.filter(fltr)
                qry2 = qry2.filter(fltr)
        if outerjoins:
            for outerjoin in outerjoins:
                qry = qry.outerjoin(outerjoin)
                qry2 = qry2.outerjoin(outerjoin)
        if group_bies:
            for group_by in group_bies:
                qry = qry.group_by(group_by)
        if group_bies2:
            for group_by in group_bies2:
                qry2 = qry2.group_by(group_by)

        return True, OK, [getDict(q) for q in qry.order_by(direction(sort).nullslast()).slice(start, start + limit)], qry2.scalar()
    except:
        errorlog.error('Exception raised during ormObj grid_list', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()
