# -*- encoding: utf-8 -*-
'''
Éles számlaadatok kezelését végző modul

Created on 2010.12.11.

@author: kulcsarb
@newfield url: Web service, Web services
'''

import gdtlive.store.db as db
from gdtlive.store.db import Account, PortfolioAccount, PortfolioRun, PortfolioAccountRun, AccountPerformance, Portfolio, LiveRunConfig, PerformanceData, PerformanceStrategySerialData
import gdtlive.store.ormObj as ormObj
from gdtlive.languages.default import OK, EXCEPTION_OCCURED, GUI_REFRESH_NEEDED, NAME_CONFLICT, NOT_FOUND
import traceback
from gdtlive.constants import LIVE_BROKERS#, LIVERUN_STATUS_STOPPED, LIVERUN_STATUS_ABORTED, LIVERUN_STATUS_FUCKEDUP, LIVERUN_STATUS_EXITED
from gdtlive.admin.system.log import errorlog, extra
import cherrypy
from gdtlive.control.router import store_wrapper, ajax_wrapper, tree_wrapper
#from sqlalchemy.sql.expression import or_, not_
from sqlalchemy import desc, func
from gdtlive.utils import datetime2str
import json
from datetime import datetime
import ast
import calendar

@cherrypy.expose
@ajax_wrapper
def info(id, userId):
    '''Lekéri egy account adatait

    @url: /liveaccount/info

    @type  id: int
    @param id: az account azonosítója

    @rtype:  (bool, string, dict) tuple 
    @return: Visszaadja a művelet sikerességét, szöveges üzenetet, valamint az stratégia adat-szótárát
    '''
    session = db.Session()
    try:
#        strat=session.query(Strategy).get(id)
#        if not strat:
#            return False, GUI_REFRESH_NEEDED
#        res=OrderedDict()
#        res["Strategy ID"]=strat.id
#        res["Name"]=first0(session.query(EntityName.element_name).filter(EntityName.type==ELEMENT_TYPE_STRATEGY).filter(EntityName.element_id==strat.id), '')
#        res["Creation date"]=datetime2str(first0(session.query(StrategyRun.time).filter(StrategyRun.type==STRATEGYRUN_EVOL), datetime.now()))
#        return True, OK, res
        return True, OK, {'param0': u'írjál meg: store/liveaccount/info'}
    except:
        errorlog.error('Exception raised during account info', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@ajax_wrapper
def getAll(id = 0, itemId = 0):
    '''
    Az (felhasználó) összes éles számla összes éles beállítását visszaadó függvény

    @url: /liveaccount/getAll

    @rtype: dict
    @return: sikeresség jelzés, felhasználó brókerszámláinak adatai dictben
    '''
    session = db.Session()
    res = []
    filters = []
    try:
        if id:
            currPfrun = session.query(PortfolioRun.id).filter(PortfolioRun.portfolio_id == id).order_by(desc(PortfolioRun.id)).first()
            filters.append(Account.id == PortfolioAccount.account_id)
            filters.append(PortfolioAccount.portfolio_id == id)

        if not id or not currPfrun:
            qry = session.query(Account)
            for fltr in filters:
                qry = qry.filter(fltr)
            if itemId:
                qry = qry.filter(Account.id == itemId)

            for r in qry:
                resItem = dict(r)
                del(resItem["password"])
                resItem["brokername"] = LIVE_BROKERS[resItem["broker_id"]]
                res.append(resItem)
        else:
            qry = session.query(Account)
            for fltr in filters:
                qry = qry.filter(fltr)
            if itemId:
                qry = qry.filter(Account.id == itemId)

            resIds = []
            resDict = {}
            for r in qry:
                resItem = dict(r)
                itemId = resItem['id']
                del(resItem["password"])
                resItem["brokername"] = LIVE_BROKERS[resItem["broker_id"]]
                res.append(resItem)
                resDict[itemId] = resItem
                resIds.append(itemId)

            if len(res):
                parunMax = session.query(PortfolioAccountRun.account_id, func.max(PortfolioAccountRun.id).label('id')).filter(PortfolioAccountRun.portfoliorun_id == currPfrun[0]).filter(PortfolioAccountRun.account_id.in_(resIds)).group_by(PortfolioAccountRun.account_id).subquery('parunMax')
                for r in session.query(PortfolioAccountRun.account_id, PortfolioAccountRun.state, PortfolioAccountRun.running_from, PortfolioAccountRun.running_to, PortfolioAccountRun.connection_state).filter(PortfolioAccountRun.id == parunMax.c.id).all():
                    resDict[r[0]]['state'] = r[1]
                    resDict[r[0]]['running_from'] = datetime2str(r[2])
                    resDict[r[0]]['running_to'] = datetime2str(r[3])
                    resDict[r[0]]['connection_state'] = r[4]

                aperfMax = session.query(AccountPerformance.account_id, func.max(AccountPerformance.id).label('id')).filter(AccountPerformance.account_id.in_(resIds)).group_by(AccountPerformance.account_id).subquery('aperfMax')
                for r in session.query(PortfolioAccountRun.account_id, PerformanceData.winning_trades, PerformanceData.losing_trades, PerformanceData.open_trades, PerformanceData.floating_profit, PerformanceData.net_profit, PerformanceData.starting_balance).filter(PerformanceData.id == PortfolioAccountRun.performance_id).filter(PortfolioAccountRun.id == parunMax.c.id).all():
                    resDict[r[0]]['winning_trades'] = r[1]
                    resDict[r[0]]['losing_trades'] = r[2]
                    resDict[r[0]]['open_trades'] = r[3]
                    resDict[r[0]]['floating_profit'] = r[4]
                    resDict[r[0]]['net_profit'] = r[5]
                    resDict[r[0]]['balance'] = r[5] + r[6]

#            parunMax = session.query(PortfolioAccountRun.account_id, func.max(PortfolioAccountRun.id).label('id')).filter(PortfolioAccountRun.portfoliorun_id == currPfrun[0]).group_by(PortfolioAccountRun.account_id).subquery('parunMax')
#            parun = session.query(PortfolioAccountRun).filter(PortfolioAccountRun.id == parunMax.c.id).subquery('parun')
#            filters.append(or_(parun.c.portfoliorun_id == None, parun.c.portfoliorun_id == currPfrun[0]))
#            filters.append(PortfolioRun.id == currPfrun[0])
#
#            aperfMax = session.query(AccountPerformance.account_id.label('account_id'), func.max(AccountPerformance.id)).group_by(AccountPerformance.account_id).subquery('aperfMax')
#            aperf = session.query(AccountPerformance).filter(AccountPerformance.account_id == aperfMax.c.account_id).subquery('aperf')
#
#            qry = session.query(Account, PortfolioRun.id, parun.c.portfoliorun_id, parun.c.state, parun.c.running_from, parun.c.running_to, parun.c.connection_state, aperf.c.equity, aperf.c.balance, aperf.c.floating, aperf.c.trade_num)
#            for fltr in filters:
#                qry = qry.filter(fltr)
#            qry = qry.outerjoin((PortfolioRun, PortfolioRun.portfolio_id == id)).outerjoin((parun, parun.c.account_id == Account.id)).outerjoin((aperf, aperf.c.account_id == Account.id))
#
#            for r in qry:
#                resItem = dict(r[0])
#                prunid = r[1]
#                parun_prunid = r[2]
#                if prunid and parun_prunid and prunid == parun_prunid:
#                    resItem['state'] = r[3]
#                    resItem['running_from'] = datetime2str(r[4])
#                    resItem['running_to'] = datetime2str(r[5])
#                    resItem['connection_state'] = r[6]
#                    resItem['equity'] = r[7]
#                    resItem['balance'] = r[8]
#                    resItem['floating'] = r[9]
#                    resItem['trade_num'] = r[10]
#                del(resItem["password"])
#                resItem["brokername"] = LIVE_BROKERS[resItem["broker_id"]]
#                res.append(resItem)
        return True, OK, res
    except:
        errorlog.error('Exception raised during personal account list', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

#@cherrypy.expose
#@paging_wrapper
#def listActive(start, limit, sort, dir, id = 0):
#    '''
#    Az aktív éles számlák összes éles beállítását visszaadó függvény
#
#    @url: /liveaccount/listActive
#
#    @type  id: int
#    @param id: currently not in use
#    @type  start:  int
#    @param start: az első visszaadandó elem sorszáma
#    @type  limit:  int
#    @param limit: megadja hogy maximum hány elemet tartalmazzon a válasz
#    @type  sort:  int
#    @param sort: rendezési mező (ált. db object attribútum név) 
#    @type  dir:  string
#    @param dir: rendezés iránya (ASC, DESC)
#
#    @rtype: dict
#    @return: sikeresség jelzés, felhasználó brókerszámláinak adatai dictben
#    '''
#    session = db.Session()
#    res = []
#    try:
#        if dir == "DESC":
#            direction = sa.desc
#        else:
#            direction = sa.asc
#        if sort == 'brokername':
#            sort = 'brokerId'
#        elif sort == 'apiName':
#            sort = 'api'
#        filters = [db.AccountData.id == db.LiveRunConfig.brokerAccountId, not_(db.LiveRunConfig.statusId.in_((LIVERUN_STATUS_STOPPED, LIVERUN_STATUS_ABORTED, LIVERUN_STATUS_FUCKEDUP, LIVERUN_STATUS_EXITED)))]
#        qry = session.query(db.AccountData)
#        qry2 = session.query(sa.func.count(db.AccountData.id))
#        for fltr in filters:
#            qry = qry.filter(fltr)
#            qry2 = qry2.filter(fltr)
#        for r in qry.order_by(direction(eval('db.AccountData.' + sort)).nullslast()).slice(start, start + limit):
#            r = dict(r)
#            del(r["password"])
#            r["brokername"] = LIVE_BROKERS[r["brokerId"]]
#            r["apiName"] = LIVE_BROKER_APIS[r["api"]]
#            res.append(r)
#        return True, OK, res, qry2.scalar()
#    except:
#        errorlog.error('Exception raised during personal account list', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
#        return False, EXCEPTION_OCCURED
#    finally:
#        session.close()

@cherrypy.expose
@ajax_wrapper
def runHistoryTree(id):
    '''Lekéri egy account lefutásainak listáját

    @url: /liveaccount/runHistoryTree

    @type  id: int
    @param id: az account azonosítója

    @rtype:  (bool, string, list) tuple 
    @return: Visszaadja a művelet sikerességét, szöveges üzenetet, valamint a stratégia lefutásainak listáját (adat-szótár tartalommal
    '''
    session = db.Session()
    res = {}
    try:
        for i in session.query(PortfolioAccountRun.id, Portfolio.id, Portfolio.name, PortfolioAccountRun.running_from, PortfolioAccountRun.running_to).filter(PortfolioAccountRun.account_id == id).filter(PortfolioAccountRun.portfolio_id == Portfolio.id).order_by(PortfolioAccountRun.running_from).all():
            if i[1] not in res:    #portfolio as root
                res[i[1]] = {'name': i[2], 'children': {}}
            if i[0] not in res[i[1]]['children']:    #accountruns under portfolio
                res[i[1]]['children'][i[0]] = {'from_date': i[3], 'to_date': i[4]}
        from_date = datetime.max
        to_date = datetime.min
        for portfolio_id, res_portfolio in res.iteritems():
            from_date_portfolio = datetime.max
            to_date_portfolio = datetime.min
            for run_id, res_run in res_portfolio['children'].iteritems():
                from_date_portfolio = min(from_date_portfolio, res_run['from_date'])
                # Ha res_run['to_date'] None, akkor elhal... 
                to_date_portfolio = max(to_date_portfolio, res_run['to_date'])
                res_run['from_date'] = datetime2str(res_run['from_date'])
                res_run['to_date'] = datetime2str(res_run['to_date'])
            res_portfolio['from_date'] = datetime2str(from_date_portfolio)
            res_portfolio['to_date'] = datetime2str(to_date_portfolio)
            from_date = min(from_date, from_date_portfolio)
            to_date = max(to_date, to_date_portfolio)
        res['from_date'] = datetime2str(from_date)
        res['to_date'] = datetime2str(to_date)
        return (True, OK, (json.dumps(res)))
    except:
        session.rollback()
        errorlog.error('Exception raised during account runHistoryTree', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@ajax_wrapper
def perf_summary(id = 0, id2 = 0, id3 = 0, from_date = None, to_date = None):
    '''
    Lekéri egy account lefutás összesített performancia adatait
    
    @url: /liveaccount/perf_summary

    @type  id: int
    @param id: broker account id, szűkíti a keresést
    @type  id2: int
    @param id2: portfolio id, szűkíti a keresést
    @type  id3: int
    @param id3: portfolio account run id, szűkíti a keresést
    @type from_date:  str
    @param from_date: Portfolio és Account esetén ha nem None, akkor ettől az időponttól van csak a perf számítás
    @type to_date:  str
    @param to_date: Portfolio és Account esetén ha nem None, akkor eddig az időpontig van csak a perf számítás

    @rtype: dict
    @return: performancia adatok 
    '''
    session = db.Session()
    try:
        if id3:
            o = session.query(PortfolioAccountRun).get(id3)
            if not o:
                return False, GUI_REFRESH_NEEDED
            pc = session.query(PerformanceData).get(o.performance_id)
            if pc:
                return True, OK, pc.view()
            else:
                return True, OK, {}
        else:   #TODO:
            print 'írjál meg: store/liveaccount/perf_summary'
            #Account.id == id
#            if id2:
#                Portfolio.id == id2
            return True, OK, {'param0': 'írjál meg: store/liveaccount/perf_summary'}
    except:
        errorlog.error('Exception raised during accountrun perf_summary', extra = extra(globals = globals(), locals = locals()), exc_info = traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@ajax_wrapper
def brokernames():
    '''
    Elmenti az adott felhasználó összes éles számla beállítását.

    @url: /liveaccount/brokernames

    @rtype: dict
    @return: sikeresség jelzés, felhasználó által elérhető brókerek nevei dictben
    '''
    return True, OK, [{"broker_id": broker_id, "brokername": brokername} for broker_id, brokername in LIVE_BROKERS.items()]

#@cherrypy.expose
#@ajax_wrapper
#def apinames(brokerId = 0):
#    '''
#    Elmenti az adott felhasználó összes éles számla beállítását.
#
#    @url: /liveaccount/apinames
#
#    @rtype: dict
#    @return: sikeresség jelzés, felhasználó által elérhető brókerek nevei dictben
#    '''
#    result = []
#    if brokerId:
#        for connection in LIVE_BROKER_CONNECTIONS[int(brokerId)]:
#            result.append({"apiId": connection, "apiName": LIVE_BROKER_APIS[connection]})
#    else:
#        result = [{"apiId": apiId, "apiName": apiname} for apiId, apiname in LIVE_BROKER_APIS.items()]
#    return True, OK, result

@cherrypy.expose
@store_wrapper
def save(**accountData):
    '''
    Elmenti az adott felhasználó összes éles számla beállítását.

    @url: /liveaccount/save

    @type accountData:     dict
    @param accountData:    számlaadatok 
    '''
    session=db.Session()
    del accountData["brokername"]
    if 'state' in accountData:
        del accountData['state']
    if 'running_from' in accountData:
        del accountData['running_from']
    if 'running_to' in accountData:
        del accountData['running_to']
    for key in accountData.keys():
        if accountData[key] == '':
            accountData[key] = None
    accountData["broker_id"] = int(accountData["broker_id"])
    if not accountData["password"]:
        del accountData["password"]
    itemId = int(accountData.get("id", 0))
    name = accountData.get('name')
    try:
        if itemId:
            item = session.query(Account).get(itemId)
            if not item:
                return False, GUI_REFRESH_NEEDED
            if dict(item).get('name') != name:
                sameExist = session.query(Account.id).filter(Account.name == name).first()
                if sameExist:
                    return False, NAME_CONFLICT
            print accountData
            for attrName in accountData:                
                setattr(item, attrName, accountData[attrName])
            
            session.commit()    
            try:
                import gdtlive.core.tradeengine as live
                if live.trade_engine:                    
                    live.trade_engine.refresh_account(itemId)                
            except:
                pass            
        else:
            sameExist = session.query(Account.id).filter(Account.name == name).first()
            if sameExist:
                return False, NAME_CONFLICT
            del accountData['id']
            item = Account(**accountData)
            session.add(item)
            session.commit()
            
        return getAll(itemId = item.id)
    except:
        session.rollback()
        errorlog.error('Exception raised during portfolio save', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@store_wrapper
def delete(account_id):
    '''
    Törli az adott számla beállítást.

    @url: /liveaccount/delete

    @type accountId:     int
    @param accountId:    account azonosító
    '''
    return ormObj.delete(Account, account_id)

@cherrypy.expose
@tree_wrapper
def treelist(id = 0):
    '''A GUI számára visszaadja a megadott liveaccounthoz tartozó éles stratégiák azonosító-név listáját  

    @url: /liveaccount/treelist

    @type  showSize: int
    @param showSize: not used yet
    @type  id: int
    @param id: liveaccount id, a megadott liveaccount szűkítő azonosító

    @rtype: (bool, str, list) tuple
    @return: sikeresség, szöveges üzenet, az evolúsiós ciklus adat-szótárak listája
    '''
    session = db.Session()
    return ormObj.tree_list(session, session.query(Account.id, Account.name).filter(PortfolioAccount.portfolio_id == id).filter(PortfolioAccount.account_id == Account.id))

@cherrypy.expose
@ajax_wrapper
def runSymbolTimeframes(id = 0, id2 = 0, id3 = 0):
    '''
    Returns symbol and timeframe pairs for a live account run

    @url: /liveaccount/runSymbolTimeframes

    @type  id: int
    @param id: broker account id, szűkíti a keresést
    @type  id2: int
    @param id2: portfolio id, szűkíti a keresést
    @type  id3: int
    @param id3: portfolio account run id, szűkíti a keresést
    @rtype: dict
    @return: symbol-timeframe pairs
    '''
    session = db.Session()
    paruns = []
    try:
        if id3:
            paruns.append(id3)
        else:
            qry = session.query(PortfolioAccountRun.id).filter(PortfolioAccountRun.account_id == id)
            if id2:
                qry = qry.filter(PortfolioAccountRun.portfolio_id == id2)
            paruns = [parun[0] for parun in qry.all()]
        timeframes = {}
        for symbols, timeframe in session.query(LiveRunConfig.symbols, LiveRunConfig.timeframe).filter(LiveRunConfig.accountrun_id.in_(paruns)):
            if timeframe not in timeframes:
                timeframes[timeframe] = []
            for symbol in symbols.split(','):
                if symbol not in timeframes[timeframe]:
                    timeframes[timeframe].append(symbol)
        return True, OK, timeframes
    except:
        errorlog.error('Exception raised during account runSymbolTimeframes performance', extra = extra(globals = globals(), locals = locals()), exc_info = traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@ajax_wrapper
def periods(id = 0, id2 = 0, id3 = 0, from_date = None, to_date = None):
    '''
    Lekéri egy account lefutás időszakos teljesitményadatait

    @url: /liveaccount/periods

    @type  id: int
    @param id: broker account id, szűkíti a keresést
    @type  id2: int
    @param id2: portfolio id, szűkíti a keresést
    @type  id3: int
    @param id3: portfolio account run id, szűkíti a keresést
    @type from_date:  str
    @param from_date: Portfolio és Account esetén ha nem None, akkor ettől az időponttól van csak a perf számítás
    @type to_date:  str
    @param to_date: Portfolio és Account esetén ha nem None, akkor eddig az időpontig van csak a perf számítás

    @rtype: dict
    @return: performancia adatok
    '''
    session = db.Session()
    try:
        if id3:
            o = session.query(PortfolioAccountRun).get(id3)
            if not o:
                return False, GUI_REFRESH_NEEDED
            d = session.query(PerformanceStrategySerialData).get(o.performanceserial_id)
            if not d:
                return False, NOT_FOUND + 'performance'
            conf = None
            perf_from_date = o.running_from
    
            drawdown = []
            trades_profit_loss = []
            MAE = []
            MFE = []
            startTime = calendar.timegm(perf_from_date.timetuple())
            weekTime = (startTime - (startTime + 259200) % 604800) * 1000  #week start time
            beginYear = perf_from_date.year
            beginMonth = perf_from_date.month
            weekly_profit = ast.literal_eval(d.weekly_profit)
            monthly_profit = ast.literal_eval(d.monthly_profit)
            yearly_profit = ast.literal_eval(d.yearly_profit)
            drawdown_lst = ast.literal_eval(d.drawdown)
            trades_profit_loss_lst = ast.literal_eval(d.trades_profit_loss)
            MAE_lst = ast.literal_eval(d.MAE)
            MFE_lst = ast.literal_eval(d.MFE)
            month = beginMonth
            year = beginYear
            for i in xrange(len(monthly_profit)):
                monthly_profit[i] = (str(year) + "-" + str(month).zfill(2), monthly_profit[i])
                month += 1
                if month == 13:
                    year += 1
                    month = 1
            year = beginYear
            for i in xrange(len(yearly_profit)):
                yearly_profit[i] = (str(year), yearly_profit[i])
                year += 1
            for i in xrange(len(weekly_profit)):
                weekly_profit[i] = (weekTime, weekly_profit[i])
                drawdown += drawdown_lst[i]
                trades_profit_loss += trades_profit_loss_lst[i]
                MAE += MAE_lst[i]
                MFE += MFE_lst[i]
                weekTime += 604800000
    #            if not len(drawdown_lst[i]):
    #                startTime += 604800
    #                continue
    #            drawdown.append((startTime, 'separator'))
    #            trades_profit_loss.append((startTime, 'separator'))
    #            MAE.append((startTime, 'separator'))
    #            MFE.append((startTime, 'separator'))
    #            drawdown += drawdown_lst[i]
    #            trades_profit_loss += trades_profit_loss_lst[i]
    #            MAE += MAE_lst[i]
    #            MFE += MFE_lst[i]
    #            startTime += 604800
    #            if i == count - 1 or not len(drawdown_lst[i + 1]):
    #                drawdown.append((startTime, 'separator'))
    #                trades_profit_loss.append((startTime, 'separator'))
    #                MAE.append((startTime, 'separator'))
    #                MFE.append((startTime, 'separator'))
    
            return True, OK, {"account_balance": ast.literal_eval(d.net_profit),
                              "drawdown": drawdown,
                              "trades_profit_loss": trades_profit_loss,
                              "daily_profit": ast.literal_eval(d.daily_profit),
                              "weekly_profit": weekly_profit,
                              "monthly_profit": monthly_profit,
                              "yearly_profit": yearly_profit,
                              "floating_profit": ast.literal_eval(d.floating_profit),
                              "use_of_leverage": ast.literal_eval(d.use_of_leverage),
                              "MAE": MAE,
                              "MFE": MFE,
                              "beta": ast.literal_eval(d.beta)
                              }
        else:   #TODO:
            print 'írjál meg: store/liveaccount/periods'
            #Account.id == id
#            if id2:
#                Portfolio.id == id2
            return True, OK, {"account_balance": [],
                              "drawdown": [],
                              "trades_profit_loss": [],
                              "daily_profit": [],
                              "weekly_profit": [],
                              "monthly_profit": [],
                              "yearly_profit": [],
                              "floating_profit": [],
                              "use_of_leverage": [],
                              "MAE": [],
                              "MFE": [],
                              "beta": []
                              }
    except:
        errorlog.error('Exception raised during account run get periodic performance', extra = extra(globals = globals(), locals = locals()), exc_info = traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()