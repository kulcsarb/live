# -*- encoding: utf-8 -*- 
'''
Created on May 24, 2011

@author: gdtlive
'''

#import gdtlive.store.db as db
#import gdtlive.store.ormObj as ormObj
#from gdtlive.languages.default import *
#import traceback
#from gdtlive.constants import *
#from gdtlive.utils import datetime2str, date2str, R, first0
#import ast
#from gdtlive.store.strategy import getSymbols, getNumofInstruments
#from gdtlive.admin.system.log import errorlog, extra
#import cherrypy
#from gdtlive.control.router import ajax_wrapper, store_wrapper
#from gdtlive.control.session import handle_session

#@cherrypy.expose
#@ajax_wrapper
#def list(userId):
#    '''Visszaadja a felhasználó éles futási listájának adatait
#
#    @url: /livelist/list
#
#    @type  userId:  int
#    @param userId:  user azonosító
#
#    @return: az éles futási lista összes adata
#    @rtype:    (bool, str, dict) tuple
#    @return:    Állapotjelzés, szöveges üzenet, az éles futási lista adat-szótára 
#    '''
#    session=db.Session()
#    try:
#        livelist=session.query(db.LiveListsEntry).filter(db.LiveListsEntry.userId==userId).all()               
#        result=[]        
#        for entry in livelist:
#            res={}
#            res["id"]=entry.strategyId
#            res["stratname"]=first0(session.query(db.EntityName.element_name).filter(db.EntityName.element_id==entry.strategyId).filter(db.EntityName.type==ELEMENT_TYPE_STRATEGY), '')               
#            ec=session.query(db.EvolConfig).get(first0(session.query(db.Strategy.evolconfId).filter(db.Strategy.id==entry.strategyId), 0))
#            res["instruments"]=""
#            if ec:
#                for datarow in ec.datarows: 
#                    res["instruments"]+=datarow.symbol+", "
#                res["instruments"]=res["instruments"][:-2]
#                res["number_of_instruments"] = len(ec.datarows)
#                res["timeframe"]=TIMEFRAME[datarow.timeframe_num]
#                res["testfromto"]=date2str(ec.from_date)+" - "+date2str(ec.to_date)
#                res["evolconfname"]=first0(session.query(db.EntityName.element_name).filter(db.EntityName.element_id==ec.id).filter(db.EntityName.type==ELEMENT_TYPE_EVOLCONFIG), '')
#            else:
#                res["instruments"]=getSymbols(entry.strategyId)
#                res["number_of_instruments"] = getNumofInstruments(entry.strategyId)
#                res["timeframe"]=""
#                res["testfromto"]=""
#                res["evolconfname"]=""
#            perf=session.query(db.PerformanceData).filter(db.PerformanceData.id==db.StrategyRun.performanceIdCurrency).filter(db.StrategyRun.strategyId==entry.strategyId).order_by(db.StrategyRun.time).first()
#            if perf:
#                res["fitness"]=R(perf.fitness)
#                res["maxDD"]=R(perf.maximum_drawdown)
#                res["sharpe"]=R(perf.sharpe_ratio)
#                res["netprofit"]=R(perf.net_profit)
#            else:
#                res["fitness"]=0
#                res["maxDD"]=0
#                res["sharpe"]=0
#                res["netprofit"]=0
#            result.append(res)
#        return True, OK, result
#    except:
#        errorlog.error('Exception raised during livelist list', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
#        return False, EXCEPTION_OCCURED
#    finally:
#        session.close()

#@cherrypy.expose
#@ajax_wrapper
#def add(userId, strategyIds):
#    '''Hozzáadja az adott stratégiát a felhasználó éles futáasi listájához
#
#    @url: /livelist/add
#
#    @type  userId:  int
#    @param userId:  user azonosító
#    @type  strategyIds:  list
#    @param strategyIds:  stratégia azonosítók listája
#
#    @rtype:    (bool, str) tuple
#    @return:    Állapotjelzés, szöveges üzenet (ha már szerepel a listában, szöveges jelzés)
#    '''
#    session=db.Session()
#    try:
#        added=0
#        already=0
#        userId=ast.literal_eval(userId)
#        strategyIds=ast.literal_eval(strategyIds)              
#        for strat in strategyIds:            
#            exists=session.query(db.LiveListsEntry).filter(db.LiveListsEntry.userId==userId).filter(db.LiveListsEntry.strategyId==strat).first()
#            if not(exists):
#                livelistentry=db.LiveListsEntry(userId, strat)
#                session.add(livelistentry)
#                session.commit()
#                added+=1
#            else:
#                already+=1
#        res=str(added)+" strategies added to Live List, "+str(already)+" were already in it."
#        return True, OK, res 
#    except:
#        errorlog.error('Exception raised during livelist add', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
#        return False, EXCEPTION_OCCURED
#    finally:
#        session.close()

#@cherrypy.expose
#@store_wrapper
#def remove(id):
#    '''Törli az adott stratégiát a felhasználó éles futáasi listájából
#
#    @url: /livelist/remove
#
#    @type  strategyId:  int
#    @param strategyId:  stratégia azonosító
#
#    @rtype:    (bool, str) tuple
#    @return:    Állapotjelzés, szöveges üzenet 
#    '''
#    session=db.Session()
#    http_session = handle_session()
#    try:           
#        strat=session.query(db.LiveListsEntry).filter(db.LiveListsEntry.strategyId==id).filter(db.LiveListsEntry.userId==http_session.userId).first()
#        if not strat:
#            return True, OK
#        if session.query(db.LiveRunConfig).filter(db.LiveRunConfig.strategyId==id).filter(db.LiveRunConfig.statusId!=4).filter(db.LiveRunConfig.statusId!=5).first():
#            return False, "Strategy "+str(id)+" can not be deleted, since it is actually running."
#        else:
#            session.delete(strat)
#            session.commit()
#            return True, OK
#    except:
#        errorlog.error('Exception raised during livelist remove', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
#        return False, EXCEPTION_OCCURED
#    finally:
#        session.close()
