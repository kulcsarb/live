# -*- encoding: utf-8 -*-

import cherrypy
import traceback
import gdtlive.store.db as db
import sqlalchemy as sa
#from sqlalchemy.sql.expression import not_
from gdtlive.control.router import paging_wrapper
from gdtlive.languages.default import OK, EXCEPTION_OCCURED, GUI_REFRESH_NEEDED
from gdtlive.admin.system.log import errorlog, extra
from gdtlive.core.constants import TRADE_CLOSED, TRADE_STR
from gdtlive.constants import DIR_STR, ORDERTYPE_STR, STRATEGYRUN_LIVE, STRATEGYRUN_BACKTEST
from gdtlive.core.trade import MARKETORDER_STATES, CONDORDER_STATES
from gdtlive.utils import first0, R, R2
import ast
from datetime import datetime
from gdtlive.store.db import Strategy, Trade, MarketOrder, Stoploss, Takeprofit, StrategyRun, Account, Portfolio,\
    EntityName

@cherrypy.expose
@paging_wrapper
def gridlist(start, limit, sort, dir, id = 0, tradeId = None):
    '''
    Az (felhasználó) összes éles trade-et visszaadó függvény

    @url: /trade/gridlist

    @type  id: int
    @param id: strategy id for filtering
    @type  start:  int
    @param start: az első visszaadandó elem sorszáma
    @type  limit:  int
    @param limit: megadja hogy maximum hány elemet tartalmazzon a válasz
    @type  sort:  int
    @param sort: rendezési mező (ált. db object attribútum név) 
    @type  dir:  string
    @param dir: rendezés iránya (ASC, DESC)

    @rtype: dict
    @return: sikeresség jelzés, felhasználó brókerszámláinak adatai dictben
    '''
    session = db.Session()
    res = []
    if tradeId:
        try:
            tradeId_int = int(tradeId[2:]);
        except:
            tradeId_int = 0
    else:
        tradeId_int = 0
    if tradeId_int:
        subLines = session.query(MarketOrder, Stoploss, Takeprofit).filter(MarketOrder.trade_id == tradeId_int).filter(Stoploss.trade_id == tradeId_int).filter(Takeprofit.trade_id == tradeId_int).first()
        if subLines:
            mo = dict(subLines[0])
            mo["state"] = MARKETORDER_STATES[mo["state"]]
            mo["type"] = 'marketorder'
            mo["_parent"] = tradeId
            mo["_is_leaf"] = True
            mo["_id"] = "mo" + str(mo["id"])
            del mo['trade_id'], mo['symbol'], mo['amount'], mo['open_direction'], mo['open_send_time'], mo['open_clordid'], mo['close_direction'], mo['close_send_time'], mo['close_clordid'], mo['correction_price'], mo['correction_send_time'], mo['correction_fill_time'], mo['correction_clordid'], mo['_clordid']
            res.append(mo)
            sl = dict(subLines[1])
            sl["state"] = CONDORDER_STATES[sl["state"]]
            sl["type"] = 'stoploss'
            sl["_parent"] = tradeId
            sl["_is_leaf"] = True
            sl["_id"] = "sl" + str(sl["id"])
            del sl['trade_id'], sl['entry_clordid'], sl['entry_orderid'], sl['entry_sent_time'], sl['filled_amount'], sl['cancel_clordid'], sl['cancel_sent_time'], sl['canceled_time'], sl['_clordid']
            res.append(sl)
            tp = dict(subLines[2])
            tp["state"] = CONDORDER_STATES[tp["state"]]
            tp["type"] = 'takeprofit'
            tp["_parent"] = tradeId
            tp["_is_leaf"] = True
            tp["_id"] = "tp" + str(tp["id"])
            del tp['trade_id'], tp['entry_clordid'], tp['entry_orderid'], tp['entry_sent_time'], tp['filled_amount'], tp['cancel_clordid'], tp['cancel_sent_time'], tp['canceled_time'], tp['_clordid']
            res.append(tp)
        return True, OK, res, len(res)
    else:
        resIds = []
        resDict = {}
        try:
            if dir == "DESC":
                direction = sa.desc
            else:
                direction = sa.asc
    #        if sort == 'brokername':
    #            sort = 'brokerId'
    #        elif sort == 'apiName':
    #            sort = 'api'
            filters = [Trade.state != TRADE_CLOSED, Trade.marketorder_id != 0]
            qry = session.query(Trade)
            qry2 = session.query(sa.func.count(Trade.id))
            for fltr in filters:
                qry = qry.filter(fltr)
                qry2 = qry2.filter(fltr)
            try:
                qry = qry.order_by(direction(eval('Trade.' + sort)).nullslast())
            except:
                pass
            for r in qry.slice(start, start + limit):
                r = dict(r)
                itemId = r['id']
                resIds.append(itemId)
                resDict[itemId] = r
                del r['marketorder_id'], r['stoploss_id'], r['takeprofit_id'], r['open_time'], r['open_price'], r['sl_price'], r['tp_price'], r['floating_profit'], r['profit'], r['MAE'], r['MFE']
                r["state"] = TRADE_STR[r["state"]]
                r["direction"] = DIR_STR[r["direction"]]
                r["type"] = 'trade'
                r["_parent"] = None
                r["_is_leaf"] = False
                r["_id"] = "tr" + str(itemId)
                res.append(r)
            if len(res):
                for r in session.query(Trade.id, Account.name).filter(Account.id == Trade.account_id).filter(Trade.id.in_(resIds)).all():
                    resDict[r[0]]['account_name'] = r[1]
                for r in session.query(Trade.id, Portfolio.name).filter(Portfolio.id == Trade.portfolio_id).filter(Trade.id.in_(resIds)).all():
                    resDict[r[0]]['portfolio_name'] = r[1]

            return True, OK, res, qry2.scalar()
        except:
            errorlog.error('Exception raised during trade gridlist', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
            return False, EXCEPTION_OCCURED
        finally:
            session.close()

@cherrypy.expose
@paging_wrapper
def gridlist_browse(id = 0, id2 = 0, id3 = 0, id4 = 0, type4 = STRATEGYRUN_LIVE, sort = 'id', start = 0, limit = 0, dir = 0):
    '''
    returns grid list of trades for live browse

    @url: /livestrategy/gridlist_browse

    @type  id: int
    @param id: broker account id, szűkíti a keresést
    @type  id2: int
    @param id2: portfolio id, szűkíti a keresést
    @type  id3: int
    @param id3: portfolio account run id, szűkíti a keresést
    @type  id4: int
    @param id4: strategy run id, szűkíti a keresést
    @type  start:  int
    @param start: az első visszaadandó elem sorszáma
    @type  limit:  int
    @param limit: megadja hogy maximum hány elemet tartalmazzon a válasz
    @type  sort:  int
    @param sort: rendezési mező (ált. db object attribútum név) 
    @type  dir:  string
    @param dir: rendezés iránya (ASC, DESC)

    @rtype: (bool, str, list, int) tuple
    @return: sikeresség, szöveges üzenet, az adat-szótárak listája, és a stratégiák teljes száma

    @rtype: list
    @return:  listája  
    '''
    session = db.Session()
    res = []
    type4 = int(type4)
    #print 'account id:', id, 'portfolio id:', id2
    try:
        if dir == "DESC":
            direction = sa.desc
        else:
            direction = sa.asc
        filters = []
        if id:
            filters.append(Trade.account_id == id)
        if id2:
            filters.append(Trade.portfolio_id == id2)
        if id3:
            filters.append(Trade.accountrun_id == id3)
        if id4:
            if type4 == STRATEGYRUN_BACKTEST:
                res = []
                log = first0(session.query(StrategyRun.tradelog).filter(StrategyRun.id == id4), 0)
                if not log:
                    return False, GUI_REFRESH_NEEDED
                if log == '()':
                    return True, OK, ''
        
                #azert hogy együttműködjön a régebben generált, hibásan formázott stratégiákkal is...                     
                log = ast.literal_eval(log)
                if type(log[0][0]) != tuple:
                    log = (log,)
                for r in log:
                    fr = dict(zip(["id", "symbol", "direction", "opentime", "openprice", "openamount", "sl", "tp", "opencommission"], r[0]))
                    fr['amount'] = fr['openamount']
                    fr["open_time"] = datetime.utcfromtimestamp(int(fr["opentime"])).strftime('%Y-%m-%d %H:%M:%S')
                    fr["direction"] = DIR_STR[fr["direction"]]
                    fr['sl_price'] = fr['sl']
                    fr['tp_price'] = fr['tp']
                    fr['state'] = TRADE_STR[TRADE_CLOSED]
#                    fr["opencommission"] = round(fr["opencommission"], 2)
                    fr["open_price"] = R(fr["openprice"])
                    #res.append(fr)
                    del fr['openamount'], fr['opencommission'], fr['opentime'], fr['sl'], fr['tp']
                    tmp = dict(zip(["type", "closetime", "closeprice", "closeamount", "pip", "profit", "closecommission", "rollover", "group"], [0, 0, 0, 0, 0, 0, 0, 0]))
                    for c in r[1:]:
                        tmp = dict(zip(["type", "closetime", "closeprice", "pip", "profit", "closecommission", "rollover", "group"], c))
                        tmp["close_time"] = datetime.utcfromtimestamp(int(tmp["closetime"])).strftime('%Y-%m-%d %H:%M:%S')
                        tmp["close_type"] = ORDERTYPE_STR[tmp["type"]]
                        tmp["close_price"] = R(tmp["closeprice"])
#                        tmp["closecommission"] = R2(tmp["closecommission"])
                        tmp["profit"] = R2(tmp["profit"])
                        del tmp['type'], tmp['closetime'], tmp['closeprice'], tmp['pip'], tmp['closecommission'], tmp['rollover'], tmp['group']
                        #fr.append(tmp)
                        #res.append(tmp)
                    if tmp:
                        res.append(dict(fr, **tmp))
                return True, OK, res
            filters.append(Trade.strategyrun_id == id4)
        else:
            if limit:
                filters.append(Trade.state != TRADE_CLOSED)
        qry = session.query(Trade, Strategy.timeframe)
        filters.append(Trade.strategy_id == Strategy.id)
        if limit:
            qry2 = session.query(sa.func.count(Trade.id))
        for fltr in filters:
            qry = qry.filter(fltr)
            if limit:
                qry2 = qry2.filter(fltr)
        try:
            qry = qry.order_by(direction(eval('Trade.' + sort)).nullslast())
        except:
            pass
        if limit:
            qryRes = qry.slice(start, start + limit)
        else:
            qryRes = qry.all()
        resIds = []
        resDict = {}
        for r in qryRes:
            timeframe = r[1]
            r = dict(r[0])
            resIds.append(r['strategy_id'])
            resDict[r['strategy_id']] = r
            r['timeframe'] = timeframe
            del r['marketorder_id'], r['stoploss_id'], r['takeprofit_id'], r['strategyrun_id'], r['group_id'], r['req_price']
            if r["state"] != TRADE_CLOSED:
                r["profit"] = r["floating_profit"]
            else:
                del r["floating_profit"]
            r["state"] = TRADE_STR[r["state"]]
            r["direction"] = DIR_STR[r["direction"]]            
            if r["close_price"] == 0.0:
                del r["close_price"]
            if r["close_type"]:
                r["close_type"] = ORDERTYPE_STR[r["close_type"]]
            else:
                r["close_type"] = ''
            res.append(r)
        if id2:
            for r in session.query(EntityName.element_id, EntityName.element_name).filter(EntityName.element_id.in_(resIds)).all():
                resDict[r[0]]['strategy_name'] = r[1]
        if limit:
            return True, OK, res, qry2.scalar()
        else:
            return True, OK, res
    except:
        errorlog.error('Exception raised during personal account list', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()
