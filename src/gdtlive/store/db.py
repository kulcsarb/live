# -*- encoding: utf-8 -*-
'''
Created on Jul 9, 2012

@author: gdtlive
'''
from sqlalchemy.pool import QueuePool, NullPool, SingletonThreadPool, StaticPool
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float, Time, Boolean, DateTime, Date, Text, create_engine, ForeignKey, Table, Sequence, LargeBinary, MetaData
from sqlalchemy.orm import sessionmaker, Session, relationship
from sqlalchemy.schema import UniqueConstraint
from sqlalchemy.exc import OperationalError
from sqlalchemy.sql.expression import bindparam
from gdtlive.utils import hashed, date2str, str2date, R, R2, first0, OrderedDict
from datetime import datetime, date, timedelta
from gdtlive.config import SQL_USER, SQL_PASS, SQL_URL
import logging
from logging.handlers import RotatingFileHandler
from gdtlive.constants import PERFORMANCE_CURRENCY
from gdtlive.core.constants import STOPPED
from gdtlive.admin.system.log import LOG_FORMAT
from gdtlive.config import POSTLOAD_DAYS

log = logging.getLogger('gdtlive')


new_db_created = False
session = None
engine = None
Session = None

def init():
    global session, Session, engine, new_db_created
    
    #sqllog = logging.getLogger('sqlalchemy')
    #sqllog.setLevel(logging.DEBUG)
    #handler = RotatingFileHandler('/opt/sql.log','a',10*1024*1024, 10)
    #handler.setFormatter(logging.Formatter(LOG_FORMAT))    
    #sqllog.addHandler(handler)
    
    
    new_db_created = False    
    log.info('connecting to %s:%s@%s' % (SQL_USER, SQL_PASS, SQL_URL))            
    engine = create_engine('postgresql+psycopg2://%s:%s@%s' % (SQL_USER, SQL_PASS, SQL_URL), poolclass = NullPool, echo = False)        
    
    metadata = MetaData()
    try:
        metadata.create_all(engine)
    except OperationalError, e:
        if "not exist" in str(e):
            try:
                server_url, db_name = SQL_URL.split('/')
                log.info('Creating database %s....' % db_name)
                engine = create_engine('postgresql+psycopg2://%s:%s@%s/postgres' % (SQL_USER, SQL_PASS, server_url), poolclass = NullPool, echo = False)
                session = sessionmaker(bind = engine)()
                session.connection().connection.set_isolation_level(0)
                session.execute("CREATE DATABASE %s WITH OWNER = %s ENCODING = 'UTF8' CONNECTION LIMIT = -1" % (db_name, SQL_USER))
                session.connection().connection.set_isolation_level(1)
                log.info('Database created.')
                engine = create_engine('postgresql+psycopg2://%s:%s@%s' % (SQL_USER, SQL_PASS, SQL_URL), poolclass = NullPool, echo = False)
                new_db_created = True


            except Exception, e:
                log.error(str(e))
                return False
        else:
            log.error(str(e))
            return False

    Session = sessionmaker(bind = engine, expire_on_commit=False)
    metadata.create_all(engine)        
    
    User.metadata.create_all(engine)

    session = Session()
    users = session.query(User).all()
    if not users:
        U = User(username = 'admin',
                 email = 'admin@gdtlive-master.hu',
                 password = hashed('password'),
                 name = 'Administrator',
                 popup_notify = False,
                 email_notify = False,
                 drawdown_percent_notify = 0,
                 is_admin = True,
                 is_creator = True,
                 is_dbadmin = True,
                 is_viewer = True,
                 is_runner = True
                 )
        session.add(U)
    
    spreadconfigs = session.query(SpreadConfig).all()
    if not spreadconfigs:
        sc = SpreadConfig('Historical spreads', 0, 0, True)
        session.add(sc)
        sc = SpreadConfig('Custom spreads', 0, 0, True)
        session.add(sc)

    rolloverconfigs = session.query(RolloverConfig).all()
    if not rolloverconfigs:
        rc = RolloverConfig('Default')
        session.add(rc)

    historicconfigs = session.query(HistoricPluginsConfig).all()
    if not historicconfigs:
        import gdtlive.historic.plugin.dukascopy_fs as dukascopy
        dukascopy.initialize_historic_configs()



    session.commit()
    return True


def getAttrNames(base):
    return [c.name for c in base.__table__.columns]


def todict(self):
    def convert_datetime(value):
        if value:
            return value.strftime("%Y-%m-%d %H:%M:%S")
        else:
            return value

    d = {}
    for c in self.__table__.columns:
        if isinstance(c.type, DateTime):
            value = convert_datetime(getattr(self, c.name))
        else:
            value = getattr(self, c.name)

        yield(c.name, value)

def iterfunc(self):
    """Returns an iterable that supports .next()
        so we can do dict(sa_instance)
 
    """
    return self.todict()

Base = declarative_base()
Base.todict = todict
Base.__iter__ = iterfunc



class Account(Base):
    '''
    Éles számlabeállítások tárolására szolgáló perzisztens osztály
    
    @author: Elekes Árpád
    '''
    __tablename__ = 'account'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True)
    '''
    @ivar: A rekord egyedi azonosítója
    @type: int
    '''
    user_id = Column(Integer)
    '''
    @ivar: A felhasználó azonosítója
    @type: int
    '''
    broker_id = Column(Integer)
    '''
    @ivar: A bróker egyedi azonosítója, megfelel egy bróker plugin Id-jének
    @type: int
    '''
    name = Column(String)    
    '''
    @ivar: A felhasználó által adott egyedi elnevezés a konfiguráció számára
    @type: string
    '''    
    account_number = Column(String)   #GUIRA!, nem kotelezo
    '''
    @ivar: A brókerszámla neve, vagy azonositója, amennyiben a bróker ad ilyet
    @type: string
    '''        
    username = Column(String)   # nem kotelezo
    '''
    @ivar: Felhasználónév a konfigurált számlához 
    @type: string
    '''
    password = Column(String)
    '''
    @ivar: Jelszó a konfigurált számlához
    @type: string
    '''
    api = Column(Integer)   # kijon
    '''
    @ivar: JForex: 0, FIX: 1
    @type: int
    '''    
    sendercompid = Column(String)
    '''
    @ivar: e?
    @type: string
    '''
    targetcompid = Column(String)
    '''
    @ivar: e?
    @type: string
    '''
    server_url = Column(String)   #GUI
    '''
    @ivar: A FIX szerver dns neve, vagy IP-je
    @type: string
    '''
    server_port = Column(Integer)   #GUI
    '''
    @ivar: A FIX szerver url-je
    @type: string
    '''
    use_ssl = Column(Boolean)   #GUI
    '''
    @ivar: Kell-e SSL a kapcsolódáshoz ?
    @type: boolean
    '''        
    
    online = Column(Boolean)
    
    base_equity = Column(Float)   #GUI
    
    leverage = Column(Integer)    #GUI
    
    risk_multiplier = Column(Float)
    
    benchmark = Column(Boolean)
    
    balance = Column(Float)
    
    
    def __init__(self, broker_id, name, username, password, sendercompid, targetcompid, base_equity, balance, leverage, account_number, server_url, server_port, use_ssl, user_id = None, benchmark=False, risk_multiplier=1):
        '''
        Konstruktor
        '''
        self.user_id = user_id
        self.broker_id = broker_id
        self.name = name
        self.username = username
        self.password = password
        self.sendercompid = sendercompid
        self.targetcompid = targetcompid
        self.online = False
        self.base_equity = base_equity
        self.balance = balance
        self.leverage = leverage
        self.account_number = account_number
        self.server_url = server_url
        self.server_port = server_port
        self.use_ssl = use_ssl
        self.risk_multiplier = risk_multiplier
        self.benchmark = benchmark
        


class User(Base):
    '''
    A felhasználói adatok tárolására szolgáló perzisztens osztály
    
    @author: Elekes Árpád
    '''
    __tablename__ = 'users'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True)
    '''
    @ivar: Egyedi azonosító
    @type: int
    '''
    username = Column(String)
    '''
    @ivar: Felhasználónév
    @type: string
    '''
    email = Column(String)
    '''
    @ivar: Email cím
    @type: string
    '''
    password = Column(String)
    '''
    @ivar: Jelszó hash
    @type: string
    '''
    name = Column(String)
    '''
    @ivar: Név
    @type: string
    '''
    popup_notify = Column(Boolean)
    '''
    @ivar: Értesítés feladat befejeződéséről felugró ablakon keresztül
    @type: boolean
    '''
    email_notify = Column(Boolean)
    '''
    @ivar: Értesítés feladat befejeződéséről e-mailen keresztül
    @type: boolean
    '''
    drawdown_percent_notify = Column(Integer)
    '''
    @ivar: Az éles számlán ekkora % veszteség elérése után értesítése a felhasználót
    @type: int
    '''
    is_admin = Column(Boolean)
    '''
    @ivar: Adminisztrátori jogosultság
    @type: boolean
    '''
    is_creator = Column(Boolean)
    '''
    @ivar: Kreátori jogosultság
    @type: boolean
    '''
    is_dbadmin = Column(Boolean)
    '''
    @ivar: Adatbázis adminisztrátori jogosultság
    @type: boolean
    '''
    is_viewer = Column(Boolean)
    '''
    @ivar: Megtekintői jogosultság
    @type: boolean
    '''
    is_runner = Column(Boolean)
    '''
    @ivar: Éles futtatói jogosultság
    @type: boolean
    '''

    live_client_url = Column(String)

    def __init__(self, username, email, password, name = '', popup_notify = False, email_notify = False, drawdown_percent_notify = 0, is_admin = False, is_creator = False, is_dbadmin = False, is_viewer = False, is_runner = False):
        '''
        Konstruktor
        '''
        self.username = username
        self.name = name
        self.email = email
        self.password = password
        self.popup_notify = popup_notify
        self.email_notify = email_notify
        self.drawdown_percent_notify = drawdown_percent_notify
        self.is_admin = is_admin
        self.is_creator = is_creator
        self.is_dbadmin = is_dbadmin
        self.is_viewer = is_viewer
        self.is_runner = is_runner

    def __repr__(self):
        return 'User(%d, %s, %s, %s)' % (self.id, self.username, self.name, self.email)


class RenewPassword(Base):
    '''Jelszómegújitási kérelmek tárolására szolgáló perzisztens osztály
        
    @author: Elekes Árpád
    
    '''
    __tablename__ = 'renewPasswords'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    renewId = Column(String, primary_key = True)
    '''
    @ivar: Egyedi azonosító
    @type: string
    '''
    userId = Column(Integer, ForeignKey('users.id')) # 1..1 -> USERS.id
    '''
    @ivar: Felhasználó azonosítója
    @type: int
    '''
    time = Column(DateTime)
    '''
    @ivar: A jelszó kérésének időpontja
    @type: int
    '''
    def __init__(self, renewId, userId, time):
        '''
        Constructor
        '''
        self.renewId = renewId
        self.userId = userId
        self.time = time


class Message(Base):
    '''
    A felhasználó értesítésére szolgáló perzisztens osztály
    '''
    __tablename__ = 'messages'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True)
    '''
    @ivar: A rekord egyedi azonosítója
    @type: int
    '''
    userId = Column(Integer)
    '''
    @ivar: A felhasználó azonosítója
    @type: int
    '''
    message = Column(String)
    '''
    @ivar: Az üzenet szövege
    @type: string
    '''
    time = Column(DateTime)
    '''
    @ivar: A művelet kezdete
    @type: datetime
    '''
    def __init__(self, userId, message):
        '''
        Konstruktor
        '''
        self.userId = userId
        self.message = message
        self.time = datetime.now()


backtestconfig_datarow_association_table = Table('backtestconfig_datarow_association', Base.metadata,
    Column('backtestConfigId', Integer, ForeignKey('backtestConfigs.id')),
    Column('datarowId', Integer, ForeignKey('datarowDescriptors.id'))
)


class BacktestConfig(Base):
    '''
    A backtest konfigurációk tárolására szolgáló perzisztens osztály
    
    @author: Elekes Árpád
    '''
    __tablename__ = 'backtestConfigs'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True)
    '''
    @ivar: Egyedi azonosító
    @type: int
    '''
    strategies = Column(String)
    '''
    @ivar: python lista a stratégiák Id-jével, stringgé konvertálva
    @type: string
    '''
    from_date = Column(DateTime)
    '''
    @ivar: historic data start time
    @type: datetime
    '''
    to_date = Column(DateTime)
    '''
    @ivar: historic data end time
    @type: datetime
    '''
    perf_from_date = Column(DateTime)
    '''
    @ivar: performance start time, >= from_date
    @type: datetime
    '''
    perf_to_date = Column(DateTime)
    '''
    @ivar: performance end time, <= to_date
    @type: datetime
    '''
    spreadconfigId = Column(Integer)
    '''
    @ivar: 
    @type: int
    '''
    rolloverconfigId = Column(Integer)
    '''
    @ivar: 
    @type: int
    '''
    mmpluginId = Column(Integer)
    '''
    @ivar: 
    @type: int
    '''
    mmplugin_parameters = Column(String)
    '''
    @ivar: python tömb stringgé konvertálva
    @type: string
    '''
    stratpluginId = Column(Integer)
    '''
    @ivar: A strategy plugin azonosítója
    @type: int
    '''
    accpluginId = Column(Integer)
    '''
    @ivar: Az account plugin azonosítója
    @type: int
    '''
    base_equity = Column(Integer)
    '''
    @ivar: 
    @type: int
    '''
    leverage = Column(Integer)
    '''
    @ivar: 
    @type: int
    '''
    slippage = Column(Float)
    '''
    @ivar: A gyertya nyitó ára
    @type: float
    '''
    failed_trades = Column(Float)
    '''
    @ivar: A gyertya nyitó ára
    @type: float
    '''
    starting_time = Column(DateTime)
    '''
    @ivar: Kezdés Időpontja 
    @type: datetime
    '''
    total_count = Column(Integer)
    '''
    @ivar: a stratégiák darabszáma
    @type: int
    '''
    tested_count = Column(Integer)
    '''
    @ivar: a letesztelt stratégiák darabszáma
    @type: int
    '''
    datarows = relationship("DatarowDescriptor",
                    secondary = backtestconfig_datarow_association_table,
                    backref = "backtest_configs")

    '''N-M kapcsolatot megvalósitó SqlAlchemy attributum a DatarowDescriptorok felé. 
    Ezen az attribbutumon keresztül lehet hozzáadni és elérni a konfighoz kapcsolt adatsorokat.'''

    def __init__(self, strategies, from_date, to_date, spreadconfigId, rolloverconfigId, mmpluginId, mmplugin_parameters, base_equity,
                 leverage, slippage, failed_trades, starting_time, total_count, tested_count, perf_from_date = None, perf_to_date = None, stratpluginId = 1, accpluginId = 1):#,backtestId):
        '''
        Konstruktor
        '''
        self.strategies = strategies
        self.from_date = from_date
        self.to_date = to_date
        self.perf_from_date = perf_from_date if perf_from_date else from_date
        self.perf_to_date = perf_to_date if perf_to_date else to_date + timedelta(days = POSTLOAD_DAYS)
        self.spreadconfigId = spreadconfigId
        self.rolloverconfigId = rolloverconfigId
        self.mmpluginId = mmpluginId
        self.mmplugin_parameters = mmplugin_parameters
        self.stratpluginId = stratpluginId
        self.accpluginId = accpluginId
        self.base_equity = base_equity
        self.leverage = leverage
        self.slippage = slippage
        self.failed_trades = failed_trades
        self.starting_time = starting_time
        self.total_count = total_count
        self.tested_count = tested_count
#        self.backtestId = backtestId


liverunconfig_datarow_association_table = Table('liverunconfig_datarow_association', Base.metadata,
    Column('liveRunConfigId', Integer, ForeignKey('liveRunConfigs.id')),
    Column('datarowId', Integer, ForeignKey('datarowDescriptors.id'))
)



class LiveRunConfig(Base):
    '''
    Az éles futások konfigurációit tároló osztály
    
    @author: Elekes Árpád
    '''
    __tablename__ = 'liveRunConfigs'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True)
    '''
    @ivar: A rekord egyedi azonosítója
    @type: int
    '''
    user_id = Column(Integer)
    '''
    @ivar: A felhasználó azonosítója 
    @type: int
    '''
    account_id = Column(Integer)  #-> Account.id

    accountrun_id = Column(Integer)
            
    portfstratrun_id = Column(Integer)
    
    strategy_id = Column(Integer)   #  STRATEGIES.id
    '''
    @ivar: A stratégia egyedi azonosítója
    @type: int
    '''    
    running_from = Column(DateTime) #mutatnak az adatsorok első      
    '''
    @ivar: A futás kezdete
    @type: datetime
    '''
    running_to = Column(DateTime) #és utolsó bejegyzésére 


    status_id = Column(Integer)
    '''
    @ivar: A státusz azonosítója running / stopped / aborted / waiting to stop / waiting to abort / fucked up
    @type: int
    '''
    status_message = Column(String)
    '''
    @ivar: A status_message
    @type: string
    '''    

    timeframe = Column(Integer)

    symbols = Column(String)

    opentrades = Column(Integer)

    floating_profit = Column(Float)

    net_profit = Column(Float)

    use_of_leverage = Column(Float)
    
    drawdown = Column(Float)
    
    mmplugin_id = Column(Integer)

    mmplugin_parameters = Column(String)
    
    balance_at_start = Column(Float)
    
    risk_multiplier = Column(Float)

    datarows = relationship("DatarowDescriptor",
                    secondary = liverunconfig_datarow_association_table,
                    backref = "liverun_configs")


    def __init__(self, userid, mmplugin_id, mmplugin_parameters, account_id, accountrun_id, portfstratrun_id, strategyid, timeframe, balance, risk):        
        self.user_id = userid
        self.mmplugin_id = mmplugin_id
        self.mmplugin_parameters = mmplugin_parameters
        self.account_id = account_id        
        self.accountrun_id = accountrun_id
        self.portfstratrun_id = portfstratrun_id                
        self.strategy_id = strategyid        
        self.timeframe = timeframe
        self.balance_at_start = balance
        self.risk_multiplier = risk



class SpreadConfig(Base):
    '''
    Spread és jutalék konfigurációk tárolására szolgáló perzisztens osztály
    
    @author: Elekes Árpád
    '''
    __tablename__ = 'spreadConfigs'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True)
    '''
    @ivar: Egyedi azonosító
    @type: int
    '''
    name = Column(String)
    '''
    @ivar: Megnevezés
    @type: string
    '''
    default_commission = Column(Float)
    '''
    @ivar: Alapértelmezett jutalék
    @type: float
    '''
    default_spread = Column(Float)
    '''
    @ivar: Alapértelmezett spread
    @type: float
    '''
    commission_in_pips = Column(Boolean)
    '''
    @ivar: Jutalék pipben van-e (false: devisában)
    @type: boolean
    '''
    rows = relationship("SpreadConfigsRow", backref = "spreadConfigs", cascade = 'all, delete, delete-orphan')

    def __init__(self, name, default_commission, default_spread, commission_in_pips):
        '''
        Konstruktor
        '''
        self.name = name
        self.default_commission = default_commission
        self.default_spread = default_spread
        self.commission_in_pips = commission_in_pips

    def __repr__(self):
        result = "ID:%d '%s' spread: %.1f, commission: %.1f" % (self.id, self.name, self.default_spread, self.default_commission)
        for row in self.rows:
            result += '\n\t%r' % row
        return result

class SpreadConfigsRow(Base):
    '''
    Spread és jutalék konfigurációkhoz kapcsolódó instrumentum beállítások tárolására szolgáló perzisztens osztály
    
    @author: Elekes Árpád
    '''
    __tablename__ = 'spreadConfigsRows'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True)
    '''
    @ivar: Egyedi azonosító
    @type: int
    '''
    configId = Column(Integer, ForeignKey('spreadConfigs.id'))    # *..1 SPREAD_CONFIGS.id
    '''
    @ivar: Kapcsolat a szülő L{SpreadConfig} osztályhoz
    @type: int
    '''
    symbol = Column(String)
    '''
    @ivar: Instrumentum neve
    @type: string
    '''
    spread = Column(Float)
    '''
    @ivar: Instrumentum alapértelmezett spreadje
    @type: float
    '''
    exception_from = Column(Integer)     # HH formátum
    '''
    @ivar: Kivételes időszak kezdete, órában megadva
    @type: int
    '''
    exception_to = Column(Integer)     # HH formátum
    '''
    @ivar: Kivételes időszak vége, órában megadva
    @type: int
    '''
    exception_spread = Column(Float)
    '''
    @ivar: Kivételes időszakra értelmezett spread
    @type: float
    '''
    def __init__(self, symbol, spread, exception_from, exception_to, exception_spread, configId = 0):
        '''
        Konstruktor
        '''
        self.configId = configId
        self.symbol = symbol
        self.spread = spread
        if exception_spread:
            self.exception_spread = exception_spread
            self.exception_from = exception_from
            self.exception_to = exception_to

    def __repr__(self):
        result = '%s: %.1f' % (self.symbol, self.spread)
        if self.exception_spread > 0:
            result += ' (%dh -> %dh: %.1f)' % (self.exception_from, self.exception_to, self.exception_spread)
        return result

class RolloverConfig(Base):
    '''
    A rollover konfigurációk tárolására szolgáló perzisztens osztály
    
    @author: Elekes Árpád
    '''
    __tablename__ = 'rolloverConfigs'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True)
    '''
    @ivar: Egyedi azonosító
    @type: int
    '''
    name = Column(String)
    '''
    @ivar: Megnevezés
    @type: string
    '''

    rows = relationship("RolloverConfigRow", backref = "rolloverConfigs", cascade = 'all, delete, delete-orphan')

    def __init__(self, name):
        '''
        Konstruktor
        '''
        self.name = name


class RolloverConfigRow(Base):
    '''
    A rollover konfigurációkhoz tartozó pénznemek alapkamatainak tárolására szolgáló perzisztens osztály
    
    @author: Elekes Árpád
    '''
    __tablename__ = 'rolloverConfigRows'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True)
    '''
    @ivar: Egyedi azonosító
    @type: int
    '''
    configId = Column(Integer, ForeignKey('rolloverConfigs.id'))     #  N..1 -> ROLLOVER_CONFIG.id
    '''
    @ivar: Kapcsolat a szülő L{RolloverConfig} rekordhoz 
    @type: int
    '''
    currency = Column(String)
    '''
    @ivar: A pénznem megnevezése
    @type: string
    '''
    interest = Column(Float)
    '''
    @ivar: A pénznem alapkamata
    @type: float
    '''
    def __init__(self, currency, interest):
        '''
        Konstruktor
        '''
        self.currency = currency
        self.interest = interest


class HistoricPluginsConfig(Base):
    '''
    Historikus adatforrások konfigurációinak tárolására szolgáló perzisztens osztály
    
    @author: Elekes Árpád
    '''
    __tablename__ = 'historicPluginsConfigs'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True)
    '''
    @ivar: Egyedi azonosító
    @type: int
    '''
    configName = Column(String)
    '''
    @ivar: konfiguráció neve 
    @type: string
    '''
    pluginId = Column(Integer)
    '''
    @ivar: referencia a historikus plugin osztály beépitett id-jére 
    @type: int
    '''
    username = Column(String)
    '''
    @ivar: Felhasználónév
    @type: string
    '''
    password = Column(String)
    '''
    @ivar: Jelszó
    @type: string
    '''

    rows = relationship("HistoricPluginsConfigsInstrument", backref = "historicPluginsConfigs", cascade = 'all, delete, delete-orphan')

    def __init__(self, configName, pluginId, username = "", password = ""):
        '''
        Konstruktor
        '''
        self.configName = configName
        self.pluginId = pluginId
        self.username = username
        self.password = password


class HistoricPluginsConfigsInstrument(Base):
    '''
    Historikus adatforrás konfigurációk kapcsolódó instrumentum listájának tárolására szolgáló perzisztens osztály
    
    @author: Elekes Árpád
    '''
    __tablename__ = 'historicPluginsConfigsInstruments'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True)
    '''
    @ivar: Egyedi azonosító
    @type: int
    '''
    configId = Column(Integer, ForeignKey("historicPluginsConfigs.id")) # *..1 HISTORIC_PLUGINS_CONFIGS.id
    '''
    @ivar: Referencia a historicPluginsConfigs.id mezőre
    @type: int
    '''
    symbol = Column(String)
    '''
    @ivar: Instrumentum azonosító
    @type: string
    '''
    timeframe = Column(Integer)
    '''
    @ivar: Timeframe azonosító
    @type: string
    '''
    available_from = Column(Date)
    '''
    @ivar: Az elérhetőség kezdő dátuma a szolgáltatónál
    @type: date
    '''
    def __init__(self, symbol, timeframe, available_from):#,configId=0):
        '''
        Konstruktor
        '''
#        self.configId = configId     
        self.symbol = symbol
        self.timeframe = timeframe
        self.available_from = available_from


class DatarowDescriptor(Base):
    '''
    Historikus adatsorok jellemzőit tároló perzisztens osztály
    @author: Elekes Árpád
    '''
    __tablename__ = 'datarowDescriptors'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True, index = True)
    '''
    @ivar: Egyedi azonosító
    @type: int
    '''
    pluginId = Column(Integer)
    '''
    @ivar: referencia a historikus plugin osztály beépitett id-jére 
    @type: int
    '''
    #configId = Column(Integer, ForeignKey("historicPluginsConfigs.id")) #  HISTORIC_PLUGINS_CONFIGS.id
    configId = Column(Integer)
    '''
    @ivar: Referencia a historicPluginsConfigs.id mezőre
    @type: int
    '''
    #configRowId = Column(Integer, ForeignKey("historicPluginsConfigsInstruments.id")) #  HISTORIC_PLUGINS_CONFIGS.id
    configRowId = Column(Integer)
    '''
    @ivar: Referencia a historicPluginsConfigsInstrument.id mezőre
    @type: int
    '''
    symbol = Column(String, index = True)
    '''
    @ivar: Instrumentum név
    @type: string
    '''
    timeframe_num = Column(Integer, index = True)
    '''
    @ivar: Időtáv azonosító percekben kifejezve (1 = M1, 5=M5, ....)
    @type: int
    '''
    download_date = Column(DateTime)
    '''
    @ivar: Letöltés időpontja
    @type: datetime
    '''
    from_date = Column(DateTime)
    '''
    @ivar: Legkorábbi letöltött árfolyamadat időpontja 
    @type: datetime
    '''
    to_date = Column(DateTime)
    '''
    @ivar: Legutolsó letöltött árfolyamadat időpontja
    @type: datetime
    '''
    price_type = Column(Integer)   #  
    '''
    @ivar: Az adatsor által tartalmazott ár típusa (0:ask, 1:bid, 2:middle) 
    @type: int
    '''
    status = Column(String)
    '''
    @ivar: A legutolsó letöltési művelet állapota 
    @type: str
    '''
    status_text = Column(Text)
    '''
    @ivar: A legutolsó letöltési művelet naplója 
    @type: string
    '''
    bar_count = Column(Integer)
    '''
    @ivar: Az összes, a descriptorhoz tartozó gyertya száma 
    @type: int 
    '''
    invalid_count = Column(Integer)
    '''
    @ivar: A hibás gyertyák száma 
    @type: int
    '''
    def __init__(self, pluginId, configId, configRowId, symbol, timeframe_num, from_date, to_date, download_date = None, price_type = 0, status = '', status_text = '', bar_count = 0, invalid_count = 0):
        '''
        Konstruktor
        '''
        self.pluginId = pluginId
        self.configId = configId
        self.configRowId = configRowId
        self.symbol = symbol
        self.timeframe_num = timeframe_num
        self.download_date = download_date
        self.from_date = from_date
        self.to_date = to_date
        self.price_type = price_type
        self.status = status
        self.status_text = status_text
        self.bar_count = bar_count
        self.invalid_count = invalid_count


class DatarowData(Base):
    '''
    Historikus adatsorok árfolyamadatait tároló perzisztens osztály
    @author: Elekes Árpád
    '''
    __tablename__ = 'DatarowData'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True)
    '''
    @ivar: Egyedi azonosító
    @type: int
    '''
    datarowId = Column(Integer, ForeignKey("datarowDescriptors.id"))   #  N..1 DATAROW_DESCRIPTORS.id
    '''
    @ivar: Referencia a DatarowDescriptor.id mezőre
    @type: int
    '''
    ask_open = Column(Float)
    '''
    @ivar: ASK gyertya nyitó ára
    @type: float
    '''
    ask_high = Column(Float)
    '''
    @ivar: ASK gyertya legmagasabb árponja
    @type: float
    '''
    ask_low = Column(Float)
    '''
    @ivar: ASK gyertya legalacsonyabb árpontja
    @type: float
    '''
    ask_close = Column(Float)
    '''
    @ivar: ASK gyertya záró ára
    @type: float
    '''
    bid_open = Column(Float)
    '''
    @ivar: BID gyertya nyitó ára
    @type: float
    '''
    bid_high = Column(Float)
    '''
    @ivar: BID gyertya legmagasabb árponja
    @type: float
    '''
    bid_low = Column(Float)
    '''
    @ivar: BID gyertya legalacsonyabb árpontja
    @type: float
    '''
    bid_close = Column(Float)
    '''
    @ivar: BID gyertya záró ára
    @type: float
    '''
    volume = Column(Integer)
    '''
    @ivar: A gyertya ideje alatt lévő forgalom
    @type: int
    '''
    time = Column(DateTime)
    '''
    @ivar: A gyertya nyitásának időpontja
    @type: datetime
    '''
    invalid = Column(Boolean)
    '''
    @ivar: A gyertyában tárolt adatok helyesek-e?
    @type: boolean
    '''

    def __init__(self, datarowId, open, high, low, close, time, volume, invalid = False):
        '''
        Konstruktor
        '''
        self.datarowId = datarowId
        self.ask_open = open
        self.ask_high = high
        self.ask_low = low
        self.ask_close = close
        self.bid_open = 0.0
        self.bid_high = 0.0
        self.bid_low = 0.0
        self.bid_close = 0.0
        self.volume = volume
        self.time = time
        self.invalid = invalid

    def __repr__(self):
        return str(self.time) + " (%.4f, %.4f, %.4f, %.4f) (%.4f, %.4f, %.4f, %.4f) %d" % \
                    (self.ask_open, self.ask_high, self.ask_low, self.ask_close,
                     self.bid_open, self.bid_high, self.bid_low, self.bid_close,
                     self.volume)

    def as_tohlc(self):
        # return {'time': self.time.strftime('%Y-%m-%d %H:%M:%S'), 'open': self.open, 'high': round(self.high,4), 'low': round(self.low,4), 'close': round(self.close,4)}
        return [self.time.strftime('%Y-%m-%d %H:%M:%S'), self.ask_open, self.ask_high, self.ask_low, self.ask_close]

class Strategy(Base):
    '''
    A stratégiákat tároló osztály
    
    @author: Elekes Árpád
    '''
    __tablename__ = 'strategys'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True, index = True)
    '''
    @ivar: A rekord egyedi azonosítója
    @type: int
    '''
    dns = Column(String)
    '''
    @ivar: A stratégia DNS-e kódolt formában
    @type: string
    '''
    timeframe = Column(Integer)
    symbol = Column(String)
    mmplugin_id = Column(Integer)
    mmplugin_parameters = Column(String)
    deleted = Column(Boolean)
    deletion_reason = Column(String)
    evolved_from = Column(DateTime)
    evolved_to = Column(DateTime)
    individual = Column(String)
    weekend_start = Column(Integer)
    stratplugin_id = Column(Integer)
    description = Column(Text)
    
    def __init__(self, dns, timeframe, symbol, mmplugin_id, mmplugin_parameters):
        '''
        Constructor
        '''        
        self.dns = dns
        self.timeframe = timeframe
        self.symbol = symbol
        self.mmplugin_id = mmplugin_id
        self.mmplugin_parameters = mmplugin_parameters
        self.deleted = False
        self.weekend_start = 0




class EntityName(Base):
    '''
    Az éles futások konfigurációit tároló osztály
    
    @author: Elekes Árpád
    '''
    __tablename__ = 'entityNames'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True, index = True)
    '''
    @ivar: A rekord egyedi azonosítója
    @type: int
    '''
    type = Column(Integer, index = True)
    '''
    @ivar: A típus gdtlive.constants.ELEMENT_TYPE -ban található
    @type: int
    '''
    element_id = Column(Integer, index = True)
    '''
    @ivar: A referred entity azonosítója
    @type: int
    '''
    element_name = Column(String, index = True)
    '''
    @ivar: Az element_name
    @type: string
    '''
    def __init__(self, type, element_id, element_name):
        '''
        Constructor
        '''
        self.type = type
        self.element_id = element_id
        self.element_name = element_name



class StrategyRun(Base):
    '''
    A stratégiák lefutásait tároló osztály
    
    @author: Elekes Árpád
    '''
    __tablename__ = 'strategyRuns'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True, index = True)
    '''
    @ivar: A rekord egyedi azonosítója
    @type: int
    '''
    strategyId = Column(Integer, index = True) #N..1 -> STRATEGIES.ID
    '''
    @ivar: A stratégia azonosítója
    @type: int
    '''
    type = Column(Integer, index = True)
    '''
    @ivar: A típus (0,1,2) evol/backtest/live
    @type: int
    '''
    time = Column(DateTime, index = True)
    '''
    @ivar: A művelet kezdete
    @type: datetime
    '''
    performanceIdPoints = Column(Integer)    #f1..1 PERFORMANCE_DATA.ID
    '''
    @ivar: A performance azonosítója
    @type: int
    '''
    performanceIdCurrency = Column(Integer, index = True)    #f1..1 PERFORMANCE_DATA.ID
    '''
    @ivar: A performance azonosítója
    @type: int
    '''

    performanceSerialId = Column(Integer)    #f1..1 PerformanceStrategySerialData.id
    '''
    @ivar: A performance azonosítója
    @type: int
    '''
    configId = Column(Integer)
    '''
    @ivar: Ami vagy egy backtestconfig vagy liverunconfig
    @type: int
    '''
    orderlog = Column(Text)

    tradelog = Column(Text)

    def __init__(self, strategyId, type, time, points_id, currency_id, serial_id, configId):
        '''
        Constructor
        '''
        self.strategyId = strategyId
        self.type = type
        self.time = datetime.now()
        if time:
            self.time = time
        self.performanceIdCurrency = currency_id
        self.performanceIdPoints = points_id
        self.performanceSerialId = serial_id
        self.configId = configId



class AccountPerformance(Base):
    __tablename__ = 'account_performance'
    id = Column(Integer, primary_key = True)
    account_id = Column(Integer)
    accountrun_id = Column(Integer)
    timestamp = Column(DateTime)
    equity = Column(Float)
    balance = Column(Float)
    floating = Column(Float)    
    trade_num = Column(Integer)
    leverage = Column(Float)    
    use_of_leverage = Column(Float)
    usable_margin = Column(Float)
        
    def __init__(self, accountid, timestamp, equity, balance, floating, trade_num, leverage, use_of_leverage, usable_margin):
        self.account_id = accountid
        self.timestamp = timestamp
        self.equity = equity
        self.balance = balance
        self.trade_num = trade_num
        self.floating = floating
        self.leverage = leverage
        self.use_of_leverage = use_of_leverage
        self.usable_margin = usable_margin
        
    

class PerformanceData(Base):
    '''
    Az  tároló osztály
    
    @author: Elekes Árpád
    '''
    __tablename__ = 'performanceDatas'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, Sequence('performacedb_sequence'), primary_key = True, index = True)  # 
    '''
    @ivar: A rekord egyedi azonosítója
    @type: int
    '''
    type = Column(Integer)
    '''
    @ivar: A rekord típusa, 1 = PERFORMANCE_POINTS,  2 = PERFORMANCE_CURRENCY
    @type: int
    '''    
    starting_balance = Column(Float)
    '''
    @ivar: A számitás alapját képező nyitóegyenleg
    @type: float
    '''    
    net_profit = Column(Float, index = True)
    '''
    @ivar: A teljes netto profit
    @type: float
    '''
    gross_profit = Column(Float)
    ''' 
    @ivar: A profit
    @type: float
    '''
    gross_loss = Column(Float)
    '''
    @ivar: Gross_loss
    @type: float
    '''
    cumulated_profit = Column(Float)
    '''
    @ivar: Cumulated_profit
    @type: float
    '''
    commission = Column(Float)
    '''
    @ivar: Commission
    @type: float
    '''
    profit_factor = Column(Float)
    '''
    @ivar: Profit_factor
    @type: float
    '''
    maximum_drawdown = Column(Float, index = True)
    '''
    @ivar: max_drawdown
    @type: float
    '''
    sharpe_ratio = Column(Float)
    '''
    @ivar: Sharpe_ratio
    @type: float
    '''
    number_of_trades = Column(Integer)
    '''
    @ivar: A Total_num_of_trades
    @type: int
    '''
    winning_trades = Column(Integer)

    losing_trades = Column(Integer)

    average_trade = Column(Float)

    percent_profitable = Column(Float)
    '''
    @ivar: average_trade
    @type: float
    '''
    average_winner = Column(Float)
    '''
    @ivar: average_winner
    @type: float
    '''
    average_loser = Column(Float)
    '''
    @ivar: average_loser
    @type: float
    '''
    average_wl_ratio = Column(Float)
    '''
    @ivar: Ratio_avgwin/avgloss
    @type: float
    '''
    max_conseq_winners = Column(Integer)
    '''
    @ivar: A max_conseq_winners
    @type: int
    '''
    max_conseq_losers = Column(Integer)
    '''
    @ivar: A max_conseq_losers
    @type: int
    '''
    largest_winner = Column(Float)
    '''
    @ivar: Largest_winning_trades
    @type: float
    '''
    largest_loser = Column(Float)
    '''
    @ivar: Instrumentum alapértelmezett spreadje
    @type: float
    '''
    trades_per_day = Column(Float)
    '''
    @ivar: Instrumentum alapértelmezett spreadje
    @type: float
    '''
    avg_time_in_market = Column(Float)
    '''
    @ivar: Instrumentum alapértelmezett spreadje
    @type: float
    '''
    avg_bars_in_trade = Column(Float)
    '''
    @ivar: Instrumentum alapértelmezett spreadje
    @type: float
    '''
    profit_per_month = Column(Float)
    '''
    @ivar: Instrumentum alapértelmezett spreadje
    @type: float
    '''

    avgMAE = Column(Float)
    '''
    @ivar: Instrumentum alapértelmezett spreadje
    @type: float
    '''
    avgMFE = Column(Float)
    '''
    @ivar: Instrumentum alapértelmezett spreadje
    @type: float
    '''
    fitness = Column(Float, index = True)
    '''
    @ivar: fitness
    @type: float
    '''
    profit_variance = Column(Float)
    '''
    @ivar: profit variance
    @type: float
    '''
    open_trades = Column(Float)
    '''
    @ivar: open trades number at the end of run
    @type: float
    '''
    floating_profit = Column(Float)
    '''
    @ivar: outstanding profit at the end of run
    @type: float
    '''
    min_yearly_profit_p = Column(Float)
    '''
    @ivar: minimum yearly profit in % 
    @type: float
    '''
    max_drawdown_p = Column(Float)
    '''
    @ivar: maximum drawdown relative to the actual balance in % 
    @type: float
    '''
    min_floating_p = Column(Float)
    '''
    @ivar: lowest % of floating profit relative to the account balance
    @type: float
    '''
    min_floating = Column(Float)
    '''
    @ivar: lowest floating profit during the run
    @type: float
    '''
    avg_neg_floating_p = Column(Float)
    '''
    @ivar: average negative floating profit, relative to the account balance in %  
    @type: float
    '''
    avg_pos_floating_p = Column(Float)
    '''
    @ivar: 
    @type: float
    '''
    avg_neg_floating = Column(Float)
    '''
    @ivar: 
    @type: float
    '''
    avg_pos_floating = Column(Float)
    '''
    @ivar: 
    @type: float
    '''
    max_use_of_lev = Column(Float)
    '''
    @ivar: 
    @type: float
    '''
    avg_use_of_lev = Column(Float)
    '''
    @ivar: 
    @type: float
    '''
    chance1m = Column(Float)
    '''
    @ivar: 
    @type: float
    '''
    chance3m = Column(Float)
    '''
    @ivar: 
    @type: float
    '''
    score = Column(Float)
    '''
    @ivar: 
    @type: float
    '''
    
    exposure = Column(Integer)

    def __init__(self, data_type, data):
        '''
        Constructor
        '''
        self.type = data_type
        if isinstance(data, dict):
            self.update(data)

    def clear(self):
        for c in self.__table__.columns:
            a,s,field = str(c).partition('.')
            if field not in ["type",'id']:            
                setattr(self, field, 0)                

    def update(self, data):
        changed = {}
        for field, value in data.iteritems():
            if field == 'score':
                continue
            if type(value) == float and field in ['number_of_trades', 'winning_trades', 'losing_trades', 'max_conseq_losers', 'max_conseq_winners','exposure']:
                value = int(value)
            try:
                oldvalue = getattr(self, field)
                if type(oldvalue) == float:
                    if round(oldvalue, 5) != round(value, 5) :
                        changed[field] = (oldvalue, value)
                elif oldvalue != value:
                    changed[field] = (oldvalue, value)
                setattr(self, field, value)
            except:
                pass
        return changed


    def view(self):
        prefix = '$ ' if self.type == PERFORMANCE_CURRENCY else ''
        suffix = ' %'
        def conv(field):
            try:
                return str(R2(self.__dict__[field]))
            except:
                return ''

        o = OrderedDict()
        if self.open_trades:
            o['Open trades'] = int(self.open_trades)
        if self.floating_profit:            
            o['Floating profit'] = prefix + conv('floating_profit')
        if self.exposure:
            o['Exposure'] = self.exposure
        if self.open_trades or self.floating_profit:
            o['------------------'] = ''
        
        if self.starting_balance:
            o['Starting balance'] = prefix + conv('starting_balance')
            
        o['Net profit'] = prefix + conv('net_profit')
        if self.cumulated_profit:
            o['Cumulated profit'] = prefix + conv('cumulated_profit')
        o['Gross profit'] = prefix + conv('gross_profit')
        o['Gross loss'] = prefix + conv('gross_loss')
        if self.commission:
            o['Commission'] = prefix + conv('commission')
        o['Profit factor'] = conv('profit_factor')
        if self.chance1m:
            o['Chance to win 1m'] = conv('chance1m') + suffix
        if self.chance3m:
            o['Chance to win 3m'] = conv('chance3m') + suffix
        o['Maximum drawdown $'] = prefix + conv('maximum_drawdown')
        o['Maximum drawdown %'] = conv('max_drawdown_p') + suffix
        o['Profit / month'] = conv('profit_per_month')
        #o['Profit variance'] = conv('profit_variance')
        o['Sharpe ratio'] = conv('sharpe_ratio')
        o['Average MFE'] = conv('avgMFE')
        o['Average MAE'] = conv('avgMAE')
        if self.min_floating_p:
            o['Largest negative floating profit %'] = conv('min_floating_p') + suffix
        if self.avg_neg_floating_p:
            o['Average negative floating profit %'] = conv('avg_neg_floating_p') + suffix
        if self.avg_pos_floating_p:
            o['Average positive floating profit %'] = conv('avg_pos_floating_p') + suffix
        if self.max_use_of_lev:
            o['Maximum use of leverage %'] = conv('max_use_of_lev')
        if self.avg_use_of_lev:
            o['Average use of leverage %'] = conv('avg_use_of_lev')
        o['# of trades'] = conv('number_of_trades')
        o['# of winning'] = conv('winning_trades')
        o['# of losing'] = conv('losing_trades')
        o['% profitable'] = conv('percent_profitable')        
            
        o['Max. consequtive winners'] = conv('max_conseq_winners')
        o['Max. consequtive losers'] = conv('max_conseq_losers')
        o['Largest winner'] = prefix + conv('largest_winner')
        o['Largest loser'] = prefix + conv('largest_loser')
        #o['Average trade'] = prefix + conv('average_trade')
        #o['Average winner'] = prefix + conv('average_winner')
        #o['Average loser'] = prefix + conv('average_loser')
        #o['Average W/L ratio'] = conv('average_wl_ratio')
        #o['Trades / day'] = conv('trades_per_day')
        o['Average time in market'] = conv('avg_time_in_market')
        if self.avg_bars_in_trade:
            o['Average bars in trade'] = conv('avg_bars_in_trade')
        #o['Score'] = conv('score') + suffix
        #o['Fitness'] = conv('fitness')
        #o['Minimum yearly profit'] = conv('min_yearly_profit_p') + suffix
        #o['Average negative floating profit $'] = conv('avg_neg_floating')
        #o['Average positive floating profit $'] = conv('avg_pos_floating')
        o['Largest negative floating profit $'] = conv('min_floating')
        return o



class PerformanceStrategySerialData(Base):
    '''
    Az  tároló osztály
    
    @author: Elekes Árpád
    '''
    __tablename__ = 'performanceStrategySerialDatas'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True)
    '''
    @ivar: A rekord egyedi azonosítója
    @type: int
    '''

    net_profit = Column(Text)       #net_profit := account_balance

    drawdown = Column(Text)

    trades_profit_loss = Column(Text)

    daily_profit = Column(Text)

    weekly_profit = Column(Text)

    monthly_profit = Column(Text)

    yearly_profit = Column(Text)

    MAE = Column(Text)

    MFE = Column(Text)

    floating_profit = Column(Text)

    use_of_leverage = Column(Text)

    beta = Column(Text)

    def __init__(self, net_profit, drawdown, trades_profit_loss, daily_profit, weekly_profit, monthly_profit, yearly_profit, MAE, MFE, floating_profit, use_of_leverage, beta):
        '''
        Constructor
        '''
        self.net_profit = str(net_profit)
        self.drawdown = str(drawdown)
        self.trades_profit_loss = str(trades_profit_loss)
        self.daily_profit = str(daily_profit)
        self.weekly_profit = str(weekly_profit)
        self.monthly_profit = str(monthly_profit)
        self.yearly_profit = str(yearly_profit)
        self.MAE = str(MAE)
        self.MFE = str(MFE)
        self.floating_profit = str(floating_profit)
        self.use_of_leverage = str(use_of_leverage)
        self.beta = str(beta)


    def update(self, serial_data):
        try:
            for field, value in serial_data.iteritems():                
                for i in xrange(len(value)):
                    if type(value[i]) == list:
                        for j in xrange(len(value[i])):
                            value[i][j] = R2(value[i][j])
                    else:
                        value[i] = R2(value[i])
                setattr(self, field, str(value))
        except:
            print locals()
            raise


class HttpSession(Base):
    '''
    Session információk tárolására szolgáló perzisztens osztály
    
    @author: Elekes Árpád
    '''
    __tablename__ = 'sessions'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(String, primary_key = True)
    '''
    @ivar: Session azonosító
    @type: string
    '''
    last_access_time = Column(DateTime)
    '''
    @ivar: Legutolsó hozzáférés ideje
    @type: datetime
    '''
    ip = Column(String)
    '''
    @ivar: IP cím
    @type: string 
    '''
    userId = Column(Integer) # *..1 -> USERS.id
    '''
    @ivar: Felhasználói azonosító
    @type: int
    '''
    def __init__(self, id, last_access_time, ip, userId):
        '''
        Konstruktor
        '''
        self.id = id
        self.last_access_time = last_access_time
        self.ip = ip
        self.userId = userId


class PipValue(Base):
    '''
    A pip értékek kiszámításához szükséges napi árfolyamadatok tárolására szolgáló osztály
    
    '''
    __tablename__ = 'pipValues'
    '''
    SQL tábla neve az objektumok tárolására az SQLAlchemy számára
    @type: string
    '''
    id = Column(Integer, primary_key = True)
    '''
    @ivar: A rekord egyedi azonosítója
    @type: int
    '''
    symbol = Column(String)
    '''
    @ivar: Az instrumentum neve
    @type: string
    '''
    date = Column(Date)
    '''
    @ivar: Az ár dátuma 
    @type: string
    '''
    price = Column(Float)
    '''
    @ivar: Az adott napra érvényes középárfolyam ( open + abs(open+close)/2 ) 
    @type: string
    '''


class FIXOrderHistory(Base):
    __tablename__ = 'fix_messages'
    id = Column(Integer, primary_key = True)
    trade_id = Column(Integer)
    time = Column(DateTime)
    direction = Column(Integer)
    message = Column(String)
    success = Column(Boolean)
    
    
    

#class LiveLogEntry(Base):
#
#    __tablename__ = 'livelogentry'
#
#    id = Column(Integer, primary_key = True)
#
#    configId = Column(Integer)   #N..1 -> LiveRunConfig.ID 
#
#    time = Column(DateTime)
#
#    level = Column(Integer)
#
#    module = Column(String)
#
#    funcname = Column(String)
#
#    lineno = Column(Integer)
#
#    message = Column(Text)
#
#
#

#class Notification(Base):
#    __tablename__ = 'live_notifications'    
#    id = Column(Integer, primary_key = True)
#    account_name = Column(String)
#    sending_time = Column(DateTime)
#    priority = Column(Integer)
#    text = Column(String)
#  
    
class AccountExposure(Base):
    __tablename__ = 'account_exposure'
    id = Column(Integer, primary_key = True)
    account_id = Column(Integer)
    accountrun_id = Column(Integer)
    time = Column(DateTime)
    symbol = Column(String)
    exposure = Column(Integer)
    

class StrategyExposure(Base):
    __tablename__ = 'strategy_exposure'
    id = Column(Integer, primary_key = True)
    strategy_id = Column(Integer)
    strategyrun_id = Column(Integer)
    time = Column(DateTime)
    symbol = Column(String)
    exposure = Column(Integer)
    


class MarketOrder(Base):
    
    __tablename__ = 'market_orders'
    id = Column(Integer, primary_key = True)
    trade_id = Column(Integer) 
           
    state = Column(Integer)
    symbol = Column(String) 
    amount = Column(Float)
    current_amount = Column(Float)
    
    open_direction = Column(Integer)
    open_price = Column(Float)        
    open_send_time = Column(DateTime)
    open_fill_time = Column(DateTime)
    open_clordid = Column(String)
    open_reject_count = Column(Integer)
    
    close_direction = Column(Integer)
    close_price = Column(Float)
    close_send_time = Column(DateTime)
    close_fill_time = Column(DateTime)    
    close_clordid = Column(String)
    close_reject_count = Column(Integer)
            
    correction_amount = Column(Float)    
    correction_price = Column(Float)
    correction_send_time = Column(DateTime)
    correction_fill_time = Column(DateTime)
    correction_clordid = Column(String)
    correction_reject_count = Column(Integer)
    
    _clordid = Column(Integer)
    
    
    
class Stoploss(Base):
    __tablename__ = 'stoplosses'
    id = Column(Integer, primary_key = True)
    trade_id = Column(Integer)                    
    state = Column(Integer)
    
    entry_clordid = Column(String)
    entry_orderid = Column(String)
    entry_sent_time = Column(DateTime)
    entry_price = Column(Float)
    entry_retries_count = Column(Integer)
    
    filled_amount = Column(Integer)
    fill_price = Column(Float)
    fill_time = Column(DateTime)
        
    cancel_clordid = Column(String)
    cancel_sent_time = Column(DateTime)
    canceled_time = Column(DateTime)
    cancel_retries_count = Column(Integer)
    _clordid = Column(Integer)
        

class Takeprofit(Base):
    __tablename__ = 'takeprofits'
    id = Column(Integer, primary_key = True)
    trade_id = Column(Integer)                    
    state = Column(Integer)
    
    entry_clordid = Column(String)
    entry_orderid = Column(String)
    entry_sent_time = Column(DateTime)
    entry_price = Column(Float)
    entry_retries_count = Column(Integer)
        
    filled_amount = Column(Integer)
    fill_price = Column(Float)
    fill_time = Column(DateTime)
        
    cancel_clordid = Column(String)
    cancel_sent_time = Column(DateTime)
    canceled_time = Column(DateTime)
    cancel_retries_count = Column(Integer)
    
    _clordid = Column(Integer)
    
    

class Trade(Base):
    
    __tablename__ = 'trades'    
    id = Column(Integer, primary_key = True)
    portfolio_id = Column(Integer)
    account_id = Column(Integer)    
    accountrun_id = Column(Integer)
    strategy_id = Column(Integer)
    strategyrun_id = Column(Integer)    
    group_id = Column(Integer)    
    state = Column(Integer)
    symbol = Column(String)    
    direction = Column(Integer)
    open_time = Column(DateTime)
    req_price = Column(Float)
    open_price = Column(Float)    
    amount = Column(Integer)    
    sl_price = Column(Float)
    tp_price = Column(Float)    
    close_price = Column(Float)
    close_type = Column(Integer)
    close_time = Column(DateTime)
    
    floating_profit = Column(Float)    
    profit = Column(Float)                
    MAE = Column(Float)
    MFE = Column(Float)
    
    marketorder_id = Column(Integer)
    stoploss_id = Column(Integer)
    takeprofit_id = Column(Integer)


class Portfolio(Base):
    __tablename__ = 'portfolios'
    id = Column(Integer, primary_key = True)
    name = Column(String)
    description = Column(Text)    
    management_type = Column(Integer)
    daily_profit_target = Column(Float)
    benchmarkaccount_id = Column(Integer)

    def __init__(self, name, description, management_type):
        self.name = name
        self.description = description        
        self.management_type = management_type
        self.state = STOPPED
        self.daily_profit_target = 0


class PortfolioStrategy(Base):
    __tablename__ = 'portfolio_strategies'    
    id = Column(Integer, primary_key = True)
    portfolio_id = Column(Integer)
    strategy_id = Column(Integer)
   

    def __init__(self, portfolio_id, strategy_id):
        self.portfolio_id = portfolio_id
        self.strategy_id = strategy_id


class PortfolioAccount(Base):
    __tablename__ = 'portfolio_accounts'
    id = Column(Integer, primary_key = True)
    portfolio_id = Column(Integer)
    account_id = Column(Integer)


    def __init__(self, portfolio_id, account_id):
        self.portfolio_id = portfolio_id
        self.account_id = account_id        


class PortfolioRun(Base):
    __tablename__ = 'portfolio_runs'
    id = Column(Integer, primary_key = True)
    portfolio_id = Column(Integer)
    state = Column(String)
    running_from = Column(DateTime)
    running_to = Column(DateTime)
    start_time = Column(DateTime)
    end_time = Column(DateTime) 
    
    
    def __init__(self, portfolio_id, start_time):
        self.portfolio_id = portfolio_id
        self.start_time = start_time        
        #self.state = ''
            

class PortfolioStrategyRun(Base):
    __tablename__ = 'portfolio_strategy_runs'    
    id = Column(Integer, primary_key = True)        
    portfoliorun_id = Column(Integer)
    strategy_id = Column(Integer)
    state = Column(String)
    running_from = Column(DateTime)
    running_to = Column(DateTime)
    
        
    def __init__(self, portfoliorun_id, strategy_id, running_from, running_to, state=STOPPED):
        self.portfoliorun_id = portfoliorun_id        
        self.strategy_id = strategy_id
        self.running_from = running_from
        self.running_to = running_to
        self.state = state    
        
        
                
class PortfolioAccountRun(Base):
    __tablename__ = 'portfolio_account_runs'
    id = Column(Integer, primary_key = True)
    portfolio_id = Column(Integer)
    portfoliorun_id = Column(Integer)
    account_id = Column(Integer)
    state = Column(String)
    connection_state = Column(String)
    running_from = Column(DateTime)
    running_to = Column(DateTime)
    performanceserial_id = Column(Integer)
    performance_id = Column(Integer)    
    
    def __init__(self, portfolio_id, portfoliorun_id, account_id, running_from, running_to):
        self.portfolio_id = portfolio_id        
        self.account_id = account_id
        self.portfoliorun_id = portfoliorun_id
        self.running_from = running_from
        self.running_to = running_to
        
