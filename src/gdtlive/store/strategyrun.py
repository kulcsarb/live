# -*- encoding: utf-8 -*- 
'''
Stratégia lefutásokkal kapcsolatos web serviceok

Created on 2010.12.12.

@author: kulcsarb
@newfield url: Web service, Web services
'''
import logging, calendar, ast, traceback, cherrypy
import gdtlive.store.db as db
from gdtlive.languages.default import OK, EXCEPTION_OCCURED, GUI_REFRESH_NEEDED, NOT_FOUND
from gdtlive.constants import STRATEGYRUN_BACKTEST
from gdtlive.admin.system.log import errorlog, extra
from gdtlive.control.router import ajax_wrapper
from gdtlive.store.db import StrategyRun, PerformanceData, PerformanceStrategySerialData, BacktestConfig, LiveRunConfig

log = logging.getLogger('gdtlive.store')

@cherrypy.expose
@ajax_wrapper
def perf_summary(id):
    '''
    Lekéri egy stratégia lefutás összesített performancia adatait
    
    @url: /strategyrun/perf_summary

    @type id:  int
    @param id: strategyRunId, stratégia lefutás azonosítója

    @rtype: dict
    @return: performancia adatok 
    '''
    session = db.Session()
    try:
        o = session.query(StrategyRun).get(id)#.first()
        if not o:
            return False, GUI_REFRESH_NEEDED
        pc = session.query(PerformanceData).get(o.performanceIdCurrency)
        if pc:
            return True, OK, pc.view()
        else:
            return True, OK, {}
    except:
        errorlog.error('Exception raised during strategyrun perf_summary', extra = extra(globals = globals(), locals = locals()), exc_info = traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@ajax_wrapper
def periods(id):
    '''
    Lekéri egy stratégia lefutás időszakos teljesitményadatait
    
    @url: /strategyrun/periods
     
    @type id:  int
    @param id: strategyRunId, stratégia lefutás azonosítója
    
    @rtype: dict
    @return: performancia adatok
    '''
    session = db.Session()
    try:
        strR = session.query(StrategyRun).get(id)
        if not strR:
            return False, GUI_REFRESH_NEEDED
        d = session.query(PerformanceStrategySerialData).get(strR.performanceSerialId)
        if not d:
            return False, NOT_FOUND + 'performance'
        conf = None
        if strR.type == STRATEGYRUN_BACKTEST:
            perf_from_date = session.query(BacktestConfig.perf_from_date).filter(BacktestConfig.id == strR.configId).first()[0]
        else:
            perf_from_date = session.query(LiveRunConfig.running_from).filter(LiveRunConfig.id == strR.configId).first()[0]

        drawdown = []
        trades_profit_loss = []
        MAE = []
        MFE = []
        startTime = calendar.timegm(perf_from_date.timetuple())
        weekTime = (startTime - (startTime + 259200) % 604800) * 1000  #week start time
        beginYear = perf_from_date.year
        beginMonth = perf_from_date.month
        weekly_profit = ast.literal_eval(d.weekly_profit)
        monthly_profit = ast.literal_eval(d.monthly_profit)
        yearly_profit = ast.literal_eval(d.yearly_profit)
        drawdown_lst = ast.literal_eval(d.drawdown)
        trades_profit_loss_lst = ast.literal_eval(d.trades_profit_loss)
        MAE_lst = ast.literal_eval(d.MAE)
        MFE_lst = ast.literal_eval(d.MFE)
        month = beginMonth
        year = beginYear
        for i in xrange(len(monthly_profit)):
            monthly_profit[i] = (str(year) + "-" + str(month).zfill(2), monthly_profit[i])
            month += 1
            if month == 13:
                year += 1
                month = 1
        year = beginYear
        for i in xrange(len(yearly_profit)):
            yearly_profit[i] = (str(year), yearly_profit[i])
            year += 1
        for i in xrange(len(weekly_profit)):
            weekly_profit[i] = (weekTime, weekly_profit[i])
            drawdown += drawdown_lst[i]
            trades_profit_loss += trades_profit_loss_lst[i]
            MAE += MAE_lst[i]
            MFE += MFE_lst[i]
            weekTime += 604800000
#            if not len(drawdown_lst[i]):
#                startTime += 604800
#                continue
#            drawdown.append((startTime, 'separator'))
#            trades_profit_loss.append((startTime, 'separator'))
#            MAE.append((startTime, 'separator'))
#            MFE.append((startTime, 'separator'))
#            drawdown += drawdown_lst[i]
#            trades_profit_loss += trades_profit_loss_lst[i]
#            MAE += MAE_lst[i]
#            MFE += MFE_lst[i]
#            startTime += 604800
#            if i == count - 1 or not len(drawdown_lst[i + 1]):
#                drawdown.append((startTime, 'separator'))
#                trades_profit_loss.append((startTime, 'separator'))
#                MAE.append((startTime, 'separator'))
#                MFE.append((startTime, 'separator'))

        return True, OK, {"account_balance": ast.literal_eval(d.net_profit),
                          "drawdown": drawdown,
                          "trades_profit_loss": trades_profit_loss,
                          "daily_profit": ast.literal_eval(d.daily_profit),
                          "weekly_profit": weekly_profit,
                          "monthly_profit": monthly_profit,
                          "yearly_profit": yearly_profit,
                          "floating_profit": ast.literal_eval(d.floating_profit),
                          "use_of_leverage": ast.literal_eval(d.use_of_leverage),
                          "MAE": MAE,
                          "MFE": MFE,
                          "beta": ast.literal_eval(d.beta)
                          }
    except:
        errorlog.error('Exception raised during strategy run get periodic performance', extra = extra(globals = globals(), locals = locals()), exc_info = traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()
