# -*- encoding: utf-8 -*- 
'''
Live account lefutásokkal kapcsolatos web serviceok

Created on 2012.07.03.

@newfield url: Web service, Web services
'''
#import gdt.store.db as db
#import gdt.backtest as backtest
#import gdt.store.ormObj as ormObj
from gdt.languages.default import OK, EXCEPTION_OCCURED
#from gdt.constants import *
##import gdt.constants
import traceback
#import ast
#import gdt.evol.info as info
#from gdt.evol.fitness import fitness_plugins
#from gdt.evol.plugin import evolplugins_list
#from gdt.backtest import mm_plugins, peakthrough_plugins
from gdt.utils import OrderedDict
#import datetime
from gdt.admin.system.log import errorlog, extra
import cherrypy
from gdt.control.router import ajax_wrapper
import logging
#import time
#import calendar

log = logging.getLogger('gdt.store')

@cherrypy.expose
@ajax_wrapper
def info(id):
    '''Lekéri egy live account adatait

    @url: /accountrun/info

    @type  id:  int
    @param id: accountRunId, live account azonosítója

    @rtype:  (bool, string, dict) tuple 
    @return: Visszaadja a művelet sikerességét, szöveges üzenetet, valamint a stratégia lefutáshoz tartozó evol konfig adat-szótárát
    '''
#    session = db.Session()
    res = OrderedDict()
    try:
        return True, OK, res
    except :
        errorlog.error('Exception raised during strategy run get', extra = extra(globals = globals(), locals = locals()), exc_info = traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        pass
#        session.close()

@cherrypy.expose
@ajax_wrapper
def perf_summary(id):
    '''
    Lekéri egy live account összesített performancia adatait
    
    @url: /accountrun/perf_summary

    @type id:  int
    @param id: accountRunId, live account azonosítója

    @rtype: dict
    @return: performancia adatok 
    '''
#    session = db.Session()
    try:
        return True, OK, []
    except:
        errorlog.error('Exception raised during strategyrun perf_summary', extra = extra(globals = globals(), locals = locals()), exc_info = traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        pass
#        session.close()

@cherrypy.expose
@ajax_wrapper
def periods(id):
    '''
    Lekéri egy live account időszakos teljesitményadatait
    
    @url: /accountrun/periods

    @type id:  int
    @param id: accountRunId, live account azonosítója
    
    @rtype: dict
    @return: performancia adatok
    '''
#    session = db.Session()
    try:
        return True, OK, {}
    except:
        errorlog.error('Exception raised during strategy run get periodic performance', extra = extra(globals = globals(), locals = locals()), exc_info = traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        pass
#        session.close()

@cherrypy.expose
@ajax_wrapper
def orderlog(start, limit, id):
    '''
    Lekéri egy live account kötési naplóját
    
    @url: /accountrun/orderlog
     
    @type id:  int
    @param id: accountRunId, live account azonosítója
    
    @rtype: list
    @return: kötési napló
    '''
#    session = db.Session()
    res = []
    log = []
    try:
        return True, OK, res, len(log)
    except:
        errorlog.error('Exception raised during strategyrun get_orderlog', extra = extra(globals = globals(), locals = locals()), exc_info = traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        pass
#        session.close()

@cherrypy.expose
@ajax_wrapper
def tradelog(id, instrument = ''):
    '''
    Lekéri egy live account trade naplóját egy adott instrumentumra nézve

    Ha instrumentum nincs megadva, akkor az teljes trade naplót adja vissza

    @url: /accountrun/tradelog

    @type id:  int
    @param id: accountRunId, live account azonosítója

    @rtype: list
    @return: trade napló
    '''
#    session = db.Session()
    res = []
    try:
        return True, OK, res
    except :
        errorlog.error('Exception raised during strategy run get tradelog', extra = extra(globals = {}, locals = locals()), exc_info = traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        pass
#        session.close()

@cherrypy.expose
@ajax_wrapper
def getinstruments(id):
    '''
    Lekéri egy live account strategyview chart fülön választható instrumentumait  

    @url: /accountrun/getinstruments

    @type id:  int
    @param id: accountRunId, live account azonosítója

    @rtype: list
    @return: választható stratégiák listája
    
    '''
#    session = db.Session()
    res = []
    try:
            return True, OK, res
    except:
        errorlog.error('Exception raised during strategyrun get_instruments', extra = extra(globals = globals(), locals = locals()), exc_info = traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        pass
#        session.close()

@cherrypy.expose
@ajax_wrapper
def getinstrumentsall(id):
    '''
    Lekéri egy live account strategyview graph fülön választható instrumentumait  

    @url: /accountrun/getinstrumentsall

    @type id:  int
    @param id: accountRunId, live account azonosítója

    @rtype: list
    @return: választható stratégiák listája
    '''
#    session = db.Session()
    res = []
    try:
        return True, OK, res
    except:
        errorlog.error('Exception raised during strategyrun get_instruments_all', extra = extra(globals = globals(), locals = locals()), exc_info = traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        pass
#        session.close()
