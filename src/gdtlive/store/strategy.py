# -*- encoding: utf-8 -*- 
'''
Stratégiákkal kapcsolatos web serviceok

Created on 2010.12.12.

@author: kulcsarb
@newfield url: Web service, Web services
'''
#from sqlalchemy.orm import aliased
#from sqlalchemy import func
import gdtlive.store.db as db
#import sqlalchemy as sa
import gdtlive.store.ormObj as ormObj
#import gdtlive.config as config
from gdtlive.languages.default import GUI_REFRESH_NEEDED, OK, EXCEPTION_OCCURED, NOT_FOUND
#from types import DictionaryType
from datetime import datetime
from gdtlive.utils import datetime2str, first0, OrderedDict#, date2str, R, R2
from gdtlive.constants import ELEMENT_TYPE_STRATEGY, STRATEGYRUN_BACKTEST, STRATEGYRUN_EVOL, STRATEGYRUN_LIVE#, STRATEGYRUN_STR, ELEMENT_TYPE_EVOLCONFIG, TIMEFRAME
from gdtlive.evol.gnodes.GenomeBase import GTree
from gdtlive.admin.system.log import errorlog, extra
from gdtlive.control.router import store_wrapper, ajax_wrapper#, paging_wrapper
import traceback
import cherrypy
import ast
from gdtlive.store.db import Account, LiveRunConfig, StrategyRun, BacktestConfig, Strategy, EntityName, PortfolioStrategyRun, PortfolioRun, Portfolio
import json


@cherrypy.expose
@ajax_wrapper
def info(id, userId):
    '''Lekéri egy stratégia adatait

    @url: /strategy/info

    @type  id: int
    @param id: a stratégia azonosítója

    @rtype:  (bool, string, dict) tuple 
    @return: Visszaadja a művelet sikerességét, szöveges üzenetet, valamint az stratégia adat-szótárát
    '''
    session = db.Session()
    try:
        strat = session.query(Strategy).get(id)
        if not strat:
            return False, GUI_REFRESH_NEEDED
        res = OrderedDict()
        res["Strategy ID"] = id
        res["Name"] = first0(session.query(EntityName.element_name).filter(EntityName.type == ELEMENT_TYPE_STRATEGY).filter(EntityName.element_id == strat.id), '')
        res["Individual"] = strat.individual
        stratrunEvol = session.query(StrategyRun).filter(StrategyRun.strategyId == id).first()
        if stratrunEvol:
            res["Creation date"] = datetime2str(stratrunEvol.time)
        else:
            res["Creation date"] = ""
        return True, OK, res
    except:
        errorlog.error('Exception raised during strategy get', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@ajax_wrapper
def runHistoryTree(id):
    '''Lekéri egy stratégia lefutásainak listáját

    @url: /strategy/runHistoryTree

    @type  id: int
    @param id: a stratégia azonosítója

    @rtype:  (bool, string, list) tuple 
    @return: Visszaadja a művelet sikerességét, szöveges üzenetet, valamint a stratégia lefutásainak listáját (adat-szótár tartalommal
    '''
    session = db.Session()
    try:
        backtestruns = dict((i[0], {'from_date': i[1].strftime('%Y-%m-%d %H:%M:%S'), 'to_date': i[2].strftime('%Y-%m-%d %H:%M:%S')}) for i in session.query(StrategyRun.id, BacktestConfig.from_date, BacktestConfig.to_date).filter(StrategyRun.strategyId == id).filter(StrategyRun.type == STRATEGYRUN_BACKTEST).filter(StrategyRun.configId == BacktestConfig.id).order_by(StrategyRun.strategyId).all())
        liveruns = {}
        for i in session.query(StrategyRun.id, Account.id, Account.name, Portfolio.id, Portfolio.name, LiveRunConfig.running_from, LiveRunConfig.running_to).filter(StrategyRun.strategyId == id).filter(StrategyRun.type == STRATEGYRUN_LIVE).filter(StrategyRun.configId == LiveRunConfig.id).filter(Account.id == LiveRunConfig.account_id).filter(LiveRunConfig.portfstratrun_id == PortfolioStrategyRun.id).filter(PortfolioStrategyRun.portfoliorun_id == PortfolioRun.id).filter(PortfolioStrategyRun.strategy_id == id).filter(Portfolio.id == PortfolioRun.portfolio_id).order_by(LiveRunConfig.running_from).all():
            if not i[3] or not i[4]:
                continue
            if i[3] not in liveruns:    #portfolio as root
                liveruns[i[3]] = {'name': i[4], 'children': {}}
            if not i[1] or not i[2]:
                continue
            if i[1] not in liveruns[i[3]]['children']:    #account under portfolio
                liveruns[i[3]]['children'][i[1]] = {'name': i[2], 'children': {}}
            if not i[0]:
                continue
            if i[0] not in liveruns[i[3]]['children'][i[1]]['children']:    #strategyrun under account
                liveruns[i[3]]['children'][i[1]]['children'][i[0]] = {'from_date': datetime2str(i[5]), 'to_date': datetime2str(i[6])}
        return (True, OK, (backtestruns, json.dumps(liveruns)))
    except:
        session.rollback()
        errorlog.error('Exception raised during strategy runHistoryTree', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()


def getSymbols(strategyId):
    '''Visszaadja a stratégia által használt instrumentumok listáját

    @type  strategyId:   list 
    @param strategyId:   Stratégia azonosító

    @rtype: str
    @return: A stratégia instrumentumainak listája
    '''
    session=db.Session()
    try:        
        gen=first0(session.query(Strategy.dns).filter(Strategy.id==strategyId), 0)
        if not gen:
            return []
        else:
            genome=GTree.decode(gen)
            symbols=genome.getSymbols()
            res = ','.join(symbols)
            return res
    except:
        errorlog.error('Exception raised during strategy getSymbols', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return []


def getNumofInstruments(strategyId):
    '''Visszaadja a stratégia által használt instrumentumok darabszámát

    @type  strategyId:   list 
    @param strategyId:   Stratégia azonosító

    @rtype: int
    @return: A stratégia instrumentumainak száma
    '''
    session=db.Session()
    try:
        gen=first0(session.query(Strategy.dns).filter(Strategy.id==strategyId),0)
        if not gen:
            return 0
        else:
            genome=GTree.decode(gen)
            symbols=genome.getSymbols()    
            return len(symbols)
    except:
        errorlog.error('Exception raised during strategy getNumofInstruments', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return 0

@cherrypy.expose
@store_wrapper
def delete(ids):
    '''Törli az id-kkel megadott stratégiákat 

    @url:     /strategy/delete

    @type  ids:   list 
    @param ids:   Stratégia azonosítók

    @rtype: (bool, str, list) tuple
    @return: sikeresség, szöveges üzenet, a törölt stratégiák azonositói
    '''
    return ormObj.deleteFromGridList(Strategy, ids, deleteRow)


def deleteRow(session, id):
    session.query(Strategy).filter(Strategy.id==id).delete()   

@cherrypy.expose
@ajax_wrapper
def isExportOk(ids):
    '''Megvizsgálja, hogy az adott stretégiák exportálhatóak-e 
    (azaz nem törlődtek-e a hívá óta, így az export nem üres file-lal tér-e vissza)

    @url: /strategy/isExportOk

    @type  ids: list
    @param ids: a stratégiák azonosítói

    @rtype:  bool
    @return: Exportálhatóak-e? (True/False)
    '''
    result = export(ids)
    if type(result)==tuple:
        return result[0], result[1]
    return not not result

@cherrypy.expose
@ajax_wrapper
def export(ids):
    '''Visszaadja az azonósítókkal adott stratégiák adatait:
    név ha van/space;dns/n

    @url: /strategy/export

    @type  ids: list
    @param ids: a stratégiák azonosítói

    @rtype:  (bool, str, dict) tuple
    @return: sikeresség, szöveges üzenet, az adatokat tartalmazó stringet egy dict értékben, aminek kulcsa txt  
    '''
    cherrypy.response.headers['Content-Disposition'] = 'attachment; filename="GDTStrategyExport.dat"'
    ids=ast.literal_eval(ids)
    if isinstance(ids, int):
        ids = [ids]
    session = db.Session()    
    try:  
        txt = ""
        for id in ids:
            dns = first0(session.query(Strategy.dns).filter(Strategy.id==id), "")
            name = first0(session.query(EntityName.element_name).filter(EntityName.element_id==id).filter(EntityName.type==ELEMENT_TYPE_STRATEGY), "")
            if dns:
                txt = txt + name + ";" + dns + '\n'
        return str(txt)
    except:
        errorlog.error('Exception raised during strategy export', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@ajax_wrapper
def importStrategies(exportTxt, userId):
    '''Elmenti a strinként kapott stratégiákat

    @url: /strategy/importStrategies

    @type  exportTxt: string
    @param exportTxt: a stratégiák adatai: név;dns\n

    @rtype: (bool, str) tuple
    @return: sikeresség, szöveges üzenet
    '''
    session = db.Session()
    fileCnt = exportTxt.file.read()
    try:
        if fileCnt.endswith("\n"):
            fileCnt = fileCnt.rstrip("\n")
        for row in fileCnt.splitlines():#split('\n')
            name, dns = row.split(';')
            #print name, dns
            s = Strategy(**{"dns": dns, 'timeframe': 1, 'symbol': 'EURUSD', 'mmplugin_id': 1, 'mmplugin_parameters': ''})    #TODO: ??
            #print s, dict(s)
            session.add(s)
            session.commit()
            if name != " ":
                session.add(EntityName(**{"type":ELEMENT_TYPE_STRATEGY, "element_id":s.id, "element_name":name}))
            session.commit()
        return True, OK
    except:
        session.rollback()
        errorlog.error('Exception raised during importStrategies', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@ajax_wrapper
def get_DNS(id):
    '''Visszaadja az adott stratégia DNS-ét:

    @url: /strategy/get_DNS

    @type  id: int
    @param id: a stratégia azonosítója

    @rtype: (bool, str, dict) tuple
    @return: sikeresség, szöveges üzenet, az adatokat tartalmazó string  
    '''
    session = db.Session()
    try:
        dns=GTree.decode(first0(session.query(Strategy.dns).filter(Strategy.id==id),""))
        if dns:
            dns=dns.__repr__()
            return True, OK, dns
        else:
            return False, NOT_FOUND + " strategy " +str(id)
    
    except:
        errorlog.error('Exception raised during strategy get_DNS', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@ajax_wrapper
def mmplugin(id):
    '''Visszaadja az adott stratégia mmpluginját:

    @url: /strategy/mmplugin

    @type  id: int
    @param id: a stratégia azonosítója

    @rtype: (bool, str, dict) tuple
    @return: sikeresség, szöveges üzenet, az adatokat tartalmazó string  
    '''
    session = db.Session()
    try:
        res = session.query(Strategy.mmplugin_id, Strategy.mmplugin_parameters).filter(Strategy.id == id).first()
        if res:
            return True, OK, {'id': res[0], 'params': res[1]}
        else:
            return False, NOT_FOUND + " strategy " +str(id)

    except:
        errorlog.error('Exception raised during strategy get_DNS', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()
