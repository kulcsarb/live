# -*- encoding: utf-8 -*- 

import gdtlive.store.db as db
from gdtlive.store.db import Portfolio, PortfolioRun, PortfolioStrategy, PortfolioAccount, Strategy, Account, PortfolioStrategyRun, PortfolioAccountRun, PerformanceData
from gdtlive.languages.default import OK, EXCEPTION_OCCURED, MISSING_DATA, NAME_CONFLICT, GUI_REFRESH_NEEDED, NOT_FOUND
import traceback
import ast
from gdtlive.admin.system.log import errorlog, extra
import cherrypy
from gdtlive.control.router import store_wrapper, ajax_wrapper
from gdtlive.control.router import tree_wrapper
from gdtlive.store import ormObj
from sqlalchemy import desc

management_types = {1: 'default'}

@cherrypy.expose
@ajax_wrapper
def managementtypes():
    '''Returns available management types

    @url:     /portfolio/managementtypes

    @rtype:    (bool, str, list) tuple
    @return:    Állapotjelzés, szöveges üzenet, az adatok név-érték szótárának listája
    '''
    return True, OK, [dict(id = key, name = value) for key, value in management_types.iteritems()]

@cherrypy.expose
@ajax_wrapper
def getAll(id = 0, start = 0):
    '''
    Resturns all portfolios.

    @url: /portfolio/getAll

    @rtype:    (bool, str, list) tuple
    @return:    Success, Message, The list of all portfolios
    '''
    from gdtlive.backtest import mm_plugins

    id = int(id)
    session = db.Session()
    query = session.query(Portfolio)

    res = []
    resDict = {}
    resIds = []
    try:
        if id:
            query = query.filter(Portfolio.id == id)
        for portf in query.all():
            resItem = dict(portf)
            itemId = portf.id
            pfrun = session.query(PortfolioRun).filter(PortfolioRun.portfolio_id == portf.id).order_by(desc(PortfolioRun.id)).first()
            if pfrun:
                resItem['state'] = pfrun.state
                resItem['start_time'] = pfrun.start_time.strftime('%Y-%m-%d %H:%M:%S')
                if pfrun.end_time:
                    resItem['end_time'] = pfrun.end_time.strftime('%Y-%m-%d %H:%M:%S')
            resItem['management_type_name'] = management_types[resItem['management_type']]
            resItem['mmplugin_name'] = ""
            res.append(resItem)
            resDict[itemId] = resItem
            resIds.append(itemId)

            for r in session.query(PortfolioAccountRun.portfolio_id, PerformanceData.winning_trades, PerformanceData.losing_trades, PerformanceData.open_trades, PerformanceData.net_profit, PerformanceData.floating_profit).filter(PerformanceData.id == PortfolioAccountRun.performance_id).filter(PortfolioAccountRun.portfolio_id.in_(resIds)).filter(PortfolioAccountRun.account_id == Account.id).filter(Account.benchmark == True):
                resDict[r[0]]['benchmark_winning_trades'] = r[1]
                resDict[r[0]]['benchmark_losing_trades'] = r[2]
                resDict[r[0]]['benchmark_open_trades'] = r[3]
                resDict[r[0]]['benchmark_net_profit'] = r[4]
                resDict[r[0]]['benchmark_floating_profit'] = r[5]

        return True, OK, res
    except:
        errorlog.error('Exception raised during portfolio list', extra=extra(globals={}, locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@store_wrapper
def save(**configData):
    '''    
    Saves a search. In case of a name conflict returns error message.

    @url: /portfolio/save

    @type  configData:  dict
    @param configData:  all the parameters of the portfolio

    @rtype:    (bool, str, dict) tuple
    @return:    Success, Message, Id of the saved element
    '''
    session=db.Session()
    configData.pop('management_type_name', None)
    configData.pop('mmplugin_name', None)
    itemId = int(configData.get("id", 0))
    name = configData.get('name')
    try:
        if itemId:
            item = session.query(Portfolio).get(itemId)
            if not item:
                return False, GUI_REFRESH_NEEDED
            if dict(item).get('name') != name:
                sameExist = session.query(Portfolio.id).filter(Portfolio.name == name).first()
                if sameExist:
                    return False, NAME_CONFLICT
            for attrName in configData:
                setattr(item, attrName, configData[attrName])
            #benchmark_account = session.query(db.Account).filter()
        else:
            sameExist = session.query(Portfolio.id).filter(Portfolio.name == name).first()
            if sameExist:
                return False, NAME_CONFLICT
            configData.pop('id')
            item = Portfolio(**configData)
            session.add(item)
            
        session.commit()
        
        accounts = session.query(db.PortfolioAccount.account_id).filter(db.PortfolioAccount.portfolio_id == item.id).all()        
        if not accounts:            
            account = db.Account(0, 'Benchmark of %s' % item.name, None, None, None, None, 10000, 10000, 30, None, None, 0, False, None, True)
            session.add(account)
            session.commit()            
            portfacc = db.PortfolioAccount(item.id, account.id)
            session.add(portfacc)
            session.commit()
        else:
            account = session.query(db.Account).filter(db.Account.broker_id==0).filter(db.Account.id == db.PortfolioAccount.account_id).filter(db.PortfolioAccount.portfolio_id == item.id).first()
            if account:
                account.name = 'Benchmark of %s' % item.name
                session.commit()
            
        return getAll(item.id)
    except:
        session.rollback()
        errorlog.error('Exception raised during portfolio save', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@store_wrapper
def delete(id):
    '''    
    Deletes a personal list.

    @url: /portfolio/delete

    @type  id: int
    @param id: id of the portfolio

    @rtype:    (bool, str, dict) tuple
    @return:    Success, Message
    '''
    session=db.Session()
    try:        
        session.query(Portfolio).filter(Portfolio.id == id).delete()
        session.commit()
        return True, OK
    except:
        errorlog.error('Exception raised during portfolio delete', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()

@cherrypy.expose
@tree_wrapper
def treelist(id = 0):
    '''A GUI számára visszaadja az összes evol konfig listáját, vagy az adott workflowrun-hoz tartozó konfigok listáját, showSize-nak megfelelően az összes kapcsolódó adat méretét a névben 

    @url: /evolconfig/treelist 

    @type  id: int
    @param id: workflowrun id, a megadott workflow lefutásra szűkíti a keresést. Ha 0, akkor az összes evolúciós konfigurációt adja vissza.

    @rtype: (bool, str, list) tuple
    @return: sikeresség, szöveges üzenet, az evolúciós konfiguráció adat-szótárak listája
    '''
    session = db.Session()
    return ormObj.tree_list(session, session.query(Portfolio.id, Portfolio.name))


def alreadyAddedList(session, portfolioOtherTable, field, table2, portfolio, items):
    alreadyAdded = []
    success = 0
    missing = []
    tableField = getattr(portfolioOtherTable, field)
    for already in session.query(tableField).filter(getattr(portfolioOtherTable, 'portfolio_id') == portfolio.id).filter(tableField.in_(items)).all():
        alreadyAdded.append(already[0])
    if len(alreadyAdded):
        items = list(set(items) - set(alreadyAdded))
    for item in items:
        subItem = session.query(table2).get(item)
        if not subItem:
            missing.append(item)
        else:
            session.add(portfolioOtherTable(portfolio.id, subItem.id))
            success += 1
    session.commit()
    result = str(success) + ' items has been added'
    if len(alreadyAdded):
        if len(alreadyAdded) == 1:
            alreadyAdded = str(alreadyAdded[0])
        else:
            alreadyAdded = str(alreadyAdded)[1:-1]
        result += '; the following items were already added: ' + alreadyAdded
    if len(missing):
        result += '; the following items do not exist: ' + str(missing)
    return result

def removeAddedList(session, portfolioOtherTable, portfolioOtherRunTable, otherField, items, portfolio_id):
    tableField = getattr(portfolioOtherTable, otherField)
    portfolioRun = session.query(PortfolioRun).filter(PortfolioRun.portfolio_id == portfolio_id).order_by(desc(PortfolioRun.id)).first()
    portfolioOtherPortfolioIdField = getattr(portfolioOtherTable, 'portfolio_id')
    if not portfolioRun:
        removable = items
        cantRemove = []
    else:
        cantRemove = []
        removable = []
        for element in session.query(tableField, portfolioOtherRunTable.state).filter(portfolioOtherPortfolioIdField == portfolio_id).filter(tableField.in_(items)).outerjoin((portfolioOtherRunTable, portfolioOtherRunTable.portfoliorun_id == portfolioRun.id)).all():
            if not element[1] or element[1] == 'STOPPED' or element[1] == 'ABORTED':
                removable.append(element[0])
            else:
                cantRemove.append(element[0])
    session.query(portfolioOtherTable).filter(portfolioOtherPortfolioIdField == portfolio_id).filter(tableField.in_(items)).delete('fetch')
    session.commit()
    result = str(len(removable)) + ' items has been removed'
    if len(cantRemove):
        if len(cantRemove) == 1:
            cantRemove = str(cantRemove[0])
        else:
            cantRemove = str(cantRemove)[1:-1]
        result += '; the following items cannot be removed because they are not stopped: ' + cantRemove
    return result

@cherrypy.expose
@ajax_wrapper
def addSub(id, ids, element_type, add):
    '''    
    Adds or removes subitems to portfolio.

    @url: /portfolio/addSub

    @type  id: int
    @param id: id of the portfolio
    @type  ids: string
    @param ids: the list of ids to add or remove in string format
    @type  element_type: str
    @param element_type: type of the items

    @rtype:    (bool, str) tuple
    @return:    Success, Message
    '''
    session=db.Session()

    intId = int(id)
    items = ast.literal_eval(ids)
    add = int(add)
    try:
        portfolio = session.query(Portfolio).get(intId)
        if not portfolio:
            return False, GUI_REFRESH_NEEDED
        if element_type == 'strategy':
            if add:
                return True, alreadyAddedList(session, PortfolioStrategy, 'strategy_id', Strategy, portfolio, items)
            else:
                return True, removeAddedList(session, PortfolioStrategy, PortfolioStrategyRun, 'strategy_id', items, intId)
        elif element_type == 'account':
            if add:
                return True, alreadyAddedList(session, PortfolioAccount, 'account_id', Account, portfolio, items)
            else:
                return True, removeAddedList(session, PortfolioAccount, PortfolioAccountRun, 'account_id', items, intId)
        else:
            return False, NOT_FOUND + ' element_type: ' + element_type
    except:
        errorlog.error('Exception raised during personal list addRemove', extra=extra(globals=globals(), locals=locals()), exc_info=traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()
