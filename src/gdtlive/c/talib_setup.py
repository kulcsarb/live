from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import os
import sys
import numpy

current = os.path.dirname(__file__)
try:
    os.unlink(current + os.sep + '_talib/talib.c')
    os.unlink(current + os.sep + 'talib.so')
except:
    pass

if len(sys.argv) == 1:
    sys.argv.append('build_ext')
    sys.argv.append('--inplace')


if sys.platform == "linux2" :
    include_talib_dir = "/usr/include/ta-lib/"
    lib_talib_dir = "/usr/lib/"
elif sys.platform == "win32":
    include_talib_dir = r"G:\lang\c\ta-lib\c\include"
    lib_talib_dir = r"G:\lang\c\ta-lib\c\lib"    

ext = Extension("talib", ["_talib/talib.pyx"],
    include_dirs=[numpy.get_include(), include_talib_dir],
    library_dirs=[lib_talib_dir],
    libraries=["ta_lib"]
)

setup(ext_modules=[ext],
    cmdclass = {'build_ext': build_ext})