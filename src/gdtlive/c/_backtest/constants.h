/*
 * constants.h
 *
 *  Created on: 2011.04.14.
 *      Author: Sherab
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_

enum DIRECTION {DIR_BUY=0, DIR_SELL=1};

const int PEAKTHROUGH_LIMIT = 4;

enum ORDERTYPE {ORDERTYPE_BUY=0,
				ORDERTYPE_SELL=1,
				ORDERTYPE_CLOSE=2,
				ORDERTYPE_PARTIAL_CLOSE=3,
				ORDERTYPE_CLOSE_BY_TP=4,
				ORDERTYPE_CLOSE_BY_SL=5,
				ORDERTYPE_SETSL=6,
				ORDERTYPE_SETTP=7,
				ORDERTYPE_CLOSETRADE=8,
				ORDERTYPE_CLOSEFIRST=9,
				ORDERTYPE_CLOSELAST=10,
				ORDERTYPE_CLOSETRADE_PARTIAL=11,
				ORDERTYPE_CLOSETRADE_PERCENT=12,
				ORDERTYPE_CLOSEALL=13,
				ORDERTYPE_CLOSEAMOUNT=14,
				ORDERTYPE_CLOSEAMOUNTPERCENT=15,
				ORDERTYPE_SETSL_FORALL=16,
				ORDERTYPE_SETTP_FORALL=17,
				ORDERTYPE_STOPLOSSHIT=18,
				ORDERTYPE_TAKEPROFITHIT=19,
				ORDERTYPE_CLOSEALL_LOSING=20,
				ORDERTYPE_FORCE_CLOSE=21		//force close trades after a certain time
};

const int MAX_TRADE_TIME = 7776000;				//90 days in seconds
const int HOLIDAY_LEVERAGE = 30;				//leverage value while holiday
const int HOLIDAY_PRE_TIME = 21600;				//22:00 - 17:00 + 1:00(for being sure) in seconds
const int HOLIDAY_CHANGE_LEVERAGE = 17;			//leverage is being reduced at 17:00
const int MINIMUM_AMOUNT = 1000;				//the minimum amount for a new trade

#endif /* CONSTANTS_H_ */
