#include <iostream>
#include "constants.h"
#include "account.h"
#include <stdlib.h>
#include <string.h>
#include "backtest.h"
#include "math.h"
#include "ta-lib/ta_func.h"

using namespace std;


/*MMPlugin::MMPlugin(void* accountptr) {
	this->account = accountptr;
}*/

NormalMMPlugin::NormalMMPlugin(void* accountptr) {
	this->account = accountptr;
	this->base_equity = 10000;
	this->leverage = 30;
	this->positionsize = 20000;
	this->positionmode = 0;
	this->riskmanagement = 0;
	this->set_sl = false;
	this->maxrisk = 0;
	this->riskreward_rate = 0;
	this->maxnumoftrades = 0;
	this->margincall_rate = 50;
	this->holiday = false;
	this->slBuy = 0;
	this->slSell = 0;
	this->peakthroughparam2 = 3;
	this->be_threshold = 0;
	this->modifysl_any = false;
}

NormalMMPlugin::~NormalMMPlugin() {
	free(this->slBuy);
	this->slBuy = 0;
	free(this->slSell);
	this->slSell = 0;
}

void NormalMMPlugin::configure(long base_equity, int leverage, double positionsize, int positionmode, bool riskmanagement, bool setsl, double maxrisk, double riskreward_rate, int maxnumoftrades, double margincall_rate, int sltype, int peakthroughplugin, double peakthroughparam, bool modifysl, int be_threshold) {
	// cout << base_equity << " " << leverage << " " << positionmode << " " << positionsize << " " << riskmanagement << " " << setsl << " " << maxrisk << " " << riskreward_rate << " " << maxnumoftrades << " " << margincall_rate << endl;
	this->base_equity = base_equity;
	this->leverage = leverage;
	this->positionsize = positionsize;
	this->positionmode = positionmode;
	this->riskmanagement = riskmanagement;
	this->set_sl = setsl;
	this->maxrisk = maxrisk;				// %-os ertek -> tort konverzio
	this->riskreward_rate = riskreward_rate;
	this->maxnumoftrades = maxnumoftrades;
	this->margincall_rate = margincall_rate / 100;  		// %-os ertek -> tort konverzio
	this->sltype = sltype;
	this->peakthroughplugin = peakthroughplugin;
	this->peakthroughparam = peakthroughparam;
	this->modifysl = modifysl;
	this->be_threshold = be_threshold;
	if (modifysl || be_threshold) {
		this->modifysl_any = true;
	}
	if ((this->positionmode == 0) || (this->positionmode == 1)) {
		this->positionsize = this->positionsize / 100.0;
	} else if (this->positionmode == 2) {
		this->positionsize = this->positionsize * this->base_equity / 100.0;
	}
	this->lastSize = 0;
}

void NormalMMPlugin::countPeakThrough(int instrument_num, instrument_t* instruments, int candle_num) {
	int i, j, k;
	double limit;
	instrument_t* inst;
	if (this->peakthroughplugin == 1) {		//talib min-max
		for (i = 0; i < instrument_num; i++) {
			inst = &instruments[i];
			TA_MIN(0, candle_num - 1, inst->bidlow, (int)this->peakthroughparam, &j, &k, this->slBuy + i * candle_num);
			TA_MAX(0, candle_num - 1, inst->askhigh, (int)this->peakthroughparam, &j, &k, this->slSell + i * candle_num);
			for (k = candle_num - 1; k >= j; k--) {
				limit = (double)PEAKTHROUGH_LIMIT * inst->value[k];
				this->slBuy[k + i * candle_num] = this->slBuy[k - j + i * candle_num] - limit;
				this->slSell[k + i * candle_num] = this->slSell[k - j + i * candle_num] + limit;
			}
			for (; k >= 0; k--) {
				this->slBuy[k + i * candle_num] = NAN;
				this->slSell[k + i * candle_num] = NAN;
			}
		}
	} else if (this->peakthroughplugin == 2) {
		for (i = 0; i < instrument_num; i++) {
			inst = &instruments[i];
		    int period = this->peakthroughparam;
		    double mult2 = this->peakthroughparam2;
		    int dir, dir2;
		    double newValue, diff, sqr;
		    double sum = 0;
		    double squares[period];
		    double d[candle_num];
	        d[0] = NAN;
		    double oldValue = this->evaluationFunction(inst, 0);
		    double mult = 1 / (double)period;
			for (k = 1; k < period; k++) {
		        newValue = this->evaluationFunction(inst, k);
		        diff = newValue - oldValue;
		        sqr = diff * diff;
		        squares[k - 1] = sqr;
		        sum += sqr;
		        oldValue = newValue;
		        d[k] = NAN;
				this->slBuy[k + i * candle_num - 1] = NAN;
				this->slSell[k + i * candle_num - 1] = NAN;
		    }
		    squares[period - 1] = 0;
		    int squareIndex = period - 1;
		    for (; k < candle_num; k++) {
		        sum -= squares[squareIndex];
		        newValue = this->evaluationFunction(inst, k);
		        diff = newValue - oldValue;
		        sqr = diff * diff;
		        squares[squareIndex] = sqr;
		        sum += sqr;
		        oldValue = newValue;
		        squareIndex = (squareIndex + 1) % period;
		        d[k] = sqrt(sum * mult) * mult2;
		    }
		    k = 2;
		    while ((isnan(d[k])) && (k < candle_num)) {
		        k++;
		    }
		    oldValue = this->evaluationFunction(inst, k - 2);
		    newValue = this->evaluationFunction(inst, k - 1);
		    dir = (newValue > oldValue) ? 1 : 0;    //where the chart moves
		    dir2 = dir;     //if the last saved was a peak or valley
		    double lastValue = dir ? -INFINITY : INFINITY;   //last peak or valley value
		    int lastIndex = k - 1;
		    double tmp = 0;
		    oldValue = newValue;
		    int lastIndex2 = 0;
		    double swap;
		    double peak, valley;
		    double lastValue2 = 0;       //before last peak or valley value
		    for (; k < candle_num; k++) {
		        newValue = this->evaluationFunction(inst, k);
		        if (((dir == 1) != (newValue > oldValue)) && (((dir2 == 0) && (lastValue - oldValue > d[k])) || ((dir2 == 1) && (oldValue - lastValue > d[k])))) {
					if ((tmp) || (lastIndex)) {
						lastValue2 = lastValue;
						lastIndex2 = lastIndex;
					}
					lastIndex = k - 1;
					tmp = oldValue;
					lastValue = oldValue;
					dir2 = 1 - dir2;
		        }

		        dir = (newValue > oldValue) ? 1 : 0;

		        if (((tmp) && (dir != dir2)) && (((dir == 1) && (newValue >= tmp)) || ((dir == 0) && (newValue <= tmp)))) {
					tmp = 0;
		            swap = lastValue;
		            lastValue = lastValue2;
		            lastValue2 = swap;
					swap = lastIndex;
					lastIndex = lastIndex2;
					lastIndex2 = (int)swap;
					dir2 = 1 - dir2;
		        }
		        if (dir2 == 0) {
		            peak = inst->askhigh[lastIndex];
		            if (lastIndex2) {
		                valley = inst->bidlow[lastIndex2];
		            } else {
		                valley = NAN;
		            }
		        } else {
		            valley = inst->bidlow[lastIndex];
		            if (lastIndex2) {
		                peak = inst->askhigh[lastIndex2];
		            } else {
		                peak = NAN;
		            }
		        }
		        oldValue = newValue;
				this->slBuy[k + i * candle_num - 1] = valley;
				this->slSell[k + i * candle_num - 1] = peak;
		    }
			this->slBuy[(i + 1) * candle_num - 1] = this->slBuy[(i + 1) * candle_num - 2];
			this->slSell[(i + 1) * candle_num - 1] = this->slSell[(i + 1) * candle_num - 2];
//				time_t ct;
//				for (k = 0; k < candle_num; k++) {
//					cout << this->slSell[k + i * candle_num];
//					if (k % 20 == 0) {
//						ct = inst->time[k];
//						struct tm* _tm = gmtime(&ct);
//						cout << " " << i << ". " << k << ". " << (1900 + _tm->tm_year) << "-" << (1 + _tm->tm_mon) << "-" << _tm->tm_mday << " " << _tm->tm_hour;
//					}
//					cout << endl;
//				}
		}
	}
}

void NormalMMPlugin::initPeakThrough(int instrument_num, instrument_t* instruments, int candle_num) {
	//peak-through, initializing stop-loss arrays for peak and valley plugins (Trailing stop is not one of them)
	if (((this->sltype == 1) || (this->sltype == 2) || this->modifysl) && (this->peakthroughplugin != 3)) {
		if (this->lastSize < candle_num * instrument_num) {
			this->lastSize = candle_num * instrument_num;
			free(this->slBuy);
			this->slBuy = (double*)malloc(sizeof(double) * this->lastSize);
			free(this->slSell);
			this->slSell = (double*)malloc(sizeof(double) * this->lastSize);
		}
		this->countPeakThrough(instrument_num, instruments, candle_num);
	}
}

double NormalMMPlugin::evaluationFunction(instrument_t* inst, int index) {
	return inst->askopen[index] + inst->askhigh[index] + inst->asklow[index] + inst->askclose[index] + inst->bidopen[index] + inst->bidhigh[index] + inst->bidlow[index] + inst->bidclose[index];
}

void NormalMMPlugin::setCurrents(instrument_t* inst, int index) {
	if (((this->sltype == 1) || (this->sltype == 2) || this->modifysl) && (this->peakthroughplugin != 3)) {
		inst->current_sl_buy = this->slBuy[index];
		inst->current_sl_sell = this->slSell[index];
	}
}

/**
 * Holiday setter, leverage is lower in case of this->holiday == true.
 */
void NormalMMPlugin::setHoliday(bool holiday) {
	this->holiday = holiday;
}

/**
 * Returns current leverage based on the current time state.
 */
double NormalMMPlugin::getLeverage() {
	return this->holiday ? HOLIDAY_LEVERAGE : this->leverage;
}

/**
 * Rounds stoploss or takeprofit value to 3 or 5 decimals based on trade's instrument pipsize.
 * This method is valid only for positive numbers.
 */
double NormalMMPlugin::roundSlTp(double sltp, instrument_t* inst) {
	if (!sltp) {
		return sltp;
	}
	double mult = inst->pipsize * 0.1;
	return floorf(sltp / mult + 0.5) * mult;
}

double NormalMMPlugin::calc_maxrisk(instrument_t* inst, double total_net_profit, long amount, int direction, double ask, double bid, double stoploss) {
	double stoploss2;
	if (direction == DIR_BUY) {
		stoploss2 = ask - this->maxrisk * inst->pipsize;
		if (((!stoploss) || (stoploss2 > stoploss)) && (stoploss2 < bid)) {
			stoploss = stoploss2;
		}
	} else {
		stoploss2 = bid + this->maxrisk * inst->pipsize;
		if (((!stoploss) || (stoploss2 < stoploss)) && (stoploss2 > ask)) {
			stoploss = stoploss2;
		}
	}
//	cout << " stoploss: " << stoploss << endl;
	return this->roundSlTp(stoploss, inst);
}

/*
 * called on trade open if the sl is invalid
 */
double NormalMMPlugin::calc_invalidSl(instrument_t* inst, int current_candle, double total_net_profit, long amount, int direction) {
	if (this->sltype != 1) {
		return calc_maxrisk(inst, total_net_profit, amount, direction, inst->current_ask, inst->current_bid);
	} else {
		double retVal;
		if (direction == DIR_BUY) {
			retVal = inst->bidlow[current_candle] - (double)PEAKTHROUGH_LIMIT * inst->value[current_candle];
		} else {
			retVal = inst->askhigh[current_candle] + (double)PEAKTHROUGH_LIMIT * inst->value[current_candle];
		}
		return this->roundSlTp(retVal, inst);
	}
}

long NormalMMPlugin::calc_amount(instrument_t* inst, int current_candle, double total_net_profit, int direction, double ask, double bid, double* stoploss, double* takeprofit, double commandpercent) {
	long amount = 0;
	double stoploss2, priceDiff;
	double pipvalue = inst->pipvalue;
	double pipsize = inst->pipsize;
	double balance;
	if (this->positionmode == 1) {
		balance = ((AccountBase *)this->account)->performance->last_month_balance;	//balance is the last month balance for amount and stop loss
//		cout << "last month balance: " << balance << endl;
	} else {
		balance = total_net_profit + this->base_equity;
	}
//	cout << "balance: " << balance << " positionmode: " << this->positionmode;
	//double floating_profit = ((Account *)this->account)->getFloatingProfit();

	// kötésméret kiszámítása
//	time_t ct;
//	ct = inst->current_time;
//	struct tm* _tm = gmtime(&ct);
	switch (this->positionmode) {
		case 0:	//balance %
		case 1:	//monthly balance %
			amount = balance * this->positionsize;
//			if ((1900 + _tm->tm_year == 2004) && (1 + _tm->tm_mon == 8) && (_tm->tm_mday == 8)) {
//				cout << "amount: " << amount << " balance: " << balance << " positionsize: " << this->positionsize << endl;
//			}
//			cout << " positionsize: " << this->positionsize << " amount: " << amount;
			break;
		case 2:	//Fix
			amount = this->positionsize;
//			cout << " positionsize: " << this->positionsize << " amount: " << amount;
			break;
		case 3:	//Dynamic
			if (direction == DIR_BUY) {
				stoploss2 = inst->current_sl_buy;
				if ((!*stoploss) || (stoploss2 > *stoploss)) {
					*stoploss = stoploss2;
				}
				if (*stoploss >= bid) {
					*stoploss = inst->bidlow[current_candle] - (double)PEAKTHROUGH_LIMIT * inst->value[current_candle];
				}
				priceDiff = bid - *stoploss;
			} else {
				stoploss2 = inst->current_sl_sell;
				if ((!*stoploss) || (stoploss2 < *stoploss)) {
					*stoploss = stoploss2;
				}
				if (*stoploss <= ask) {
					*stoploss = inst->askhigh[current_candle] + (double)PEAKTHROUGH_LIMIT * inst->value[current_candle];
				}
				priceDiff = *stoploss - ask;
			}
//			ct = inst->current_time;
//			_tm = gmtime(&ct);
//			cout << (1900 + _tm->tm_year) << "-" << (1 + _tm->tm_mon) << "-" << _tm->tm_mday << " " << _tm->tm_hour;
			if (isnan(*stoploss)) {
//				cout << " current_sl_buy: " << inst->current_sl_buy << " current_sl_sell: " << inst->current_sl_sell << endl;
				return 0;
			}
			amount = balance * this->maxrisk * 0.01 * pipsize / (priceDiff * pipvalue);
//			cout << " dyn isbuy: " << (direction == DIR_BUY) << " sl: " << stoploss << " price diff: " << priceDiff << " amount: " << amount << endl;
			break;
		default:
			cout << " wrong positionmode: " << this->positionmode << endl;
			return 0;
	}
	amount = (long)(amount * commandpercent);

	if (this->riskmanagement) {	//a felhasználó a kötelező SL használatot választotta
		if (this->sltype != 1) {
			*stoploss = this->calc_maxrisk(inst, total_net_profit, amount, direction, ask, bid, *stoploss);
		}	//not dynamic case        && contains peak-through
		if ((this->positionmode != 3) && ((this->sltype == 1) || (this->sltype == 2)) && (this->peakthroughplugin != 3)) {
			if (direction == DIR_BUY) {
				stoploss2 = inst->current_sl_buy;
				if (((!*stoploss) || (stoploss2 > *stoploss)) && (stoploss2 < bid)) {
					*stoploss = stoploss2;
				}
			} else {
				stoploss2 = inst->current_sl_sell;
				if (((!*stoploss) || (stoploss2 < *stoploss)) && (stoploss2 > ask)) {
					*stoploss = stoploss2;
				}
			}
		}
		if (!*stoploss) {
			return 0;
		}

		if (this->riskreward_rate) {
//			cout << "takeprofit " << bid << " 1. " << ask << " 2. " << stoploss << " 3. " << this->riskreward_rate << " 4. " << (direction == DIR_BUY) << endl;
			if (direction == DIR_BUY) {
				*takeprofit = ask + ((ask - *stoploss) * this->riskreward_rate);
			} else {
				*takeprofit = bid - ((*stoploss - bid) * this->riskreward_rate);
			}
//			if ((inst->current_time == 1131499800) || (inst->current_time == 1154055600) || (inst->current_time == 1212420600)) {
//				cout << "ask: " << ask << " bid: " << bid << " stoploss: " << *stoploss << " takeprofit: " << *takeprofit << " " << inst->current_time << endl;
//			}
		}
		*stoploss = this->roundSlTp(*stoploss, inst);
		*takeprofit = this->roundSlTp(*takeprofit, inst);
	} else {
//		cout << " no risk management!" << endl;
	}

	return amount;
}
