/*
 * Trade.h
 *
 *  Created on: 2011.04.14.
 *      Author: Sherab
 */

#ifndef TRADE_H_
#define TRADE_H_
#include <iostream>
#include <list>
#include "time.h"
#include "constants.h"
#include "backtest.h"

//*****************************************
//;	// MI A FASZ EZ?????? EGY UBUNTU UPDATE UTÁN, HA A PONTOSVESSZŐT NEM RAKOM IDE, ELHAL A COMPILER:
//     Trade.h:18: error: expected unqualified-id before ‘using’
//*****************************************

using namespace std;

struct tradeperf {
	double MAE;
	double MFE;
	double drawdown;
};

typedef struct tradeperf tradeperf_t;


class Trade {
	void apply_slippage(double &ask, double &bid);
public:
	int group;
	long id;
	long index;
	int symbol;
	int direction;
	time_t time;
	double price;
	double ask;
	double bid;
	long amount;
	double exposure;
	double stoploss;
	double takeprofit;
	double commission;
	bool closed;
	double be_threshold;			//if non-zero, the trade's be is at this value
	double ts_diff;				//trailing stop difference between price and default sl
	tradeperf_t currency;
	instrument_t* instrument;			//ownership at account object
	Trade* next_trade;					//ownership at account object
	Trade* prev_trade;					//ownership at account object

	//close variables
	double rollover;
	double pip;
	int close_type;
	time_t close_time;
	double close_price;
	double close_commission;
	long close_index;
	double cumprofit;

	Trade(int symbol, int direction, time_t time, double ask, double bid,  long amount, double stoploss, double takeprofit, double commission, double exposure, int group);
	~Trade();

	bool close(int ordertype=ORDERTYPE_CLOSE, time_t time=0, double ask=0, double bid=0);
	bool setStoploss(double ask, double bid, double stoploss);
	bool modifyStoploss(bool printError = true);
	bool setTakeprofit(double ask, double bid, double takeprofit);
	bool isTakeprofitHit(double ask, double bid);
	bool isStoplossHit(double ask, double bid);
	double getFloatingProfit();
	double getFloatingProfitMin();
	void calcProfit(double ask, double bid);
	double calcRollover(time_t time);
	void print();
	void toString(string* str);

	static double total_net_profit;
	static long open_trades;
	static long counter;
	static double slippage;
	static long current_candle;
	static instrument_t* instruments;			//ownership at account object
	static Trade* first_trade;					//ownership at account object
	static Trade* last_trade;					//ownership at account object
	static list<Trade*> open_trades_list;
	static int modifyPluginParam;			//-1 if peak and valley plugin, plugin param if trailing stop
};


#endif /* TRADE_H_ */
