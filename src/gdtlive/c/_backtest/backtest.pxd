
cdef extern from "time.h":	
	ctypedef long time_t
	cdef time_t time(time_t*)
	
cdef extern from "backtest.h":
	cdef struct profit:
		float usd
		float pip
		float point
	
	cdef struct instrument:
		char *name
		int symbol
		int open_trades
		float pipsize
		float pipvalue
		float interest
		float stoploss
		float takeprofit
		float current_ask
		float current_bid
		time_t current_time
		long *time
		float *askopen
		float *askhigh
		float *asklow
		float *askclose
		float *bidopen
		float *bidhigh
		float *bidlow
		float *bidclose
		float *volume
		float *value
	
	ctypedef instrument instrument_t
	ctypedef profit profit_t
		
