#ifndef MMPLUGIN_H_
#define MMPLUGIN_H_

/*class MMPlugin {
	MMPlugin(void *accountptr);
};*/

class NormalMMPlugin {
public:
	void *account;
	long base_equity;
	double leverage;
	double positionsize;
	int positionmode;
	bool riskmanagement;
	bool set_sl;
	double maxrisk;
	double riskreward_rate;
	int maxnumoftrades;
	double margincall_rate;
	bool holiday;			//in case of holiday the leverage is lower
	int sltype;
	int peakthroughplugin;
	double peakthroughparam, peakthroughparam2;
	bool modifysl;
	bool modifysl_any;
	int be_threshold;
	double* slBuy, * slSell;	//sl min-max arrays
	int lastSize;			//size of slBuy and slSell

	NormalMMPlugin(void *accountptr);
	~NormalMMPlugin();

	void configure(long base_equity, int leverage, double positionsize, int positionmode, bool riskmanagement, bool setsl, double maxrisk, double riskreward_rate, int maxnumoftrades, double margincall_rate, int sltype, int peakthroughplugin, double peakthroughparam, bool modifysl, int be_threshold);
	void countPeakThrough(int instrument_num, instrument_t* instruments, int candle_num);
	void initPeakThrough(int instrument_num, instrument_t* instruments, int candle_num = 0);
	void setCandles(int candle_num, int instrument_num, instrument_t* instruments);
	double evaluationFunction(instrument_t* inst, int index);
	void setCurrents(instrument_t* inst, int index);
	void setHoliday(bool holiday);
	double getLeverage();
	double roundSlTp(double sltp, instrument_t* inst);
	long calc_amount(instrument_t* inst, int current_candle, double total_net_profit, int direction, double ask, double bid, double* stoploss, double* takeprofit, double commandpercent);
	double calc_maxrisk(instrument_t* inst, double total_net_profit, long amount, int direction, double ask, double bid, double stoploss = 0);
	double calc_invalidSl(instrument_t* inst, int current_candle, double total_net_profit, long amount, int direction);
};


#endif /* MMPLUGIN_H_ */
