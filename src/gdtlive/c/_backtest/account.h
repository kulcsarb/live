/*
 * account.h
 *
 *  Created on: Apr 15, 2011
 *      Author: 
 */

#ifndef ACCOUNT_H_
#define ACCOUNT_H_

#include "time.h"
#include "Trade.h"
#include "constants.h"
#include "mmplugin.h"
#include "performance.h"
#include <iostream>
#include <list>
#include <set>

using namespace std;


class AccountBase {
	void open(int symbol, int direction, long amount = 0, double sl = 0, double tp = 0, double percent = 1.0, int group = 0);
	void openOrHold(int symbol, int direction, long amount = 0, double sl = 0, double tp = 0, double percent = 1.0, int group = 0);
	bool closeTrade(Trade* trade, int ordertype = ORDERTYPE_CLOSE, time_t time = 0, double ask = 0, double bid = 0);
	struct order_s {
		list<long> ids;
		set<int> symbols;
		char apicall[100];
		char action[100];
		bool executed;
		time_t time;
		int group;
	} order;

	//array of holiday start and end time: [<holiday0 start>, <holiday0 end>, <holiday1 start>, <holiday1 end>, ...]
	long* holidays;			//ownership at strategy object

	int currentHoliday;		//the index of the current holiday element; even in case of holiday, odd otherwise
	bool checkClosable;		//checks before a trade close if the trade can be closed
	int holidayCount;
	int currencyNum;		//count of base currencies
public:
	bool canOpen;			//the strategy may not open trades at a certain time
	time_t to_date;			//the time where strategy can not open any more trades

	instrument_t* instruments;
	Trade* trades;
	PerformanceBase* performance;			//ownership at simulatedaccount object
	NormalMMPlugin* mm_plugin;

	int instrument_num;
	double commission;
	bool commission_in_pip;
	double failed_trades;
	string orderlog;
	long orderid;
	bool use_orderlog;
	string tradelog;

	AccountBase(PerformanceBase* performance);
	~AccountBase();
	void destroyTrades();
	void resetValues();

	void setInstruments(instrument_t *instruments, int instrument_num);
	void setInstrumentNum(int instrument_num);
	void setInstrument(int symbol, double pipsize, char* name);
	void setDatarow(int symbol, int datarow_id, double* datarow);
	void setTimeDatarow(time_t *datarow);
	void setTimeframe(int timeframe);
	void setCandleNum(long candle_num);
	void setSlippage(float slippage);
	float getSlippage();
	double getLeverage();
	void configMM(int mmPluginId, long base_equity, int leverage, double positionsize, int positionmode,
		bool riskmanagement, bool setsl, double maxrisk, double riskreward_rate, int maxnumoftrades,
		double margincall_rate, int sltype, int peakthroughplugin, double peakthroughparam, bool modifysl,
		int be_threshold);
	void setHolidays(long* holidays, int holidayCount);

	void show();
	bool onClose(long index);
	void backtestDone();
	void buy(int symbol, long amount=0, double sl=0, double tp=0, double percent=1.0, int group=0);
	void sell(int symbol, long amount=0, double sl=0, double tp=0, double percent=1.0, int group=0);
	void buyOrHold(int symbol, long amount=0, double sl=0, double tp=0, double percent=1.0, int group=0);
	void sellOrHold(int symbol, long amount=0, double sl=0, double tp=0, double percent=1.0, int group=0);
	void closeTrade(int position, int group=0);
	void closeFirstTrade(int symbol, int group=0);
	void closeLastTrade(int symbol, int group=0);
	void closeAll(int group=0);
	void closeAllSymbol(int symbol, int group=0);
	bool closeAllLosing();
	void closeAmountPercent(int symbol, double percent, int group=0);
	void setTakeprofit(int symbol, double takeprofit, int group=0);
	void setStoploss(int symbol, double stoploss, int group=0);
	void addOrder();
	double getFloatingProfit();
	double getFloatingProfitMin();
	double getBalance();
	double getEquity();
	char* getOrderlog();
	char* getTradelog();
	long getTradeNum();
	double expSum(instrument_t* aInst = NULL, int direction = 0);
	double calculate_exp(int direction, long amount, instrument_t* instrument);
};

class Account1 : public AccountBase {
public:
	Account1(PerformanceBase* performance);
};

#endif /* ACCOUNT_H_ */
