from backtest cimport instrument_t, profit_t, time_t


cdef extern from "Trade.h":

	cdef struct closedtrade :
		int type
		int time
		float price
		int amount
		profit_t * profit
		float commission
		float rollover
		int index
		closedtrade * next_close

	struct tradeperf:
		float MAE
		float MFE
		float profit

	ctypedef closedtrade closedtrade_t
	ctypedef tradeperf performance_t


	cdef cppclass Trade:
		Trade(int, int, time_t, float, float, int, float, float, float)
		int id
		int index
		int direction
		int symbol
		int time
		float price
		float ask
		float bid
		int open_amount
		int actual_amount
		float stoploss
		float takeprofit
		float commission
		bint closed
		bint partially_closed
		performance_t currency
		performance_t point
		instrument_t * instrument
		closedtrade_t * first_close
		closedtrade_t * last_close
		Trade * next_trade
		Trade * prev_trade

		void close(time_t time, float ask, float bid, int ordertype)
		void partialClose(int amount, time_t time, float ask, float bid)
		void stoplossHit(time_t time, float ask, float bid)
		void takeprofitHit(time_t time, float ask, float bid)
		void setStoploss(float ask, float bid, float stoploss)
		void setTakeprofit(float ask, float bid, float takeprofit)
		bint isTakeprofitHit(float ask, float bid)
		bint isStoplossHit(float ask, float bid)
		float getProfit()
		float getFloatingProfit(float ask, float bid)

		profit_t * calcProfit(int amount, float ask, float bid)
		float calcRollover(time_t time, int amount)


cdef extern from "Trade.h" namespace "Trade::Trade":
	int open_trades
	int counter
	double slippage
	int current_candle
	instrument_t * instruments
	Trade * first_trade
	Trade * last_trade

