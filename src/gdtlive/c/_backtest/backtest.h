/*
 * backtest.h
 *
 *  Created on: Apr 15, 2011
 *      Author: 
 */

#ifndef BACKTEST_H_
#define BACKTEST_H_
#include "time.h"
//;

struct instrument {
	char *name;
	int symbol;
	long open_trades;
	double pipsize;
	double pipvalue;
	double interest;
	double stoploss;
	double takeprofit;
	double current_ask;
	double current_bid;
	double current_baseusd_rate;
	time_t current_time;
	double current_sl_buy;
	double current_sl_sell;
	time_t *time;
	double *askopen;
	double *askhigh;
	double *asklow;
	double *askclose;
	double *bidopen;
	double *bidhigh;
	double *bidlow;
	double *bidclose;
	double *volume;
	double *value;
	double *baseusd_rate;
	double *datarows[11];
	double exposure;
};

typedef struct instrument instrument_t;

#endif /* BACKTEST_H_ */
