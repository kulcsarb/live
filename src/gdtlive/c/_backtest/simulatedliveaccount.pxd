# -*- encoding: utf-8 -*-
from backtest cimport instrument_t, time_t
cimport numpy as np


#cdef extern from "time.h" nogil:
#    ctypedef long time_t

cdef extern from "backtest.h":
    cdef struct instrument:
        char *name
        int symbol
        long open_trades
        double pipsize
        double pipvalue
        double interest
        double stoploss
        double takeprofit
        double current_ask
        double current_bid
        double current_baseusd_rate
        time_t current_time
        double current_sl_buy
        double current_sl_sell
        time_t *time
        double *askopen
        double *askhigh
        double *asklow
        double *askclose
        double *bidopen
        double *bidhigh
        double *bidlow
        double *bidclose
        double *volume
        double *value
        double *baseusd_rate
        double *datarows[11]
        double exposure

    ctypedef instrument instrument_t

cdef extern from "mmplugin.h":
    cdef cppclass NormalMMPlugin:
        long base_equity
        double leverage
        NormalMMPlugin(void *accountptr)
        void configure(long base_equity, int leverage, double positionsize, int positionmode, bint riskmanagement, bint setsl, double maxrisk, double riskreward_rate, int maxnumoftrades, double margincall_rate, int sltype, int peakthroughplugin, double peakthroughparam, bint modifysl, int be_threshold)
        void setHoliday(bint holiday)
        double getLeverage()
        long calc_amount(instrument_t* inst, int current_candle, double total_net_profit, int direction, double ask, double bid, double* stoploss, double* takeprofit, double commandpercent)
        void setCurrents(instrument_t* inst, int index)
        void initPeakThrough(int instrument_num, instrument_t* instruments, int candle_num)


cdef class SimulatedLiveAccount:
    cdef:
        NormalMMPlugin* mmplugin
        int instrument_num
        int candle_num
        instrument_t* instruments
    
    cpdef setLeverage(self, double leverage)
    cpdef setBaseEquity(self, long base_equity)
    cpdef setInstrumentNum(self, int instrument_num)    
    cpdef setInstrument(self, int symbol, double pipsize, char* name)
    cpdef setDatarow(self, int instrument, int datarow_id, np.ndarray[np.double_t, ndim = 1] datarow)
    cpdef setCandleNum(self, int candle_num)
    cpdef configMM(self, int id, object conf)
    cpdef setHoliday(self, bint holiday)
    cpdef getLeverage(self)
    cpdef calc_amount(self, int symbol, int current_candle, double total_net_profit, int direction, double ask, double bid, double commandpercent)
    cpdef initPeakThrough(self, candle_num)

    cpdef destroy(self)
