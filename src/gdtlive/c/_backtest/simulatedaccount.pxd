from backtest cimport instrument_t, time_t
from trade cimport Trade
cimport numpy as np


cdef extern from "time.h" nogil:
    ctypedef long time_t


cdef extern from "performance.h":
    cdef struct performance:
        double net_profit
        double cumulated_profit
        double commission
        double gross_profit
        double gross_loss
        double sharpe_ratio
        double maximum_drawdown
        double profit_factor
        long number_of_trades
        long winning_trades
        long losing_trades
        double percent_profitable
        double average_trade
        double average_loser
        double average_winner
        double average_wl_ratio
        double largest_winner
        double largest_loser
        long max_conseq_winners
        long max_conseq_losers
        double trades_per_day
        double avg_time_in_market
        double avg_bars_in_trade
        double profit_per_month
        double avgMFE
        double avgMAE
        double floating_profit
        double open_trades
        double profit_variance
        double min_yearly_profit_p
        double max_drawdown_p
        double min_floating_p
        double min_floating
        double avg_neg_floating_p
        double avg_pos_floating_p
        double avg_neg_floating
        double avg_pos_floating
        double max_use_of_lev
        double avg_use_of_lev
        double chance1m
        double chance3m
        double score


    ctypedef performance performance_t

    cdef cppclass PerformanceBase:
        double* drawdown_per_close
        double* profit_per_close
        double* account_balance
        double** maeWeekly
        double** mfeWeekly
        double** drawdownWeekly
        double** profitlossWeekly
        double* yearly_profit
        double* monthly_profit
        double* weekly_profit
        double* daily_profit
        double* floating_profit
        double* use_of_leverage
        double* beta
        int* noTrades_per_week
        performance_t perf
        int month_num, year_num, day_num, week_num
        time_t perf_from_date
        time_t perf_to_date
        double min_yearly_profit_percent
        double max_drawdown_percent
        long candle_num
        int timeframe

        void setMonthlyGain(double* gains)
        void setBetaPeriod(int beta_period)

    cdef cppclass Performance1:
        Performance1()

cdef extern from "account.h":

    cdef cppclass AccountBase:
        instrument_t* instruments
        PerformanceBase* performance
        Trade* trades
        double failed_trades
        double commission
        bint commission_in_pip
        int instrument_num
        bint use_orderlog
        bint canOpen
        double min_pl_percent
        double sum_pl_percent
        time_t to_date

        AccountBase(PerformanceBase *)
        void setInstrumentNum(int num)
        void setInstruments(instrument_t* instruments, int instrument_num)
        void setInstrument(int symbol, double pipsize, char* name)
        void setDatarow(int instrument, int datarow_id, double* datarow)
        void setTimeDatarow(time_t* datarow)
        void setTimeframe(int timeframe)
        void setCandleNum(long candle_num)
        void setSlippage(float slippage)
        float getSlippage()
#        void configMM(long base_equity, double positionsize, double positionpercentorunits, bint forcesl, double maxrisk, int slactiontype, double maxmarginatentry, double marginlimitintrade, int leverage)
        void configMM(int mmPluginId, long base_equity, int leverage, double positionsize, int positionmode, bint riskmanagement, bint setsl, double maxrisk, double riskreward_rate, int maxnumoftrades, double margincall_rate, int sltype, int peakthroughplugin, double peakthroughparam, bint modifysl, int be_threshold)
        void setHolidays(long* holidays, int holidayCount)
        void resetValues()
        bint onClose(long index)
        void backtestDone()
        void buy(int symbol, long amount, double sl, double tp, double percent, int group)
        void sell(int symbol, long amount, double sl, double tp, double percent, int group)
        void buyOrHold(int symbol, long amount, double sl, double tp, double percent, int group)
        void sellOrHold(int symbol, long amount, double sl, double tp, double percent, int group)
        #void closeTrade(int position)
        void closeFirstTrade(int symbol, int group)
        void closeLastTrade(int symbol, int group)
        #void closeTradePartial(int position, int amount)
        #void closeTradePercent(int position, float percent)
        void closeAll(int group)
        void closeAllSymbol(int symbol, int group)
#        void closeAllLosing(int group)
#        void closeAmount(int symbol, long amount, int group)
        void closeAmountPercent(int symbol, double percent, int group)
        void setTakeprofit(int symbol, double takeprofit, int group)
        void setStoploss(int symbol, double stoploss, int group)
        char* getOrderlog()
        char* getTradelog()
#        void calcSerialPerformance()
        int getTradeNum()
        void show()
        double getBalance()

    cdef cppclass Account1:
        Account1(PerformanceBase *)


cdef class SimulatedAccount:
    cdef:
        AccountBase* thisptr
        PerformanceBase* performance

        void setInstrumentNum(self, int num)
        void add_instruments(self, instrument_t* i, int num)
        void setInstrument(self, int symbol, double pipsize, char* name)
        void setDatarow(self, int instrument, int datarow_id, np.ndarray[np.double_t, ndim = 1] datarow)
        void setTimeDatarow(self, np.ndarray[np.int_t, ndim = 1] datarow)
        void setTimeframe(self, int timeframe)
        int getTimeframe(self)
        void setBetaPeriod(self, int beta_period)
        void setMonthlyGain(self, np.ndarray[np.double_t, ndim = 1])
        void setDayNum(self, int day_num)
        void setCandleNum(self, int candle_num)
        void configMM(self, int id, object conf)
        void resetValues(self)
        bint onClose(self, long index)
        void backtestDone(self)
    cpdef buy(self, int symbol, int amount = ?, double sl = ?, double tp = ?, double percent = ?, int group = ?, int strategy_id = ?)
    cpdef sell(self, int symbol, int amount = ?, double sl = ?, double tp = ?, double percent = ?, int group = ?, int strategy_id = ?)
    cpdef buyOrHold(self, int symbol, int amount = ?, double sl = ?, double tp = ?, double percent = ?, int group = ?, int strategy_id = ?)
    cpdef sellOrHold(self, int symbol, int amount = ?, double sl = ?, double tp = ?, double percent = ?, int group = ?, int strategy_id = ?)
#    cpdef closeTrade(self, int position)
    cpdef closeFirstTrade(self, int symbol, int group = ?, int strategy_id = ?)
    cpdef closeLastTrade(self, int symbol, int group = ?, int strategy_id = ?)
#    cpdef closeTradePartial(self, int position, int amount)
#    cpdef closeTradePercent(self, int position, double percent)
    cpdef closeAll(self, int group = ?, int strategy_id = ?)
    cpdef closeAllSymbol(self, int symbol, int group = ?, int strategy_id = ?)
#    cpdef closeAllLosing(self, int group = ?, int strategy_id = ?)
#    cpdef closeAmount(self, int symbol, long amount, int group = ?, int strategy_id = ?)
    cpdef closeAmountPercent(self, int symbol, double percent, int group = ?, int strategy_id = ?)
    cpdef setTakeprofitForAll(self, int symbol, double takeprofit, int group = ?, int strategy_id = ?)
    cpdef setStoplossForAll(self, int symbol, double stoploss, int group = ?, int strategy_id = ?)
    cpdef show(self)
    cpdef destroy(self)
    cpdef get_performance(self)
    cpdef get_serial_performance(self)
    cpdef get_orderlog(self)
    cpdef get_tradelog(self)
    cdef set_interest(self, int symbol, double value)
    cpdef setHolidays(self, np.ndarray[np.long_t, ndim = 1] holidays, int holidayCount)
    cdef setDates(self, time_t to_date)
    cpdef use_orderlog(self, bint value)
    cpdef allowOpen(self, bint value)
