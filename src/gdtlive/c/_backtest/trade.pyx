    

cdef class cTrade:
    cdef Trade *thisptr
    
    def __cinit__(self, int symbol, int direction, time_t time, float ask, float bid,  int amount, float stoploss, float takeprofit, float commission):
        self.thisptr = new Trade(symbol, direction, time, ask, bid, amount, stoploss, takeprofit, commission)

    def __dealloc__(self, ):
        del self.thisptr

    def close(self, time_t time, float ask, float bid, int ordertype):
        self.thisptr.close(time, ask, bid, ordertype)
    
    def partialClose(self, int amount, time_t time, float ask, float bid):
        self.thisptr.partialClose(amount, time, ask, bid)

    def stoplossHit(self, time_t time, float ask, float bid):
        self.thisptr.stoplossHit(time, ask, bid)

    def takeprofitHit(self, time_t time, float ask, float bid):
        self.thisptr.takeprofitHit(time, ask, bid)

    def setStoploss(self, float ask, float bid, float stoploss):
        self.thisptr.setStoploss(ask, bid, stoploss)

    def setTakeprofit(self, float ask, float bid, float takeprofit):
        self.thisptr.setTakeprofit(ask, bid, takeprofit)

    def isTakeprofitHit(self, float ask, float bid):
        return self.thisptr.isTakeprofitHit(ask, bid)

    def isStoplossHit(self, float ask, float bid):
        return self.thisptr.isStoplossHit(ask, bid)

    def getProfit(self):
        return self.thisptr.getProfit()

    def getFloatingProfit(self, float ask, float bid):
        return self.thisptr.getFloatingProfit(ask, bid)

    def calcProfit(self, int amount, float ask, float bid):
        cdef profit_t *profit = self.thisptr.calcProfit(amount, ask, bid) 
        return {'usd' : profit.usd, 'pip':profit.pip, 'point': profit.point}

    def calcRollover(self, time_t time, int amount):
        return self.thisptr.calcRollover(time, amount)		

    property open_amount:
        def __get__(self): return self.thisptr.open_amount
    

    