#include "performance.h"
#include "constants.h"
#include <iostream>
#include <gsl/gsl_statistics_double.h>
#include "stdlib.h"
#include "time.h"
#include "string.h"
#include "math.h"
#include "ta-lib/ta_func.h"

using namespace std;


/*void debug(string identifier, int num) {
	if (Trade::current_candle != 47994) {
		return;
	}
	cout << identifier << " " << num << endl;
	if (num == 5) {
		exit(0);
	}
}*/

/*void debug2(string identifier, double param1, double param2, double param3) {
	cout << identifier << Trade::current_candle << " " << param1 << " " << param2 << " " << param3 << endl;
}*/

/*void checkIndex(string id, int index, int length) {
	if (index >= length) {
		cout << "!!! error at " << id << " >= " << length << endl;
	}
}*/

inline double minArray(double *data, long s, long e) {
	double min=data[s++];
	for (; s<=e; s++) {
		if (data[s] < min )
			min = data[s];
	}
	return min;
}

inline double maxArray(double *data, long s, long e) {
	double max=data[s++];
	for (; s<=e; s++) {
		if (data[s] > max )
			max = data[s];
	}
	return max;
}

double standard_dev(double *data, long length) {
	double avg = 0;
	double sum = 0;
	double diffFromAvg;
	int i;
	if (!length) return 0;

	for (i=0; i<length; i++) {
		avg += data[i];
	}
	avg /= (double)length;
	for (i=0; i<length; i++) {
		diffFromAvg = data[i] - avg;
		sum += diffFromAvg * diffFromAvg;
	}
	sum /= (double)length;
	return sqrt(sum);
}


PerformanceBase::PerformanceBase() {
	this->instruments = 0;
	this->timeframe = 0;
	this->candle_num = 0;
	this->day_max = 0;
	this->week_max = 0;
	this->month_max = 0;
	this->year_max = 0;
	this->perf_from_date = 0;
	this->perf_to_date = 0;
	this->begin_year = 0;
	this->begin_month = 0;
	this->tradesInCloseOrder = 0;

	this->zeroYearlies();
	this->zeroMonthlies();
	this->zeroWeeklies();
	this->zeroDailies();

	this->monthly_gains = 0;
	this->trade_end = 0;
}

void PerformanceBase::zeroYearlies() {
	this->yearly_profit = 0;
}

void PerformanceBase::allocYearlies(int year_num) {
	this->yearly_profit = (double*)malloc(sizeof(double) * year_num);
}

void PerformanceBase::clearYearlies() {
	free(this->yearly_profit);
}

void PerformanceBase::zeroMonthlies() {
	this->monthly_percents = 0;
	this->monthly_profit = 0;
	this->beta = 0;
	this->noTrades_per_month = 0;
	this->noWinningTrades_per_month = 0;
	this->noLosingTrades_per_month = 0;
}

void PerformanceBase::allocMonthlies(int month_num) {
	this->monthly_percents = (double*)malloc(sizeof(double) * month_num);
	this->monthly_profit = (double*)malloc(sizeof(double) * month_num);
	this->beta = (double*)malloc(sizeof(double) * (month_num - PerformanceBase::beta_period + 1));
	this->noTrades_per_month = (int*)malloc(sizeof(int) * month_num);
	this->noWinningTrades_per_month = (int*)malloc(sizeof(int) * month_num);
	this->noLosingTrades_per_month = (int*)malloc(sizeof(int) * month_num);
}

void PerformanceBase::clearMonthlies() {
	free(this->monthly_percents);
	free(this->monthly_profit);
	free(this->beta);
	free(this->noTrades_per_month);
	free(this->noWinningTrades_per_month);
	free(this->noLosingTrades_per_month);
}

void PerformanceBase::zeroWeeklies() {
	this->drawdownWeekly = 0;
	this->profitlossWeekly = 0;
	this->maeWeekly = 0;
	this->mfeWeekly = 0;
	this->weekly_profit = 0;
	this->noTrades_per_week = 0;
	this->noWinningTrades_per_week = 0;
	this->noLosingTrades_per_week = 0;
}

void PerformanceBase::allocWeeklies(int week_num) {
	this->drawdownWeekly = (double**)malloc(sizeof(double*) * week_num);
	this->profitlossWeekly = (double**)malloc(sizeof(double*) * week_num);
	this->maeWeekly = (double**)malloc(sizeof(double*) * week_num);
	this->mfeWeekly = (double**)malloc(sizeof(double*) * week_num);
	this->weekly_profit = (double*)malloc(sizeof(double) * week_num);
	this->noTrades_per_week = (int*)malloc(sizeof(int) * week_num);
	this->noWinningTrades_per_week = (int*)malloc(sizeof(int) * week_num);
	this->noLosingTrades_per_week = (int*)malloc(sizeof(int) * week_num);
}

void PerformanceBase::clearWeeklies() {
	free(this->drawdownWeekly);
	free(this->profitlossWeekly);
	free(this->maeWeekly);
	free(this->mfeWeekly);
	free(this->weekly_profit);
	free(this->noTrades_per_week);
	free(this->noWinningTrades_per_week);
	free(this->noLosingTrades_per_week);
}

void PerformanceBase::clearWeeklies2() {
	if (this->drawdownWeekly) {
		int i;
		for (i = 0; i <= this->week_current; i++) {
			free(this->drawdownWeekly[i]);
			this->drawdownWeekly[i] = 0;
			free(this->profitlossWeekly[i]);
			this->profitlossWeekly[i] = 0;
			free(this->maeWeekly[i]);
			this->maeWeekly[i] = 0;
			free(this->mfeWeekly[i]);
			this->mfeWeekly[i] = 0;
		}
	}
}

void PerformanceBase::zeroDailies() {
	this->account_balance = 0;
	this->daily_profit = 0;
	this->floating_profit = 0;
	this->use_of_leverage = 0;
	this->noTrades_per_day = 0;
	this->noWinningTrades_per_day = 0;
	this->noLosingTrades_per_day = 0;
}

void PerformanceBase::allocDailies(int day_num) {
	this->account_balance = (double*)malloc(sizeof(double) * day_num);
	this->daily_profit = (double*)malloc(sizeof(double) * day_num);
	this->floating_profit = (double*)malloc(sizeof(double) * day_num);
	this->use_of_leverage = (double*)malloc(sizeof(double) * day_num);
	this->noTrades_per_day = (int*)malloc(sizeof(int) * day_num);
	this->noWinningTrades_per_day = (int*)malloc(sizeof(int) * day_num);
	this->noLosingTrades_per_day = (int*)malloc(sizeof(int) * day_num);
}

void PerformanceBase::clearDailies() {
	free(this->account_balance);
	free(this->daily_profit);
	free(this->floating_profit);
	free(this->use_of_leverage);
	free(this->noTrades_per_day);
	free(this->noWinningTrades_per_day);
	free(this->noLosingTrades_per_day);
}

PerformanceBase::~PerformanceBase() {
//	 cout << "~PerformanceBase start" << endl;
	free(this->tradesInCloseOrder);
	this->tradesInCloseOrder = 0;

	this->clearYearlies();
	this->zeroYearlies();
	this->clearMonthlies();
	this->zeroMonthlies();
	this->clearWeeklies2();
	this->clearWeeklies();
	this->zeroWeeklies();
	this->clearDailies();
	this->zeroDailies();
}

void PerformanceBase::reset() {
//	cout << "reset start, day_num:" << this->day_num << endl;
	//reset scalar performances
	this->perf.net_profit = 0;
	this->perf.cumulated_profit = 0;
	this->perf.commission = 0;
	this->perf.gross_profit = 0;
	this->perf.gross_loss = 0;
	this->perf.sharpe_ratio = 0;
	this->perf.maximum_drawdown = 0;
	this->perf.max_drawdown_p = 0;
	this->perf.profit_factor = 0;
	this->perf.number_of_trades = 0;
	this->perf.winning_trades = 0;
	this->perf.losing_trades = 0;
	this->perf.percent_profitable = 0;
	this->perf.average_trade = 0;
	this->perf.average_loser = 0;
	this->perf.average_winner = 0;
	this->perf.average_wl_ratio = 0;
	this->perf.largest_winner = 0;
	this->perf.largest_loser = 0;
	this->perf.max_conseq_winners = 0;
	this->perf.max_conseq_losers = 0;
	this->perf.trades_per_day = 0;
	this->perf.avg_time_in_market = 0;
	this->perf.avg_bars_in_trade = 0;
	this->perf.profit_per_month = 0;
	this->perf.avgMFE = 0;
	this->perf.avgMAE = 0;
	this->perf.floating_profit = 0;
	this->perf.open_trades = 0;
	this->perf.profit_variance = 0;
	this->perf.min_yearly_profit_p = INFINITY;
	this->perf.min_floating_p = 0;
	this->perf.min_floating = 0;
	this->perf.avg_neg_floating_p = 0;
	this->perf.avg_pos_floating_p = 0;
	this->perf.avg_neg_floating = 0;
	this->perf.avg_pos_floating = 0;
	this->perf.max_use_of_lev = 0;
	this->perf.avg_use_of_lev = 0;
	this->perf.chance3m = 0;
	this->perf.chance1m = 0;
	this->perf.score = 0;
	this->pos_floating_candle_num = 0;
	this->neg_floating_candle_num = 0;
	this->use_of_lev_candle_num = 0;
	this->max_balance = -INFINITY;
	this->conseq_counter = 0;
	this->prev_candle_time = 0;
	this->winning_months = 0;

	struct tm* _tm = gmtime(&this->perf_from_date);
	this->begin_year = _tm->tm_year;
	this->begin_month = _tm->tm_mon;

	_tm = gmtime(&this->perf_to_date);
	int end_year = _tm->tm_year;
	int end_month = _tm->tm_mon;

	int year_num = end_year - this->begin_year + 1;
	int month_num = 12 * (end_year - this->begin_year) + end_month - this->begin_month + 1;
	int week_num = ((this->perf_to_date - this->perf_from_date) / 604800) + 1;
	if ((this->perf_to_date - 345600) % 604800 < (this->perf_from_date - 345600) % 604800) {	//from Thursday to Tuesday is 2 weeks
		week_num++;
	}

	this->clearWeeklies2();

	if (this->day_max < this->day_num) {	//reallocate memory for daily performance fields
		this->clearDailies();
		this->allocDailies(this->day_num);

		//week number can only change when day number changes
		if (this->week_max < week_num) {	//reallocate memory for weekly performance fields
			this->clearWeeklies();
			this->allocWeeklies(week_num);
			this->week_max = week_num;
		}

		//month number can only change when day number changes
		if (this->month_max < month_num) {	//reallocate memory for monthly performance fields
			this->clearMonthlies();
			this->allocMonthlies(month_num);
			this->month_max = month_num;

			//year number can only change when month number changes
			if (this->year_max < year_num) {	//reallocate memory for yearly performance fields
				this->clearYearlies();
				this->allocYearlies(year_num);
				this->year_max = year_num;
			}
		}
	}

	//set cumulative daily fields to zero
	memset(this->use_of_leverage, 0, sizeof(double) * this->day_num);
	memset(this->daily_profit, 0, sizeof(double) * this->day_num);
	memset(this->noTrades_per_day, 0, sizeof(int) * this->day_num);
	memset(this->noWinningTrades_per_day, 0, sizeof(int) * this->day_num);
	memset(this->noLosingTrades_per_day, 0, sizeof(int) * this->day_num);
	this->floating_profit[0] = INFINITY;

	this->week_num = week_num;
	//set cumulative weekly fields to zero
	memset(this->weekly_profit, 0, sizeof(double) * this->week_num);
	memset(this->noTrades_per_week, 0, sizeof(int) * this->week_num);
	memset(this->noWinningTrades_per_week, 0, sizeof(int) * this->week_num);
	memset(this->noLosingTrades_per_week, 0, sizeof(int) * this->week_num);
//	if (this->week_max) {
//		this->drawdownWeekly[0] = 0;
//		this->profitlossWeekly[0] = 0;
//		this->maeWeekly[0] = 0;
//		this->mfeWeekly[0] = 0;
//	}

	this->month_num = month_num;
	//set cumulative monthly fields to zero
	memset(this->monthly_profit, 0, sizeof(double) * this->month_num);
	memset(this->noTrades_per_month, 0, sizeof(int) * this->month_num);
	memset(this->noWinningTrades_per_month, 0, sizeof(int) * this->month_num);
	memset(this->noLosingTrades_per_month, 0, sizeof(int) * this->month_num);

	this->year_num = year_num;
	//set cumulative yearly fields to zero
	memset(this->yearly_profit, 0, sizeof(double) * this->year_num);

	this->trade_from = 0;
	this->trade_to = 0;
	this->day_current = 0;
	this->week_current = 0;
	this->month_current = 0;
	this->year_current = 0;
	this->current_balance = NAN;
//	cout << "reset end" << endl;
}

void PerformanceBase::setMonthlyGain(double *gains) {
	this->monthly_gains = gains;
}

void PerformanceBase::show() {
	cout << "net profit: " << this->perf.net_profit << endl;
	cout << "cumulated profit: " << this->perf.cumulated_profit << endl;
	cout << "gross_profit: " << this->perf.gross_profit << endl;
	cout << "gross_loss: " << this->perf.gross_loss << endl;
	cout << "commission: " << this->perf.commission << endl;
	cout << "profit factor: " << this->perf.profit_factor << endl;
	cout << "max drawdown: " << this->perf.maximum_drawdown << endl;
	cout << "sharpe ratio: " << this->perf.sharpe_ratio << endl;
	cout << "total number of trades: " << this->perf.number_of_trades << endl;
	cout << "winning trades: " << this->perf.winning_trades << endl;
	cout << "losing trades: " << this->perf.losing_trades << endl;
	cout << "percent profitable: " << this->perf.percent_profitable << endl;
	cout << "average trade: " << this->perf.average_trade << endl;
	cout << "average loser: " << this->perf.average_loser << endl;
	cout << "average winner: " << this->perf.average_winner << endl;
	cout << "ratio avgwin/avgloss: " << this->perf.average_wl_ratio << endl;
	cout << "largest winner: " << this->perf.largest_winner << endl;
	cout << "largest loser: " << this->perf.largest_loser << endl;
	cout << "max conseq winners: " << this->perf.max_conseq_winners << endl;
	cout << "max conseq losers: " << this->perf.max_conseq_losers << endl;
	cout << "# of trades per day: " << this->perf.trades_per_day << endl;
	cout << "avg time in market (h): " << this->perf.avg_time_in_market << endl;
	cout << "avg bars in trade: " << this->perf.avg_bars_in_trade << endl;
	cout << "profit per month: " << this->perf.profit_per_month << endl;
	cout << "avg MAE: " << this->perf.avgMAE << endl;
	cout << "avg MFE: " << this->perf.avgMFE << endl;
}

void PerformanceBase::setBetaPeriod(int beta_period) {
	PerformanceBase::beta_period = beta_period;
}

int PerformanceBase::beta_period = 0;

/*void PerformanceBase::coutValue(int type, double value, Trade* trade) {
	time_t ct = this->instruments[0].current_time;
	struct tm* _tm = gmtime(&ct);
	cout << "{";
	if (type == 0) {
		cout << "'c'";
	} else if (type == 1) {
		cout << "'p'";
	}
	cout << ":" << value << ",'time':'" << (1900 + _tm->tm_year) << "-";
	if (_tm->tm_mon < 9) {
		cout << "0";
	}
	cout << (1 + _tm->tm_mon) << "-";
	if (_tm->tm_mday < 10) {
		cout << "0";
	}
	cout << _tm->tm_mday << " ";
	if (_tm->tm_hour < 10) {
		cout << "0";
	}
	cout << _tm->tm_hour << ":";
	if (_tm->tm_min < 10) {
		cout << "0";
	}
	cout << _tm->tm_min << ":00'";
	if (trade) {
		cout << ",'group':" << trade->group;
	}
	cout << "}" << endl;
}*/

void PerformanceBase::onTradeOpen(Trade* trade) {
	if ((this->instruments[0].current_time < this->perf_from_date) || (this->instruments[0].current_time > this->perf_to_date)) {
		return;
	}
	double commission = trade->commission;
	this->perf.commission += commission;	//add commission for all opens
	if (this->day_current == this->day_num) {
		cout << "too many days: day_current(" << this->day_current << ") == day_num(" << this->day_num << ")" << endl;
		exit(0);
	}
	if (this->week_current == this->week_num) {
		cout << "too many weeks: week_current(" << this->week_current << ") == week_num(" << this->week_num << ")" << endl;
		exit(0);
	}
	if (this->month_current == this->month_num) {
		cout << "too many months: month_current(" << this->month_current << ") == month_num(" << this->month_num << ")" << endl;
		exit(0);
	}
	if (this->year_current == this->year_num) {
		cout << "too many months: year_current(" << this->year_current << ") == year_num(" << this->year_num << ")" << endl;
		exit(0);
	}
	this->yearly_profit[this->year_current] -= commission;
	this->monthly_profit[this->month_current] -= commission;
	this->weekly_profit[this->week_current] -= commission;
	this->daily_profit[this->day_current] -= commission;
	instrument_t* inst;
	inst = &this->instruments[trade->symbol];
	if (trade->direction == DIR_BUY) {
		trade->currency.MAE = inst->bidlow[Trade::current_candle];
		trade->currency.MFE = inst->bidhigh[Trade::current_candle];
	} else {
		trade->currency.MAE = inst->asklow[Trade::current_candle];
		trade->currency.MFE = inst->askhigh[Trade::current_candle];
	}
}

void PerformanceBase::onTradeClose(Trade* trade, double balance) {
	if ((this->instruments[0].current_time < this->perf_from_date) || (this->instruments[0].current_time > this->perf_to_date)) {
		return;
	}
	if (this->day_current == this->day_num) {
		cout << "too many days: day_current(" << this->day_current << ") == day_num(" << this->day_num << ")" << endl;
		exit(0);
	}
	if (this->week_current == this->week_num) {
		cout << "too many weeks: week_current(" << this->week_current << ") == week_num(" << this->week_num << ")" << endl;
		exit(0);
	}
	if (this->month_current == this->month_num) {
		cout << "too many months: month_current(" << this->month_current << ") == month_num(" << this->month_num << ")" << endl;
		exit(0);
	}
	if (this->year_current == this->year_num) {
		cout << "too many months: year_current(" << this->year_current << ") == year_num(" << this->year_num << ")" << endl;
		exit(0);
	}
	double gross_profit = trade->cumprofit;
	double close_profit = gross_profit - trade->close_commission + trade->rollover;
	double trade_profit = close_profit - trade->commission;
//	cout << "onTradeClose0 this->trade_from: " << this->trade_from << " this->trade_to: " << this->trade_to << " this->trade_end: " << this->trade_end << endl;
	if (this->trade_end == this->trade_to) {	//arrived at the end of the array
		if (this->trade_from > 0) {	//there is some unused space before used data
			//the empty space is smaller than dynMemAllocLimit / 2, reallocate a bigger one
			if (this->trade_from < dynMemAllocLimit / 2) {
				this->trade_end += dynMemAllocLimit;
//				cout << "onTradeClose0 before" << endl;
				this->tradesInCloseOrder = (Trade**)realloc(this->tradesInCloseOrder, sizeof(Trade*) * this->trade_end);
//				cout << "onTradeClose0 after" << endl;
			}
			//move the data to the start of the array
			int i;
			for (i = this->trade_from; i < this->trade_to; i++) {
				this->tradesInCloseOrder[i - this->trade_from] = this->tradesInCloseOrder[i];
			}
			this->trade_to -= this->trade_from;
			this->trade_from = 0;
		} else {
			this->trade_end += dynMemAllocLimit;
//			cout << "onTradeClose1 before" << endl;
			this->tradesInCloseOrder = (Trade**)realloc(this->tradesInCloseOrder, sizeof(Trade*) * this->trade_end);
//			cout << "onTradeClose1 after" << endl;
		}
	}
	this->tradesInCloseOrder[this->trade_to] = trade;
	this->trade_to++;
//	cout << "onTradeClose1 this->trade_from: " << this->trade_from << " this->trade_to: " << this->trade_to << " this->trade_end: " << this->trade_end << endl;
	this->noTrades_per_month[this->month_current]++;
	this->noTrades_per_week[this->week_current]++;
	this->noTrades_per_day[this->day_current]++;
	if (gross_profit > 0) {
		this->perf.gross_profit += gross_profit;
	} else {
		this->perf.gross_loss += gross_profit;
	}
	if (trade_profit > 0) {
		this->perf.winning_trades++;
		this->perf.largest_winner = max(this->perf.largest_winner, trade_profit);
		if (this->conseq_counter > 0) {
			this->conseq_counter++;
			this->perf.max_conseq_winners = max(this->perf.max_conseq_winners, this->conseq_counter);
		} else {
			this->conseq_counter = 1;
		}
		this->noWinningTrades_per_month[this->month_current]++;
		this->noWinningTrades_per_week[this->week_current]++;
		this->noWinningTrades_per_day[this->day_current]++;
	} else {
		this->perf.losing_trades++;
		this->perf.largest_loser = max(this->perf.largest_loser, -trade_profit);
		if (this->conseq_counter < 0) {
			this->conseq_counter--;
			this->perf.max_conseq_losers = max(this->perf.max_conseq_losers, -this->conseq_counter);
		} else {
			this->conseq_counter = -1;
		}
		this->noLosingTrades_per_month[this->month_current]++;
		this->noLosingTrades_per_week[this->week_current]++;
		this->noLosingTrades_per_day[this->day_current]++;
	}
	this->current_balance = balance;
	if (balance > this->max_balance) {
		this->max_balance = balance;
	} else if (balance < this->max_balance) {
		double drawdown = balance - this->max_balance;
		this->perf.maximum_drawdown = min(drawdown, this->perf.maximum_drawdown);
		this->perf.max_drawdown_p = min(drawdown / this->max_balance, this->perf.max_drawdown_p);
		trade->currency.drawdown = drawdown;
	}

	if (trade->direction == DIR_BUY) {
		trade->currency.MAE = trade->currency.MAE - trade->price;
		trade->currency.MFE = trade->currency.MFE - trade->close_price;
	} else {
		trade->currency.MAE = trade->price - trade->currency.MAE;
		trade->currency.MFE = trade->close_price - trade->currency.MFE;
	}
	double mult = (double)trade->amount * this->instruments[trade->symbol].pipvalue / this->instruments[trade->symbol].pipsize;
	trade->currency.MAE *= mult;
	trade->currency.MFE *= mult;

	this->perf.avgMAE += trade->currency.MAE;
	this->perf.avgMFE += trade->currency.MFE;

	this->yearly_profit[this->year_current] += close_profit;
	this->monthly_profit[this->month_current] += close_profit;
	this->weekly_profit[this->week_current] += close_profit;
	this->daily_profit[this->day_current] += close_profit;
	this->perf.commission += trade->close_commission;
	this->perf.avg_time_in_market += trade->close_time - trade->time;
}

void PerformanceBase::addWeeklies() {
	if (this->week_current == this->week_num) {
		cout << "too many weeks: week_current(" << this->week_current << ") == week_num(" << this->week_num << ")" << endl;
		exit(0);
	}
	Trade* trade;
	int i;
	int numberOfTrades = this->noTrades_per_week[this->week_current];
//	cout << "addWeeklies week_index: " << this->week_current << " numberOfTrades: " << numberOfTrades << endl;
	if (!numberOfTrades) {
		this->drawdownWeekly[this->week_current] = NULL;
		this->profitlossWeekly[this->week_current] = NULL;
		this->maeWeekly[this->week_current] = NULL;
		this->mfeWeekly[this->week_current] = NULL;
		return;
	}
//	cout << "addWeeklies0 this->trade_from: " << this->trade_from << " this->trade_to: " << this->trade_to << " this->trade_end: " << this->trade_end << endl;
//	cout << "addWeeklies this->drawdownWeekly[" << this->week_current << "]: " << this->drawdownWeekly[this->week_current] << endl;
	this->drawdownWeekly[this->week_current] = (double*)malloc(sizeof(double) * numberOfTrades);
	this->profitlossWeekly[this->week_current] = (double*)malloc(sizeof(double) * numberOfTrades);
	this->maeWeekly[this->week_current] = (double*)malloc(sizeof(double) * numberOfTrades);
	this->mfeWeekly[this->week_current] = (double*)malloc(sizeof(double) * numberOfTrades);
//	cout << "addWeeklies after malloc" << endl;
	for (i = 0; i < numberOfTrades; i++) {
		trade = this->tradesInCloseOrder[this->trade_from + i];
		this->drawdownWeekly[this->week_current][i] = trade->currency.drawdown;
		this->profitlossWeekly[this->week_current][i] = trade->cumprofit - trade->commission - trade->close_commission + trade->rollover;
		this->maeWeekly[this->week_current][i] = trade->currency.MAE;
		this->mfeWeekly[this->week_current][i] = trade->currency.MFE;
//		cout << i << ".:" << trade->currency.drawdown << " " << this->profitlossWeekly[week_index][i] << " " << this->maeWeekly[week_index][i] << " " << this->mfeWeekly[week_index][i] << " " << endl;
	}
	this->trade_from += i;
//	cout << "addWeeklies1 this->trade_from: " << this->trade_from << " this->trade_to: " << this->trade_to << " this->trade_end: " << this->trade_end << endl;
}

void PerformanceBase::onCandleClose(double expSum, double floating_profit, double balance, double leverage) {
	if ((this->instruments[0].current_time < this->perf_from_date) || (this->instruments[0].current_time > this->perf_to_date)) {
		return;
	}
	time_t current_time = this->instruments[0].current_time;
	double floating_percent = (floating_profit / balance) * 100.0;

	// debug by kulcsarb
	// struct tm* _tm = gmtime(&current_time);
	// cout << _tm->tm_mon << "."<<_tm->tm_wday<<" "<<_tm->tm_hour << " b:" << balance << " f:" << floating_profit << endl;

	this->current_balance = balance;
	this->perf.min_floating = min(this->perf.min_floating, floating_profit);
	this->perf.min_floating_p = min(this->perf.min_floating_p, floating_percent);
	if (floating_profit < 0) {
		this->perf.avg_neg_floating += floating_profit;
		this->perf.avg_neg_floating_p += floating_percent;
		this->neg_floating_candle_num++;
	}
	if (floating_profit > 0) {
		this->perf.avg_pos_floating += floating_profit;
		this->perf.avg_pos_floating_p += floating_percent;
		this->pos_floating_candle_num++;
	}
	// debug by kulcsarb
	//cout << _tm->tm_mon << "."<<_tm->tm_wday<<" "<<_tm->tm_hour << " b:" << balance << " f:" << floating_profit << " mf:" << this->perf.min_floating << " anf:" << this->perf.avg_neg_floating << " apf:" << this->perf.avg_pos_floating <<  endl;

	this->floating_profit[this->day_current] = min(this->floating_profit[this->day_current], floating_profit);
	double use_of_lev = expSum * 100 / ((balance + floating_profit) * leverage);
	this->use_of_leverage[this->day_current] = max(this->use_of_leverage[this->day_current], use_of_lev);

	if (expSum) {
		this->use_of_lev_candle_num++;
		this->perf.max_use_of_lev = max(use_of_lev, this->perf.max_use_of_lev);
		this->perf.avg_use_of_lev += use_of_lev;
	}

	if (this->prev_candle_time) {
		struct tm* time_tm = gmtime(&current_time);
		int current_time_year = time_tm->tm_year;
		int current_time_month = time_tm->tm_mon;
		int current_time_day = time_tm->tm_mday;
		int current_time_weekday = time_tm->tm_wday;

		time_tm = gmtime(&this->prev_candle_time);
		int prev_time_year = time_tm->tm_year;
		int prev_time_month = time_tm->tm_mon;
		int prev_time_day = time_tm->tm_mday;

		if (current_time_day != prev_time_day) {
//			cout << "new day: " << current_time_day << " " << balance << " " << floating_profit << " " << this->day_current << endl;
//			cout << this->account_balance << endl;
			this->account_balance[this->day_current] = balance;
			this->day_current++;
			if (this->day_current < this->day_num) {
				this->floating_profit[this->day_current] = INFINITY;
			}

			if (current_time_weekday == 1) {	//week may change only in case of day change, change on Monday
				this->addWeeklies();
				this->week_current++;
			}

			if (current_time_month != prev_time_month) {	//month may change only in case of day change
				if (this->monthly_profit[this->month_current] > 0) {
					this->winning_months++;
				}
				this->monthly_percents[this->month_current] = (balance - this->last_month_balance) * 100.0 / this->last_month_balance;
				this->last_month_balance = balance;
//				cout << "last_month_balance: " << this->last_month_balance << endl;
//				coutValue(0, balance, NULL);
//				coutValue(1, floating_profit, NULL);
				this->month_current++;

				if (current_time_year != prev_time_year) {	//year may change only in case of month change
					this->last_year_time = current_time;
					this->perf.min_yearly_profit_p = min(this->perf.min_yearly_profit_p, (balance - this->last_year_balance) / this->last_year_balance);
					this->last_year_balance = balance;
					this->perf.net_profit += this->yearly_profit[this->year_current];
					this->year_current++;
				}
			}
		} else {
		}
	}
	Trade* trade;
	instrument_t* inst;
	for (list<Trade*>::iterator it=Trade::open_trades_list.begin(); it!=Trade::open_trades_list.end(); ++it) {
		trade = *it;
		inst = &this->instruments[trade->symbol];
		if (trade->direction == DIR_BUY) {
			trade->currency.MAE = min(trade->currency.MAE, inst->bidlow[Trade::current_candle]);
			trade->currency.MFE = max(trade->currency.MFE, inst->bidhigh[Trade::current_candle]);
		} else {
			trade->currency.MAE = max(trade->currency.MAE, inst->askhigh[Trade::current_candle]);
			trade->currency.MFE = min(trade->currency.MFE, inst->asklow[Trade::current_candle]);
		}
	}
	this->prev_candle_time = current_time;
}

void PerformanceBase::onBacktestDone(double balance) {
	int i, j;
	this->current_balance = balance;
	int year_num = this->year_current + 1;
	int month_num = this->month_current + 1;
	int week_num = this->week_current + 1;
	int day_num = this->day_current + 1;
//	cout << "this->day_current: " << this->day_current << " this->day_num: " << this->day_num << " balance: " << balance << endl;
	if (this->day_num == this->day_current) {	//indexing overflow
		this->day_current--;
		day_num--;
	} else {
		this->account_balance[this->day_current] = balance;	//adding last balance
	}
	if (this->week_num == this->week_current) {
		this->week_current--;
		week_num--;
	} else {
		this->addWeeklies();	//adding last week performances
	}
	if (this->month_num == this->month_current) {
		this->month_current--;
		month_num--;
		if (this->year_num == this->year_current) {
			this->year_current--;
			year_num--;
		} else {
			this->perf.net_profit += this->yearly_profit[this->year_current];
		}
	} else {
		this->monthly_percents[this->month_current] = (balance - this->last_month_balance) / this->last_month_balance;
		this->perf.net_profit += this->yearly_profit[this->year_current];
	}

	double yearly_percent = (this->current_balance - this->last_year_balance) / this->last_year_balance;
	double elapsed_time = this->perf_to_date - this->last_year_time;
	yearly_percent *= (elapsed_time / 31536000.0);
	this->perf.min_yearly_profit_p = min(this->perf.min_yearly_profit_p, yearly_percent);
	this->perf.min_yearly_profit_p *= 100.0;
	this->perf.max_drawdown_p *= 100.0;

//	cout << "onBacktestDone this->pos_floating_candle_num: " << this->pos_floating_candle_num << " this->neg_floating_candle_num: " << this->neg_floating_candle_num << " this->use_of_lev_candle_num: " << this->use_of_lev_candle_num << endl;
	if (this->pos_floating_candle_num) {
		this->perf.avg_pos_floating = this->perf.avg_pos_floating / (double)this->pos_floating_candle_num;
		this->perf.avg_pos_floating_p = this->perf.avg_pos_floating_p / (double)this->pos_floating_candle_num;
	}
	if (this->neg_floating_candle_num) {
		this->perf.avg_neg_floating = this->perf.avg_neg_floating / (double)this->neg_floating_candle_num;
		this->perf.avg_neg_floating_p = this->perf.avg_neg_floating_p / (double)this->neg_floating_candle_num;
	}
	if (this->use_of_lev_candle_num) {
		this->perf.avg_use_of_lev = this->perf.avg_use_of_lev / (double)this->use_of_lev_candle_num;
	}

	this->perf.cumulated_profit = this->perf.gross_profit + this->perf.gross_loss;
	if (this->perf.gross_loss) {
		this->perf.profit_factor = -this->perf.gross_profit / this->perf.gross_loss;
	} else {
		this->perf.profit_factor = 0.0;
	}
	this->perf.trades_per_day = (double)this->perf.number_of_trades / (double)day_num;
	this->perf.number_of_trades = this->perf.winning_trades + this->perf.losing_trades;
	if (this->perf.number_of_trades) {
		this->perf.percent_profitable = this->perf.winning_trades / (double)this->perf.number_of_trades * 100;
		this->perf.average_trade = this->perf.cumulated_profit / (double)this->perf.number_of_trades;
		this->perf.avg_time_in_market = this->perf.avg_time_in_market / (3600.0 * (double)this->perf.number_of_trades);
		this->perf.avgMAE = this->perf.avgMAE / (double)this->perf.number_of_trades;
		this->perf.avgMFE = this->perf.avgMFE / (double)this->perf.number_of_trades;
	} else {
		this->perf.avg_time_in_market = 0.0;
		this->perf.avgMAE = 0.0;
		this->perf.avgMFE = 0.0;
	}
	if (this->perf.losing_trades) {
		this->perf.average_loser = this->perf.gross_loss / (double)this->perf.losing_trades;
	}
	if (this->perf.winning_trades) {
		this->perf.average_winner = this->perf.gross_profit / (double)this->perf.winning_trades;
	}
	if (this->perf.average_loser) {
		this->perf.average_wl_ratio = -this->perf.average_winner / this->perf.average_loser;
	}
	this->perf.profit_per_month = this->perf.net_profit / (double)month_num;	//month_num must be positive
	this->perf.profit_variance = standard_dev(this->monthly_profit, month_num);
	this->perf.sharpe_ratio = this->perf.profit_per_month / this->perf.profit_variance;
	this->perf.avg_bars_in_trade = this->perf.avg_time_in_market * 60.0 / (double)timeframe;
	this->perf.chance1m = max((this->winning_months / (double)month_num) * 100.0, 0.0);
	if (month_num >= 3) {
		int chance3Limit = 0.1;
		double sum = this->monthly_profit[0] + this->monthly_profit[1] + this->monthly_profit[2];
		this->perf.chance3m += (sum > chance3Limit) ? 1 : 0;
		for (i = 3; i < month_num; i++) {
			sum += this->monthly_profit[i] - this->monthly_profit[i - 3];
			this->perf.chance3m += (sum > chance3Limit) ? 1 : 0;
		}
		this->perf.chance3m *= (100.0 / (double)(month_num - 2));
	}
	if (month_num >= PerformanceBase::beta_period - 1) {
		TA_BETA(0, month_num, this->monthly_percents, this->monthly_gains, PerformanceBase::beta_period, &i, &j, this->beta);
	}
//	cout << "day_num: " << day_num << " this->day_num: " << this->day_num << " balance: " << balance << endl;
	if (day_num < this->day_num) {	//the backtest ended earlier, the rest must be set to zero
//		memset(this->account_balance + day_num, balance, (this->day_num - day_num) * sizeof(double));
		for (i = day_num; i < this->day_num; i++) {
			this->account_balance[i] = balance;
		}
		memset(this->floating_profit + day_num, 0, (this->day_num - day_num) * sizeof(double));
	}

	if (week_num < this->week_num) {
		memset(this->drawdownWeekly + week_num, 0, (this->week_num - week_num) * sizeof(double*));
		memset(this->profitlossWeekly + week_num, 0, (this->week_num - week_num) * sizeof(double*));
		memset(this->maeWeekly + week_num, 0, (this->week_num - week_num) * sizeof(double*));
		memset(this->mfeWeekly + week_num, 0, (this->week_num - week_num) * sizeof(double*));
	}

	if (month_num < this->month_num) {	//the months can only be fewer if there are missing days
		memset(this->monthly_percents + month_num, 0, (this->month_num - month_num) * sizeof(double));
		int beta_month_num = month_num;
		if (month_num < PerformanceBase::beta_period - 1) {
			beta_month_num = PerformanceBase::beta_period - 1;
		}
		memset(this->beta + beta_month_num - PerformanceBase::beta_period + 1, 0, (this->month_num - beta_month_num) * sizeof(double));

//		if (year_num < this->year_num) {
//		}
	}
}

Performance1::Performance1() : PerformanceBase() {
}
