#ifndef PERFORMANCE_H_
#define PERFORMANCE_H_

#include "backtest.h"
#include "Trade.h"

const int dynMemAllocLimit = 50;

struct performance {
	double net_profit;
	double cumulated_profit;
	double commission;
	double gross_profit;
	double gross_loss;
	double sharpe_ratio;
	double maximum_drawdown;
	double max_drawdown_p;
	double profit_factor;
	long number_of_trades;
	long winning_trades;
	long losing_trades;
	double percent_profitable;
	double average_trade;
	double average_loser;
	double average_winner;
	double average_wl_ratio;
	double largest_winner;
	double largest_loser;
	long max_conseq_winners;
	long max_conseq_losers;
	double trades_per_day;
	double avg_time_in_market;
	double avg_bars_in_trade;
	double profit_per_month;
	double avgMFE;
	double avgMAE;
	double floating_profit;
	long open_trades;
	double profit_variance;
	double min_yearly_profit_p;
	double min_floating_p;
	double min_floating;
	double avg_neg_floating_p;
	double avg_pos_floating_p;
	double avg_neg_floating;
	double avg_pos_floating;
	double max_use_of_lev;
	double avg_use_of_lev;
	double chance3m;
	double chance1m;
	double score;
};

typedef struct performance performance_t;


class PerformanceBase {
	int begin_year, begin_month;
	int pos_floating_candle_num, neg_floating_candle_num, use_of_lev_candle_num;	//certain type of candle counters
	long conseq_counter;	//>0: winning, <0: losing
	int year_current, week_current, day_current;
	int year_max, month_max, week_max, day_max;	//max array sizes
	int trade_from, trade_to, trade_end;

	void zeroYearlies();
	void allocYearlies(int year_num);
	void clearYearlies();

	void zeroMonthlies();
	void allocMonthlies(int month_num);
	void clearMonthlies();
	void addWeeklies();

	void zeroWeeklies();
	void allocWeeklies(int week_num);
	void clearWeeklies();
	void clearWeeklies2();

	void zeroDailies();
	void allocDailies(int day_num);
	void clearDailies();

public:
	int month_current;
	double current_balance, max_balance;
	int year_num, month_num, week_num, day_num;	//current array sizes
	time_t perf_from_date, perf_to_date;		//performance calculations' time limits
	double last_month_balance, last_year_balance;
	time_t last_year_time, last_day_time, prev_candle_time;
	long candle_num;
	int timeframe;
	int winning_months;

	performance_t perf;
	instrument_t* instruments;			//ownership at account object

	Trade** tradesInCloseOrder;
	double* monthly_gains;				//ownership at strategy object

	//yearlies
	double* yearly_profit;

	//monthlies
	double* monthly_percents, * monthly_profit, * beta;
	int* noTrades_per_month, * noWinningTrades_per_month, * noLosingTrades_per_month;

	//weeklies
	double** drawdownWeekly, ** profitlossWeekly, ** maeWeekly, ** mfeWeekly;
	double* weekly_profit;
	int* noTrades_per_week, * noWinningTrades_per_week, * noLosingTrades_per_week;

	//dailies
	double* account_balance, * floating_profit, * use_of_leverage, * daily_profit;
	int* noTrades_per_day, * noWinningTrades_per_day, * noLosingTrades_per_day;

	PerformanceBase();
	~PerformanceBase();
	void reset();

	void onTradeOpen(Trade* trade);
	void onTradeClose(Trade* trade, double balance);
	void onCandleClose(double expSum, double floating_profit, double balance, double leverage);
	void onBacktestDone(double balance);

	void show();
	void setMonthlyGain(double *gains);
	void setBetaPeriod(int beta_period);

//	void coutValue(int type, double value, Trade* trade);

	static int beta_period;
};
//void debug(string identifier, int num = 0);
//void debug2(string identifier, double param1 = 0.0, double param2 = 0.0, double param3 = 0.0);
//void checkIndex(string id, int index, int length);

class Performance1 : public PerformanceBase {
public:
	Performance1();
};


#endif /* PERFORMANCE_H_ */
