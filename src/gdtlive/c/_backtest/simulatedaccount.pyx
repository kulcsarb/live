from libc.stdlib cimport free
from libc.stdio cimport printf
from math import isnan, isinf
import cython
import traceback
import gdtlive.config


def check_nan(value):
    if isnan(value) or isinf(value):
        return 0
    else:
        return value

cdef class SimulatedAccount:

    def __cinit__(self, accPluginId, stratPluginId):
        if stratPluginId == 1 or stratPluginId == 2 or stratPluginId == 3 or stratPluginId == 4:
            self.performance = <PerformanceBase*>new Performance1()

        if accPluginId == 1:
            self.thisptr = <AccountBase*>new Account1(self.performance)

    cpdef destroy(self):
        del self.thisptr
        self.thisptr = NULL
        del self.performance
        self.performance = NULL

    cdef void setInstrumentNum(self, int num):
        self.thisptr.setInstrumentNum(num)

    cdef void setInstrument(self, int symbol, double pipsize, char* name):
        self.thisptr.setInstrument(symbol, pipsize, name)

    cdef void setDatarow(self, int instrument, int datarow_id, np.ndarray[np.double_t, ndim=1] datarow):
        self.thisptr.setDatarow(instrument, datarow_id, <double *>datarow.data)

    cdef void setTimeDatarow(self, np.ndarray[np.int_t, ndim=1] datarow):        
        self.thisptr.setTimeDatarow(<time_t *>datarow.data)

    cdef void setTimeframe(self, int timeframe):
        self.thisptr.setTimeframe(timeframe)        

    cdef int getTimeframe(self):
        return self.thisptr.performance.timeframe

    cdef void setBetaPeriod(self, int beta_period):
        self.thisptr.performance.setBetaPeriod(beta_period)

    cdef void setMonthlyGain(self, np.ndarray[np.double_t, ndim=1] gains):
        self.thisptr.performance.setMonthlyGain(<double*> gains.data)

    cdef void add_instruments(self, instrument_t* instruments, int num):
        self.thisptr.setInstruments(instruments, num)        

    cdef void setCandleNum(self, int candle_num):
        self.thisptr.setCandleNum(candle_num)

    cdef void setDayNum(self, int day_num):
        self.thisptr.performance.day_num = day_num

    cdef void configMM(self, int id, object conf):
        cdef:
            long base_equity = 0
            int leverage = 0
            double positionsize = 0
            int positionmode = 0
            bint riskmanagement = False
            bint setsl = False
            double maxrisk = 0
            double riskreward_rate = 0
            int maxnumberoftrades = 0
            double margincall_rate = 0
            int be_threshold = 0
        try:
            base_equity = conf.get('equity',10000)
            leverage = conf.get('leverage',50)
            positionsize = conf['parameters'].get('positionsize', 10000)
            positionmode = conf['parameters'].get('positionmode', 0)
            riskmanagement = conf['parameters'].get('riskmanagement', 0)
            setsl = conf['parameters'].get('setsl', 0)
            maxrisk = conf['parameters'].get('maxrisk', 0)
            riskreward_rate = conf['parameters'].get('riskreward_rate', 0)
            maxnumoftrades = conf['parameters'].get('maxnumoftrades', 0)
            margincall_rate = conf['parameters'].get('margincall_rate', 50)
            sltype = conf['parameters'].get('sltype', 0)
            peakthroughplugin = conf['parameters'].get('peakthroughplugin', 0)
            peakthroughparam = conf['parameters'].get('peakthroughparam', 0)
            modifysl = conf['parameters'].get('modifysl', 0)
            be_threshold = conf['parameters'].get('be_threshold', 0)
        except Exception as e:
            print traceback.format_exc()
        self.thisptr.configMM(id, base_equity, leverage, positionsize, positionmode, riskmanagement, setsl, maxrisk, riskreward_rate, maxnumoftrades, margincall_rate, sltype, peakthroughplugin, peakthroughparam, modifysl, be_threshold)


    cdef void resetValues(self):
        self.thisptr.resetValues()


    cdef bint onClose(self, long index):
        return self.thisptr.onClose(index)


    cdef void backtestDone(self):
        self.thisptr.backtestDone()


    cpdef buy(self, int symbol, int amount=0, double sl=0, double tp=0, double percent=1.0, int group=0, int strategy_id=0):
        self.thisptr.buy(symbol, amount, sl, tp, percent, group)


    cpdef sell(self, int symbol, int amount=0, double sl=0, double tp=0, double percent=1.0, int group=0, int strategy_id=0):
        self.thisptr.sell(symbol,amount, sl, tp, percent, group)


    cpdef buyOrHold(self, int symbol, int amount=0, double sl=0, double tp=0, double percent=1.0, int group=0, int strategy_id=0):
        self.thisptr.buyOrHold(symbol, amount, sl, tp, percent, group)


    cpdef sellOrHold(self, int symbol, int amount=0, double sl=0, double tp=0, double percent=1.0, int group=0, int strategy_id=0):
        self.thisptr.sellOrHold(symbol,amount, sl, tp, percent, group)


#    cpdef closeTrade(self, int position):
#        self.thisptr.closeTrade(position)


    cpdef closeFirstTrade(self, int symbol, int group=0, int strategy_id=0):
        self.thisptr.closeFirstTrade(symbol, group)


    cpdef closeLastTrade(self, int symbol, int group=0, int strategy_id=0):
        self.thisptr.closeLastTrade(symbol, group)


#    cpdef closeTradePartial(self, int position, int amount):
#        self.thisptr.closeTradePartial(position, amount)
#
#
#    cpdef closeTradePercent(self, int position, double percent):
#        self.thisptr.closeTradePercent(position, percent)
#

    cpdef closeAll(self, int group=0, int strategy_id=0):
        self.thisptr.closeAll(group)


    cpdef closeAllSymbol(self, int symbol, int group=0, int strategy_id=0):
        self.thisptr.closeAllSymbol(symbol, group)


#    cpdef closeAllLosing(self, int group=0, int strategy_id=0):
#        self.thisptr.closeAllLosing(group)


#    cpdef closeAmount(self, int symbol, long amount, int group=0):
#        self.thisptr.closeAmount(symbol, amount, group)


    cpdef closeAmountPercent(self, int symbol, double percent, int group=0, int strategy_id=0):
        self.thisptr.closeAmountPercent(symbol, percent, group)


    cpdef setTakeprofitForAll(self, int symbol, double takeprofit, int group=0, int strategy_id=0):
        self.thisptr.setTakeprofit(symbol, takeprofit, group)


    cpdef setStoplossForAll(self, int symbol, double stoploss, int group=0, int strategy_id=0):
        self.thisptr.setStoploss(symbol, stoploss, group)

    cpdef show(self):
        self.thisptr.show()

    cpdef get_performance(self):
        result = dict(self.thisptr.performance.perf)                                                                                  
        for k in result.iterkeys():
            result[k] = check_nan(result[k])

        return result;

    cpdef get_serial_performance(self):
        cdef:
#            int tradenum
            int i, j, day_num, week_num, month_num, year_num, trade_per_week

#        trade_num = self.thisptr.getTradeNum()
        candle_num = self.thisptr.performance.candle_num
        day_num = self.thisptr.performance.day_num
        week_num = self.thisptr.performance.week_num
        month_num = self.thisptr.performance.month_num
        year_num = self.thisptr.performance.year_num

        floating_profit = [0] * day_num
        use_of_leverage = [0] * day_num

        for i in xrange(day_num):
            floating_profit[i] = check_nan(self.thisptr.performance.floating_profit[i])
            use_of_leverage[i] = check_nan(self.thisptr.performance.use_of_leverage[i])

        maeWeekly = [0] * week_num
        mfeWeekly = [0] * week_num
        drawdownWeekly = [0] * week_num
        profitlossWeekly = [0] * week_num
        for i in xrange(week_num):
            trade_per_week = self.thisptr.performance.noTrades_per_week[i]
            if trade_per_week:
                maeWeeklyi = [0] * trade_per_week
                mfeWeeklyi = [0] * trade_per_week
                drawdownWeeklyi = [0] * trade_per_week
                profitlossWeeklyi = [0] * trade_per_week
                for j in xrange(trade_per_week):
                    maeWeeklyi[j] = check_nan(self.thisptr.performance.maeWeekly[i][j])
                    mfeWeeklyi[j] = check_nan(self.thisptr.performance.mfeWeekly[i][j])
                    drawdownWeeklyi[j] = check_nan(self.thisptr.performance.drawdownWeekly[i][j])
                    profitlossWeeklyi[j] = check_nan(self.thisptr.performance.profitlossWeekly[i][j])
            else:
                maeWeeklyi = []
                mfeWeeklyi = []
                drawdownWeeklyi = []
                profitlossWeeklyi = []
            maeWeekly[i] = maeWeeklyi
            mfeWeekly[i] = mfeWeeklyi
            drawdownWeekly[i] = drawdownWeeklyi
            profitlossWeekly[i] = profitlossWeeklyi

        balance = [0] * day_num
        daily = [0] * day_num
        weekly = [0] * week_num
        monthly = [0] * month_num
        yearly = [0] * year_num
        beta = [0] * (month_num - gdtlive.config.BETA_PERIOD + 1)

        for i in xrange(day_num):
            daily[i] = check_nan(self.thisptr.performance.daily_profit[i])
            balance[i] = check_nan(self.thisptr.performance.account_balance[i])

        for i in xrange(week_num):
            weekly[i] = check_nan(self.thisptr.performance.weekly_profit[i])

        for i in xrange(month_num):
            monthly[i] = check_nan(self.thisptr.performance.monthly_profit[i])

        for i in xrange(year_num):
            yearly[i] = check_nan(self.thisptr.performance.yearly_profit[i])

        for i in xrange(month_num - gdtlive.config.BETA_PERIOD + 1):
            beta[i] = check_nan(self.thisptr.performance.beta[i])

        result = {
                  'MAE': maeWeekly,
                  'MFE': mfeWeekly,
                  'net_profit': balance,       #net_profit := account_balance
                  'drawdown': drawdownWeekly,
                  'trades_profit_loss': profitlossWeekly,
                  'daily_profit': daily,
                  'weekly_profit': weekly,
                  'monthly_profit': monthly,
                  'yearly_profit': yearly,
                  'floating_profit': floating_profit,
                  'use_of_leverage': use_of_leverage,
                  'beta': beta
                  }
        return result

    cdef set_interest(self, int symbol, double value):
        self.thisptr.instruments[symbol].interest = value

    cpdef setHolidays(self, np.ndarray[np.long_t, ndim=1] holidays, int holidayCount):        
        self.thisptr.setHolidays(<long*> holidays.data, holidayCount)
        
    cdef setDates(self, long to_date):
        self.thisptr.to_date = <time_t>to_date

    cpdef use_orderlog(self, bint value):
        self.thisptr.use_orderlog = value

    cpdef allowOpen(self, bint value):
        self.thisptr.canOpen = value
    
    cpdef get_orderlog(self):
        cdef char* c_str = self.thisptr.getOrderlog()
        cdef bytes py_str
        try:
            py_str = c_str;
        except:
            print 'Memory is full!!!!'
        return py_str;

    cpdef get_tradelog(self):
        cdef char* c_str = self.thisptr.getTradelog()
        cdef bytes py_str
        try:
            py_str = c_str;
        except:
            print 'Memory is full!!!!'
        return py_str;

    property slippage:
        def __get__(self):
            return self.thisptr.getSlippage()
        def __set__(self, value):
            self.thisptr.setSlippage(value)

    property commission:
        def __get__(self):
            return self.thisptr.commission
        def __set__(self, value):
            self.thisptr.commission = value

    property commission_in_pip:
        def __get__(self):
            return self.thisptr.commission_in_pip
        def __set__(self, value):
            self.thisptr.commission_in_pip = value

    property balance:
        def __get__(self):
            return self.thisptr.getBalance()
