/*
 * Trade.cpp
 *
 *  Created on: 2011.04.14.
 *      Author: Sherab
 */

#include <iostream>
#include <list>
#include <stdlib.h>
#include <time.h>
#include "Trade.h"
#include "stdio.h"
#include "math.h"
#include "constants.h"

using namespace std;

Trade::Trade(int symbol, int direction, time_t time, double ask, double bid, long amount, double stoploss, double takeprofit, double commission, double exposure, int group) {
	// TODO Auto-generated constructor stub
//	cout << symbol << " " << time << " " << ask << " " << amount << endl;
	this->instrument = &Trade::instruments[symbol];

	this->id = Trade::counter++;
	this->group = group;
	this->index = Trade::current_candle;
	this->symbol = symbol;
	this->direction = direction;
	this->time = time;
	this->ask = ask;
	this->bid = bid;
	this->price = direction == DIR_BUY ? ask : bid;
	this->ts_diff = 0;
	this->amount = amount;
	this->exposure = exposure;
	this->instrument->exposure += exposure;
	this->stoploss = stoploss;
	this->takeprofit = takeprofit;
	this->commission = commission;
	this->closed = false;
	this->be_threshold = 0;
	this->cumprofit = 0;
	this->currency.drawdown = 0;
	this->currency.MAE = 0;
	this->currency.MFE = 0;
	this->rollover = 0.0;
	this->pip = 0.0;
	this->close_type = 0;
	this->close_time = 0;
	this->close_price = 0.0;
	this->close_commission = 0;
	this->close_index = 0;
	this->prev_trade = 0;
	this->next_trade = 0;

	this->apply_slippage(this->ask, this->bid);

	if (!Trade::first_trade) {
		Trade::first_trade = this;
		Trade::last_trade = this;
	} else {
		Trade::last_trade->next_trade = this;
		this->prev_trade = Trade::last_trade;
		Trade::last_trade = this;
	}
	Trade::total_net_profit -= commission;
//	cout << "commission: " << commission << endl;
	Trade::open_trades++;
	Trade::open_trades_list.push_back(this);
	this->instrument->open_trades++;
//	cout << "new trade added :" << this << " with id:" << this->id << "| first: " << Trade::first_trade->id << endl;
}

void Trade::apply_slippage(double &ask, double &bid) {
	double slippage = (double)rand();
	if (slippage) {
		slippage /= (double)RAND_MAX;
		slippage -= 0.5;
		slippage = (double)((int)(slippage * Trade::slippage * 10)) / 10;
		slippage *= this->instrument->pipsize;
	}
	ask += slippage;
	bid += slippage;
}

bool Trade::close(int ordertype, time_t time, double ask, double bid) {
	// cout << "close id:" << this->id <<" idx:" << this->index << " amount: " << this->open_amount << " sl:" << this->stoploss << "exposure:" << this->exposure << " closed:" << this->closed;
	if (this->closed) {
		return false;
	}
	if (!time) {
		time = this->instrument->current_time;
		ask = this->instrument->current_ask;
		bid = this->instrument->current_bid;
//		cout << this->instrument->current_ask << " 1. " << this->instrument->current_bid << " 2. " << this->instrument->value[index] << " 3. " << this->instrument->current_time << " 4. " << this->instrument->current_baseusd_rate << endl;
	}
	if ((ordertype == ORDERTYPE_CLOSE) || (ordertype == ORDERTYPE_FORCE_CLOSE)) {
		this->apply_slippage(ask, bid);
	}
	this->close_index = Trade::current_candle;
	this->close_type = ordertype;
	this->close_time = time;
	this->close_price = this->direction == DIR_BUY ? bid : ask;
	this->rollover = this->calcRollover(time);
	this->calcProfit(ask, bid);
	if (ordertype == ORDERTYPE_FORCE_CLOSE) {
		if (this->cumprofit > 0) {
			this->cumprofit = -1 / (this->cumprofit + 1);
		} else {
			this->cumprofit = this->cumprofit - 1;
		}
		this->pip = this->cumprofit / (this->instrument->pipvalue * (double)this->amount);
	}
	this->close_commission = this->commission;
	Trade::open_trades--;
	this->instrument->open_trades--;
	if (this->instrument->open_trades == 0) {
		this->instrument->stoploss = 0;
		this->instrument->takeprofit = 0;
	}
	this->instrument->exposure -= this->exposure;
//	cout << "close: *this->instrument->exposure: " << *this->instrument->exposure << " exposure: " << this->exposure << endl;
	this->closed = true;
	Trade::total_net_profit += this->cumprofit - this->close_commission + this->rollover;

	return true;
}

bool Trade::setStoploss(double ask, double bid, double stoploss) {
	if (isnan(stoploss)) {
		return false;
	}
	if (!stoploss) {
		return true;
	}
	if (((this->direction == DIR_BUY) && (bid <= stoploss)) || ((this->direction == DIR_SELL) && (ask >= stoploss))) {
		time_t ct = this->instrument->time[Trade::current_candle];
		struct tm* _tm = gmtime(&ct);
		cout << "setStoploss invalid stop-loss found: " << stoploss << " current " << (this->direction == DIR_BUY ? "bid": "ask") << ": " << (this->direction == DIR_BUY ? bid : ask) << " at: " << (1900 + _tm->tm_year) << "-" << (1 + _tm->tm_mon) << "-" << _tm->tm_mday << " " << _tm->tm_hour << endl;
		return false;
	}
	this->stoploss = stoploss;
	return true;
}

bool Trade::modifyStoploss(bool printError) {
	instrument_t* inst = this->instrument;
	double price, stoploss;
	double stoploss_be;
	double stoploss_msl;
	double ts_param = 0;
	if (Trade::modifyPluginParam > 0) {
		ts_param = (double)Trade::modifyPluginParam * this->instrument->pipsize;
	}
	if (this->direction == DIR_BUY) {
		stoploss_be = 0;
		stoploss_msl = 0;
		price = inst->current_bid;
		if (Trade::modifyPluginParam > 0) {
			if (price >= this->stoploss + ts_param + this->ts_diff) {
				stoploss_msl = this->stoploss + ts_param;

			}
		} else if (Trade::modifyPluginParam == -1) {
			stoploss_msl = inst->current_sl_buy;
		}
		if ((this->be_threshold) && (price >= this->be_threshold)) {
			stoploss_be = this->price + 2 * this->commission / this->amount;
			this->be_threshold = 0;
		}
		stoploss = max(stoploss_msl, stoploss_be);
		if (price <= stoploss) {
			if (printError) {
				time_t ct = this->instrument->time[Trade::current_candle];
				struct tm* _tm = gmtime(&ct);
				cout << "modifyStoploss invalid stop-loss found: " << stoploss << " current bid: " << price << " at: " << (1900 + _tm->tm_year) << "-" << (1 + _tm->tm_mon) << "-" << _tm->tm_mday << " " << _tm->tm_hour << endl;
				ct = this->time;
				_tm = gmtime(&ct);
				cout << "current stoploss: " << this->stoploss << " open price: " << this->price << " at: " << (1900 + _tm->tm_year) << "-" << (1 + _tm->tm_mon) << "-" << _tm->tm_mday << " " << _tm->tm_hour << endl;
			}
			return false;
		}
		this->stoploss = max(this->stoploss, stoploss);
//		cout << "mod price: " << price << " msl: " << stoploss_msl << " be: " << stoploss_be << " nsl: " << this->stoploss << endl;
	} else {
		stoploss_be = 1000000;
		stoploss_msl = 1000000;
		price = inst->current_ask;
		if (Trade::modifyPluginParam > 0) {
			if (price <= this->stoploss - ts_param + this->ts_diff) {
				stoploss_msl = this->stoploss - ts_param;
			}
		} else if (Trade::modifyPluginParam == -1) {
			stoploss_msl = inst->current_sl_sell;
		}
		if ((this->be_threshold) && (price <= this->be_threshold)) {
			stoploss_be = this->price - 2 * this->commission / this->amount;
			this->be_threshold = 0;
		}
		stoploss = min(stoploss_msl, stoploss_be);
		if (price >= stoploss) {
			if (printError) {
				time_t ct = this->instrument->time[Trade::current_candle];
				struct tm* _tm = gmtime(&ct);
				cout << "modifyStoploss invalid stop-loss found: " << stoploss << " current ask: " << price << " at: " << (1900 + _tm->tm_year) << "-" << (1 + _tm->tm_mon) << "-" << _tm->tm_mday << " " << _tm->tm_hour << endl;
				ct = this->time;
				_tm = gmtime(&ct);
				cout << "current stoploss: " << this->stoploss << " open price: " << this->price << " at: " << (1900 + _tm->tm_year) << "-" << (1 + _tm->tm_mon) << "-" << _tm->tm_mday << " " << _tm->tm_hour << endl;
			}
			return false;
		}
		this->stoploss = min(this->stoploss, stoploss);
//		cout << "mod id: " << this->id << " msl: " << stoploss_msl << " be: " << stoploss_be << " nsl: " << this->stoploss << endl;
	}
	return true;
}

bool Trade::setTakeprofit(double ask, double bid, double takeprofit) {
	if (isnan(takeprofit)) {
		return false;
	}
	if (!takeprofit) {
		return true;
	}
	if (((this->direction == DIR_BUY) && (bid >= takeprofit)) || ((this->direction == DIR_SELL) && (ask <= takeprofit))) {
		time_t ct = this->instrument->time[Trade::current_candle];
		struct tm* _tm = gmtime(&ct);
		cout << "setTakeprofit invalid take-profit found: " << stoploss << " current " << (this->direction == DIR_BUY ? "bid": "ask") << ": " << (this->direction == DIR_BUY ? bid : ask) << " at: " << (1900 + _tm->tm_year) << "-" << (1 + _tm->tm_mon) << "-" << _tm->tm_mday << " " << _tm->tm_hour << endl;
		return false;
	}
	this->takeprofit = takeprofit;
	return true;
}

bool Trade::isTakeprofitHit(double ask, double bid) {
//	cout << "TP " << this->id << " 1. " << ask << " 2. " << bid << " 3. " << (this->direction == DIR_BUY) << " 4. " << this->takeprofit << endl;
	return (this->takeprofit && ((this->direction == DIR_BUY && bid >= this->takeprofit) || (this->direction == DIR_SELL && ask <= this->takeprofit)));
}

bool Trade::isStoplossHit(double ask, double bid) {
//	if (this->stoploss && ((this->direction == DIR_BUY && bid <= this->stoploss) || (this->direction == DIR_SELL && ask >= this->stoploss ))) {
//		time_t ct = this->instrument->time[Trade::current_candle];
//		struct tm* _tm = gmtime(&ct);
//		cout << (1900 + _tm->tm_year) << "-" << (1 + _tm->tm_mon) << "-" << _tm->tm_mday << " " << _tm->tm_hour << " stop-loss for " << (this->direction == DIR_BUY ? "buy" : "sell") << " at: " << this->stoploss << " " << (this->direction == DIR_BUY ? "bid" : "ask") << ": " << (this->direction == DIR_BUY ? bid : ask) << endl;
//	}
	return (this->stoploss && ((this->direction == DIR_BUY && bid <= this->stoploss) || (this->direction == DIR_SELL && ask >= this->stoploss ) ));
}

void Trade::calcProfit(double ask, double bid) {
	this->pip = (this->direction == DIR_BUY ? bid - this->price : this->price - ask) / this->instrument->pipsize;
	this->cumprofit = this->pip * (double)this->amount * this->instrument->pipvalue;
//	cout << "buy: " << (this->direction == DIR_BUY) << " price: " << this->price << " price diff: " << (this->direction == DIR_BUY ? bid - this->price : this->price - ask) << " pipsize: " << this->instrument->pipsize << " amount: " << this->amount << " pipvalue: " << this->instrument->pipvalue << " cumprofit: " << this->cumprofit << endl;
}

double Trade::calcRollover(time_t time) {
	//										amount	  *		instrument interest	   * time diff in seconds / (open price  * seconds in a year)
	return this->instrument->interest ? (this->amount * this->instrument->interest * (time - this->time)) / (this->price * 31536000.0) : 0;
}

double Trade::getFloatingProfit() {
	if (this->closed) {
		return 0.0;
	}
	return ((this->direction == DIR_BUY ? this->instrument->current_bid - this->price : this->price - this->instrument->current_ask) / this->instrument->pipsize) * (double)this->amount * this->instrument->pipvalue;
}

/**
 * Returns the minimum floating profit for the current candle.
 */
double Trade::getFloatingProfitMin() {
	if (this->closed) {
		return 0.0;
	}
	return ((this->direction == DIR_BUY ? this->instrument->bidlow[Trade::current_candle] - this->price : this->price - this->instrument->askhigh[Trade::current_candle]) / this->instrument->pipsize) * (double)this->amount * this->instrument->pipvalue;
}

void Trade::print() {
	char time_str[20];
	strftime(time_str, 20, "%Y-%m-%d %H:%M", localtime(&this->time));
	cout << this->id << "\t" <<this->instrument->name << "\t" << time_str << "\t" << (this->direction==DIR_BUY ? "BUY" : "SELL") << "\t" << this->price << "\t" << this->amount << "\t" <<
		"\t" << this->amount << "\t" << this->stoploss << "\t" << this->takeprofit << endl;
	strftime(time_str, 20, "%Y-%m-%d %H:%M", localtime(&this->close_time));
	cout << "\t\t\t\t\t\t\t\t" << time_str << "\t" << this->close_price << "\t" << this->pip << "\t" << (this->cumprofit - this->commission - this->close_commission + this->rollover) << endl;
}

void Trade::toString(string *str) {
	char buffer[100];
	char sl[50], tp[50], cm[50], ro[50];
	//cout << "toString" << endl;

	this->stoploss ? sprintf(sl, "%.5f", this->stoploss) : sprintf(sl, "%d", 0);
	this->takeprofit ? sprintf(tp, "%.5f", this->takeprofit) : sprintf(tp, "%d", 0);
	this->commission ? sprintf(cm, "%.2f", this->commission) : sprintf(cm, "%d", 0);
	sprintf(buffer, "(%ld,\"%s\",%d,%ld,%.5f,%ld,%s,%s,%s)", this->id, this->instrument->name, this->direction, this->time, this->price, this->amount, sl, tp, cm);
	//printf("<%s,", buffer);
	str->append("(");
	str->append(buffer);
	str->append(",");
	this->close_commission ? sprintf(cm, "%.2f", this->close_commission) : sprintf(cm, "%d", 0);
	this->rollover ? sprintf(ro, "%.2f", this->rollover) : sprintf(ro, "%d", 0);
	sprintf(buffer, "(%d,%ld,%.5f,%.1f,%.1f,%s,%s,%d)", this->close_type, this->close_time, this->close_price, this->pip, this->cumprofit - this->commission - this->close_commission + this->rollover, cm, ro, this->group);
	//printf("<%s>", buffer);
	str->append(buffer);
	str->append(")");
	//printf(">\n");
	//cout << "<" << *str << ">" << endl;
}

Trade::~Trade() {
}


long Trade::counter = 0;
double Trade::slippage = 3.5;
long Trade::current_candle = 0;
instrument_t *Trade::instruments = 0;
Trade* Trade::first_trade = 0;
Trade* Trade::last_trade = 0;
long Trade::open_trades = 0;
double Trade::total_net_profit = 0;
list<Trade*> Trade::open_trades_list;
int Trade::modifyPluginParam = 0;
