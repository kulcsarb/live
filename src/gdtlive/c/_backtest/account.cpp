/*
 * account.cpp
 *
 *  Created on: Apr 15, 2011
 *      Author: 
 */

#include <iostream>
#include <list>
#include <set>
#include <algorithm>
#include "math.h"
#include "time.h"
#include "stdio.h"
#include "account.h"
#include "Trade.h"
#include "constants.h"
#include "stdlib.h"
#include "string.h"
#include "locale.h"

using namespace std;


#define ORDERLOG(type, symbol, time, price, amount) \
	if (this->use_orderlog) this->addOrder(type, symbol, time, price);

bool isClosed(Trade *t) {return t->closed==true;}

AccountBase::AccountBase(PerformanceBase* performance) {
	setlocale(LC_NUMERIC, "POSIX");
	this->instruments = 0;
	this->instrument_num = 0;
	this->trades = 0;
	this->commission = 0.0;
	this->commission_in_pip = false;
	this->failed_trades = 0.0;
	this->use_orderlog = false;
	this->performance = performance;
	this->mm_plugin = 0;

	Trade::slippage = 0.0;
	Trade::instruments = instruments;
	Trade::counter = 0;
}

void AccountBase::resetValues() {
//	cout << "resetValues start" << endl;
	int i;
	srand (time (0));
	this->destroyTrades();

	for (i = 0; i < this->instrument_num; i++) {
		this->instruments[i].exposure = 0.0;
		this->instruments[i].open_trades = 0;
		this->instruments[i].stoploss = 0;
		this->instruments[i].takeprofit = 0;
		this->instruments[i].current_ask = 0;
		this->instruments[i].current_bid = 0;
		this->instruments[i].current_time = 0;
		this->instruments[i].current_baseusd_rate = 0;
	}

	this->orderlog = "";
	this->orderid = 0;
	this->canOpen = true;
	this->currentHoliday = 0;
	this->checkClosable = true;
	order.ids.clear();
	order.symbols.clear();
	order.executed = false;
	order.action[0] = 0;
	order.apicall[0] = 0;
	order.time = 0;
	order.group = -1;
	Trade::total_net_profit = 0;

	if (this->mm_plugin) {
		this->performance->current_balance = this->mm_plugin->base_equity;
		this->performance->last_month_balance = this->mm_plugin->base_equity;
		this->performance->last_year_balance = this->mm_plugin->base_equity;
	}
//	cout << "last_month_balance: " << this->performance->last_month_balance << endl;
	this->performance->instruments = this->instruments;
	this->performance->last_year_time = this->instruments[0].time[0];
	this->performance->last_day_time = this->instruments[0].time[0];
	if (this->performance->perf_from_date < this->instruments[0].time[0]) {
		this->performance->perf_from_date = this->instruments[0].time[0];
	}
	if (this->performance->perf_to_date > this->instruments[0].time[this->performance->candle_num - 1]) {
		this->performance->perf_to_date = this->instruments[0].time[this->performance->candle_num - 1];
	}
	this->performance->reset();
//	cout << "resetValues end" << endl;
}

AccountBase::~AccountBase() {
	// cout << "~AccountBase start" << endl;
	free(this->instruments);
	this->instruments = 0;
	delete this->mm_plugin;
	this->mm_plugin = 0;
	this->destroyTrades();
}

void AccountBase::destroyTrades() {
	Trade* t, * trade = this->trades;
	while (trade) {
		t = trade->next_trade;
		delete trade;
		trade = t;
	};
	this->trades = 0;
	Trade::counter = 0;
	Trade::open_trades = 0;
	Trade::current_candle = 0;
	Trade::first_trade = 0;
	Trade::last_trade = 0;
	Trade::open_trades_list.clear();
}

void AccountBase::setInstrumentNum(int instrument_num) {
	this->instrument_num = instrument_num;
	if (!this->instruments) {
		this->instruments = (instrument_t*)calloc(1, sizeof(instrument_t) * instrument_num);
	} else {
		this->instruments = (instrument_t *)realloc(this->instruments, sizeof(instrument_t) * instrument_num);
	}
	Trade::instruments = this->instruments;
}

void AccountBase::setInstrument(int symbol, double pipsize, char* name) {
	if (this->instruments && this->instrument_num > symbol) {
		this->instruments[symbol].symbol = symbol;
		this->instruments[symbol].pipsize = pipsize;
		this->instruments[symbol].name = name;
	}
}

void AccountBase::setSlippage(float slippage) {
	Trade::slippage = slippage;
}

float AccountBase::getSlippage() {
	return Trade::slippage;
}

void AccountBase::setDatarow(int symbol, int datarow_id, double* datarow) {
	if (this->instruments && this->instrument_num > symbol) {
		switch (datarow_id) {
			case 1: this->instruments[symbol].askopen = datarow; break;
			case 2: this->instruments[symbol].askhigh = datarow; break;
			case 3: this->instruments[symbol].asklow = datarow; break;
			case 4: this->instruments[symbol].askclose = datarow; break;
			case 5: this->instruments[symbol].bidopen = datarow; break;
			case 6: this->instruments[symbol].bidhigh = datarow; break;
			case 7: this->instruments[symbol].bidlow = datarow; break;
			case 8: this->instruments[symbol].bidclose = datarow; break;
			case 9: this->instruments[symbol].volume = datarow; break;
			case 10: this->instruments[symbol].value = datarow; break;
			case 11: this->instruments[symbol].baseusd_rate = datarow; break;
		}
	}
}

void AccountBase::setTimeDatarow(time_t *datarow) {
	for (int i=0; i< this->instrument_num; i++) {
		this->instruments[i].time = datarow;
	}
}

void AccountBase::setInstruments(instrument_t *instruments, int instrument_num) {
	this->instruments = instruments;
	this->instrument_num = instrument_num;
	Trade::instruments = instruments;
}

void AccountBase::setCandleNum(long candle_num) {
	this->performance->candle_num = candle_num;
	this->mm_plugin->initPeakThrough(this->instrument_num, this->instruments, this->performance->candle_num);
}

void AccountBase::setTimeframe(int timeframe) {
	this->performance->timeframe = timeframe;
}

void AccountBase::configMM(int mmPluginId, long base_equity, int leverage, double positionsize, int positionmode, bool riskmanagement, bool setsl, double maxrisk, double riskreward_rate, int maxnumoftrades, double margincall_rate, int sltype, int peakthroughplugin, double peakthroughparam, bool modifysl, int be_threshold) {
//	cout << "configMM " << mmPluginId << " " << base_equity << " " << leverage << " " << positionsize << " " << positionmode << " " << riskmanagement << " " << setsl << " " << maxrisk << " " << riskreward_rate << " " << maxnumoftrades << " " << margincall_rate << " " << sltype << " " << peakthroughplugin << " " << peakthroughparam << " " << modifysl << endl;
	delete this->mm_plugin;
	this->mm_plugin = new NormalMMPlugin(this);
	this->mm_plugin->configure(base_equity, leverage, positionsize, positionmode, riskmanagement, setsl, maxrisk, riskreward_rate, maxnumoftrades, margincall_rate, sltype, peakthroughplugin, peakthroughparam, modifysl, be_threshold);
	if (peakthroughplugin > 2) {
		Trade::modifyPluginParam = peakthroughparam;
	} else if (peakthroughplugin) {
		Trade::modifyPluginParam = -1;
	}
}

void AccountBase::setHolidays(long* holidays, int holidayCount) {
	this->holidays = holidays;
	this->holidayCount = holidayCount;
}

/**
 * Evaluation functionality after each candle.
 */
bool AccountBase::onClose(long index) {
	int i;
	//setting current values for further calculations
	Trade::current_candle = index;
	instrument_t* inst;
	Trade* trade;
//	cout << "onClose " << index << endl;
	for (i = 0; i < this->instrument_num; i++) {
		inst = &this->instruments[i];
		inst->pipvalue = inst->value[index];
		inst->current_ask = inst->askclose[index];
		inst->current_bid = inst->bidclose[index];
		inst->current_time = inst->time[index];
		inst->current_baseusd_rate = inst->baseusd_rate[index];
		this->mm_plugin->setCurrents(inst, i * this->performance->candle_num + index);
	}
	time_t ct = this->instruments[0].current_time;

	//checking if time state is changing
	if (this->currentHoliday < this->holidayCount) {
//		cout << this->currentHoliday << " " << this->holidayCount << " " << this->holidays[this->currentHoliday + 1] << " " << ct << " " << ctime(&ct);
		if (!this->mm_plugin->holiday) {	//HOLIDAY_PRE_TIME hours before the next holiday
			if (this->holidays[this->currentHoliday] - ct <= HOLIDAY_PRE_TIME) {
				tm* currTimeTm = localtime(&ct);
				//if the current hour is HOLIDAY_CHANGE_LEVERAGE, reducing leverage
				if (currTimeTm->tm_hour == HOLIDAY_CHANGE_LEVERAGE) {
					this->mm_plugin->setHoliday(true);
					this->currentHoliday++;
//					cout << "changed to reduced leverage at " << ctime(&ct);
				}
			}
		} else {
			if (this->holidays[this->currentHoliday] <= ct) {	//holiday ends
				this->mm_plugin->setHoliday(false);
				this->currentHoliday++;
//				cout << "leverage increased at " << ctime(&ct);
			}
		}
	}

	double floating_profit = this->getFloatingProfit();
	double balance = this->getBalance();
	if (this->mm_plugin->margincall_rate && (floating_profit < 0 && floating_profit <= -1 * this->mm_plugin->margincall_rate * balance)) {
//		cout << "margin call: margincall_rate: " << this->mm_plugin->margincall_rate << " floating_profit: " << floating_profit << " balance: " << balance << endl;
		if (use_orderlog) {
			sprintf(order.action, "floating profit below allowed minimum, closing all losing trades.");
			order.group = -1;
			this->addOrder();
		}
		if (!this->closeAllLosing()) {
			this->closeAll();
		}
		Trade::open_trades_list.remove_if(isClosed);
		floating_profit = this->getFloatingProfit();
		balance = this->getBalance();
	}
	if (balance + floating_profit <= 0) {
//		cout << "die: floating_profit: " << floating_profit << " balance: " << balance << endl;
		if (use_orderlog) {
			sprintf(order.action, "balance <= 0, account died.");
			order.group = -1;
			this->addOrder();
		}
		this->closeAll();
		Trade::open_trades_list.remove_if(isClosed);
		return true;
	}
	//cout << floating_profit << " " << balance << " " << this->mm_plugin->getLeverage() << " " << Trade::open_trades << " " << this->expSum() << endl;

	//check if the current use of leverage is more than 150%
	if ((balance + this->getFloatingProfitMin()) * this->mm_plugin->getLeverage() * 1.5 < this->expSum()) {
		double maxExp;		//the maximum found exposure for instruments
		instrument_t* maxInst = 0;	//the instrument that's exposure is the biggest absolute
		int direction;		//the direction of trades to close for the current instrument
		this->checkClosable = false;	//do not need check if trades can be closed
		while ((balance + floating_profit) * this->mm_plugin->getLeverage() * 0.9 < this->expSum()) {		//more modifier trades are needed
//			cout << floating_profit << " " << balance << " " << this->mm_plugin->getLeverage() << " " << Trade::open_trades << " " << this->expSum() << endl;
			maxExp = -INFINITY;
			for (i = 0; i < this->instrument_num; i++) {	//search for the biggest exposure
				if (fabs(this->instruments[i].exposure) > maxExp) {
					maxInst = &this->instruments[i];
					maxExp = fabs(maxInst->exposure);
				}
			}
//			cout << "maxExp: " << maxExp << " Trade::open_trades: " << Trade::open_trades << endl;
			direction = (maxInst->exposure > 0) ? DIR_BUY : DIR_SELL;
			for (list<Trade*>::iterator it=Trade::open_trades_list.begin(); it!=Trade::open_trades_list.end(); ++it) {
				trade = *it;
				inst = &this->instruments[trade->symbol];
				//the trade's instrument and direction is fixed
//				cout << "closing trade with id: " << trade->id << endl;
				if ((inst == maxInst) && (trade->direction == direction)) {
//					cout << "closing trade with id: " << trade->id << endl;
					this->closeTrade(trade);
					if (use_orderlog) {
						sprintf(order.action, "closed because of margin call");
						sprintf(order.apicall, "onClose(%ld)", index);
						order.symbols.insert(inst->symbol);
						order.ids.push_back(trade->id);
						order.group = trade->group;
						this->addOrder();
					}
					break;
				}
			}
			Trade::open_trades_list.remove_if(isClosed);
			floating_profit = this->getFloatingProfit();
			balance = this->getBalance();
			if (balance + floating_profit <= 0) {
				if (use_orderlog) {
					sprintf(order.action, "balance <= 0, account died.");
					order.group = -1;
					this->addOrder();
				}
				this->closeAll();
				Trade::open_trades_list.remove_if(isClosed);
				return true;
			}
//			cout << "this->expSum(): " << this->expSum() << endl;
		}
		this->checkClosable = true;	//need to check if trades can be closed
	}

	this->performance->onCandleClose(this->expSum(), this->getFloatingProfitMin(), balance, this->mm_plugin->getLeverage());
	double hit_price = 0.0;
	//stop loss and take profit calculations
	//cout << "processCandle " << index << endl;
	for (list<Trade*>::iterator it=Trade::open_trades_list.begin(); it!=Trade::open_trades_list.end(); ++it) {
		trade = *it;
		inst = &this->instruments[trade->symbol];
		//stop loss
		if (trade->isStoplossHit(inst->askhigh[index], inst->bidhigh[index]) || trade->isStoplossHit(inst->asklow[index], inst->bidlow[index]) ) {
			hit_price = trade->stoploss;

			if (trade->direction == DIR_BUY && inst->bidopen[index] < trade->stoploss) {
				hit_price = inst->bidopen[index];
//				cout << "SLHIT with GAP:" << (trade->stoploss - hit_price) / inst->pipsize << endl;
			}
			if (trade->direction == DIR_SELL && inst->askopen[index] > trade->stoploss) {
				hit_price = inst->askopen[index];
//				cout << "SLHIT with GAP:" << (hit_price - trade->stoploss)  / inst->pipsize << endl;
			}

			// cout << "stoplossHit " << trade->id << " at " << trade->stoploss << " profit:" << trade->getFloatingProfit() << endl;
			if (this->closeTrade(trade, ORDERTYPE_CLOSE_BY_SL, inst->current_time, hit_price, hit_price) && use_orderlog) {
				order.symbols.insert(inst->symbol);
				order.ids.push_back(trade->id);
				sprintf(order.apicall, "StoplossHit(%f)", trade->stoploss);
				order.group = trade->group;
				this->addOrder();
			}
			continue;
		}
		//take profit
		if (trade->isTakeprofitHit(inst->askhigh[index], inst->bidhigh[index]) || trade->isTakeprofitHit(inst->asklow[index], inst->bidlow[index])) {
			hit_price = trade->takeprofit;

			if (trade->direction == DIR_BUY && inst->bidopen[index] > trade->takeprofit) {
				hit_price = inst->bidopen[index];
//				cout << "TPHIT with GAP:" << (hit_price - trade->takeprofit) / inst->pipsize << endl;
			}
			if (trade->direction == DIR_SELL && inst->askopen[index] < trade->takeprofit) {
				hit_price = inst->askopen[index];
//				cout << "TPHIT with GAP:" << (trade->takeprofit - hit_price)  / inst->pipsize << endl;
			}

			// cout << "takeprofitHit " << trade->id << " at " << trade->takeprofit << " profit:" << trade->getFloatingProfit() << endl;
			if (this->closeTrade(trade, ORDERTYPE_CLOSE_BY_TP, inst->current_time, hit_price, hit_price) && use_orderlog) {
				order.symbols.insert(inst->symbol);
				order.ids.push_back(trade->id);
				sprintf(order.apicall, "TakeprofitHit(%f)", trade->takeprofit);
				order.group = trade->group;
				this->addOrder();
			}
			continue;
		}
		//punish trades that have not been closed until MAX_TRADE_TIME time
		if (inst->current_time - trade->time >= MAX_TRADE_TIME) {
//			cout << "force close opened at: " << ctime(&trade->time) << " closed at: " << ctime(&inst->current_time) << " amount: " << trade->amount << " id: " << trade->id << endl;
			if (closeTrade(trade, ORDERTYPE_FORCE_CLOSE) && use_orderlog) {
				order.symbols.insert(inst->symbol);
				order.ids.push_back(trade->id);
				sprintf(order.action, "force closed because of too long time in market");
				order.group = trade->group;
				this->addOrder();
			}
		}
	}
	Trade::open_trades_list.remove_if(isClosed);

	floating_profit = this->getFloatingProfit();
	balance = this->getBalance();
	if (this->mm_plugin->margincall_rate && (floating_profit < 0 && floating_profit <= -1 * this->mm_plugin->margincall_rate * balance)) {
		if (use_orderlog) {
			sprintf(order.action, "floating profit below allowed minimum, closing all losing trades.");
			order.group = -1;
			this->addOrder();
		}
		if (!this->closeAllLosing()) {
			this->closeAll();
		}
		Trade::open_trades_list.remove_if(isClosed);
		floating_profit = this->getFloatingProfit();
		balance = this->getBalance();
	}
	if (balance + floating_profit <= 0) {
		if (use_orderlog) {
			sprintf(order.action, "balance <= 0, account died.");
			order.group = -1;
			this->addOrder();
		}
		this->closeAll();
		Trade::open_trades_list.remove_if(isClosed);
		return true;
	}

	if (this->mm_plugin->modifysl_any) {
//		struct tm* _tm;
		for (list<Trade*>::iterator it=Trade::open_trades_list.begin(); it!=Trade::open_trades_list.end(); ++it) {
			trade = *it;
			inst = trade->instrument;
//			_tm = gmtime(&ct);
//			if ((_tm->tm_year + 1900 == 2003) && (_tm->tm_mon + 1 == 12) && (_tm->tm_mday == 15)) {
//				cout << "trade stoploss: " << trade->stoploss << " current stoploss: " << (trade->direction == DIR_BUY ? inst->current_sl_buy : inst->current_sl_sell) << endl;
//			}
			if (!trade->modifyStoploss(false)) {
				if (trade->isStoplossHit(inst->askhigh[index], inst->bidhigh[index]) || trade->isStoplossHit(inst->asklow[index], inst->bidlow[index]) ) {
					// cout << "stoplossHit " << trade->id << " at " << trade->stoploss << " profit:" << trade->getFloatingProfit() << endl;
					if (this->closeTrade(trade, ORDERTYPE_CLOSE_BY_SL, inst->current_time, trade->stoploss, trade->stoploss) && use_orderlog) {
						order.symbols.insert(inst->symbol);
						order.ids.push_back(trade->id);
						sprintf(order.apicall, "StoplossHit(%f)", trade->stoploss);
						order.group = trade->group;
						this->addOrder();
					}
				}
			}
		}
		Trade::open_trades_list.remove_if(isClosed);
	}

	//the strategy can no more open trades at the end for a certain time (constants.POSTLOAD_DAYS days)
	if (ct >= this->to_date) {
		this->canOpen = false;
//		struct tm* _tm = gmtime(&ct);
//		cout << "open_trades: " << Trade::open_trades << " " << (1900 + _tm->tm_year) << "-" << (1 + _tm->tm_mon) << "-" << _tm->tm_mday << " " << _tm->tm_hour << endl;
	}

	return !(this->canOpen || Trade::open_trades);
}


void AccountBase::backtestDone() {
//	cout << "backtestDone start" << endl;
	this->performance->onBacktestDone(this->getBalance());
//	cout << "backtestDone end, exposure: " << this->expSum() << " open_trades: " << Trade::open_trades << endl;
}


void AccountBase::open(int symbol, int direction, long amount, double sl, double tp, double percent, int group) {
	if ((!this->canOpen) || (Trade::current_candle + 1 == this->performance->candle_num)) {
			return;
		}

	instrument_t* instrument = &this->instruments[symbol];

	time_t next_time = instrument->time[Trade::current_candle + 1];
	struct tm* time_tm = gmtime(&next_time);
	if (time_tm->tm_wday == 0) {
		return;//can't trade on Sundays
	}

	if (use_orderlog) {
		sprintf(order.apicall, "%s(%s,%ld,%.5f,%.5f,%ld,%.5f,%.5f,%.2f)", (direction == DIR_BUY ? "Buy" : "Sell"), instrument->name, (long)instrument->current_time, instrument->current_ask, instrument->current_bid, amount, sl, tp, percent);
		order.group = group;
	}
	if ((this->mm_plugin->maxnumoftrades > 0) && (this->mm_plugin->maxnumoftrades == Trade::open_trades)) {
		// cout << "too many open trades" << endl;
		if (use_orderlog) {
			sprintf(order.action, "rejected: too many open trades");
			this->addOrder();
		}
		return;
	}

	amount = this->mm_plugin->calc_amount(instrument, Trade::current_candle, Trade::total_net_profit, direction, instrument->current_ask, instrument->current_bid, &sl, &tp, percent);
	if (isnan(sl)) {
		if (use_orderlog) {
			sprintf(order.action, "rejected: stop loss is invalid");
			this->addOrder();
		}
		return;
	}

//	cout << "open:" << " " << amount << " unit, dir:" << direction << " stoploss: " << sl << " takeprofit: " << tp << " bid: " << instrument->current_bid << " ask:" << instrument->current_ask  << endl;

	if (!amount || amount < MINIMUM_AMOUNT) {
		if (use_orderlog) {
			if (amount < MINIMUM_AMOUNT) {
				sprintf(order.action, "rejected: too small amount");
			} else {
				sprintf(order.action, "rejected by money management");
			}
			this->addOrder();
		}
		return;
	}

	double exposure = this->calculate_exp(direction, amount, instrument);
	if (((this->getBalance() + this->getFloatingProfit()) * this->mm_plugin->getLeverage() * 0.9) < this->expSum(instrument, direction) + fabs(exposure)) {
//		cout << "balance: " << this->getBalance() << " floating:" << this->getFloatingProfit() << " levelrage: " << this->mm_plugin->getLeverage() << " currExp: " << ((this->getBalance() + this->getFloatingProfit()) * this->mm_plugin->getLeverage() * 0.9) << " expsum: " << this->expSum(instrument, direction) << " exp: " << fabs(exposure) <<  endl;
		if (use_orderlog) {
			sprintf(order.action, "rejected: insufficient exposure");
			this->addOrder();
		}
		return;
	}

	double commission = this->commission_in_pip ? amount * instrument->pipvalue * this->commission : this->commission;
//	cout << " amount: " << amount << " pipvalue: " << instrument->pipvalue << " commission: " << commission << endl;

//	cout << symbol << " " << direction << " " << instrument->current_time << " " << instrument->current_ask << " " << instrument->current_bid << " " << amount << " " << sl << " " << tp << " " << commission << " " << exposure << " " << group << endl;
	if ((sl) && (((direction == DIR_BUY) && (instrument->current_ask <= sl)) || ((direction == DIR_SELL) && (instrument->current_bid >= sl)))) {
		sl = this->mm_plugin->calc_invalidSl(instrument, Trade::current_candle, Trade::total_net_profit, amount, direction);
	}
	Trade* trade = 0;
	trade = new Trade(symbol, direction, instrument->current_time, instrument->current_ask, instrument->current_bid, amount, sl, tp, commission, exposure, group);
	if (this->mm_plugin->be_threshold) {
		trade->be_threshold = trade->price + (direction == DIR_BUY ? 1 : -1) * (2 * commission / amount + this->mm_plugin->be_threshold * instrument->pipsize);
//		cout << "open price: " << trade->price << " buy: " << (direction == DIR_BUY ? 1 : 0) << " 2cm: " << (2 * commission / amount) << " beThrs: " << (this->mm_plugin->be_threshold * instrument->pipsize) << " be: "<< trade->be_threshold;
	}
	if (this->mm_plugin->peakthroughplugin > 2) {
		trade->ts_diff = trade->price - trade->stoploss;
//		cout << " sl:" << trade->stoploss << " ts: "<< trade->ts_diff << endl;
	}
//	cout << "open id: " << trade->id << " " << direction << " " << instrument->current_time << " " << instrument->current_ask << " " << instrument->current_bid << " " << amount << " " << exposure << " " << endl;
	if (use_orderlog) {
		order.symbols.insert(symbol);
		order.ids.push_back(trade->id);
		//sprintf(order.action,"opened a new trade");
		this->addOrder();
	}

	if (trade && !this->trades)
		this->trades = trade;

	this->performance->onTradeOpen(trade);
}

void AccountBase::buy(int symbol, long amount, double sl, double tp, double percent, int group) {
	this->open(symbol, DIR_BUY, amount, sl, tp, percent, group);
}


void AccountBase::sell(int symbol, long amount, double sl, double tp, double percent, int group) {
	this->open(symbol, DIR_SELL, amount, sl, tp, percent, group);
}

void AccountBase::openOrHold(int symbol, int direction, long amount, double sl, double tp, double percent, int group) {
	if (Trade::open_trades > 0) {
		if (Trade::open_trades_list.back()->direction == direction) {
			return;
		} else {
			this->closeAll(group);
		}
	}
	this->open(symbol, direction, amount, sl, tp, percent, group);
}

void AccountBase::buyOrHold(int symbol, long amount, double sl, double tp, double percent, int group) {
	this->openOrHold(symbol, DIR_BUY, amount, sl, tp, percent, group);
}


void AccountBase::sellOrHold(int symbol, long amount, double sl, double tp, double percent, int group) {
	this->openOrHold(symbol, DIR_SELL, amount, sl, tp, percent, group);
}

void AccountBase::closeFirstTrade(int symbol, int group) {
	Trade *trade;
	for (list<Trade*>::iterator it=Trade::open_trades_list.begin(); it!=Trade::open_trades_list.end(); ++it) {
		trade = *it;
		if (trade->symbol == symbol and trade->group==group) {
			if (use_orderlog) order.ids.push_back(trade->id);
			this->closeTrade(trade);
			break;
		}
	}
	Trade::open_trades_list.remove_if(isClosed);
	if (use_orderlog) {
		if (!order.ids.empty()) {
			order.symbols.insert(symbol);
			sprintf(order.action, "closed first trade");
		}
		sprintf(order.apicall, "closeFirstTrade()");
		order.group = group;
		this->addOrder();
	}
}

void AccountBase::closeLastTrade(int symbol, int group) {
	Trade *trade;
	for (list<Trade*>::reverse_iterator it=Trade::open_trades_list.rbegin(); it!=Trade::open_trades_list.rend(); ++it) {
		trade = *it;
		if (trade->symbol == symbol and trade->group==group) {
			if (use_orderlog) order.ids.push_back(trade->id);
			this->closeTrade(trade);
			break;
		}
	}
	Trade::open_trades_list.remove_if(isClosed);
	if (use_orderlog) {
		if (!order.ids.empty()) {
			order.symbols.insert(symbol);
			sprintf(order.action, "closed last trade");
		}
		sprintf(order.apicall, "CloseLastTrade()");
		order.group = group;
		this->addOrder();
	}
}


void AccountBase::closeAll(int group) {
	Trade *trade;
	// cout << "closeAll" << endl;
	this->checkClosable = false;	//no need for checking, because we close all trades
	for (list<Trade*>::iterator it=Trade::open_trades_list.begin(); it!=Trade::open_trades_list.end(); ++it) {
		trade = *it;
		// cout << trade->id << " " << trade->group << endl;
		if (trade->group == group) {
			this->closeTrade(trade);
			if (use_orderlog) {
				order.ids.push_back(trade->id);
				order.symbols.insert(trade->symbol);
			}
		}
	}
	Trade::open_trades_list.remove_if(isClosed);
	if (use_orderlog) {
		if (!order.ids.empty()) {
			sprintf(order.action, "closed %ld trades", (long)order.ids.size());
		}
		sprintf(order.apicall, "CloseAll()");
		order.group = group;
		this->addOrder();
	}
	this->checkClosable = true;
}

void AccountBase::closeAllSymbol(int symbol, int group) {
	Trade* trade;
	this->checkClosable = false;	//no need to check because exposure is based on instrument
	for (list<Trade*>::iterator it=Trade::open_trades_list.begin(); it!=Trade::open_trades_list.end(); ++it) {
		trade = *it;
		if (trade->symbol == symbol and trade->group==group) {
			this->closeTrade(trade);
			if (use_orderlog) {
				order.ids.push_back(trade->id);
			}
		}
	}
	Trade::open_trades_list.remove_if(isClosed);
	if (use_orderlog) {
		if (!order.ids.empty()) {
			order.symbols.insert(symbol);
			sprintf(order.action, "closed %ld trades", (long)order.ids.size());
		}
		sprintf(order.apicall, "CloseAllSymbol(%s)", instruments[symbol].name);
		order.group = group;
		this->addOrder();
	}
	this->checkClosable = true;
}

/**
 * Closes all loosing trades if possible.
 * 	if it fails because of exposure problems, it does not close anything and returns false
 */
bool AccountBase::closeAllLosing() {
	Trade* losingTrades[Trade::open_trades];	//an array to collect losing trades
	int count = 0;
	Trade* trade;
	double intrumentExps[this->instrument_num];
	int i;
	for (i = 0; i < this->instrument_num; i++) {	//check exposure sum of the non-losing trades
		intrumentExps[i] = 0.0;
	}
	//iterate over trades
	for (list<Trade*>::iterator it=Trade::open_trades_list.begin(); it!=Trade::open_trades_list.end(); ++it) {
		trade = *it;
		// cout << "trade: " << trade->id << ", profit: " << trade->getFloatingProfit() << endl; 
		if (trade->getFloatingProfit() < 0) {	//losing trade to add
			losingTrades[count] = trade;
			count++;
		} else {		//non-losing trade to check exposure for the rest
			intrumentExps[trade->symbol] += trade->exposure;
		}
	}
	double sum = 0;
	for (i = 0; i < this->instrument_num; i++) {	//check exposure sum of the non-losing trades
		sum += fabs(intrumentExps[i]);
	}
	if ((this->getBalance() + this->getFloatingProfit()) * this->mm_plugin->getLeverage() * 0.9 < sum) {
		return false;
	}
	this->checkClosable = false;	//check has been already done
	for (i = 0; i < count; i++) {
		trade = losingTrades[i];
		this->closeTrade(trade);
		if (use_orderlog) {
			order.ids.push_back(trade->id);
			order.symbols.insert(trade->symbol);
		}
	}
	Trade::open_trades_list.remove_if(isClosed);
	if (use_orderlog) {
		if (!order.ids.empty()) {
			sprintf(order.action, "closed %d losing trades", (int)order.ids.size());
		}
		sprintf(order.apicall, "CloseAllLosing()");
		order.group = -1;
		this->addOrder();
	}
	this->checkClosable = true;
	return true;
}

/**
 * Closes the first "percent" percent of the amount of the given symbol.
 * 	if the current close fails because of exposure problems, it breaks the loop
 */
void AccountBase::closeAmountPercent(int symbol, double percent, int group) {
	long amount = (long)((double)this->instruments[symbol].exposure * 0.01 * percent);
	long orig_amount = amount;
	Trade *trade;
	for (list<Trade*>::iterator it=Trade::open_trades_list.begin(); it!=Trade::open_trades_list.end(); ++it) {
		trade = *it;
		if (trade->symbol == symbol and trade->group==group) {
			if (amount > trade->amount) {
				if (!this->closeTrade(trade)) {
					break;
				}
				amount -= trade->amount;
				if (use_orderlog) order.ids.push_back(trade->id);
			} else {
				if (!this->closeTrade(trade)) {
					break;
				}
				if (use_orderlog) order.ids.push_back(trade->id);
				break;
			}
		}
	}
	Trade::open_trades_list.remove_if(isClosed);
	if (use_orderlog) {
		if (!order.ids.empty()) {
			order.symbols.insert(symbol);
			sprintf(order.action, "closed %ld units, affected %ld trades", orig_amount, (long)order.ids.size());
		}
		sprintf(order.apicall, "CloseAmountPercent(%s, %.2f)", instruments[symbol].name, percent);
		order.group = group;
		this->addOrder();
	}

}


void AccountBase::setTakeprofit(int symbol, double takeprofit, int group) {
	Trade *trade;
	for (list<Trade*>::iterator it=Trade::open_trades_list.begin(); it!=Trade::open_trades_list.end(); ++it) {
		trade = *it;
		if (trade->symbol == symbol and trade->group==group) {
			trade->setTakeprofit(trade->instrument->current_ask, trade->instrument->current_bid, takeprofit);
			if (use_orderlog) order.ids.push_back(trade->id);
		}
	}
	if (use_orderlog) {
		if (!order.ids.empty()) {
			order.symbols.insert(symbol);
			sprintf(order.action, "Stoploss reached at %.5f for %ld trades ", instruments[symbol].stoploss, (long)order.ids.size());
		}
		sprintf(order.apicall, "SetTakeprofit(%s, %.5f)", instruments[symbol].name, takeprofit);
		order.group = group;
		this->addOrder();
	}
}


void AccountBase::setStoploss(int symbol, double stoploss, int group) {
	Trade *trade;
	for (list<Trade*>::iterator it=Trade::open_trades_list.begin(); it!=Trade::open_trades_list.end(); ++it) {
		trade = *it;
		if (trade->symbol == symbol and trade->group==group) {
			trade->setStoploss(trade->instrument->current_ask, trade->instrument->current_bid, stoploss);
			if (use_orderlog) order.ids.push_back(trade->id);
		}
	}
	if (use_orderlog) {
		if (!order.ids.empty()) {
			order.symbols.insert(symbol);
			sprintf(order.action, "Stoploss reached at %.5f for %ld trades ", instruments[symbol].stoploss, (long)order.ids.size());
		}
		sprintf(order.apicall, "SetStoploss(%s, %.5f)", instruments[symbol].name, stoploss);
		order.group = group;
		this->addOrder();
	}
}


void AccountBase::addOrder() {
	char id[10];
	set<int>::iterator it;
//	cout <<"addOrder" << endl;
	string ids_str = "[";
	order.executed = !order.ids.empty();
	while (!order.ids.empty()) {
		if (ids_str.length()>1) {
			sprintf(id, ",%ld", (long)order.ids.front());
		}
		else {
			sprintf(id, "%ld", (long)order.ids.front());
		}
		order.ids.pop_front();
		ids_str += id;
	}
	ids_str += "]";
	string symbols_str = "";
	for (it=order.symbols.begin(); it!=order.symbols.end(); it++) {
		if (symbols_str.length()) {
			sprintf(id, ",%s", instruments[*it].name);
		} else {
			sprintf(id, "%s", instruments[*it].name);
		}
		symbols_str += id;
	}
	char *buffer = (char*)calloc(1, sizeof(char)*500+ids_str.length());
	sprintf(buffer, "(%ld,%ld,'%s',%d,'%s','%s','%s','%d')", this->orderid, (long)this->instruments[0].current_time, order.apicall, order.executed, symbols_str.c_str(), order.action, ids_str.c_str(), order.group);
//	cout << buffer << endl;
	if (this->orderid++) {
		this->orderlog += ",";
		this->orderlog += buffer;
	} else {
		this->orderlog += buffer;
	}
	free(buffer);

	order.ids.clear();
	order.symbols.clear();
	order.executed = false;
	order.action[0] = 0;
	order.apicall[0] = 0;
	order.time = 0;
	order.group = -1;
}


double AccountBase::getFloatingProfit() {
	double profit = 0;
	for (list<Trade*>::iterator it=Trade::open_trades_list.begin(); it!=Trade::open_trades_list.end(); ++it) {
		profit += (*it)->getFloatingProfit();
	}

	return profit;
}

/**
 * Returns the minimum floating profit for the current candle.
 */
double AccountBase::getFloatingProfitMin() {
	double profit = 0;
	for (list<Trade*>::iterator it=Trade::open_trades_list.begin(); it!=Trade::open_trades_list.end(); ++it) {
		profit += (*it)->getFloatingProfitMin();
	}

	return profit;
}


double AccountBase::getBalance() {
	return this->mm_plugin->base_equity + Trade::total_net_profit;
}


double AccountBase::getEquity() {
	return this->mm_plugin->base_equity + this->getFloatingProfit();
}


void AccountBase::show() {
	Trade *trade = this->trades;
	cout << "Account tradelog : " << endl;
	while (trade) {
		trade->print();
		trade = trade->next_trade;
	}
	cout << "Profit :" << Trade::total_net_profit << endl;
}

char* AccountBase::getOrderlog() {
	return (char*)this->orderlog.c_str();
}

char* AccountBase::getTradelog() {
	this->tradelog =  "(";
	string t="";
	Trade* trade = this->trades;
	while (trade) {
		t = "";
		trade->toString(&t);
		this->tradelog += t;
		// ha nincs ott a , a vegen, akkor az megkavarja a literal_evalt 1 trade eseten,
		// es elnyeli a kulso zarojelet. Igy megtarta, 1 elemu tupleként, es tobb trade eseten sem jelent gondot
		//if (trade->next_trade) {
		this->tradelog += ",";
		//}
		trade = trade->next_trade;
	}
	this->tradelog += ")";
	//cout << "Tradelog: " << endl;
	//cout << this->tradelog << endl;
	//cout << "ctradelog1 : " << endl;
	//printf("%s\n", (char*)this->tradelog.c_str());
	return (char*)this->tradelog.c_str();
}

long AccountBase::getTradeNum() {
	return Trade::counter;
}

/**
 * Checks if a trade can be closed and closes it in case yes.
 */
bool AccountBase::closeTrade(Trade* trade, int ordertype, time_t time, double ask, double bid) {
//	cout << "closeTrade checkClosable: " << this->checkClosable << " trade->direction: " << trade->direction << " trade->exposure: " << trade->exposure << endl;
	if (this->checkClosable) {
		int direction = (trade->direction == DIR_SELL) ? DIR_BUY : DIR_SELL;
		double tradeExp = trade->exposure;
		double sum = this->expSum(trade->instrument, direction) + fabs(tradeExp);
		if ((this->getBalance() + this->getFloatingProfit()) * this->mm_plugin->getLeverage() * 0.9 < sum) {
//			time_t ct = trade->instrument->time[Trade::current_candle];
//			struct tm* _tm = gmtime(&ct);
//			cout << (1900 + _tm->tm_year) << "-" << (1 + _tm->tm_mon) << "-" << _tm->tm_mday << " " << _tm->tm_hour << " trade close failed; balance: " << this->getBalance() << " floating: " << this->getFloatingProfit() << " current exp 90%: " << ((this->getBalance() + this->getFloatingProfit()) * this->mm_plugin->getLeverage() * 0.9) << " expsum: " << this->expSum(trade->instrument, direction) << " trade exp: " << tradeExp << endl;
			return false;
		}
	}

	if (trade->close(ordertype, time, ask, bid)) {
		this->performance->onTradeClose(trade, this->getBalance());
//		cout << "close id: " << trade->id << " " << trade->instrument->current_time << endl;
	}
//	if ((ordertype == ORDERTYPE_CLOSE_BY_TP) && (trade->cumprofit - trade->close_commission + trade->rollover) < 0) {
//		time_t ct = this->instruments[0].current_time;
//		cout << "cumprofit: " << trade->cumprofit << " close_commission: " << trade->close_commission << " rollover: " << trade->rollover << " takeprofit: " << trade->takeprofit << " ask: " << ask << " bid: " << bid << " direction == DIR_BUY: " << (trade->direction == DIR_BUY) << " open-price: " << trade->price << " run-time: " << (trade->close_time - trade->time) << " " << this->instruments[0].current_time << " " << ctime(&ct);
//	}
	return true;
}

/**
 * Counts exposure sum for all instruments.
 * 	If aInst and direction arguments are set, it supposes the sum for a new trade in a given instrument and direction.
 *
 * @param aInst
 * If set: the address of the instrument we're interested in.
 *
 * @param direction
 * If set, this means exposure sum in the given direction for the given instrument.
 */
double AccountBase::expSum(instrument_t* aInst, int direction) {
	double sum = 0.0;
	int i;
	instrument_t* inst;
	for (i = 0; i < this->instrument_num; i++) {
		inst = &this->instruments[i];
		if ((aInst) && (inst == aInst)) {
			if (direction == DIR_BUY) {	//in case of buy the current exposure of this instrument is added
				sum += inst->exposure;
			} else {					//in case of sell the current exposure of this instrument is substracted
				sum -= inst->exposure;
			}
		} else {
			sum += fabs(inst->exposure);
		}
	}
	return sum;
}

/**
 * Calculates exposure for a new trade.
 */
double AccountBase::calculate_exp(int direction, long amount, instrument_t* instrument) {
	double exposure = amount;
	if (strncmp(instrument->name + sizeof(char) * 3, "USD", 3) == 0) {	//last currency is USD
		if (direction == DIR_BUY) {
			exposure = exposure * instrument->current_ask;
		} else {
			exposure = exposure * instrument->current_bid;
		}
	} else if (strncmp(instrument->name, "USD", 3) != 0) {				//neither currency is USD
		exposure = exposure * instrument->current_baseusd_rate;
	}	//else strncmp(instrument->name, "USD", 3) == 0, so exposure = exposure * 1;
	if (direction == DIR_SELL) {
		exposure = -exposure;			//negative if sell
	}
	return exposure;
}

Account1::Account1(PerformanceBase* performance) : AccountBase(performance) {
}
