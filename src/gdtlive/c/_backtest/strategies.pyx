# -*- encoding: utf-8 -*-
from gdtlive.c._backtest.simulatedaccount cimport SimulatedAccount, instrument_t
cimport libc.stdlib as stdlib
from gdtlive.constants import TIMEFRAME, PRICE_BID, PRICE_ASK, PRICE_OPEN, PRICE_PIPVALUE, PRICE_BASEUSD_RATE, PRICE_TIME, PRICE_BIDASK, PRICE_BIDOPEN, PRICE_BIDHIGH, PRICE_BIDLOW, PRICE_BIDCLOSE, PRICE_ASKOPEN, PRICE_ASKHIGH, PRICE_ASKLOW, PRICE_ASKCLOSE, PIP_MULTIPLIER
import gdtlive.config
cimport numpy as np
import numpy
import time
import calendar
from datetime import datetime, date, timedelta
import gdtlive.store.db as db
import logging
import gdtlive.backtest.pipvalue as pipvalue
import multiprocessing
import gdtlive.historic.datarow
from gdtlive.evol.gnodes.GenomeBase import GTree

__docformat__ = 'restructuredtext en'

log = logging.getLogger('gdtlive.strategies')

cdef class StrategyBase(object):
    '''
    Stratégia objektum, mely gyertyánként kiértékel egy GTree alapú stratégiát        
    '''

    cdef:
        SimulatedAccount account
        object instruments
        readonly object candles
        object instrument_ids
        int candle_num
        object candles_np
        object candles_gn
        object monthly_gain_np
        object _from_date
        object _start_date
        object _to_date
        public object available_from
        public object available_to
        readonly object spread_config
        object spread_config_rows
        object rollover_config
        object interest_rates
        object session
        int pid
        int startIndex
        object holidays
        public object datarows
        bint force_spread
        bint history_loaded
        bint start_date_changed
        bint perf_date_changed
        bint session_open
        bint weekend
        object holiday
        int holidayCount
        object symbol


    def __cinit__(self, session, accPluginId, mmPluginId, opener = None):
        self.instruments = []
        self.instrument_ids = {}
        self._from_date = None
        self._start_date = None
        self._to_date = None
        self.available_from = None
        self.available_to = None
        self.candles = {}
        self.spread_config = db.SpreadConfig('default', 0.0, 2.0, False)
        self.spread_config.id = 0
        self.spread_config_rows = {}
        self.rollover_config = db.RolloverConfig('default')
        self.rollover_config.id = 0
        self.interest_rates = {}
        self.session = session
        self.pid = multiprocessing.current_process().pid
        self.datarows = {}
        self.force_spread = True
        self.history_loaded = False
        self.start_date_changed = True
        self.perf_date_changed = True
        self.holiday = None
        self.weekend = False





    def __dealloc__(self):
        if self.account:
            self.account.destroy()

    def load_historic(self):
        '''Betölti az előzőleg felvett historikus adatsorokat, és előkészül a kiértékelésre

        A konfiguráció végeztével, az első `evaluate` elött hivandó

        Feladatai:
        - betölti az adatsorokat
        - módosítja az adatsorokat az aktuális spread beállítások alapján
        - kiszámolja az adatosorokhoz tartozó pip értékeket
        - kiszámolja az instrumentumok rollover értékeit
        - beállítja a szimulált számlát
        '''

        if len(self.datarows) == 0:
            raise Exception('No datarows added!')

        if self.to_date <= self.from_date:
            raise Exception('Something fucked up:  to_date <= from_date!')

        for datarow in self.datarows.itervalues():
            self.load_datarow(datarow)

        self.prepare_historic_data()

        self.calculate_pipvalues()

        self.setCandles()

        self.setup_rollover()

        self.history_loaded = True

    def evaluate(self, encoded_genome):
        '''
        Evaluates the strategy on the loaded historical datarows
        Must be overridden
        '''
        if not self.history_loaded:
            self.load_historic()

        if self.start_date_changed:
            self.resetDates()

        if self.perf_date_changed:
            self.resetPerfDates()

    def load_datarow(self, datarow):
        '''Betölti a megadott adatsorhoz tartozó árfolyamadatokat

        :Parameters:
            datarow : DatarowDescriptor
                a betöltendő adatsor leirója
        '''
        cdef int i
        cdef int startDelIndex
        result = gdtlive.historic.datarow.load(datarow.id, self.from_date - timedelta(days = gdtlive.config.PRELOAD_DAYS), self.to_date + timedelta(days = gdtlive.config.POSTLOAD_DAYS))

        if len(result[PRICE_TIME]):
            log.info('%d - Datarow %s %s loaded from %s to %s, %d candle' % (self.pid, datarow.symbol, TIMEFRAME[datarow.timeframe_num], result[PRICE_TIME][0], result[PRICE_TIME][-1], len(result[PRICE_TIME])))
        else:
            log.info('%d - No candle loaded for datarow %s %s' % (self.pid, datarow.symbol, TIMEFRAME[datarow.timeframe_num]))

        #    Az adatsor szures következtében az adatsorokból hiányozhatnak egyes gyertyák, amik más adatsorokban ott vannak.
        # ekkor bár az adatsorok eleje és a vége ugyanazm, a hosszuk eltérő, és/vagy ugyanolyan hosszúak, de egyes gyertyáik eltérőek.

        if len(self.candles.keys()) > 0:
            log.info('synchronizing with already loaded datarows')
            symbol = self.candles.keys()[0]
            try:
                i = 0
                while True:
                    while self.candles[symbol][PRICE_TIME][i] != result[PRICE_TIME][i]:
                        if self.candles[symbol][PRICE_TIME][i] < result[PRICE_TIME][i]:
                            # a már betöltött adatsorokban van plusz gyertya, ami nincs meg az aktuálisan betöltöttben
                            # ezért ezt töröljük az összes betöltött közül
                            for s in self.candles.keys():
                                for price in self.candles[s].keys():
                                    del self.candles[s][price][i]
                        else:
                            # az új adatsorban van egy gyertya, aminek nem kéne ott lennie
                            for price in result.keys():
                                del result[price][i]
                    i += 1
            except IndexError:
                #tervezett kilépési pont a ciklusból
                if len(self.candles[symbol][PRICE_TIME]) > len(result[PRICE_TIME]):
                    startDelIndex = len(result[PRICE_TIME])
                    for s in self.candles.keys():
                        for price in self.candles[s].keys():
                            del self.candles[s][price][startDelIndex:]
                elif len(self.candles[symbol][PRICE_TIME]) < len(result[PRICE_TIME]):
                    startDelIndex = len(self.candles[symbol][PRICE_TIME])
                    for price in result.keys():
                        del result[price][startDelIndex:]

        self.candles[datarow.symbol.encode()] = result

    def add_instruments(self, instrument_list):
        # itt kell majd megjegyezni a név-érték párokat, és a dns-ekben ennek megfelelően lecserélni
        # a symbolum neveket id-kre.
        # létre kell hozni len darad instrument-es tömböt     
        self.instruments = instrument_list    
        self.instrument_ids = {}
        self.account.setInstrumentNum(len(instrument_list))                                
        for i, symbol in enumerate(instrument_list):
            self.instrument_ids[symbol] = i
            self.account.setInstrument(i, PIP_MULTIPLIER.get(symbol,0.0001), symbol)

    def add_instrument(self, symbol):                
        self.instruments.append(symbol)

    def configMM(self, id, config):
        self.account.configMM(id, config)

    def prepare_historic_data(self):
        '''A betöltött adatsorokat módosítja a spread beállításokkal'''
        #több instrumentum esetén adatsor javitás
        #bizonyos esetekben a betöltött adatsorok adott nap 0h, vagy 1h-tól indulnak, ami kiértékelési problémákhoz vezet
        #ezért meg kell bizonyosodni róla, hogy azonos időpontban kezdődnek.
        cdef int i = 0
        first_time = None
        for symbol in self.candles.iterkeys():
            if not first_time:
                first_time = self.candles[symbol][PRICE_TIME][0]
            elif first_time < self.candles[symbol][PRICE_TIME][0]:
                first_time = self.candles[symbol][PRICE_TIME][0]
        for symbol in self.candles.iterkeys():
            while self.candles[symbol][PRICE_TIME][0] < first_time:
                for price in self.candles[symbol].iterkeys():
                    del self.candles[symbol][price][0]

        if len(self.candles.keys()) > 1:
            log.info('multiple datarows loaded, checking for inconsistent data')
            try:
                symbol = self.candles.keys()[0]
                for i in xrange(len(self.candles[symbol][PRICE_TIME])):                
                    d = self.candles[symbol][PRICE_TIME][i]
                    for s in self.candles.keys():
                        assert d == self.candles[s][PRICE_TIME][i]
            except AssertionError, e:
                log.error('Datarows not in sync, revision needed for the synchronizing algorithm')
                log.error('candle %d.  %s, %s != %s, %s' % (i, symbol, d, s, self.candles[s][PRICE_TIME][i]))
                raise AssertionError('candle %d.  %s, %s != %s, %s' % (i, symbol, d, s, self.candles[s][PRICE_TIME][i]))
            log.info('datarows in sync')

        #spread rászámolás
        for datarow in self.datarows.itervalues():
            instrument_spread = self.spread_config_rows.get(datarow.symbol, None)

            if not datarow.symbol in self.candles:
                raise RuntimeError('WTFF???')

            candles = self.candles[datarow.symbol]
            datarow_len = len(candles[PRICE_TIME])
            for i in range(datarow_len):
                candle_time = candles[PRICE_TIME][i]

                if instrument_spread:
                    if instrument_spread.exception_to > 0 and candle_time.hour >= instrument_spread.exception_from and candle_time.hour <= instrument_spread.exception_to:
                        spread = instrument_spread.exception_spread
                    else:
                        spread = instrument_spread.spread
                else:
                    spread = self.spread_config.default_spread

                spread *= PIP_MULTIPLIER.get(datarow.symbol, 0.0001)

                if datarow.price_type == PRICE_ASK:
                    candles[PRICE_BIDOPEN][i] = candles[PRICE_ASKOPEN][i] - spread
                    candles[PRICE_BIDHIGH][i] = candles[PRICE_ASKHIGH][i] - spread
                    candles[PRICE_BIDLOW][i] = candles[PRICE_ASKLOW][i] - spread
                    candles[PRICE_BIDCLOSE][i] = candles[PRICE_ASKCLOSE][i] - spread
                elif datarow.price_type == PRICE_BID:
                    candles[PRICE_ASKOPEN][i] = candles[PRICE_BIDOPEN][i] + spread
                    candles[PRICE_ASKHIGH][i] = candles[PRICE_BIDHIGH][i] + spread
                    candles[PRICE_ASKLOW][i] = candles[PRICE_BIDLOW][i] + spread
                    candles[PRICE_ASKCLOSE][i] = candles[PRICE_BIDCLOSE][i] + spread
                elif datarow.price_type == PRICE_BIDASK:
                    if self.force_spread and instrument_spread:
                        candles[PRICE_BIDOPEN][i] = candles[PRICE_ASKOPEN][i] - spread
                        candles[PRICE_BIDHIGH][i] = candles[PRICE_ASKHIGH][i] - spread
                        candles[PRICE_BIDLOW][i] = candles[PRICE_ASKLOW][i] - spread
                        candles[PRICE_BIDCLOSE][i] = candles[PRICE_ASKCLOSE][i] - spread
        self.account.setBetaPeriod(gdtlive.config.BETA_PERIOD)
        self.to_date = min(self.to_date, self.candles[symbol][PRICE_TIME][-1] - timedelta(days = gdtlive.config.POSTLOAD_DAYS))

    def resetDates(self):
        symbol = self.candles.keys()[0]
        #the strategy can be activated from this index, earlier candles are for indicators
        self.startIndex = 0
        for candle_time in self.candles[symbol][PRICE_TIME]:
            if candle_time < self.start_date:
                self.startIndex += 1
            else:
                break

        holidays = []  #collect holidays from the historic datarows
        #holiday is when time between two candles are longer than the timeframe (holidays are filtered)
        timeframe = timedelta(minutes = self.datarows[self.datarows.keys()[0]].timeframe_num)
        lastDate = self.candles[symbol][PRICE_TIME][self.startIndex]
        currDate = lastDate
        for i in xrange(self.startIndex + 1, len(self.candles[symbol][PRICE_TIME])):
            if self.candles[symbol][PRICE_TIME][i] - self.candles[symbol][PRICE_TIME][i - 1] > timeframe:
                #time difference between the current and the last candle is more than a candle
                holidays.append(calendar.timegm(self.candles[symbol][PRICE_TIME][i - 1].timetuple()))
                holidays.append(calendar.timegm(self.candles[symbol][PRICE_TIME][i].timetuple()))
            elif self.candles[symbol][PRICE_TIME][i] > self.to_date:
                break

        self.holidays = numpy.array(holidays, dtype = numpy.long)
        self.account.setHolidays(self.holidays, len(holidays))
        self.start_date_changed = False

    def resetPerfDates(self):
        cdef int day_num = 0
        cdef int startPerfIndex = 0
        symbol = self.candles.keys()[0]
        #the strategy can be activated from this index, earlier candles are for indicators
        for candle_time in self.candles[symbol][PRICE_TIME]:
            if candle_time < self.perf_from_date:
                startPerfIndex += 1
            else:
                break

        timeframe = timedelta(minutes = self.datarows[self.datarows.keys()[0]].timeframe_num)
        lastDate = self.candles[symbol][PRICE_TIME][startPerfIndex]
        currDate = lastDate
        perf_to_date = self.perf_to_date
        for i in xrange(startPerfIndex + 1, len(self.candles[symbol][PRICE_TIME])):
            if self.candles[symbol][PRICE_TIME][i] - self.candles[symbol][PRICE_TIME][i - 1] > timeframe or self.candles[symbol][PRICE_TIME][i] > perf_to_date:
                #time difference between the current and the last candle is more than a candle
#                if self.candles[symbol][PRICE_TIME][i] <= perf_to_date:
                currDate = self.candles[symbol][PRICE_TIME][i - 1]
#                else:
#                    currDate = perf_to_date
                day_num += (currDate - lastDate).days + 1   #+1 because datarow holidays and around 9-10 pm
                #+1 if end time of the day is smaller than start time of the day (.days means floor)
                if ((currDate.hour * 60 + currDate.minute) * 60 + currDate.second) * 60 + currDate.microsecond < ((lastDate.hour * 60 + lastDate.minute) * 60 + lastDate.second) * 60 + lastDate.microsecond:
                    day_num += 1
#                print 'lastDate, currDate, day_num', lastDate, currDate, day_num
                lastDate = self.candles[symbol][PRICE_TIME][i]
                if self.candles[symbol][PRICE_TIME][i] > perf_to_date:
                    currDate = False
                    break
        if currDate != False:
            currDate = self.candles[symbol][PRICE_TIME][len(self.candles[symbol][PRICE_TIME]) - 1]
            if lastDate != currDate:
                day_num += (currDate - lastDate).days + 1
                if ((currDate.hour * 60 + currDate.minute) * 60 + currDate.second) * 60 + currDate.microsecond < ((lastDate.hour * 60 + lastDate.minute) * 60 + lastDate.second) * 60 + lastDate.microsecond:
                    day_num += 1
#                print 'lastDate, currDate, day_num', lastDate, currDate, day_num
        self.account.setDates(calendar.timegm(min(self.to_date, perf_to_date - timedelta(days = gdtlive.config.POSTLOAD_DAYS)).timetuple()))
        self.account.setDayNum(day_num)
        self.perf_date_changed = False

    def calculate_pipvalues(self):
        '''Kiszámolja az összes betöltött adatsor pip értékét'''

        for symbol in self.candles.iterkeys():
            if symbol[3:] == 'USD':
                self._pipvalue_calc_case1(symbol)
            elif symbol[:3] == 'USD':
                self._pipvalue_calc_case2(symbol)
            else:
                self._pipvalue_calc_case3(symbol)


    def _pipvalue_calc_case1(self, symbol):
        '''Kiszámolja az USD-re végződő instrumentumok pip értékét minden gyertyára nézve.
        
        Az eredményt a `candles`[symbol][PRICE_PIPVALUE] tömbben tárolja
                
        
        :Parameters:
            symbol : str
                Az instrumentum azonosítója, ahol instrumentum = "*USD"  (AUDUSD, EURUSD, GBPUSD, ....)
        
        Algoritmus 
        ==========
        
        Determine your account’s currency (in about 90% cases it’s USD). If it’s the same currency 
        as the based currency of the pair (the second one, which goes after “/”) then you should 
        simply multiply the amount of currency units in your position (100,000 for 1 standard lot) 
        by the size of one pip. You’ll get a pip value of 10 currency units per 1 standard lot. 
        
        '''
        datarow_length = len(self.candles[symbol][PRICE_TIME])
        self.candles[symbol][PRICE_PIPVALUE] = [PIP_MULTIPLIER[symbol] for i in range(datarow_length)]
        self.candles[symbol][PRICE_BASEUSD_RATE] = [1.0] * datarow_length


    def _pipvalue_calc_case2(self, symbol):
        '''Kiszámolja az USD-vel kezdődő instrumentumok pip értékét minden gyertyára nézve.
        
        Az eredményt a `candles`[symbol][PRICE_PIPVALUE] tömbben tárolja
                
        
        :Parameters:
            symbol : str
                Az instrumentum azonosítója, ahol instrumentum = "USD*"  (USDCAD, USDZAR, ....)
        
        Algoritmus 
        ==========
        
        If the account’s currency is different from the base currency but is the same as the currency 
        pair’s long currency (the first one, which goes before “/”) then check this currency pair’s 
        current Ask rate (the highest of the rates).
        You should multiply the amount of currency units in your position (100,000 for 1 standard lot) 
        by the size of one pip and then divide the result by the Ask rate 
        '''
        cdef int i
        datarow_length = len(self.candles[symbol][PRICE_TIME])
        self.candles[symbol][PRICE_PIPVALUE] = [0] * datarow_length
        self.candles[symbol][PRICE_BASEUSD_RATE] = [1.0] * datarow_length
        for i in xrange(len(self.candles[symbol][PRICE_TIME])):
            value = 1 * PIP_MULTIPLIER[symbol] / self.candles[symbol][PRICE_ASKCLOSE][i]
            self.candles[symbol][PRICE_PIPVALUE][i] = value


    def _pipvalue_calc_case3(self, symbol):
        '''Kiszámolja az adott keresztárfolyam pip értékét minden gyertyára nézve.

        Az eredményt a `candles`[symbol][PRICE_PIPVALUE] tömbben tárolja

        :Parameters:
            symbol : str
                Az instrumentum azonosítója, ahol az instrumentumban nem szerepel az USD  (CADJPY, AUDNZD, ....)

        Algoritmus
        ==========

        If the account’s currency is different from any of the currencies from the pair of the position,
        you have to check the current rate of this currency relative to the base currency of the pair:
            3.A:
                If the currency pair combined of the account’s currency and the base currency of the position
                has the account’s currency as the base currency (second, after “/”) then you should check
                and remember its current Bid rate (the lowest of the rates).
            3.B
                If the currency pair combined of the account’s currency and the base currency of the position
                has the account’s currency as the long currency (first, before “/”) then you should check
                and remember its current Ask rate (the highest of the rates).

        You should multiply the amount of currency units in your position (100,000 for 1 standard lot) by
        the size of one pip and either multiply the result by the Bid rate received in step 3a or divide
        the result by the Ask rate received in step 3b.

        :TODO: unittest
        '''
        cdef int i
        symbol_3B = 'USD' + symbol[:3]   # CASE 3.B : account currency as long 
        symbol_3A = symbol[:3] + 'USD'    # CASE 3.A : account currency as short    
        case = ''
        if pipvalue.has_symbol(symbol_3B):
            case = '3B'
            price = pipvalue.get_price(symbol_3B, self.from_date.date(), self.to_date.date())
        elif pipvalue.has_symbol(symbol_3A):
            case = '3A'
            price = pipvalue.get_price(symbol_3A, self.from_date.date(), self.to_date.date())

#        else:
#            print 'no conversion rate available to %s from USD' % symbol[3:]
#            return False
#        if not len(price):
#            print 'no conversion rate available to %s from USD' % symbol[3:]
#            return False
        datarow_length = len(self.candles[symbol][PRICE_TIME])
        self.candles[symbol][PRICE_PIPVALUE] = [0] * datarow_length
        self.candles[symbol][PRICE_BASEUSD_RATE] = [0] * datarow_length
        time_row = self.candles[symbol][PRICE_TIME]
        pipvalue_datarow = self.candles[symbol][PRICE_PIPVALUE]
        baseusd_datarow = self.candles[symbol][PRICE_BASEUSD_RATE]
        if case == '3A':
            for i in xrange(len(time_row)):
                p_index = time_row[i].date()
                baseusd_datarow[i] = price[p_index]
                value = PIP_MULTIPLIER[symbol_3A] * price[p_index]
                pipvalue_datarow[i] = value

        elif case == '3B':
            for i in xrange(len(time_row)):
                p_index = time_row[i].date()
                baseusd_datarow[i] = price[p_index]
                value = PIP_MULTIPLIER[symbol_3B] / price[p_index]
                pipvalue_datarow[i] = value

        else:
            # ha nincs meg az árfolyam, 1-nek vesszük.... 
            for i in xrange(len(time_row)):
                value = 0.0001
                pipvalue_datarow[i] = value
                baseusd_datarow[i] = 1.0

    def load_spread(self, id):
        '''Betölti az adatbázisból a megadott spread konfigurációt

        :Parameters:
            id : int
                A használni kívánt SpreadConfig objektum azonosítója

        :raise OSError: Amennyiben nem található a megadott spread config id
        '''
        spread_config = self.session.query(db.SpreadConfig).get(id)
        if not spread_config:
            log.error('Spread config %d not found!' % id)
            raise OSError('Spread config %d not found!' % id)
        self.spread_config = spread_config
        for row in self.spread_config.rows:
            self.spread_config_rows[row.symbol.encode()] = row

        self.commission = self.spread_config.default_commission
        self.commission_in_pip = self.spread_config.commission_in_pips

    def load_rollover(self, id):
        '''Betölti az adatbázisból a megadott rollover konfigurációt

        A betöltött adatokat eltárolja az `interest_rates` dict-ben, aminek
        alapján a `setup_rollover` kiszámítja az aktuálisan használt instrumentumok
        kamatkülönbözetét.

        :Parameters:
            id : int
                A használni kívánt RolloverConfig objektum azonosítója

        :raise OSError: Amennyiben nem található a megadott rollover config id
        '''
        rollover_config = self.session.query(db.RolloverConfig).get(id)
        if not rollover_config:
            log.error('Rollover config %d not found!' % id)
            raise OSError('Rollover config %d not found!' % id)
        self.rollover_config = rollover_config
        for row in self.rollover_config.rows:
            self.interest_rates[row.currency.encode()] = row.interest

    def setup_rollover(self):
        '''Beállítja az instrumentumokhoz tartozó kamatkülönbözet mértékét. '''
        for symbol in self.candles.iterkeys():
            long_currency = symbol[:3]
            short_currency = symbol[3:]
            if long_currency in self.interest_rates and short_currency in self.interest_rates:
                self.set_interest(symbol, self.interest_rates[long_currency] - self.interest_rates[short_currency])

    property spread:
        def __get__(self):
            return self.spread_config.default_spread
        def __set__(self, value):
            if type(value) == tuple:
                if len(value) == 5:
                    (symbol, spread, ex_from, ex_to, ex_spread) = value
                elif len(value) == 2:
                    (symbol, spread, ex_from, ex_to, ex_spread) = (value[0], value[1], 0, 0, 0.0)
                else:
                    raise Exception('Please use spread = (symbol, spread, from_hour, to_hour, spread) or spread = (symbol, spread) format!')

                if symbol in self.spread_config_rows:
                    self.spread_config_rows[symbol].spread = spread
                    self.spread_config_rows[symbol].exception_from = ex_from
                    self.spread_config_rows[symbol].exception_to = ex_to
                    self.spread_config_rows[symbol].exception_spread = ex_spread
                else:
                    config_row = db.SpreadConfigsRow(symbol, spread, ex_from, ex_to, ex_spread)
                    self.spread_config.rows.append(config_row)
                    self.spread_config_rows[symbol] = config_row

            elif type(value) == float:
                self.spread_config.default_spread = value
            else:
                raise Exception('Please use spread = (symbol, from_hour, to_hour, spread) or spread = 1.5 format!')

    property slippage:
        def __get__(self):
            return self.account.slippage            
        def __set__(self, value):    
            self.account.slippage = value            

    property timeframe:
        def __get__(self):            
            return self.account.getTimeframe();
            
        def __set__(self, value):    
            self.account.setTimeframe(value)            

    property failed_trades:
        def __get__(self):
            #return self.account.failed_trades
            return 0
        def __set__(self, value):
            #self.account.failed_trades = value
            pass

    property from_date:
        def __get__(self):
            if not self._from_date and not self.available_from:
                raise Exception('from_date has not been set yet')
            if self.available_from and not self._from_date:
                return self.available_from
            if self.available_from and self._from_date and self.available_from > self._from_date:
                return self.available_from
            return self._from_date
        def __set__(self, value):
            if self.history_loaded:
                raise Exception('datarows has been loaded, from_date set is impossible')
            self._from_date = self._parse_date(value)
            self.start_date = 0 #resets value

    property to_date:
        def __get__(self):
            if not self._to_date and not self.available_to:
                raise Exception('to_date has not been set yet')
            if self.available_to and not self._to_date:
                return self.available_to
            if self.available_to and self._to_date and self.available_to < self._to_date:
                return self.available_to
            return self._to_date
#            return self._to_date if self._to_date and self._to_date <= self.available_to else self.available_to
        def __set__(self, value):
            if self.history_loaded:
                raise Exception('datarows has been loaded, from_date set is impossible')
            self._to_date = self._parse_date(value)
            self.perf_to_date = 0 #resets value

    property start_date:
        def __get__(self):
            if not self._start_date:
                return self.from_date
            return self._start_date
        def __set__(self, value):
            orig = self._start_date
            if not value:
                if self._start_date:
                    _start_date = max(self._start_date, self.from_date)
                else:
                    _start_date = self.from_date
            else:
                parsed = self._parse_date(value)
                if parsed < self.from_date:
                    raise Exception('start_date < from_date')
                self._start_date = parsed
            self.perf_from_date = 0 #resets value
            if not orig or orig < self._start_date:
                self.start_date_changed = True

    property perf_from_date:
        def __get__(self):
            if self.account.thisptr.performance.perf_from_date:
                return datetime.utcfromtimestamp(self.account.thisptr.performance.perf_from_date)
            return self.start_date
        def __set__(self, value):
            cdef int perf_from_date
            cdef int orig = self.account.thisptr.performance.perf_from_date
            if not value:
                if self.account.thisptr.performance.perf_from_date:
                    perf_from_date = max(calendar.timegm(self.start_date.timetuple()), self.account.thisptr.performance.perf_from_date)
                else:
                    perf_from_date = calendar.timegm(self.start_date.timetuple())
            else:
                newValue = self._parse_date(value)
                if newValue < self.start_date:
                    raise Exception('perf_from_date < start_date')
                perf_from_date = calendar.timegm(newValue.timetuple())
            if self.account.thisptr.performance.perf_to_date and self.account.thisptr.performance.perf_to_date <= perf_from_date:
                raise Exception('perf_from_date >= perf_to_date')
            self.account.thisptr.performance.perf_from_date = perf_from_date
            if orig != self.account.thisptr.performance.perf_from_date:
                self.perf_date_changed = True

    property perf_to_date:
        def __get__(self):
            if self.account.thisptr.performance.perf_to_date:
                return datetime.utcfromtimestamp(self.account.thisptr.performance.perf_to_date)
            return self.to_date + timedelta(days = gdtlive.config.POSTLOAD_DAYS)
        def __set__(self, value):
            cdef int perf_to_date
            cdef int orig = self.account.thisptr.performance.perf_from_date
            if not value:
                if self.account.thisptr.performance.perf_to_date:
                    perf_to_date = min(calendar.timegm((self.to_date + timedelta(days = gdtlive.config.POSTLOAD_DAYS)).timetuple()), self.account.thisptr.performance.perf_to_date)
                else:
                    perf_to_date = calendar.timegm((self.to_date + timedelta(days = gdtlive.config.POSTLOAD_DAYS)).timetuple())
            else:
                newValue = self._parse_date(value)
                if newValue > self.to_date + timedelta(days = gdtlive.config.POSTLOAD_DAYS):
                    raise Exception('perf_to_date > to_date + postload_days')
                perf_to_date = calendar.timegm(newValue.timetuple())
            if self.account.thisptr.performance.perf_from_date and self.account.thisptr.performance.perf_from_date >= perf_to_date:
                raise Exception('perf_to_date <= perf_from_date')
            self.account.thisptr.performance.perf_to_date = perf_to_date
            if orig != self.account.thisptr.performance.perf_from_date:
                self.perf_date_changed = True

    property commission:
        def __get__(self):
            return self.account.commission
        def __set__(self, value):
            self.account.commission = value
            self.spread_config.default_commission = value

    property commission_in_pip:
        def __get__(self):
            return self.account.commission_in_pip
        def __set__(self, value):
            self.account.commission_in_pip = value


    def _parse_date(self, value):
        '''Segédfunkció egy valamilyen formában megadott dátum érték konverziójához'''

        if type(value) == datetime:
            result = value
        elif type(value) == date:
            result = datetime(value.year, value.month, value.day)
        elif type(value) == tuple:
            try:
                result = datetime(value[0], value[1], value[2])
            except:
                raise Exception('Wrong date format! Please 3 tuple when setting the date, like: from_date=2010,1,1 ')
        elif type(value) == str:
            try:
                result = datetime.strptime(value, '%Y/%m/%d')
            except:
                raise Exception('Wrong date format! Please use YYYY/MM/DD format.')
        else:
            print '_parse_date error at', value
            raise Exception('Unknown parameter type! ')
        return result


    def setCandles(self):
        cdef int i
        if len(self.instruments) > 0:
            self.add_instruments(self.instruments) 

        import gdtlive.evol.gnodes as gnodes

        gnodes.Price.datarow = self.candles
        symbol = self.candles.__iter__().next()        
        self.candle_num = len(self.candles[symbol][0])
        self.candles_gn = {}  
        monthly_gain = []
        symbol = self.candles.__iter__().next()
        times = [0] * self.candle_num;
        c_month = self.candles[symbol][PRICE_TIME][0].month
        open_price = self.candles[symbol][PRICE_ASKOPEN][0]

        for i in xrange(self.candle_num):
            times[i] = calendar.timegm(self.candles[symbol][PRICE_TIME][i].timetuple())
            if self.candles[symbol][PRICE_TIME][i].month != c_month:
                gain = ((self.candles[symbol][PRICE_ASKOPEN][i] - open_price) / open_price) * 100.0
                monthly_gain.append(gain)
                c_month = self.candles[symbol][PRICE_TIME][i].month
                open_price = self.candles[symbol][PRICE_ASKOPEN][i]               

        gain = ((self.candles[symbol][PRICE_ASKOPEN][self.candle_num-1] - open_price) / open_price) * 100.0
        monthly_gain.append(gain)            

        self.monthly_gain_np = numpy.array(monthly_gain, dtype=numpy.double)
        self.account.setMonthlyGain(self.monthly_gain_np)

        self.candles_np = numpy.array(times, dtype=numpy.int)
        self.account.setTimeDatarow(self.candles_np)
        cdef int price
        for i, symbol in enumerate(self.instruments):
            self.candles_gn[symbol] = {}            
            self.candles_gn[symbol][PRICE_TIME] = self.candles[symbol][PRICE_TIME]
            for price in xrange(PRICE_OPEN, PRICE_BASEUSD_RATE+1):
                datarow = numpy.array(self.candles[symbol][price], dtype=numpy.double) 
                self.candles_gn[symbol][price] = datarow                                        
                self.account.setDatarow(i, price, datarow)                                

        self.account.setCandleNum(self.candle_num)

    def set_interest(self, symbol, value):
        self.account.set_interest(self.instrument_ids[symbol], value)            
    
    def use_orderlog(self, value):
        self.account.use_orderlog(value)
        
    def get_orderlog(self):
        '''A stratégia kötési naplójának lekérése'''
        return self.account.get_orderlog()        

    def get_tradelog(self):
        '''A stratégia trade naplójának lekérése'''
        return self.account.get_tradelog()

    def get_performance(self):
        '''A stratégia teljesítményének lekérése '''
        return self.account.get_performance()

    def get_serial_performance(self):
        return self.account.get_serial_performance()


    cpdef check_session(self, index):
        if gdtlive.config.WEEKEND_START > 0:
            if self.candles_gn[self.symbol][PRICE_TIME][index].isoweekday() == 5 and self.candles_gn[self.symbol][PRICE_TIME][index].hour >= gdtlive.config.WEEKEND_START or \
                (self.candles_gn[self.symbol][PRICE_TIME][index].isoweekday() == 5 and self.candles_gn[self.symbol][PRICE_TIME][index-1].isoweekday() == 4 and self.candles_gn[self.symbol][PRICE_TIME][index].minute == 0) :
                if not self.weekend:                    
                    self.account.closeAll()
                    self.account.allowOpen(False)
                    self.weekend = True
                    self.session_open = False
                    #print 'WEEK CLOSE:', self.candles_gn[self.symbol][PRICE_TIME][index]                        

            else:
                if (self.candles_gn[self.symbol][PRICE_TIME][index].isoweekday() == 1) or \
                   (self.candles_gn[self.symbol][PRICE_TIME][index].isoweekday() == 7 and self.candles_gn[self.symbol][PRICE_TIME][index-1].isoweekday() == 5 and self.candles_gn[self.symbol][PRICE_TIME][index].minute == 0) :                                
                    if self.weekend:                            
                        self.account.allowOpen(True)                                                        
                        self.weekend = False
                        self.session_open = True
                        #print 'WEEK OPEN:', self.candles_gn[self.symbol][PRICE_TIME][index]

        if not self.weekend:
            if gdtlive.config.SESSION_START < gdtlive.config.SESSION_END:
                if self.candles_gn[self.symbol][PRICE_TIME][index].hour >= gdtlive.config.SESSION_START and self.candles_gn[self.symbol][PRICE_TIME][index].hour < gdtlive.config.SESSION_END:
                    if not self.session_open:                            
                        self.account.allowOpen(True)                                                        
                        self.session_open = True
                        #print 'OPEN:', self.candles_gn[self.symbol][PRICE_TIME][index] 
                else:
                    if self.session_open:
                        if gdtlive.config.SESSION_END_CLOSE:
                            #print 'CLOSEALL', self.candles_gn[self.symbol][PRICE_TIME][index]
                            self.account.closeAll()
                        self.account.allowOpen(False)
                        self.session_open = False
                        #print 'CLOSE:', self.candles_gn[self.symbol][PRICE_TIME][index]                        
                    
            elif gdtlive.config.SESSION_START > gdtlive.config.SESSION_END:
                if self.candles_gn[self.symbol][PRICE_TIME][index].hour >= gdtlive.config.SESSION_START or self.candles_gn[self.symbol][PRICE_TIME][index].hour < gdtlive.config.SESSION_END:
                    if not self.session_open:
                        self.account.allowOpen(1)                                                        
                        self.session_open = True
                        #print 'OPEN:', self.candles_gn[self.symbol][PRICE_TIME][index]
                else:
                    if self.session_open:
                        if gdtlive.config.SESSION_END_CLOSE:
                            #print 'CLOSEALL', self.candles_gn[self.symbol][PRICE_TIME][index]
                            self.account.closeAll()
                        self.account.allowOpen(0)
                        self.session_open = False
                        #print 'CLOSE:', self.candles_gn[self.symbol][PRICE_TIME][index]

            elif gdtlive.config.SESSION_END == 0 and gdtlive.config.SESSION_START == 0 and gdtlive.config.SESSION_END_CLOSE and index > 0:
                if self.candles_gn[self.symbol][PRICE_TIME][index].hour == 0 and self.candles_gn[self.symbol][PRICE_TIME][index-1].hour == 23:
                    #print 'CLOSEALL', self.candles_gn[self.symbol][PRICE_TIME][index]
                    self.account.closeAll()


cdef class Strategy1(StrategyBase):
    '''
    Strategy object, evaluates a strategy based on GTree structure.
    '''

    def __cinit__(self, session, accPluginId, mmPluginId, opener):
        #1 means that this comes from Strategy1 for creating corresponding performance object
        self.account = SimulatedAccount(accPluginId, 1)

    def evaluate(self, genome):

        StrategyBase.evaluate(self, genome)
        cdef int index
        #s = time.time()
        if type(genome) == str or type(genome) == unicode:
            genome = GTree.decode(genome)

        genome.prepareForBacktest(self.candles_gn, self.account)
        commands = genome.getCommandNodes()
        for c in commands:
            c.symbol = self.instruments.index(c.symbol)

        self.symbol = self.candles_gn.__iter__().next()

        try:
            self.account.resetValues()
            for index in xrange(self.startIndex, self.candle_num):
#                print 'before onClose', index
                if self.account.onClose(index):
                    pass
#                    break
                if self.account.balance <= 100:
#                    print 'self.account.balance <= 100, closeAll'
                    self.account.closeAll()
                    if self.account.balance <= 100:
#                        print 'self.account.balance <= 100, died'
                        break

                self.check_session(index)                            

                for command in commands:
                    if command.signal[index]:
#                        print 'before command.signal[index]', index
                        command.execute(index)
#                        print 'after command.signal[index]', index
        except Exception as e:
            print e
        finally:
            self.account.backtestDone()
            for c in commands:
                c.symbol = self.instruments[c.symbol]



cdef class Strategy4(StrategyBase):
    '''
    Strategy object, evaluates a strategy based on GNetEcho structure.
    '''
    cdef:
        object perf
        object serial
        object genome
        
    def __cinit__(self, session, accPluginId, mmPluginId, opener):
        #4 means that this comes from Strategy4 for creating corresponding performance object
        self.account = SimulatedAccount(accPluginId, 4)


    def evaluate(self, genome):
        from gdtlive.evol.gnets.NetBaseEchoPredict import GNetEchoPredict
        self.genome = genome
        
        StrategyBase.evaluate(self, self.genome)
        cdef int index
        #s = time.time()
        if type(self.genome) == str or type(self.genome) == unicode:
            self.genome = GNetEchoPredict.decode(self.genome)
                                        
        self.genome.prepareForBacktest(self.candles_gn, self.account)
                                
        self.symbol = self.candles_gn.__iter__().next()
        self.genome.symbol = self.instruments.index(self.genome.symbol)                           
        
        try:
            self.account.resetValues()
            for index in xrange(self.startIndex, self.candle_num):
                #print index, self.candles_gn[self.symbol][PRICE_TIME][index], 
                if self.account.onClose(index):
                    pass
                if self.account.balance <= 100:
#                    print 'self.account.balance <= 100, closeAll'
                    self.account.closeAll()
                    if self.account.balance <= 100:
#                        print 'self.account.balance <= 100, died'
                        break

                self.check_session(index)                            
              
                self.genome.execute(index)
                
            self.account.closeAll()
        except Exception as e:
            print e
        finally:
            self.account.backtestDone()            
            self.genome.symbol = self.instruments[self.genome.symbol]
