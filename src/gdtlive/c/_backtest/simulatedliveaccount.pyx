#from backtest cimport instrument_t
from libc.stdlib cimport free, calloc, realloc
import cython
import traceback
import gdtlive.config
cimport numpy as np

cdef class SimulatedLiveAccount:

    def __cinit__(self):
        self.mmplugin = new NormalMMPlugin(NULL)
        self.instruments = NULL

    cpdef destroy(self):
        del self.mmplugin
        self.mmplugin = NULL
        free(self.instruments)
    
    
    cpdef setLeverage(self, double leverage):
        self.mmplugin.leverage = leverage
        
    cpdef setBaseEquity(self, long base_equity):        
        self.mmplugin.base_equity = base_equity
        
    
    cpdef calc_amount(self, int symbol, int current_candle, double total_net_profit, int direction, double ask, double bid, double commandpercent):
        cdef double sl = 0
        cdef double tp = 0
        self.instruments[symbol].pipvalue = self.instruments[symbol].value[current_candle]
        cdef int i = 0
        while i < self.instrument_num:
            self.mmplugin.setCurrents(&self.instruments[i], i * self.candle_num + current_candle)
            i += 1
        cdef long amount = self.mmplugin.calc_amount(&self.instruments[symbol], current_candle, total_net_profit, direction, ask, bid, &sl, &tp, commandpercent)
        return (sl, tp, amount)

    cpdef setInstrumentNum(self, int instrument_num):
        self.instrument_num = instrument_num
        if not self.instruments:
            self.instruments = <instrument_t*>calloc(1, sizeof(instrument_t) * instrument_num)
        else:
            self.instruments = <instrument_t *>realloc(self.instruments, sizeof(instrument_t) * instrument_num)

    cpdef setInstrument(self, int symbol, double pipsize, char* name):
        if self.instruments and self.instrument_num > symbol:
            self.instruments[symbol].symbol = symbol
            self.instruments[symbol].pipsize = pipsize
            self.instruments[symbol].name = name

    cpdef setDatarow(self, int instrument, int datarow_id, np.ndarray[np.double_t, ndim=1] datarow):
        if self.instruments and self.instrument_num > instrument:
            if datarow_id == 1:
                self.instruments[instrument].askopen = <double *>datarow.data
            elif datarow_id == 2:
                self.instruments[instrument].askhigh = <double *>datarow.data
            elif datarow_id == 3:
                self.instruments[instrument].asklow = <double *>datarow.data
            elif datarow_id == 4:
                self.instruments[instrument].askclose = <double *>datarow.data
            elif datarow_id == 5:
                self.instruments[instrument].bidopen = <double *>datarow.data
            elif datarow_id == 6:
                self.instruments[instrument].bidhigh = <double *>datarow.data
            elif datarow_id == 7:
                self.instruments[instrument].bidlow = <double *>datarow.data
            elif datarow_id == 8:
                self.instruments[instrument].bidclose = <double *>datarow.data
            elif datarow_id == 10:
                self.instruments[instrument].value = <double *>datarow.data

    cpdef initPeakThrough(self, candle_num):
        self.candle_num = candle_num        
        self.mmplugin.initPeakThrough(self.instrument_num, self.instruments, self.candle_num);

    cpdef setCandleNum(self, int candle_num):
        self.candle_num = candle_num;

    cpdef configMM(self, int id, object conf):
        cdef:
            long base_equity = 0
            int leverage = 0
            double positionsize = 0
            int positionmode = 0
            bint riskmanagement = False
            bint setsl = False
            double maxrisk = 0
            double riskreward_rate = 0
            int maxnumberoftrades = 0
            double margincall_rate = 0
            int be_threshold = 0
        try:
            base_equity = conf.get('equity',10000)
            leverage = conf.get('leverage',50)
            positionsize = conf['parameters'].get('positionsize', 10000)
            positionmode = conf['parameters'].get('positionmode', 0)
            riskmanagement = conf['parameters'].get('riskmanagement', 0)
            setsl = conf['parameters'].get('setsl', 0)
            maxrisk = conf['parameters'].get('maxrisk', 0)
            riskreward_rate = conf['parameters'].get('riskreward_rate', 0)
            maxnumoftrades = conf['parameters'].get('maxnumoftrades', 0)
            margincall_rate = conf['parameters'].get('margincall_rate', 50)
            sltype = conf['parameters'].get('sltype', 0)
            peakthroughplugin = conf['parameters'].get('peakthroughplugin', 0)
            peakthroughparam = conf['parameters'].get('peakthroughparam', 0)
            modifysl = conf['parameters'].get('modifysl', 0)
            be_threshold = conf['parameters'].get('be_threshold', 0)
        except Exception as e:
            print traceback.format_exc()
        self.mmplugin.configure(base_equity, leverage, positionsize, positionmode, riskmanagement, setsl, maxrisk, riskreward_rate, maxnumoftrades, margincall_rate, sltype, peakthroughplugin, peakthroughparam, modifysl, be_threshold)

    cpdef setHoliday(self, bint holiday):
        self.mmplugin.setHoliday(holiday)

    cpdef getLeverage(self):
        return self.mmplugin.getLeverage()
