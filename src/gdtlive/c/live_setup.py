'''
Created on Mar 28, 2011

@author: gdtlive
'''
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import os.path
import sys
import numpy
import os

current = os.path.dirname(__file__)
try:
    os.unlink(current + os.sep + '_backtest/simulatedliveaccount.cpp')
    os.unlink(current + os.sep + '_backtest/simulatedliveaccount.so')
except:
    pass


if len(sys.argv) == 1:
    sys.argv.append('build_ext')
    sys.argv.append('--inplace')

if sys.platform == "linux2" :
    include_dir = os.path.dirname(__file__) + os.sep + '_backtest'
    lib_dir = os.path.dirname(__file__) + os.sep + '_backtest'
elif sys.platform == "win32":
    pass

ext = Extension('simulatedliveaccount', ['_backtest/simulatedliveaccount.pyx',
                                          '_backtest/mmplugin.cpp'],
                include_dirs = [include_dir, numpy.get_include(), '/usr/include', '/usr/include/ta-lib/'],
                library_dirs = ['libc', 'stdc++', '/usr/lib', lib_dir],
                libraries = ["ta_lib"],
                language = 'c++'
                 )


setup(ext_modules = [ext],
    cmdclass = {'build_ext': build_ext})

c = os.path.dirname(__file__)
os.rename(c + os.sep + 'simulatedliveaccount.so', c + os.sep + '_backtest' + os.sep + 'simulatedliveaccount.so')
