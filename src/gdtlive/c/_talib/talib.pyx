import numpy
cimport numpy as np
cimport cython

cdef int UPPER_LIMIT = 100000 
cdef int LOWER_LIMIT = 2
cdef int MA_MAX = 8
cdef int MA_MIN = 0

ctypedef int TA_RetCode
ctypedef int TA_MAType    
ctypedef int TA_FuncUnstId
ctypedef int TA_CandleSettingType
ctypedef int TA_RangeType


cdef extern from "ta_libc.h":
    enum: TA_SUCCESS    

    TA_RetCode TA_ACOS( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )
    
    TA_RetCode TA_AD( int    startIdx,
                    int    endIdx,
                    double inHigh[],
                    double inLow[],
                    double inClose[],
                    double inVolume[],
                    int          *outBegIdx,
                    int          *outNBElement,
                    double        outReal[] )
    
    TA_RetCode TA_ADD( int    startIdx,
                     int    endIdx,
                     double  inReal0[],
                     double  inReal1[],
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    
    TA_RetCode TA_ADOSC( int    startIdx,
                       int    endIdx,
                       double  inHigh[],
                       double  inLow[],
                       double  inClose[],
                       double  inVolume[],
                       int           optInFastPeriod,
                       int           optInSlowPeriod,
                       int          *outBegIdx,
                       int          *outNBElement,
                       double        outReal[] )

    TA_RetCode TA_ADX( int    startIdx,
                     int    endIdx,
                     double  inHigh[],
                     double  inLow[],
                     double  inClose[],
                     int           optInTimePeriod,
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    
    TA_RetCode TA_ADXR( int    startIdx,
                      int    endIdx,
                      double  inHigh[],
                      double  inLow[],
                      double  inClose[],
                      int           optInTimePeriod, 
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )

    TA_RetCode TA_APO( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int           optInFastPeriod,
                     int           optInSlowPeriod,
                     TA_MAType     optInMAType,
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    
    TA_RetCode TA_AROON( int    startIdx,
                       int    endIdx,
                       double  inHigh[],
                       double  inLow[],
                       int           optInTimePeriod,
                       int          *outBegIdx,
                       int          *outNBElement,
                       double        outAroonDown[],
                       double        outAroonUp[] )
    
    TA_RetCode TA_AROONOSC( int    startIdx,
                          int    endIdx,
                          double  inHigh[],
                          double  inLow[],
                          int           optInTimePeriod,
                          int          *outBegIdx,
                          int          *outNBElement,
                          double        outReal[] )
    
    TA_RetCode TA_ASIN( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )

    TA_RetCode TA_ATAN( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )

    TA_RetCode TA_ATR( int    startIdx,
                     int    endIdx,
                     double  inHigh[],
                     double  inLow[],
                     double  inClose[],
                     int           optInTimePeriod,
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    
    TA_RetCode TA_AVGPRICE( int    startIdx,
                          int    endIdx,
                          double  inOpen[],
                          double  inHigh[],
                          double  inLow[],
                          double  inClose[],
                          int          *outBegIdx,
                          int          *outNBElement,
                          double        outReal[] )
    
    TA_RetCode TA_BBANDS( int    startIdx,
                        int    endIdx,
                        double  inReal[],
                        int           optInTimePeriod, 
                        double        optInNbDevUp, 
                        double        optInNbDevDn, 
                        TA_MAType     optInMAType,
                        int          *outBegIdx,
                        int          *outNBElement,
                        double        outRealUpperBand[],
                        double        outRealMiddleBand[],
                        double        outRealLowerBand[] )
    
    TA_RetCode TA_BETA( int    startIdx,
                      int    endIdx,
                      double  inReal0[],
                      double  inReal1[],
                      int           optInTimePeriod,
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )
    
    TA_RetCode TA_BOP( int    startIdx,
                     int    endIdx,
                     double  inOpen[],
                     double  inHigh[],
                     double  inLow[],
                     double  inClose[],
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )

    TA_RetCode TA_CCI( int    startIdx,
                     int    endIdx,
                     double  inHigh[],
                     double  inLow[],
                     double  inClose[],
                     int           optInTimePeriod, 
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    
    TA_RetCode TA_CDL2CROWS( int    startIdx,
                           int    endIdx,
                           double  inOpen[],
                           double  inHigh[],
                           double  inLow[],
                           double  inClose[],
                           int          *outBegIdx,
                           int          *outNBElement,
                           int           outInteger[] )
    
    TA_RetCode TA_CDL3BLACKCROWS( int    startIdx,
                                int    endIdx,
                                double  inOpen[],
                                double  inHigh[],
                                double  inLow[],
                                double  inClose[],
                                int          *outBegIdx,
                                int          *outNBElement,
                                int           outInteger[] )
    
    TA_RetCode TA_CDL3INSIDE( int    startIdx,
                            int    endIdx,
                            double  inOpen[],
                            double  inHigh[],
                            double  inLow[],
                            double  inClose[],
                            int          *outBegIdx,
                            int          *outNBElement,
                            int           outInteger[] )

    TA_RetCode TA_CDL3LINESTRIKE( int    startIdx,
                                int    endIdx,
                                double  inOpen[],
                                double  inHigh[],
                                double  inLow[],
                                double  inClose[],
                                int          *outBegIdx,
                                int          *outNBElement,
                                int           outInteger[] )

    TA_RetCode TA_CDL3OUTSIDE( int    startIdx,
                             int    endIdx,
                             double  inOpen[],
                             double  inHigh[],
                             double  inLow[],
                             double  inClose[],
                             int          *outBegIdx,
                             int          *outNBElement,
                             int           outInteger[] )

    TA_RetCode TA_CDL3STARSINSOUTH( int    startIdx,
                                  int    endIdx,
                                  double  inOpen[],
                                  double  inHigh[],
                                  double  inLow[],
                                  double  inClose[],
                                  int          *outBegIdx,
                                  int          *outNBElement,
                                  int           outInteger[] )
    
    TA_RetCode TA_CDL3WHITESOLDIERS( int    startIdx,
                                   int    endIdx,
                                   double  inOpen[],
                                   double  inHigh[],
                                   double  inLow[],
                                   double  inClose[],
                                   int          *outBegIdx,
                                   int          *outNBElement,
                                   int           outInteger[] )
    
    TA_RetCode TA_CDLABANDONEDBABY( int    startIdx,
                                  int    endIdx,
                                  double  inOpen[],
                                  double  inHigh[],
                                  double  inLow[],
                                  double  inClose[],
                                  double        optInPenetration, 
                                  int          *outBegIdx,
                                  int          *outNBElement,
                                  int           outInteger[] )

    TA_RetCode TA_CDLADVANCEBLOCK( int    startIdx,
                                 int    endIdx,
                                 double  inOpen[],
                                 double  inHigh[],
                                 double  inLow[],
                                 double  inClose[],
                                 int          *outBegIdx,
                                 int          *outNBElement,
                                 int           outInteger[] )
    
    TA_RetCode TA_CDLBELTHOLD( int    startIdx,
                             int    endIdx,
                             double  inOpen[],
                             double  inHigh[],
                             double  inLow[],
                             double  inClose[],
                             int          *outBegIdx,
                             int          *outNBElement,
                             int           outInteger[] )

    TA_RetCode TA_CDLBREAKAWAY( int    startIdx,
                              int    endIdx,
                              double  inOpen[],
                              double  inHigh[],
                              double  inLow[],
                              double  inClose[],
                              int          *outBegIdx,
                              int          *outNBElement,
                              int           outInteger[] )

    TA_RetCode TA_CDLCLOSINGMARUBOZU( int    startIdx,
                                    int    endIdx,
                                    double  inOpen[],
                                    double  inHigh[],
                                    double  inLow[],
                                    double  inClose[],
                                    int          *outBegIdx,
                                    int          *outNBElement,
                                    int           outInteger[] )
    
    TA_RetCode TA_CDLCONCEALBABYSWALL( int    startIdx,
                                     int    endIdx,
                                     double  inOpen[],
                                     double  inHigh[],
                                     double  inLow[],
                                     double  inClose[],
                                     int          *outBegIdx,
                                     int          *outNBElement,
                                     int           outInteger[] )

    TA_RetCode TA_CDLCOUNTERATTACK( int    startIdx,
                                  int    endIdx,
                                  double  inOpen[],
                                  double  inHigh[],
                                  double  inLow[],
                                  double  inClose[],
                                  int          *outBegIdx,
                                  int          *outNBElement,
                                  int           outInteger[] )
    
    TA_RetCode TA_CDLDARKCLOUDCOVER( int    startIdx,
                                   int    endIdx,
                                   double  inOpen[],
                                   double  inHigh[],
                                   double  inLow[],
                                   double  inClose[],
                                   double        optInPenetration, 
                                   int          *outBegIdx,
                                   int          *outNBElement,
                                   int           outInteger[] )
    
    TA_RetCode TA_CDLDOJI( int    startIdx,
                         int    endIdx,
                         double  inOpen[],
                         double  inHigh[],
                         double  inLow[],
                         double  inClose[],
                         int          *outBegIdx,
                         int          *outNBElement,
                         int           outInteger[] )
    
    TA_RetCode TA_CDLDOJISTAR( int    startIdx,
                             int    endIdx,
                             double  inOpen[],
                             double  inHigh[],
                             double  inLow[],
                             double  inClose[],
                             int          *outBegIdx,
                             int          *outNBElement,
                             int           outInteger[] )
    
    TA_RetCode TA_CDLDRAGONFLYDOJI( int    startIdx,
                                  int    endIdx,
                                  double  inOpen[],
                                  double  inHigh[],
                                  double  inLow[],
                                  double  inClose[],
                                  int          *outBegIdx,
                                  int          *outNBElement,
                                  int           outInteger[] )

    TA_RetCode TA_CDLENGULFING( int    startIdx,
                              int    endIdx,
                              double  inOpen[],
                              double  inHigh[],
                              double  inLow[],
                              double  inClose[],
                              int          *outBegIdx,
                              int          *outNBElement,
                              int           outInteger[] )
    
    TA_RetCode TA_CDLEVENINGDOJISTAR( int    startIdx,
                                    int    endIdx,
                                    double  inOpen[],
                                    double  inHigh[],
                                    double  inLow[],
                                    double  inClose[],
                                    double        optInPenetration,
                                    int          *outBegIdx,
                                    int          *outNBElement,
                                    int           outInteger[] )
    
    TA_RetCode TA_CDLEVENINGSTAR( int    startIdx,
                                int    endIdx,
                                double  inOpen[],
                                double  inHigh[],
                                double  inLow[],
                                double  inClose[],
                                double        optInPenetration,
                                int          *outBegIdx,
                                int          *outNBElement,
                                int           outInteger[] )
    
    TA_RetCode TA_CDLGAPSIDESIDEWHITE( int    startIdx,
                                     int    endIdx,
                                     double  inOpen[],
                                     double  inHigh[],
                                     double  inLow[],
                                     double  inClose[],
                                     int          *outBegIdx,
                                     int          *outNBElement,
                                     int           outInteger[] )

    TA_RetCode TA_CDLGRAVESTONEDOJI( int    startIdx,
                                   int    endIdx,
                                   double  inOpen[],
                                   double  inHigh[],
                                   double  inLow[],
                                   double  inClose[],
                                   int          *outBegIdx,
                                   int          *outNBElement,
                                   int           outInteger[] )

    TA_RetCode TA_CDLHAMMER( int    startIdx,
                           int    endIdx,
                           double  inOpen[],
                           double  inHigh[],
                           double  inLow[],
                           double  inClose[],
                           int          *outBegIdx,
                           int          *outNBElement,
                           int           outInteger[] )
    
    TA_RetCode TA_CDLHANGINGMAN( int    startIdx,
                               int    endIdx,
                               double  inOpen[],
                               double  inHigh[],
                               double  inLow[],
                               double  inClose[],
                               int          *outBegIdx,
                               int          *outNBElement,
                               int           outInteger[] )
    
    TA_RetCode TA_CDLHARAMI( int    startIdx,
                           int    endIdx,
                           double  inOpen[],
                           double  inHigh[],
                           double  inLow[],
                           double  inClose[],
                           int          *outBegIdx,
                           int          *outNBElement,
                           int           outInteger[] )
    
    TA_RetCode TA_CDLHARAMICROSS( int    startIdx,
                                int    endIdx,
                                double  inOpen[],
                                double  inHigh[],
                                double  inLow[],
                                double  inClose[],
                                int          *outBegIdx,
                                int          *outNBElement,
                                int           outInteger[] )
    
    TA_RetCode TA_CDLHIGHWAVE( int    startIdx,
                             int    endIdx,
                             double  inOpen[],
                             double  inHigh[],
                             double  inLow[],
                             double  inClose[],
                             int          *outBegIdx,
                             int          *outNBElement,
                             int           outInteger[] )
    
    TA_RetCode TA_CDLHIKKAKE( int    startIdx,
                            int    endIdx,
                            double  inOpen[],
                            double  inHigh[],
                            double  inLow[],
                            double  inClose[],
                            int          *outBegIdx,
                            int          *outNBElement,
                            int           outInteger[] )

    TA_RetCode TA_CDLHIKKAKEMOD( int    startIdx,
                               int    endIdx,
                               double  inOpen[],
                               double  inHigh[],
                               double  inLow[],
                               double  inClose[],
                               int          *outBegIdx,
                               int          *outNBElement,
                               int           outInteger[] )

    TA_RetCode TA_CDLHOMINGPIGEON( int    startIdx,
                                 int    endIdx,
                                 double  inOpen[],
                                 double  inHigh[],
                                 double  inLow[],
                                 double  inClose[],
                                 int          *outBegIdx,
                                 int          *outNBElement,
                                 int           outInteger[] )

    TA_RetCode TA_CDLIDENTICAL3CROWS( int    startIdx,
                                    int    endIdx,
                                    double  inOpen[],
                                    double  inHigh[],
                                    double  inLow[],
                                    double  inClose[],
                                    int          *outBegIdx,
                                    int          *outNBElement,
                                    int           outInteger[] )

    TA_RetCode TA_CDLINNECK( int    startIdx,
                           int    endIdx,
                           double  inOpen[],
                           double  inHigh[],
                           double  inLow[],
                           double  inClose[],
                           int          *outBegIdx,
                           int          *outNBElement,
                           int           outInteger[] )
    
    TA_RetCode TA_CDLINVERTEDHAMMER( int    startIdx,
                                   int    endIdx,
                                   double  inOpen[],
                                   double  inHigh[],
                                   double  inLow[],
                                   double  inClose[],
                                   int          *outBegIdx,
                                   int          *outNBElement,
                                   int           outInteger[] )

    TA_RetCode TA_CDLKICKING( int    startIdx,
                            int    endIdx,
                            double  inOpen[],
                            double  inHigh[],
                            double  inLow[],
                            double  inClose[],
                            int          *outBegIdx,
                            int          *outNBElement,
                            int           outInteger[] )
    
    TA_RetCode TA_CDLKICKINGBYLENGTH( int    startIdx,
                                    int    endIdx,
                                    double  inOpen[],
                                    double  inHigh[],
                                    double  inLow[],
                                    double  inClose[],
                                    int          *outBegIdx,
                                    int          *outNBElement,
                                    int           outInteger[] )
    
    TA_RetCode TA_CDLLADDERBOTTOM( int    startIdx,
                                 int    endIdx,
                                 double  inOpen[],
                                 double  inHigh[],
                                 double  inLow[],
                                 double  inClose[],
                                 int          *outBegIdx,
                                 int          *outNBElement,
                                 int           outInteger[] )
    
    TA_RetCode TA_CDLLONGLEGGEDDOJI( int    startIdx,
                                   int    endIdx,
                                   double  inOpen[],
                                   double  inHigh[],
                                   double  inLow[],
                                   double  inClose[],
                                   int          *outBegIdx,
                                   int          *outNBElement,
                                   int           outInteger[] )
    TA_RetCode TA_CDLLONGLINE( int    startIdx,
                             int    endIdx,
                             double  inOpen[],
                             double  inHigh[],
                             double  inLow[],
                             double  inClose[],
                             int          *outBegIdx,
                             int          *outNBElement,
                             int           outInteger[] )
    TA_RetCode TA_CDLMARUBOZU( int    startIdx,
                             int    endIdx,
                             double  inOpen[],
                             double  inHigh[],
                             double  inLow[],
                             double  inClose[],
                             int          *outBegIdx,
                             int          *outNBElement,
                             int           outInteger[] )
    TA_RetCode TA_CDLMATCHINGLOW( int    startIdx,
                                int    endIdx,
                                double  inOpen[],
                                double  inHigh[],
                                double  inLow[],
                                double  inClose[],
                                int          *outBegIdx,
                                int          *outNBElement,
                                int           outInteger[] )
    TA_RetCode TA_CDLMATHOLD( int    startIdx,
                            int    endIdx,
                            double  inOpen[],
                            double  inHigh[],
                            double  inLow[],
                            double  inClose[],
                            double        optInPenetration,
                            int          *outBegIdx,
                            int          *outNBElement,
                            int           outInteger[] )
    TA_RetCode TA_CDLMORNINGDOJISTAR( int    startIdx,
                                    int    endIdx,
                                    double  inOpen[],
                                    double  inHigh[],
                                    double  inLow[],
                                    double  inClose[],
                                    double        optInPenetration, 
                                    int          *outBegIdx,
                                    int          *outNBElement,
                                    int           outInteger[] )
    TA_RetCode TA_CDLMORNINGSTAR( int    startIdx,
                                int    endIdx,
                                double  inOpen[],
                                double  inHigh[],
                                double  inLow[],
                                double  inClose[],
                                double        optInPenetration,
                                int          *outBegIdx,
                                int          *outNBElement,
                                int           outInteger[] )    
    TA_RetCode TA_CDLONNECK( int    startIdx,
                           int    endIdx,
                           double  inOpen[],
                           double  inHigh[],
                           double  inLow[],
                           double  inClose[],
                           int          *outBegIdx,
                           int          *outNBElement,
                           int           outInteger[] )
    TA_RetCode TA_CDLPIERCING( int    startIdx,
                             int    endIdx,
                             double  inOpen[],
                             double  inHigh[],
                             double  inLow[],
                             double  inClose[],
                             int          *outBegIdx,
                             int          *outNBElement,
                             int           outInteger[] )
    TA_RetCode TA_CDLRICKSHAWMAN( int    startIdx,
                                int    endIdx,
                                double  inOpen[],
                                double  inHigh[],
                                double  inLow[],
                                double  inClose[],
                                int          *outBegIdx,
                                int          *outNBElement,
                                int           outInteger[] )
    TA_RetCode TA_CDLRISEFALL3METHODS( int    startIdx,
                                     int    endIdx,
                                     double  inOpen[],
                                     double  inHigh[],
                                     double  inLow[],
                                     double  inClose[],
                                     int          *outBegIdx,
                                     int          *outNBElement,
                                     int           outInteger[] )
    TA_RetCode TA_CDLSEPARATINGLINES( int    startIdx,
                                    int    endIdx,
                                    double  inOpen[],
                                    double  inHigh[],
                                    double  inLow[],
                                    double  inClose[],
                                    int          *outBegIdx,
                                    int          *outNBElement,
                                    int           outInteger[] )
    TA_RetCode TA_CDLSHOOTINGSTAR( int    startIdx,
                                 int    endIdx,
                                 double  inOpen[],
                                 double  inHigh[],
                                 double  inLow[],
                                 double  inClose[],
                                 int          *outBegIdx,
                                 int          *outNBElement,
                                 int           outInteger[] )    
    TA_RetCode TA_CDLSHORTLINE( int    startIdx,
                              int    endIdx,
                              double  inOpen[],
                              double  inHigh[],
                              double  inLow[],
                              double  inClose[],
                              int          *outBegIdx,
                              int          *outNBElement,
                              int           outInteger[] )
    TA_RetCode TA_CDLSPINNINGTOP( int    startIdx,
                                int    endIdx,
                                double  inOpen[],
                                double  inHigh[],
                                double  inLow[],
                                double  inClose[],
                                int          *outBegIdx,
                                int          *outNBElement,
                                int           outInteger[] )
    TA_RetCode TA_CDLSTALLEDPATTERN( int    startIdx,
                                   int    endIdx,
                                   double  inOpen[],
                                   double  inHigh[],
                                   double  inLow[],
                                   double  inClose[],
                                   int          *outBegIdx,
                                   int          *outNBElement,
                                   int           outInteger[] )
    TA_RetCode TA_CDLSTICKSANDWICH( int    startIdx,
                                  int    endIdx,
                                  double  inOpen[],
                                  double  inHigh[],
                                  double  inLow[],
                                  double  inClose[],
                                  int          *outBegIdx,
                                  int          *outNBElement,
                                  int           outInteger[] )
    TA_RetCode TA_CDLTAKURI( int    startIdx,
                           int    endIdx,
                           double  inOpen[],
                           double  inHigh[],
                           double  inLow[],
                           double  inClose[],
                           int          *outBegIdx,
                           int          *outNBElement,
                           int           outInteger[] )
    TA_RetCode TA_CDLTASUKIGAP( int    startIdx,
                              int    endIdx,
                              double  inOpen[],
                              double  inHigh[],
                              double  inLow[],
                              double  inClose[],
                              int          *outBegIdx,
                              int          *outNBElement,
                              int           outInteger[] )
    TA_RetCode TA_CDLTHRUSTING( int    startIdx,
                              int    endIdx,
                              double  inOpen[],
                              double  inHigh[],
                              double  inLow[],
                              double  inClose[],
                              int          *outBegIdx,
                              int          *outNBElement,
                              int           outInteger[] )
    TA_RetCode TA_CDLTRISTAR( int    startIdx,
                            int    endIdx,
                            double  inOpen[],
                            double  inHigh[],
                            double  inLow[],
                            double  inClose[],
                            int          *outBegIdx,
                            int          *outNBElement,
                            int           outInteger[] )
    TA_RetCode TA_CDLUNIQUE3RIVER( int    startIdx,
                                 int    endIdx,
                                 double  inOpen[],
                                 double  inHigh[],
                                 double  inLow[],
                                 double  inClose[],
                                 int          *outBegIdx,
                                 int          *outNBElement,
                                 int           outInteger[] )
    TA_RetCode TA_CDLUPSIDEGAP2CROWS( int    startIdx,
                                    int    endIdx,
                                    double  inOpen[],
                                    double  inHigh[],
                                    double  inLow[],
                                    double  inClose[],
                                    int          *outBegIdx,
                                    int          *outNBElement,
                                    int           outInteger[] )
    TA_RetCode TA_CDLXSIDEGAP3METHODS( int    startIdx,
                                     int    endIdx,
                                     double  inOpen[],
                                     double  inHigh[],
                                     double  inLow[],
                                     double  inClose[],
                                     int          *outBegIdx,
                                     int          *outNBElement,
                                     int           outInteger[] )
    TA_RetCode TA_CEIL( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )
    TA_RetCode TA_CMO( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int           optInTimePeriod,
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_CORREL( int    startIdx,
                        int    endIdx,
                        double  inReal0[],
                        double  inReal1[],
                        int           optInTimePeriod, 
                        int          *outBegIdx,
                        int          *outNBElement,
                        double        outReal[] )
    TA_RetCode TA_COS( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_COSH( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )
    TA_RetCode TA_DEMA( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      int           optInTimePeriod, 
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )
    TA_RetCode TA_DIV( int    startIdx,
                     int    endIdx,
                     double  inReal0[],
                     double  inReal1[],
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_DX( int    startIdx,
                    int    endIdx,
                    double  inHigh[],
                    double  inLow[],
                    double  inClose[],
                    int           optInTimePeriod, 
                    int          *outBegIdx,
                    int          *outNBElement,
                    double        outReal[] )
    TA_RetCode TA_EMA( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int           optInTimePeriod, 
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_EXP( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_FLOOR( int    startIdx,
                       int    endIdx,
                       double  inReal[],
                       int          *outBegIdx,
                       int          *outNBElement,
                       double        outReal[] )
    TA_RetCode TA_HT_DCPERIOD( int    startIdx,
                             int    endIdx,
                             double  inReal[],
                             int          *outBegIdx,
                             int          *outNBElement,
                             double        outReal[] )
    TA_RetCode TA_HT_DCPHASE( int    startIdx,
                            int    endIdx,
                            double  inReal[],
                            int          *outBegIdx,
                            int          *outNBElement,
                            double        outReal[] )
    TA_RetCode TA_HT_PHASOR( int    startIdx,
                           int    endIdx,
                           double  inReal[],
                           int          *outBegIdx,
                           int          *outNBElement,
                           double        outInPhase[],
                           double        outQuadrature[] )
    TA_RetCode TA_HT_SINE( int    startIdx,
                         int    endIdx,
                         double  inReal[],
                         int          *outBegIdx,
                         int          *outNBElement,
                         double        outSine[],
                         double        outLeadSine[] )
    TA_RetCode TA_HT_TRENDLINE( int    startIdx,
                              int    endIdx,
                              double  inReal[],
                              int          *outBegIdx,
                              int          *outNBElement,
                              double        outReal[] )
    TA_RetCode TA_HT_TRENDMODE( int    startIdx,
                              int    endIdx,
                              double  inReal[],
                              int          *outBegIdx,
                              int          *outNBElement,
                              int           outInteger[] )
    TA_RetCode TA_KAMA( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      int           optInTimePeriod, 
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )
    TA_RetCode TA_LINEARREG( int    startIdx,
                           int    endIdx,
                           double  inReal[],
                           int           optInTimePeriod,
                           int          *outBegIdx,
                           int          *outNBElement,
                           double        outReal[] )
    TA_RetCode TA_LINEARREG_ANGLE( int    startIdx,
                                 int    endIdx,
                                 double  inReal[],
                                 int           optInTimePeriod, 
                                 int          *outBegIdx,
                                 int          *outNBElement,
                                 double        outReal[] )
    TA_RetCode TA_LINEARREG_INTERCEPT( int    startIdx,
                                     int    endIdx,
                                     double  inReal[],
                                     int           optInTimePeriod, 
                                     int          *outBegIdx,
                                     int          *outNBElement,
                                     double        outReal[] )
    TA_RetCode TA_LINEARREG_SLOPE( int    startIdx,
                                 int    endIdx,
                                 double  inReal[],
                                 int           optInTimePeriod, 
                                 int          *outBegIdx,
                                 int          *outNBElement,
                                 double        outReal[] )
    TA_RetCode TA_LN( int    startIdx,
                    int    endIdx,
                    double  inReal[],
                    int          *outBegIdx,
                    int          *outNBElement,
                    double        outReal[] )
    TA_RetCode TA_LOG10( int    startIdx,
                       int    endIdx,
                       double  inReal[],
                       int          *outBegIdx,
                       int          *outNBElement,
                       double        outReal[] )
    TA_RetCode TA_MA(int         startIdx, 
                        int     endIdx, 
                        double     inReal[], 
                        int     optInTimePeriod, 
                        int     optInMAType, 
                        int     *outBegIdx, 
                        int     *outNbElement, 
                        double     outReal[])
    TA_RetCode TA_MACD( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      int           optInFastPeriod, 
                      int           optInSlowPeriod, 
                      int           optInSignalPeriod, 
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outMACD[],
                      double        outMACDSignal[],
                      double        outMACDHist[] )
    TA_RetCode TA_MACDEXT( int    startIdx,
                         int    endIdx,
                         double  inReal[],
                         int           optInFastPeriod, 
                         TA_MAType     optInFastMAType,
                         int           optInSlowPeriod, 
                         TA_MAType     optInSlowMAType,
                         int           optInSignalPeriod, 
                         TA_MAType     optInSignalMAType,
                         int          *outBegIdx,
                         int          *outNBElement,
                         double        outMACD[],
                         double        outMACDSignal[],
                         double        outMACDHist[] )
    TA_RetCode TA_MACDFIX( int    startIdx,
                         int    endIdx,
                         double  inReal[],
                         int           optInSignalPeriod, 
                         int          *outBegIdx,
                         int          *outNBElement,
                         double        outMACD[],
                         double        outMACDSignal[],
                         double        outMACDHist[] )
    TA_RetCode TA_MAMA( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      double        optInFastLimit,
                      double        optInSlowLimit,
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outMAMA[],
                      double        outFAMA[] )
    TA_RetCode TA_MAVP( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      double  inPeriods[],
                      int           optInMinPeriod, 
                      int           optInMaxPeriod, 
                      TA_MAType     optInMAType,
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )
    TA_RetCode TA_MAX( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int           optInTimePeriod,
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_MAXINDEX( int    startIdx,
                          int    endIdx,
                          double  inReal[],
                          int           optInTimePeriod, 
                          int          *outBegIdx,
                          int          *outNBElement,
                          int           outInteger[] )
    TA_RetCode TA_MEDPRICE( int    startIdx,
                          int    endIdx,
                          double  inHigh[],
                          double  inLow[],
                          int          *outBegIdx,
                          int          *outNBElement,
                          double        outReal[] )
    TA_RetCode TA_MFI( int    startIdx,
                     int    endIdx,
                     double  inHigh[],
                     double  inLow[],
                     double  inClose[],
                     double  inVolume[],
                     int           optInTimePeriod, 
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_MIDPOINT( int    startIdx,
                          int    endIdx,
                          double  inReal[],
                          int           optInTimePeriod,
                          int          *outBegIdx,
                          int          *outNBElement,
                          double        outReal[] )
    TA_RetCode TA_MIDPRICE( int    startIdx,
                          int    endIdx,
                          double  inHigh[],
                          double  inLow[],
                          int           optInTimePeriod, 
                          int          *outBegIdx,
                          int          *outNBElement,
                          double        outReal[] )
    TA_RetCode TA_MIN( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int           optInTimePeriod,
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_MININDEX( int    startIdx,
                          int    endIdx,
                          double  inReal[],
                          int           optInTimePeriod,
                          int          *outBegIdx,
                          int          *outNBElement,
                          int           outInteger[] )
    TA_RetCode TA_MINMAX( int    startIdx,
                        int    endIdx,
                        double  inReal[],
                        int           optInTimePeriod, 
                        int          *outBegIdx,
                        int          *outNBElement,
                        double        outMin[],
                        double        outMax[] )
    TA_RetCode TA_MINMAXINDEX( int    startIdx,
                             int    endIdx,
                             double  inReal[],
                             int           optInTimePeriod, 
                             int          *outBegIdx,
                             int          *outNBElement,
                             int           outMinIdx[],
                             int           outMaxIdx[] )
    TA_RetCode TA_MINUS_DI( int    startIdx,
                          int    endIdx,
                          double  inHigh[],
                          double  inLow[],
                          double  inClose[],
                          int           optInTimePeriod, 
                          int          *outBegIdx,
                          int          *outNBElement,
                          double        outReal[] )
    TA_RetCode TA_MINUS_DM( int    startIdx,
                          int    endIdx,
                          double  inHigh[],
                          double  inLow[],
                          int           optInTimePeriod, 
                          int          *outBegIdx,
                          int          *outNBElement,
                          double        outReal[] )
    TA_RetCode TA_MOM( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int           optInTimePeriod, 
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_MULT( int    startIdx,
                      int    endIdx,
                      double  inReal0[],
                      double  inReal1[],
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )
    TA_RetCode TA_NATR( int    startIdx,
                      int    endIdx,
                      double  inHigh[],
                      double  inLow[],
                      double  inClose[],
                      int           optInTimePeriod, 
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )
    TA_RetCode TA_OBV( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     double  inVolume[],
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_PLUS_DI( int    startIdx,
                         int    endIdx,
                         double  inHigh[],
                         double  inLow[],
                         double  inClose[],
                         int           optInTimePeriod,
                         int          *outBegIdx,
                         int          *outNBElement,
                         double        outReal[] )
    TA_RetCode TA_PLUS_DM( int    startIdx,
                         int    endIdx,
                         double  inHigh[],
                         double  inLow[],
                         int           optInTimePeriod,
                         int          *outBegIdx,
                         int          *outNBElement,
                         double        outReal[] )
    TA_RetCode TA_PPO( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int           optInFastPeriod,
                     int           optInSlowPeriod,
                     TA_MAType     optInMAType,
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_ROC( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int           optInTimePeriod,
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_ROCP( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      int           optInTimePeriod,
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )
    TA_RetCode TA_ROCR( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      int           optInTimePeriod,
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )
    TA_RetCode TA_ROCR100( int    startIdx,
                         int    endIdx,
                         double  inReal[],
                         int           optInTimePeriod, 
                         int          *outBegIdx,
                         int          *outNBElement,
                         double        outReal[] )
    TA_RetCode TA_RSI( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int           optInTimePeriod,
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_SAR( int    startIdx,
                     int    endIdx,
                     double  inHigh[],
                     double  inLow[],
                     double        optInAcceleration, 
                     double        optInMaximum, 
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_SAREXT( int    startIdx,
                        int    endIdx,
                        double  inHigh[],
                        double  inLow[],
                        double        optInStartValue, 
                        double        optInOffsetOnReverse, 
                        double        optInAccelerationInitLong, 
                        double        optInAccelerationLong, 
                        double        optInAccelerationMaxLong, 
                        double        optInAccelerationInitShort, 
                        double        optInAccelerationShort, 
                        double        optInAccelerationMaxShort, 
                        int          *outBegIdx,
                        int          *outNBElement,
                        double        outReal[] )
    TA_RetCode TA_SIN( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_SINH( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )
    TA_RetCode TA_SMA( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int           optInTimePeriod, 
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_SQRT( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )
    TA_RetCode TA_STDDEV( int    startIdx,
                        int    endIdx,
                        double  inReal[],
                        int           optInTimePeriod, 
                        double        optInNbDev, 
                        int          *outBegIdx,
                        int          *outNBElement,
                        double        outReal[] )
    TA_RetCode TA_STOCH( int    startIdx,
                       int    endIdx,
                       double  inHigh[],
                       double  inLow[],
                       double  inClose[],
                       int           optInFastK_Period, 
                       int           optInSlowK_Period, 
                       TA_MAType     optInSlowK_MAType,
                       int           optInSlowD_Period, 
                       TA_MAType     optInSlowD_MAType,
                       int          *outBegIdx,
                       int          *outNBElement,
                       double        outSlowK[],
                       double        outSlowD[] )
    TA_RetCode TA_STOCHF( int    startIdx,
                        int    endIdx,
                        double  inHigh[],
                        double  inLow[],
                        double  inClose[],
                        int           optInFastK_Period, 
                        int           optInFastD_Period, 
                        TA_MAType     optInFastD_MAType,
                        int          *outBegIdx,
                        int          *outNBElement,
                        double        outFastK[],
                        double        outFastD[] )
    TA_RetCode TA_STOCHRSI( int    startIdx,
                          int    endIdx,
                          double  inReal[],
                          int           optInTimePeriod, 
                          int           optInFastK_Period, 
                          int           optInFastD_Period, 
                          TA_MAType     optInFastD_MAType,
                          int          *outBegIdx,
                          int          *outNBElement,
                          double        outFastK[],
                          double        outFastD[] )
    TA_RetCode TA_SUB( int    startIdx,
                     int    endIdx,
                     double  inReal0[],
                     double  inReal1[],
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_SUM( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int           optInTimePeriod, 
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_T3( int    startIdx,
                    int    endIdx,
                    double  inReal[],
                    int           optInTimePeriod, 
                    double        optInVFactor, 
                    int          *outBegIdx,
                    int          *outNBElement,
                    double        outReal[] )
    TA_RetCode TA_TAN( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_TANH( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )
    TA_RetCode TA_TEMA( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      int           optInTimePeriod, 
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )
    TA_RetCode TA_TRANGE( int    startIdx,
                        int    endIdx,
                        double  inHigh[],
                        double  inLow[],
                        double  inClose[],
                        int          *outBegIdx,
                        int          *outNBElement,
                        double        outReal[] )
    TA_RetCode TA_TRIMA( int    startIdx,
                       int    endIdx,
                       double  inReal[],
                       int           optInTimePeriod, 
                       int          *outBegIdx,
                       int          *outNBElement,
                       double        outReal[] )
    TA_RetCode TA_TRIX( int    startIdx,
                      int    endIdx,
                      double  inReal[],
                      int           optInTimePeriod, 
                      int          *outBegIdx,
                      int          *outNBElement,
                      double        outReal[] )
    TA_RetCode TA_TSF( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int           optInTimePeriod, 
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_TYPPRICE( int    startIdx,
                          int    endIdx,
                          double  inHigh[],
                          double  inLow[],
                          double  inClose[],
                          int          *outBegIdx,
                          int          *outNBElement,
                          double        outReal[] )
    TA_RetCode TA_ULTOSC( int    startIdx,
                        int    endIdx,
                        double  inHigh[],
                        double  inLow[],
                        double  inClose[],
                        int           optInTimePeriod1, 
                        int           optInTimePeriod2, 
                        int           optInTimePeriod3, 
                        int          *outBegIdx,
                        int          *outNBElement,
                        double        outReal[] )
    TA_RetCode TA_VAR( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int           optInTimePeriod,
                     double        optInNbDev, 
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    TA_RetCode TA_WCLPRICE( int    startIdx,
                          int    endIdx,
                          double  inHigh[],
                          double  inLow[],
                          double  inClose[],
                          int          *outBegIdx,
                          int          *outNBElement,
                          double        outReal[] )
    TA_RetCode TA_WILLR( int    startIdx,
                       int    endIdx,
                       double  inHigh[],
                       double  inLow[],
                       double  inClose[],
                       int           optInTimePeriod,
                       int          *outBegIdx,
                       int          *outNBElement,
                       double        outReal[] )
    TA_RetCode TA_WMA( int    startIdx,
                     int    endIdx,
                     double  inReal[],
                     int           optInTimePeriod,
                     int          *outBegIdx,
                     int          *outNBElement,
                     double        outReal[] )
    
    
    TA_RetCode TA_SetUnstablePeriod( TA_FuncUnstId id,
                                 unsigned int  unstablePeriod )
    
    unsigned int TA_GetUnstablePeriod( TA_FuncUnstId id )
    
    TA_RetCode TA_SetCandleSettings( TA_CandleSettingType settingType, 
                                 TA_RangeType rangeType, 
                                 int avgPeriod, 
                                 double factor )
    
    TA_RetCode TA_RestoreCandleDefaultSettings( TA_CandleSettingType settingType )
           
    TA_RetCode TA_Initialize()
    TA_RetCode TA_Shutdown()


def Initialize():
    retCode =  TA_Initialize()
    if retCode != TA_SUCCESS:
       raise Exception("Cannot initialize TA-Lib (%d)!\n" % retCode)

def Shutdown():
    TA_Shutdown()


def ACOS(np.ndarray[np.double_t, ndim=1] input):
    cdef:
        int start = 0
        int element_num = 0    
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))
        int RetCode = 0
        
    RetCode = TA_ACOS(0,input.shape[0]-1,<double *>input.data,
                      &start, &element_num, <double *>output.data )
    
    return output


def AD(np.ndarray[np.double_t, ndim=1] high,
        np.ndarray[np.double_t, ndim=1] low,
        np.ndarray[np.double_t, ndim=1] close,
        np.ndarray[np.double_t, ndim=1] volume):
    cdef:
        int start = 0
        int element_num = 0
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))
        int RetCode = 0
        
    RetCode = TA_AD(0, high.shape[0]-1, <double *>high.data,<double *>low.data,
                    <double *>close.data,<double *>volume.data,&start,&element_num,
                    <double *>output.data)
    corrigate(output, element_num, start)
    return output
       

def ADD(np.ndarray[np.double_t, ndim=1] input1, 
        np.ndarray[np.double_t, ndim=1] input2): 
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input1.shape[0]))
        int RetCode = 0

    RetCode=TA_ADD(0, input1.shape[0]-1, <double *>input1.data, <double *>input2.data,
                     &start, &element_num, <double *>output.data )
    
    return output


def ADOSC(np.ndarray[np.double_t, ndim=1] high, 
            np.ndarray[np.double_t, ndim=1] low,
            np.ndarray[np.double_t, ndim=1] close,
            np.ndarray[np.double_t, ndim=1] volume,
            int fastPeriod,
            int slowPeriod):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))
        int RetCode = 0
        
    if fastPeriod < LOWER_LIMIT: fastPeriod = LOWER_LIMIT
    if slowPeriod < LOWER_LIMIT: slowPeriod = LOWER_LIMIT
    if fastPeriod > UPPER_LIMIT : fastPeriod = UPPER_LIMIT
    if slowPeriod > UPPER_LIMIT : slowPeriod = UPPER_LIMIT
    
    RetCode=TA_ADOSC(0, high.shape[0]-1, <double *>high.data, <double *>low.data,
                       <double *>close.data, <double *>volume.data,                       
                       fastPeriod, slowPeriod, &start, &element_num,
                       <double *> output.data)
    corrigate(output, element_num, start)
    return output
    
    
def ADX(np.ndarray[np.double_t, ndim=1] high,
        np.ndarray[np.double_t, ndim=1] low,
        np.ndarray[np.double_t, ndim=1] close,
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))
        int RetCode = 0
        
    if period < LOWER_LIMIT : period = LOWER_LIMIT
    if period < UPPER_LIMIT : period = UPPER_LIMIT
    
    RetCode=TA_ADX(0, high.shape[0]-1,                     
                     <double *> high.data, 
                     <double *> low.data,
                     <double *> close.data, 
                     period, &start, &element_num, 
                     <double *>output.data)
    corrigate(output, element_num, start)
    return output

    
def ADXR(np.ndarray[np.double_t, ndim=1] high,
        np.ndarray[np.double_t, ndim=1] low,
        np.ndarray[np.double_t, ndim=1] close,
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))
        int RetCode = 0
        
    if period < LOWER_LIMIT : period = LOWER_LIMIT
    if period < UPPER_LIMIT : period = UPPER_LIMIT
    
    RetCode=TA_ADXR(0, high.shape[0]-1,                     
                     <double *> high.data, 
                     <double *> low.data,
                     <double *> close.data, 
                     period, &start, &element_num, 
                     <double *>output.data)
    corrigate(output, element_num, start)
    return output


def APO(np.ndarray[np.double_t, ndim=1] input,
        int fastPeriod,
        int slowPeriod,
        int maType):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))
        int RetCode = 0
        
    if fastPeriod < LOWER_LIMIT : fastPeriod = LOWER_LIMIT
    if fastPeriod < UPPER_LIMIT : fastPeriod = UPPER_LIMIT
    if slowPeriod < LOWER_LIMIT : slowPeriod = LOWER_LIMIT
    if slowPeriod < UPPER_LIMIT : slowPeriod = UPPER_LIMIT
    if maType < MA_MIN : maType = MA_MIN
    if maType > MA_MAX : maType = MA_MAX
    
    RetCode=TA_APO(0, input.shape[0]-1, <double *> input.data, 
                     fastPeriod, slowPeriod, maType, &start, &element_num, 
                     <double *>output.data)
    corrigate(output, element_num, start)
    return output

    
def AROON_UP(np.ndarray[np.double_t, ndim=1] high,
        np.ndarray[np.double_t, ndim=1] low,
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] up = numpy.zeros((high.shape[0]))
        np.ndarray[np.double_t, ndim=1] down = numpy.zeros((high.shape[0]))
        int RetCode = 0
    
    if period < LOWER_LIMIT : period = LOWER_LIMIT
    if period < UPPER_LIMIT : period = UPPER_LIMIT
    
    RetCode=TA_AROON(0, high.shape[0]-1,
                    <double *> high.data, <double *> low.data, 
                     period, &start, &element_num, 
                     <double *>up.data, <double *>down.data)
    corrigate(up, element_num, start)    
    return up
    

def AROON_DOWN(np.ndarray[np.double_t, ndim=1] high,
        np.ndarray[np.double_t, ndim=1] low,
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] up = numpy.zeros((high.shape[0]))
        np.ndarray[np.double_t, ndim=1] down = numpy.zeros((high.shape[0]))
        int RetCode = 0
    
    if period < LOWER_LIMIT : period = LOWER_LIMIT
    if period < UPPER_LIMIT : period = UPPER_LIMIT
    
    RetCode=TA_AROON(0, high.shape[0]-1,
                    <double *> high.data, <double *> low.data, 
                     period, &start, &element_num, 
                     <double *>up.data, <double *>down.data)    
    corrigate(down, element_num, start)
    return down


def AROONOSC(np.ndarray[np.double_t, ndim=1] high,
        np.ndarray[np.double_t, ndim=1] low,
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0

    if period < LOWER_LIMIT : period = LOWER_LIMIT
    if period < UPPER_LIMIT : period = UPPER_LIMIT
        
    RetCode=TA_AROONOSC(0, high.shape[0]-1, 
                    <double *> high.data, <double *> low.data, 
                     period, &start, &element_num, 
                     <double *>output.data)
    corrigate(output, element_num, start)
    return output


def ATR(np.ndarray[np.double_t, ndim=1] high,
        np.ndarray[np.double_t, ndim=1] low,
        np.ndarray[np.double_t, ndim=1] close,
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0
    
    if period < LOWER_LIMIT : period = LOWER_LIMIT
    if period < UPPER_LIMIT : period = UPPER_LIMIT
    
    RetCode=TA_ATR(0, high.shape[0]-1, 
                    <double *> high.data, <double *> low.data, 
                    <double *> close.data, 
                     period, &start, &element_num, 
                     <double *>output.data)
    corrigate(output, element_num, start)
    return output


def AVGPRICE(np.ndarray[np.double_t, ndim=1] open,
        np.ndarray[np.double_t, ndim=1] high,
        np.ndarray[np.double_t, ndim=1] low,
        np.ndarray[np.double_t, ndim=1] close):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0
    
    RetCode=TA_AVGPRICE(0, high.shape[0]-1, 
                    <double *> open.data, <double *> high.data, 
                    <double *> low.data, <double *> close.data, 
                    &start, &element_num, 
                     <double *>output.data)
    corrigate(output, element_num, start)
    return output


def ASIN(np.ndarray[np.double_t, ndim=1] input):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))
        int RetCode = 0
        
    RetCode = TA_ASIN(0, input.shape[0]-1, <double *>input.data,
                      &start, &element_num, <double *>output.data )
    
    return output


def ATAN(np.ndarray[np.double_t, ndim=1] input):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))
        int RetCode = 0
        
    RetCode = TA_ATAN(0,input.shape[0]-1, <double *>input.data,
                      &start, &element_num, <double *>output.data )
    
    return output


def BBANDS_UPPER(np.ndarray[np.double_t, ndim=1] input,
        int period, 
        double devUp, 
        double devDown, 
        int maType):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] upper = numpy.zeros((input.shape[0]))        
        np.ndarray[np.double_t, ndim=1] middle = numpy.zeros((input.shape[0]))
        np.ndarray[np.double_t, ndim=1] lower = numpy.zeros((input.shape[0]))
        int RetCode = 0
    
    RetCode=TA_BBANDS(0, input.shape[0]-1, <double *>input.data,
                     period, devUp, devDown, maType,
                    &start, &element_num, 
                     <double *>upper.data, <double *>middle.data, <double *>lower.data)
    corrigate(upper, element_num, start)    
    return upper


def BBANDS_MIDDLE(np.ndarray[np.double_t, ndim=1] input,
        int period, 
        double devUp, 
        double devDown, 
        int maType):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] upper = numpy.zeros((input.shape[0]))        
        np.ndarray[np.double_t, ndim=1] middle = numpy.zeros((input.shape[0]))
        np.ndarray[np.double_t, ndim=1] lower = numpy.zeros((input.shape[0]))
        int RetCode = 0
    
    RetCode=TA_BBANDS(0, input.shape[0]-1, <double *>input.data,
                     period, devUp, devDown, maType,
                    &start, &element_num, 
                     <double *>upper.data, <double *>middle.data, <double *>lower.data)
    corrigate(middle, element_num, start)    
    return middle


def BBANDS_LOWER(np.ndarray[np.double_t, ndim=1] input,
        int period, 
        double devUp, 
        double devDown, 
        int maType):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] upper = numpy.zeros((input.shape[0]))        
        np.ndarray[np.double_t, ndim=1] middle = numpy.zeros((input.shape[0]))
        np.ndarray[np.double_t, ndim=1] lower = numpy.zeros((input.shape[0]))
        int RetCode = 0
    
    RetCode=TA_BBANDS(0, input.shape[0]-1, <double *>input.data,
                     period, devUp, devDown, maType,
                    &start, &element_num, 
                     <double *>upper.data, <double *>middle.data, <double *>lower.data)
    corrigate(lower, element_num, start)    
    return lower





def BETA(np.ndarray[np.double_t, ndim=1] input1,
        np.ndarray[np.double_t, ndim=1] input2,
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input1.shape[0]))        
        int RetCode = 0
    
    RetCode=TA_BETA(0, input1.shape[0]-1, 
                    <double *> input1.data, <double *> input2.data, 
                    period, 
                    &start, &element_num, 
                     <double *>output.data)
    corrigate(output, element_num, start)
    return output

def BOP(np.ndarray[np.double_t, ndim=1] open,
        np.ndarray[np.double_t, ndim=1] high,
        np.ndarray[np.double_t, ndim=1] low,
        np.ndarray[np.double_t, ndim=1] close,
        ):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((open.shape[0]))        
        int RetCode = 0
    
    RetCode=TA_BOP(0, open.shape[0]-1, 
                    <double *> open.data, <double *> high.data,
                    <double *> low.data, <double *> close.data,                     
                    &start, &element_num, 
                     <double *>output.data)
    corrigate(output, element_num, start)
    return output

def CCI(np.ndarray[np.double_t, ndim=1] high, 
        np.ndarray[np.double_t, ndim=1] low, 
        np.ndarray[np.double_t, ndim=1] close,
        int period):        
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0    
    RetCode=TA_CCI(0, high.shape[0]-1, 
                    <double *> high.data, <double *> low.data, <double *> close.data,
                    period, &start, &element_num, 
                     <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def CANDLE(np.ndarray[np.double_t, ndim=1] open, 
        np.ndarray[np.double_t, ndim=1] high, 
        np.ndarray[np.double_t, ndim=1] low, 
        np.ndarray[np.double_t, ndim=1] close,
        int pattern,
        double penetration=0):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.int_t, ndim=1] output = numpy.zeros((open.shape[0]), numpy.int)                
        int RetCode = 0    
    
    if pattern < 0 : pattern = 0
    if pattern > 59 : pattern = 59
    
    if pattern == 0:
        RetCode = TA_CDL2CROWS(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 1:
        RetCode = TA_CDL3BLACKCROWS(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 2:
        RetCode = TA_CDL3INSIDE(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 3:
        RetCode = TA_CDL3LINESTRIKE(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 4:
        RetCode = TA_CDL3OUTSIDE(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 5:
        RetCode = TA_CDL3STARSINSOUTH(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 6:
        RetCode = TA_CDL3WHITESOLDIERS(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)   
    elif pattern == 7:
        RetCode = TA_CDLABANDONEDBABY(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, penetration, &start, &element_num, <int *>output.data)
    elif pattern == 8:
        RetCode = TA_CDLADVANCEBLOCK(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 9:
        RetCode = TA_CDLBELTHOLD(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 10:
        RetCode = TA_CDLBREAKAWAY(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 11:
        RetCode = TA_CDLCLOSINGMARUBOZU(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 12:
        RetCode = TA_CDLCONCEALBABYSWALL(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 13:
        RetCode = TA_CDLCOUNTERATTACK(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 14:
        RetCode = TA_CDLDARKCLOUDCOVER(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, penetration, &start, &element_num, <int *>output.data)
    elif pattern == 15:
        RetCode = TA_CDLDOJI(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 16:
        RetCode = TA_CDLDOJISTAR(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 17:
        RetCode = TA_CDLDRAGONFLYDOJI(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 18:
        RetCode = TA_CDLENGULFING(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 19:
        RetCode = TA_CDLEVENINGDOJISTAR(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, penetration, &start, &element_num, <int *>output.data)
    elif pattern == 20:
        RetCode = TA_CDLEVENINGSTAR(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, penetration, &start, &element_num, <int *>output.data)
    elif pattern == 21:
        RetCode = TA_CDLGAPSIDESIDEWHITE(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 22:
        RetCode = TA_CDLHAMMER(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 23:
        RetCode = TA_CDLHANGINGMAN(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 24:
        RetCode = TA_CDLHARAMI(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 25:
        RetCode = TA_CDLHARAMICROSS(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 26:
        RetCode = TA_CDLHIGHWAVE(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 27:
        RetCode = TA_CDLHIKKAKE(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 28:
        RetCode = TA_CDLHIKKAKEMOD(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 29:
        RetCode = TA_CDLHOMINGPIGEON(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 30:
        RetCode = TA_CDLIDENTICAL3CROWS(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 31:
        RetCode = TA_CDLINNECK(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)
    elif pattern == 32:
        RetCode = TA_CDLINVERTEDHAMMER(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 33:
        RetCode = TA_CDLKICKING(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 34:
        RetCode = TA_CDLKICKINGBYLENGTH(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 35:
        RetCode = TA_CDLLADDERBOTTOM(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 36:
        RetCode = TA_CDLLONGLEGGEDDOJI(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 37:
        RetCode = TA_CDLLONGLINE(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 38:
        RetCode = TA_CDLMARUBOZU(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 39:
        RetCode = TA_CDLMATCHINGLOW(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 40:
        RetCode = TA_CDLMATHOLD(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, penetration, &start, &element_num, <int *>output.data)  
    elif pattern == 41:
        RetCode = TA_CDLMORNINGDOJISTAR(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, penetration, &start, &element_num, <int *>output.data)  
    elif pattern == 42:
        RetCode = TA_CDLMORNINGSTAR(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, penetration, &start, &element_num, <int *>output.data)  
    elif pattern == 43:
        RetCode = TA_CDLONNECK(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 44:
        RetCode = TA_CDLPIERCING(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 45:
        RetCode = TA_CDLRICKSHAWMAN(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 46:
        RetCode = TA_CDLRISEFALL3METHODS(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 47:
        RetCode = TA_CDLSEPARATINGLINES(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 48:
        RetCode = TA_CDLSHOOTINGSTAR(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 49:
        RetCode = TA_CDLSHORTLINE(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 50:
        RetCode = TA_CDLSPINNINGTOP(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 51:
        RetCode = TA_CDLSTALLEDPATTERN(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 52:
        RetCode = TA_CDLSTICKSANDWICH(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 53:
        RetCode = TA_CDLTAKURI(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 54:
        RetCode = TA_CDLTASUKIGAP(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 55:
        RetCode = TA_CDLTHRUSTING(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 56:
        RetCode = TA_CDLTRISTAR(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 57:
        RetCode = TA_CDLUNIQUE3RIVER(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 58:
        RetCode = TA_CDLUPSIDEGAP2CROWS(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
    elif pattern == 59:
        RetCode = TA_CDLXSIDEGAP3METHODS(0, open.shape[0]-1, <double *>open.data, <double *>high.data, <double *>low.data, <double *>close.data, &start, &element_num, <int *>output.data)  
                
    return (start, element_num, output)


def CMO(np.ndarray[np.double_t, ndim=1] input, 
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
        int RetCode = 0    

    RetCode = TA_CMO(0, input.shape[0]-1, <double *>input.data, period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def CORREL(np.ndarray[np.double_t, ndim=1] input,
            np.ndarray[np.double_t, ndim=1] input2, 
            int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
        int RetCode = 0    

    RetCode = TA_CORREL(0, input.shape[0]-1, <double *>input.data, <double *>input2.data, period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output


def DEMA(np.ndarray[np.double_t, ndim=1] input, int period):
    '''
    ROSSSSZZZZ!!!!!!!!!!
    
    '''
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
        int RetCode = 0    

    RetCode = TA_DEMA(0, input.shape[0]-1, <double *>input.data, period, &start, &element_num, <double *>output.data)
    #print 'start, element_num', start, element_num
    #print 'before', output    
    corrigate(output, element_num, start)
    #print 'after', output
    return output


def DX(np.ndarray[np.double_t, ndim=1] high,
        np.ndarray[np.double_t, ndim=1] low,
        np.ndarray[np.double_t, ndim=1] close,
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))                
        int RetCode = 0    

    RetCode = TA_DX(0, high.shape[0]-1, <double *>high.data, <double *>low.data, <double *>close.data, period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def EMA(np.ndarray[np.double_t, ndim=1] input,        
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
        int RetCode = 0    

    RetCode = TA_EMA(0, input.shape[0]-1, <double *>input.data, period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output


#def HT_DCPERIOD(np.ndarray[np.double_t, ndim=1] input):
#    cdef:
#        int start = 0
#        int element_num = 0 
#        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
#        int RetCode = 0    
#
#    RetCode = TA_HT_DCPERIOD(0, input.shape[0]-1, <double *>input.data, &start, &element_num, <double *>output.data)    
#    
#    return (start, element_num, output)

#def HT_DCPHASE(np.ndarray[np.double_t, ndim=1] input):
#    cdef:
#        int start = 0
#        int element_num = 0 
#        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
#        int RetCode = 0    
#
#    RetCode = TA_HT_DCPHASE(0, input.shape[0]-1, <double *>input.data, &start, &element_num, <double *>output.data)    
#    
#    return (start, element_num, output)
 
#def HT_PHASOR(np.ndarray[np.double_t, ndim=1] input):
#    cdef:
#        int start = 0
#        int element_num = 0 
#        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
#        np.ndarray[np.double_t, ndim=1] output2 = numpy.zeros((input.shape[0]))
#        int RetCode = 0    
#
#    RetCode = TA_HT_PHASOR(0, input.shape[0]-1, <double *>input.data, &start, &element_num, <double *>output.data, <double *>output2.data)    
#    
#    return (start, element_num, output, output2)

#def HT_SINE(np.ndarray[np.double_t, ndim=1] input):
#    cdef:
#        int start = 0
#        int element_num = 0 
#        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
#        np.ndarray[np.double_t, ndim=1] output2 = numpy.zeros((input.shape[0]))
#        int RetCode = 0    
#
#    RetCode = TA_HT_SINE(0, input.shape[0]-1, <double *>input.data, &start, &element_num, <double *>output.data, <double *>output2.data)    
#    
#    return (start, element_num, output, output2)

#def HT_TRENDLINE(np.ndarray[np.double_t, ndim=1] input):
#    cdef:
#        int start = 0
#        int element_num = 0 
#        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                        
#        int RetCode = 0    
#
#    RetCode = TA_HT_TRENDLINE(0, input.shape[0]-1, <double *>input.data, &start, &element_num, <double *>output.data)    
#    
#    return (start, element_num, output)

#def HT_TRENDMODE(np.ndarray[np.double_t, ndim=1] input):
#    cdef:
#        int start = 0
#        int element_num = 0 
#        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]), numpy.int)                        
#        int RetCode = 0    
#
#    RetCode = TA_HT_TRENDMODE(0, input.shape[0]-1, <double *>input.data, &start, &element_num, <int *>output.data)    
#    
#    return (start, element_num, output)

def KAMA(np.ndarray[np.double_t, ndim=1] input, int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
        int RetCode = 0    

    RetCode = TA_KAMA(0, input.shape[0]-1, <double *>input.data, period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output


def LINEARREG(np.ndarray[np.double_t, ndim=1] input, int period):

    cdef:
        int outbegidx = 0
        int outnbelement = 0
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))    
        
    TA_LINEARREG(0, input.shape[0]-1, <double *>input.data, period, &outbegidx, &outnbelement, <double *>output.data)    
    corrigate(output, outnbelement, outbegidx)
    return output

def LINEARREG_ANGLE(np.ndarray[np.double_t, ndim=1] input, int period):

    cdef:
        int outbegidx = 0
        int outnbelement = 0
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))    
        
    TA_LINEARREG_ANGLE(0, input.shape[0]-1, <double *>input.data, period, &outbegidx, &outnbelement, <double *>output.data)    
    corrigate(output, outnbelement, outbegidx)
    return output

def LINEARREG_INTERCEPT(np.ndarray[np.double_t, ndim=1] input, int period):

    cdef:
        int outbegidx = 0
        int outnbelement = 0
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))    
        
    TA_LINEARREG_INTERCEPT(0, input.shape[0]-1, <double *>input.data, period, &outbegidx, &outnbelement, <double *>output.data)    
    corrigate(output, outnbelement, outbegidx)
    return output

def LINEARREG_SLOPE(np.ndarray[np.double_t, ndim=1] input, int period):

    cdef:
        int outbegidx = 0
        int outnbelement = 0
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))    
        
    TA_LINEARREG_SLOPE(0, input.shape[0]-1, <double *>input.data, period, &outbegidx, &outnbelement, <double *>output.data)    
    corrigate(output, outnbelement, outbegidx)
    return output


def MA(np.ndarray[np.double_t, ndim=1] input,
               int period=1, int MAType=1):

    cdef int outbegidx = 0
    cdef int outnbelement = 0
    cdef np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))
    
    cdef begIdx = 0
    cdef int endIdx = input.shape[0]-1

    TA_MA(begIdx, endIdx, <double *>input.data, period, MAType, &outbegidx, &outnbelement, <double *>output.data)
    corrigate(output, outnbelement, outbegidx)
    return output
        
def MACD(np.ndarray[np.double_t, ndim=1] input, int fast, int slow, int signal):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output1 = numpy.zeros((input.shape[0]))                
        np.ndarray[np.double_t, ndim=1] output2 = numpy.zeros((input.shape[0]))
        np.ndarray[np.double_t, ndim=1] output3 = numpy.zeros((input.shape[0]))
        int RetCode = 0    

    RetCode = TA_MACD(0, input.shape[0]-1, <double *>input.data, fast, slow, signal, &start, &element_num, <double *>output1.data, <double *>output2.data, <double *>output3.data)    
    corrigate(output1, element_num, start)
    corrigate(output2, element_num, start)
    corrigate(output3, element_num, start)
    return (output1, output2, output3)

def MACDEXT(np.ndarray[np.double_t, ndim=1] input, 
            int fast, int fastMaType,
            int slow, int slowMaType,
            int signal, int signalMaType):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output1 = numpy.zeros((input.shape[0]))                
        np.ndarray[np.double_t, ndim=1] output2 = numpy.zeros((input.shape[0]))
        np.ndarray[np.double_t, ndim=1] output3 = numpy.zeros((input.shape[0]))
        int RetCode = 0    

    RetCode = TA_MACDEXT(0, input.shape[0]-1, <double *>input.data, fast, fastMaType, slow, slowMaType, signal, signalMaType, &start, &element_num, <double *>output1.data, <double *>output2.data, <double *>output3.data)    
    corrigate(output1, element_num, start)
    corrigate(output2, element_num, start)
    corrigate(output3, element_num, start)
    return (output1, output2, output3)

def MACDFIX(np.ndarray[np.double_t, ndim=1] input, int signal):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output1 = numpy.zeros((input.shape[0]))                
        np.ndarray[np.double_t, ndim=1] output2 = numpy.zeros((input.shape[0]))
        np.ndarray[np.double_t, ndim=1] output3 = numpy.zeros((input.shape[0]))
        int RetCode = 0    

    RetCode = TA_MACDFIX(0, input.shape[0]-1, <double *>input.data, signal, &start, &element_num, <double *>output1.data, <double *>output2.data, <double *>output3.data)    
    corrigate(output1, element_num, start)
    corrigate(output2, element_num, start)
    corrigate(output3, element_num, start)
    return (output1, output2, output3)

def MAMA(np.ndarray[np.double_t, ndim=1] input, int fast, int slow):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
        np.ndarray[np.double_t, ndim=1] output2 = numpy.zeros((input.shape[0]))
        int RetCode = 0    

    RetCode = TA_MAMA(0, input.shape[0]-1, <double *>input.data, fast, slow, &start, &element_num, <double *>output.data, <double *>output2.data)    
    corrigate(output, element_num, start)
    corrigate(output2, element_num, start)
    return (output, output2)

#def MAVP(np.ndarray[np.double_t, ndim=1] input, 
#        np.ndarray[np.double_t, ndim=1] input2,
#        int min, 
#        int max,
#        int maType):
#    cdef:
#        int start = 0
#        int element_num = 0 
#        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                        
#        int RetCode = 0    
#
#    RetCode = TA_MAVP(0, input.shape[0]-1, <double *>input.data,<double *>input2.data, min, max, maType, &start, &element_num, <double *>output.data)    
#    
#    return (start, element_num, output)

def MAX(np.ndarray[np.double_t, ndim=1] input, int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_MAX(0, input.shape[0]-1, <double *>input.data, period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output


def MEDPRICE(np.ndarray[np.double_t, ndim=1] high, 
            np.ndarray[np.double_t, ndim=1] low):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0    

    RetCode = TA_MEDPRICE(0, high.shape[0]-1, <double *>high.data,<double *>low.data, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def MFI(np.ndarray[np.double_t, ndim=1] high, 
        np.ndarray[np.double_t, ndim=1] low,
        np.ndarray[np.double_t, ndim=1] close,
        np.ndarray[np.double_t, ndim=1] volume,
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0    

    RetCode = TA_MFI(0, high.shape[0]-1, <double *>high.data,<double *>low.data, <double *>close.data, <double *>volume.data, period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def MIDPOINT(np.ndarray[np.double_t, ndim=1] input, int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_MIDPOINT(0, input.shape[0]-1, <double *>input.data, period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def MIDPRICE(np.ndarray[np.double_t, ndim=1] high, 
            np.ndarray[np.double_t, ndim=1] low,
            int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0    

    RetCode = TA_MIDPRICE(0, high.shape[0]-1, <double *>high.data,<double *>low.data, period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def MIN(np.ndarray[np.double_t, ndim=1] input, int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_MIN(0, input.shape[0]-1, <double *>input.data, period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output


def MINUS_DI(np.ndarray[np.double_t, ndim=1] high, 
        np.ndarray[np.double_t, ndim=1] low,
        np.ndarray[np.double_t, ndim=1] close,        
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0    

    RetCode = TA_MINUS_DI(0, high.shape[0]-1, <double *>high.data,<double *>low.data, <double *>close.data, period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def MINUS_DM(np.ndarray[np.double_t, ndim=1] high, 
        np.ndarray[np.double_t, ndim=1] low,           
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0    

    RetCode = TA_MINUS_DM(0, high.shape[0]-1, <double *>high.data,<double *>low.data, period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def MOM(np.ndarray[np.double_t, ndim=1] input,                   
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_MOM(0, input.shape[0]-1, <double *>input.data, period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output
    

def NATR(np.ndarray[np.double_t, ndim=1] high, 
        np.ndarray[np.double_t, ndim=1] low,
        np.ndarray[np.double_t, ndim=1] close,
        int period
        ):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0    

    RetCode = TA_NATR(0, high.shape[0]-1, 
                        <double *>high.data, <double *>low.data,
                        <double *>close.data, period,
                        &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def OBV(np.ndarray[np.double_t, ndim=1] input, 
        np.ndarray[np.double_t, ndim=1] volume):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_OBV(0, input.shape[0]-1, 
                        <double *>input.data, <double *>volume.data, 
                        &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output
    
def PLUS_DI(np.ndarray[np.double_t, ndim=1] high, 
        np.ndarray[np.double_t, ndim=1] low,
        np.ndarray[np.double_t, ndim=1] close,        
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0    

    RetCode = TA_PLUS_DI(0, high.shape[0]-1, <double *>high.data,<double *>low.data, <double *>close.data, period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def PLUS_DM(np.ndarray[np.double_t, ndim=1] high, 
        np.ndarray[np.double_t, ndim=1] low,           
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0    

    RetCode = TA_PLUS_DM(0, high.shape[0]-1, <double *>high.data,<double *>low.data, period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def PPO(np.ndarray[np.double_t, ndim=1] input,
        int fast,
        int slow,
        int maType):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_PPO(0, input.shape[0]-1, <double *>input.data, 
                fast, slow, maType, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def ROC(np.ndarray[np.double_t, ndim=1] input,
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_ROC(0, input.shape[0]-1, <double *>input.data, 
                period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output
    
def ROCP(np.ndarray[np.double_t, ndim=1] input,
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_ROCP(0, input.shape[0]-1, <double *>input.data, 
                period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def ROCR(np.ndarray[np.double_t, ndim=1] input,
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_ROCR(0, input.shape[0]-1, <double *>input.data, 
                period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def ROCR100(np.ndarray[np.double_t, ndim=1] input,
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_ROCR100(0, input.shape[0]-1, <double *>input.data, 
                period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def RSI(np.ndarray[np.double_t, ndim=1] input,
        int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_RSI(0, input.shape[0]-1, <double *>input.data, 
                period, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def SAR(np.ndarray[np.double_t, ndim=1] high,
        np.ndarray[np.double_t, ndim=1] low,
        double acceleration,
        double maximum):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0    

    RetCode = TA_SAR(0, high.shape[0]-1, <double *>high.data,  <double *>low.data, 
                acceleration, maximum, &start, &element_num, <double *>output.data)    
    corrigate(output, element_num, start)
    return output

def SAREXT(np.ndarray[np.double_t, ndim=1] high,
        np.ndarray[np.double_t, ndim=1] low,
        double startValue,
        double offsetOnReverse,
        double accInitLong,
        double accLong,
        double accMaxLong,
        double accInitShort,
        double accShort,
        double accMaxShort):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0    

    RetCode = TA_SAREXT(0, high.shape[0]-1, <double *>high.data,  <double *>low.data, 
                startValue, offsetOnReverse,
                accInitLong, accLong, accMaxLong,
                accInitShort, accShort, accMaxShort,
                &start, &element_num, <double *>output.data)    
    
    corrigate(output, element_num, start)
    return output

def SMA(np.ndarray[np.double_t, ndim=1] input, int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_SMA(0, input.shape[0]-1, <double *>input.data, period, 
                &start, &element_num, <double *>output.data)    
    
    corrigate(output, element_num, start)
    return output


def STDDEV(np.ndarray[np.double_t, ndim=1] input, int period, double dev):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_STDDEV(0, input.shape[0]-1, <double *>input.data, period, dev, 
                &start, &element_num, <double *>output.data)    
    
    corrigate(output, element_num, start)
    return output


def STOCH_SLOWK(np.ndarray[np.double_t, ndim=1] high,
            np.ndarray[np.double_t, ndim=1] low,
            np.ndarray[np.double_t, ndim=1] close, 
            int fastKPeriod,
            int slowKPeriod,
            int slowKMaType,
            int slowDPeriod,
            int slowDMaType):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] slowK = numpy.zeros((high.shape[0]))        
        np.ndarray[np.double_t, ndim=1] slowD = numpy.zeros((high.shape[0]))
        int RetCode = 0    

    RetCode = TA_STOCH(0, high.shape[0]-1, 
                <double *>high.data, <double *>low.data, <double *>close.data,  
                fastKPeriod, slowKPeriod, slowKMaType, slowDPeriod, slowDMaType,
                &start, &element_num, <double *>slowK.data, <double *>slowD.data)    
    
    corrigate(slowK, element_num, start)
    return slowK
    

def STOCH_SLOWD(np.ndarray[np.double_t, ndim=1] high,
            np.ndarray[np.double_t, ndim=1] low,
            np.ndarray[np.double_t, ndim=1] close, 
            int fastKPeriod,
            int slowKPeriod,
            int slowKMaType,
            int slowDPeriod,
            int slowDMaType):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] slowK = numpy.zeros((high.shape[0]))        
        np.ndarray[np.double_t, ndim=1] slowD = numpy.zeros((high.shape[0]))
        int RetCode = 0    

    RetCode = TA_STOCH(0, high.shape[0]-1, 
                <double *>high.data, <double *>low.data, <double *>close.data,  
                fastKPeriod, slowKPeriod, slowKMaType, slowDPeriod, slowDMaType,
                &start, &element_num, <double *>slowK.data, <double *>slowD.data)    
    
    corrigate(slowD, element_num, start)
    return slowD
    
def STOCHF_FASTK(np.ndarray[np.double_t, ndim=1] high,
            np.ndarray[np.double_t, ndim=1] low,
            np.ndarray[np.double_t, ndim=1] close, 
            int fastKPeriod,            
            int fastDPeriod,
            int fastDMaType):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] fastK = numpy.zeros((high.shape[0]))        
        np.ndarray[np.double_t, ndim=1] fastD = numpy.zeros((high.shape[0]))
        int RetCode = 0    

    RetCode = TA_STOCHF(0, high.shape[0]-1, 
                <double *>high.data, <double *>low.data, <double *>close.data,  
                fastKPeriod, fastDPeriod, fastDMaType,
                &start, &element_num, <double *>fastK.data, <double *>fastD.data)
    corrigate(fastK, element_num, start)
    return fastK
    

def STOCHF_FASTD(np.ndarray[np.double_t, ndim=1] high,
            np.ndarray[np.double_t, ndim=1] low,
            np.ndarray[np.double_t, ndim=1] close, 
            int fastKPeriod,            
            int fastDPeriod,
            int fastDMaType):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] fastK = numpy.zeros((high.shape[0]))        
        np.ndarray[np.double_t, ndim=1] fastD = numpy.zeros((high.shape[0]))
        int RetCode = 0    

    RetCode = TA_STOCHF(0, high.shape[0]-1, 
                <double *>high.data, <double *>low.data, <double *>close.data,  
                fastKPeriod, fastDPeriod, fastDMaType,
                &start, &element_num, <double *>fastK.data, <double *>fastD.data)    
        
    corrigate(fastD, element_num, start)
    return fastD
    

def STOCHRSI_FASTK(np.ndarray[np.double_t, ndim=1] input,            
            int period, 
            int fastKPeriod,            
            int fastDPeriod,
            int fastDMaType):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] fastK = numpy.zeros((input.shape[0]))        
        np.ndarray[np.double_t, ndim=1] fastD = numpy.zeros((input.shape[0]))
        int RetCode = 0    

    RetCode = TA_STOCHRSI(0, input.shape[0]-1, 
                <double *>input.data,   
                period, fastKPeriod, fastDPeriod, fastDMaType,
                &start, &element_num, <double *>fastK.data, <double *>fastD.data)    
    
    corrigate(fastK, element_num, start)
    return fastK
    
    
def STOCHRSI_FASTD(np.ndarray[np.double_t, ndim=1] input,            
            int period, 
            int fastKPeriod,            
            int fastDPeriod,
            int fastDMaType):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] fastK = numpy.zeros((input.shape[0]))        
        np.ndarray[np.double_t, ndim=1] fastD = numpy.zeros((input.shape[0]))
        int RetCode = 0    

    RetCode = TA_STOCHRSI(0, input.shape[0]-1, 
                <double *>input.data,   
                period, fastKPeriod, fastDPeriod, fastDMaType,
                &start, &element_num, <double *>fastK.data, <double *>fastD.data)    
    
    corrigate(fastD, element_num, start)
    return fastD


def SUM(np.ndarray[np.double_t, ndim=1] input, int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_SUM(0, input.shape[0]-1, <double *>input.data,
                period, &start, &element_num, <double *>output.data)    
    
    corrigate(output, element_num, start)
    return output
    
    
def T3(np.ndarray[np.double_t, ndim=1] input, int period, double vFactor):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_T3(0, input.shape[0]-1, <double *>input.data,
                period, vFactor, &start, &element_num, <double *>output.data)    
    
    corrigate(output, element_num, start)
    return output
    
    
def TEMA(np.ndarray[np.double_t, ndim=1] input, int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_TEMA(0, input.shape[0]-1, <double *>input.data, 
                period, &start, &element_num, <double *>output.data)    
    
    corrigate(output, element_num, start)
    return output

def TRANGE(np.ndarray[np.double_t, ndim=1] high,
            np.ndarray[np.double_t, ndim=1] low,
            np.ndarray[np.double_t, ndim=1] close):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0    

    RetCode = TA_TRANGE(0, high.shape[0]-1, 
                <double *>high.data, <double *>low.data, <double *>close.data,
                &start, &element_num, <double *>output.data)    
    
    corrigate(output, element_num, start)
    return output
    
def TRIMA(np.ndarray[np.double_t, ndim=1] input, int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_TRIMA(0, input.shape[0]-1, <double *>input.data, 
                period, &start, &element_num, <double *>output.data)    
    
    corrigate(output, element_num, start)
    return output

def TRIX(np.ndarray[np.double_t, ndim=1] input, int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_TRIX(0, input.shape[0]-1, <double *>input.data, 
                period, &start, &element_num, <double *>output.data)    
    
    corrigate(output, element_num, start)
    return output

def TSF(np.ndarray[np.double_t, ndim=1] input, int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_TSF(0, input.shape[0]-1, <double *>input.data, 
                period, &start, &element_num, <double *>output.data)    
    
    corrigate(output, element_num, start)
    return output

def TYPPRICE(np.ndarray[np.double_t, ndim=1] high,
            np.ndarray[np.double_t, ndim=1] low,
            np.ndarray[np.double_t, ndim=1] close):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0    

    RetCode = TA_TYPPRICE(0, high.shape[0]-1, 
                <double *>high.data, <double *>low.data, <double *>close.data,
                &start, &element_num, <double *>output.data)    
    
    corrigate(output, element_num, start)
    return output

def ULTOSC(np.ndarray[np.double_t, ndim=1] high,
            np.ndarray[np.double_t, ndim=1] low,
            np.ndarray[np.double_t, ndim=1] close,
            int period1,
            int period2,
            int period3):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0    

    RetCode = TA_ULTOSC(0, high.shape[0]-1, 
                <double *>high.data, <double *>low.data, <double *>close.data,
                period1, period2, period3, 
                &start, &element_num, <double *>output.data)    
    
    corrigate(output, element_num, start)
    return output

def VAR(np.ndarray[np.double_t, ndim=1] input, int period, double dev):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_VAR(0, input.shape[0]-1, <double *>input.data, 
                period, dev, &start, &element_num, <double *>output.data)    
    
    corrigate(output, element_num, start)
    return output

def WCLPRICE(np.ndarray[np.double_t, ndim=1] high,
            np.ndarray[np.double_t, ndim=1] low,
            np.ndarray[np.double_t, ndim=1] close):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0    

    RetCode = TA_WCLPRICE(0, high.shape[0]-1, 
                <double *>high.data, <double *>low.data, <double *>close.data,
                &start, &element_num, <double *>output.data)    
    
    corrigate(output, element_num, start)
    return output

def WILLR(np.ndarray[np.double_t, ndim=1] high,
            np.ndarray[np.double_t, ndim=1] low,
            np.ndarray[np.double_t, ndim=1] close,
            int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((high.shape[0]))        
        int RetCode = 0    

    RetCode = TA_WILLR(0, high.shape[0]-1, 
                <double *>high.data, <double *>low.data, <double *>close.data,
                period, &start, &element_num, <double *>output.data)    
    
    corrigate(output, element_num, start)
    return output


def WMA(np.ndarray[np.double_t, ndim=1] input, int period):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    
        
    RetCode = TA_WMA(0, input.shape[0]-1, 
                <double *>input.data, period,
                &start, &element_num, <double *>output.data)    
    
    corrigate(output, element_num, start)    
    return output


#@cython.boundscheck(False)
cdef void corrigate(np.ndarray[np.double_t, ndim=1] output, int element_num = 0, int start = 0):
    cdef:
        unsigned int j = 0
        unsigned int i = output.shape[0]
    
    #print 'before:', output
    #ha az element_num nagyobb, akkor a talib rossz eredményt adott, és nem értelmezhető az elementÜnum
    if element_num >= i:
        return
    #uj kod a talib baszas kivedesere..... ha ez nincs benne, akkor bizonyos esetekben elbaszodnak a talib eredmenyek
    if element_num + start >=i:
        element_num = i - start
        
    j = element_num
    while j:
        j -= 1
        i -= 1
        output[i] = output[j]        
    while i:
        i -= 1
        output[i] = 0

    #print 'after:', output


def CEIL(np.ndarray[np.double_t, ndim=1] input):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
        int RetCode = 0    

    RetCode = TA_CEIL(0, input.shape[0]-1, <double *>input.data, &start, &element_num, <double *>output.data)    
    
    return output

def COS(np.ndarray[np.double_t, ndim=1] input):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
        int RetCode = 0    

    RetCode = TA_COS(0, input.shape[0]-1, <double *>input.data, &start, &element_num, <double *>output.data)    
    
    return output


def COSH(np.ndarray[np.double_t, ndim=1] input):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
        int RetCode = 0    

    RetCode = TA_COSH(0, input.shape[0]-1, <double *>input.data, &start, &element_num, <double *>output.data)    
    
    return output

def DIV(np.ndarray[np.double_t, ndim=1] input,
        np.ndarray[np.double_t, ndim=1] input2):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
        int RetCode = 0    

    RetCode = TA_DIV(0, input.shape[0]-1, <double *>input.data, <double *>input2.data, &start, &element_num, <double *>output.data)    
    
    return output

def EXP(np.ndarray[np.double_t, ndim=1] input):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
        int RetCode = 0    

    RetCode = TA_EXP(0, input.shape[0]-1, <double *>input.data, &start, &element_num, <double *>output.data)    
    
    return output


def FLOOR(np.ndarray[np.double_t, ndim=1] input):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
        int RetCode = 0    

    RetCode = TA_FLOOR(0, input.shape[0]-1, <double *>input.data, &start, &element_num, <double *>output.data)    
    
    return output


def LN(np.ndarray[np.double_t, ndim=1] input):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
        int RetCode = 0    

    RetCode = TA_LN(0, input.shape[0]-1, <double *>input.data, &start, &element_num, <double *>output.data)    
    
    return output


def LOG10(np.ndarray[np.double_t, ndim=1] input):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))                
        int RetCode = 0    

    RetCode = TA_LOG10(0, input.shape[0]-1, <double *>input.data, &start, &element_num, <double *>output.data)    
    
    return output


#def MAXINDEX(np.ndarray[np.double_t, ndim=1] input, int period):
#    cdef:
#        int start = 0
#        int element_num = 0 
#        np.ndarray[np.int_t, ndim=1] output = numpy.zeros((input.shape[0]), numpy.int)        
#        int RetCode = 0    
#
#    RetCode = TA_MAXINDEX(0, input.shape[0]-1, <double *>input.data, period, &start, &element_num, <int *>output.data)    
#    
#    return (start, element_num, output)
#
#
#def MININDEX(np.ndarray[np.double_t, ndim=1] input, int period):
#    cdef:
#        int start = 0
#        int element_num = 0 
#        np.ndarray[np.int_t, ndim=1] output = numpy.zeros((input.shape[0]), numpy.int)        
#        int RetCode = 0    
#
#    RetCode = TA_MININDEX(0, input.shape[0]-1, <double *>input.data, period, &start, &element_num, <int *>output.data)    
#    
#    return (start, element_num, output)
#
#
#def MINMAX(np.ndarray[np.double_t, ndim=1] input, int period):
#    cdef:
#        int start = 0
#        int element_num = 0 
#        np.ndarray[np.double_t, ndim=1] min = numpy.zeros((input.shape[0]))        
#        np.ndarray[np.double_t, ndim=1] max = numpy.zeros((input.shape[0]))
#        int RetCode = 0    
#
#    RetCode = TA_MINMAX(0, input.shape[0]-1, <double *>input.data, period, &start, &element_num, <double *>min.data, <double *>max.data)    
#    
#    return (start, element_num, min, max)
#
#def MINMAXINDEX(np.ndarray[np.double_t, ndim=1] input, int period):
#    cdef:
#        int start = 0
#        int element_num = 0 
#        np.ndarray[np.int_t, ndim=1] minindex = numpy.zeros((input.shape[0]), numpy.int)
#        np.ndarray[np.int_t, ndim=1] maxindex = numpy.zeros((input.shape[0]), numpy.int)        
#        int RetCode = 0    
#
#    RetCode = TA_MINMAXINDEX(0, input.shape[0]-1, <double *>input.data, period, &start, &element_num, <int *>minindex.data, <int *>maxindex.data)    
#    
#    return (start, element_num, minindex, maxindex)


def MULT(np.ndarray[np.double_t, ndim=1] input1, 
        np.ndarray[np.double_t, ndim=1] input2):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input1.shape[0]))        
        int RetCode = 0    

    RetCode = TA_MULT(0, input1.shape[0]-1, <double *>input1.data,<double *>input2.data, &start, &element_num, <double *>output.data)    
    
    return (start, element_num, output)


def SIN(np.ndarray[np.double_t, ndim=1] input):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_SIN(0, input.shape[0]-1, <double *>input.data, 
                &start, &element_num, <double *>output.data)    
    
    return (start, element_num, output)

def SINH(np.ndarray[np.double_t, ndim=1] input):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_SINH(0, input.shape[0]-1, <double *>input.data, 
                &start, &element_num, <double *>output.data)    
    
    return (start, element_num, output)

def SQRT(np.ndarray[np.double_t, ndim=1] input):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_SQRT(0, input.shape[0]-1, <double *>input.data,
                &start, &element_num, <double *>output.data)    
    
    return (start, element_num, output)

    
def SUB(np.ndarray[np.double_t, ndim=1] input,
        np.ndarray[np.double_t, ndim=1] input2
        ):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_SUB(0, input.shape[0]-1, <double *>input.data, <double *>input2.data,
                &start, &element_num, <double *>output.data)    
    
    return (start, element_num, output)

def TAN(np.ndarray[np.double_t, ndim=1] input):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_TAN(0, input.shape[0]-1, <double *>input.data, 
                &start, &element_num, <double *>output.data)    
    
    return (start, element_num, output)

def TANH(np.ndarray[np.double_t, ndim=1] input):
    cdef:
        int start = 0
        int element_num = 0 
        np.ndarray[np.double_t, ndim=1] output = numpy.zeros((input.shape[0]))        
        int RetCode = 0    

    RetCode = TA_TANH(0, input.shape[0]-1, <double *>input.data, 
                &start, &element_num, <double *>output.data)    
    
    return (start, element_num, output)
    