'''
Created on Mar 28, 2011

@author: gdtlive
'''
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import os.path
import sys
import numpy
#import os

current = os.path.dirname(__file__)
try:
    os.unlink(current + os.sep + '_backtest/strategies.cpp')
    os.unlink(current + os.sep + '_backtest/simulatedaccount.cpp')
    os.unlink(current + os.sep + '_backtest/simulatedaccount.so')
    os.unlink(current + os.sep + 'strategies.so')
except:
    pass


if len(sys.argv) == 1:
    sys.argv.append('build_ext')
    sys.argv.append('--inplace')

if sys.platform == "linux2" :
    include_dir = os.path.dirname(__file__) + os.sep + '_backtest'
    lib_dir = os.path.dirname(__file__) + os.sep + '_backtest'
elif sys.platform == "win32":
    pass

ext1 = Extension('strategies', ['_backtest/strategies.pyx'],
                include_dirs = [include_dir, '/home/gdt/workspace/Live/src', numpy.get_include(), '/usr/include'],
                library_dirs = ['libc', 'stdc++', '/usr/lib', lib_dir],
                language = 'c++'
                 )

ext = Extension("simulatedaccount", ["_backtest/simulatedaccount.pyx",
                                '_backtest/account.cpp',
                                '_backtest/Trade.cpp',
                                '_backtest/mmplugin.cpp',
                                '_backtest/performance.cpp'],
                include_dirs = [include_dir, '/home/gdt/workspace/Live/src', numpy.get_include(), '/usr/include', '/usr/include/gsl', '/usr/include/ta-lib/'],
                library_dirs = ['/usr/lib', lib_dir, "/usr/lib/"],
                libraries = ['gsl', 'gslcblas', "ta_lib"],
                language = 'c++'
                )


setup(ext_modules = [ext, ext1],
    cmdclass = {'build_ext': build_ext})

c = os.path.dirname(__file__)
os.rename(c + os.sep + 'simulatedaccount.so', c + os.sep + '_backtest' + os.sep + 'simulatedaccount.so')
