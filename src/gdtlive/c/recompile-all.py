'''
Created on May 7, 2011

@author: kulcsar
'''
import sys
import os.path

print 'TALIB'.center(100,'-')
import talib_setup
print 'SIGNAL'.center(100,'-')
import signal_setup
print 'BACKTEST'.center(100,'-')
import backtest_setup
print 'LIVE'.center(100,'-')
import live_setup    
