from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

import numpy, sys, os
current = os.path.dirname(__file__)

try:
    os.unlink(current + os.sep + '_signal/signal.c')
except:
    pass
try:
    os.unlink(current + os.sep + 'signal.so')
except:
    pass
try:
    os.unlink(current + os.sep + '_signal/transform.c')
except:
    pass
try:
    os.unlink(current + os.sep + 'transform.so')
except:
    pass



if len(sys.argv) == 1:
    sys.argv.append('build_ext')
    sys.argv.append('--inplace')


signal_ext = Extension("signal", ["_signal/signal.pyx"],
    include_dirs=[numpy.get_include()],    
)

transform_ext = Extension("transform", ["_signal/transform.pyx"],
    include_dirs=[numpy.get_include()],    
)


setup(ext_modules=[signal_ext, transform_ext],
    cmdclass = {'build_ext': build_ext})