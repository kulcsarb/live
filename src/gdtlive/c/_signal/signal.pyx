import numpy as np
cimport numpy as np
cimport cython

@cython.boundscheck(False)
def on_signal(inp, sig, float w, int inv, int validFrom):
    cdef:            
        np.ndarray[np.int8_t, ndim=1] input = inp.astype(np.int8)
        np.ndarray[np.double_t, ndim=1] signal = sig
        float weight = w
        int invert = inv
        unsigned int length = input.shape[0]
        unsigned int i
    for i in range(validFrom + 1, length):        
        if input[i]==1 and input[i-1]==0:
            signal[i] = 1 * weight * invert

@cython.boundscheck(False)
def off_signal(inp, sig, float w, int inv, int validFrom):
    cdef:            
        np.ndarray[np.int8_t, ndim=1] input = inp.astype(np.int8)
        np.ndarray[np.double_t, ndim=1] signal = sig
        float weight = w
        int invert = inv
        unsigned int length = input.shape[0]
        unsigned int i
    for i in range(validFrom + 1, length):        
        if input[i]==0 and input[i-1]==1:
            signal[i] = 1 * weight * invert           
            
@cython.boundscheck(False)
def onoff_signal(inp, sig, float w, int inv, int validFrom):
    cdef:            
        np.ndarray[np.int8_t, ndim=1] input = inp.astype(np.int8)
        np.ndarray[np.double_t, ndim=1] signal = sig
        float weight = w
        int invert = inv
        unsigned int length = input.shape[0]
        unsigned int i
    for i in range(validFrom + 1, length):        
        if input[i]==1 and input[i-1]==0:
            signal[i] = 1 * weight * invert              
        if input[i]==0 and input[i-1]==1:
            signal[i] = -1 * weight * invert
            
            
@cython.boundscheck(False)
def transform_signal(inp, sig, int d, int s):
    cdef:            
        np.ndarray[np.double_t, ndim=1] input = inp
        np.ndarray[np.double_t, ndim=1] signal = sig
        int divider = d
        int step = s
        float amount = 1.0 / divider
        unsigned int length = input.shape[0]
        unsigned int signal_length = divider * step
        unsigned int i
        unsigned int to
    for i in range(length, 0, -1):
        if input[i]:
            to = length if i + signal_length > length else i+signal_length            
            for j in xrange(i, to, step):
                signal[j] = amount                
        
