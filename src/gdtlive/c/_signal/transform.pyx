import numpy as np
cimport numpy as np
cimport cython


@cython.boundscheck(False)
def difference(inp, int param, float digit):
    cdef:            
        np.ndarray[np.double_t, ndim=1] input = inp.astype(np.double)
        #np.ndarray[np.double_t, ndim=1] signal = sig
        np.ndarray[np.double_t, ndim=1] signal = np.zeros(len(inp), dtype=np.double)
        int p = param        
        float d = digit
        unsigned int length = input.shape[0]
        unsigned int i
    for i in range(p, length):
        #print input[i], input[i-p], (input[i] - input[i-p]) 
        signal[i] = round((input[i] - input[i-p]) / digit, 1)
        
    return signal


@cython.boundscheck(False)
def updown(inp, int param, float digit):
    cdef:            
        np.ndarray[np.double_t, ndim=1] input = inp.astype(np.double)
        #np.ndarray[np.double_t, ndim=1] signal = sig
        np.ndarray[np.double_t, ndim=1] signal = np.zeros(len(inp), dtype=np.double)
        int p = param        
        float d = digit
        unsigned int length = input.shape[0]
        unsigned int i
    for i in range(p, length):
        #print input[i], input[i-p], (input[i] - input[i-p])
        if input[i] > input[i-p]: 
            signal[i] = 1 
        
    return signal


def shift(inp, int param):        
    cdef:
        np.ndarray[np.double_t, ndim=1] input = inp
        int j = 0
        unsigned int i = input.shape[0]    
        
    j = i - abs(param)    
    while j > 0:
        j -= 1
        i -= 1
        input[i] = input[j]                        
    while i:
        i -= 1
        input[i] = 0
        
    return input


def shift_n(inp, int param):        
    cdef:
        np.ndarray[np.double_t, ndim=1] input = inp
        np.ndarray[np.double_t, ndim=1] output = np.zeros(len(inp), dtype=np.double)
        int j = 0
        unsigned int i = input.shape[0]    
        
    j = i - abs(param)    
    while j > 0:
        j -= 1
        i -= 1
        output[i] = input[j]                        
#    while i:
#        i -= 1
#        input[i] = 0
        
    return output
