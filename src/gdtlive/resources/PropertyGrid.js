PropertyGrid = Ext.extend(PropertyGridUi, {
    initComponent: function() {
        PropertyGrid.superclass.initComponent.call(this);
        var self = this;
        var i;
        if (!self.columnNum) {
            if (self.columnWidths) {
                self.columnNum = self.columnWidths.length;
            } else {
                self.columnNum = 2;
            }
        }
        var colModel = self.getColumnModel();
        var count = colModel.getColumnCount();
        for (i = self.columnNum; i < count; i++) {
            colModel.setHidden(i, true);
        }

        if (!self.columnWidths) {
            var countRec = 1 / self.columnNum;
            self.columnWidths = [];
            for (i = 0; i < self.columnNum; i++) {
                self.columnWidths.push(countRec);
            }
        }
        self.applyColumnsWidth();
        delete self.columnWidths;
        if (typeof(self.autoExpandColumn) === 'number') {
            self.autoExpandColumnSize = 0;
        }

        /**
         * when the conponent is expanded, the content is shown
         */
        self.on('expand', function() {
            self.fireEvent('show');
        }, self);

        self.on('resize', function() {
            var colModel = self.getColumnModel();
            if (self.autoColumnRate) {
                if (!isArray(self.autoColumnRate)) {
                    self.autoColumnRate = [self.autoColumnRate];
                }
                var rest = 1;
                var allWidth = self.getView().getGridInnerWidth();
                var i;
                for (i = 0; i < self.columnNum - 1; i++) {
                    rest -= self.autoColumnRate[i];
                    colModel.setColumnWidth(i, self.autoColumnRate[i] * allWidth);
                }
                colModel.setColumnWidth(self.columnNum - 1, rest * allWidth);
            }
            if ((typeof(self.autoExpandColumn) === 'number') && (colModel.getColumnWidth(self.autoExpandColumn) < self.autoExpandColumnSize + 10)) {
                colModel.setColumnWidth(self.autoExpandColumn, self.autoExpandColumnSize + 10);
            }
        }, self);

        /**
         * fires when the grid is shown
         *  loads saved data or calls needed service
         * @event
         */
        self.on('show', function() {
            var store = self.getStore();
            if ((!store) || ((store.currGrid === self) && (!self.toReload))) {
                return;
            }
            var errMessages = {};
            if (!self.url) {
                errMessages['urlError'] = 'URL NOT SET!';
            }
            if (!self.params) {
                errMessages['paramError'] = 'PARAMS NOT SET!';
            }
            var i;
            if (store.getCount() > 0) {
                if (store.currGrid) {
                    store.currGrid.data = store.getRange();
                }
                self.clear();
            }
            store.currGrid = self;
            if ((self.data) && (!self.toReload)) {
                store.add(self.data);
                self.data = null;
            } else if (!isEmpty(errMessages)) {
                self.columnWidths = [160, 0];
                self.applyColumnsWidth();
                self.loadData(errMessages);
            } else {
                self.toReload = false;
                Ext.Ajax.request({
                    url: self.url,
                    params: self.params,
                    success: function(response) {
                        response = ajaxSuccessHandle(response);
                        if (!response) {
                            return;
                        }
                        response = response.data;
                        if (self.order) {
                            var orderedName;
                            for (i in self.order) {
                                orderedName = self.order[i];
                                if ((typeof(orderedName) === 'function') || !(orderedName in response)) {
                                    continue;
                                }
                                self.addProperty(orderedName, response[orderedName]);
                                delete response[orderedName];
                            }
                        }
                        self.loadData(response);
                        store.loading = true;
                    },
                    failure: function(response) {
                        ajaxErrorHandle(response);
                    }
                });
            }
        }, self);

        /**
         * called before the grid has been destroyed
         *  clears the references left if any
         * @event
         */
        self.on('beforedestroy', function() {
            var store = self.getStore();
            if (store.currGrid === self.gridCycles) {
                store.currGrid = null;
            }
        }, self);
    },

    /**
     * sets new url
     * @param {String} url the new url
     */
    setUrl: function(url) {
        var self = this;
        if (url !== self.url) {
            self.url = url;
            self.toReload = true;
        }
    },

    /**
     * sets new params
     * @param {Object} params the new params
     */
    setParams: function(params) {
        var self = this;
        if (!compareObjects(params, self.params)) {
            self.params = params;
            self.toReload = true;
        }
    },

    /**
     * adds a property to the grid
     * @param {String} name property name
     * @param {String} value property value
     * @param {String|undefined} prefix a prefix for the name
     */
    addProperty: function(name, value, prefix) {
        var self = this;
        var nameStr = (prefix ? prefix + ': ' : '') + name;
        if (typeof(self.autoExpandColumn) === 'number') {
            var stringToMeasure;
            if (self.autoExpandColumn === 0) {
                stringToMeasure = nameStr;
            } else if (isArray(value)) {
                stringToMeasure = value[self.autoExpandColumn - 1];
            } else {
                stringToMeasure = value;
            }
            if (gridCell === undefined) {
                gridCell = mainViewport.liveManagementP.gridContent.getView().getCell(0, 2);
            }
            self.autoExpandColumnSize = Math.max(self.autoExpandColumnSize, Ext.util.TextMetrics.measure(gridCell, stringToMeasure).width);
        }
        var store = self.getStore();
        var i;
        name = name.replace(/_/g, ' ');     //replace underscores to spaces
        var matches = /[^A-Z]([A-Z])[^A-Z]/g.exec(name);    //separate camelcase words
        if ((matches) && (matches.length > 0)) {
            name = name.replace(matches[1], ' ' + matches[1].toLowerCase());
        }
        name = name.replace(/([^A-Z])([A-Z])/g, '$1 $2')    //separate abbreviations from other words
        name = name[0].toUpperCase() + name.substr(1);      //first letter will be uppercase
        //if the value is a JSON encoded object (or array), it will be shown in parts with the prefix of the current name
        if ((typeof(value) === 'string') && ((value[0] === '{') || ((value[0] === '[')))) {
            decoded = decodeMsg(value);
            if (decoded !== value) {
                for (i in decoded) {
                    if (typeof(decoded[i]) === 'function') {
                        continue;
                    }
                    self.addProperty(i, decoded[i], name);
                }
                return;
            }
        }
        var config = {};
        config.name = nameStr;
        if (typeof(value) !== 'object') {
            config.value0 = value;
        } else {
            for (i in value) {
                if (typeof(value[i]) === 'function') {
                    continue;
                }
                config['value' + i] = value[i];
            }
        }
        store.add([new store.recordType(config)]);
    },

    /**
     * reloads the grid's data
     */
    reload: function() {
        var self = this;
        var store = self.getStore();
        store.currGrid = null;
        self.fireEvent('show');
    },

    /**
     * loads the given data to the grid's store
     * @param {Array|Object} data the data to load
     * @param {Boolean} clear if the current data must be cleared before adding new
     */
    loadData: function(data, clear) {
        var self = this;
        var i, j;
        var tmp = {};
        if (clear) {
            self.clear();
        }
        if (!data) {
            return;
        } else if (data.length === undefined) {
            for (i in data) {
                if (typeof(data[i]) === 'function') {
                    continue;
                }
                self.addProperty(i, data[i]);
            }
        } else {
            var datai;
            for (i in data) {
                datai = data[i];
                if (typeof(datai) === 'function') {
                    continue;
                }
                for (j in datai) {
                    if (typeof(datai[j]) === 'function') {
                        continue;
                    }
                    if (!(j in tmp)) {
                        tmp[j] = [];
                    }
                    tmp[j].push(datai[j]);
                }
            }
            for (i in tmp) {
                if (typeof(tmp[i]) === 'function') {
                    continue;
                }
                self.addProperty(i, tmp[i]);
            }
        }
        if (self.rendered) {
            self.fireEvent('resize');
        }
    },

    /**
     * clears grid's data
     */
    clear: function() {
        var self = this;
        var store = self.getStore();
        store.removeAll();
    },

    applyColumnsWidth: function() {
        var self = this;
        var colModel = self.getColumnModel();
        if (isInt(self.columnWidths[0])) {
            for (i = 0; i < self.columnNum; i++) {
                if (self.columnWidths[i]) {
                    colModel.setColumnWidth(i, self.columnWidths[i]);
                } else {
                    self.autoExpandColumn = i;
                }
            }
        } else {
            self.autoColumnRate = self.columnWidths;
        }
    }
});
Ext.reg('propertygrid', PropertyGrid);
PropertyGrid2 = Ext.extend(PropertyGrid, {'store': 'PropertyStore2'});
Ext.reg('propertygrid2', PropertyGrid2);