/**
 * @fileOverview
 * @author vendelin8
 * @version 0.2
 */
/**
 * a modal window with Yes and No buttons. The title, window description and handlers must be set after initialization
 * @class YesNoWindow
 */
YesNoWindow = Ext.extend(YesNoWindowUi, {
    btnYesText: 'Yes',
    btnNoText: 'No',
    /** @constructs */
    initComponent: function() {
        YesNoWindow.superclass.initComponent.call(this);
        var self = this;
        self.btnYes.text = self.btnYesText;
        self.btnNo.text = self.btnNoText;

        /**
         * fires when clicked on "No" button
         *  closes the window
         * @event
         */
        self.btnNo.on('click', function() {
            self.close();
        });
    }
});
