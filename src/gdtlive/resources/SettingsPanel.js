/**
 * @fileOverview
 * @author vendelin8
 * @version 0.5
 */
/**
 * strategy view
 * @class ManagePersonalsPanel
 * @lends ManagePersonalsPanel.prototype
 */
SettingsPanel = Ext.extend(SettingsPanelUi, {
    mainTitle: 'Settings',
    /** @constructs */
    initComponent: function() {
        SettingsPanel.superclass.initComponent.call(this);
        var self = this;
        self.title = self.mainTitle;
    }
});
