/*
 * File: AdminRolloverConfigStore.js
 * Date: Fri Jul 29 2011 15:48:12 GMT+0200 (CEST)
 * 
 * This file was generated by Ext Designer version 1.1.2.
 * http://www.sencha.com/products/designer/
 *
 * This file will be auto-generated each and everytime you export.
 *
 * Do NOT hand edit this file.
 */

AdminRolloverConfigStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        AdminRolloverConfigStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AdminRolloverConfigStore',
            root: 'data',
            autoSave: false,
            api: {
                read: {
                    url: 'rollover/listConfigs',
                    method: 'POST'
                },
                create: {
                    url: 'rollover/saveConfig',
                    method: 'POST'
                },
                update: {
                    url: 'rollover/saveConfig',
                    method: 'POST'
                },
                destroy: {
                    url: 'rollover/deleteConfig',
                    method: 'POST'
                }
            },
            fields: [
                {
                    name: 'id',
                    type: 'int'
                },
                {
                    name: 'name',
                    type: 'string'
                }
            ]
        }, cfg));
    }
});
new AdminRolloverConfigStore();