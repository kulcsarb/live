var step = 2;
var xType = 'date';
var type = 'candle';
var packSize = 100;
var trades = [];
var sequences, seqIndex, data, lastData, plot, scrollWidth, from, from2, to2, neededDownloadDataCount;
var idAutoIncrement = 0;

function addPlot(from_date, to_date, neededResult, options) {
    console.log('TEST CASE no.', idAutoIncrement);
    options = options || {}
    options.xType = xType;
    options.type = type;
    options.globalSource = globalSource;
    if (trades) {
        options.extras = ['tradeTriangle'];
    }
    textDiv = addText('test case no. ' + idAutoIncrement + ': ' + type + ' from: ' + from_date + ' to: ' + to_date);
    plot = jsplot(dynamicDiv(textDiv.offsetHeight).id, options);
    readData(getDateTs(from_date), getDateTs(to_date), neededResult);
}

onload = function() {
    document.body.style.margin = "0px 0px 0px 0px";
    heigth = window.innerHeight;
    step = 900000;
    document.body.style.overflow = 'hidden'; 
    scrollWidth = document.body.clientWidth;
    document.body.style.overflow = 'scroll'; 
    scrollWidth -= document.body.clientWidth; 
    if (!scrollWidth) {
        scrollWidth = document.body.offsetWidth - document.body.clientWidth;
    }
    document.body.style.overflow = ''; 

    globalSource = 'testData';

    /*
     * test case 0:
     * read: 2012-12-21 00:00:00 - 2012-12-31 00:00:00
     * result: [2012-12-21 00:00:00 - 2012-12-31 00:00:00]
     */
    addPlot('2012-12-21 00:00:00', '2012-12-31 00:00:00', [[[1356044400000, 1356908400000]], [[0, 1356044400000, 1356908400000, 0]], [960], [], [1356044400000, 1356908400000], 960, [0]]);

    /*
     * test case 1:
     * read: 2012-12-31 00:00:00 - 2013-03-09 00:00:00
     * result: [2012-12-21 00:00:00 - 2013-03-09 00:00:00]
     */
    addPlot('2012-12-31 00:00:00', '2013-01-09 00:00:00', [[[1356908400000, 1357686000000]], [[1, true, 1357686000000, 0]], [864], [], [1356044400000, 1357686000000], 864, [0, 960]]);
}
