//var openTrades = [];

function compareLog(error, a, b, name) {
    if (name) {
        console.error('compareObjects', name, error, a, b);
    } else {
        console.log('compareObjects', error, a, b);
    }
    return false;
}

/**
 * compares 2 js objects by their properties
 * beware of putting Ext objects to it, because they contain chain-references to their parents and children which will probably lead to an infinity loop
 * @param {object} a the first object to compare
 * @param {object} b the second object to compare
 * @return {Boolean} if they are equal
 */
function compareObjects(a, b, name) {
    if (typeof(a) !== typeof(b)) {
        return compareLog('typeof', typeof(a), typeof(b), name);
    }
    if (typeof(a) === 'object') {
        if (isArray(a) !== isArray(b)) {
            return compareLog('is array', isArray(a), isArray(b), name);
        }
        if (isArray(a)) {
            if (a.length !== b.length) {
                return compareLog('array length', a.slice(), b.slice(), name);
            }
            for (var i = 0; i < a.length; i++) {
                if (!compareObjects(a[i], b[i])) {
                    return compareLog('array element no. ' + i, a, b, name);
                }
            }
        } else {
            for (var i in a) {
                if (!(i in b)) {
                    return compareLog(i + ' not in b', a, b, name);
                }
                if (!compareObjects(a[i], b[i])) {
                    return compareLog('object element ' + i, a, b, name);
                }
            }
            for (var i in b) {
                if (!(i in a)) {
                    return compareLog(i + ' not in a', a, b, name);
                }
            }
        }
    } else if (a.toString() !== b.toString()) {
        return compareLog('toString', a.toString(), b.toString(), name);
    }
    return true;
}

function dynamicDiv(text) {
    var margin = 8;
    var height = document.body.clientHeight;
    var width = document.body.clientWidth - ((document.body.clientHeight < document.body.scrollHeight) ? 0 : scrollWidth);
    if (typeof(text) === 'number') {
        height -= text + margin * 2;
        text = null;
    }
    var dynDiv = document.createElement("div");
    if (text) {    //text element
        var textNode = document.createTextNode(text);
        dynDiv.style.margin = margin + 'px';
        width -= 2 * margin;
        dynDiv.style.fontSize = "20px";
        dynDiv.appendChild(textNode);
    } else {       //div element
        dynDiv.style.backgroundColor = 'gray';    //TODO
        id = 'jsplot' + idAutoIncrement;
        idAutoIncrement++;
        dynDiv.id = id;
        dynDiv.style.height = height + "px";
    }
    dynDiv.style.width = width + "px";
//console.log('width', width);
    document.body.appendChild(dynDiv);
    return dynDiv;
}

function _sortNumber(a, b) {
    if (typeof(a) === 'object') {
        return a[0] - b[0];
    } else {
        return a - b;
    }
}

function createIndexes() {
    retVal = [];
    for (x = from2; x < to2; x += step) {
        if (xType === 'date') {
            retVal.push([tsToDate(x) + " " + tsToTime(x)]);
        } else {
            retVal.push([x]);
        }
    }
    return retVal;
}

function pack(retVal) {
    if (packSize) {
        var tmp = retVal;
        retVal = [];
        var i = 0;
        while (true) {
            retVal.push(tmp.slice(i, i + packSize));
            i += packSize;
            if (i >= tmp.length) {
                break;
            }
        }
    }
    return retVal;
}

function lineData(functions) {
    var i, x, f;
    if (functions === undefined) {
        functions = ['sin(x)'];
    } else if (functions.length === undefined) {
        functions = [functions];
    }
    var retVal = createIndexes();
    var part = [];
    i = 0;
    for (f in functions) {
        if (typeof(functions[f]) === 'function') {
            continue;
        }
        eval('functions[f] = function(x) { return isNaN(x) ? "e!" : ' + functions[f] + ';}');
    }
    for (x = from2; x < to2; x += step) {
        for (f in functions) {
            if (functions[f](NaN) === 'e!') {
                retVal[i].push(functions[f](x));
            }
        }
        i++;
    }
    return retVal;
}

function barData() {
    var i, x, rand, sign;
    var retVal = createIndexes();
    var part = [];
    i = 0;
    var lastRef = 0;
    for (x = from2; x < to2; x += step) {
        rand = Math.random() - 0.5;
        sign = rand > 0 ? 150 : -150;
        lastRef = lastRef + rand * rand * sign;
        retVal[i].push(lastRef);
        i++;
    }
    return retVal;
}

function candleData() {
    var i, x, rand, rand2, c;
    var retVal = createIndexes();
    var part = [];
    i = 0;
    var o = 100;
    for (x = from2; x < to2; x += step) {
        rand = Math.random() - 0.5;
        c = o + rand * rand * (rand > 0 ? 50 : -50);
        retVal[i].push(o);
        rand = Math.random();
        retVal[i].push(Math.max(o, c) + rand * rand * 4);   //h
        rand2 = Math.random();
        retVal[i].push(Math.min(o, c) - rand2 * rand2 * 4); //l
        retVal[i].push(c);
        o = c;
        i++;
    }
    return retVal;
//    var i, x, funcX, rand, rand2, randRound, randRound2, sign, o, c, h, l, trade, tradeIndex;
//    var retVal = createIndexes(from, to2);
//    var part = [];
//    i = 0;
//    var mid = 100;
//    var values;
//    for (x = from; x < to2; x += step) {
//        funcX = i * 0.02;
//        values = [Math.sin(funcX), Math.cos(funcX)];
//        values.push(-values[0]);
//        values.push(-values[1]);
//        values.sort(_sortNumber);
//        l = mid + values[0];
//        if (i % 2 === 0) {
//            o = mid + values[1];
//            c = mid + values[2];
//        } else {
//            o = mid + values[2];
//            c = mid + values[1];
//        }
//        h = mid + values[3];
//        retVal[i].push(o);
//        retVal[i].push(h);
//        retVal[i].push(l);
//        retVal[i].push(c);
//        rand = Math.random();
//        randRound = Math.round(rand * 1000);
//        rand2 = Math.random();
//        randRound2 = Math.round(rand2 * 1000);
//        if ((tradeTriangle) && (openTrades.length) && (randRound % Math.round(31 / Math.sqrt(openTrades.length)) === 0)) {
//            if (openTrades.length === 1) {
//                tradeIndex = 0;
//            } else {
//                tradeIndex = randRound % openTrades.length;
//            }
//            trade = openTrades[tradeIndex];
//            openTrades.splice(tradeIndex, 1);
//            trade.closetime = tsToDate(x);
//            trade.closeprice = l + rand * (h - l);
//            trade.profit = ((trade.direction === 'BUY') ? 1 : -1) * (trade.closeprice - trade.openprice) * trade.amount;
//        }
//        if ((tradeTriangle) && (randRound2 % 53 === 0)) {
//            trade = {'opentime': tsToDate(x), 'amount': (randRound2 % 120) + 60, 'openprice': l + rand2 * (h - l), 'direction' : (randRound2 % 2 === 1) ? 'SELL' : 'BUY', 'closetime': 0, 'type': 'CLOSE'};
//            openTrades.push(trade);
//            trades.push(trade);
//        }
//        i++;
//    }
//    return pack(retVal);
}

function _searchLow(o, v) {
    var h = o.length, l = -1, m;
    while (h - l > 1) {
        if (o[m = h + l >> 1] <= v) {
            l = m;
        } else {
            h = m;
        }
    }
    return l;
}

function generateTrades() {
    var timeDiff = plot._dataLength * step;
    var timeDiff2 = timeDiff * 2;
    var tradeCount = Math.max(0.005 * plot._dataLength, 2);
//tradeCount = 4;
//var as = [77, 185, 464, 518];
//var bs = [800, 801, 802, 803, 804];
    var i, a, b, trade, candle;
    var plotData = plot._source._yData;
    var plotIndex = plot._source._xData;
    for (i = 0; i < tradeCount; i++) {
        a = from + Math.round(Math.random() * timeDiff);
//a = from + as[i] * step;
        b = from + Math.round(Math.random() * timeDiff2);
//b = from + bs[i] * step;
        trade = {'amount': 100, 'direction' : (Math.random() < 0.5) ? 'SELL' : 'BUY'}
        if (a > b) {
            trade.opentime = b;
            trade.closetime = a;
        } else {
            trade.opentime = a;
            trade.closetime = b;
        }
        candle = _searchLow(plotIndex, trade.opentime);
        trade.openprice = plotData[candle][1] + Math.random() * (plotData[candle][2] - plotData[candle][1]);
        if (trade.closetime > from + timeDiff) {
            delete trade.closetime;
        } else {
            candle = _searchLow(plotIndex, trade.closetime);
            trade.closeprice = plotData[candle][1] + Math.random() * (plotData[candle][2] - plotData[candle][1]);
            trade.type ='CLOSE';
        }
//        trade.profit = (trade.closeprice - trade.openprice) * trade.amount * (trade.direction === 'BUY' ? 1 : -1);
        trades.push(trade);
    }
//console.log(trades.slice());
    plot.readExtra(trades);
    plot.plot();
}

function readData(dataFromValue, dataToValue, neededResult) {
    dataFromValue -= dataFromValue % step;
    if (dataToValue % step) {
        dataToValue += step - (dataToValue % step);
    }
    from = dataFromValue;
    sequences = plot.dataToDownload(dataFromValue, dataToValue - step, dataToValue);
    compareObjects(sequences, neededResult[0], 'dataToDownload');
    compareObjects(plot._downloadSequence, neededResult[1], 'downloadSequence');
    neededDownloadDataCount = neededResult[2];
    seqIndex = -1;
    data = [];
    lastData = null;
//if (jsplot._autoId === 2) {
//    throw "e!";
//}
    readNext();
    compareObjects(plot._source._spaceIndex, neededResult[3], 'spaceIndex');
    compareObjects(plot._source._intervals, neededResult[4], 'intervals');
    compareObjects(plot._dataLength, neededResult[5], 'dataLength');
    var globalDiffs = [];
    for (var i = 0; i < plot._source._plots.length; i++) {
        globalDiffs.push(plot._source._plots[i]._globalDiff);
    }
    compareObjects(globalDiffs, neededResult[6], 'global indexes');
}

function readNext() {
    if (data.length) {
        var curr = data.splice(0, 1)[0];
        plot.read(curr, seqIndex);
        readNext();
    } else if (lastData) {
        plot.read(lastData, seqIndex, 1);
        lastData = null;
        readNext();
    } else {
        if (!sequences.length) {
            if (!!trades) {
                generateTrades();
            }
            trades = [];
            return;
        }
        seqIndex++;
        var currSeq = sequences.splice(0, 1)[0];
        from2 = currSeq[0];
        to2 = currSeq[1];
        if (type === 'line') {
            data = lineData(['x', '10 * Math.sqrt(x)', 'x * (x - 50) * 0.01', 'Math.log(x) * 30', '50 - x', 'Math.sin(x * 0.1) * 100', 'Math.exp((x - 100) * 0.035)', 'Math.atan(x * 0.1) * 100']);
        } else if ((type === 'bar') || (type === 'distrib')) {
            data = barData();
        } else if (type === 'candle') {
            data = candleData();
        }
        compareObjects(data.length, neededDownloadDataCount.splice(0, 1)[0], 'downloaded data length');
        data = pack(data);
        lastData = data.pop();
        readNext();
    }
}

function addText(text) {
    return dynamicDiv(text);
}