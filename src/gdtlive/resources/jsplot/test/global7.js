var step = 2;
var xType = 'date';
var type = 'candle';
var packSize = 100;
var trades = [];
var sequences, seqIndex, data, lastData, plot, scrollWidth, from, from2, to2, neededDownloadDataCount;
var idAutoIncrement = 0;

function addPlot(from_date, to_date, neededResult, options) {
    console.log('TEST CASE no.', idAutoIncrement);
    options = options || {}
    options.xType = xType;
    options.type = type;
    options.globalSource = globalSource;
    if (trades) {
        options.extras = ['tradeTriangle'];
    }
    textDiv = addText('test case no. ' + idAutoIncrement + ': ' + type + ' from: ' + from_date + ' to: ' + to_date);
    plot = jsplot(dynamicDiv(textDiv.offsetHeight).id, options);
    readData(getDateTs(from_date), getDateTs(to_date), neededResult);
}

onload = function() {
    document.body.style.margin = "0px 0px 0px 0px";
    heigth = window.innerHeight;
    step = 3600000;
    document.body.style.overflow = 'hidden'; 
    scrollWidth = document.body.clientWidth;
    document.body.style.overflow = 'scroll'; 
    scrollWidth -= document.body.clientWidth; 
    if (!scrollWidth) {
        scrollWidth = document.body.offsetWidth - document.body.clientWidth;
    }
    document.body.style.overflow = ''; 

    globalSource = 'testData';

    /*
     * test case 0:
     * read: 2009-01-02 00:00:00 - 2009-01-28 00:00:00
     * result: [2009-01-02 00:00:00 - 2009-01-28 00:00:00]
     */
    addPlot('2009-01-02 00:00:00', '2009-01-28 00:00:00', [[[1230850800000, 1233097200000]], [[0, 1230850800000, 1233097200000, 0]], [624], [], [1230850800000, 1233097200000], 624, [0]]);
//    readData(getDateTs('2008-01-01 00:00:00'), getDateTs('2009-01-01 00:00:00'), [[[1199142000000, 1230764400000]], [[0, 1199142000000, 1230764400000, 0]], [8784], [8784], [1199142000000, 1230764400000, 1230850800000, 1233097200000], 8784, [0]]);

    /*
     * test case 1:
     * read: 2008-01-01 00:00:00 - 2009-01-01 00:00:00
     * result: [2008-01-01 00:00:00 - 2009-01-01 00:00:00, 2009-01-02 00:00:00 - 2009-01-28 00:00:00]
     */
//    addPlot('2008-01-01 00:00:00', '2009-01-01 00:00:00', [[[1199142000000, 1230764400000]], [[0, 1199142000000, 1230764400000, 0]], [8784], [8784], [1199142000000, 1230764400000, 1230850800000, 1233097200000], 8784, [8784, 0]]);

    /*
     * test case 2:
     * read: 2009-01-02 00:00:00 - 2009-01-28 00:00:00
     * result: [2008-12-25 00:00:00 - 2009-01-01 00:00:00, 2009-01-02 00:00:00 - 2009-01-28 00:00:00]
     */
//    addPlot('2009-01-02 00:00:00', '2009-01-28 00:00:00', [[], [], [], [168], [1230159600000, 1230764400000, 1230850800000, 1233097200000], 624, [168, 0, 168]]);

    /*
     * test case 3:
     * read: 2008-12-25 00:00:00 - 2009-01-01 00:00:00
     * result: [2008-12-25 00:00:00 - 2009-01-01 00:00:00, 2009-01-02 00:00:00 - 2009-01-28 00:00:00]
     */
//    addPlot('2008-12-25 00:00:00', '2009-01-01 00:00:00', [[], [], [], [168], [1230159600000, 1230764400000, 1230850800000, 1233097200000], 168, [168, 0, 168, 0]]);
}
