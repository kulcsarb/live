/**
 * @fileOverview
 * @author Gergely Bodi <vendelin8@gmail.com>
 * @version 0.22.1
 * @description JSPlot: A library for plotting Stock Charts and other types of data using HTML5 canvas
 * 
 * Copyright (c) 2011 - Gergely Bodi <vendelin8@gmail.com>
 * 
 * Licensed under the GPL License
 * http://www.gnu.org/copyleft/gpl.html
 */

if ((!window.console) || (!console.log)) {
    var consoleFunctions = ['log', 'debug', 'info', 'warn', 'error', 'assert', 'dir', 'dirxml', 'group', 'groupEnd', 'time', 'timeEnd', 'count', 'trace', 'profile', 'profileEnd'];

    window.console = {};
    for (var i = 0; i < consoleFunctions.length; i++) {
        window.console[consoleFunctions[_i]] = function() {}
    }
}

/**
 * the plot object
 * @class jsplot
 * @lends jsplot.prototype
 */
jsplot = function(targetId, options) {
    /** constructor */
    return new jsplot._fn._init(targetId, options);
}

/** global settings */
jsplot['sett'] = {         //canvas default options
    'candleWidth': 8,     //the default candle width in pixels
    'candleMin': 3.5,     //the minimum candle size in pixels
    'candleMax': 25,      //the maximum candle size in pixels
    'triangleMin': 15,    //the minimum triangle size in pixels
    'distribRadiusMult': 0.25,    //the multiplier (to current candle width) of the distribution type's circle in pixels
    'maxDistribRadius': 20,       //the maximum radius for the distribution circles' radius in pixels
    'gridLengthY': 135,   //an approximated distance between vertical grid lines in pixels
    'gridMaxX': 90,       //the maximum distance between horizontal grid lines in pixels
    'yLabelWidth': 35,    //y axis label width in pixels; it will be grown if needed
    'margin': 5,          //the default margin in pixels
    'zoomPlotHeight': 50, //height of the zoom plot in pixels
    'textLineBreak': 5,   //vertical space between text lines in pixels
    'font': 'Verdana',    //font for all texts
    'fontSizeX': 10,      //x-axis labels' font size
    'fontSizeY': 10,      //y-axis labels' font size
    'fontSizeTooltip': 10,    //tooltip font size
    'fontSizeMessage': 20,    //message font size
    'fontSizeError': 20,  //error font size
    'fontSizeLegend': 10, //legend font size
    'reservedColors': ['fitness', 'profit'],  //for the first n colors: parameter 'labels' in options
    'colorscheme': 'light',   //default color scheme, can be 'light' or 'dark'
    'zoomPanel': true,    //if zoom is needed
    'pannable': true,     //if pan will be available
    'tippable': true,     //if tooltips will be available
    'errorHandler': console.error.bind(console),  //custom error handler function;
    //options are:
    //console error: function message( console.error(message); }
    //javascipt alert: function(message) { alert(message); }
    //in case of Ext framework: function(message) { Ext.Msg.alert('failure', message); }
    'errorPriority': 0,   //error messages with higher priority than this number will be shown:
    //0->debug; 1->info; 2->warning; 3->error
    'dataReadPolicy': 15, //how read errors are treated: 0-1 bits->plot data; 2-3 bits->indicator data;
    //0->no errors; 1->"read"-ing; 2->"append"-ing and no loaded data exist AND "read"-ing; 3->all errors
    'roundInput': 6,      //if not zero, rounds the loaded data to the given decimals
    'errorOnNaN': true,   //if an error message is needed when a NaN comes as input
    //when an +/-Infinity comes as input, it replaces to +/-10^replaceInf
    'replaceInf': 20,     //Inf is around 10^309, so replaceInf should be between ~10 and 308; -1 means 0
    'sourceAutoDestroy': false,   //if destroy global source when all related plots are destroyed
    'dataReadyNeeded': false,     //if the data ready set is needed before plot() can run
    'memoryLimit': 30 * 1024,     //memory limit in KiloBytes (0 for infinite)
    'memoryType': 1,      //if local or global sources are included:  0->disabled; 1->globals; 2->locals; 3->both
    'memoryAction': 'ignore', //what to do in case of an error: abort->abort current read, data load
    //can be restarted after closing some other plots to be able to clean up their sources;
    //ignore->let this one go (shows error anyway) and deny next dataToDownload or read if problem still exist
    //source default options
    'type': 'candle',     //default type is candle
    'xType': 'date',      //default x-axis label type is date
    'labelPattern': '{0}',    //default pattern for custom labels, {0} will be replaced with indexes
    'labelStartIndex': 0  //default start index for custom labels
}
/** messages */
jsplot['messages'] = {
    'loading': 'Loading...{0}',
    'tradeTriangleNetProfit': 'net profit',
    'tradeTriangleSELL': 'SELL'
}
/** errors */
jsplot['errors'] = {
    'globalMustBeIndexed': 'Global sources must be indexed.',
    'notArray': 'The given data is not an array.',
    'jsonConvert': 'JSON convertion error: data cannot be parsed.',
    'wrongInputNumber': 'Wrong number of inputs.',
    'tooManyReservedColors': "The reserved colors' number is greater than the available colors, please check your configuration.",
    'dataError': 'Plotting has been failed, because the plotted data has too large difference between minimum and maximum value. Please check your data.',
    'nanInput': 'A NaN arrived as input at {0}, variable {1}',
    'memoryAbort': 'The memory limit has been reached. Restart this plot when you closed some plots of other sources.',
    'memoryDeny': 'This plot cannot be shown because the memory limit has been reached.',
    'noDataAvailable': 'No data is available.',
    'memoryLimitReached': 'The memory limit has been reached. Please close some plots of other sources to free them.',
    'dataDownload0': 'dataToDownload got wrong input: from > to or to >= to2',
    'noGeneratorFunction': 'The given indicator has no data generator function.',
    'notfound': 'Unknown {0}: {1}',
    'indicatorName': 'Your line indicator should have a name.',
    'indicatorCount': 'Your line indicators should have a variable count number.'
}
/** styles */
jsplot['styles'] = {
    'dark': {
        'background': '#000000',  //canvas background
        'label': '#EEEEEE',   //texts
        'stroke': '#AAAAAA',  //grid lines, rectangle sides, some important lines
        'rcandle': '#FF0000', //red candles
        'gcandle': '#00FF00', //green candles
        'zoomline': '#7777ff',    //zoom line
        'zoomslider': '#ffaaaa',  //the slider rect of zoom
        'infoBgd': '#333333', //info canvas background
        //data lines for line plots
        'line': ['#FF8000', '#04B45F', '#DF01D7', '#B0B532', '#F5D0A9', '#A9F5F2', '#585858', '#FEAE2E'],
        'triangle': '#6699FF',    //triangles for 'tradeTriangle' indicator
        'trade': '#00FF33',   //lines in zoom panel for 'tradeTriangle' indicator
        'peakThrough': '#2E64FE'  //peak-through marker
    },
    'light': {
        'background': '#FBFBFB',
        'label': '#080808',
        'stroke': '#0B0B0B',
        'rcandle': '#880000',
        'gcandle': '#008800',
        'zoomline': '#000099',
        'zoomslider': '#ff0000',
        'infoBgd': '#dddddd',
        'line': ['#FF8000', '#04B45F', '#DF01D7', '#B0B532', '#F5D0A9', '#A9F5F2', '#585858', '#FEAE2E'],
        'triangle': '#6699FF',
        'trade': '#00FF33',
        'peakThrough': '#0101DF'
    }
}

jsplot._globalSources = {};  //the sources that more canvas item can use
jsplot._localSources = [];   //single sources for memory management
jsplot._actionPanel = false; //the panel where mouse action is on
jsplot._autoId = 0;          //auto id for plots that don't have id in options

/** The plot object prototype */
jsplot._fn = {
    /**
     * @constructs
     * @returns {Boolean} success
     */
    _init: function(targetId, options) {
        var self = this;

        //console log management
        if (jsplot['sett']['errorPriority'] > 0) {
            self._debug = function() {};
        } else {
            self._debug = console.debug.bind(console);
        }
        if (jsplot['sett']['errorPriority'] > 1) {
            self._info = function() {};
        } else {
            self._info = console.info.bind(console);
        }
        if (jsplot['sett']['errorPriority'] > 2) {
            self._prepWarn = function() {};
            self._warn = function() {};
        } else {
            self._prepWarn = function(msg, params) {
                return self._decodeErrorString(msg, params);
            };
            self._warn = console.warn.bind(console);
        }
        if (jsplot['sett']['errorPriority'] > 3) {
            self._prepError = function() {};
            self._error = function() {};
        } else {
            self._prepError = function(msg, params) {
                self._decodeErrorString(msg, params);
            };
            self._error = jsplot['sett']['errorHandler'];
        }

        var i, j;
        self._yExtra = {};              //extras' data
        self._tipExtra = {};            //extras' tooltip
        self['visible'] = true;         //if canvas is visible
        self._lastXmin = 0;             //the last set left x-axis index
        self._lastXmax = 0;             //the last set right x-axis index
        self._globalDiff = 0;           //the difference pointer for global sources
        self._dataLength = 0;           //the data length of the current plot (even if it uses a global source)
        self._drawingPanel = 1;         //index of the current buffer panel; 1 - _drawingPanel is the index of the active panel
        self._dataReady = false;        //if the data is ready to plot
        self._reInit = {_all: true};    //if the plotter is being reinitiated (new data may have been added since last redraw)
        self._insertCursor = false;     //used only for loading data, this means the current cursor where to insert new data
        self._zoomPanelVisible = false; //if zoom will be available
        self._zoomIndex = 0;            //index of the variable that will be plotted in zoom panel
        self._ignoreDeny = false;       //if load can go on until this one is finished
        self._downloadSequence = [];    //contains the details of the current download parts

        self._candleWidth = jsplot['sett']['candleWidth'];  //the default candle width in pixels
        self._candleMin = jsplot['sett']['candleMin'];  //the minimum candle size in pixels
        self._candleMax = jsplot['sett']['candleMax'];  //the maximum candle size in pixels
        self._gridLengthY = options['gridLengthY'] ? options['gridLengthY'] : jsplot['sett']['gridLengthY'];  //an approximated distance between vertical grid lines in pixels
        self._gridMaxX = options['gridMaxX'] ? options['gridMaxX'] : jsplot['sett']['gridMaxX'];  //the maximum distance between horizontal grid lines in pixels
        self._yLabelWidth = jsplot['sett']['yLabelWidth'];  //y axis label width in pixels

        self._target = document.getElementById(targetId);
        options = hasProperty(options) ? clone(options) : {};

        if ('id' in options) {
            self['id'] = options['id'];
        } else {    //auto id for plots that don't have id given
            self['id'] = 'jsplot' + jsplot._autoId;
            jsplot._autoId++;
        }

        var lineExtra = 0;      //count number of indicator lines
        self._extras = options['extras'] ? options['extras'] : [];    //array of extras' type
        var currentExtra;
        for (i = 0; i < self._extras.length; i++) {
            currentExtra = self._extras[i];
            if (typeof(currentExtra) !== 'object') {        //the indicator is given by it's type
                self._extras[i] = {'type': currentExtra};   //converting to object...
                currentExtra = self._extras[i];
            }

            if (currentExtra['type'] === 'line') {
                lineExtra++;
                if (!(currentExtra['name'])) {
                    self._warn(self._prepWarn('indicatorName'));
                    currentExtra['name'] = i;
                }
                if (!('showInLegend' in currentExtra)) {
                    currentExtra['showInLegend'] = true;
                }
            } else if (currentExtra['type'] === 'tradeTriangle') {
                if (!('autoProfit' in currentExtra)) {
                    currentExtra['autoProfit'] = true;
                }
            }

            if (currentExtra['type'] === 'lines') {
                if (!(currentExtra['count'])) {
                    self._warn(self._prepWarn('indicatorCount'));
                    currentExtra['count'] = 2;
                }
                lineExtra += currentExtra['count'];
                if (!(currentExtra['name'])) {
                    self._warn(self._prepWarn('indicatorName'));
                    currentExtra['name'] = [];
                    for (j = 0; j < currentExtra['count']; j++) {
                        currentExtra['name'].push(i + '.' + j.toString());
                    }
                }
                if (!('showInLegend' in currentExtra)) {
                    currentExtra['showInLegend'] = true;
                }
            }
        }

        /** style and controls */
        self._cs = jsplot['styles'][options['colorscheme'] ? options['colorscheme'] : jsplot['sett']['colorscheme']];
        self._zoomPanel = (options['zoomPanel'] !== undefined) ? options['zoomPanel'] : jsplot['sett']['zoomPanel'];  //if zoom panel is needed
        if (self._zoomPanel) {
            self._zoomPlotHeight = options['zoomPlotHeight'] ? options['zoomPlotHeight'] : jsplot['sett']['zoomPlotHeight'];
            self._zoomPanelHeight = jsplot['sett']['margin'] * 2 + self._zoomPlotHeight;
        }
        self._pannable = (options['pannable'] !== undefined) ? options['pannable'] : jsplot['sett']['pannable'];  //if pan is needed
        self._tippable = (options['tippable'] !== undefined) ? options['tippable'] : jsplot['sett']['tippable'];  //if tooltips are needed

        /** canvas 'info' for tooltips and other messages*/
        self._infoPanel = self._getCanvas('info-tooltip', true, jsplot['sett']['fontSizeMessage']);
        self._infoPanel.style.zIndex = 1000;

        self['setSource'](options['globalSource'], options);
        var source = self._source;
        if (source._globalId) {  //it is a global source
            //if destroy global source when all related plots are destroyed
            self._sourceAutoDestroy = options['sourceAutoDestroy'] ? options['sourceAutoDestroy'] : jsplot['sett']['sourceAutoDestroy'];
        }

        var actualColor, freeColors;    //assigning line colors and labels
        var reservedColorNumber = jsplot['sett']['reservedColors'].length;
        if ((options['labels'] !== undefined) || (lineExtra > 0)) {
            actualColor = reservedColorNumber;
            freeColors = self._cs['line'].length - reservedColorNumber;
            if (freeColors < 0) {
                return self._error(self._prepError('tooManyReservedColors'));
            }
        }
        if (source._type === 'candle') {        //variable label to the tooltip (and the legend)
            self._labels = ['open', 'high', 'low', 'close'];    //candle labels are fixed to [open, high, low, close]
        } else if (options['labels'] !== undefined) {  //variable labels
            self._labels = options['labels'];
            var ready;
            if (source._type === 'line') {      //colors are reserved only for line type
                self._lines = [];    //the color of the different variables
                for (i = 0; i < self._labels.length; i++) {
                    ready = false;
                    for (j = 0; j < reservedColorNumber; j++) { //check if the label has a reserved color
                        if (jsplot['sett']['reservedColors'][j] === self._labels[i]) {
                            self._lines[i] = self._cs['line'][j];
                            ready = true;
                            break;
                        }
                    }
                    if (!ready) {   //the label has no reserved color, it gets the next free one
                        self._lines[i] = self._cs['line'][actualColor];
                        actualColor++;
                        actualColor = (actualColor - reservedColorNumber) % freeColors + reservedColorNumber;
                    }
                }
            }
        }

        for (i = 0; i < self._extras.length; i++) {  //reserve colors for lines and line indicators
            currentExtra = self._extras[i];
            if (currentExtra['type'] === 'line') {
                if (!self._extralines) {
                    self._extralines = {};
                }
                self._extralines[i] = self._cs['line'][actualColor];
                actualColor++;
                actualColor = (actualColor - reservedColorNumber) % freeColors + reservedColorNumber;
            } else if (currentExtra['type'] === 'lines') {
                if (!self._extralines) {
                    self._extralines = {};
                }
                self._extralines[i] = [];
                for (j = 0; j < currentExtra['name'].length; j++) {
                    self._extralines[i].push(self._cs['line'][actualColor]);
                    actualColor++;
                    actualColor = (actualColor - reservedColorNumber) % freeColors + reservedColorNumber;
                }
            }

        }

        if (options['zoomIndex'] !== undefined) {  //index of the variable that will be plotted in zoom panel
            self._zoomIndex = options['zoomIndex'];
        } else if (source._type === 'candle') {
            self._zoomIndex = 3;     //candle zoom variable is 'close'
        }

        //if the data ready set is needed before plot() can run
        self._dataReadyNeeded = options['dataReadyNeeded'] ? options['dataReadyNeeded'] : jsplot['sett']['dataReadyNeeded'];
        self._totalLength = options['totalLength'];    //needed in case of long data to update progress percent

        if (options['maxXlabel'] !== undefined) {      //max label width
            self._maxXlabel = options['maxXlabel'];
        } else if (source._xType === 'date') {
            self._maxXlabel = tsToDate(new Date(2000, 7, 30));
        }

        if (source._type === 'line') {   //this is only available for line plots
            self._candleMin = 0;         //lines can be zoomed in all
            self._initZoom = options['initZoom'];  //the proportion of the initially shown data in case of zoomed view
        }

        if (options['visible'] === false) {    //hide canvas if needed
            self['setVisible'](false);
        } else {
            self._target._plot = self;   //this plot is the default plot for the dom target object
        }
        self._info('_init done');
        return true;
    },

    /**
     * Checks which parts of the data must be downloaded.
     * from <= data <= to < to2
     * @param {Number|String} from the first index of the new data
     * @param {Number|String} to the last index of the new data
     * @param {Number|String} to2 the next possible index after the last one
     * @returns {Boolean|Array} false on failure or the requested download sequences
     *  [[0from, 0to2], [1from, 1to2], ... , [nfrom, nto2]], where mfrom <= mdata < mto2
     */
    'dataToDownload': function(from, to, to2) {
        var self = this;
        if (self._memLimitReached()) {  //the memory limit has been reached
            if (jsplot['sett']['memDeny']) {
                return self._error(self._prepError('memoryDeny'));
            } else if (jsplot['sett']['memoryAction'] === 'abort') {
                return self._error(self._prepError('memoryAbort'));
            } else if (jsplot['sett']['memoryAction'] === 'ignore') {
                self._ignoreDeny = true;
                jsplot['sett']['memDeny'] = true;
                self._error(self._prepError('memoryLimitReached'));
            }
        } else {
            jsplot['sett']['memDeny'] = false;
        }
        self['clear'](true, !self['rendered']);
        var source = self._source;
        source._destroyable = 0;
        var i, endData, endInterval, lastSequence, endResult, tmp;
        if (source._xType === 'date') {     //convert string inputs to dates if needed
            from = getDateTs(from);
            to = getDateTs(to);
            to2 = getDateTs(to2);
        }
        if ((to >= to2) || (from > to)) {
            return self._error(self._prepError('dataDownload0'));
        }
        if (!source._indexStep) {    //the step between two indexes, suppose it constant for a source
            source._indexStep = to2 - to;
        }
        var indexStep = source._indexStep;
        self['setTotalLength']((to2 - from) / indexStep);
        self._downloadSequence = [];
        //each item is [in which interval the data needs to be inserted, startValue, endValue]
        //start and end value van be an xData index, or true if it is one side of a current interval
        var result = [];                //an array that contains intervals to download
        self._insertCursor = false;
        var fromSequence = from;
        //it's index defines to which downloadSequence it belongs
        if (!source._xData.length) {     //there were no data added yet
            tmp = [0, from, to2, 0];
            self._debug('downloadSequence.push(' + tmp + ')');
            self._downloadSequence.push(tmp);
            result.push([from, to2]);   //the downloadSequence is not needed
            self._debug('_globalDiff = 0');
            self._globalDiff = 0;        //the start of the data on this plot is 0
        } else {                        //at least one interval has been read already
            var spaceIndex = source._spaceIndex;
            var intervals = source._intervals;
            var beginLow = _searchLowExt(intervals, from, indexStep);   //the index of the interval where the new data starts
            //eg. if there is a space interval from 5 to 10 and 5 <= from value < 10, than it will be the interval's left side index
            var endLow = _searchLowExt(intervals, to2, indexStep);  //same left side of interval index for data end
            self._debug('beginLow: ' + beginLow + ' endLow: ' + endLow);
            self._globalDiff = _searchArrayExt(source._xData, from, indexStep);  //where this plot's data starts in the whole data
            if (self._globalDiff === -2) {
                self._globalDiff = 0;
            } else if (self._globalDiff === -3) {
                self._globalDiff = source._xData.length;
            }
            var add_dataLength = 0;
            if (beginLow === -2) {      //the beginning is before all others
                self._insertCursor = 0;  //insert all data to the beginning
                i = 0; //index of the first space interval to add as download sequence
            } else if (beginLow === -3) {
                i = intervals.length;
            } else {
                i = beginLow + 1;   //index of the first space interval
                if (spaceIndex.length > (beginLow >> 1)) {
                    self._insertCursor = spaceIndex[beginLow >> 1];
                }
                if ((intervals[beginLow] === from) && (beginLow % 2)) {
                    fromSequence = true;
                }
                if ((intervals[beginLow] !== from) && !(beginLow % 2)) {
                    tmp = _searchLow(source._xData, from);
                    self._debug('add_dataLength -= ' + tmp);
                    add_dataLength -= tmp;   //remove index of the first data
                }

            }
            if (endLow === -2) {
                endInterval = 0;
            } else if (endLow === -3) {
                endInterval = intervals.length;
            } else {
                endInterval = endLow + 1;
            }
            if (intervals[endInterval - 1] === to2) {
                endInterval--;
            }
            self._debug('i: ' + i + ' endInterval: ' + endInterval);
            for (; i <= endInterval; i++) {      //for all intervals between
                self._debug('in for loop: ' + i + ' add_dataLength: ' + add_dataLength);
                if (!(i % 2)) {
                    if (add_dataLength) {
                        self._debug('_dataLength += add_dataLength: ' + self._dataLength + ' += ' + add_dataLength);
                        self._dataLength += add_dataLength;
                        add_dataLength = 0;
                    }
                    if ((i < endInterval) || ((i === endInterval) && (intervals[endInterval] === to2))) {
                        endData = true;
                        endResult = intervals[i];
                    } else {
                        endData = to2;
                        endResult = to2;
                    }
                    lastSequence = [i >> 1, fromSequence, endData, 0];
                    self._debug('downloadSequence.push(' + lastSequence + ')');
                    self._downloadSequence.push(lastSequence);
                    result.push([from, endResult]);
                } else {
                    if (i > 2) {
                        tmp = spaceIndex[(i >> 1) - 1];
                        self._debug('spaceIndex.length(' + spaceIndex.length + ') > (i >> 1) - 1(' + (i >> 1) - 1 + '): add_dataLength -=' + tmp);
                        add_dataLength -= tmp;
                    }
                    self._debug('endLow:' + endLow);
                    if (i === endInterval) {
                        tmp = _searchArrayExt(source._xData, to2, indexStep);
                        self._debug('i === endInterval: add_dataLength += ' + tmp);
                        add_dataLength += tmp;
                    } else if (spaceIndex.length > i >> 1) {
                        tmp = spaceIndex[i >> 1];
                        self._debug('spaceIndex.length(' + spaceIndex.length + ') > i >> 1(' + (i >> 1) + '): add_dataLength += ' + tmp);
                        add_dataLength += tmp;
                    } else if (endLow === -3) {
                        tmp = source._xData.length;
                        self._debug('spaceIndex.length(' + spaceIndex.length + ') <= (i >> 1) + 1(' + (i >> 1) + 1 + '): add_dataLength += ' + tmp);
                        add_dataLength += tmp;
                    }
                    fromSequence = true;
                    from = intervals[i];
                    if (lastSequence) {
                        self._debug('lastSequence _dataLength = ' + add_dataLength);
                        lastSequence[3] = add_dataLength;
                        add_dataLength = 0;
                        lastSequence = null;
                    }
                }
            }
            self._debug('_dataLength += ' + add_dataLength);
            self._dataLength += add_dataLength;
        }
        self._reInit._data = true;

        if (!result.length) {       //no more data needs to be downloaded, can be plotted
            self._onReadDone();
        }
        self._info('dataToDownload: from = ' + from + ' to = ' + to + ' to2 = ' + to2 + ' result = ' + stringify(result));
        return result;
    },

    /**
     * Sets a new source to the plot.
     * @param {String|undefined} globalSource the identifier of the new global source in case of global source
     * @param {Object|Boolean|undefined} options the source options
     *  if false: default options are used
     *  if undefined: options are copied from old source, if there was any, defaults otherwise
     * @returns {Boolean} success
     */
    'setSource': function(globalSource, options) {
        var self = this;
        var source = self._source;
        if ((source) && (globalSource === source._globalId)) {
            self._info('no need to set source');
            return true;   //the same source, no set needed
        }
        var oldSource = source;
        self._removeData(true);      //init data and write 'Loading...'
        options = options ? options : ((options === false) || (!oldSource) ? {} : oldSource);
        if (oldSource) {
            self._removeData();
        }
        if ((globalSource) && (jsplot._globalSources[globalSource])) {   //it already has data
            self._source = jsplot._globalSources[globalSource];       //no need to initialize, but need to add this plot to it
            source = self._source;
            source._plots.push(self);
        } else {
            /** init new data source */
            self._source = {
                _intervals: [], //the array of the existing data intervals' ends in x-axis indexes
                _xData: [],     //the x-axis labels
                _yData: [],     //the plot data
                _spaceIndex: [],    //the spaces between data intervals in array indexes
                _destroyable: 0,    //when all canvases has been destroyed, gets the current time
                //to be able to clear the last used ones when memory is needed
                _indexStep: 0,   //the step between neighbour indexes
                _indexedInput: false,    //if the inputs' first variable contains x-axis label
                _globalId: false,    //the id of the global source if it is a global source
                _sorted: false
            };
            source = self._source;
            if ((options['xType'] === 'custom') || (options['xType'] === 'date')) {
                if (options['sorted'] !== undefined) {
                    source._sorted = options['sorted'];
                } else if (!options['indexedInput']) {
                    source._sorted = true;
                }
            }

            if (globalSource) {  //it is a global source
                source._globalId = globalSource; //add new source to global sources
                jsplot._globalSources[source._globalId] = source;
                source._plots = [self];
            } else {
                jsplot._localSources.push(source);   //add new source to local sources
            }

            source._type = options['type'] || jsplot['sett']['type'];       //the type of the plot, eg. 'candle', 'line', 'bar', 'distrib'
            source._xType = options['xType'] || jsplot['sett']['xType'];    //the type of the x axis labels, eg. 'date', 'custom'

            if (options['indexedInput'] === undefined) {
                if (source._xType === 'date') {
                    source._indexedInput = true;         //the first value in a column is the date
                } else {
                    source._indexedInput = false;
                }
            } else {
                source._indexedInput = options['indexedInput']; //if the first value is the index (or auto indexing)
            }
            if (source._xType === 'custom') { //custom label indexing
                source._labelPattern = options['labelPattern'] ? options['labelPattern'] : jsplot['sett']['labelPattern'];      //the label pattern in case of custom indexing
                source._labelStartIndex = options['labelStartIndex'] ? options['labelStartIndex'] : jsplot['sett']['labelStartIndex'];  //the start index in case of custom indexing
            }

            if ((source._globalId) && (!source._indexedInput)) {
                return self._error(self._prepError('globalMustBeIndexed'));
            }
        }
        if (oldSource) {
            self._destroySource(oldSource);
        }
        self._info('Source has been set to ' + (globalSource ? globalSource : 'local source') + ' with options ' + stringify(options));
    },

    /**
     * Stores chart data.
     *  In case of a local source if there were data stored already, it will be replaced.
     * @param {Array|String} data the data to read
     *  must be a complete block of data without skipped indexes that could contain data in the future
     * @param {Integer|undefined} downloadIndex used only for global sources, the index
     *  of the current download part from download sequence
     * @param {Boolean|undefined} finished used only for global sources, if this is the last data of the current part
     * @returns {Boolean} success
     */
    'read': function(data, downloadIndex, finished) {
        var self = this;
        var source = self._source;
        var data;
        data = self._loadData(data);
        if (typeof(data) === 'string') {    //error
            if ((jsplot['sett']['dataReadPolicy'] & 3) !== 0) { //no errors are shown
                return self._error(self._prepError(data));
            }
            self._warn(self._prepWarn(data));
            data = [];
        }
        if (downloadIndex === undefined) {
            self._removeData(true);
        }
        if (data.length) {
            self._read(data, downloadIndex, finished);
        }
        self._info('Reading ' + data.length + ' values has finished.');
        return true;
    },

    /**
     * Stores chart data.
     *  If there were data stored already, new data will be appended to it.
     * @param {Array|String} data the data to read
     * @param {Integer|undefined} downloadIndex used only for global sources, the index
     *  of the current download part from download sequence
     * @param {Boolean|undefined} finished used only for global sources, if this is the last data of the current part
     * @returns {Boolean} success
     */
    'append': function(data, downloadIndex, finished) {
        var self = this;
        var source = self._source;
        var data = self._loadData(data);
        if (typeof(data) === 'string') {    //error
            var currentErrorPolicy = jsplot['sett']['dataReadPolicy'] & 3;
            if (currentErrorPolicy === 3) { //all errors are shown
                return self._error(self._prepError(data));
            }
            if ((currentErrorPolicy === 2) && (self._dataLength === 0)) {
                return self._error(self._prepError(data));//errors are shown if no data is loaded yet
            }
            self._warn(self._prepWarn(data));
            data = [];
        }
        self._read(data, downloadIndex, finished);
        if ((self._totalLength) && ((!self['rendered']) || (self['cleared']))) {
            self._showPercent();
        }
        self._info('Appending ' + data.length + ' values has finished.');
        return true;
    },

    /**
     * Stores indicator data to the given index.
     *  If there were indicator data stored already in this index, it will be replaced.
     * @param {Array|String} data the indicator data to load
     * @param {Integer|undefined} index the index of this extra data
     * @param {Boolean|undefined} sort if the extra data needs to be sorted (by x label)
     * @returns {Boolean} success
     */
    'readExtra': function(data, index, sort) {
        var self = this;
        sort = sort || false;
        index = index || 0;
        data = self._loadData(data, sort);
        if (typeof(data) === 'string') {    //error
            var dataReadPolicy;
            if ('dataReadPolicy' in self._extras[index]) {
                dataReadPolicy = self._extras[index]['dataReadPolicy'];
            } else {
                dataReadPolicy = jsplot['sett']['dataReadPolicy'];
            }
            if ((dataReadPolicy & 12) !== 0) { //no errors are shown
                return self._error(self._prepError(data));
            }
            self._warn(self._prepWarn(data));
            data = [];
        }
        data = self._readExtra(data, index);
        if ((!data) || (data.length === 0)) {
            self._extras[index]._empty = true;
        }
        self._yExtra[index] = data;
        if (!self._extras[index]._loadLater) {
            self._info('Reading ' + data.length + ' indicator values has finished.');
        }
        return true;
    },

    /**
     * Stores indicator data to the given index.
     *  If there were indicator data stored already in this index, new indicator data will be appended to it.
     * @param {Array|String} data the indicator data to load
     * @param {Integer|undefined} index the index of this extra data
     * @param {Boolean|undefined} sort if the extra data needs to be sorted (by x label)
     * @returns {Boolean} success
     */
    'appendExtra': function(data, index, sort) {
        var self = this;
        sort = sort || false;
        index = index || 0;
        var data = self._loadData(data, sort);
        if (typeof(data) === 'string') {    //error
            var dataReadPolicy;
            if ('dataReadPolicy' in self._extras[index]) {
                dataReadPolicy = self._extras[index]['dataReadPolicy'];
            } else {
                dataReadPolicy = jsplot['sett']['dataReadPolicy'];
            }
            var currentErrorPolicy = dataReadPolicy & 12;
            if (currentErrorPolicy === 3) { //all errors are shown
                return self._error(self._prepError(data));
            }
            if ((currentErrorPolicy === 2) && (!self._yExtra[index])) {
                return self._error(self._prepError(data));//errors are shown if no data is loaded yet
            }
            return self._warn(self._prepWarn(data));
        }
        data = self._readExtra(data, index);
        if (!self._yExtra[index]) {
            self._yExtra[index] = [data];
        } else {
            self._yExtra[index].push(data);
        }
        if (!self._extras[index]._loadLater) {
            self._info('Appending ' + data.length + ' indicator values has finished.');
        }
        return true;
    },

    /**
     * Initiates or redraws plot (because of resize).
     */
    'plot': function() {
        var self = this;
        if (!self['isReadyToPlot']()) { //there was no read before
            return;
        }
        var i, j, currentExtra, converted, extraData;
        for (i = 0; i < self._extras.length; i++) {  //calculate indicators given by function, generated before plotting
            currentExtra = self._extras[i];
            if (currentExtra._loadLater) {
                delete currentExtra._loadLater;
                self['readExtra'](self._yExtra[i], i);
            } else if (((self._reInit._all) || (self._reInit._data)) && (currentExtra['func'])) {
                converted = self._generateExtra(self._globalDiff, self._globalDiff + self._dataLength, currentExtra);
                self._yExtra[i] = converted;
            }

            //optimizing trade triangle drawing by cashing open and close date order and the middle index
            if ((currentExtra['type'] === 'tradeTriangle') && (currentExtra._middleIndex === null)) {
                currentExtra._middleIndex = 0;
                _initTradeTriangle(currentExtra, self._yExtra[i]);
            }
        }
        self._initPlotCanvas();

        self._clear(self._mainPanels[self._drawingPanel]);
        self._doplot();
        self._reInit = {};
    },

    /**
     * Shows or hides plot.
     * @param {Boolean|undefined} newVal if the plot needs to be shown
     * @returns {Boolean} success
     */
    'setVisible': function(newVal) {
        var self = this;
        newVal = (newVal !== undefined) ? newVal : self['visible'];
        self['visible'] = newVal;
        if (newVal) {
            newVal = 'visible';
            if (!self['rendered']) {  //if no data has been read, 'Loading...' will be visible
                self._infoPanel.style.visibility = newVal;
            }
            self._target._plot = self;
        } else {
            newVal = 'hidden';
            self._infoPanel.style.visibility = newVal;
        }
        if (!self['rendered']) {
            return false;   //Panels do not exist!
        }
        self._leftPanel.style.visibility = newVal;
        self._topPanel.style.visibility = newVal;
        self._mainPanels[1 - self._drawingPanel].style.visibility = newVal;
        self._mainOverlay.style.visibility = newVal;
        self._bottomPanel.style.visibility = newVal;
        if ((self._zoomOverlay) && (self._zoomPanelVisible)) {
            self._zoomOverlay.style.visibility = newVal;
        }
        self._info('Plot visibility has been set to: ' + newVal);//(', self._target, ')
        return true;
    },

    /**
     * Clears all canvases of the plot.
     */
    'clear': function(setLoading, keepExtras) {
        var self = this;
        self._clear(self._infoPanel);
        if (!self['rendered']) {
            return 'Panels do not exist!';
        }
        self._clear(self._leftPanel);
        self._clear(self._topPanel);
        self._clear(self._mainPanels[0]);
        self._clear(self._mainPanels[1]);
        self._clear(self._bottomPanel);
        self._mainOverlay.removeAttribute('tabindex')
        if ((self._zoomOverlay) && (self._zoomPanelVisible)) {
            self._clear(self._zoomOverlay);
            self._zoomOverlay.style.cursor = 'default';
            self._zoomOverlay.removeAttribute('tabindex')
        }
        if (setLoading === undefined) {
            setLoading = false;
        }
        self._removeData(setLoading, keepExtras);
        self['cleared'] = true;
        self._info('Plot cleared.');
    },

    /**
     * If the plot has any indicators loaded.
     * @param {Integer|undefined} index the index of the examined indicator
     * @returns {Boolean} if the plot has any indicators loaded
     */
    'hasExtra': function(index) {
        var self = this;
        index = index || 0;
        return (!!self._yExtra[index]) || (self._extras[index]['func']) || (self._extras[index]._empty === true);
    },

    /**
     * Resets parameters for generated indicators.
     *  If the data is already generated, resets it as well.
     * @param {Array} xData x-axis data
     * @return {Boolean} success
     */
    'resetExtraParams': function(params, index) {
        var self = this;
        index = index || 0;
        var currentExtra = self._extras[index];
        if (!currentExtra['func']) {
            return self._error(self._prepError('noGeneratorFunction'));
            
        }
        currentExtra['parameters'] = params;
        if (!hasProperty(self._reInit)) {
            self._reInit._data = true;
            self['plot']();
        }   //else it will be plotted soon
        self._info('Indicator params no. ' + index + ' has been set to: ' + stringify(params));
        return true;
    },

    /**
     * Destroys dom elements for this plot.
     */
    'destroy': function() {
        var self = this;
        var source = self._source;
        self._removeCanvas(self._infoPanel);
        if (!self['rendered']) {
            return;
        }
        self._removeCanvas(self._leftPanel);
        self._removeCanvas(self._topPanel);
        self._removeCanvas(self._mainPanels[0]);
        self._removeCanvas(self._mainPanels[1]);
        self._removeCanvas(self._bottomPanel);
        if (self._zoomOverlay) {
            self._removeCanvas(self._zoomOverlay);
        }
        self._destroySource();
        self._info('Plot destroyed.');
    },

    /**
     * Sets total data length.
     *  Useful for showing the percent while loading big amount of data to a local source.
     *  This can be done by local option 'totalLength' as well.
     * @param {Integer} totalLength the length of the data
     */
    'setTotalLength': function(totalLength) {
        var self = this;
        self._totalLength = totalLength;
        self._info('Total length has been set to: ' + totalLength);
    },

    /**
     * Signifies that the data is ready to plot if dataReadyNeeded option was set.
     * @param {Boolean} newVal if the data is now ready to plot
     */
    'setDataReady': function(newVal) {
        var self = this;
        self._dataReady = newVal;
        self._info('dataReady has been set to: ' + newVal);
    },

    /**
     * Returns if we're ready to plot.
     *  All requested indicators need to have any data.
     *  If dataReadyNeeded was set, setDataReady needs to be called before this becomes true.
     *  Otherwise it is enough if the xData is not empty (any data has been loaded).
     * @param {Boolean|undefined} ignoreExtras ignores indicators if true
     * @returns {Boolean} if we're ready to plot
     */
    'isReadyToPlot': function(ignoreExtras) {
        var self = this;
        var source = self._source;
        if (!ignoreExtras) {
            for (i = 0; i < self._extras.length; i++) {
                if (!self['hasExtra'](i)) {
                    return false;
                }
            }
        }
        self._debug('isReadyToPlot: dataReadyNeeded = ' + self._dataReadyNeeded + ' dataReady = ' + self._dataReady + ' totalLength = ' + self._totalLength + ' _dataLength = ' + self._dataLength);
        return self._dataReadyNeeded ? (self._dataReady ? true : self._totalLength === self._dataLength) : self._dataLength;
    },

    /**
     * Clears all data from the plot.
     *  If the source is local, all data is deleted.
     *  Otherwise only indicators are deleted and global pointers (_dataLength, _globalDiff) are set to zero.
     * @param {Boolean|undefined} setLoading if 'Loading...' message is needed after the action.
     */
    _removeData: function(setLoading, keepExtras) {
        var self = this;
        var source = self._source;
        if (setLoading) {
            self._showMsg('loading', ['']);
        }
        if (!source) {
            return;
        }
        if (!keepExtras) {
            self._removeExtras();
        }
        self._dataLength = 0;
        if (source._globalId) {  //it has a global source, the data will not be cleared, only the indexes are reset
            self._globalDiff = 0;
        } else {
            source._xData = [];
            source._yData = [];
        }
        self._dataReady = false;
        self._info('Data removed. setLoading = ' + setLoading + ' keepExtras = ' + keepExtras);
    },

    /**
     * Clears indicator data.
     */
    _removeExtras: function() {
        var self = this;
        var j, remove;
        var i;
        for (i = 0; i < self._extras.length; i++) {  //calculate indicators given by function, generated before plotting
            delete self._yExtra[i];
            delete self._tipExtra[i];
            delete self._extras[i]._empty;
        }
        self._info('Indicators removed.');
    },

    /**
     * Destroys current data source, if it is not equal with the existing one
     * @param {Source|undefined} oldSource the previous source to destroy if set
     */
    _destroySource: function(oldSource) {
        var self = this;
        var source = oldSource ? oldSource : self._source;
        if (!source) {
            return;
        }
        if (source._plots) { //global source
            source._plots.splice(source._plots.indexOf(self), 1);
            if (!source._plots.length) {
                source._destroyable = new Date();
                if ((source._globalId) && (self._sourceAutoDestroy)) {
                    delete jsplot._globalSources[source._globalId];
                }
            }
        } else {    //delete local source
            jsplot._localSources.splice(jsplot._localSources.indexOf(source), 1);
        }
        if (!oldSource) {
            self._source = null;
        }
        self._info('Source destroyed: old source = ' + stringify(oldSource));
    },

    /**
     * Shows current how much percent of data has been loaded yet.
     *  Low level plot function, should not be used directly.
     */
    _showPercent: function() {
        var self = this;
        self._showMsg('loading', [parseFloat((self._dataLength * 100 / self._totalLength)).toFixed(1) + '%']);
    },

    /**
     * Initiates or resizes panels.
     *  Low level plot function, should not be used directly.
     * @returns {Object} the plot itself
     */
    _initPlotCanvas: function() {
        var self = this;
        var source = self._source;

        /** in case of panel resize we already have main panels */
        var info = self._infoPanel;
        var i, j, ctx, size, bottomPanelHeight, currentExtra;

        var height = self['height'];
        self['height'] = parseInt(self._target.style.height);
        var width = self['width'];
        self['width'] = parseInt(self._target.style.width);
        if ((height !== self['height']) || (width !== self['width'])) {
            self._reInit._resize = true;
        }

        if (self._reInit._all) {
            self._leftPanel = self._getCanvas('leftpanel');
            self._leftPanel.style.left = self._target.offsetLeft + 'px';
            self._leftPanel.style.top = self._target.offsetTop + 'px';
            self._topPanel = self._getCanvas('toppanel');
            self._topPanel.style.left = self._target.offsetLeft + jsplot['sett']['margin'] + 'px';
            self._topPanel.style.top = self._target.offsetTop + 'px';
        }

        if (self._reInit._resize) {
            self._setSize(self._leftPanel, jsplot['sett']['margin'], self['height']);
            self._plotWidth = self['width'] - (jsplot['sett']['margin'] * 2 + self._yLabelWidth);
        }

        var legendLabels = [];      //legend at top: variable-color pairs
        if ((self._reInit._resize) || (self._reInit._data)) {
            var topPanelHeight = self._topPanelHeight;
            if (source._type === 'line') {
                for (i = 0; i < source._dataCount; i++) {
                    legendLabels.push([self._labels[i], self._lines[i]]);
                }
            }
            for (i = 0; i < self._extras.length; i++) {    //indicators in zoom panel
                currentExtra = self._extras[i];
                if ((currentExtra['type'] === 'line') && (currentExtra['showInLegend'])) {
                    legendLabels.push([currentExtra['name'], self._extralines[i]]);
                }
                if ((currentExtra['type'] === 'lines') && (currentExtra['showInLegend'])) {
                    for (j = 0; j < currentExtra['name'].length; j++) {
                        legendLabels.push([currentExtra['name'][j], self._extralines[i][j]]);
                    }
                }
            }
            var topPanel = self._topPanel;
            ctx = topPanel._ctx;
            var sampleWidth, padding, breakAt, left, margin;
            if (legendLabels.length > 1) {
                self._topPanelHeight = jsplot['sett']['margin'] * 2 + jsplot['sett']['fontSizeLegend'] + jsplot['sett']['textLineBreak'];
                sampleWidth = 8;    //color sample rectangle width
                padding = 3;        //horizontal space before color rectangle
                margin = 8;         //horizontal space between 2 variables
                _setFont(ctx, jsplot['sett']['fontSizeLegend']);
                var widthRest = self._plotWidth + margin;    //the rest width we can fill
                var breakAt = [];   //indexes to break line
                left = [];          //left positions
                var varcountInLine = 0; //how many variables are in the current line (to make sure that every line has at least 1 line)
                for (i = 0; i < legendLabels.length; i++) {
                    varcountInLine++;
                    size = ctx.measureText(legendLabels[i][0] + ':').width;   //measure current text's width
                    left.push([self._plotWidth + margin - widthRest, size]);
                    size += margin + padding + sampleWidth;
                    widthRest -= size;
                    if ((widthRest < 0) && (varcountInLine > 1)) {  //break line
                        left[i][0] = 0;
                        breakAt.push(i);
                        self._topPanelHeight += jsplot['sett']['fontSizeLegend'] + jsplot['sett']['textLineBreak'];
                        widthRest = self._plotWidth + margin - size;
                        varcountInLine = 1;
                    }
                }
                if (topPanelHeight !== self._topPanelHeight) {
                    self._reInit._resize = true;
                }
            } else {
                self._topPanelHeight = jsplot['sett']['margin'];
            }
        }

        if (self._reInit._resize) {
            self._setSize(topPanel, self['width'] - jsplot['sett']['margin'], self._topPanelHeight);
        }
        if (legendLabels.length > 1) {   //draw legend
            var sampleHeight = 6;
            ctx.translate(0, jsplot['sett']['margin'] + jsplot['sett']['fontSizeLegend']);
            breakAt.push(Infinity);
            for (i = 0; i < legendLabels.length; i++) {
                ctx.fillStyle = self._cs['label'];
                ctx.fillText(legendLabels[i][0] + ':', left[i][0], 0);
                ctx.fillStyle = legendLabels[i][1];
                ctx.fillRect(left[i][0] + left[i][1] + padding, -jsplot['sett']['fontSizeLegend'] + sampleHeight * 0.5, sampleWidth, sampleHeight);
                if (breakAt[0] === i + 1) {
                    ctx.translate(0, jsplot['sett']['fontSizeLegend'] + jsplot['sett']['textLineBreak']);
                    breakAt.shift();
                }
            }
        }

        if (hasProperty(self._reInit)) {
            info.style.visibility = 'hidden';
        }

        if (self._plotWidth * 2 > self._candleMax * self._dataLength) { //the data is too small for zooming
            self._pannable = false;
            self._zoomPanelVisible = false;
            self._xmax = self._globalDiff + self._dataLength;
            self._xmin = self._globalDiff;
            self._candles = self._xmax - self._xmin;
            self._candleWidth = self._plotWidth / self._candles;
            self._minCandles = self._candles;
            self._maxCandles = self._candles;
            if (self._zoomOverlay) {
                self._zoomOverlay.style.visibility = 'hidden';
            }
        } else {
            self._pannable = true;
            if ((!self._zoomOverlay) || (!self._zoomPanelVisible)) {
                self._reInit._zoomPanel = true;
            }
            if (self._zoomPanel) {                            //zooming is available
                self._zoomPanelVisible = true;
            }
            self._minCandles = Math.round(self._plotWidth / self._candleMax);
            self._maxCandles = Math.min(Math.round(self._plotWidth / self._candleMin), self._dataLength);
            if (self._plotWidth > self._candleWidth * self._dataLength) {    //increases candle width for corect view
                self._xmax = self._dataLength + self._globalDiff;
                self._xmin = self._globalDiff;
                self._candles = self._dataLength;
                self._candleWidth = self._plotWidth / self._candles;
            } else if ((self._reInit._all) || ((self._reInit._data) && (self._xmax > self._globalDiff + self._dataLength))) {
                self._xmax = self._globalDiff + self._dataLength;
                if (self._initZoom) {
                    self._candles = self._dataLength * self._initZoom;
                } else {
                    self._candles = Math.round(self._plotWidth / self._candleWidth);
                }
                self._xmin = Math.min(self._xmax - 1, self._globalDiff);
                self._candleWidth = self._plotWidth / self._candles;
            } else {                                        //candle number stays the same
                self._candleWidth = self._plotWidth / self._candles;
            }
            self._xmin = self._xmax - self._candles;
            if (self._xmin < self._globalDiff) {
                self._xmin = self._globalDiff;
                self._xmax = self._xmin + self._candles;
            }
        }

        if (self._reInit._all) {
            self._mainPanels = [];   //initiate main panels (2 for being able to double buffering)
            for (i = 0; i < 2; i++) {
                self._mainPanels[i] = self._getCanvas('mainpanel' + i.toString(), true, jsplot['sett']['fontSizeX'], true);
                self._mainPanels[i].style.left = (self._target.offsetLeft + jsplot['sett']['margin']) + 'px';
                self._mainPanels[i].style.visibility = 'hidden';
            }
            ctx = self._mainPanels[0]._ctx;
            if (!self._maxXlabel) {   //the width of the x labels
                self._maxXlabel = self._xDataFormat(0);
            }
            var xLabelWidth = -Infinity;
            if (typeof(self._maxXlabel) === 'object') {
                for (i = 0; i < self._maxXlabel.length; i++) {
                    xLabelWidth = max(xLabelWidth, ctx.measureText(self._maxXlabel[i]).width);
                }
            } else {
                xLabelWidth = ctx.measureText(self._maxXlabel).width;
            }
            var gridDiff = xLabelWidth + 12;    //additional space
            var maxRotation = 0.35;
            if (source._xType === 'date') {
                maxRotation = 0.4;
            }
            var rotatedTextHeight, rotatedExtraHeight, rotatedWidth;
            if (gridDiff > self._gridMaxX) {   //the label is longer than the max grid length, max rotation
                self._gridLengthX = self._gridMaxX;
                var beta = Math.atan(gridDiff / jsplot['sett']['fontSizeX']);
                self._rotateX = Math.min(-Math.asin(self._gridMaxX * Math.sin(beta) / gridDiff) + beta, maxRotation);
                var cosRotateX = Math.cos(self._rotateX);
                var sinRotateX = Math.sin(self._rotateX);
                rotatedTextHeight = jsplot['sett']['fontSizeX'] * cosRotateX;  //rotation decreases additional line height
                rotatedExtraHeight = xLabelWidth * sinRotateX;
                rotatedWidth = xLabelWidth * cosRotateX + jsplot['sett']['fontSizeX'] * sinRotateX;
            } else {    //the label can fit horizontally, so rotation is 0
                self._gridLengthX = gridDiff;
                self._rotateX = 0;
                rotatedTextHeight = jsplot['sett']['fontSizeX'];
                rotatedExtraHeight = 0;
                rotatedWidth = xLabelWidth;
            }
            self._xLabelHeight = Math.round(rotatedTextHeight + rotatedExtraHeight) + jsplot['sett']['textLineBreak']; //text height
            self._xTop = Math.round(self._xLabelHeight * 0.5) + jsplot['sett']['textLineBreak']; //label top is at half way
            if (source._xType === 'date') {  //text height is bigger with an extra line in case of date
                //but from rotateX >= 0.57 the extra line does not give any additional space
                self._xLabelHeight = Math.round(self._xLabelHeight + (Math.max(0.57 - self._rotateX, 0)) * rotatedTextHeight / 0.57);
            }
            self._gridLengthXHalf = rotatedWidth * 0.5;
        }

        if (self._zoomPanelVisible) {   //main plot height decreases with the height of the zoom panel and separator too
            bottomPanelHeight = jsplot['sett']['margin'] * 2 + self._zoomPanelHeight + self._xLabelHeight;
        } else {                //main plot height
            bottomPanelHeight = jsplot['sett']['margin'];
        }
        var plotHeight = self._plotHeight;
        self._plotHeight = self['height'] - (self._topPanelHeight + self._xLabelHeight + bottomPanelHeight);
        if (plotHeight !== self._plotHeight) {
            self._reInit._resize = true;
        }

        if (self._reInit._resize) {
            self._setSize(self._mainPanels[0], self['width'] - jsplot['sett']['margin'], self._plotHeight + self._xLabelHeight);
            self._mainPanels[0].style.top = (self._target.offsetTop + self._topPanelHeight) + 'px';
            _setFont(self._mainPanels[0]._ctx, jsplot['sett']['fontSizeX']);
            self._setSize(self._mainPanels[1], self['width'] - jsplot['sett']['margin'], self._plotHeight + self._xLabelHeight);
            self._mainPanels[1].style.top = (self._target.offsetTop + self._topPanelHeight) + 'px';
            _setFont(self._mainPanels[1]._ctx, jsplot['sett']['fontSizeX']);
        }

        if (self._reInit._all) { //initiate main overlay for panning
            var mainOverlay = self._getCanvas('mainoverlay', false);
            self._mainOverlay = mainOverlay;
            mainOverlay.style.left = (self._target.offsetLeft + jsplot['sett']['margin']) + 'px';
            mainOverlay.tabIndex = 0;
            mainOverlay._plot = self;
            //mouse click events for panning
             /**
             * Stops pan action.
             * @event
             */
            mainOverlay._stopAction = function(event) {
                _stopMouseAction(mainOverlay);
                self._xmin = mainOverlay._newXmin;
                self._xmax = mainOverlay._newXmax;
            }
            /**
             * Fires in case of mousedown on main overlay panel if panning is enabled.
             *  Starts panning.
             * @event
             */
           if (mainOverlay.addEventListener) {
                mainOverlay.addEventListener('mousedown', _mainOverlayMousedown, false);
            } else if (mainOverlay.attachEvent) {
                mainOverlay.attachEvent('onmousedown', _mainOverlayMousedown);
            }
            /**
             * Fires in case of mouseup on main overlay panel if panning is enabled.
             *  Ends panning.
             * @event
             */
            if (mainOverlay.addEventListener) {
                mainOverlay.addEventListener('mouseup', _mainOverlayMouseup, false);
            } else if (mainOverlay.attachEvent) {
                mainOverlay.attachEvent('onmouseup', _mainOverlayMouseup);
            }
            /**
             * Fires in case of keyup on main overlay panel if panning is enabled.
             *  Ctrl+z undos current panning action.
             * @event
             */
            if (mainOverlay.addEventListener) {
                mainOverlay.addEventListener('keyup', _mainOverlayKeyup, false);
            } else if (mainOverlay.attachEvent) {
                mainOverlay.attachEvent('onkeyup', _mainOverlayKeyup);
            }
            /**
             * Fires in case of mousemove on main overlay panel.
             *  If panning is on, redraws main plot (and zoom overlay).
             *  Otherwise shows tooltips if the mouse is over a value.
             * @event
             */
            if (mainOverlay.addEventListener) {
                mainOverlay.addEventListener('mousemove', _mainOverlayMousemove, false);
            } else if (mainOverlay.attachEvent) {
                mainOverlay.attachEvent('onmousemove', _mainOverlayMousemove);
            }
            /**
             * Fires in case of mouseout on main overlay panel.
             *  Hides info if needed.
             * @event
             */
            if (mainOverlay.addEventListener) {
                mainOverlay.addEventListener('mouseout', _mainOverlayMouseout, false);
            } else if (mainOverlay.attachEvent) {
                mainOverlay.attachEvent('onmouseout', _mainOverlayMouseout);
            }
            /**
             * Wheel action on main overlay.
             * @event
             */
            if (mainOverlay.addEventListener) {  //mozilla
                mainOverlay.addEventListener('DOMMouseScroll', _wheel, false);
                mainOverlay.addEventListener('mousewheel', _wheel, false);
            } else {
                mainOverlay.onmousewheel = _wheel;    //IE/Opera
            }
            mainOverlay.style.top = (self._target.offsetTop + self._topPanelHeight) + 'px';
        }
        if ((self._reInit._resize) || (self._reInit._rightWidth)) {
            self._setSize(self._mainOverlay, self._plotWidth, self._plotHeight);
        }

        if (self._reInit._all) {     //initiate bottom panel
            self._bottomPanel = self._getCanvas('bottompanel', true, jsplot['sett']['fontSizeX'], true);
            self._bottomPanel.style.left = (self._target.offsetLeft + jsplot['sett']['margin']) + 'px';
        }

        if (self._reInit._resize) {
            self._bottomPanel.style.top = (self._target.offsetTop + self._topPanelHeight + self._plotHeight + self._xLabelHeight) + 'px';
            self._setSize(self._bottomPanel, self['width'] - jsplot['sett']['margin'], bottomPanelHeight);
        }

        //zoom is enabled, we need zoom panel and overlay
        if ((self._zoomPanelVisible) && ((self._reInit._resize) || (self._reInit._data))) {
            self._clear(self._bottomPanel);
            var xlo;
            ctx = self._bottomPanel._ctx;
            var lineBottom = jsplot['sett']['margin'] * 2;
            var lineTop = lineBottom + self._zoomPlotHeight;
            ctx.strokeStyle = self._cs['stroke'];
            ctx.lineWidth = 1;
            ctx.strokeRect(0, lineBottom, self._plotWidth, self._zoomPlotHeight);
            var zyminmax = self._minmax1d(source._yData.slice(self._globalDiff, self._globalDiff + self._dataLength), self._zoomIndex);
            self._padding = (zyminmax[1] - zyminmax[0]) * 0.05;
            zyminmax[0] -= self._padding; // little margin below
            zyminmax[1] += self._padding; // little margin above
            if (zyminmax[1] === zyminmax[0]) {
                zyminmax[1]++;
            }
            var xScale = self._plotWidth / (self._dataLength - 1);
            var yScale = -self._zoomPlotHeight / (zyminmax[1] - zyminmax[0]);

            var extraData;
            for (i = 0; i < self._extras.length; i++) {    //indicators in zoom panel
                extraData = self._yExtra[i];
                switch (self._extras[i]['type']) {
                    case 'tradeTriangle':
                        ctx.strokeStyle = self._cs['trade'];
                        ctx.lineWidth = 0.8;
                        for (j in extraData) {
                            if (typeof(extraData[j]) === 'function') {
                                continue;
                            }
                            xlo = (extraData[j][0]._time - self._globalDiff) * xScale;
                            _drawline(ctx, xlo, lineBottom, xlo, lineTop);
                        }
                        break;
                    case 'peakThrough': //peak-through is not visible in zoom panel
                        break;
                    case 'line': //line is not visible in zoom panel
                        break;
                    case 'lines': //lines are not visible in zoom panel
                        break;
                }
            }

            //x grid
            var xstop = Math.round(self._dataLength * self._gridLengthX / self._plotWidth);
            ctx.fillStyle = self._cs['label'];
            ctx.textAlign = 'center';
            ctx.lineWidth = 0.4;
            _setFont(ctx, jsplot['sett']['fontSizeX']);
            var zoomXData;
            for (i = xstop; i < self._dataLength; i += xstop) {
                xlo = i * xScale;
                _drawline(ctx, xlo, lineBottom, xlo, lineTop, self._cs['stroke']);
                ctx.save();
                ctx.translate(xlo, lineTop + self._xTop);
                if (self._rotateX) {
                    ctx.rotate(self._rotateX);
                }
                zoomXData = self._xDataFormat(i + self._globalDiff);
                if (source._xType === 'date') {
                    ctx.fillText(tsToDate(zoomXData), 0, 0);
                    ctx.fillText(tsToTime(zoomXData), 0, 12);
                } else {
                    ctx.fillText(zoomXData, 0, 0);
                }
                ctx.restore();
            }
            if ((source._type === 'distrib') || (source._type === 'bar')) {
                ctx.lineWidth = 0.5;
                var zero = self._zoomPlotHeight + lineBottom + zyminmax[0] * -yScale;
                _drawline(ctx, 0, zero, self._plotWidth, zero, self._cs['stroke']);
            }
            ctx.save();
            ctx.translate(0, self._zoomPlotHeight + lineBottom + zyminmax[0] * -yScale);
            if (xScale > 0.3) {
                ctx.lineWidth = 1;
            } else if (xScale > 0.13) {
                ctx.lineWidth = -8.17 * xScale * xScale + 5.28 * xScale + 0.1513;
            } else if (xScale > 0.03) {
                ctx.lineWidth = 10 * xScale * xScale + 1.9 * xScale + 0.284;
            }
            ctx.strokeStyle = self._cs['zoomline'];
            ctx.beginPath();
            ctx.moveTo(0, self._yDataFormat(self._globalDiff, self._zoomIndex) * yScale);
            if (xScale < 0.03) {    //large data should be drawn faster
                j = Math.ceil(1 / (xScale * 33));
                for (i = 1; i < self._dataLength; i += j) {
                    ctx.lineTo(i * xScale, self._yDataFormat(self._globalDiff + i, self._zoomIndex) * yScale);
                }
            } else {
                for (i = 1; i < self._dataLength; i++) {
                    ctx.lineTo(i * xScale, self._yDataFormat(self._globalDiff + i, self._zoomIndex) * yScale);
                }
            }
            i--;
            ctx.stroke();
            ctx.closePath();
            ctx.restore();
        }
        if (self._zoomPanelVisible) {
            var zoomOverlay;
            if (self._reInit._zoomPanel) {     //initiate zoom overlay
                self._zoomOverlay = self._getCanvas('zoomoverlay');
                self._zoomOverlay.style.left = (self._target.offsetLeft + jsplot['sett']['margin']) + 'px';
                self._zoomOverlay.tabIndex = 1;
                self._zoomOverlay.style.cursor = 'col-resize';
                ctx = self._zoomOverlay._ctx;
                ctx.globalAlpha = 0.2;
            } else {
                self._clear(self._zoomOverlay);
            }
            zoomOverlay = self._zoomOverlay;
            //set zoom overlay's size and top (resize can change top)
            if (self._reInit._resize) {
                zoomOverlay.style.top = (self._target.offsetTop + self._topPanelHeight + self._plotHeight + self._xLabelHeight + jsplot['sett']['margin'] * 2) + 'px';
                self._setSize(zoomOverlay, self._plotWidth, self._zoomPlotHeight);
            }
            if ((self._reInit._resize) || (self._reInit._data)) {
                zoomOverlay._xScaleRec = self._dataLength / self._plotWidth;
                zoomOverlay._xScale = xScale;
            }
            if (self._reInit._zoomPanel) {    //mouse click events for zooming
                ctx.fillStyle = self._cs['zoomslider'];
                zoomOverlay._plot = self;
                /**
                 * Fires in case of mousedown on zoom overlay panel.
                 *  Starts zooming.
                 * @event
                 */
                if (zoomOverlay.addEventListener) {
                    zoomOverlay.addEventListener('mousedown', _zoomOverlayMousedown, false);
                } else if (zoomOverlay.attachEvent) {
                    zoomOverlay.attachEvent('onmousedown', _zoomOverlayMousedown);
                }
                /**
                 * Fires in case of mousemove on zoom overlay panel.
                 *  If zooming is on, redraws the main panel (and zoom overlay).
                 *  Otherwise changes mouse cursor pointer style if needed.
                 * @event
                 */
                if (zoomOverlay.addEventListener) {
                    zoomOverlay.addEventListener('mousemove', _zoomOverlayMousemove, false);
                } else if (zoomOverlay.attachEvent) {
                    zoomOverlay.attachEvent('onmousemove', _zoomOverlayMousemove);
                }
                /**
                 * Stops zoom action.
                 * @event
                 */
                zoomOverlay._stopAction = function(event) {
                    _stopMouseAction(zoomOverlay);
                    var relPos = Math.max(Math.min(_canvasOffsetX(event.clientX, zoomOverlay), self._plotWidth - 1), 0);
                    if (relPos === 0) {
                        self._zoomEnd = self._globalDiff;
                    } else if (relPos === self._plotWidth - 1) {
                        self._zoomEnd = self._globalDiff + self._dataLength;
                    } else {
                        self._zoomEnd = self._globalDiff + Math.round(relPos * zoomOverlay._xScaleRec);
                    }
                    self._doplot(true);
                }
                /**
                 * Fires in case of mouseup on zoom overlay panel.
                 *  Ends zooming.
                 * @event
                 */
                if (zoomOverlay.addEventListener) {
                    zoomOverlay.addEventListener('mouseup', _zoomOverlayMouseup, false);
                } else if (zoomOverlay.attachEvent) {
                    zoomOverlay.attachEvent('onmouseup', _zoomOverlayMouseup);
                }
                /**
                 * fires in case of mouseout on zoom overlay panel
                 *  zoom until the sides
                 * @event
                 */
                if (zoomOverlay.addEventListener) {
                    zoomOverlay.addEventListener('mouseout', _zoomOverlayMouseout, false);
                } else if (zoomOverlay.attachEvent) {
                    zoomOverlay.attachEvent('onmouseout', _zoomOverlayMouseout);
                }
                /**
                 * Fires in case of keyup on zoom overlay panel.
                 *  If zoom is on, Ctrl+z makes undo for zooming.
                 * @event
                 */
                if (zoomOverlay.addEventListener) {
                    zoomOverlay.addEventListener('keyup', _zoomOverlayKeyup, false);
                } else if (zoomOverlay.attachEvent) {
                    zoomOverlay.attachEvent('onkeyup', _zoomOverlayKeyup);
                }
            }
        }

        if (self._reInit._all) {
            /**
             * Fires in case of mouseup in the whole document to make sure that current action has been finished.
             *  If panning is on, redraws main plot (and zoom overlay).
             * @event
             */
            if (document.addEventListener) {
                document.addEventListener('mouseup', _documentMouseup, false);
            } else if (document.attachEvent) {
                document.attachEvent('onmouseup', _documentMouseup);
            }
            /**
             * Fires in case of keyup in the whole document.
             *  Does not do anything while a zoom/pan action
             *  Zooms in for Ctrl+* and zooms out for Ctrl+/.
             * @event
             */
            if (self._target.addEventListener) {
                self._target.addEventListener('mouseover', function() {
                    jsplot._mouseOverTargets.push(self);
                });
                self._target.addEventListener('mouseout', function() {
                    jsplot._mouseOverTargets.splice(jsplot._mouseOverTargets.indexOf(self), 1);
                });
            } else if (self._target.attachEvent) {
                self._target.attachEvent('mouseover', function() {
                    jsplot._mouseOverTargets.push(self);
                });
                self._target.attachEvent('mouseout', function() {
                    jsplot._mouseOverTargets.splice(jsplot._mouseOverTargets.indexOf(self), 1);
                });
            }
        }
        self['rendered'] = true;
        self._debug('_initPlotCanvas done');
        return self;
    },

    /**
     * Clears a given panel.
     *  Low level plot function, should not be used directly.
     * @param {Canvas} panel the panel we need to clear
     * @param {Color|undefined} bgd the panel needs to have this color instead of current background
     */
    _clear: function(panel, bgd) {
        var self = this;
        panel._ctx.clearRect(0, 0, panel.width, panel.height);
        var fillStyle = panel._ctx.fillStyle;
        if (bgd) {
            panel._ctx.fillStyle = bgd;
        } else {
            panel._ctx.fillStyle = self._cs['background'];
        }
        panel._ctx.fillRect(0, 0, panel.width, panel.height);
        panel._ctx.fillStyle = fillStyle;
        self._debug('Panel ' + panel._name + ' cleared ' + panel.height + 'x' + panel.width);
    },

    /**
     * Plots the data on the main panel and refreshes zoom state.
     *  Low level plot function, should not be used directly.
     * @param {Boolean|Integer|undefined} param
     *  if undefined: called after init or panel resize or key/scroll zoom
     *  if true: called from a zoompanel action
     *  if false: undo last action
     *  if Integer: pan, the value is the current xmax
     * @returns {Array} the x limits of the currently visible data
     */
    _doplot: function(param) {
        var self = this;
        self._debug('_doplot: param = ' + param);
        var source = self._source;
        var buffer = self._mainPanels[self._drawingPanel];    //start drawing to buffered image
        var yData = source._yData;
        var ctx = buffer._ctx;
        var xmin = self._xmin;
        var xmax = self._xmax;
        var candles = self._candles;
        var i, j, k;

        if (param === true) {   //zoom
            if (self._zoomEnd < self._zoomBegin) {    //move cursor to left
                if (self._zoomBegin - self._zoomEnd < self._minCandles) {  //current distance is smaller than min
                    if (self._zoomBegin < self._minCandles) { //left space is too small
                        self._zoomEnd = self._zoomBegin + self._minCandles;    //move to right instead
                    } else {    //the left space is enough
                        self._zoomEnd = self._zoomBegin - self._minCandles;    //expand zoom to minimal size to left
                    }
                } else if (self._zoomBegin - self._zoomEnd > self._maxCandles) {   //current distance is bigger than max
                    self._zoomEnd = self._zoomBegin - self._maxCandles;    //reduce zoom to maximal size
                }
            } else {    //move cursor to right
                if (self._zoomEnd - self._zoomBegin < self._minCandles) {  //current distance is smaller than min
                    if (self._zoomBegin + self._minCandles > self._dataLength + self._globalDiff) { //right space is too small
                        self._zoomEnd = self._zoomBegin - self._minCandles;    //move to left instead
                    } else {    //the left space is enpugh
                        self._zoomEnd = self._zoomBegin + self._minCandles;    //expand zoom to minimal size to right
                    }
                } else if (self._zoomEnd - self._zoomBegin > self._maxCandles) {   //current distance is bigger than max
                    self._zoomEnd = self._zoomBegin + self._maxCandles;    //reduce zoom to maximal size
                }
            }
            if (self._zoomBegin < self._zoomEnd) {    //set current view to these values
                xmin = self._zoomBegin;
                xmax = self._zoomEnd;
            } else {
                xmin = self._zoomEnd;
                xmax = self._zoomBegin;
            }
            candles = xmax - xmin;
            self._xmin = xmin;
            self._xmax = xmax;
            self._candles = candles;
            self._candleWidth = self._plotWidth / candles;
        } else if (typeof(param) === 'number') {    //pan or key zoom
            xmax = Math.min(param, self._dataLength + self._globalDiff);
            xmin = xmax - self._candles;
            if (xmin < self._globalDiff) {
                xmin = self._globalDiff;
                xmax = xmin + self._candles;
            }
        } else if (param === false) {   //undo
            candles = xmax - xmin;
            self._candles = candles;
        }
        if ((xmin === self._lastXmin) && (xmax === self._lastXmax) && (param !== undefined)) {
            return [xmin, xmax];     //the view stays the same
        }

        // set Y range based on current X range and the data between
        var d_ = self._minmax2d(yData.slice(xmin, xmax));
        var ymin = d_[0];
        var ymax = d_[1];
        var padding = (ymax - ymin) * 0.05;
        if (padding === 0) {
            if (self._padding) {
                padding = self._padding;
            } else {
                d_ = self._minmax2d(yData.slice(self._globalDiff, self._globalDiff + self._dataLength));
                padding = (d_[1] - d_[0]) * 0.05;
                padding = padding || 1;
            }
        }
        var distribRadius = Math.min(self._candleWidth * jsplot['sett']['distribRadiusMult'], jsplot['sett']['maxDistribRadius']);
        var currScale = (ymax - ymin) / (self._plotHeight - 2 * distribRadius);
        if (((source._type === 'distrib') || ((source._type === 'line') && (xmax - xmin === 1))) && (distribRadius * currScale > padding)) {
            padding = distribRadius * currScale;
        }
        self._padding = padding;
        ymin -= padding; // little margin below
        ymax += padding; // little margin above
        self._ymin = ymin;
        self._ymax = ymax;
        if (self._zoomPanelVisible) {   //refresh zoom overlay
            i = Math.max(20, self._dataLength * 0.0005);
            if (self._last_globalDiff === undefined) {    //in case of global sources other plots can change _globalDiff, but we need the last one
                self._zoomOverlay._ctx.clearRect((self._lastXmin - self._globalDiff - i) * self._zoomOverlay._xScale, 0, (self._lastXmax - self._lastXmin + 2 * i) * self._zoomOverlay._xScale, self._zoomPanelHeight);
            } else {
                self._zoomOverlay._ctx.clearRect((self._lastXmin - self._last_globalDiff - i) * self._zoomOverlay._xScale, 0, (self._lastXmax - self._lastXmin + 2 * i) * self._zoomOverlay._xScale, self._zoomPanelHeight);
            }
            self._zoomOverlay._ctx.fillRect((xmin - self._globalDiff) * self._zoomOverlay._xScale, 0, (xmax - xmin - 1) * self._zoomOverlay._xScale, self._zoomPlotHeight);
            self._last_globalDiff = self._globalDiff;
        }

        ctx.strokeStyle = self._cs['stroke'];
        ctx.strokeRect(0, 0, self._plotWidth, self._plotHeight);

        var csize = Math.min(self._candleWidth * 0.7, self._candleMax);
        var candleLeft = (self._candleWidth - csize) * 0.5; // left of first (and every) candle
        var csizeHalf = csize * 0.5;
        self._distribCenter = self._candleWidth * 0.5;

        if (ymax === ymin) {    //all zero size candles with no padding
            ymax++;
        }
        var scale = self._plotHeight / (ymax - ymin); // scale: how much a unit takes on the plot
        self._scale = scale;

        //y grid
        var ystop = (self._gridLengthY / scale).toPrecision(1);
        if (ystop === 0) {
            self._error(self._prepError('dataError'));
            return [0, 0];
        }
        if (ystop.indexOf('e') === -1) {    //convert to exponential
            ystop = String(parseFloat(ystop).toExponential());
        }

        i = parseInt(ystop[0]);
        j = parseInt(ystop.substr(2));
        if (i > 5) {                        //round down to 5
            ystop = '5' + ystop.substr(1);
        } else if (i > 1) {
            ystop = '1' + ystop.substr(1);
        }
        ystop = parseFloat(ystop);
        var ystopHalf = ystop * 0.5;
        ctx.fillStyle = self._cs['label'];
        _setFont(ctx, jsplot['sett']['fontSizeY']);
        ctx.textAlign = 'left';
        ctx.lineWidth = 0.4;
        var mod = ymin % ystop;
        if (mod < 0) {
            mod += ystop;
        }
        var size = String(ystop).length;
        i = ymin + ystop - (mod ? mod : ystop);
        var size2;
        if (ystop % 1) {
            size--;
            size2 = ctx.measureText(i.toPrecision(size)).width;
        } else {
            var maxNum = i + Math.floor((ymax - i) / ystop) * ystop;
            size2 = Math.max(ctx.measureText(String(roundFloat(maxNum, size))).width, ctx.measureText(String(roundFloat(i, size))).width);
            size = Math.max(size, String(maxNum).length - size + 1);
        }
        if (size2 > self._yLabelWidth) {
            self._yLabelWidth = size2;
            self._reInit._rightWidth = true;
            self._info('new y label width: ' + self._yLabelWidth);
            self['plot']();
            return;
        }
        var str;
        for (; i < ymax; i += ystop) {
            var y1 = self._plotHeight - (i - ymin) * scale;
            if (y1 < 15) {
                continue;
            }
            _drawline(ctx, 0, y1, self._plotWidth, y1, self._cs['stroke']);
            if ((i < ystopHalf) && (i > -ystopHalf)) {
                str = '0';
            } else {
                str = String(roundFloat(i, size));
            }
            ctx.fillText(str, self._plotWidth + jsplot['sett']['margin'], y1);
        }

        //x grid
        var xstop = Math.ceil((xmax - xmin) * self._gridLengthX / self._plotWidth);
        ctx.fillStyle = self._cs['label'];
        ctx.textAlign = 'center';
        ctx.lineWidth = 0.4;
        var currXData, xlo, xline;
        _setFont(ctx, jsplot['sett']['fontSizeX']);
        for (i = xmin + xstop - ((xmin % xstop) ? (xmin % xstop) : xstop); i < xmax; i += xstop) {
            xlo = candleLeft + (i - xmin) * self._candleWidth;
            if (xlo < self._gridLengthXHalf) {
                continue;
            }
            xline = xlo + csizeHalf;
            _drawline(ctx, xline, 0, xline, self._plotHeight, self._cs['stroke']);
            ctx.save();
            ctx.translate(xline, self._plotHeight + self._xTop);
            if (self._rotateX) {
                ctx.rotate(self._rotateX);    //rotate 15 degrees
            }
            currXData = self._xDataFormat(i);
            if (source._xType === 'date') {
                ctx.fillText(tsToDate(currXData), 0, 0);
                ctx.fillText(tsToTime(currXData), 0, 12);
            } else {
                ctx.fillText(currXData, 0, 0);
            }
            ctx.restore();
        }

        ctx.lineWidth = csize * 0.14;
        if ((source._type === 'distrib') || (source._type === 'bar')) {
            var zero = self._plotHeight - (0 - ymin) * scale;
            _drawline(ctx, 0, zero, self._plotWidth, zero, self._cs['stroke']);
        }
        ctx.lineWidth = csize * 0.14;

        var yop, yhi, ylo, ycl, yright, yleft, t, currentY;
        if (source._type === 'line') {
            yleft = [];
            for (j = 0; j < source._dataCount; j++) {
                yleft[j] = self._plotHeight - (self._yDataFormat(xmin, j) - ymin) * scale;
            }
            if (xmax - xmin === 1) {
                for (j = 0; j < source._dataCount; j++) {
                    ctx.fillStyle = self._lines[j];
                    ctx.beginPath();
                    ctx.arc(self._distribCenter, yleft[j], distribRadius, 0, Math.PI * 2, true);
                    ctx.closePath();
                    ctx.fill();
                }
            }
        }
        for (i = xmin; i < xmax; i++) {     //chart draw
            currentY = self._yDataFormat(i);
            if (source._type === 'candle') {
                yop = self._plotHeight - (currentY[0] - ymin) * scale;
                yhi = self._plotHeight - (currentY[1] - ymin) * scale;
                ylo = self._plotHeight - (currentY[2] - ymin) * scale;
                ycl = self._plotHeight - (currentY[3] - ymin) * scale;
                xlo = candleLeft + (i - xmin) * self._candleWidth;
                xline = xlo + csizeHalf;

                if (yop < ycl) {
                    ctx.fillStyle = self._cs['rcandle'];
                } else {
                    ctx.fillStyle = self._cs['gcandle'];
                    t = ycl; ycl = yop; yop = t;    //swap
                }
                if (yop === ycl) {
                    ycl++;
                }
                ctx.fillRect(xlo, yop, csize, ycl - yop);
                _drawline(ctx, xline, yhi, xline, ylo, ctx.fillStyle);
            } else if (source._type === 'bar') {
                for (j = 0; j < source._dataCount; j++) {
                    yhi = self._plotHeight - (currentY[j] - ymin) * scale;
                    ylo = self._plotHeight - (0 - ymin) * scale;
                    if (currentY[j] < 0) {
                        ctx.fillStyle = self._cs['rcandle'];
                        t = yhi; yhi = ylo; ylo = t;    //swap
                    } else {
                        ctx.fillStyle = self._cs['gcandle'];
                    }
                    if (yhi === ylo) {
                        yhi++;
                    }
                    ctx.fillRect(candleLeft + (i - xmin) * self._candleWidth, ylo, csize, yhi - ylo);
                }
            } else if (source._type === 'distrib') {
                for (j = 0; j < source._dataCount; j++) {
                    if (currentY[j] < 0) {
                        ctx.fillStyle = self._cs['rcandle'];
                    } else {
                        ctx.fillStyle = self._cs['gcandle'];
                    }
                    ctx.beginPath();
                    ctx.arc(self._distribCenter + (i - xmin) * self._candleWidth, self._plotHeight - (currentY[j] - ymin) * scale, distribRadius, 0, Math.PI * 2, true);
                    ctx.closePath();
                    ctx.fill();
                }
            } else if (source._type === 'line') {
                xlo = candleLeft + (i - xmin) * self._candleWidth + csizeHalf;
                if (i === xmax - 1) {
                    break;
                }
                ctx.lineWidth = 1.5;
                for (j = 0; j < source._dataCount; j++) {
                    yright = self._plotHeight - (self._yDataFormat(i + 1, j) - ymin) * scale;
                    _drawline(ctx, xlo, yleft[j], xlo + self._candleWidth, yright, self._lines[j]);
                    yleft[j] = yright;
                }
            }/* else if (source._type === 2) {         // OHLC
                _drawline(ctx,xline, yhi, xline, ylo, ctx.fillStyle, 2);
                _drawline(ctx, xlo, yop, xline, yop, ctx.fillStyle, 2);
                _drawline(ctx, xline, ycl, xlo + csize, ycl, ctx.fillStyle, 2);
            } else {
                if (i - xmin > 0) {     // skip first line
                    _drawline(ctx, prevxy[0], prevxy[1], xline, ycl, self._cs['stroke'], 3);
                }
                prevxy = [xline, ycl];
            }*/
        }

        var extraData, currentExtra, currentExtraData;
        for (i in self._extras) {
            currentExtra = self._extras[i];
            if (typeof(currentExtra) === 'function') {
                continue;
            }
            extraData = self._yExtra[i];
            switch (currentExtra['type']) {
                //triangles to trade events (buy: up, sell: down, close: inverse of the open one) and line between them
                case 'tradeTriangle':
                    self._triangleWidthHalf = Math.min(Math.max(self._candleWidth * 3, jsplot['sett']['triangleMin']), self._candleMax) * 0.5;
                    var lineStart, lineEnd, lineGradient;
                    ctx.fillStyle = self._cs['triangle'];
                    ctx.miterLimit = 5;
                    ctx.lineWidth = 0.8;
                    //the old tradeTriangle drawer algorithm
//                    for (j in extraData) {
//                        currentExtraData = extraData[j];
//                        if ((typeof(currentExtraData) === 'function') || (currentExtraData[0]._time >= xmax) || ((currentExtraData.length > 1) && (currentExtraData[1]._time < xmin)) || ((currentExtraData.length === 1) && (currentExtraData[0]._time < xmin))) {
//                            continue;   //skip the ones that are not on the screen at all
//                        }               //but the lines must be there even if none of the ends are visible
//                        if (self._drawTriangle(xmin, xmax, ymin, scale, currentExtraData[0])) { //triangle drawn
//                            lineStart = [currentExtraData[0]._time, currentExtraData[0].price];
//                            lineGradient = Number.NaN;
//                        } else {
//                            lineGradient = (currentExtraData[1].price - currentExtraData[0].price) / (currentExtraData[1]._time - currentExtraData[0]._time);
//                            lineStart = [xmin, currentExtraData[0].price + (xmin - currentExtraData[0]._time) * lineGradient];
//                            if (lineStart[1] < ymin) {
//                                lineStart = [(ymin - currentExtraData[0].price) / lineGradient + currentExtraData[0]._time, ymin];
//                            } else if (lineStart[1] > ymax) {
//                                lineStart = [(ymax - currentExtraData[0].price) / lineGradient + currentExtraData[0]._time, ymax];
//                            }
//                        }
//                        if (currentExtraData.length === 1) {
//                            lineEnd = [Number.NaN];
//                        } else if (self._drawTriangle(xmin, xmax, ymin, scale, currentExtraData[1])) {
//                            lineEnd = [currentExtraData[1]._time, currentExtraData[1].price];
//                        } else {
//                            if (isNaN(lineGradient)) {
//                                lineGradient = (currentExtraData[1].price - currentExtraData[0].price) / (currentExtraData[1]._time - currentExtraData[0]._time);
//                            }
//                            lineEnd = [xmax - 1, currentExtraData[1].price - (currentExtraData[1]._time - (xmax - 1)) * lineGradient];
//                            if (lineEnd[1] < ymin) {
//                                lineEnd = [(ymin - currentExtraData[1].price) / lineGradient + currentExtraData[1]._time, ymin];
//                            } else if (lineEnd[1] > ymax) {
//                                lineEnd = [(ymax - currentExtraData[1].price) / lineGradient + currentExtraData[1]._time, ymax];
//                            }
//                        }
//                        if (!isNaN(lineEnd[0])) {
//                            lineStart = [self._distribCenter + (lineStart[0] - xmin) * self._candleWidth, self._plotHeight - (lineStart[1] - ymin) * scale];
//                            lineEnd = [self._distribCenter + (lineEnd[0] - xmin) * self._candleWidth, self._plotHeight - (lineEnd[1] - ymin) * scale];
//                            _dashline(ctx, lineStart[0], lineStart[1], lineEnd[0], lineEnd[1], currentExtraData[2] < 0 ? self._cs['rcandle'] : self._cs['gcandle']);
//                        }
//                    }

                    var h, m, startOnScreen;
                    var l = -1;
                    var closeList = currentExtra._closeList;
                    var openList = currentExtra._openList;
                    if (xmin + xmax > currentExtra._middleIndex) { //right side
                        h = closeList.length;
                        while (h - l > 1) {
                            m = h + l >> 1;
                            if ((extraData[closeList[m]][1]) && (extraData[closeList[m]][1]._time < xmin)) {
                                l = m;
                            } else {
                                h = m;
                            }
                        }
                        for (j = h; j < closeList.length; j++) {
                            currentExtraData = extraData[closeList[j]];
                            if (currentExtraData[0]._time >= xmax) {
                                continue;   //skip the ones that are not on the screen at all
                            }               //but the lines must be there even if none of the ends are visible
                            startOnScreen = self._drawTriangle(xmin, xmax, ymin, scale, currentExtraData[0]);
                            if (!currentExtraData[1]) {
                                continue;
                            }
                            if (startOnScreen) {
                                lineStart = [currentExtraData[0]._time, currentExtraData[0]._price];
                                lineGradient = Number.NaN;
                            } else {
                                lineGradient = (currentExtraData[1]._price - currentExtraData[0]._price) / (currentExtraData[1]._time - currentExtraData[0]._time);
                                lineStart = [xmin, currentExtraData[0]._price + (xmin - currentExtraData[0]._time) * lineGradient];
                                if (lineStart[1] < ymin) {
                                    lineStart = [(ymin - currentExtraData[0]._price) / lineGradient + currentExtraData[0]._time, ymin];
                                    if (lineStart[0] > xmax - 1) {
                                        continue;
                                    }
                                } else if (lineStart[1] > ymax) {
                                    lineStart = [(ymax - currentExtraData[0]._price) / lineGradient + currentExtraData[0]._time, ymax];
                                    if (lineStart[0] > xmax - 1) {
                                        continue;
                                    }
                                }
                            }

                            if (self._drawTriangle(xmin, xmax, ymin, scale, currentExtraData[1])) {
                                lineEnd = [currentExtraData[1]._time, currentExtraData[1]._price];
                            } else {
                                if (isNaN(lineGradient)) {
                                    lineGradient = (currentExtraData[1]._price - currentExtraData[0]._price) / (currentExtraData[1]._time - currentExtraData[0]._time);
                                }
                                lineEnd = [xmax - 1, currentExtraData[1]._price - (currentExtraData[1]._time - (xmax - 1)) * lineGradient];
                                if (lineEnd[1] < ymin) {
                                    lineEnd = [(ymin - currentExtraData[1]._price) / lineGradient + currentExtraData[1]._time, ymin];
                                } else if (lineEnd[1] > ymax) {
                                    lineEnd = [(ymax - currentExtraData[1]._price) / lineGradient + currentExtraData[1]._time, ymax];
                                }
                            }
                            lineStart = [self._distribCenter + (lineStart[0] - xmin) * self._candleWidth, self._plotHeight - (lineStart[1] - ymin) * scale];
                            lineEnd = [self._distribCenter + (lineEnd[0] - xmin) * self._candleWidth, self._plotHeight - (lineEnd[1] - ymin) * scale];
                            _dashline(ctx, lineStart[0], lineStart[1], lineEnd[0], lineEnd[1], currentExtraData[2] < 0 ? self._cs['rcandle'] : self._cs['gcandle']);
                        }
                    } else {                                                 //left side
                        h = openList.length;
                        while (h - l > 1) {
                            m = h + l >> 1;
                            if (extraData[openList[m]][0]._time >= xmax) {
                                l = m;
                            } else {
                                h = m;
                            }
                        }
                        for (j = h; j < openList.length; j++) {
                            currentExtraData = extraData[openList[j]];
                            if ((currentExtraData[1]) && (currentExtraData[1]._time < xmin)) {
                                continue;   //skip the ones that are not on the screen at all
                            }               //but the lines must be there even if none of the ends are visible
                            startOnScreen = self._drawTriangle(xmin, xmax, ymin, scale, currentExtraData[0]);
                            if (!currentExtraData[1]) {
                                continue;
                            }
                            if (startOnScreen) {
                                lineStart = [currentExtraData[0]._time, currentExtraData[0]._price];
                                lineGradient = Number.NaN;
                            } else {
                                lineGradient = (currentExtraData[1]._price - currentExtraData[0]._price) / (currentExtraData[1]._time - currentExtraData[0]._time);
                                lineStart = [xmin, currentExtraData[0]._price + (xmin - currentExtraData[0]._time) * lineGradient];
                                if (lineStart[1] < ymin) {
                                    lineStart = [(ymin - currentExtraData[0]._price) / lineGradient + currentExtraData[0]._time, ymin];
                                    if (lineStart[0] > xmax) {
                                        continue;
                                    }
                                } else if (lineStart[1] > ymax) {
                                    lineStart = [(ymax - currentExtraData[0]._price) / lineGradient + currentExtraData[0]._time, ymax];
                                    if (lineStart[0] > xmax) {
                                        continue;
                                    }
                                }
                            }

                            if (self._drawTriangle(xmin, xmax, ymin, scale, currentExtraData[1])) {
                                lineEnd = [currentExtraData[1]._time, currentExtraData[1]._price];
                            } else {
                                if (isNaN(lineGradient)) {
                                    lineGradient = (currentExtraData[1]._price - currentExtraData[0]._price) / (currentExtraData[1]._time - currentExtraData[0]._time);
                                }
                                lineEnd = [xmax - 1, currentExtraData[1]._price - (currentExtraData[1]._time - (xmax - 1)) * lineGradient];
                                if (lineEnd[1] < ymin) {
                                    lineEnd = [(ymin - currentExtraData[1]._price) / lineGradient + currentExtraData[1]._time, ymin];
                                } else if (lineEnd[1] > ymax) {
                                    lineEnd = [(ymax - currentExtraData[1]._price) / lineGradient + currentExtraData[1]._time, ymax];
                                }
                            }
                            lineStart = [self._distribCenter + (lineStart[0] - xmin) * self._candleWidth, self._plotHeight - (lineStart[1] - ymin) * scale];
                            lineEnd = [self._distribCenter + (lineEnd[0] - xmin) * self._candleWidth, self._plotHeight - (lineEnd[1] - ymin) * scale];
                            _dashline(ctx, lineStart[0], lineStart[1], lineEnd[0], lineEnd[1], currentExtraData[2] < 0 ? self._cs['rcandle'] : self._cs['gcandle']);
                        }
                    }
                    break;
                case 'peakThrough':
                    ctx.fillStyle = self._cs['peakThrough'];
                    var peakthroughHeight = padding * scale * 0.15;
                    var peakthroughLeft = -self._candleWidth * 0.25;
                    var peakthroughWidth = self._candleWidth * 1.5;
                    for (j in extraData) {
                        currentExtraData = extraData[j];
                        if ((typeof(currentExtraData) === 'function') || (currentExtraData._time >= xmax) || (currentExtraData._time < xmin)) {
                            continue;   //skip the ones that are not on the screen at all
                        }               //but the lines must be there even if none of the ends are visible
                        xlo = (currentExtraData._time - xmin) * self._candleWidth + peakthroughLeft;
                        currentY = self._yDataFormat(currentExtraData._time);
                        if (currentExtraData._position === 0) {  //peak
                            ylo = self._plotHeight - (currentY[1] - ymin) * scale - peakthroughHeight;
                            yhi = ylo - peakthroughHeight;
                        } else {
                            yhi = self._plotHeight - (currentY[2] - ymin) * scale + peakthroughHeight;
                            ylo = yhi + peakthroughHeight;
                        }
                        ctx.fillRect(xlo, ylo, peakthroughWidth, yhi - ylo);
                    }
                    break;
                case 'line':
                    yleft = self._plotHeight - (extraData[xmin] - ymin) * scale;
                    for (j = xmin; j < xmax - 1; j++) {     //chart draw
                        if ((extraData[j] === undefined) || (extraData[j] === null)) {
                            yleft = self._plotHeight - (extraData[j + 1] - ymin) * scale;
                            continue;
                        }
                        xlo = candleLeft + (j - xmin) * self._candleWidth + csizeHalf;
                        ctx.lineWidth = 1.5;
                        yright = self._plotHeight - (extraData[j + 1] - ymin) * scale;
                        _drawline(ctx, xlo, yleft, xlo + self._candleWidth, yright, self._extralines[i]);
                        yleft = yright;
                    }
                    break;
                case 'lines':
                    yleft = new Array(currentExtra['count']);
                    for (k = 0; k < currentExtra['count']; k++) {
                        j = xmin;
                        while (((!extraData[j]) || (!extraData[j][k] === undefined) || (!extraData[j][k] === null)) && (j < xmax - 1)) {
                            j++;
                        }
                        if (j < xmax - 1) {
                            yleft[k] = self._plotHeight - (extraData[j][k] - ymin) * scale;
                        }
                    }
                    for (j = xmin; j < xmax - 1; j++) {     //chart draw
                        if ((extraData[j] === undefined) || (extraData[j] === null)) {
                            continue;
                        }
                        for (k = 0; k < currentExtra['count']; k++) {
                            if ((extraData[j][k] === undefined) || (extraData[j][k] === null)) {
                                yleft[k] = self._plotHeight - (extraData[j + 1][k] - ymin) * scale;
                                continue;
                            }
                            xlo = candleLeft + (j - xmin) * self._candleWidth + csizeHalf;
                            ctx.lineWidth = 1.5;
                            yright = self._plotHeight - (extraData[j + 1][k] - ymin) * scale;
                            _drawline(ctx, xlo, yleft[k], xlo + self._candleWidth, yright, self._extralines[i][k]);
                            yleft[k] = yright;
                        }
                    }
                    break;
            }
        }

        if (self['visible']) {
            buffer.style.visibility = 'visible';        //move buffer to foreground
        }
        self._drawingPanel = 1 - self._drawingPanel;
        buffer = self._mainPanels[self._drawingPanel];
        buffer.style.visibility = 'hidden';
        self._clear(buffer);
        self._lastXmin = xmin;
        self._lastXmax = xmax;
        return [xmin, xmax];
    },

    /**
     * Draws a trade triangle.
     *  Low level plot function, should not be used directly.
     * @param {Integer} xmin the current screen's minimum x value
     * @param {Integer} xmax the current screen's maximum x value
     * @param {Integer} ymin the current screen's minimum y value
     * @param {Float} scale the current screen's y scale
     * @param {Integer} data the details of the triangle
     * @returns {Boolean} if the triangle is visible on the plot
     */
    _drawTriangle: function(xmin, xmax, ymin, scale, data) {
        var self = this;
        if ((data._time >= xmax) || (data._time < xmin)) {    //the triangle is not visible
            return 0;
        }
        var x0 = self._distribCenter + (data._time - xmin) * self._candleWidth;    //the x level of the middle of the triangle
        var y0 = self._plotHeight - (data._price - ymin) * scale;         //the y level of the middle of the triangle
        var arrowDir = data._direction ? -self._triangleWidthHalf : self._triangleWidthHalf;
        var ctx = self._mainPanels[self._drawingPanel]._ctx;
        ctx.beginPath();
        ctx.moveTo(x0, y0);
        ctx.lineTo(x0 + self._triangleWidthHalf, y0 + arrowDir);
        ctx.lineTo(x0 - self._triangleWidthHalf, y0 + arrowDir);
        ctx.lineTo(x0, y0);
        ctx.closePath();
        ctx.fill();
        return 1;
    },

    /**
     * Zooms in or out.
     *  Low level plot function, should not be used directly.
     * @param {Number} unit how many candle appears on the screen (<0 means zoom in, >0 means zoom out)
     * @param {Float|undefined} zoomCenter 0<zoomCenter<1, the center of the zoomin/zoomout on the screen
     */
    _doZoom: function(unit, zoomCenter) {
        var self = this;
        if (zoomCenter === undefined) {
            zoomCenter = 0.5;
        }
        var newCandles = Math.min(Math.max(self._candles + unit * 2, self._minCandles), self._maxCandles);
        if (newCandles === self._candles) {
            return;
        }
        var oldCandles = self._candles;
        self._candles = newCandles;
        self._candleWidth = self._plotWidth / self._candles;
        var d_ = self._doplot(Math.min(self._xmax + Math.round(zoomCenter * (self._candles - oldCandles)), self._globalDiff + self._dataLength - 1));
        self._xmin = d_[0];
        self._xmax = d_[1];
    },

    /**
     * Creates a canvas. Creates drawing context if needed as well.
     *  Low level plot function, should not be used directly.
     * @param {String} name used only in case of an error
     * @param {Boolean|undefined} createContext if the canvas needs a context
     * @param {Boolean|undefined} fontSize deafult font size of the new canvas
     * @param {Boolean|undefined} bold if the text need to be bold
     * @returns {Canvas|Boolean} the new canvas, or false on error
     */
    _getCanvas: function(name, createContext, fontSize, bold) {
        var self = this;
        name = self['id'] + ':' + name;
        self._debug('_getCanvas: name = ' + name + ' createContext = ' + createContext + ' fontSize = ' + fontSize + ' bold = ' + bold);
        if (createContext === undefined) {
            createContext = true;
        }
        var canvas;
        canvas = document.createElement('canvas');
        if (!canvas) {
            alert('Cannot Initialize ' + name);
            return false;
        }
        canvas._name = name;
        self._target.appendChild(canvas);
        canvas.style.position = 'absolute';
        if ((typeof(G_vmlCanvasManager) != 'undefined') && (G_vmlCanvasManager.initElement)) {
            G_vmlCanvasManager.initElement(canvas);
        }
        canvas._ctx = canvas.getContext('2d');
        if (!createContext) {
            canvas._ctx.globalAlpha = 0;
        }
        if (fontSize) {
            _setFont(canvas._ctx, fontSize, bold);
        }
        if (!self['visible']) {
            canvas.style.visibility = 'hidden';
        }
        self._debug(name + ' canvas created');
        return canvas;
    },

    /**
     * Removes the dom element of a panel (destroy).
     *  Low level plot function, should not be used directly.
     * @param {Canvas} panel the canvas to destroy
     */
    _removeCanvas: function(panel) {
        var self = this;
        self._target.removeChild(panel);
        self._debug('_removeCanvas ' + panel._name + ' removed');
    },

    /**
     * Sets the size of a canvas panel, and restores properties. Clears panel if it has a context.
     *  Low level plot function, should not be used directly.
     * @param {Canvas} panel to set size
     * @param {Integer|Boolean|null} width new width, or ! if the width must stay the same
     * @param {Integer|Boolean|null|undefined} height new height, or ! if the height must stay the same
     * @param {Color|undefined} bgd the panel needs to have this color instead of current background on clear
     */
    _setSize: function(panel, width, height, bgd) {
        var self = this;
        self._debug('_setSize ' + panel._name + ': width = ' + width + ' height = ' + height + ' bgd = ' + bgd);
        var ctx = panel._ctx;
        var font, globalAlpha;
        if (ctx) {
            globalAlpha = ctx.globalAlpha;
            if (ctx.font) {
                font = ctx.font;
            }
        }
        if (width) {
            panel.width = width;
        }
        if (height) {
            panel.height = height;
        }
        if (font) {
            ctx.font = font;
        }
        if (ctx) {
            ctx.globalAlpha = globalAlpha;
            self._clear(panel, bgd);
        }
    },

    /**
     * Stores data.
     *  Low level plot function, should not be used directly.
     * @param {Array} data the data to store
     *  must be ordered if the first value is the index
     *  must be a complete block of data without skipped indexes that could contain data in the future
     * @param {Integer|undefined} downloadIndex to which space interval the data goes in case of global source
     * @param {Boolean|undefined} finished if this is the last item of the current download sequence
     * @returns {Boolean} success
     */
    _read: function(data, downloadIndex, finished) {
        var self = this;
        var source = self._source;
        var inputCount, index, datai, intervals, spaceIndex, currDownload, interval, minIndex, maxIndex, i, j, plot, firstDataIndex;
        if (typeof(data[0]) === 'object') {
            inputCount = data[0].length;
            firstDataIndex = source._indexedInput ? 1 : 0;
            if ((source._dataCount !== undefined) && (source._dataCount !== inputCount - firstDataIndex)) {
                return self._warn(self._prepWarn('wrongInputNumber'));
            }
            source._dataCount = inputCount - firstDataIndex;
            if (source._dataCount === 0) {
                return self._error(self._prepError('wrongInputNumber'));
            }
//            } else if (typeof(data[0]) === 'undefined') {
//                inputCount = 0;
//                source._dataCount = 0;
        } else if (typeof(data[0]) !== 'undefined') {
            inputCount = 1;
            source._dataCount = 1;
        }
        if (!self._ignoreDeny) {
            if (self._memLimitReached()) {
                if (jsplot['sett']['memDeny']) {
                    return self._error(self._prepError('memoryDeny'));
                } else if (jsplot['sett']['memoryAction'] === 'abort') {
                    return self._error(self._prepError('memoryAbort'));
                } else if (jsplot['sett']['memoryAction'] === 'ignore') {
                    self._ignoreDeny = true;
                    jsplot['sett']['memDeny'] = true;
                    self._error(self._prepError('memoryLimitReached'));
                }
            } else {
                jsplot['sett']['memDeny'] = false;
            }
        }
        //no space info means that the data is not global or this is the only data in it
        if ((downloadIndex !== undefined) && (self._downloadSequence.length)) {  //space info is available
            intervals = source._intervals;   //the current intervals as [data0From, data0To=space0From, space0To=data1From, ... space(n)To=data(n+1From, data(n+1)To]
            spaceIndex = source._spaceIndex; //the indexes where the data is cut with spaces
            currDownload = self._downloadSequence[downloadIndex];    //current download details
            interval = currDownload[0]      //the interval to insert the data
            minIndex = currDownload[1];     //the minimum index
            maxIndex = currDownload[2];     //the maximum index
            if (minIndex === true) {    //the begining of the interval
                minIndex = intervals[interval * 2 - 1];
            }
            if (maxIndex === true) {    //the end of the interval
                maxIndex = intervals[interval * 2];
            }
        }
        var count = 0;
        var cursorCopy = self._insertCursor;
        for (i in data) {
            if (typeof(data[i]) === 'function') {
                continue;
            }
            index = source._indexedInput ? data[i][0] : i;   //xData
            if (typeof(data[i]) === 'object') {             //yData
                datai = data[i].slice(firstDataIndex, inputCount);
            } else {
                datai = [data[i]];
            }
            if (source._xType === 'date') {
                index = getDateTs(index);
            }
            if (currDownload && ((index < minIndex) || (index >= maxIndex))) {
                self._warn(self._prepWarn('loaded data may be wrong: currDownload: ' + currDownload + '; minIndex(' + minIndex + '); index(' + index + '); maxIndex(' + maxIndex + ')'));
                continue;
            }
            for (j in datai) {
                if (typeof(datai[j]) === 'function') {
                    continue;
                }
                if (!isFinite(datai[j])) {
                    if (isNaN(datai[j])) {
                        if (jsplot['sett']['errorOnNaN']) {
                            self._error(self._prepError('nanInput', [i, j]));
                        } else {
                            datai[j] = 0;
                        }
                    } else {
                        if (jsplot['sett']['replaceInf'] === -1) {
                            datai[j] = 0;
                        } else {
                            datai[j] = parseFloat(((datai[j] > 0) ? '1' : '-1') + 'e' + jsplot['sett']['replaceInf']);
                        }
                    }
                } else if (jsplot['sett']['roundInput']) {
                    datai[j] = parseFloat(datai[j].toPrecision(jsplot['sett']['roundInput']));
                }
            }
            if (self._insertCursor === false) {  //the data goes to the end
                source._xData.push(index);
                source._yData.push(datai);
            } else {                            //the data goes to the current cursor
                source._xData.splice(self._insertCursor, 0, index);
                source._yData.splice(self._insertCursor, 0, datai);
                self._insertCursor++;
            }
            count++;
        }

        self._dataLength += count;

        if (self._insertCursor !== false) {   //repair diff pointers
            for (i = 0; i < source._plots.length; i++) {
                if (source._plots[i] !== self) {
                    source._plots[i]._change_globalDiff(count, cursorCopy);
                }
            }
        }
        if (currDownload) {
            if (currDownload[1] === true) { //if the begining is the same as the interval's
                i = interval - 1;   //it is moved as well
            } else {        //otherwise
                i = interval;   //only from the next one
            }
            for (; i < spaceIndex.length; i++) {
                self._debug('spaceIndex[', i, '] += ' + count);
                spaceIndex[i] += count; //increase space indexes with the inserted length
            }
            if (finished) {     //all needed data has been filled to this space interval
                if (currDownload[2] === true) { //the space interval's end is here
                    if (spaceIndex.length > interval) { //there is a next space index
                        self._insertCursor = spaceIndex[interval];   //insert cursor jumps there
                    } else {    //no more space indexes
                        self._insertCursor = false;  //insert cursor goes to the end
                    }
                }
                if (currDownload[1] === true) {
                    if (currDownload[2] === true) { //the data fills the whole interval
                        self._debug('spaceIndex.splice(' + interval - 1 + ', ' + 1 + ')');
                        spaceIndex.splice(interval - 1, 1);             //remove space index
                        self._debug('intervals.splice(' + interval * 2 - 1 + ', ' + 2 + ')');
                        intervals.splice(interval * 2 - 1, 2);      //remove space interval
                        for (i = parseInt(downloadIndex) + 1; i < self._downloadSequence.length; i++) {
                            self._downloadSequence[i][0]--;  //repair the next download sequence intervals
                        }
                    } else {    //only the smaller end has changed
                        self._debug('intervals[' + (interval * 2 - 1) + '] = ' + currDownload[2]);
                        intervals[interval * 2 - 1] = currDownload[2];
                    }
                } else {
                    if (currDownload[2] === true) { //only the greater end has changed
                        self._debug('intervals[' + interval * 2 + '] = ' + currDownload[1]);
                        intervals[interval * 2] = currDownload[1];
                    } else {    //insert new data interval
                        self._debug('intervals.splice(' + interval * 2 + ', ' + 0 + ', ' + minIndex + ', ' + maxIndex + ')');
                        intervals.splice(interval * 2, 0, minIndex, maxIndex);
                        if (intervals.length > 3) { //insert space interval as well
                            if (interval <= spaceIndex.length) {  //this is not going to the end
                                self._debug('spaceIndex.splice(' + interval + ', ' + 0 + ', ' + (self._globalDiff + self._dataLength) + ')');
                                spaceIndex.splice(interval, 0, self._globalDiff + self._dataLength);  //space interval will be after data
                            } else {                //space interval will be before this one
                                self._debug('spaceIndex.push(' + self._globalDiff + ')');
                                spaceIndex.push(self._globalDiff);
                            }
                        }
                        for (i = parseInt(downloadIndex) + 1; i < self._downloadSequence.length; i++) {
                            self._downloadSequence[i][0]++;  //repair the next download sequence intervals
                        }
                    }
                }
                self._debug('_dataLength += ' + currDownload[3]);
                self._dataLength += currDownload[3];
                if (self._downloadSequence.length === parseInt(downloadIndex) + 1) {   //download done
                    self._onReadPartDone(inputCount);
                    self._onReadDone();
                    self._ignoreDeny = false;
                    return true;
                }
            } else {    //not finished, but has to store the status informations of the current state
                var endIndex = index + source._indexStep;
                if (currDownload[1] === true) { //change the end of the data interval to the current end
                    self._debug('intervals[' + (interval * 2 - 1) + '] = ' + endIndex);
                    intervals[interval * 2 - 1] = endIndex;
                } else {
                    self._debug('intervals.splice(' + (interval * 2) + ', ' + 0 + ', ' + minIndex + ', ' + endIndex + ')');
                    intervals.splice(interval * 2, 0, minIndex, endIndex);
                    if (intervals.length > 3) { //insert space interval as well
                        if (interval <= spaceIndex.length) {  //this is not going to the end
                            self._debug('spaceIndex.splice(' + interval + ', ' + 0 + ', ' + (self._globalDiff + self._dataLength) + ')');
                            spaceIndex.splice(interval, 0, self._globalDiff + self._dataLength);  //space interval will be after data
                        } else {                //space interval will be before this one
                            self._debug('spaceIndex.push(' + self._globalDiff + ')');
                            spaceIndex.push(self._globalDiff);
                        }
                    }
                    currDownload[1] = true; //now the new interval exists, so I can fix the beginning
                    for (i = parseInt(downloadIndex); i < self._downloadSequence.length; i++) {
                        self._downloadSequence[i][0]++;  //repair the next download sequence intervals
                    }
                }
            }
        }

        self._onReadPartDone(inputCount);
        return true;
    },

    _change_globalDiff: function(diff, cursor) {
        var self = this;
        self._debug('_change_globalDiff id: ' + self['id'] + ' current _globalDiff:' + self._globalDiff + ' addition: ' + diff + ' cursor:' + cursor);
        if (self._globalDiff < cursor) {    //if the other global diff >= this one's
            return;
        }
        self._globalDiff += diff;    //increase them with the inserted length
        self._xmin += diff;
        self._xmax += diff;
        var currentExtra, extraData, extraDataj, i, j;
        for (i in self._extras) {    //calculate indicators given by function, generated before plotting
            currentExtra = self._extras[i];
            if (typeof(currentExtra) === 'function') {
                continue;
            }
            switch (currentExtra['type']) {    //TODO: other types?
                case 'tradeTriangle':
                    extraData = self._tipExtra[i];
                    var newExtraData = {};
                    for (j in extraData) {
                        extraDataj = extraData[j];
                        if (typeof(extraDataj) === 'function') {
                            continue;
                        }
                        newExtraData[parseInt(j) + diff] = extraDataj;
                    }
                    self._tipExtra[i] = newExtraData;
                    extraData = self._yExtra[i];
                    for (j in extraData) {
                        extraDataj = extraData[j];
                        if (typeof(extraData[j]) === 'function') {
                            continue;
                        }
                        extraDataj[0]._time += diff;
                        if (1 in extraDataj) {
                            extraDataj[1]._time += diff;
                        }
                    }
                    if (typeof(currentExtra._middleIndex) === 'number') {
                        currentExtra._middleIndex += diff;
                    }
                    break;
                case 'peakThrough':
                    break;
                case 'line':
                    break;
                case 'lines':
                    break;
            }
        }
    },

    /**
     * Called when an amount of data has been read.
     */
    _onReadPartDone: function(inputCount) {
        var self = this;
        if (self._labels === undefined) {
            self._labels = [];
            for (var i = 0; i < inputCount; i++) {
                self._labels.push(i);
            }
        }
        // if (self._globalId) {
            // self._dataLength = source._xData.length;
        // } else {
            // self._dataLength += count;
        // }

        self._debug('_onReadPartDone');
    },

    /**
     * Called when data read is done.
     */
    _onReadDone: function() {
        var self = this;
        self._reInit._data = true;
        if (self._dataReadyNeeded) {
            self['setDataReady'](true);
        }
        if (self._mainOverlay) {
            self._mainOverlay.tabIndex = 0;
        }
        if ((self._zoomOverlay) && (self._zoomPanelVisible)) {
            self._zoomOverlay.style.cursor = 'col-resize';
            self._zoomOverlay.tabIndex = 1;
        }
        self['cleared'] = false;
        self['plot']();
        self._debug('_onReadDone');
    },

    /**
     * Formats x-axis variable to adequate value and readable format.
     *  Low level plot function, should not be used directly.
     * @param {Integer} index the global index of the label
     * @returns {Integer|String|Date} the label value
     */
    _xDataFormat: function(index) {
        var self = this;
        var source = self._source;
        var value = source._xData[index];
        if (source._xType === 'custom') {
            return source._labelPattern.replace('{0}', parseInt(value) + source._labelStartIndex);
        } else {
            return value;
        }
    },

    /**
     * Formats y-axis variable to adequate value and readable format.
     *  Low level plot function, should not be used directly.
     * @param {Integer} index the global index of the data
     * @param {Integer|undefined} subVal if set: index of the needed variable in the array
     * @returns {Number} the needed value
     */
    _yDataFormat: function(index, subVal) {
        var self = this;
        var value = self._source._yData[index];
        if (subVal !== undefined) {
            value = value[subVal];
        }
        return value;
    },

    /**
     * Counts global min and max on a 1d data with an optional sub array index.
     *  Low level plot function, should not be used directly.
     *  If the source type is bar or distribution, zero is added to the comparision as well.
     * @param {Array} data the given data
     * @param {Integer|undefined} subIndex data is a 2-dimensional array, and we want the minimum
     *  and maximum of every nth value in the sub-arrays.
     * @returns {Array} an array with the minimum and maximum value in it
     */
    _minmax1d: function(data, subIndex) {
        var self = this;
        subIndex = (subIndex !== undefined) ? subIndex : false;
        var max = -Infinity;
        var min = Infinity;
        var datai = 0;
       
        for (var i in data) {
            if (typeof(data[i]) === 'function') {
                continue;
            }
            if (subIndex !== false) {
                datai = data[i][subIndex];
            } else {
                datai = data[i];
            }
            if (datai >= max)  max = datai;
            if (datai < min) min = datai;
        }
        if ((self._source._type === 'bar') || (self._source._type === 'distrib')) {
            min = Math.min(min, 0);
            max = Math.max(max, 0);
        }
        return [min, max];
    },

    /**
     * Counts global min and max on a 2d data
     *  Low level plot function, should not be used directly.
     *  If the source type is bar or distribution, zero is added to the comparision as well.
     * @param {Array} data the given data
     * @param {Array|Integer|undefined} subIndex the indexes we need from the sub-arrays, undefined for all values
     * @returns {Array} an array with the minimum and maximum value in it
     */
    _minmax2d: function(data, subIndex) {
        var self = this;
        var max = -Infinity;
        var min = Infinity;
        var i, j, k;
        if (subIndex === undefined) {
            subIndex = [];
            for (j in data[0]) {
                if (typeof(data[0][j]) === 'function') {
                    continue;
                }
                subIndex[subIndex.length] = j;
            }
        } else if (typeof(subIndex) !== 'object') {
            subIndex = [subIndex];
        }
        for (i in data) {
            if (typeof(data[i]) === 'function') {
                continue;
            }
            for (j in subIndex) {
                k = data[i][subIndex[j]];
                if (k > max)  max = k;
                if (k < min) min = k;
            }
        }
        if ((self._source._type === 'bar') || (self._source._type === 'distrib')) {
            min = Math.min(min, 0);
            max = Math.max(max, 0);
        }
        return [min, max];
    },

    /**
     * Converts indicator data to a adequate format and stores it's tooltips.
     *  Low level plot function, should not be used directly.
     *  If the plot is not ready to plot, indicator data is stored in this format, it will be processed on the first plot() call.
     * @param {Array|String} data the indicator data
     * @param {Integer} index the index of this indicator data
     * @returns {Array} the converted indicator data
     */
    _readExtra: function(data, index) {
        var self = this;
        var source = self._source;
        var currentExtra = self._extras[index];
        if (!self['isReadyToPlot'](true)) {
            currentExtra._loadLater = true;
            return data;
        }
        var converted = [];
        var i, j, k, tip, datai, openTime, closeTime, currentTip;
        switch (currentExtra['type']) {
            case 'tradeTriangle':
                tip = self._tipExtra[index] ? self._tipExtra[index] : {};
                var direction, openReady, closeReady;
                for (i = 0; i < data.length; i++) {
                    datai = data[i];
                    openReady = 0;
                    closeReady = 0;
                    datai['openprice'] = parseFloat(datai['openprice'].toPrecision(jsplot['sett']['roundInput']));
                    openTime = datai['opentime'];
                    closeTime = datai['closetime'];
                    if (source._xType === 'date') {
                        openTime = getDateTs(openTime);
                        if (closeTime) {
                            closeTime = getDateTs(closeTime);
                        }
                    }
                    j = _searchLowExt(source._xData, openTime, source._indexStep);
                    if (j < 0) {
                        self._warn(self._prepWarn('Something is strange: open candle not found in data: ' + openTime));
                        continue;
                    }
                    if (!tip[j]) {
                        tip[j] = [[], []];
                    }
                    //3 * 8 + (9 + 6) * 2 = 54 byte
                    tip[j][0].push(datai['openprice']);
                    tip[j][1].push({_direction: datai['direction'], _amount: datai['amount']});
                    if (closeTime) {
                        k = _searchLowExt(source._xData, closeTime, source._indexStep);
                    } else {
                        k = -3;
                    }
                    direction = datai['direction'] === jsplot['messages']['tradeTriangleSELL'] ? 1 : 0;
                    if (k < 0) {
                        converted.push([{_time: j, _price: datai['openprice'], _direction: direction}]);
                    } else {
                        if (!tip[k]) {
                            tip[k] = [[], []];
                        }
                        //4 * 8 + (9 + 6 + 6) * 2 = 74 byte
                        datai['closeprice'] = parseFloat(datai['closeprice'].toPrecision(jsplot['sett']['roundInput']));
                        if ((!('profit' in datai)) && (currentExtra['autoProfit'])) {
                            datai['profit'] = (datai['closeprice'] - datai['openprice']) * datai['amount'] * (direction === 1 ? -1 : 1);
                        }
                        tip[k][0].push(datai['closeprice']);
                        currentTip = {_direction: datai['type'], _amount: datai['amount']};
                        tip[k][1].push(currentTip);
                        if (datai['profit']) {
                            currentTip._profit = parseFloat(datai['profit'].toPrecision(jsplot['sett']['roundInput']));
                        }
                        //7 * 8 + (4 + 5 + 9) * 2 * 2) = 128 byte
                        converted.push([{_time: j, _price: datai['openprice'], _direction: direction}, {_time: k, _price: datai['closeprice'], _direction: !direction}, datai['profit']]);
                        //sum = 256 byte + array pointers ~ 270 byte
                    }
                }
                self._tipExtra[index] = tip;
                self._extras[index]._middleIndex = null;
                break;
            case 'peakThrough':
                converted = self._readPeakThrough(data, index);
                break;
            case 'line':
            case 'lines':
            default:
                converted = data;
        }
        if (currentExtra._loadLater) {
            delete currentExtra._loadLater;
        }
        return converted;
    },

    _generateExtra: function(fromIndex, toIndex, currentExtra) {
        var self = this;
        self._debug('_generateExtra');
        var i;
        var data = currentExtra['func'](self._source._xData.slice(fromIndex, toIndex), self._source._yData.slice(fromIndex, toIndex), currentExtra['parameters']);
        switch (currentExtra['type']) {
            case 'peakThrough':
                data = self._readPeakThrough(data, i);
                break;
            case 'line':
                if ((!currentExtra['name'].length) && (data[0].length)) {
                    self._error(self._prepError('Indicator data contains array of arrays but indicator has only one name.'));
                }
                break;
        }
        return data;
    },

    _readPeakThrough: function(data, index) {
        var self = this;
        var source = self._source;
        var i, j, k, datai, time;
        var converted = [];
        for (i in data) {
            datai = data[i];
            if (typeof(datai) === 'function') {
                continue;
            }
            time = datai['time'];
            if (source._xType === 'date') {
                time = getDateTs(time);
            }
            j = _searchArray(source._xData, time);
            if (j === -1) {
                self._error(self._prepError('Something is strange: open candle not found in data', datai));
                j = _searchLow(source._xData, time) + 1;
            }
            converted.push({_time: j, _position: datai['position']});
        }
        return converted;
    },

    /**
     * Loads and converts data to adequate format.
     *  Low level plot function, should not be used directly.
     * @param {Array|String} data the data to read
     * @param {Boolean|undefined} sort if the data needs to be sorted by it's first value.
     * @param {Boolean|undefined} ignoreWarnings if warnings are ignored to show
     * @returns {Array} the converted indicator data
     */
    _loadData: function(data, sort) {
        var self = this;
        if (sort === undefined) {
            sort = !self._source._sorted;
        }
        // We expect the data to be in the following form
        // {data: [[ts,o,h,l,c,v], [ts,o,h,l,c,v],....]}

        if (data['data']) {
            if (isArray(data['data'])) {
                if (!data['data'].length) {// empty array
                    return 'noDataAvailable';
                }
                var d = data['data'];
            }
        } else if (isArray(data)) {
            if (!data.length) {// empty array
                return 'noDataAvailable';
            }
            var d = data;
        }

        // neither data or data.data was an array, try to parse json or else give up
        if (!d) {
            var d = parseJSON(data);
            if (typeof(d) === 'string') {
                return d;
            } else {
                if (d['data'])  {
                    if (isArray(d['data'])) {
                        d = d['data'];
                    } else {
                        return 'notArray';
                    }
                } else if (!isArray(d)) {
                        return 'notArray';
                }
            }
        }

        if (sort) {
            if (self._source._xType === 'custom') {
                d.sort(_sortNumber);
            } else {
                d.sort(_sortDate);
            }
        }
        return d;
    },

    /**
     * Returns requested GUI error.
     */
    _decodeErrorString: function(input, params) {
        var self = this;
        if (input in jsplot['errors']) {
            return _replaceStringParams(jsplot['errors'][input], params);
        }
        var output = input;
        if (params) {
            output = output + ' with params: ' + stringify(params);
        }
        return _replaceStringParams(jsplot['errors']['notfound'], ['error', output]);
    },

    /**
     * Shows information to the user.
     *  Low level plot function, should not be used directly.
     *  Messages are always written on the plot (like 'Loading...x%').
     *  Errors and warnings can be sent to a user defined function by the 'errorHandler' global option.
     *  If the information is plotted, and it does not fit because of it's width, it is truncated to lines
     * @param {String|undefined} msg the message; it can be a key in jsplot.messages, jsplot.errors
     * @param {Boolean|undefined} isError in case of the 'msg' is set, it states that it is an error
     * @param {Boolean|undefined} ignoreWarnings if warnings are ignored to show
     * @param {Array|undefined} params optional string format params for the message
     * @returns {Boolean} !success
     */
    _showMsg: function(msg, params) {
        var self = this;
        if (!msg) {
            return true;
        }
        if (msg in jsplot['messages']) {
            msg = _replaceStringParams(jsplot['messages'][msg], params);
        } else {
            var errorMsg = msg;
            if (params) {
                errorMsg = errorMsg + ' with params: ' + params;
            }
            self._error(self._prepError('notfound', ['message', errorMsg]));
            console.trace();
            return true;
        }
        var fontSize, line, i, param;
        fontSize = jsplot['sett']['fontSizeMessage'];
        var info = self._infoPanel;
        info.style.left = self._target.offsetLeft + 'px';
        info.style.top = self._target.offsetTop + 'px';
        var ctx = info._ctx;
        _setFont(ctx, fontSize);
        var width = ctx.measureText(msg).width;
        var lines = 1;
        var msgCopy = msg;
        msg = [];
        if (width > parseInt(self._target.style.width)) {    //the info does not fit in the canvas
            lines = Math.ceil(width / parseInt(self._target.style.width));  //wrap it to lines
            width = parseInt(self._target.style.width);
        }
        //this wrap does not care about word limits, cuts the string to +- equal length strings
        var charPerLine = Math.round(msgCopy.length / lines);
        for (i = 0; i < lines; i++) {
            msg.push(msgCopy.substr(i * charPerLine, charPerLine));
        }
        self._setSize(self._infoPanel, width, parseInt(lines) * (fontSize + jsplot['sett']['textLineBreak']));
        ctx.fillStyle = self._cs['label'];
        for (line in msg) {
            if (typeof(msg[line]) === 'function') {
                continue;
            }
            ctx.fillText(msg[line], 0, fontSize + parseInt(line) * (fontSize + jsplot['sett']['textLineBreak']));
        }
        if (self['visible']) {
            info.style.visibility = 'visible';
        }
        return false;
    },

    /**
     * If the memory limithas been reached.
     *  Low level plot function, should not be used directly.
     *  If the limit is reached, it tries to free up unused global sources
     *      ordered by the time since they can be freed up
     * @returns {Boolean} if the memory limithas been reached
     */
    _memLimitReached: function() {
        var self = this;
        if ((!jsplot['sett']['memoryLimit']) || (jsplot['sett']['memoryType']) || ((!(jsplot['sett']['memoryType'] & 2)) && (!self._source._globalId)) || ((!(jsplot['sett']['memoryType'] & 1)) && (self._source._globalId))) {
            return false;
        }
        var source, i, currentSize;
        var clearables = [];
        var size = 0;
        var staticMem = 50;
        if (jsplot['sett']['memoryType'] & 1) {    //globals
            for (i in jsplot._globalSources) {
                source = jsplot._globalSources[i];
                if (typeof(source) === 'function') {
                    continue;
                }
                if (source._yData.length) {//data length * (variable number + label) + static
                    currentSize = source._xData.length * (source._yData[0].length + 1) + staticMem;
                } else {
                    currentSize = staticMem;
                }
                if (!source._plots.length) {
                    clearables.push([source._destroyable, i, currentSize]);
                }
                size += currentSize;
            }
        }
        if (jsplot['sett']['memoryType'] & 2) {    //locals
            for (i in jsplot._localSources) {
                source = jsplot._localSources[i];
                if (typeof(source) === 'function') {
                    continue;
                }
                //            data length         * (variable number + label   ) + static
                currentSize = source._xData.length * (source._yData[0].length + 1) + staticMem;
                size += currentSize;
            }
        }
        size = size >> 7;  //Number and Date fields are 2^3 bytes in javascript, setting is for 2^10 bytes
        if ((size > jsplot['sett']['memoryLimit']) && (clearables.length)) {
            clearables.sort(_sortNumber);
            for (i in clearables) {
                source = clearables[i];
                if (typeof(source) === 'function') {
                    continue;
                }
                clearables.splice(i, 1);
                delete jsplot._globalSources[source[1]];
                size -= source[2];
                if (size <= jsplot['sett']['memoryLimit']) {
                    return false;
                }
            }
        }
        return size > jsplot['sett']['memoryLimit'];
    }
}

/** This is needed to give the object all the methods required. */
jsplot._fn._init.prototype = jsplot._fn;

/**
 * IE 6-7-8 is a pain in the ass... Took me about 2 days to make it work on that peace of shit.
 * And it's extremely slow, but I don't care anymore.
 */
function _fuckIEHackToGetSelf(event) {
    if ((event.target) && (event.target._plot)) {
        return event.target._plot;
    }
    return event.currentTarget._plot;
}

function _mainOverlayMousedown(event) {
    var self = _fuckIEHackToGetSelf(event);
    if ((!self['visible']) || (self['cleared'])) {
        return;
    }
    if (self._pannable) {
        var mainOverlay = self._mainOverlay;
        jsplot._actionPanel = mainOverlay;
        mainOverlay.style.cursor = 'move';
        mainOverlay._begin = _canvasOffsetX(event.clientX, mainOverlay);
        mainOverlay._xmin = self._xmin;  //save start params for undo
        mainOverlay._xmax = self._xmax;
        mainOverlay._newXmin = self._xmin;
        mainOverlay._newXmax = self._xmax;
    }
}

function _mainOverlayMouseup(event) {
    var self = _fuckIEHackToGetSelf(event);
    var mainOverlay = self._mainOverlay;
    if (jsplot._actionPanel === mainOverlay) {
        mainOverlay._stopAction(event);
        return false;
    }
}

function _mainOverlayKeyup(event) {
    var self = _fuckIEHackToGetSelf(event);
    if ((!self['visible']) || (self['cleared'])) {
        return;
    }
    var mainOverlay = self._mainOverlay;
    if (jsplot._actionPanel === mainOverlay) {
        if ((event.keyCode === 90) && (event.ctrlKey === true)) { //Ctrl+z
            _stopMouseAction(mainOverlay);
            self._xmin = mainOverlay._xmin; //restore values
            self._xmax = mainOverlay._xmax;
            self._doplot(false);
        }
        return false;
    }
}

function _mainOverlayMousemove(event) {
    var self = _fuckIEHackToGetSelf(event);
    if ((!self['visible']) || (self['cleared'])) {
        return;
    }
    var mainOverlay = self._mainOverlay;
    var info = self._infoPanel;
    if (jsplot._actionPanel === mainOverlay) {
        var d_ = self._doplot(self._xmax - Math.round((_canvasOffsetX(event.clientX, mainOverlay) - mainOverlay._begin) / self._candleWidth));
        mainOverlay._newXmin = d_[0];
        mainOverlay._newXmax = d_[1];
        return false;
    } else if (self._tippable) {
        var style = info.style;
        var xCoord = _canvasOffsetX(event.clientX, mainOverlay) + document.body.scrollLeft; //relative x coordinate to main overlay
        var yCoord = _canvasOffsetY(event.clientY, mainOverlay) + document.body.scrollTop;  //relative y coordinate to main overlay
        var relIndex = Math.round((xCoord - self._candleWidth * 0.5) / self._candleWidth);    //relative data index to main overlay
        var absIndex = Math.min(self._xmin + relIndex, self._globalDiff + self._dataLength);   //absolute data index
        var relYCursor = self._plotHeight - yCoord;   //relative y cursor pos
        var extraLines = [];
        var extraLimits = [];
        var extra, extraLine, i, j, currentExtra, currentTip, extraData;
        for (i in self._extras) {  //indicator data tooltips
            currentExtra = self._extras[i];
            currentTip = self._tipExtra[i];
            if ((typeof(currentExtra) === 'function') || ((!currentTip) && (currentExtra['type'] !== 'line') && (currentExtra['type'] !== 'lines'))) {
                continue;
            }
            extraData = self._yExtra[i][absIndex];
            switch (currentExtra['type']) {
                case 'tradeTriangle':
                    if (absIndex in currentTip) {
                        extra = currentTip[absIndex];
                        extraLimits.push.apply(extraLimits, extra[0]);
                        var str;
                        for (j in extraLimits) {
                            if (typeof(extraLimits[j]) === 'function') {
                                continue;
                            }
                            extraLine = extra[1][j];
                            str = extraLine._direction + ' ' + extraLine._amount.toString() + ' @ ' + extraLimits[j].toString();
                            if (extraLine._profit) {
                                str += ', ' + jsplot['messages']['tradeTriangleNetProfit'] + ': ' + extraLine._profit.toString();
                            }
                            extraLines.push(str);
                        }
                        break;
                    }
                    break;
                case 'peakThrough':
                    break;  //no tooltips for peak-through
                case 'line':
                    if (!extraData) {
                        break;
                    }
                    extraLines.push(currentExtra['name'] + ': ' + extraData.toString());
                    break;
                case 'lines':
                    if (!extraData) {
                        break;
                    }
                    for (j = 0; j < currentExtra['count']; j++) {
                        if (extraData[j]) {
                            extraLines.push(currentExtra['name'][j] + ': ' + extraData[j].toString());
                        }
                    }
                    break;
            }
        }
        var currYData = self._yDataFormat(absIndex);    //current y data value array
        if (!currYData) {
            style.visibility = 'hidden';    //the mouse cursor is not over a candle
            return;
        }
        var source = self._source;
        if ((source._type === 'distrib') || (source._type === 'bar')) {
            currYData = currYData.slice();
            currYData.push(0);
        }
        if ((source._dataCount > 1) || ((source._type === 'distrib') || (source._type === 'bar')) || (extraLimits.length > 0)) {
            var min = Math.min(currYData);
            var max = Math.max(currYData);
            if (extraLimits.length > 0) {
                min = Math.min(Math.min(extraLimits), min);
                max = Math.max(Math.max(extraLimits), max);
            }
            if ((relYCursor < (min - self._ymin) * self._scale - 5) || (relYCursor > (max - self._ymin) * self._scale + 5)) {
                style.visibility = 'hidden';    //the mouse cursor is not over a candle
                return;
            }
        } else {
            var relYValue = (currYData[0] - self._ymin) * self._scale //relative y data to main overlay
            if ((relYCursor < relYValue - 15) || (relYCursor > relYValue + 15)) {
                style.visibility = 'hidden';    //the mouse cursor is not over a data point
                return;
            }
        }
        var ctx = info._ctx;
        var currXData = self._xDataFormat(absIndex);
        var lines;
        if (source._xType === 'date') {
            lines = [tsToDate(currXData) + ' ' + tsToTime(currXData)];
        } else {
            lines = [currXData];
        }
        _setFont(ctx, jsplot['sett']['fontSizeTooltip']);
        var strLen = ctx.measureText(lines[0]).width;
        var xStrLen = strLen;
        for (i = 0; i < source._dataCount; i++) {   //measure strings to get the biggest length
            lines.push(self._labels[i] + ': ' + String(currYData[i]));
            strLen = Math.max(strLen, ctx.measureText(lines[i + 1]).width);
        }
        for (j in extraLines) {
            if (typeof(extraLines[j]) === 'function') {
                continue;
            }
            lines.push(extraLines[j]);
            i++;
            strLen = Math.max(strLen, ctx.measureText(lines[i]).width);
        }
        self._setSize(info, jsplot['sett']['margin'] * 2 + strLen, jsplot['sett']['margin'] * 2 + jsplot['sett']['fontSizeTooltip'] * lines.length + jsplot['sett']['textLineBreak'] * (lines.length - 1), self._cs['infoBgd']);
        if (relIndex < (self._xmax - self._xmin) * 0.5) { //check if tooltips goes left or right to the cursor
            style.left = (self._target.offsetLeft + xCoord + 25) + 'px';
        } else {
            style.left = (self._target.offsetLeft + xCoord - info.width) + 'px';
        }
        style.top = (self._target.offsetTop + self._topPanelHeight + Math.max(yCoord - info.height * 0.5, 0)) + 'px';
        _drawline(ctx, jsplot['sett']['margin'], jsplot['sett']['margin'] + jsplot['sett']['fontSizeTooltip'] + 2, jsplot['sett']['margin'] + xStrLen, jsplot['sett']['margin'] + jsplot['sett']['fontSizeTooltip'] + 2, self._cs['stroke'], 0.4);
        ctx.strokeStyle = self._cs['stroke'];
        ctx.strokeRect(0, 0, info.width, info.height);
        ctx.fillStyle = self._cs['label'];
        for (i = 0; i < lines.length; i++) {
            ctx.fillText(lines[i], jsplot['sett']['margin'], jsplot['sett']['margin'] + jsplot['sett']['fontSizeTooltip'] * (i + 1) + jsplot['sett']['textLineBreak'] * i);
        }
        style.visibility = 'visible';
        return false;
    }
}

function _mainOverlayMouseout(event) {
    var self = _fuckIEHackToGetSelf(event);
    if ((!self['visible']) || (self['cleared'])) {
        return;
    }
    self._infoPanel.style.visibility = 'hidden';
}

function _getZoomPos(self, relPos) {
    if (relPos === 0) {
        return self._globalDiff;
    }
    if (relPos === self._plotWidth - 1) {
        return self._globalDiff + self._dataLength;
    }
    return self._globalDiff + Math.round(relPos * self._zoomOverlay._xScaleRec);
}

function _zoomOverlayMousedown(event) {
    var self = _fuckIEHackToGetSelf(event);
    if ((!self['visible']) || (self['cleared'])) {
        return;
    }
    var zoomOverlay = self._zoomOverlay;
    var ctx = zoomOverlay._ctx;
    jsplot._actionPanel = zoomOverlay;
    zoomOverlay.style.cursor = 'move';
    ctx._xmin = self._xmin; //save last values to be able to redo
    ctx._xmax = self._xmax;
    ctx._candleWidth = self._candleWidth;
    var relPos = _canvasOffsetX(event.clientX, zoomOverlay);
    self._zoomBegin = _getZoomPos(self, relPos);
    self._zoomEnd = self._zoomBegin;
    self._doplot(true);
    return false;
}

function _zoomOverlayMousemove(event) {
    var self = _fuckIEHackToGetSelf(event);
    if ((!self['visible']) || (self['cleared'])) {
        return;
    }
    var zoomOverlay = self._zoomOverlay;
    var relPos = _canvasOffsetX(event.clientX, zoomOverlay);
    if (jsplot._actionPanel === zoomOverlay) {
        self._zoomEnd = _getZoomPos(self, relPos);
        self._doplot(true);
        return false;
    } else {
        if (relPos === 0) {
            zoomOverlay.style.cursor = 'e-resize';
        } else if (relPos === self._plotWidth - 1) {
            zoomOverlay.style.cursor = 'w-resize';
        } else {
            zoomOverlay.style.cursor = 'col-resize';
        }
        return false;
    }
}

function _zoomOverlayMouseup(event) {
    var self = _fuckIEHackToGetSelf(event);
    var zoomOverlay = self._zoomOverlay;
    if (jsplot._actionPanel === zoomOverlay) {
        zoomOverlay._stopAction(event);
        return false;
    }
}

function _zoomOverlayMouseout(event) {
    var self = _fuckIEHackToGetSelf(event);
    var zoomOverlay = self._zoomOverlay;
    if ((!self['visible']) || (self['cleared']) || (jsplot._actionPanel !== zoomOverlay)) {
        return;
    }
    var relPos = Math.max(Math.min(_canvasOffsetX(event.clientX, zoomOverlay), self._plotWidth - 1), 0);
    self._zoomEnd = _getZoomPos(self, relPos);
    self._doplot(true);
    return false;
}

function _zoomOverlayKeyup(event) {
    var self = _fuckIEHackToGetSelf(event);
    if ((!self['visible']) || (self['cleared'])) {
        return;
    }
    var zoomOverlay = self._zoomOverlay;
    if (jsplot._actionPanel === zoomOverlay) {
        if ((event.keyCode === 90) && (event.ctrlKey === true)) { //Ctrl+z
            _stopMouseAction(zoomOverlay);
            var ctx = zoomOverlay._ctx;
            self._xmin = ctx._xmin; //restore values
            self._xmax = ctx._xmax;
            self._candleWidth = ctx._candleWidth;
            self._doplot(false);
        }
        return false;
    }
}

function _documentMouseup(event) {
    if (!jsplot._actionPanel) {
        return;
    }
    jsplot._actionPanel._stopAction(event);
    return false;
}

function _targetKeypressed(event) {
    var i, self;
    var action = 0;
    var key = event.keyCode || event.charCode;
    if ((event.ctrlKey === false) && (event.shiftKey === false) && (event.altKey === false)) {
        if (key === 43) { //+
            action = 2;
        } else if (key === 45) { //-
            action = -2;
        }
    }
    if (!action) {
        return;
    }
    for (i in jsplot._mouseOverTargets) {
        self = jsplot._mouseOverTargets[i];
        if (typeof(self) === 'function') {
            continue;
        }
        if ((!self['visible']) || (self['cleared'])) {
            return;
        }
        if (jsplot._actionPanel) {
            return;
        }
        self._doZoom(action);
    }
    return false;
}

jsplot._mouseOverTargets = [];
if (document.addEventListener) {
    document.addEventListener('keypress', _targetKeypressed, false);
} else if (document.attachEvent) {
    document.attachEvent('onkeyup', _targetKeypressed);
}


/**
 * Stops current mouse action and restates mouse cursor.
 *  Low level helper function, should not be used directly.
 * @param {Canvas} panel the panel to restore mouse cursor and stop any action.
 */
function _stopMouseAction(panel) {
    panel.style.cursor = 'default';
    jsplot._actionPanel = false;
}

/**
 * Sets the context font.
 *  Low level helper function, should not be used directly.
 * @param {Context} ctx the canvas context
 * @param {Integer} size the font size
 * @param {Boolean|undefined} bold if the font needs to be bold
 */
function _setFont(ctx, size, bold) {
    ctx.font = (bold ? 'bold ' : '') + size + 'pt ' + jsplot['sett']['font'];
}

/**
 * Compare function sorting of numbers or arrays that has number as the first item.
 *  Low level helper function, should not be used directly.
 * @param {Number|Array} a the first number to compare, or an array and it's first item is the number
 * @param {Number|Array} b the second number to compare, or an array and it's first item is the number
 * @returns {Number} the difference
 */
function _sortNumber(a, b) {
    if (typeof(a) === 'object') {
        return a[0] - b[0];
    } else {
        return a - b;
    }
}

/**
 * Compare function sorting of dates or arrays that has date as the first item.
 *  Low level helper function, should not be used directly.
 * @param {Date|Array} a the first date to compare, or an array and it's first item is the date
 * @param {Date|Array} b the second date to compare, or an array and it's first item is the date
 * @returns {Date} the difference
 */
function _sortDate(a, b) {
    if (typeof(a) === 'object') {
        return getDateTs(a[0]) - getDateTs(b[0]);
    } else {
        return getDateTs(a) - getDateTs(b);
    }
}

/**
 * Binary search in a sorted array.
 *  Low level helper function, should not be used directly.
 *  Returns the index of the value if it is there, or -1 when not found.
 * @param {Array} o the ordered array
 * @param {Number} v the value to search for
 * @returns {Integer} the index, or -1 if the value does not exist in the array
 */
function _searchArray(o, v) {
    var h = o.length, l = -1, m;
    while (h - l > 1) {
        if (o[m = h + l >> 1] < v) {
            l = m;
        } else {
            h = m;
        }
    }
    return o[h] === v ? h : -1;
}

/**
 * Binary search for lower neighbour in a sorted array.
 *  Low level helper function, should not be used directly.
 *  Returns the index of the value if it is there, or the index of it's lower neighbour when not found.
 * @param {Array} o the ordered array
 * @param {Number} v the value to search for
 * @returns {Integer} the index, or the index of it's lower neighbour when not found
 */
function _searchLow(o, v) {
    var h = o.length, l = -1, m;
    while (h - l > 1) {
        if (o[m = h + l >> 1] <= v) {
            l = m;
        } else {
            h = m;
        }
    }
    return l;
}

/**
 * Extended Binary search in a sorted array.
 *  Low level helper function, should not be used directly.
 *  Returns the index of the value if it is there, or the index of it's lower neighbour when not found.
 *  Returns -2 if the value is smallert than the smallest item.
 *  Returns -3 if the value is bigger the biggest item + minimum step interval.
 * @param {Array} o the ordered array
 * @param {Number} v the value to search for
 * @param {Number} step the minimum step interval
 * @returns {Integer} the index, or the index of it's lower neighbour when not found
 */
function _searchLowExt(o, v, step) {
    var h = o.length, l = -1, m;
    if (v < o[0]) {
        return -2;
    }
    if (v >= o[h - 1] + step) {
        return -3;
    }
    while (h - l > 1) {
        if (o[m = h + l >> 1] <= v) {
            l = m;
        } else {
            h = m;
        }
    }
    return l;
}

/**
 * Extended Binary search in a sorted array.
 *  Low level helper function, should not be used directly.
 *  Returns the index of the value if it is there.
 *  Returns -2 if the value is smallert than the smallest item.
 *  Returns -3 if the value is bigger the biggest item + minimum step interval.
 *  Returns -1 when not found.
 * @param {Array} o the ordered array
 * @param {Number} v the value to search for
 * @param {Number} step the minimum step interval
 * @returns {Integer} the index, or the index of it's lower neighbour when not found
 */
function _searchArrayExt(o, v, step) {
    var h = o.length, l = -1, m;
    if (v < o[0]) {
        return -2;
    }
    if (v > o[h - 1] + step) {
        return -3;
    }
    while (h - l > 1) {
        if (o[m = h + l >> 1] < v) {
            l = m;
        } else {
            h = m;
        }
    }
    return h;
}

/**
 * The mouse wheel event handler for main overlay.
 *  Low level helper function, should not be used directly.
 *  Zooms in or out with 5 units centered to the current position of the mouse.
 * @param {Event} event the mouse event
 */
function _wheel(event) {
    var self = event.target._plot;
    if ((!self['visible']) || (self['cleared'])) {
        return;
    }
    var delta = 0;
    if (!event) // For IE.
        event = window.event;
    if (event.wheelDelta) { // IE/Opera.
        delta = event.wheelDelta / 120;
        if (window.opera) { // In Opera 9, delta differs in sign as compared to IE.
            delta = -delta;
        }
    } else if (event.detail) {  // Mozilla case.
        delta = -event.detail / 3;    // In Mozilla, sign of delta is different than in IE. Also, delta is multiple of 3.
    }
    if (delta) {    // If delta is nonzero, handle it. Basically, delta is now positive if wheel was scrolled up, and negative, if wheel was scrolled down.
        self._doZoom(-delta * 5, 1 - _canvasOffsetX(event.clientX, this) / self._plotWidth);
    }
    if (event.preventDefault) {
        event.preventDefault();
    }
    event.returnValue = false;
}

function _initTradeTriangle(currentExtra, extraData) {
    var openList = [];
    var closeList = [];
    currentExtra._openList = openList;
    currentExtra._closeList = closeList;
    if (!extraData.length) {
        return;
    }
    var i;
    var notClosed = [];
    var j = extraData.length;
    for (i = 0; i < j; i++) {
        openList.push(j - 1 - i);//trade open time indexes
        if (extraData[i][1]) {
            closeList.push(i);//trade close time indexes if the trade is closed
        } else {
            notClosed.push(i);
        }
    }
    closeList.sort(function(a, b) {//sort close times
        return extraData[a][1]._time - extraData[b][1]._time;
    });
    var closeListLength = closeList.length;
    openList.sort(function(a, b) {//sort open times reversed
        return extraData[b][0]._time - extraData[a][0]._time;
    });
    if (!closeListLength) {
        currentExtra._middleIndex = 9007199254740992;    //max int value
        return;
    }
    closeList.push.apply(closeList, notClosed);//add unclosed trades to the end
    j = 0;
    var xBegin = extraData[closeList[j]][1]._time;
    var xEnd = extraData[openList[j]][0]._time;
    var xBeginLast = xBegin;
    var xEndLast = xEnd;
    if (xBegin < xEnd) {
        while (true) {  //search for the index where the number of closed trades are
            if (j + 1 < closeListLength) {
                j++;        //equal to the number of unopened trades
                xBegin = extraData[closeList[j]][1]._time;
                if (xBegin >= xEnd) {
                    xBegin = extraData[closeList[j - 1]][1]._time;
                    break;
                }
            }
            xEnd = extraData[openList[j]][0]._time;
            if (xBegin >= xEnd) {
                xEnd = extraData[openList[j - 1]][0]._time;
                break;
            }
            if ((xBeginLast === xBegin) && (xEndLast === xEnd)) {
                break;
            }
            xBeginLast = xBegin;
            xEndLast = xEnd;
        }
    }
    currentExtra._middleIndex = xEnd + xBegin;   //*2
}

/**
 * Counts panel relative x coordinate.
 *  Low level helper function, should not be used directly.
 * @param {Integer} x absolute x coordinate of the mouse cursor
 * @param {Canvas} c panel
 * @returns {Integer} the requested offset
 */
function _canvasOffsetX(x, c) {
    var ox = 0;
    do {
        ox += c.offsetLeft;
        c = c.offsetParent;
    } while (c) ; // from quirksmode
    return  x - ox;
}

/**
 * Counts panel relative y coordinate.
 *  Low level helper function, should not be used directly.
 * @param {Integer} y absolute y coordinate of the mouse cursor
 * @param {Canvas} c panel
 * @returns {Integer} the requested offset
 */
function _canvasOffsetY(y, c) {
    var oy = 0;
    do {
        oy += c.offsetTop;
        c = c.offsetParent;
    } while (c) ; // from quirksmode
    return  y - oy;
}

/**
 * Draws a line with given parameters.
 *  Low level helper function, should not be used directly.
 * @param {Context} ctx the canvas context to draw to
 * @param {Integer} x1 line start x-coordinate
 * @param {Integer} y1 line start y-coordinate
 * @param {Integer} x2 line end x-coordinate
 * @param {Integer} y2 line end y-coordinate
 * @param {Color|undefined} color the line color, the current ctx.strokeStyle if not set
 * @param {Number|undefined} width the line width, the current ctx.lineWidth if not set
 */
function _drawline(ctx, x1, y1, x2, y2, color, width) {
    if (width) {
        ctx.lineWidth = width;
    }
    if (color) {
        ctx.strokeStyle = color;
    }
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
}

/**
 * Draws a dashed line with given parameters.
 *  Low level helper function, should not be used directly.
 * @param {Context} ctx the canvas context to draw to
 * @param {Integer} x1 line start x-coordinate
 * @param {Integer} y1 line start y-coordinate
 * @param {Integer} x2 line end x-coordinate
 * @param {Integer} y2 line end y-coordinate
 * @param {Color|undefined} color the line color, the current ctx.strokeStyle if not set
 * @param {Number|undefined} width the line width, the current ctx.lineWidth if not set
 * @param {Number|undefined} dash the lines' length, 5 if not set
 * @param {Number|undefined} space the spaces' length, 4 if not set
 */
function _dashline(ctx, x1, y1, x2, y2, color, width, dash, space) {
    if (((x1 === x2) && (y1 === y2))) {
        return;
    }
    if ((isNaN(x1)) || (isNaN(x2)) || (isNaN(y1)) || (isNaN(y2)) || (!isFinite(x1)) || (!isFinite(x2)) || (!isFinite(y1)) || (!isFinite(y2))) {
        alert(['dashline not appropriate input: x1, y1, x2, y2', x1, y1, x2, y2]);
        return;
    }
    if (width) {
        ctx.lineWidth = width;
    }
    dash = dash || 6.5;
    space = space || 4.5;
    ctx.strokeStyle = color || '#111111';
    var dx = x2 - x1;
    var dy = y2 - y1;
    var length = Math.sqrt(dx * dx + dy * dy);
    var lengthRec = 1 / length;
    var ux = dx * lengthRec;
    var uy = dy * lengthRec;
    var dashX = dash * ux;
    var dashY = dash * uy;
    var spaceX = space * ux;
    var spaceY = space * uy;
    var i = dash;
    var j = 0;
    var kx = x1 - spaceX;
    var ky = y1 - spaceY;
    ctx.beginPath();
    while (i < length) {
        kx += spaceX;
        ky += spaceY;
        ctx.moveTo(kx, ky);
        j = i;
        i += space + dash;
        kx += dashX;
        ky += dashY;
        ctx.lineTo(kx, ky);
    }
    j = i - j - space;
    if (j > 0) {
        kx += spaceX;
        ky += spaceY;
        ctx.moveTo(kx, ky);
        ctx.lineTo(x2, y2);
    }
    ctx.stroke();
    ctx.closePath();
}

window['stringify'] = function(inObj, seen, collection) {
    var delSeen = false;
    if (collection === undefined) {
        collection = [];
        delSeen = true;
        var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
        var string_length = 12;
        var seen = '';
        for (var i = 0; i < string_length; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            seen += chars.substring(rnum, rnum + 1);
        }
    }
    var i, inObji, stay;
    var result = ''
    if (isArray(inObj)) {
        if ((inObj.length > 0) && (inObj[inObj.length - 1] === seen)) {
            return '[array seen]'
        }
        result += '[';
        inObj.push(seen);
        collection.push(inObj);
        for (i = 0; i < inObj.length; i++) {
            inObji = inObj[i];
            if ((i === inObj.length - 1) && (inObji === seen)) {
                break;
            }
            result += stringify(inObji, seen, collection) + ', ';
        }
        if (result.length > 1) {
            result = result.substring(0, result.length - 2);
        }
        result += ']';
    } else if (typeof(inObj) === 'function') {
        result += 'function'
    } else if(typeof(inObj) === 'object'){
        try {
            if (seen in inObj) {
                return '{object seen}'
            }
        } catch (e) {
            return '{object unaccessable}'
        }
        result += '{';
        inObj[seen] = true;
        collection.push(inObj);
        stay = false;
        for (var i in inObj) {
            if (i === seen) {
                continue;
            }
            stay = true;
            try {
                result += i + ': ' + stringify(inObj[i], seen, collection) + ', ';
            } catch (e) {
                result += i + ': unaccessable, ';
            }
        }
        if (stay) {
            result = result.substring(0, result.length - 2);
        }
        result += '}';
    } else {
        result += String(inObj);
    }
    if (delSeen) {
        for (i = 0; i < collection.length; i++) {
            if (isArray(collection[i])) {
                collection[i].pop();
            } else {
                delete collection[i][seen];
            }
        }
    }

    return result;
}

function _replaceStringParams(input, params) {
    if (params) {
        var param;
        for (i in params) {
            param = params[i];
            if (typeof(param) === 'function') {
                continue;
            }
            input = input.replace('{' + i + '}', param);
        }
    }
    return input;
}

/**
 * Note: I changed error handling.
 * Following functions are used from jQuery
 *  The reason for doing this
 *      1. jquery does it properly
 *      2. Including jquery for three functions is kind of an overkill. So
 *          we'd keep including functions from jquery in here. If and when
 *          this becomes too big to be so, we'd just use jQuery library.
 *  Copyright 2010, John Resig (http://jquery.org/license)
 */
window['parseJSON'] = function(data) {
    if (typeof data !== 'string' || !data) {
        return 'noDataAvailable';
    }

    var rtrim = /^(\s|\u00A0)+|(\s|\u00A0)+$/g;    // Make sure leading/trailing whitespace is removed (IE can't handle it)
    data = data.replace(rtrim, '');

    // Make sure the incoming data is actual JSON
    // Logic borrowed from http://json.org/json2.js
    if (/^[\],:{}\s]*$/.test(data.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
        .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
        .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
        // Try to use the native JSON parser first
        return window.JSON && window.JSON.parse ? window.JSON.parse(data) : (new Function('return ' + data))();
    } else {
        return 'jsonConvert';
    }
}

window['isArray'] = function(a) {
    return Object.prototype.toString.call(a) === '[object Array]';
}

window['clone'] = function(obj) {
    if ((obj === null) || (typeof(obj) !== 'object')) {
        return obj;
    }

    if (obj instanceof Date) {
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    var i;
    if (isArray(obj)) {
        var copy = [];
        for (i = 0; i < obj.length; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    if (obj instanceof Object) {
        var copy = {};
        for (i in obj) {
            if (obj.hasOwnProperty(i)) {
                copy[i] = clone(obj[i]);
            }
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}

window['roundFloat'] = function(a, decimals) {
    return parseFloat(a.toPrecision(decimals));
}

window['hasProperty'] = function(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            return true;
        }
    }
    return false;
}

/**
 * Gets a date and interperts it as javascript Date object.
 *  Low level helper function, should not be used directly.
 *  Function below is not perfect. But as close to usable. Some Date formats like '12-Oct' fail.
 *      But yes, thats understandable.
 * @param {String} str the input string
 * @returns {Date|Integer} the interpreted Date or it's Unix timestamp
 */
window['getDateTs'] = function(str) {
    var d;
    d = new Date(str).getTime();
    if (!isNaN(d)) {
        return d;
    }
    str = str.replace(/-/g, ' '); //1 Jan 2010 works but 1-Jan-2010 doesn't
    d = new Date(str).getTime();
    if (!isNaN(d)) {
        return d;
    }
    // may be what we've is a time stamp.
    if ((d = parseInt(str)) > 100000) {
        // we are not handling something that's up on 1st Jan 1971, as yet.
        // assume it is a valid time stamp and just send it back.
       return d;
    } 
}

/**
 * Converts Date timestamp to date String in a needed format YYYY-MM-DD.
 *  Low level helper function, should not be used directly.
 * @param {Integer} ts date timestamp
 * @returns {String} the timestamp in YYYY-MM-DD format
 */
window['tsToDate'] = function(ts) {
    var d = new Date(ts);
    var dd = d.getDate();
    var mm = d.getMonth() + 1;

    return d.getFullYear() + '-' + (mm >= 10 ? mm : '0' + mm) + '-' + (dd >= 10 ? dd : '0' + dd);
}
   
/**
 * Converts Date timestamp to date String in a needed format HH:MM.
 *  Low level helper function, should not be used directly.
 * @param {Integer} ts date timestamp
 * @returns {String} the timestamp in HH:MM format
 */
window['tsToTime'] = function(ts) {
    var d = new Date(ts);
    var hh = d.getHours();
    var mm = d.getMinutes();

    return (hh >= 10 ? hh : '0' + hh) + ':' + (mm >= 10 ? mm : '0'+ mm);
}