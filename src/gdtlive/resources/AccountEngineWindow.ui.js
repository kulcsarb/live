AccountEngineWindowUi = Ext.extend(Ext.Window, {
    title: 'Account Engine',
    width: 800,
    height: 600,
    modal: true,
    layout: 'fit',
    initComponent: function() {
        this.tbar = {
            xtype: 'toolbar',
            buttonAlign: 'left',
            items: [
                {
                    xtype: 'button',
                    text: 'Refresh',
                    ref: '../btnRefresh'
                },
                {
                    xtype: 'tbfill'
                },
                {
                    xtype: 'button',
                    text: 'Connect',
                    ref: '../btnConnect'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Disconnect',
                    ref: '../btnDisconnect'
                }
            ]
        }
        this.items = [
            {
                xtype: 'tabpanel',
                activeTab: 0,
                ref: 'tabPanel',
                items: [
                    {
                        xtype: 'propertygrid',
                        title: 'Details',
                        ref: '../tabDetails',
                        columnWidths: [0.5, 0.5]
                    },
                    {
                        xtype: 'panel',
                        title: 'Log',
                        layout: 'vbox',
                        layoutConfig: {
                            align: 'stretch'
                        },
                        ref: '../tabLog',
                        items: [
                            {
                                xtype: 'container',
                                layout: 'hbox',
                                layoutConfig: {
                                    padding: '0, 0, 5, 5'
                                },
                                items: [
                                    {
                                        xtype: 'radio',
                                        boxLabel: 'Debug',
                                        name: 'logtype',
                                        flex: 1,
                                        checked: true,
                                        ref: '../../../radLogtype0'
                                    },
                                    {
                                        xtype: 'radio',
                                        boxLabel: 'Info',
                                        name: 'logtype',
                                        flex: 1,
                                        ref: '../../../radLogtype1'
                                    },
                                    {
                                        xtype: 'radio',
                                        boxLabel: 'Error',
                                        name: 'logtype',
                                        flex: 1,
                                        ref: '../../../radLogtype2'
                                    }
                                ]
                            },
                            {
                                flex: 1,
                                xtype: 'textarea',
                                name: 'log',
                                readOnly: true,
                                ref: '../../areaLog'
                            }
                        ]
                    },
                    {
                        xtype: 'propertygrid',
                        title: 'Details',
                        ref: '../tabMessages',
                        columnWidths: [160, 0]
                    }
                ]
            }

        ];
        AccountEngineWindowUi.superclass.initComponent.call(this);
    }
});
