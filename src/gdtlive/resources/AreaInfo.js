/**
 * @fileOverview
 * @author vendelin8
 * @version 0.2
 */
/**
 * shows info in a textarea
 * @class AreaInfo
 * @lends AreaInfo.prototype
 */
AreaInfo = Ext.extend(AreaInfoUi, {
    /** @constructs */
    initComponent: function() {
        AreaInfo.superclass.initComponent.call(this);
    }
});
