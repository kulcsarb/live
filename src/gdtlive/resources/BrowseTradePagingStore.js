TradeStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        TradeStore.superclass.constructor.call(this, Ext.apply({
            url: 'trade/gridlist_browse',
            root: 'data',
            autoSave: false,
            batch: true,
            storeId: 'TradeStore',
            remoteSort: true,
            baseParams: {
                start: 0,
                limit: 20,
                sort: "id",
                dir: "ASC"
            },
            fields: [
                {
                    name: 'id',
                    type: 'int'
                },
                {
                    name: 'strategy_name',
                    type: 'string'
                },
                {
                    name: 'direction',
                    type: 'string'
                },
                {
                    name: 'amount',
                    type: 'int'
                },
                {
                    name: 'open_time',
                    type: 'string'
                },
                {
                    name: 'open_price',
                    type: 'float'
                },
                {
                    name: 'symbol',
                    type: 'string'
                },
                {
                    name: 'state',
                    type: 'string'
                },
                {
                    name: 'sl_price',
                    type: 'float'
                },
                {
                    name: 'tp_price',
                    type: 'float'
                },
                {
                    name: 'profit',
                    type: 'float'
                },
                {
                    name: 'MAE',
                    type: 'float'
                },
                {
                    name: 'MFE',
                    type: 'float'
                },
                {
                    name: 'close_time',
                    type: 'string'
                },
                {
                    name: 'close_price',
                    type: 'float'
                },
                {
                    name: 'close_type',
                    type: 'str'
                },
                {
                    name: 'timeframe',
                    type: 'int'
                }
            ]
        }, cfg));
    }
});
copyStore('TradeStore', 'BrowseTradePagingStore');
new TradeStore();
var tradeStore = Ext.StoreMgr.lookup('TradeStore');
delete tradeStore.baseParams;
tradeStore.remoteSort = false;
tradeStore.on('exception', storeErrorHandle);