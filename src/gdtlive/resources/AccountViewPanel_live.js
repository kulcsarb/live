/**
 * @fileOverview
 * @author vendelin8
 * @version 0.5
 */
/**
 * account view
 * @class AccountViewPanel
 * @lends AccountViewPanel.prototype
 */
AccountViewPanel = Ext.extend(AccountViewPanelUi, {
    mainTitle: 'Account {0}',
    panOverviewTitle: 'AccountOverview',
    treeRunHistoryTitle: 'Run history tree',
    btnReloadRunsText: 'Reload',
    btnDecreaseintervalText: 'Decrease interval',
    gridPerformancesumTitle: 'Performance Sum',
    performancesumHeaders: ['Variable', 'Currency', 'Points'],
    tabGraphTitle: 'Graph',
    tabChartsTitle: 'Charts',
    btnRefreshChartText: 'Refresh',
    gridTradelogTitle: 'Trade Log',
    tradelogHeaders: ['Id', 'State', 'Strat Id', 'Open time', 'Instrument', 'Price', 'Direction', 'Amount', 'SL', 'TP', 'Close time', 'Price', 'Type', 'Net profit', 'MAE', 'MFE'],
    btnRefreshtradelogText: 'Refresh',
    allDisplay: 'All',
    /** @constructs */
    initComponent: function() {
        AccountViewPanel.superclass.initComponent.call(this);
        var self = this;
        self.panOverview.title = self.panOverviewTitle;
        self.treeRunHistory.title = self.treeRunHistoryTitle;
        self.btnReloadRuns.text = self.btnReloadRunsText;
        self.btnDecreaseinterval.text = self.btnDecreaseintervalText;
        self.gridPerformancesum.title = self.gridPerformancesumTitle;
        self.gridPerformancesum.hideHeaders = false;
        setGridColums(self.gridPerformancesum, self.performancesumHeaders);
        self.tabGraph.title = self.tabGraphTitle;
        self.tabCharts.title = self.tabChartsTitle;
        self.btnRefreshChart.text = self.btnRefreshChartText;
        self.gridTradelog.title = self.gridTradelogTitle;
        setGridColums(self.gridTradelog, self.tradelogHeaders);
        self.btnRefreshtradelog.text = self.btnRefreshtradelogText;

        self.gridPerformancesum.setUrl(urls.accountPerformancesum);
        self.gridOverview.setUrl(urls.accountInfo);

        self.graphData = {};
        self.charts = {};
        self.ready = [0, 0, 0];

        /** datarow store reference */
        self.datarowStore = Ext.StoreMgr.lookup('HistoricDatarowManagementStore');
        /** timeframe store reference as static store */
        self.timeframeStore = self.cmbChartsfilter.getStore();
        /** symbol store reference as static store */
        self.symbolStore = self.cmbChartsfilter2.getStore();
        /** timeframe store all reference as static store */
        self.timeframeStoreAll = self.cmbTradelogfilter.getStore();
        /** symbol store all reference as static store */
        self.symbolStoreAll = self.cmbTradelogfilter2.getStore();
        /** trade log store reference */
        self.tradelogStore = Ext.StoreMgr.lookup('TradeStore');
        self.tradelogStore.on('load', self.tradelogStoreLoad, self);

        /**
         * fires after the graph canvas is rendered
         *  inits graph plot
         * @event
         */
        self.canvasGraph.on('afterrender', function() {
            self.graphAccountBalance = jsplot(self.canvasGraph.id, {xType: 'custom', labelPattern: 'Day {0}', labelStartIndex: 1, type: 'line', maxXlabel: 'Day 1000', 'labels': ['account balance'], 'initZoom': 1});
            self.graphDrawdown = jsplot(self.canvasGraph.id, {xType: 'custom', labelPattern: 'Trade {0}', labelStartIndex: 1, type: 'line', maxXlabel: 'Trade 1000', 'labels': ['drawdown'], 'initZoom': 1, 'visible': false});
            self.graphProfitloss = jsplot(self.canvasGraph.id, {xType: 'custom', labelPattern: 'Trade {0}', labelStartIndex: 1, type: 'line', maxXlabel: 'Trade 1000', 'labels': ['profit/loss'], 'initZoom': 1, 'visible': false});
            self.graphDailyprofit = jsplot(self.canvasGraph.id, {xType: 'custom', labelPattern: 'Day {0}', labelStartIndex: 1, type: 'bar', maxXlabel: 'Day 1000', 'labels': ['profit'], 'visible': false});
            self.graphWeeklyprofit = jsplot(self.canvasGraph.id, {xType: 'date', type: 'bar', 'labels': ['profit'], 'visible': false});
            self.graphMonthlyprofit = jsplot(self.canvasGraph.id, {xType: 'string', indexedInput: true, type: 'bar', maxXlabel: '2000-08', 'labels': ['profit'], 'visible': false});
            self.graphYearlyprofit = jsplot(self.canvasGraph.id, {xType: 'string', indexedInput: true, type: 'bar', maxXlabel: '2000', 'labels': ['profit'], 'visible': false});
            self.graphMae = jsplot(self.canvasGraph.id, {xType: 'custom', labelPattern: 'Trade {0}', labelStartIndex: 1, type: 'line', maxXlabel: 'Trade 1000', 'labels': ['mae'], 'initZoom': 1, 'visible': false});
            self.graphMfe = jsplot(self.canvasGraph.id, {xType: 'custom', labelPattern: 'Trade {0}', labelStartIndex: 1, type: 'line', maxXlabel: 'Trade 1000', 'labels': ['mfe'], 'initZoom': 1, 'visible': false});
            self.graphBeta = jsplot(self.canvasGraph.id, {xType: 'custom', labelPattern: 'Month {0}', labelStartIndex: 1, type: 'bar', maxXlabel: 'Month 100', 'labels': ['beta'], 'visible': false});
            self.activeGraph = self.graphAccountBalance;
            if (self.graphFillAfterRender) {
                self.graphFillAfterRender = null;
                self.tabGraph.fireEvent('show');
            }
        }, self);
        /**
         * fires when an option is selected in graph type combo
         *  shows selected plot, hides the others
         * @event
         */
        self.cmbGraphtype.on('select', function(combo, record) {
            if (!self.graphAccountBalance) {
                return;
            }
            self.activeGraph.setVisible(false);
            if ((!record) || (record.get('value') === 'account_balance')) {
                self.activeGraph = self.graphAccountBalance;
            } else if (record.get('value') === 'drawdown') {
                self.activeGraph = self.graphDrawdown;
            } else if (record.get('value') === 'trades_profit_loss') {
                self.activeGraph = self.graphProfitloss;
            } else if (record.get('value') === 'daily_profit') {
                self.activeGraph = self.graphDailyprofit;
            } else if (record.get('value') === 'weekly_profit') {
                self.activeGraph = self.graphWeeklyprofit;
            } else if (record.get('value') === 'monthly_profit') {
                self.activeGraph = self.graphMonthlyprofit;
            } else if (record.get('value') === 'yearly_profit') {
                self.activeGraph = self.graphYearlyprofit;
            } else if (record.get('value') === 'MAE') {
                self.activeGraph = self.graphMae;
            } else if (record.get('value') === 'MFE') {
                self.activeGraph = self.graphMfe;
            } else if (record.get('value') === 'beta') {
                self.activeGraph = self.graphBeta;
            }
            self.activeGraph.setVisible(true);
        }, self);

        /**
         * called when the graph canvas has been resized
         *  calls a resize on the plot as well
         * @event
         */
        self.canvasGraph.on('resize', function() {
            if (!self.graphAccountBalance) {
                return;
            }
            self.graphAccountBalance.plot();
            self.graphDrawdown.plot();
            self.graphProfitloss.plot();
            self.graphDailyprofit.plot();
            self.graphWeeklyprofit.plot();
            self.graphMonthlyprofit.plot();
            self.graphYearlyprofit.plot();
            self.graphMae.plot();
            self.graphMfe.plot();
            self.graphBeta.plot();
        }, self);

        /**
         * called when the chart canvas has been resized
         *  calls a resize on the plot as well
         * @event
         */
        self.canvasChart.on('resize', function() {
            if (self.activeChart === undefined) {
                return;
            }
            self.activeChart.plot();
        }, self);

        /**
         * called when the graph tab is shown
         *  if the graph has not been rendered or there is no data, saves this information and returns
         *  if the run is the same as last time, it returns (no need to refresh)
         *      if the graph data is already saved for this run, plots it
         *      loads needed data and plots otherwise
         * @event
         */
        self.tabGraph.on('show', function() {
            if ((!self.activeGraph)) {
                self.graphFillAfterRender = true;
                return;
            }
            if (self.lastGraph === self.lastSelectedRun.uniqueId) {
                return;
            }
            if (typeof(self.graphData[self.lastSelectedRun.uniqueId]) === 'object') {
                self.readGraphs();
            } else {
                Ext.Ajax.request({
                    url: urls.accountGraph,
                    params: self.ajaxParams(),
                    success: function(response) {
                        response = ajaxSuccessHandle(response);
                        if (!response) {
                            return;
                        }
                        self.graphData[self.lastSelectedRun.uniqueId] = response.data;
                        self.readGraphs();
                    },
                    failure: function(response) {
                        ajaxErrorHandle(response);
                    }
                });
            }
        }, self);

        /**
         * called when the charts tab is shown
         *  starts downloading chart data by loading strategy symbol store in case of no data
         *  shows first chart otherwise
         * @event
         */
        self.tabCharts.on('show', function() {
            if (!self.canvasChart.rendered) {
                return;
            }
            self.initCharts();
//            if (self.lastChart === self.lastSelectedRun.uniqueId) {
//                return;
//            }
//            self.lastChart = self.lastSelectedRun.uniqueId;
        }, self);

        self.btnRefreshChart.on('click', function() {
            var activeChart = self.activeChart;
            downloadChart(activeChart.datarowId, self.lastSelectedRun.from_date, self.lastSelectedRun.to_date, self.cmbChartsfilter.getValue(), activeChart);
            self.drawingChart = activeChart;
            if (!self.reloadTradeLog()) {
                self.loadTradesToChart(self.tradelogStore.getRange(), self.drawingChart);
                delete self.drawingChart;
            }
        }, self);

        /**
         * called when a the chart timeframe filter combobox's selection has changed
         *  clears current filter, adds a new one if the current setting is not 'All'
         * @event
         */
        self.cmbChartsfilter.on('select', function(combo, record) {
            self.symbolStore.removeAll();
            self.symbolStore.loadData(record.get('others'));
            comboSelect(self.cmbChartsfilter2, true, 0);
        }, self);

        /**
         * fires when an option is selected in chart type combo
         *  downloads chart data in case of no data
         *  shows selected plot, hides the others
         * @event
         */
        self.cmbChartsfilter2.on('select', function(combo, record) {
            if (!self.canvasChart.rendered) {
                return;
            }
            if (self.activeChart !== undefined) {
                self.activeChart.setVisible(false);
            }
            self.activeChart = self.charts[self.cmbChartsfilter.getValue()][self.cmbChartsfilter2.getValue()];
            self.btnRefreshChart.fireEvent('click');
            self.activeChart.setVisible(true);
        }, self);

        /**
         * called when a the trade log timeframe filter combobox's selection has changed
         *  clears current filter, adds a new one if the current setting is not 'All'
         * @event
         */
        self.cmbTradelogfilter.on('select', function(combo, record) {
            self.symbolStoreAll.removeAll();
            self.symbolStoreAll.loadData(record.get('others'));
            comboSelect(self.cmbTradelogfilter2, true, 0);
        }, self);

        /**
         * called when a the trade log filter combobox's selection has changed
         *  clears current filter, adds a new one if the current setting is not 'All'
         * @event
         */
        self.cmbTradelogfilter2.on('select', function(combo, record) {
            self.tradelogStore.clearFilter();
            if (self.cmbTradelogfilter.getValue() !== self.allDisplay) {
                self.tradelogStore.filter('timeframe', self.cmbTradelogfilter.getValue(), false, true);
            }
            if (record.get('display') !== self.allDisplay) {
                self.tradelogStore.filter('symbol', record.get('display'), false, true);
            }
        }, self);

        /**
         * called when the trade log grid has been shown
         *  loads strategy symbol all store and trade log if needed
         * @event
         */
        self.gridTradelog.on('show', function() {
            self.reloadTradeLog();
        }, self);

        self.btnRefreshtradelog.on('click', function() {
            self.reloadTradeLog();
        }, self);

        self.canvasChart.on('afterrender', function() {
            self.initCharts();
        });

        self.panRight.on('show', function() {
            Ext.Ajax.request({
                url: urls.accountRunSymbolTimeframes,
                params: self.ajaxParams(false),
                success: function(response) {
                    response = ajaxSuccessHandle(response);
                    if (!response) {
                        return;
                    }
                    response = response.data;
                    var i, record, timeframe, symbol, timeframeRecord, symbolRecord;
                    var timeframeRecords = [];
                    var addedTimeframe = {};
                    var symbolRecords = [];
                    var addedSymbol = {};
                    var datarowStoreRecords = self.datarowStore.getRange();
                    var allSymbolRecords = [];
                    for (i = 0; i < datarowStoreRecords.length; i++) {
                        record = datarowStoreRecords[i];
                        timeframe = record.get('timeframe_num');
                        if (timeframe in response) {
                            if (timeframe in addedTimeframe) {
                                timeframeRecord = addedTimeframe[timeframe];
                            } else {
                                timeframeRecord = {'value': timeframe, 'display': record.get('timeframe_str'), 'others': []}
                                timeframeRecords.push(timeframeRecord);
                                addedTimeframe[timeframe] = timeframeRecord;
                            }
                            symbol = record.get('symbol');
                            if (response[timeframe].indexOf(symbol) !== -1) {
                                if (record.get('symbol') in addedSymbol) {
                                    symbolRecord = addedSymbol[symbol];
                                } else {
                                    symbolRecord = {'value': symbol, 'display': symbol, 'others': record.get('id')};
                                    addedSymbol[symbol] = symbolRecord;
                                    allSymbolRecords.push(symbolRecord);
                                }
                                timeframeRecord['others'].push(symbolRecord);
                            }
                        }
                    }

                    self.timeframeStore.removeAll();
                    self.timeframeStore.loadData(timeframeRecords);
                    comboSelect(self.cmbChartsfilter, true, 0);

                    self.timeframeStoreAll.removeAll();
                    timeframeRecords = timeframeRecords.slice();
                    timeframeRecords.splice(0, 0, {'display': self.allDisplay, 'value': self.allDisplay, 'others': allSymbolRecords});
                    for (i = 0; i < timeframeRecords.length; i++) {
                        timeframeRecords[i]['others'] = timeframeRecords[i]['others'].slice();
                        timeframeRecords[i]['others'].splice(0, 0, {'display': self.allDisplay, 'value': self.allDisplay});
                    }
                    self.timeframeStoreAll.loadData(timeframeRecords);
                    comboSelect(self.cmbTradelogfilter, true, 0);
                },
                failure: function(response) {
                    ajaxErrorHandle(response);
                }
            });
        });

        /**
         * Reloads account runs in the tree.
         */
        self.btnReloadRuns.on('click', function() {
            var root = self.treeRunHistory.getRootNode();
            root.removeAll();
            Ext.Ajax.request({
                url: urls.accountRunHistoryTree,
                params: {'id': self.recordId},
                success: function(response) {
                    response = ajaxSuccessHandle(response);
                    if (!response) {
                        return;
                    }
                    response = Ext.util.JSON.decode(response.data);
                    root.loadParams = {'id': self.recordId, 'from_date': Date.parseDate(response.from_date, "Y-m-d H:i:s"), 'to_date': Date.parseDate(response.to_date, "Y-m-d H:i:s")};
                    delete response.from_date;
                    delete response.to_date;
                    addTreeNodeChildren(response, root, function(child, childId, parentIds) {
				        if ((parentIds) && (parentIds.length)) {
				            if (parentIds.length === 1) {  //account + portfolio
                                child.loadParams.id = parentIds[0];
                                child.loadParams.id2 = childId;
				            } else {    //accountRun
                                child.loadParams.id = parentIds[0];
                                child.loadParams.id2 = parentIds[1];
                                child.loadParams.id3 = childId;
				            }
				        } else {    //account
                                child.loadParams.id = childId;
				        }
                        if (((self.lastSelectedRun !== -1) && (('id3' in self.lastSelectedRun) === ('id3' in child.loadParams)) && (('id2' in self.lastSelectedRun) === ('id2' in child.loadParams))) && ((('id3' in self.lastSelectedRun) && (self.lastSelectedRun.id3 === childId)) || (!('id3' in self.lastSelectedRun) && ('id2' in self.lastSelectedRun) && (self.lastSelectedRun.id2 === childId)) || (!('id2' in self.lastSelectedRun) && (self.lastSelectedRun.id === childId)))) {
                            self.treeRunHistory.getSelectionModel().select(child);
                            self.treeRunHistory.fireEvent('click', child);
                        }
                    }, self.recordId);
                    if (self.lastSelectedRun === -1) {
                        self.panRight.hide();
                    }
                },
                failure: function(response) {
                    ajaxErrorHandle(response);
                }
            });
        }, self);

        /**
         * fires when a node is collapsed
         *  resets loaded variable to maintain possible changes
         * @event
         */
        self.treeRunHistory.on('beforecollapsenode', function(node) {
            return false;
        }, self);

        /**
         * fires when a node is clicked
         *  loads the related data to the content grid
         * @event
         */
        self.treeRunHistory.on('click', function(node) {
            var loadParams = node.loadParams;
            if (!loadParams) {
                return false;
            }
            self.lastSelectedRun = loadParams;
            if (!('uniqueId' in self.lastSelectedRun)) {
		        if ('id3' in self.lastSelectedRun) {
		            self.lastSelectedRun.uniqueId = '__' + self.lastSelectedRun.id3;
		        } else if ('id2' in self.lastSelectedRun) {
		            self.lastSelectedRun.uniqueId = self.lastSelectedRun.id + '_' + self.lastSelectedRun.id2 + '_';
		        } else {
		            self.lastSelectedRun.uniqueId = self.lastSelectedRun.id + '__';
		        }
            }
//            if ('parentIds' in loadParams)
            self.btnDecreaseinterval.setDisabled(self.lastSelectedRun.type === 2);
            self.gridPerformancesum.setParams(self.ajaxParams());
            if (self.panRight.getActiveTab) {
                var activeTab = self.panRight.getActiveTab();
                if ((activeTab) && (activeTab.fireEvent)) {
                    self.panRight.getActiveTab().fireEvent('show');
                }
            }
            self.panRight.show();
        }, self);

        /**
         * fires after the panel is rendered
         *  initializes panel
         * @event
         */
        self.on('afterrender', function() {
            self.setTitle(String.format(self.mainTitle, self.recordName));
            self.panRight.setActiveTab(0);
            self.treeRunHistory.getRootNode().setText(self.recordName);
        }, self);

        /**
         * fires before the panel is shown
         *  loads active tabs' grids' stores
         * @event
         */
        self.on('show', function() {
//            var symbols = []
//            var i, symbol;
//            for (i in self.symbols) {
//                symbol = self.symbols[i];
//                if (typeof(symbol) === 'function') {
//                    continue;
//                }
//                symbols.push({'value': symbol, 'display': symbol});
//            }
            self.gridOverview.setParams({'id': self.recordId, 'userId': currentUser.id});
            self.gridOverview.fireEvent('show');
            if (!mainViewport.liveManagementP.hidden) {
                self.btnReloadRuns.fireEvent('click');
            }
        }, self);

        /**
         * fires when the window is destroyed
         *  destroys the chart components
         * @event
         */
        self.on('destroy', function() {
            if (self.graphAccountBalance) {
                self.graphAccountBalance.destroy();
                self.graphDrawdown.destroy();
                self.graphProfitloss.destroy();
                self.graphDailyprofit.destroy();
                self.graphWeeklyprofit.destroy();
                self.graphMonthlyprofit.destroy();
                self.graphYearlyprofit.destroy();
                self.graphMae.destroy();
                self.graphMfe.destroy();
                self.graphBeta.destroy();
            }
            var i, j, chartsi, chartsij;
            for (i in self.charts) {
                chartsi = self.charts[i];
                if (typeof(chartsi) === 'function') {
                    continue;
                }
	            for (j in chartsi) {
	                chartsij = chartsi[j];
	                if (typeof(chartsij) === 'function') {
	                    continue;
	                }
                    chartsij.destroy();
                }
            }
        }, self);
    },

    /**
     * reads graph data to canvas
     */
    readGraphs: function() {
        var self = this;
        var data = self.graphData[self.lastSelectedRun.uniqueId];
        self.graphAccountBalance.read(data['account_balance']);
        self.graphDrawdown.read(data['drawdown']);
        self.graphProfitloss.read(data['trades_profit_loss']);
        self.graphDailyprofit.read(data['daily_profit']);
        self.graphWeeklyprofit.read(data['weekly_profit']);
        self.graphMonthlyprofit.read(data['monthly_profit']);
        self.graphYearlyprofit.read(data['yearly_profit']);
        self.graphMae.read(data['MAE']);
        self.graphMfe.read(data['MFE']);
        self.graphBeta.read(data['beta']);
        self.lastGraph = self.lastSelectedRun.uniqueId;
        self.canvasGraph.fireEvent('resize');
    },

    /**
     * loads a new trade log if needed, or reads the saved data to the store
     */
    reloadTradeLog: function() {
        var self = this;
        var store = self.tradelogStore;
        if (store.currGrid !== self.gridTradelog) {
            if (store.getCount() > 0) {
                if (store.currGrid) {
                    store.currGrid.data = store.getRange();
                }
                store.removeAll();
            }
        }
        if ((self.lastTradelog === undefined) || (self.lastTradelog.id !== self.lastSelectedRun.uniqueId) || (self.lastTradelog.to_date < self.lastSelectedRun.to_date) || (self.lastTradelog.from_date < self.lastSelectedRun.from_date)) {
            self.lastTradelog = {id: self.lastSelectedRun.uniqueId, from_date: self.lastSelectedRun.from_date, to_date: self.lastSelectedRun.to_date};
            store.currGrid = self.gridTradelog;
            store.load({params: self.ajaxParams(false)});
            return true;
        }
        if (self.gridTradelog.data) {
            store.currGrid = self.gridTradelog;
            store.add(self.gridTradelog.data);
            self.gridTradelog.data = null;
        }
        return false;
    },

    /**
     * loads trade log data to the 
     * @param {Array} records the trade log records to show
     * @param {Integer} chartIndex index of the chart to add the trade log records
     */
    loadTradesToChart: function(records, chart) {
        var self = this;
        var filterTimeframe = self.cmbChartsfilter.getValue();
        var filterSymbol = self.cmbChartsfilter2.getValue();
        var tradelogExtraData = [];
        var record, extrai;
        for (i in records) {
            record = records[i];
            if ((typeof(record) === 'function') || (record.get('symbol') !== filterSymbol) || (record.get('timeframe') != filterTimeframe)) {
                continue;
            }
            extrai = {'opentime': Date.parseDate(record.get('open_time'), "Y-m-d H:i:s").format('U') * 1000, 'openprice': record.get('open_price'), 'direction': record.get('direction'), 'amount': record.get('amount'), 'closeprice': record.get('close_price'), 'type': record.get('close_type'), 'profit': record.get('profit')};
            if (record.get('close_time')) {
                extrai.closetime = Date.parseDate(record.get('close_time'), "Y-m-d H:i:s").format('U') * 1000;
            }
            tradelogExtraData.push(extrai);
        }
        chart.readExtra(tradelogExtraData);
        self.canvasChart.fireEvent('resize');
    },

    /**
     * called when the trade log store is loaded
     *  selects "All" option in related (filter) combo box
     *  calls chart draw if available and needed
     */
    tradelogStoreLoad: function(store, records) {
        var self = this;
        if ((self.destroying) || (self.isDestroyed) || (!self.isVisible())) {
            return;
        }
        if (records.length === undefined) {
            records = [records];
        }
        comboSelect(self.cmbTradelogfilter, true, 0);
        if (self.drawingChart !== undefined) {
            self.loadTradesToChart(records, self.drawingChart);
            delete self.drawingChart;
        }
    },

    ajaxParams: function(addDates) {
        var self = this;
        if (addDates === undefined) {
            addDates = true;
        }
        var params = {};
        if ('id3' in self.lastSelectedRun) {
            params['id3'] = self.lastSelectedRun.id3;
        } else {
            params = {'id': self.lastSelectedRun.id};
	        if ('id2' in self.lastSelectedRun) {
	            params['id2'] = self.lastSelectedRun.id2;
	        }
        }
        if (addDates) {
            params.from_date = self.lastSelectedRun.from_date;
            params.to_date = self.lastSelectedRun.to_date;
        }
        return params;
    },

    reloadTimeframSymbol: function() {
    },

    initCharts: function() {
        var self = this;
        var i, j, recordi, recordij, currParams, records2, chartsi;
        var records = self.timeframeStore.getRange();
        var params = {'visible': false, 'extras': ['tradeTriangle'], 'dataReadyNeeded': true};
        for (i = 0; i < records.length; i++) {  //iter on timeframes
            recordi = records[i];
            if (!(recordi.get('value') in self.charts)) {
                self.charts[recordi.get('value')] = {};
            }
            chartsi = self.charts[recordi.get('value')];
            records2 = recordi.get('others');
            for (j = 0; j < records2.length; j++) { //iter on symbols
                recordij = records2[j];
	            if (!(recordij.value in chartsi)) {
	                currParams = clone(params);
	                currParams.globalSource = constants.datarow + recordij.others;
                    chartsi[recordij.value] = {'chart': jsplot(self.canvasChart.id, currParams), 'datarowId': recordij.others};
	            }
            }
        }
        if (self.activeChart === undefined) {
            self.activeChart = self.charts[self.cmbChartsfilter.getValue()][self.cmbChartsfilter2.getValue()];
            self.activeChart.setVisible(true);
        }
        comboSelect(self.cmbChartsfilter, true, 0);
    }
});
