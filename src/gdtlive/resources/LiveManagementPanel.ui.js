LiveManagementPanelUi = Ext.extend(Ext.TabPanel, {
    activeTab: 0,
    enableTabScroll: true,
    unstyled: true,
    id: 'resultsTabs',
    initComponent: function() {
        this.items = [
            {
                xtype: 'panel',
                title: 'NAVIGATE',
                layout: 'border',
                ref: 'tabNavigate',
                items: [
                    {
                        xtype: 'panel',
                        region: 'center',
                        margins: '4 0 4 0',
                        layout: 'card',
                        activeItem: 0,
                        ref: '../panelContent',
                        autoDestroy: false,
                        items: [
                            {
                                xtype: 'grid',
                                store: 'CandidatesStore',
                                stripeRows: true,
//                                loadMask: true,
                                ref: '../../gridContent',
                                selModel: new Ext.grid.CheckboxSelectionModel(),
                                columns: [
                                    {
                                        xtype: 'gridcolumn',
                                        header: 'Column',
                                        sortable: true,
                                        width: 100
                                    }
                                ],
                                bbar: {
                                    xtype: 'paging',
                                    store: 'CandidatesStore',
                                    id: 'content-pagingtoolbar'
                                }
                            }
                        ],
                        tbar: {
                            xtype: 'toolbar',
                            items: [
                                {
                                    xtype: 'button',
                                    text: 'View',
                                    ref: '../../../btnView'
                                },
		                        {
		                            xtype: 'tbspacer',
		                            width: 200
		                        },
                                {
                                    xtype: 'tbseparator',
                                    ref: '../../../sepStart'
                                },
                                {
                                    xtype: 'button',
                                    text: 'Start',
                                    ref: '../../../btnStart'
                                },
                                {
                                    xtype: 'tbseparator',
                                    ref: '../../../sepStop'
                                },
                                {
                                    xtype: 'button',
                                    text: 'Stop',
                                    ref: '../../../btnStop'
                                },
                                {
                                    xtype: 'tbseparator',
                                    ref: '../../../sepAbort'
                                },
                                {
                                    xtype: 'button',
                                    text: 'Abort',
                                    ref: '../../../btnAbort'
                                },
                                {
                                    xtype: 'tbseparator',
                                    ref: '../../../sepClose'
                                },
                                {
                                    xtype: 'button',
                                    text: 'Close trade',
                                    ref: '../../../btnClose'
                                },
                                {
                                    xtype: 'tbseparator',
                                    ref: '../../../sepRemovefromportfolio'
                                },
                                {
                                    xtype: 'button',
                                    text: 'Remove',
                                    ref: '../../../btnRemovefromportfolio'
                                },
                                {
                                    xtype: 'tbseparator',
                                    ref: '../../../sepTestaccountengine'
                                },
                                {
                                    xtype: 'button',
                                    text: 'Fix Engine',
                                    ref: '../../../btnTestaccountengine'
                                }
                            ]
                        }
                    },
                    {
                        xtype: 'treepanel',
                        title: 'Browse',
                        width: 160,
                        split: true,
                        collapsible: true,
                        titleCollapse: true,
                        margins: '5 0 5 5',
                        cmargins: '5 5 5 0',
                        useArrows: true,
                        rootVisible: true,
                        autoScroll: true,
                        region: 'west',
                        ref: '../treeBrowse',
                        root: {
                            text: 'All Portfolios',
                            expandable: true
                        },
                        loader: {}
                    }
                ]
            }
        ];
        LiveManagementPanelUi.superclass.initComponent.call(this);
    }
});
