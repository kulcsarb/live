PropertyGridUi = Ext.extend(Ext.grid.GridPanel, {
    store: 'PropertyStore',
    hideHeaders: true,
    border: false,
    disableSelection: true,
    stripeRows: true,
//    loadMask: true,
    autoExpandColumn: null,
    initComponent: function() {
        this.columns = [
            {
                xtype: 'gridcolumn',
                dataIndex: 'name'
            },
            {
                xtype: 'gridcolumn',
                dataIndex: 'value0'
            },
            {
                xtype: 'gridcolumn',
                dataIndex: 'value1',
                hidden:true
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            hidden: true,
            items: [
                {
                    xtype: 'button',
                    text: 'MyButton',
                    hidden: true
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            hidden: true,
            items: [
                {
                    xtype: 'button',
                    text: 'MyButton',
                    hidden: true
                }
            ]
        };
        PropertyGridUi.superclass.initComponent.call(this);
    }
});
