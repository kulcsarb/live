/*
 * File: StrategyImportWindow.ui.js
 * Date: Wed Aug 03 2011 11:01:47 GMT+0200 (CEST)
 * 
 * This file was generated by Ext Designer version 1.1.2.
 * http://www.sencha.com/products/designer/
 *
 * This file will be auto-generated each and everytime you export.
 *
 * Do NOT hand edit this file.
 */

StrategyImportWindowUi = Ext.extend(Ext.Window, {
    title: 'Choose file...',
    width: 400,
    resizable: false,
    autoHeight: true,
    closable: false,
    modal: true,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                monitorValid: true,
                padding: 20,
                unstyled: true,
                url: '/strategy/importStrategies',
                hideLabels: true,
                fileUpload: true,
                ref: 'formFile',
                items: [
                    {
                        xtype: 'box',
                        html: 'Choose a file of exported strategies:',
                        ref: '../description'
                    },
                    {
                        xtype: 'hidden',
                        fieldLabel: 'Label',
                        anchor: '100%',
                        name: 'userId',
                        ref: '../fldUserid'
                    }
                ],
                fbar: {
                    xtype: 'toolbar',
                    buttonAlign: 'center',
                    items: [
                        {
                            xtype: 'button',
                            text: 'OK',
                            type: 'submit',
                            formBind: true,
                            ref: '../../btnOk'
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel',
                            ref: '../../btnCancel'
                        }
                    ]
                }
            }
        ];
        StrategyImportWindowUi.superclass.initComponent.call(this);
    }
});
