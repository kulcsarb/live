AddToPortfolioUi = Ext.extend(Ext.Window, {
    title: 'Add To Portfolio',
    width: 400,
    modal: true,
    labelWidth: 80,
    autoHeight: true,
    resizable: false,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                monitorValid: true,
                labelWidth: 160,
                ref: 'formBase',
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Portfolio name',
                        anchor: '85%',
                        store: 'PortfolioStore',
                        displayField: 'name',
                        valueField: 'id',
                        allowBlank: false,
                        blankText: 'Select Portfolio',
                        emptyText: 'Select Portfolio...',
                        triggerAction: 'all',
                        mode: 'local',
                        forceSelection: true,
                        ref: '../cmbPortfolio'
                    }
                ],
                fbar: {
                    xtype: 'toolbar',
                    buttonAlign: 'left',
                    items: [
                        {
                            xtype: 'button',
                            text: 'OK',
                            formBind: true,
                            type: 'submit',
                            ref: '../../btnOk'
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel',
                            ref: '../../btnCancel'
                        }
                    ]
                }
            }
        ];
        AddToPortfolioUi.superclass.initComponent.call(this);
    }
});
