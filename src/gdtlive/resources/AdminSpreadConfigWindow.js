/**
 * @fileOverview
 * @author vendelin8, kulcsarb
 * @version 0.12
 */
/**
 * admin spread configuration functionality
 * @class AdminSpreadConfigWindow
 * @lends AdminSpreadConfigWindow.prototype
 */
AdminSpreadConfigWindow = Ext.extend(AdminSpreadConfigWindowUi, {
    mainTitle: 'Spread Configuration',
    fsetConfigTitle: 'Spread Configs',
    btnAddconfigText: 'Add config',
    btnEditconfigText: 'Edit config',
    btnDeleteconfigText: 'Delete config',
    configHeaders: ['SpreadConfigName', 'Commission', 'DefaultSpread'],
    fsetRowTitle: 'Details of selected spread config',
    btnAddrowText: 'Add row',
    btnEditrowText: 'Edit row',
    btnDeleterowText: 'Delete row',
    rowHeaders: ['Instrument', 'DefaultSpread', 'ExSpreadStart', 'ExSpreadStop', 'ExSpread'],
    deleteConfigTitle: 'Delete Spread Config?',
    deleteConfigHtml: 'The selected <b>Spread Config</b> is going to be deleted! Do you want to perform it?',
    deleteRowTitle: 'Delete Spread Config Row?',
    deleteRowHtml: 'The selected <b>Spread Config Row</b> is going to be deleted! Do you want to perform it?',
    /** @constructs */
    initComponent: function() {
        AdminSpreadConfigWindow.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        self.fsetConfig.title = self.fsetConfigTitle;
        self.btnAddconfig.text = self.btnAddconfigText;
        self.btnEditconfig.text = self.btnEditconfigText;
        self.btnDeleteconfig.text = self.btnDeleteconfigText;
        setGridColums(self.gridConfig, self.configHeaders, 1);
        self.fsetRow.title = self.fsetRowTitle;
        self.btnAddrow.text = self.btnAddrowText;
        self.btnEditrow.text = self.btnEditrowText;
        self.btnDeleterow.text = self.btnDeleterowText;
        setGridColums(self.gridRow, self.rowHeaders, 1);

        /** spread config store reference */
        self.configStore = storeCRUD('AdminSpreadConfigStore', self, self.gridConfig);
        /**
         * fires after loading the store
         *  resets the buttons enablity
         * @event
         */
        self.configStore.on('load', function() {
            self.gridConfig.getSelectionModel().fireEvent('selectionchange');
            self.gridRow.getSelectionModel().fireEvent('selectionchange');
        }, self);

        /** spread config row store reference */
        self.rowStore = storeCRUD('AdminSpreadRowStore', self, self.gridRow);

        /** symbol store reference */
        self.symbolStore = lazyStoreR('SymbolStore');

        /**
         * fires when user clicks on 'Add config' button
         *  shows config edit window with empty record
         * @event
         */
        self.btnAddconfig.on('click', function() {
            self.popup = new AdminSpreadConfigEditWindow();
            self.popup.record = null;
            self.popup.show();
        });

        /**
         * fires when config grid's selection changes
         *  loads stored row to the row grid and disables/enables config modify buttons
         * @event
         */
        self.gridConfig.getSelectionModel().on('selectionchange', function() {
            var record = self.gridConfig.getSelectionModel().getSelected();
            var newVal = (record === undefined);
            if (newVal) {
                self.lastSelConfigId = null;
                self.rowStore.removeAll();
            } else if (record.get('id') !== self.lastSelConfigId) {
                self.lastSelConfigId = record.get('id');
                self.rowStore.load({params: {'id': record.get('id')}});
            }
            self.btnEditconfig.setDisabled(newVal);
            self.btnDeleteconfig.setDisabled(newVal);
            self.btnAddrow.setDisabled(newVal);
        });

        /**
         * fires when user clicks on 'Edit config' button
         *  shows a window for editing selected config with the related record
         * @event
         */
        self.btnEditconfig.on('click', function() {
            var record = self.gridConfig.getSelectionModel().getSelected();
            if (record === undefined) {
                return;
            }
            self.popup = new AdminSpreadConfigEditWindow();
            self.popup.record = record;
            self.popup.show();
        });  

        /**
         * fires when user clicks on 'Delete config' button
         *  shows a window for confirmation and deletes selected config if asked for
         * @event
         */
        self.btnDeleteconfig.on('click', function() {
            confirmDestroyRecordSafe(self, self.gridConfig, self.deleteConfigHtml, self.deleteConfigTitle);
        });

        /**
         * fires when user clicks on 'Add row' button
         *  shows row edit window with empty record and stored config id
         * @event
         */
        self.btnAddrow.on('click', function() {
            var record = self.gridConfig.getSelectionModel().getSelected();
            if (record === undefined) {
                return;
            }
            self.popup = new AdminSpreadRowEditWindow();
            self.popup.record = null;
            self.popup.configId = record.get('id');
            self.popup.show();
        });

        /**
         * fires when row grid's selection changes
         *  disables/enables row modify buttons
         * @event
         */
        self.gridRow.getSelectionModel().on('selectionchange', function() {
            var newVal = (self.gridRow.getSelectionModel().getSelected() === undefined);
            self.btnEditrow.setDisabled(newVal);
            self.btnDeleterow.setDisabled(newVal);
        });

        /**
         * fires when user clicks on 'Edit row' button
         *  shows a window for editing selected row with the related record
         * @event
         */
        self.btnEditrow.on('click', function() {
            var record = self.gridRow.getSelectionModel().getSelected();
            if (record === undefined) {
                return;
            }
            self.popup = new AdminSpreadRowEditWindow();
            self.popup.record = record;
            self.popup.configId = record.get('configId');
            self.popup.show();
        });

        /**
         * fires when user clicks on 'Delete row' button
         *  shows a window for confirmation and deletes selected row if asked for
         * @event
         */
        self.btnDeleterow.on('click', function() {
            confirmDestroyRecordSafe(self, self.gridRow, self.deleteRowHtml, self.deleteRowTitle);
        });

        /**
         * fires when the window is shown
         *  (re) loads the config store
         * @event
         */
        self.on('show', function() {
            self.configStore.load();
        }, self);
    }
});
