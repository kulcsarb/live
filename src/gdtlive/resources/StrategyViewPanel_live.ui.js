StrategyViewPanelUi = Ext.extend(Ext.Panel, {
    title: 'StrategyView',
    layout: 'border',
    closable: true,
    initComponent: function() {
        this.items = [
            {
                xtype: 'panel',
                title: 'StrategyOverview',
                layout: 'anchor',
                region: 'west',
                width: 206,
                collapsible: true,
                split: true,
                ref: 'panOverview',
                items: [
                    {
                        xtype: 'panel',
                        layout: 'vbox',
                        anchor: '100% 62%',
                        layoutConfig: {
                            align: 'stretch'
                        },
                        items: [
                            {
                                flex: 1,
                                ref: '../../gridOverview',
                                columnWidths: [100, 0],
                                xtype: 'propertygrid'
                            }
                        ],
                        fbar: {
                            xtype: 'toolbar',
                            buttonAlign: 'left',
                            items: [
                                {
                                    xtype: 'button',
                                    text: 'View DNS',
                                    ref: '../../../btnViewdns'
                                },
                                {
                                    xtype: 'button',
                                    text: 'MMPlugin',
                                    ref: '../../../btnMmplugin'
                                }
                            ]
                        }
                    },
                    {
                        xtype: 'treepanel',
                        title: 'Run tree',
                        anchor: '100% 38%',
//                        loadMask: true,
                        ref: '../treeRunHistory',
                        margins: '5 0 5 5',
                        cmargins: '5 5 5 0',
                        useArrows: true,
                        rootVisible: false,//TODO E?
                        autoScroll: true,
                        root: {
                            loaded: true,
                            expanded: true
                        },
                        loader: {},
                        tbar: {
                            xtype: 'toolbar',
                            buttonAlign: 'left',
                            items: [
                                {
                                    xtype: 'button',
                                    text: 'Reload',
                                    ref: '../../../btnReloadRuns'
                                }
                            ]
                        }
                    }
                ]
            },
            {
                xtype: 'tabpanel',
                activeTab: 5,
                region: 'center',
                width: 100,
                ref: 'panRight',
                layoutConfig: {
                    deferredRender: true
                },
                items: [
//                    {
//                        title: 'Run Settings',
//                        ref: '../gridRunsettings',
//                        xtype: 'propertygrid2'
//                    },
                    {
                        title: 'Performance Sum',
                        ref: '../gridPerformancesum',
                        xtype: 'propertygrid2'
                    },
                    {
                        xtype: 'panel',
                        title: 'Graph',
                        layout: 'vbox',
                        ref: '../tabGraph',
                        layoutConfig: {
                            align: 'stretch'
                        },
                        tbar: {
                            xtype: 'toolbar',
                            items: [
                                {
                                    xtype: 'combo',
                                    store: 'StrategyGraphTypeStore',
                                    mode: 'local',
                                    triggerAction: 'all',
                                    editable: false,
                                    displayField: 'display',
                                    value: 'Account Balance',
                                    ref: '../../../cmbGraphtype'
                                }
                            ]
                        },
                        items: [
                            {
                                xtype: 'container',
                                flex: 1,
                                ref: '../../canvasGraph'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        title: 'Charts',
                        layout: 'vbox',
                        ref: '../tabCharts',
                        layoutConfig: {
                            align: 'stretch'
                        },
                        tbar: {
                            xtype: 'toolbar',
                            items: [
                                {
                                    xtype: 'combo',
                                    store: 'StaticStore',
                                    displayField: 'display',
                                    valueField: 'value',
                                    mode: 'local',
                                    triggerAction: 'all',
                                    editable: false,
                                    ref: '../../../cmbChartsfilter'
                                },
                                {
                                    xtype: 'button',
                                    text: 'Refresh',
                                    ref: '../../../btnRefreshchart'
                                }
                            ]
                        },
                        items: [
                            {
                                xtype: 'container',
                                flex: 1,
                                ref: '../../canvasChart'
                            }
                        ]
                    },
                    {
                        xtype: 'grid',
                        title: 'Trade Log',
                        store: 'TradeStore',
//                        loadMask: true,
                        ref: '../gridTradelog',
                        disableSelection: true,
                        tbar: {
                            xtype: 'toolbar',
                            items: [
                                {
                                    xtype: 'combo',
                                    store: 'StaticStore2',
                                    displayField: 'display',
                                    valueField: 'value',
                                    mode: 'local',
                                    triggerAction: 'all',
                                    editable: false,
                                    ref: '../../../cmbTradelogfilter'
                                },
                                {
                                    xtype: 'button',
                                    text: 'Refresh',
                                    ref: '../../../btnRefreshtradelog'
                                }
                            ]
                        },
                        columns: [
                            {
                                xtype: 'gridcolumn',
                                header: 'Id',
                                width: 45,
                                dataIndex: 'id'
                            },
                            {
                                xtype: 'gridcolumn',
                                header: 'State',
                                width: 80,
                                dataIndex: 'state'
                            },
                            {
                                xtype: 'gridcolumn',
                                dataIndex: 'open_time',
                                header: 'Open time',
                                width: 111
                            },
                            {
                                xtype: 'gridcolumn',
                                dataIndex: 'symbol',
                                header: 'Instrument',
                                width: 100,
                                align: 'right'
                            },
                            {
                                xtype: 'numbercolumn',
                                dataIndex: 'open_price',
                                header: 'Price',
                                width: 50,
                                align: 'right',
                                format: '0,000.00000'
                            },
                            {
                                xtype: 'gridcolumn',
                                dataIndex: 'direction',
                                header: 'Direction',
                                width: 70
                            },
                            {
                                xtype: 'numbercolumn',
                                header: 'Amount',
                                width: 60,
                                dataIndex: 'amount',
                                align: 'right',
                                format: '0,000'
                            },
                            {
                                xtype: 'numbercolumn',
                                header: 'SL',
                                width: 60,
                                align: 'right',
                                dataIndex: 'sl_price',
                                format: '0,000.00000'
                            },
                            {
                                xtype: 'numbercolumn',
                                header: 'TP',
                                width: 60,
                                align: 'right',
                                dataIndex: 'tp_price',
                                format: '0,000.00000'
                            },
                            {
                                xtype: 'gridcolumn',
                                header: 'Close time',
                                width: 111,
                                dataIndex: 'close_time'
                            },
                            {
                                xtype: 'numbercolumn',
                                header: 'Price',
                                width: 50,
                                dataIndex: 'close_price',
                                align: 'right',
                                format: '0,000.00000'
                            },
                            {
                                xtype: 'gridcolumn',
                                header: 'Type',
                                width: 50,
                                dataIndex: 'close_type'
                            },
                            {
                                xtype: 'numbercolumn',
                                header: 'Net profit',
                                width: 65,
                                dataIndex: 'profit',
                                align: 'right'
                            },
                            {
                                xtype: 'numbercolumn',
                                header: 'MAE',
                                width: 50,
                                dataIndex: 'MAE',
                                align: 'right',
                                format: '0,000.00'
                            },
                            {
                                xtype: 'numbercolumn',
                                header: 'MFE',
                                width: 50,
                                dataIndex: 'MFE',
                                align: 'right',
                                format: '0,000.00'
                            }
                        ]
                    }
                ]
            }
        ];
        StrategyViewPanelUi.superclass.initComponent.call(this);
    }
});
