AccountsPanelUi = Ext.extend(Ext.grid.GridPanel, {
    title: 'All Accounts',
    store: 'AccountsStore',
    height: 550,
    padding: 5,
    initComponent: function() {
        this.selModel = new Ext.grid.CheckboxSelectionModel();
        this.columns = [
            this.selModel,
            {
                xtype: 'gridcolumn',
                dataIndex: 'id',
                header: 'Id',
                sortable: false,
                width: 50
            },
            {
                xtype: 'gridcolumn',
                dataIndex: 'brokername',
                header:  'Boker name',
                sortable: false,
                width: 70
            },
            {
                xtype: 'gridcolumn',
                header:  'Account name',
                sortable: true,
                width: 80,
                dataIndex: 'name'
            },
            {
                xtype: 'gridcolumn',
                header:  'Username in Broker system',
                sortable: true,
                width: 148,
                dataIndex: 'username'
            },
            {
                xtype: 'gridcolumn',
                header:  'Sender Comp Id',
                sortable: true,
                width: 90,
                dataIndex: 'sendercompid'
            },
            {
                xtype: 'gridcolumn',
                header:  'Target Comp Id',
                sortable: true,
                width: 85,
                dataIndex: 'targetcompid'
            },
            {
                xtype: 'numbercolumn',
                header:  'Base Capital',
                sortable: true,
                width: 85,
                dataIndex: 'base_equity'
            },
            {
                xtype: 'numbercolumn',
                header:  'Leverage',
                sortable: true,
                width: 60,
                dataIndex: 'leverage',
                format: '0,000'
            },
            {
                xtype: 'numbercolumn',
                header:  'Risk multiplier',
                sortable: true,
                width: 80,
                dataIndex: 'risk_multiplier'
            },
            {
                xtype: 'gridcolumn',
                header:  'Server url',
                sortable: true,
                width: 130,
                dataIndex: 'server_url'
            },
            {
                xtype: 'numbercolumn',
                header:  'Server port',
                sortable: true,
                width: 65,
                dataIndex: 'server_port',
                format: '0,000'
            },
            {
                xtype: 'gridcolumn',
                header:  'Use ssl',
                sortable: true,
                width: 50,
                dataIndex: 'use_ssl'
            }
        ];
        this.tbar = {
            xtype: 'toolbar',
            buttonAlign: 'left',
            items: [
                {
                    xtype: 'button',
                    text: 'Create',
                    ref: '../btnCreate'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Add to Portfolio',
                    ref: '../btnAddtoportfolio'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Delete',
                    ref: '../btnDelete'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View',
                    ref: '../btnView'
                }
            ]
        };
        AccountsPanelUi.superclass.initComponent.call(this);
    }
});
