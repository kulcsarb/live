window.tmpSelModel = new Ext.grid.CheckboxSelectionModel()
TradeManagementPanelUi = Ext.extend(Ext.ux.maximgb.tg.GridPanel, {
    title: 'Trade Management',
    master_column_id: 'id',
    selModel: window.tmpSelModel,
    columns: [
        window.tmpSelModel,
        {dataIndex: 'id', width: 70, id: 'id', header: 'Id'},
        {dataIndex: 'type', width: 70, header: 'Type'},
        {dataIndex: 'portfolio_name', width: 120, header: 'Portfolio'},
        {dataIndex: 'account_name', width: 120, header: 'Account'},
        {dataIndex: 'strategy_id', width: 55, header: 'StratId'},
        {dataIndex: 'group_id', width: 45, header: 'Group'},
        {dataIndex: 'state', width: 110, header: 'State'},
        {dataIndex: 'symbol', width: 60, header: 'Symbol'},
        {dataIndex: 'direction', width: 40, header: 'Dir.'},
        {dataIndex: 'amount', width: 65, header: 'Amount'},
        {dataIndex: 'current_amount', width: 75, sortable: false, header: 'Curr. amount'},
        {dataIndex: 'open_price', width: 65, sortable: false, header: 'Open price'},
        {dataIndex: 'open_fill_time', width: 111, xtype: 'datecolumn', format: 'Y-m-d H:i:s', sortable: false, header: 'Open fill time'},
        {dataIndex: 'close_price', width: 65, sortable: false, header: 'Close price'},
        {dataIndex: 'close_fill_time', width: 111, xtype: 'datecolumn', format: 'Y-m-d H:i:s', sortable: false, header: 'Close fill time'},
        {dataIndex: 'entry_price', width: 65, sortable: false, header: 'Entry price'},
        {dataIndex: 'fill_price', width: 55, sortable: false, header: 'Fill price'},
        {dataIndex: 'fill_time', width: 111, xtype: 'datecolumn', format: 'Y-m-d H:i:s', sortable: false, header: 'Fill time'}],
    stripeRows: true,
    initComponent: function() {
        this.store = TradeStore,
        this.tbar = {
            xtype: 'toolbar',
            buttonAlign: 'left',
            items: [
                {
                    xtype: 'button',
                    text: 'Repair',
                    ref: '../btnRepair'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnClose'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Mark as Closed',
                    ref: '../btnMarkasclosed'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Fix messages',
                    ref: '../btnFixmessages'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Send Open',
                    ref: '../btnSendopen'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Send Close',
                    ref: '../btnSendclose'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Place',
                    ref: '../btnPlace'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Mark as Filled',
                    ref: '../btnMarkasfilled'
                }
            ]
        };
        this.bbar = new Ext.ux.maximgb.tg.PagingToolbar({
            id: 'content-treepagingtoolbar',
            store: TradeStore
        });
        TradeManagementPanelUi.superclass.initComponent.call(this);
    }
});
delete window.tmpSelModel;
