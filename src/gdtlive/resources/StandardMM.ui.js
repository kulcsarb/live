/*
 * File: StandardMM.ui.js
 * Date: Mon Oct 10 2011 11:21:02 GMT+0200 (CEST)
 * 
 * This file was generated by Ext Designer version 1.1.2.
 * http://www.sencha.com/products/designer/
 *
 * This file will be auto-generated each and everytime you export.
 *
 * Do NOT hand edit this file.
 */

StandardMMUi = Ext.extend(Ext.Panel, {
//    height: 400,
    layout: 'form',
    hidden: true,
    labelWidth: 162,
    padding: 3,
    initComponent: function() {
        this.items = [
            {
                xtype: 'fieldset',
                title: 'Position',
                labelWidth: 160,
                ref: 'fsetPosition',
                items: [
                    {
                        xtype: 'numberfield',
                        anchor: '70%',
                        name: 'positionsize',
                        allowBlank: false,
                        fieldLabel: 'Position size',
                        blankText: 'Enter position size',
                        emptyText: 'Position size here',
                        allowNegative: false,
                        vtype: 'alphaCustom',
                        ref: '../fldPositionsize'
                    },
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        layoutConfig: {
                            padding: '0, 0, 5, 0'
                        },
                        items: [
                            {
                                xtype: 'radio',
                                boxLabel: 'Balance (%)',
                                width: 110,
                                name: 'positionmode',
                                ref: '../../radPostionunit0'
                            },
                            {
                                xtype: 'radio',
                                boxLabel: 'Monthly balance (%)',
                                name: 'positionmode',
                                width: 150,
                                ref: '../../radPostionunit1'
                            },
                            {
                                xtype: 'radio',
                                boxLabel: 'Base capital (%)',
                                name: 'positionmode',
                                width: 120,
                                ref: '../../radPostionunit2'
                            },
                            {
                                xtype: 'radio',
                                boxLabel: 'Dynamic',
                                name: 'positionmode',
                                ref: '../../radPostionunit3'
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'fieldset',
                title: 'Risk Management',
                checkboxToggle: true,
                labelWidth: 160,
                checkboxName: 'riskmanagement',
                ref: 'fsetRiskmanagement',
                items: [
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        layoutConfig: {
                            padding: '0, 0, 5, 0'
                        },
                        items: [
                            {
                                xtype: 'radio',
                                boxLabel: 'Max risk',
                                width: 100,
                                name: 'sltype',
                                ref: '../../radSltype0'
                            },
                            {
                                xtype: 'radio',
                                boxLabel: 'Pip distance',
                                width: 100,
                                name: 'sltype',
                                ref: '../../radSltype3'
                            },
                            {
                                xtype: 'radio',
                                boxLabel: 'Modify SL plugin',
                                width: 130,
                                name: 'sltype',
                                ref: '../../radSltype1'
                            },
                            {
                                xtype: 'radio',
                                boxLabel: 'Both',
                                name: 'sltype',
                                ref: '../../radSltype2'
                            }
                        ]
                    },
                    {
                        xtype: 'numberfield',
                        anchor: '70%',
                        fieldLabel: 'Max risk (%)',
                        maxLength: 4,
                        allowDecimals: false,
                        name: 'maxrisk',
                        blankText: 'Enter maximal risk',
                        emptyText: 'Maximal risk here',
                        allowNegative: false,
                        minValue: 1,
                        maxValue: 1000,
                        allowBlank: false,
                        ref: '../fldMaxrisk'
                    },
                    {
                        xtype: 'combo',
                        anchor: '70%',
                        fieldLabel: 'Modify SL plugin',
                        emptyText: 'Select plugin...',
                        store: 'PeakThroughPluginStore',
                        displayField: 'name',
                        valueField: 'id',
                        name: 'peakthroughplugin',
                        mode: 'local',
                        triggerAction: 'all',
                        blankText: 'Select plugin',
                        forceSelection: true,
                        allowBlank: false,
                        ref: '../cmbPeakthroughplugin'
                    },
                    {
                        xtype: 'numberfield',
                        anchor: '70%',
                        fieldLabel: 'Modify SL plugin parameter',
                        name: 'peakthroughparam',
                        blankText: 'Enter parameter',
                        emptyText: 'Parameter here',
                        minValue: 1,
                        ref: '../fldPeakthroughparam'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: 'Modify SL',
                        name: 'modifysl',
                        ref: '../chkModifysl'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: 'Use BE',
                        submitValue: false,
                        ref: '../chkUsebe'
                    },
                    {
                        xtype: 'numberfield',
                        anchor: '70%',
                        fieldLabel: 'BE threshold',
                        minValue: 1,
                        allowDecimals: false,
                        name: 'be_threshold',
                        blankText: 'Enter BE threshold',
                        emptyText: 'BE threshold here',
                        allowBlank: false,
                        ref: '../fldBethreshold'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: 'Use TP',
                        submitValue: false,
                        ref: '../chkUsetp'
                    },
                    {
                        xtype: 'numberfield',
                        anchor: '70%',
                        fieldLabel: 'RR rate',
                        minValue: 0,
                        maxValue: 10,
                        name: 'riskreward_rate',
                        blankText: 'Enter RR rate',
                        emptyText: 'RR rate here',
                        allowBlank: false,
                        ref: '../fldRrrate'
                    }
                ]
            },
            {
                xtype: 'numberfield',
                anchor: '70%',
                fieldLabel: 'Maximal Trade Number',
                minValue: 0,
                maxValue: 10000,
                allowDecimals: false,
                name: 'maxnumoftrades',
                blankText: 'Enter maximal number of trades',
                emptyText: 'Maximal number of trades here',
                allowBlank: false,
                ref: 'fldMaxtradenum'
            },
            {
                xtype: 'numberfield',
                anchor: '70%',
                minValue: 0,
                maxValue: 100,
                allowDecimals: false,
                fieldLabel: 'Margin Call at Balance (%)',
                name: 'margincall_rate',
                allowBlank: false,
                blankText: 'Enter Margin Call Rate',
                emptyText: 'Margin Call Rate here',
                ref: 'fldMargincallatbalance'
            }
        ];
        StandardMMUi.superclass.initComponent.call(this);
    }
});
