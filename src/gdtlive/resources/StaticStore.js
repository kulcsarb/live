StaticStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        StaticStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'StaticStore',
            autoSave: false,
            fields: [
                {
                    name: 'value'
                },
                {
                    name: 'display'
                },
                {
                    name: 'others'
                }
            ]
        }, cfg));
    }
});
copyStore('StaticStore', 'StaticStore2')
copyStore('StaticStore', 'StaticStore3')
copyStore('StaticStore', 'StaticStore4')
new StaticStore();