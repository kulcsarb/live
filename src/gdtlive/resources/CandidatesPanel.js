/**
 * @fileOverview
 * @author vendelin8
 * @version 0.3
 */
/**
 * candidate strategy management
 * @class CandidatesPanel
 * @lends CandidatesPanel.prototype
 */
CandidatesPanel = Ext.extend(CandidatesPanelUi, {
    mainTitle: 'Candidates',
    gridHeaders: ['Id', 'Name', 'Instruments', 'Timeframe', 'Individual', 'Evolved from', 'Evolved to', 'Live', 'Winning trades', 'Losing trades', 'Win-lose diff', 'Net profit'],
    btnAddtoportfolioText: 'Add to Portfolio',
    btnImportText: 'Import',
    btnExportText: 'Export',
    btnViewText: 'View',
    /** @constructs */
    initComponent: function() {
        CandidatesPanel.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        setGridColums(self, self.gridHeaders, 1);
        self.btnAddtoportfolio.text = self.btnAddtoportfolioText;
        self.btnImport.text = self.btnImportText;
        self.btnExport.text = self.btnExportText;
        self.btnView.text = self.btnViewText;

        self.rowSelectionPlugin = self.initPlugin(new Ext.ux.grid.RowSelectionPaging());

        /** candidate strategy store reference */
        self.candidatesStore = storeCRUD('CandidatesStore', self, self);
        /**
         * fires after the datarow data has been loaded or changed
         *  resets action buttons' enablity
         * @event
         */
        self.candidatesStore.on('load', function(store, records) {
            self.getSelectionModel().fireEvent('selectionchange');
            if ((records.length) && (gridCell === undefined)) {
                gridCell = self.getView().getCell(0, 2);
            }
        }, self);

        /**
         * fires when clicked on "Add to Portfolio" button
         *  shows portfolio chooser window
         * @event
         */
        self.btnAddtoportfolio.on('click', function() {
            var selected = self.rowSelectionPlugin.getSelections();
            if ((!selected) || (typeof(selected) !== 'object') || (selected.length < 1)) {
                return;
            }
            self.popup = new AddToPortfolio();
            self.popup.element_items = Ext.util.JSON.encode(collectIds(selected));
            self.popup.element_type = 'strategy';
            self.popup.show();
        }, self);

        /**
         * fires when clicked on "Import" button
         *  shows strategy import window where user can import exported strategies
         * @event
         */
        self.btnImport.on('click', function() {
            var popup = new StrategyImportWindow();
            popup.store = self.candidatesStore;
            popup.show();
        }, self);

         /**
         * fires when clicked on "Export" button
         *  shows "save as..." popup with the dns (dna) of the selected strategies
         * @event
         */
        self.btnExport.on('click', function() {
            var selected = self.rowSelectionPlugin.getSelections();
            if ((!selected) || (typeof(selected) !== 'object') || (selected.length < 1)) {
                return;
            }
            ids = Ext.util.JSON.encode(collectIds(selected));
            Ext.Ajax.request({
                url: urls.checkExport,
                params: {'ids': ids},
                success: function(response) {
                    response = ajaxSuccessHandle(response);
                    if (!response) {
                        return;
                    }
                    window.open(urls.strategyExport + '?ids=' + ids,'');
                },
                failure: function(response) {
                    response = ajaxErrorHandle(response, null, true);
                }
            });
        }, self);

        self.btnView.on('click', function() {
            var selected = self.rowSelectionPlugin.getSelections();
            if ((!selected) || (typeof(selected) !== 'object') || (selected.length < 1)) {
                return;
            }
            mainViewport.liveManagementP.addViews(selected, LIVE_PORTFOLIO);
            mainViewport.btnLivemanagement.fireEvent('click');
        }, self);

       /**
         * fires when the datarow grid's selection has been changed
         *  changes control buttons' state based on selected record's status
         * @event
         */
        self.getSelectionModel().on('selectionchange', function() {
            var record = self.rowSelectionPlugin.getSelected();
            var newVal = (record === undefined);
            self.btnAddtoportfolio.setDisabled(newVal);
            self.btnExport.setDisabled(newVal);
            self.btnView.setDisabled(newVal);
        }, self);

        /**
         * fires when the paging toolbar has been paged
         *  saves paging details to base params to be able to reload them when returning from another screen
         * @event
         */
        self.getBottomToolbar().on('change', function(tb, pageData) {
            self.rowSelectionPlugin.pagingEvent();
            if ((!self.lastBaseParams) || (self.destroying) || (self.isDestroyed) || (!self.isVisible())) {
                return;
            }
            self.lastBaseParams.start = (pageData.activePage - 1) * tb.pageSize;
        }, self);

        /**
         * fires when the panel is rendered
         *  sets store paging size
         * @event
         */
        self.on('afterrender', function() {
            setPageSize(self, function() {self.candidatesStore.load()});
        }, self);
    }
});
