/**
 * @fileOverview
 * @author vendelin8
 * @version 0.4
 */
/**
 * StandardMM: money management plugin no. 0
 * @class StandardMM
 * @lends StandardMM
 */
StandardMM = Ext.extend(StandardMMUi, {
    fsetPositionTitle: 'Position',
    fldPositionsizeLabel: 'Position size',
    fldPositionsizeBlank: 'Enter position size',
    fldPositionsizeEmpty: 'Position size here',
    fldPositionsizeVtype: 'The given format is not compatible with your choose',
    radPostionunit0Label: 'Balance (%)',
    radPostionunit1Label: 'Monthly balance (%)',
    radPostionunit2Label: 'Base capital (%)',
    radPostionunit3Label: 'Dynamic',
    fsetRiskmanagementTitle: 'Risk Management',
    radSltype0Label: 'Max risk',
    radSltype3Label: 'Pip distance',
    radSltype1Label: 'Modify SL plugin',
    radSltype2Label: 'Both',
    chkModifyslLabel: 'Modify SL',
    fldMaxriskLabel: 'Max risk (%)',
    fldPipdistanceLabel: 'Pip distance',
    fldMaxriskBlank: 'Enter maximal risk',
    fldMaxriskEmpty: 'Maximal risk here',
    cmbPeakthroughpluginLabel: 'Modify SL plugin',
    cmbPeakthroughpluginBlank: 'Select plugin',
    cmbPeakthroughpluginEmpty: 'Select plugin...',
    fldPeakthroughparamLabel: 'Modify SL plugin parameter',
    fldPeakthroughparamBlank: 'Enter parameter',
    fldPeakthroughparamEmpty: 'Parameter here',
    chkUsebeLabel: 'Use BE',
    fldBethresholdLabel: 'BE threshold',
    fldBethresholdBlank: 'Enter BE threshold',
    fldBethresholdEmpty: 'BE threshold here',
    chkUsetpLabel: 'Use TP',
    fldRrrateLabel: 'RR rate',
    fldRrrateBlank: 'Enter RR rate',
    fldRrrateEmpty: 'RR rate here',
    fldMaxtradenumLabel: 'Maximal Trade Number',
    fldMaxtradenumBlank: 'Enter maximal number of trades',
    fldMaxtradenumEmpty: 'Maximal number of trades here',
    fldMargincallatbalanceLabel: 'Margin Call at Balance (%)',
    fldMargincallatbalanceBlank: 'Enter Margin Call Rate',
    fldMargincallatbalanceEmpty: 'Margin Call Rate here',
    msgSlTpyeCantBeTS: 'SL type cannot be Trailing Stop plugin',
    /** @constructs */
    initComponent: function() {
        StandardMM.superclass.initComponent.call(this);
        var self = this;
        self.fsetPosition.title = self.fsetPositionTitle;
        self.fldPositionsize.fieldLabel = self.fldPositionsizeLabel;
        self.fldPositionsize.blankText = self.fldPositionsizeBlank;
        self.fldPositionsize.emptyText = self.fldPositionsizeEmpty;
        self.radPostionunit0.boxLabel = self.radPostionunit0Label;
        self.radPostionunit1.boxLabel = self.radPostionunit1Label;
        self.radPostionunit2.boxLabel = self.radPostionunit2Label;
        self.radPostionunit3.boxLabel = self.radPostionunit3Label;
        self.fsetRiskmanagement.title = self.fsetRiskmanagementTitle;
        self.radSltype0.boxLabel = self.radSltype0Label;
        self.radSltype1.boxLabel = self.radSltype1Label;
        self.radSltype2.boxLabel = self.radSltype2Label;
        self.radSltype3.boxLabel = self.radSltype3Label;
        self.chkModifysl.fieldLabel = self.chkModifyslLabel;
        self.fldMaxrisk.fieldLabel = self.fldPipdistanceLabel;
        self.fldMaxrisk.blankText = self.fldMaxriskBlank;
        self.fldMaxrisk.emptyText = self.fldMaxriskEmpty;
        self.cmbPeakthroughplugin.fieldLabel = self.cmbPeakthroughpluginLabel;
        self.cmbPeakthroughplugin.blankText = self.cmbPeakthroughpluginBlank;
        self.cmbPeakthroughplugin.emptyText = self.cmbPeakthroughpluginEmpty;
        self.fldPeakthroughparam.fieldLabel = self.fldPeakthroughparamLabel;
        self.fldPeakthroughparam.blankText = self.fldPeakthroughparamBlank;
        self.fldPeakthroughparam.emptyText = self.fldPeakthroughparamEmpty;
        self.chkUsebe.fieldLabel = self.chkUsebeLabel;
        self.fldBethreshold.fieldLabel = self.fldBethresholdLabel;
        self.fldBethreshold.blankText = self.fldBethresholdBlank;
        self.fldBethreshold.emptyText = self.fldBethresholdEmpty;
        self.chkUsetp.fieldLabel = self.chkUsetpLabel;
        self.fldRrrate.fieldLabel = self.fldRrrateLabel;
        self.fldRrrate.blankText = self.fldRrrateBlank;
        self.fldRrrate.emptyText = self.fldRrrateEmpty;
        self.fldMaxtradenum.fieldLabel = self.fldMaxtradenumLabel;
        self.fldMaxtradenum.blankText = self.fldMaxtradenumBlank;
        self.fldMaxtradenum.emptyText = self.fldMaxtradenumEmpty;
        self.fldMargincallatbalance.fieldLabel = self.fldMargincallatbalanceLabel;
        self.fldMargincallatbalance.blankText = self.fldMargincallatbalanceBlank;
        self.fldMargincallatbalance.emptyText = self.fldMargincallatbalanceEmpty;

        /** money management store reference */
        self.peakthroughPluginStore = lazyStoreR('PeakThroughPluginStore');

        /**
         * fires when clicked on position unit radio 0: 'Balance'
         * sets position size's format to 0 <= x <= 50000.00 (%)
         * @event
         */
        self.radPostionunit0.on('check', function(ch, checked) {
            if (checked) {
                self.fldPositionsize.vtypeParams = [5, 2];
                self.fldPositionsize.setMaxValue(50000);
                if (self.fldPositionsize.validateValue()) {
                    self.fldPositionsize.clearInvalid();
                }
            }
            self.positionModeChanged(0, checked);
        }, self);

        /**
         * fires when clicked on position unit radio 1: 'Monthly balance'
         * sets position size's format to 0 <= x <= 50000.00 (%)
         * @event
         */
        self.radPostionunit1.on('check', function(ch, checked) {
            if (checked) {
                self.fldPositionsize.vtypeParams = [5, 2];
                self.fldPositionsize.setMaxValue(50000);
                if (self.fldPositionsize.validateValue()) {
                    self.fldPositionsize.clearInvalid();
                }
            }
            self.positionModeChanged(1, checked);
        }, self);

        /**
         * fires when clicked on position unit radio 2: 'Fixed'
         * sets position size's format to 0 <= x <= 10^8
         * @event
         */
        self.radPostionunit2.on('check', function(ch, checked) {
            if (checked) {
                self.fldPositionsize.vtypeParams = [7, 0];
                self.fldPositionsize.setMaxValue(9999999);
                if (self.fldPositionsize.validateValue()) {
                    self.fldPositionsize.clearInvalid();
                }
            }
            self.positionModeChanged(2, checked);
        }, self);

        /**
         * fires when clicked on position unit radio 3: 'Dynamic'
         *  disables position size
         *  max risk is mandatory if position unit is dynamic or stop loss type has max risk 
         * @event
         */
        self.radPostionunit3.on('check', function(ch, checked) {
            self.fldPositionsize.setDisabled(checked);
            self.positionModeChanged(3, checked);
        }, self);

        /**
         * fires when clicked on stop-loss type radio 1: 'peak-through'
         *  max risk is mandatory if position unit is dynamic or stop loss type has max risk 
         * @event
         */
        self.radSltype1.on('check', function() {
            self.maxriskDisabled();
            self.peakThroughDisabled();
        }, self);

        /**
         * fires when clicked on stop-loss type radio 2: 'bith'
         *  max risk is mandatory if position unit is dynamic or stop loss type has max risk 
         * @event
         */
        self.radSltype2.on('check', function() {
            self.peakThroughDisabled();
        }, self);

        self.radSltype3.on('check', function(ch, checked) {
            if (checked) {
                self.peakthroughPluginStore.clearFilter();
            } else {
                if (typeof(self.cmbPeakthroughplugin.getValue()) != 'number') {
                    self.peakthroughPluginStore.filter('type', 0);
                    return;
                }
                if (self.peakthroughPluginStore.getById(self.cmbPeakthroughplugin.getValue()).get('type') == 1) {
                    if (self.chkModifysl.getValue()) {
                        self.radSltype3.setValue(true);
                        Ext.Msg.alert(locationStrings.failure, self.msgSlTpyeCantBeTS);
                    } else {
                        self.peakthroughPluginStore.filter('type', 0);
                        self.cmbPeakthroughplugin.setValue('');
                    }
                } else {
                    self.peakthroughPluginStore.filter('type', 0);
                }
            }
        }, self);


        self.chkModifysl.on('check', function() {
            self.peakThroughDisabled();
        }, self);

        /**
         * fires before the risk management fieldset is collapsed
         *  it is not possible if dynamic position size is active
         */
        self.fsetRiskmanagement.on('beforecollapse', function() {
            if (self.radPostionunit3.getValue()) {
                self.fsetRiskmanagement.checkbox.dom.checked = true;
                return false;
            }
        }, self);

        /**
         * fires when the "Use BE" checkbox is checked or unchecked
         *  enables/disables BE threshold field
         * @event
         */
        self.chkUsebe.on('check', function(chk, checked) {
            self.fldBethreshold.setDisabled(!checked);
            self.fldBethreshold.allowBlank = !checked;
            if (!checked) {
                self.fldBethreshold.clearInvalid();
            }
        }, self);

        /**
         * fires when the "Use TP" checkbox is checked or unchecked
         *  enables/disables RR rate field
         * @event
         */
        self.chkUsetp.on('check', function(chk, checked) {
            self.fldRrrate.setDisabled(!checked);
            self.fldRrrate.allowBlank = !checked;
            if (!checked) {
                self.fldRrrate.clearInvalid();
            }
        }, self);

        /**
         * fires after the component is rendered
         *  initiates form with default settings
         * @event
         */
        self.on('afterrender', function() {
            self.radSltype0.setDisabled(true);
            if (self.paramsFilled) {
                return;
            }
            self.radPostionunit2.setValue(true);
            self.fldMaxrisk.setValue(100);
            autoSelectLazy(self.cmbPeakthroughplugin, 1);
            self.fldPeakthroughparam.setValue(25);
            self.radSltype3.setValue(true);
            self.chkUsebe.setValue(false);
            self.fldBethreshold.setDisabled(true);
            self.fldBethreshold.setValue(50);
            self.chkUsetp.setValue(false);
            self.fldRrrate.setDisabled(true);
            self.fldRrrate.setValue(2.0);
            self.fldMaxtradenum.setValue(1);
            self.fldMargincallatbalance.setValue(50);
            self.paramsFilled = true;
            self.peakThroughDisabled();
            self.fldPositionsize.setValue(200);
        }, self);
    },

    /**
     * default plugin function for saving form field data
     * @param {Object} params the params collected from the forms
     * @return {Object} the changed params
     */
    setParams: function(params) {
        var self = this;
        if (!params.mmplugin_parameters) {
            params.mmplugin_parameters = {};
        } else {
            params.mmplugin_parameters = Ext.util.JSON.decode(params.mmplugin_parameters)
        }
        if (params.positionsize) {
            params.mmplugin_parameters.positionsize = params.positionsize;
            delete params.positionsize;
        } else {
            params.mmplugin_parameters.positionsize = 0;
        }
        var i;
        for (i = 0; i < 4; i++) {
            if (params.positionmode[i] === true) {
                params.mmplugin_parameters.positionmode = i;
                break;
            }
        }
        delete params.positionmode;
        params.mmplugin_parameters.sltype = getRadioValue(self, 'radSltype', 0, 4)
        delete params.sltype;
        params.mmplugin_parameters.riskmanagement = self.fsetRiskmanagement.checkbox.dom.checked ? 1 : 0;
        delete params.riskmanagement;
        params.mmplugin_parameters.modifysl = params.modifysl ? 1 : 0;
        delete params.modifysl;
        if (params.maxrisk) {
            params.mmplugin_parameters.maxrisk = params.maxrisk;
            delete params.maxrisk;
        } else {
            params.mmplugin_parameters.maxrisk = 0;
        }
        params.mmplugin_parameters.peakthroughplugin = params.peakthroughplugin;
        delete params.peakthroughplugin;
        params.mmplugin_parameters.peakthroughparam = params.peakthroughparam;
        delete params.peakthroughparam;
        params.mmplugin_parameters.setsl = 1;
        params.mmplugin_parameters.be_threshold = params.be_threshold;
        delete params.be_threshold;
        params.mmplugin_parameters.riskreward_rate = params.riskreward_rate;
        delete params.riskreward_rate;
        params.mmplugin_parameters.maxnumoftrades = params.maxnumoftrades;
        delete params.maxnumoftrades;
        params.mmplugin_parameters.margincall_rate = params.margincall_rate;
        delete params.margincall_rate;

        self.paramsFilled = true;
        params.mmplugin_parameters = Ext.util.JSON.encode(params.mmplugin_parameters);
        return params;
    },

    /**
     * default plugin function for filling the form fields from saved data
     * @param {Object} params the data containing the form data
     */
    fillForm: function(params) {
        var self = this;
        if (typeof(params) === 'string') {
            params = Ext.util.JSON.decode(params);
        }
        setRadioValue(self, 'radPostionunit', parseInt(params.positionmode));
        setRadioValue(self, 'radSltype', parseInt(params.sltype));
        self.fldMaxrisk.setValue((params.maxrisk === undefined) ? '' : params.maxrisk);
        if (params.peakthroughplugin) {
            autoSelectLazy(self.cmbPeakthroughplugin, params.peakthroughplugin);
        } else {
            self.cmbPeakthroughplugin.setValue('');
        }
        self.fldPeakthroughparam.setValue(params.peakthroughparam);
        self.fldBethreshold.setValue((params.be_threshold === undefined) ? '' : params.be_threshold);
        self.chkUsebe.setValue(params.be_threshold !== undefined);
        if (self.chkUsebe.fireEvent) {
            self.chkUsebe.fireEvent('check', self.chkUsebe, params.be_threshold !== undefined);
        } else {
            self.be_threshold = params.be_threshold;
            self.chkUsebe.on('afterrender', self.onchkUsebeRender, self);
        }
        self.fldRrrate.setValue((params.riskreward_rate === undefined) ? '' : params.riskreward_rate);
        self.chkUsetp.setValue(params.riskreward_rate !== undefined);
        if (self.chkUsetp.fireEvent) {
            self.chkUsetp.fireEvent('check', self.chkUsetp, params.riskreward_rate !== undefined);
        } else {
            self.riskreward_rate = params.riskreward_rate;
            self.chkUsetp.on('afterrender', self.onchkUsetpRender, self);
        }
        if (self.chkModifysl.fireEvent) {
            self.chkModifysl.setValue(params.modifysl);
            self.chkModifysl.fireEvent('check', self.chkModifysl, params.modifysl);
        } else {
            self.modifysl = params.modifysl;
            self.chkModifysl.on('afterrender', self.onchkModifyslRender, self);
        }
        self.fldMaxtradenum.setValue((params.maxnumoftrades === undefined) ? '' : params.maxnumoftrades);
        if (self.fsetRiskmanagement.checkbox) {
//            self.fsetRiskmanagement.checkbox.dom.checked = !!params.riskmanagement;
            if (params.riskmanagement) {
                self.fsetRiskmanagement.expand();
            } else {
                self.fsetRiskmanagement.collapse();
            }
        } else {
            self.riskmanagement = params.riskmanagement ? true : false;
            self.fsetRiskmanagement.on('afterrender', self.onFsetRiskmanagementRender, self);
        }
        self.fldMargincallatbalance.setValue(params.margincall_rate);
        self.paramsFilled = true;
        self.peakThroughDisabled();
        self.maxriskDisabled();
        self.fldPositionsize.setValue(params.positionsize);
    },

    onFsetRiskmanagementRender: function() {
        var self = this;
//        self.fsetRiskmanagement.checkbox.dom.checked = self.riskmanagement;
        if (self.riskmanagement) {
            self.fsetRiskmanagement.expand();
        } else {
            self.fsetRiskmanagement.collapse();
        }
        delete self.riskmanagement;
//        self.fsetRiskmanagement.onCheckClick();
        self.fsetRiskmanagement.un('afterrender', self.onFsetRiskmanagementRender, self);
    },

    onchkUsebeRender: function() {
        var self = this;
        self.chkUsebe.fireEvent('check', self.chkUsebe, self.be_threshold !== undefined);
        delete self.be_threshold;
        self.chkUsebe.un('afterrender', self.onchkUsebeRender, self);
    },

    onchkUsetpRender: function() {
        var self = this;
        self.chkUsetp.fireEvent('check', self.chkUsetp, self.riskreward_rate !== undefined);
        delete self.riskreward_rate;
        self.chkUsetp.un('afterrender', self.onchkUsetpRender, self);
    },

    onchkModifyslRender: function() {
        var self = this;
        self.chkModifysl.setValue(self.modifysl);
        self.chkModifysl.fireEvent('check', self.chkModifysl, self.modifysl);
        delete self.modifysl;
        self.chkModifysl.un('afterrender', self.onchkModifyslRender, self);
    },

    peakThroughDisabled: function() {
        var self = this;
        var newVal = !(self.chkModifysl.getValue() || self.radSltype1.getValue() || self.radSltype2.getValue());
        self.cmbPeakthroughplugin.setDisabled(newVal);
        self.fldPeakthroughparam.setDisabled(newVal);
        self.cmbPeakthroughplugin.allowBlank = newVal;
        self.fldPeakthroughparam.allowBlank = newVal;
        if (newVal) {
            self.cmbPeakthroughplugin.clearInvalid();
            self.fldPeakthroughparam.clearInvalid();
        }
    },

    positionModeChanged: function(newVal, checked) {
        var self = this;
        if (checked) {
            var dynamic = (newVal === 3);
            if (dynamic) {
                self.fldPositionsize.setValue(self.fldPositionsize.getValue() * 10);
                self.fldMaxrisk.setFieldLabel(self.fldMaxriskLabel);
                self.fsetRiskmanagement.expand();
                self.peakthroughPluginStore.filter('type', 0);
                self.radSltype1.setValue(true);
            } else {
                self.fldPositionsize.setValue(self.fldPositionsize.getValue() / 10);
                self.fldMaxrisk.setFieldLabel(self.fldPipdistanceLabel);
                self.peakthroughPluginStore.clearFilter();
                self.radSltype3.setValue(true);
            }
            self.radSltype2.setDisabled(dynamic);
            self.radSltype3.setDisabled(dynamic);
        } else  {
            if (newVal === 3) {
                self.fldPositionsize.setValue(self.fldPositionsize.getValue() / 10);
            } else {
                self.fldPositionsize.setValue(self.fldPositionsize.getValue() * 10);
            }
        }
        self.maxriskDisabled();
    },

    maxriskDisabled: function() {
        var self = this;
        var newVal = (self.radSltype1.getValue()) && (!self.radPostionunit3.getValue());
        self.fldMaxrisk.setDisabled(newVal);
        self.fldMaxrisk.allowBlank = newVal;
        if (newVal) {
            self.fldMaxrisk.clearInvalid();
        } else {
            if (self.radPostionunit3.getValue()) {  //max risk
                self.fldMaxrisk.decimalPrecision = 2;
                self.fldMaxrisk.allowDecimals = true;
                self.fldMaxrisk.minValue = 0.01;
                self.fldMaxrisk.maxValue = 50;
            } else {                                //pip distance
                self.fldMaxrisk.allowDecimals = false;
                self.fldMaxrisk.minValue = 1;
                self.fldMaxrisk.maxValue = 1000;
            }
            self.fldMaxrisk.validate();
        }
    }
});
