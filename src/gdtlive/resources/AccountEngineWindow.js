/**
 * @fileOverview
 * @author vendelin8
 * @version 0.11
 */
/**
 * Account engine log and test
 * @class AccountEngineWindow
 * @lends AccountEngineWindow.prototype
 */
AccountEngineWindow = Ext.extend(AccountEngineWindowUi, {
    btnRefreshText: 'Refresh',
    btnConnectText: 'Connect',
    btnbtnDisconnectText: 'Disconnect',
    detailsTitle: 'Details',
    logTitle: 'Log',
    radLogtype0Label: 'Debug',
    radLogtype1Label: 'Info',
    radLogtype2Label: 'Error',
    messageTitle: 'Messages',
    /** @constructs */
    initComponent: function() {
        AccountEngineWindow.superclass.initComponent.call(this);
        var self = this;
        self.btnRefresh.text = self.btnRefreshText;
        self.btnConnect.text = self.btnConnectText;
        self.btnDisconnect.text = self.btnbtnDisconnectText;
        self.tabDetails.setTitle(self.detailsTitle);
        self.tabLog.setTitle(self.logTitle);
        self.radLogtype0.boxLabel = self.radLogtype0Label;
        self.radLogtype1.boxLabel = self.radLogtype1Label;
        self.radLogtype2.boxLabel = self.radLogtype2Label;
        self.tabMessages.setTitle(self.messageTitle);

        self.tabDetails.setUrl(urls.accountEngineDetails);
        self.tabMessages.setUrl(urls.accountEngineMessages);

        for (i = 0; i < 3; i++) {
            self['radLogtype' + i].on('check', function(combo, checked) {
                if (checked) {
                    self.loadLog();
                }
            }, self);
        }

        self.btnRefresh.on('click', function() {
            self.tabPanel.getActiveTab().fireEvent('show');
        }, self);

        self.createTypedButtonAction(self.btnConnect, urls.accountEngineConnect);
        self.createTypedButtonAction(self.btnDisconnect, urls.accountEngineDisconnect);

        self.tabDetails.on('show', function() {
            self.tabDetails.toReload = true;
        }, self);

        self.tabLog.on('show', function() {
            self.loadLog();
        }, self);

        self.tabMessages.on('show', function() {
            self.tabMessages.toReload = true;
        }, self);

        self.on('render', function() {
            var params = {'account_id': self.account_id, 'portfolio_id': self.portfolio_id};
            self.tabDetails.setParams(params);
            self.tabMessages.setParams(params);
        }, self);
    },

    loadLog: function() {
        var self = this;
        var logType = self.radLogtype0.getValue() ? 0 : (self.radLogtype1.getValue() ? 1 : 2);
        Ext.Ajax.request({
            url: urls.accountEngineLog,
            params: {'account_id': self.account_id, 'portfolio_id': self.portfolio_id, logtype: logType},
            success: function(response) {
                response = ajaxSuccessHandle(response);
                if (!response) {
                    return;
                }
                self.areaLog.setValue(response.msg);
            },
            failure: function(response) {
                ajaxErrorHandle(response);
            }
        });
    },

    createTypedButtonAction: function(button, url) {
        var self = this;
        button.on('click', function() {
            Ext.Ajax.request({
                url: url,
                params: {'account_id': self.account_id, 'portfolio_id': self.portfolio_id},
                success: function(response) {
                    response = ajaxSuccessHandle(response);
                    if (!response) {
                        return;
                    }
                    Ext.Msg.alert(locationStrings.success, response.msg);
                },
                failure: function(response) {
                    response = ajaxErrorHandle(response, null, true);
                }
            });
        }, self);
    }
});
