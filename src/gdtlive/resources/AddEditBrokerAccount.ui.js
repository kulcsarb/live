AddEditBrokerAccountUi = Ext.extend(Ext.Window, {
    title: 'Add/Edit Broker Account',
    width: 600,
    modal: true,
    labelWidth: 120,
    autoHeight: true,
    resizable: false,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                monitorValid: true,
                labelWidth: 130,
                ref: 'formBase',
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Broker name',
                        store: 'BrokerNameStore',
                        allowBlank: false,
                        displayField: 'brokername',
                        valueField: 'broker_id',
                        emptyText: 'Broker name here',
                        blankText: 'Enter broker name',
                        triggerAction: 'all',
                        mode: 'local',
                        forceSelection: true,
                        ref: '../cmbBrokername'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Account name',
                        anchor: '85%',
                        vtype: 'alphanum',
                        allowBlank: false,
                        blankText: 'Enter account name',
                        emptyText: 'Account name here',
                        ref: '../fldAccountname'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Username',
                        anchor: '85%',
                        vtype: 'alphanum',
                        allowBlank: true,
                        blankText: 'Enter username',
                        emptyText: 'Username here',
                        ref: '../fldUsername'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Account number',
                        anchor: '85%',
                        ref: '../fldAccountnumber'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Password',
                        anchor: '85%',
                        blankText: 'Enter password',
                        emptyText: 'Password here',
                        inputType: 'password',
                        enableKeyEvents: true,
                        ref: '../fldPassword'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Confirm password',
                        anchor: '85%',
                        blankText: 'Retype password',
                        emptyText: 'Password here',
                        inputType: 'password',
                        vtype: 'password',
                        ref: '../fldConfirmpassword'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Sender Comp Id',
                        anchor: '85%',
                        blankText: 'Enter Sender Comp Id',
                        emptyText: 'Sender Comp Id here',
                        allowBlank: false,
                        ref: '../fldSendercompid'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Target Comp Id',
                        anchor: '85%',
                        blankText: 'Enter Target Comp Id',
                        emptyText: 'Target Comp Id here',
                        allowBlank: false,
                        ref: '../fldTargetcompid'
                    },
                    {
                        xtype: 'numberfield',
                        fieldLabel: 'Base capital (USD)',
                        anchor: '60%',
                        allowBlank: false,
                        minLength: 3,
                        allowDecimals: false,
                        blankText: 'Enter base capital',
                        emptyText: 'Base capital here',
                        ref: '../fldBasecapital'
                    },
                    {
                        xtype: 'numberfield',
                        fieldLabel: 'Balance',
                        anchor: '60%',
                        allowBlank: false,
                        blankText: 'Enter balance',
                        emptyText: 'Balance here',
                        ref: '../fldBalance'
                    },
                    {
                        xtype: 'numberfield',
                        fieldLabel: 'Leverage',
                        anchor: '60%',
                        allowBlank: false,
                        allowDecimals: false,
                        blankText: 'Enter leverage',
                        emptyText: 'Leverage here',
                        ref: '../fldLeverage'
                    },
                    {
                        xtype: 'numberfield',
                        fieldLabel: 'Risk multiplier',
                        anchor: '60%',
                        allowBlank: false,
                        blankText: 'Enter risk multiplier',
                        emptyText: 'Risk multiplier here',
                        allowNegative: false,
                        minValue: 0.1,
                        maxValue: 10,
                        ref: '../fldRiskmultiplier'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Server url',
                        anchor: '85%',
                        blankText: 'Enter Server url',
                        emptyText: 'Server url here',
                        allowBlank: false,
                        ref: '../fldServerurl'
                    },
                    {
                        xtype: 'numberfield',
                        fieldLabel: 'Server port',
                        anchor: '60%',
                        allowBlank: false,
                        allowDecimals: false,
                        blankText: 'Enter Server port',
                        emptyText: 'Server port here',
                        ref: '../fldServerport'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: 'Use ssl',
                        ref: '../chkUsessl'
                    }
                ],
                fbar: {
                    xtype: 'toolbar',
                    buttonAlign: 'left',
                    items: [
                        {
                            xtype: 'button',
                            text: 'Save',
                            formBind: true,
                            type: 'submit',
                            ref: '../../btnSave'
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel',
                            ref: '../../btnCancel'
                        },
                        {
                            xtype: 'button',
                            text: 'Test connection',
                            ref: '../../btnTestconnection'
                        }
                    ]
                }
            }
        ];
        AddEditBrokerAccountUi.superclass.initComponent.call(this);
    }
});
