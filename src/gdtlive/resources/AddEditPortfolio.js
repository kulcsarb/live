/**
 * @fileOverview
 * @author vendelin8
 * @version 0.4
 */
/**
 * add new or edit existing portfolio details
 * @class AddEditPortfolio
 * @lends AddEditPortfolio.prototype
 */
AddEditPortfolio = Ext.extend(AddEditPortfolioUi, {
    mainTitle: 'Add/Edit Portfolio',
    fldNameLabel: 'Name',
    fldNameBlank: 'Enter Portfolio name',
    fldNameEmpty: 'Portfolio name here',
    fldDescriptionLabel: 'Description',
/*    cmbManagementtypeLabel: 'Management type',
    cmbManagementtypeBlank: 'Select Management type',
    cmbManagementtypeEmpty: 'Select Management type...',
    cmbMMpluginLabel: 'Money management plugin',
    cmbMMpluginBlank: 'Select MM plugin',
    cmbMMpluginEmpty: 'Select MM plugin...',
    fsetMmpluginsettingsTitle: 'Money Management plugin settings',*/
    btnSaveText: 'Save',
    btnCancelText: 'Cancel',
    /** @constructs */
    initComponent: function() {
        AddEditPortfolio.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        self.fldName.fieldLabel = self.fldNameLabel;
        self.fldName.blankText = self.fldNameBlank;
        self.fldName.emptyText = self.fldNameEmpty;
        self.fldDescription.fieldLabel = self.fldDescriptionLabel;
/*        self.cmbManagementtype.fieldLabel = self.cmbManagementtypeLabel;
        self.cmbManagementtype.blankText = self.cmbManagementtypeBlank;
        self.cmbManagementtype.emptyText = self.cmbManagementtypeEmpty;
        self.cmbMMplugin.fieldLabel = self.cmbMMpluginLabel;
        self.cmbMMplugin.blankText = self.cmbMMpluginBlank;
        self.cmbMMplugin.emptyText = self.cmbMMpluginEmpty;
        self.fsetMmpluginsettings.title = self.fsetMmpluginsettingsTitle;*/
        self.btnSave.text = self.btnSaveText;
        self.btnCancel.text = self.btnCancelText;

//        /** money management store reference */
//        self.mMPluginStore = lazyStoreR('MMPluginStore');
        /** broker api name store reference */
        self.managementTypeStore = Ext.StoreMgr.lookup('ManagementTypeStore');
        self.managementTypeStore.on('load', self.managementTypeStoreLoad, self);

//        /**
//         * fires when an option is selected from money management plugins' combo box
//         * loads the gui element of the plugin to the form, or an empty panel if it does not exist
//         * @event
//         */
//        self.cmbMMplugin.on('select', function(combo, record) {
//            if (record === undefined) {
//                self.mmPlugin = null;
//                return;
//            }
//            if (!classExist(record.get('name'))) {
//                self.mmPlugin = new Ext.Panel();
//                self.mmPlugin.fillForm = function() {};
//                self.mmPlugin.setParams = function(params) {
//                    params.mmplugin_parameters = '{}';
//                    return params;
//                };
//            } else {
//                self.mmPlugin = eval('new ' + record.get('name') + '();');
//            }
//            self.fsetMmpluginsettings.removeAll();
//            self.fsetMmpluginsettings.add(self.mmPlugin);
//            if (self.mmplugin_parameters) {
//                self.mmPlugin.fillForm(self.mmplugin_parameters);
//                delete self.mmplugin_parameters;
//            }
//            self.mmPlugin.show();
//            self.fsetMmpluginsettings.doLayout();
//        }, self);

        /**
         * called when clicked on "Save" button
         *  update the existing, or creates the new broker account
         * @event
         */
        self.btnSave.on('click', function() {
//            var mmplugin_parameters = self.mmPlugin.setParams(self.formBase.getForm().getFieldValues()).mmplugin_parameters;
//            var decoded = Ext.util.JSON.decode(mmplugin_parameters);
//            mmplugin_parameters = Ext.util.JSON.encode(decoded);
            var store;
            if (self.record) {    //update existing
                self.record.set('name', self.fldName.getValue());
                self.record.set('description', self.fldDescription.getValue());
                self.record.set('management_type', self.cmbManagementtype.getValue());
//                self.record.set('mmplugin_id', self.cmbMMplugin.getValue());
//                self.record.set('mmplugin_parameters', mmplugin_parameters);
                store = self.record.store;
            } else {                //create new
                store = Ext.StoreMgr.lookup('PortfolioStore');
                self.record = new store.recordType({
                    'id' : 0,
                    'name' : self.fldName.getRawValue(),
                    'description' : self.fldDescription.getValue(),
                    'management_type' : self.cmbManagementtype.getValue()/*,
                    'mmplugin_id': self.cmbMMplugin.getValue(),
                    'mmplugin_parameters': mmplugin_parameters*/
                });
                store.add(self.record);
            }
            if (store.save() === -1) {
                self.close();
            }
        }, self);

        /**
         * called when clicked on "Cancel" button
         *  closes the window
         * @event
         */
        self.btnCancel.on('click', function() {self.close();}, self);

        /**
         * fires when the window is shown
         *  fills the window's fields in case of edit existing element
         * @event
         */
        self.on('show', function() {
            self.managementTypeStore.load()
//            self.cmbMMplugin.reset();
//            self.mmPlugin = null;
//            self.fsetMmpluginsettings.removeAll();
            if (self.record) {
                self.fldName.setValue(self.record.get('name'));
                self.fldDescription.setValue(self.record.get('description'));
//                self.mmplugin_parameters = Ext.util.JSON.decode(self.record.get('mmplugin_parameters'));
//                autoSelectLazy(self.cmbMMplugin, self.record.get('mmplugin_id'));
            } else {
//                autoSelectLazy(self.cmbMMplugin, 1);
            }
        }, self);
    },

    managementTypeStoreLoad: function(store, records) {
        var self = this;
        var recordId = 1;
        if (self.record) {
            var selfRecordId = self.record.get('id');
            for (var i in records) {
                if (typeof(records[i]) === 'function') {
                    continue;
                }
                if (selfRecordId === records[i].get('id')) {
                    recordId = records[i].get('id');
                    break;
                }
            }
        }
        self.cmbManagementtype.setValue(recordId);
        self.managementTypeStore.un('load', self.managementTypeStoreLoad, self);
    }
});
