/**
 * @fileOverview
 * @author vendelin8, nicolas.vigh
 * @version 0.19
 */
/**
 * the user can set new password in this window
 * @class LoginWindow
 * @lends RenewPasswordWindow.prototype
 */
RenewPasswordWindow = Ext.extend(RenewPasswordWindowUi, {    
    mainTitle: 'Renew your password?',
    renewSuccess: 'Password has been changed successfully',
    renewFailure: 'Password changing failed!',
    renewHtml: 'To renew your password, type the new password  that you will use in GDTLIVE.<br/><br/>',
    fldPasswordLabel: 'New password',
    fldPasswordBlank: 'Enter your password',
    fldPasswordEmpty: 'Your password here',
    fldConfirmLabel: 'Retype password',
    fldConfirmBlank: 'Retype your password',
    fldConfirmEmpty: 'Your password here',
    btnSubmitText: 'Submit',
    btnCancelText: 'Cancel',
    /** @constructs */
    initComponent: function() {
        RenewPasswordWindow.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        self.description.html = self.renewHtml;
        self.fldPassword.fieldLabel = self.fldPasswordLabel;
        self.fldPassword.blankText = self.fldPasswordBlank;
        self.fldPassword.emptyText = self.fldPasswordEmpty;
        self.fldConfirm.fieldLabel = self.fldConfirmLabel;
        self.fldConfirm.blankText = self.fldConfirmBlank;
        self.fldConfirm.emptyText = self.fldConfirmEmpty;
        self.btnSubmit.text = self.btnSubmitText;
        self.btnCancel.text = self.btnCancelText;

        //param renewId got from url params list (GET)
        var params = Ext.urlDecode(location.search.substring(1));
        /** hidden variable to store the id that the user got in email to identify session */
        self.fldRenewid.setValue(params.renewId);

        /**
         * functionality after "Send" click
         *  renewed password is sent to the server
         *      redirect to login on success
         *      error message on fail
         * @event
         */
        self.btnSubmit.on('click', function() {
            self.formRenew.getForm().submit({waitTitle: locationStrings.waitTitle,
                waitMsg: locationStrings.waitMsg,
                success: function(form, action) {
                    submitSuccess(action, self.renewSuccess, function() {
                        window.location = '/';
                    });
                },
                failure: function(form, action) {
                    submitErrorHandle(action, self.renewFailure);
                }});
        }, self);

        /**
         * functionality after "Cancel" click
         *  redirect to login on success
         * @event
         */
        self.btnCancel.on('click', function() {
            window.location = '/';
        }, self);
    }
});
