/**
 * @fileOverview
 * @author vendelin8
 * @version 0.11
 */
/**
 * admin rollover configuration functionality
 * @class AdminRolloverConfigWindow
 * @lends AdminRolloverConfigWindow.prototype
 */
AdminRolloverConfigWindow = Ext.extend(AdminRolloverConfigWindowUi, {
    mainTitle: 'Rollover Configuration',
    fsetConfigTitle: 'Rollover Configs',
    btnAddconfigText: 'Add config',
    btnEditconfigText: 'Edit config',
    btnDeleteconfigText: 'Delete config',
    configHeaders: ['Config name'],
    fsetRowTitle: 'Details of selected rollover config',
    btnAddrowText: 'Add currency',
    btnEditrowText: 'Edit currency',
    btnDeleterowText: 'Delete currency',
    rowHeaders: ['Currency', 'Base rate'],
    deleteConfigTitle: 'Delete Rollover Config?',
    deleteConfigHtml: 'The selected <b>Rollover Config</b> is going to be deleted! Do you want to perform it?',
    deleteRowTitle: 'Delete Rollover Config Row?',
    deleteRowHtml: 'The selected <b>Rollover Config Row</b> is going to be deleted! Do you want to perform it?',
    lastSelConfigId: null,
    /** @constructs */
    initComponent: function() {
        AdminRolloverConfigWindow.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        self.fsetConfig.title = self.fsetConfigTitle;
        self.btnAddconfig.text = self.btnAddconfigText;
        self.btnEditconfig.text = self.btnEditconfigText;
        self.btnDeleteconfig.text = self.btnDeleteconfigText;
        setGridColums(self.gridConfig, self.configHeaders, 1);
        self.fsetRow.title = self.fsetRowTitle;
        self.btnAddrow.text = self.btnAddrowText;
        self.btnEditrow.text = self.btnEditrowText;
        self.btnDeleterow.text = self.btnDeleterowText;
        setGridColums(self.gridRow, self.rowHeaders, 1);

        /** rollover config store reference */
        self.configStore = storeCRUD('AdminRolloverConfigStore', self, self.gridConfig);
        /**
         * fires after loading the store
         *  resets the buttons enablity
         * @event
         */
        self.configStore.on('load', function() {
            self.gridConfig.getSelectionModel().fireEvent('selectionchange');
            self.gridRow.getSelectionModel().fireEvent('selectionchange');
        }, self);
        
        /** rollover config row store reference */
        self.rowStore = storeCRUD('AdminRolloverRowStore', self, self.gridRow);

        /** currency store reference */
        self.currencyStore = lazyStoreR('CurrencyStore');

        /**
         * fires when user clicks on 'Add config' button
         *  shows config edit window with empty record
         * @event
         */
        self.btnAddconfig.on('click', function() {
            self.popup = new AdminRolloverConfigEditWindow();
            self.popup.record = null;
            self.popup.show();
        });

        /**
         * fires when config grid's selection changes
         *  loads stored row to the row grid and disables/enables config modify buttons
         * @event
         */
        self.gridConfig.getSelectionModel().on('selectionchange', function() {
            var record = self.gridConfig.getSelectionModel().getSelected();
            var newVal = (record === undefined);
            if (newVal) {
                self.lastSelConfigId = null;
                self.rowStore.removeAll();
            } else if (record.get('id') !== self.lastSelConfigId) {
                self.lastSelConfigId = record.get('id');
                self.rowStore.load({params: {'id': record.get('id')}});
            }
            self.btnEditconfig.setDisabled(newVal);
            self.btnDeleteconfig.setDisabled(newVal);
            self.btnAddrow.setDisabled(newVal);
        });

        /**
         * fires when user clicks on 'Edit config' button
         *  shows a window for editing selected config with the related record
         * @event
         */
        self.btnEditconfig.on('click', function() {
            var record = self.gridConfig.getSelectionModel().getSelected();
            if (record === undefined) {
                return;
            }
            self.popup = new AdminRolloverConfigEditWindow();
            self.popup.record = record;
            self.popup.show();
        });

        /**
         * fires when user clicks on 'Delete config' button
         *  shows a window for confirmation and deletes selected config if asked for
         * @event
         */
        self.btnDeleteconfig.on('click', function() {
            confirmDestroyRecordSafe(self, self.gridConfig, self.deleteConfigHtml, self.deleteConfigTitle);
        });

        /**
         * fires when user clicks on 'Add row' button
         *  shows row edit window with empty record and stored config id
         * @event
         */
        self.btnAddrow.on('click', function() {
            var record = self.gridConfig.getSelectionModel().getSelected();
            if (record === undefined) {
                return;
            }
            self.popup = new AdminRolloverRowEditWindow();
            self.popup.record = null;
            self.popup.configId = record.get('id');
            self.popup.show();
        });

        /**
         * fires when row grid's selection changes
         *  disables/enables row modify buttons
         * @event
         */
        self.gridRow.getSelectionModel().on('selectionchange', function() {
            var newVal = (self.gridRow.getSelectionModel().getSelected() === undefined);
            self.btnEditrow.setDisabled(newVal);
            self.btnDeleterow.setDisabled(newVal);
        });

        /**
         * fires when user clicks on 'Edit row' button
         *  shows a window for editing selected row with the related record
         * @event
         */
        self.btnEditrow.on('click', function() {
            var record = self.gridRow.getSelectionModel().getSelected();
            if (record === undefined) {
                return;
            }
            self.popup = new AdminRolloverRowEditWindow();
            self.popup.record = record;
            self.popup.configId = record.get('configId');
            self.popup.show();
        });

        /**
         * fires when user clicks on 'Delete row' button
         *  shows a window for confirmation and deletes selected row if asked for
         * @event
         */
        self.btnDeleterow.on('click', function() {
            confirmDestroyRecordSafe(self, self.gridRow, self.deleteRowHtml, self.deleteRowTitle);
        });

        /**
         * fires when the window is shown
         *  (re) loads the config store
         * @event
         */
        self.on('show', function() {
            self.configStore.load();
        }, self);
    }
});
