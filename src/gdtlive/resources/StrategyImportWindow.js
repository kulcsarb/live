/**
 * @fileOverview
 * @author vendelin8
 * @version 0.2
 */
/**
 * a modal window where the user can browse for an exported strategy to import
 * @class StrategyImportWindow
 */
StrategyImportWindow = Ext.extend(StrategyImportWindowUi, {
    mainTitle: 'Choose file...',
    mainHtml: 'Choose a file of exported strategies:',
    fldFileBlank: 'Browse file...',
    fldFileEmpty: 'Browse file...',
    btnOkText: 'OK',
    btnCancelText: 'Cancel',
    /** @constructs */
    initComponent: function() {
        StrategyImportWindow.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        self.description.html = self.mainHtml;
        self.btnOk.text = self.btnOkText;
        self.btnCancel.text = self.btnCancelText;

        /**
         * a file upload component
         */
        self.fldFile = new Ext.ux.form.FileUploadField({'blankText': self.fldFileBlank,
            'emptyText': self.fldFileEmpty,
            'anchor': '94%',
            'allowBlank': false});
        self.fldFile.name = 'exportTxt';
        self.formFile.add(self.fldFile);

        /**
         * fires when clicked on "OK" button
         *  starts uploading
         * @event
         */
        self.btnOk.on('click', function() {
            self.formFile.getForm().submit({waitTitle: locationStrings.waitTitle,
                waitMsg: locationStrings.waitMsg,
                success: function(form, action) {
                    self.store.reload();
                    self.close();
                },
                failure: function(form, action) {
                    submitErrorHandle(action);
                }});
        }, self);

        /**
         * fires when clicked on "Cancel"
         *  closes the window
         * @event
         */
        self.btnCancel.on('click', function() {self.close();}, self);

        /**
         * fires after the window is rendered
         *  sets default parameters to send
         * @event
         */
        self.on('afterrender', function() {
            self.fldUserid.setValue(currentUser.id);
        }, self);
    }
});
