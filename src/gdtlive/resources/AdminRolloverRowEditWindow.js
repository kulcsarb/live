/**
 * @fileOverview
 * @author vendelin8
 * @version 0.5
 */
/**
 * admin rollover row edit functionality
 * @class AdminRolloverRowEditWindow
 * @lends AdminRolloverRowEditWindow.prototype
 */
AdminRolloverRowEditWindow = Ext.extend(AdminRolloverRowEditWindowUi, {
    mainTitle: 'Edit Rollover Config Row',
    cmbCurrencyLabel: 'Currency',
    cmbCurrencyBlank: 'Select currency',
    cmbCurrencyEmpty: 'Select currency...',
    fldInterestLabel: 'Base interest rate (%)',
    fldInterestBlank: 'Enter interest rate',
    fldInterestEmpty: 'Interest rate here',
    btnSaveText: 'Save',
    btnCancelText: 'Cancel',
    /** @constructs */
    initComponent: function() {
        AdminRolloverRowEditWindow.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        self.cmbCurrency.fieldLabel = self.cmbCurrencyLabel;
        self.cmbCurrency.blankText = self.cmbCurrencyBlank;
        self.cmbCurrency.emptyText = self.cmbCurrencyEmpty;
        self.fldInterest.fieldLabel = self.fldInterestLabel;
        self.fldInterest.blankText = self.fldInterestBlank;
        self.fldInterest.emptyText = self.fldInterestEmpty;
        self.btnSave.text = self.btnSaveText;
        self.btnCancel.text = self.btnCancelText;

        /**
         * fires when user clicks on 'Send' button
         *  saves new settings to rollover row, or create a new one 
         * @event
         */
        self.btnSave.on('click', function() {
            var rowStore = Ext.StoreMgr.lookup('AdminRolloverRowStore');
            var record;
            if ((!self.record) || (self.record.get('currency') !== self.cmbCurrency.getValue())) {
                var sameIndex = rowStore.findExact('currency', self.cmbCurrency.getValue());
                if (sameIndex !== -1) {
                    Ext.Msg.alert(locationStrings.failure, locationStrings.samecurrencyFailure);
                    return;
                }
            }
            if (self.record) {  //update existing
                self.record.set('currency', self.cmbCurrency.getValue());
                self.record.set('interest', self.fldInterest.getValue());
            } else {            //create new
                self.record = new rowStore.recordType({
                    'id' : 0,
                    'configId' : self.configId,
                    'currency' : self.cmbCurrency.getValue(),
                    'interest' : self.fldInterest.getValue()
                });
                rowStore.add(self.record);
            }
            if (rowStore.save() === -1) {
                self.close();
            }
        }, self);

        /**
         * fires when user clicks on 'Cancel' button
         *  closes the window
         * @event
         */
        self.btnCancel.on('click', function() {self.close();}, self);

        /**
         * fires when the window is shown
         *  fills the window's fields in case of edit existing element
         * @event
         */
        self.on('show', function() {
            if (self.record) {
                self.cmbCurrency.setValue(self.record.get('currency'));
                self.fldInterest.setValue(self.record.get('interest'));
            }
        }, self);
    }
});
