/**
 * @fileOverview
 * @author kulcsarb, vendelin8
 * @version 0.12
 */
/**
 * live management panel
 * @class LiveManagement
 * @lends LiveManagement.prototype
 */
LiveManagementPanel = Ext.extend(LiveManagementPanelUi, {
    tabNavigateTitle: 'NAVIGATE',
    btnRemovefromportfolioText: 'Remove',
    btnTestaccountengineText: 'Fix Engine',
    btnViewText: 'View',
    btnStartText: 'Start',
    btnStopText: 'Stop',
    btnAbortText: 'Abort',
    btnCloseText: 'Close trade',
    treeBrowseTitle: 'Browse',
    msgTooManyTabs: "The number of maximum allowed number of tabs is {0}. Please close some of the open ones or choose less.",
    removeTitle: 'Remove {0} from Portfolio?',
    removeHtml: 'Do you really want to remove {0} from Portfolio?',
    startTitle: 'Start {0}?',
    startHtml: 'Do you really want to start {0}?',
    abortTitle: 'Abort {0}?',
    abortHtml: 'Do you really want to abort {0}?',
    stopTitle: 'Stop {0}?',
    stopHtml: 'Do you really want to stop {0}?',
    closeTitle: 'Close {0}?',
    closeHtml: 'Do you really want to close {0}?',
    contentHeaders: {},
    contentHeaderProps: {},
    /** @constructs */
    initComponent: function() {
        LiveManagementPanel.superclass.initComponent.call(this);
        var self = this;
        statusIconColumn = {dataIndex: 'state', width: 25,
            renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                metaData.css = value;
                return '';
            }
        }
        self.contentHeaders[LIVE_TRADE_ENGINE] = ['', 'Id', 'Name', 'Description', 'State', 'Start time', 'End time', 'Winning trades', 'Losing trades', 'Win-lose diff', 'Open trades', 'Floating profit', 'Net profit'];
        self.contentHeaderProps[LIVE_TRADE_ENGINE] = [
            statusIconColumn,
            {dataIndex: 'id', width: 50},
            {dataIndex: 'name', width: 60},
            {dataIndex: 'description', width: 140},
            {dataIndex: 'state', width: 80},
            {dataIndex: 'start_time', width: 111, sortable: false},
            {dataIndex: 'end_time', width: 111, sortable: false},
            {dataIndex: 'benchmark_winning_trades', width: 80, sortable: false},
            {dataIndex: 'benchmark_losing_trades', width: 80, sortable: false},
            {dataIndex: 'benchmark_win_lose_diff', width: 75, sortable: false},
            {dataIndex: 'benchmark_open_trades', width: 75, sortable: false},
            {dataIndex: 'benchmark_floating_profit', width: 85, sortable: false, xtype: 'numbercolumn', format: '0,000.00'},
            {dataIndex: 'benchmark_net_profit', width: 70, sortable: false, xtype: 'numbercolumn', format: '0,000.00'}];
        self.contentHeaders[LIVE_PORTFOLIO] = ['', 'Id', 'Individual', 'Instruments', 'Timeframe', 'State', 'Running from', 'Running to', 'Winning trades', 'Losing trades', 'Win-lose diff', 'Open trades', 'Floating profit', 'Net profit', 'Exposure'];
        self.contentHeaderProps[LIVE_PORTFOLIO] = [
            statusIconColumn,
            {dataIndex: 'id', width: 45},
            {dataIndex: 'individual', width: 80},
            {dataIndex: 'instruments', width: 80, sortable: false},
            {dataIndex: 'timeframe_str', width: 65, sortable: false},
            {dataIndex: 'state', width: 80, sortable: false},
            {dataIndex: 'running_from', width: 111, sortable: false},
            {dataIndex: 'running_to', width: 111, sortable: false},
            {dataIndex: 'benchmark_winning_trades', width: 80, sortable: false},
            {dataIndex: 'benchmark_losing_trades', width: 80, sortable: false},
            {dataIndex: 'benchmark_win_lose_diff', width: 75, sortable: false},
            {dataIndex: 'benchmark_open_trades', width: 75, sortable: false},
            {dataIndex: 'benchmark_floating_profit', width: 85, sortable: false, xtype: 'numbercolumn', format: '0,000.0'},
            {dataIndex: 'benchmark_net_profit', width: 70, sortable: false, xtype: 'numbercolumn', format: '0,000.0'},
            {dataIndex: 'benchmark_exposure', width: 70, sortable: false}];
        self.contentHeaders[LIVE_ACCOUNTS] = ['', 'Id', 'Boker name', 'Account name', 'State', 'Conn. state', 'Running from', 'Running to', 'Balance', 'Winning trades', 'Losing trades', 'Win-lose diff', 'Open trades', 'Floating profit', 'Net profit'];
        self.contentHeaderProps[LIVE_ACCOUNTS] = [
            statusIconColumn,
            {dataIndex: 'id', width: 50},
            {dataIndex: 'brokername', width: 70},
            {dataIndex: 'name', width: 120},
            {dataIndex: 'state', width: 75, sortable: false},
            {dataIndex: 'connection_state', width: 150, sortable: false},
            {dataIndex: 'running_from', width: 111, sortable: false},
            {dataIndex: 'running_to', width: 111, sortable: false},
            {dataIndex: 'balance', width: 65, sortable: false, xtype: 'numbercolumn', format: '0,000.0'},
            {dataIndex: 'winning_trades', width: 80, sortable: false},
            {dataIndex: 'losing_trades', width: 80, sortable: false},
            {dataIndex: 'win_lose_diff', width: 75, sortable: false},
            {dataIndex: 'open_trades', width: 75, sortable: false},
            {dataIndex: 'floating_profit', width: 75, sortable: false, xtype: 'numbercolumn', format: '0,000.0'},
            {dataIndex: 'net_profit', width: 65, sortable: false, xtype: 'numbercolumn', format: '0,000.0'}];
        self.contentHeaders[LIVE_ACCOUNT] = ['Id', 'Strategy Name', 'State', 'Symbol', 'Dir.', 'Amount', 'Floating profit', 'Open time', 'Open price', 'Sl price', 'Tp price', 'MAE', 'MFE'];
        self.contentHeaderProps[LIVE_ACCOUNT] = [
            {dataIndex: 'id', width: 70, id: 'id'},
            {dataIndex: 'strategy_name', width: 110},
            {dataIndex: 'state', width: 111},
            {dataIndex: 'symbol', width: 60},
            {dataIndex: 'direction', width: 50},
            {dataIndex: 'amount', width: 65},
            {dataIndex: 'profit', width: 90},
            {dataIndex: 'open_time', width: 111},
            {dataIndex: 'open_price', width: 80, xtype: 'numbercolumn', format: '0,000.00000'},
            {dataIndex: 'sl_price', width: 80, xtype: 'numbercolumn', format: '0,000.00000'},
            {dataIndex: 'tp_price', width: 80, xtype: 'numbercolumn', format: '0,000.00000'},
            {dataIndex: 'MAE', width: 80, xtype: 'numbercolumn', format: '0,000.00'},
            {dataIndex: 'MFE', width: 80, xtype: 'numbercolumn', format: '0,000.00'}];
        self.tabNavigate.title = self.tabNavigateTitle;
        self.btnRemovefromportfolio.text = self.btnRemovefromportfolioText;
        self.btnTestaccountengine.text = self.btnTestaccountengineText;
        self.btnView.text = self.btnViewText;
        self.btnClose.text = self.btnCloseText;
        self.treeBrowse.title = self.treeBrowseTitle;

        var i;
        //create column models for content grid
        self.colModels = {};
        for (i in self.contentHeaderProps) {
            if (typeof(self.contentHeaderProps[i]) === 'function') {
                continue;
            }
            self.colModels[i] = self.createColumnModel(i, self.gridContent);
        }

        self.initPlugin(new Ext.ux.TabCloseMenu());
        self.rowSelectionPlugin = self.gridContent.initPlugin(new Ext.ux.grid.RowSelectionPaging());

        //task for repeated load
        self.task = new Ext.util.DelayedTask(function() {
            self.reloadContent();
        });

        lazyStoreR('HistoricDatarowManagementStore');
        lazyStoreR('MMPluginStore');
        self.stores = {};
        self.stores[LIVE_TRADE_ENGINE] = Ext.StoreMgr.lookup('PortfolioStore');
        self.stores[LIVE_PORTFOLIO] = Ext.StoreMgr.lookup('BrowseCandidatesStore');
        self.stores[LIVE_ACCOUNTS] = Ext.StoreMgr.lookup('BrowseAccountsStore');
        self.stores[LIVE_ACCOUNT] = Ext.StoreMgr.lookup('BrowseTradePagingStore');

        var store;
        for (i in self.stores) {
            store = self.stores[i];
            if (typeof(store) === 'function') {
                continue;
            }
            store.on('load', function() {
                self.gridContent.getSelectionModel().fireEvent('selectionchange');
            }, self);
        }

        self.treeUrls = {};
        self.treeUrls[LIVE_TRADE_ENGINE] = urls.treePortfolios;
        self.treeUrls[LIVE_ACCOUNTS] = urls.treeAccounts;

        self.treeBrowse.on('beforeexpandnode', function(node) {
            if (node.loaded2) {
                node.eachChild(function(childNode) {
                    childNode.expand();
                });
                return;
            }
            var loadParams = node.loadParams;
            var parentIds;
            if ('parentIds' in loadParams) {
                parentIds = loadParams.parentIds.slice();
                parentIds.push(loadParams.id);
            } else if ('id' in loadParams) {
                parentIds = [loadParams.id];
            }
            if (loadParams.level in STATIC_NODES) {
                var level = TREE_CHILDREN[loadParams.level];
                var leaf, params, node2;
                leaf = !(level in TREE_CHILDREN);
                params = {text: TREENODE_NAMES[level], leaf: leaf, expanded: !leaf, expandable: !leaf};
                if (leaf) {
                    node2 = new Ext.tree.TreeNode(params);
                } else {
                    node2 = new Ext.tree.AsyncTreeNode(params);
                }
                node2.loadParams = {'id': loadParams.id, 'level': level, 'parentIds': parentIds};
                node.appendChild(node2);
                node.loaded2 = true;
            }
        }, self);

        /**
         * fires before a tree node is expanded and loaded
         *  loader url and parameters are being set based on data stored in the node
         * @event
         */
        self.treeBrowse.on('beforeload', function(node) {
            var loadParams = node.loadParams;
            if (!loadParams) {
                return;
            }
            self.treeBrowse.getLoader().url = self.treeUrls[loadParams.level];
            var params = {};
            if (loadParams.id) {
                params.id = loadParams.id;
            }
            self.treeBrowse.getLoader().baseParams = params;
        }, self);

        /**
         * fires in case of a tree error
         *  shows error message
         * @event
         */
        self.treeBrowse.getLoader().on('loadexception', function(loader, node, response) {
            ajaxErrorHandle(response);
        }, self);

        /**
         * fires after a successful tree node expand and load
         *  adds new sub-nodes to the expanded node based on the response
         * @event
         */
        self.treeBrowse.getLoader().on('load', function(loader, node, response) {
            response = ajaxSuccessHandle(response);
            if (!response) {
                return;
            }
            response = response.data;
            var i, j, level, node2, hasStaticChildren, params;
            var loadParams = node.loadParams;
            var level = TREE_CHILDREN[loadParams.level];
            var leaf = !(level in TREE_CHILDREN);
            var loaded = level in STATIC_NODES;
            var parentIds;
            if ('parentIds' in loadParams) {
                parentIds = loadParams.parentIds.slice();
                parentIds.push(loadParams.id);
            } else if ('id' in loadParams) {
                parentIds = [loadParams.id];
            }
            for (i in response) {
                if (typeof(response[i]) === 'function') {
                    continue;
                }
                params = {text: response[i].name, leaf: leaf, expanded: !leaf, loaded: loaded, expandable: !leaf};
                if (leaf) {
                    node2 = new Ext.tree.TreeNode(params);
                } else {
                    node2 = new Ext.tree.AsyncTreeNode(params);
                }
                node2.loadParams = {'id' : response[i].id, 'level': level, 'parentIds': parentIds};
                node2.loaded2 = false;
                node.appendChild(node2);
            }
        }, self);

        /**
         * fires when a node is collapsed
         *  resets loaded variable to maintain possible changes
         * @event
         */
        self.treeBrowse.on('collapsenode', function(node) {
            if (!(node.loadParams.level in STATIC_NODES)) {
                node.loaded = false;
            } else {
                node.eachChild(function(childNode) {
                    childNode.collapse();
                });
            }
        }, self);

        /**
         * fires when a node is clicked
         *  loads the related data to the content grid
         * @event
         */
        self.treeBrowse.on('click', function(node) {
            var loadParams = node.loadParams;
            if (!loadParams) {
                return false;
            }
            var level = loadParams.level;
            if (level === LIVE_TRADE_ENGINE) {
                delete self.portfolio_id;
            } else if (level === LIVE_ACCOUNT) {
                self.portfolio_id = loadParams.parentIds[1];
                self.account_id = loadParams.id;
            } else {
                self.portfolio_id = loadParams.id;
                delete self.account_id;
            }
            
            self.gridContent.getSelectionModel().clearSelections();
            self.storeBaseParams(true, loadParams);
        }, self);

        /**
         * fires when the content grid's selection changes
         *  sets buttons' enablity related to this grid's selection
         * @event
         */
        self.gridContent.getSelectionModel().on('selectionchange', function() {
            var record = self.rowSelectionPlugin.getSelected();
            var newVal = (record === undefined);
            var records = self.rowSelectionPlugin.getSelections();
            var i;
            var startDisabled = false;
            var stopDisabled = false;
            var abortDisabled = false;
            var removeDisabled = false;
            for (i = 0; i < records.length; i++) {
                if (records[i].get('benchmark') === true) {
		            startDisabled = true;
		            stopDisabled = true;
		            abortDisabled = true;
                    removeDisabled = true;
                    break;
                }
                switch (records[i].get('state')) {
                    case 'STOPPING':
                        stopDisabled = true;
                    case 'RUNNING':
                        startDisabled = true;
                        removeDisabled = true;
                        break;
                    case 'ABORTED':
                    case 'STOPPED':
                    default:
                        stopDisabled = true;
                        abortDisabled = true;
                        break;
                }
            }
            self.btnStart.setDisabled((newVal) || (startDisabled));
            self.btnStop.setDisabled((newVal) || (stopDisabled));
            self.btnAbort.setDisabled((newVal) || (abortDisabled));
            self.btnClose.setDisabled(newVal);
            self.btnRemovefromportfolio.setDisabled((newVal) || (removeDisabled));
            self.btnTestaccountengine.setDisabled((newVal) || (records.length > 1));
            self.btnView.setDisabled(newVal);
        }, self);

        /**
         * fires when clicked on "View" button
         *  adds the selected ones as view tab (all that aren't added yet)
         *  if the number of tabs is bigger than the one set in config, it shows an error, that less tabs must be viewed at the same time
         * @event
         */
        self.btnView.on('click', function() {
            self.addViews();
        }, self);

        self.createTypedButtonAction(self.btnStart, urls.liveStart, 0, [self.startTitle, self.startHtml]);
        self.createTypedButtonAction(self.btnStop, urls.liveStop, 0, [self.stopTitle, self.stopHtml]);
        self.createTypedButtonAction(self.btnAbort, urls.liveAbort, 0, [self.abortTitle, self.abortHtml]);
        self.createTypedButtonAction(self.btnRemovefromportfolio, urls.addToPortfolio, 1, [self.removeTitle, self.removeHtml]);
        self.createTypedButtonAction(self.btnClose, urls.tradesClose, 2, [self.closeTitle, self.closeHtml]);

        /**
         * fires when clicked on "Test Account Engine" button
         *  shows Engine Window with details
         * @event
         */
        self.btnTestaccountengine.on('click', function() {
            var record = self.rowSelectionPlugin.getSelected();
            if (!record) {
                return;
            }
            var popup = new AccountEngineWindow();
            popup.account_id = record.get('id');
            popup.portfolio_id = self.parentIds[1];
            popup.show();
        }, self);

        /**
         * fires when the content grid's paging toolbar has been paged
         *  saves paging details to base params to be able to reload them when returning from another screen
         * @event
         */
        self.gridContent.getBottomToolbar().on('change', function(tb, pageData) {
            self.rowSelectionPlugin.pagingEvent();
            if ((!self.lastBaseParams) || (!self.stores[self.level]) || (self.destroying) || (self.isDestroyed) || (!self.isVisible()) || (self.tabNavigate.destroying) || (self.tabNavigate.isDestroyed) || (!self.tabNavigate.isVisible())) {
                self.startStopTask(false);
                return;
            }
            self.lastBaseParams.start = (pageData.activePage - 1) * tb.pageSize;
        }, self);

        self.treeBrowse.on('afterrender', function() {
            self.treeBrowse.getRootNode().loadParams = {'level': LIVE_TRADE_ENGINE, 'id': 0};
            self.treeBrowse.getRootNode().expand();
            self.treeBrowse.fireEvent('click', self.treeBrowse.getRootNode());
        }, self);

        /**
         * fires when the navigate panel is shown
         *  reloads the content grid with the last params
         *  reloads content grid's store
         * @event
         */
        self.tabNavigate.on('show', function() {
            if ((self.level !== undefined)) {
                self.reloadContent();
            }
        }, self);

        self.tabNavigate.on('hide', function() {
            self.startStopTask(false);
        }, self);

        self.gridContent.on('activate', function() {
            setPageSize(self.gridContent, null, [self.stores[LIVE_PORTFOLIO], self.stores[LIVE_ACCOUNT]]);
        }, self);

        /**
         * fires before the panel is shown
         *  calls active tab's show event
         * @event
         */
        self.on('show', function() {
            if (self.getActiveTab) {
                var activetab = self.getActiveTab();
                if (!activetab) {
                    self.tabNavigate.fireEvent('show');
                } else {
                    activetab.fireEvent('show');
                }
            }
            self.gridContent.getSelectionModel().fireEvent('selectionchange');
        }, self);
    },

    /**
     * reconfigures the content grid's columns and store based on the current database type
     * @param {Integer} level 5:strategy
     */
    reconfigureContent: function(level) {
        var self = this;
        if (self.level === level) {
            return;
        }
        self.gridContent.reconfigure(self.stores[level], self.colModels[level]);
        var pagingEnabled = (self.stores[level].baseParams) && (self.stores[level].baseParams.limit);
        self.gridContent.getBottomToolbar().setDisabled(!pagingEnabled);
        if (pagingEnabled) {
            self.gridContent.getBottomToolbar().bindStore(self.stores[level]);
        }
        self.level = level;
        var typeText = '';
        var showButtons = true;
        if (level in TREENODE_NAMES) {
            typeText = TREENODE_NAMES[level];
        } else {
            showButtons = false;
        }
        self.sepStart.setVisible(showButtons);
        self.btnStart.setVisible(showButtons);
        self.sepStop.setVisible(showButtons);
        self.btnStop.setVisible(showButtons);
        self.sepAbort.setVisible(showButtons);
        self.btnAbort.setVisible(showButtons);
        self.sepClose.setVisible(level === LIVE_ACCOUNT);
        self.btnClose.setVisible(level === LIVE_ACCOUNT);
        if (showButtons) {
            typeText = ' ' + typeText;
            self.btnStart.setText(self.btnStartText + typeText);
            self.btnStop.setText(self.btnStopText + typeText);
            self.btnAbort.setText(self.btnAbortText + typeText);
        }
        self.sepRemovefromportfolio.setVisible((level === LIVE_PORTFOLIO) || (level === LIVE_ACCOUNTS));
        self.btnRemovefromportfolio.setVisible((level === LIVE_PORTFOLIO) || (level === LIVE_ACCOUNTS));
        self.sepTestaccountengine.setVisible(level === LIVE_ACCOUNTS);
        self.btnTestaccountengine.setVisible(level === LIVE_ACCOUNTS);
        self.btnView.setVisible(level !== LIVE_ACCOUNT);
        self.gridContent.getSelectionModel().fireEvent('selectionchange');
    },

    /**
     * adds views as tabs
     * @param {Array|undefined} selected array of records to add as tabs, if they are not from the grid's currently selected records
     * @param {Integer|undefined} level the level of the tabs added from outside the selection
     */
    addViews: function(selected, level) {
        var self = this;
        var i, j, tab, runId, record;
        var selectedTabs = [];
        if (level === undefined) {
            level = self.level;
        }
        if (!selected) {
            var selected = self.rowSelectionPlugin.getSelections();
        }
        if ((!selected) || (typeof(selected) !== 'object') || (selected.length < 1)) {
            return;
        }
        if (selected.length === 0) {
            selected = [selected];
        }
        var rested = selected.slice().reverse();    //a reversed copy to be able to easily remove by index
        for (i in selected) {                       //iterate selected records
            if (typeof(selected[i]) === 'function') {
                continue;
            }
            for (j in self.items.items) {           //iterate all tabs for already showed view tabs
                tab = self.items.items[j];
                if (typeof(tab) === 'function') {
                    continue;
                }
            }
        }
        tab = null;
        if (rested.length + self.items.length - 1 > config.BROWSE_MAX_TABS) {
            Ext.Msg.alert(locationStrings.failure, String.format(self.msgTooManyTabs, config.BROWSE_MAX_TABS));
            return;
        } else if (!rested.length) {
            selectedTabs[selectedTabs.length - 1].show();         //no tabs are needed to add, but the last one is activated
            return;
        }
        selected = rested;
        var msg = '';
        for (i in selected) {
            record = selected[i];
            if (typeof(record) === 'function') {
                continue;
            }
            switch (level) {
                case LIVE_PORTFOLIO: //strategy
                    tab = self.addView(new StrategyViewPanel(), level, record.get('id'), record.get('name'));
                    tab.symbols = record.get('instruments').split(',');
                    tab.timeframe_str = record.get('timeframe_str');
                    tab.lastSelectedRun = -1;
                    break;
                case LIVE_ACCOUNTS: //account
                    tab = self.addView(new AccountViewPanel(), level, record.get('id'), record.get('name'));
                    tab.lastSelectedRun = -1;
                    break;
            }
        }
        if (msg !== '') {
            Ext.Msg.alert(locationStrings.failure, msg);
        }
        if (tab) {
            tab.show();
        }
    },

    /**
     * adds a view tab
     * @param {Panel} tab the view panel to add
     * @param {Integer} recordId the id of the record that is being added
     * @param {String|undefined} recordName if set, the name of the record that is being added
     * @param {Integer|undefined} lastSelectedRun if set, the id of the run to select after panel show
     * @return {panel}  the tab for more actions
     */
    addView: function(tab, level, recordId, recordName) {
        var self = this;
        tab.recordId = recordId;
        tab.recordName = recordName;
//        tab.lastSelectedRun = lastSelectedRun;
        tab.level = level;
        var j = self.add(tab);
        return tab;
    },

    /**
     * creates a column model for the given level
     * @param {Integer} level the ined of the needed database type
     * @param {GridPanel|undefined} grid if set, the grid to set the first column as selection model
     * @return {ColumnModel} the created column model
     */
    createColumnModel: function(level, grid) {
        var self = this;
        var columns = [];
        var column;
        if (grid) {
            columns.push(grid.selModel);
        }
        var headers = self.contentHeaderProps[level];
        for (i in headers) {
            column = headers[i];
            if (typeof(column) === 'function') {
                continue;
            }
            column.header = self.contentHeaders[level][i];
            columns.push(column);
        }
        return new Ext.grid.ColumnModel({columns: columns,
            defaults: {
                sortable: true,
                width: 100
            }});
    },

    /**
     * reloads the content grid's data
     */
    reloadContent: function() {
        var self = this;
        if (!self.lastBaseParams) {
            return;
        }
        for (i in self.lastBaseParams) {
            if (typeof(self.lastBaseParams[i]) === 'function') {
                continue;
            }
            self.gridContent.getStore().setBaseParam(i, self.lastBaseParams[i]);
        }
        self.gridContent.getStore().load();
        if ((self.level !== LIVE_PORTFOLIO) && (self.level !== LIVE_ACCOUNT)) {
            self.startStopTask(false);
        } else {
            self.startStopTask(true);
        }
    },

    /**
     * adds the given params as base params and removes the others
     * stores the last load params of the content grid's store
     * (re)loads the store if needed
     */
    storeBaseParams: function(load, loadParams) {
        var self = this;
        var level = loadParams.level;
        self.parentIds = loadParams.parentIds;
        if (typeof(level) === 'number') {
            self.reconfigureContent(level);
        }
        self.stores[self.level].setBaseParam('id', typeof(loadParams.id) === 'number' ? loadParams.id : 0);
        if (level == LIVE_ACCOUNT) {
            self.stores[self.level].setBaseParam('id2', loadParams.parentIds[1]);
        }
        self.lastBaseParams = {};
        var baseParams;
        for (i in self.gridContent.getStore().baseParams) {
            if (typeof(self.gridContent.getStore().baseParams[i]) === 'function') {
                continue;
            }
            self.lastBaseParams[i] = self.gridContent.getStore().baseParams[i];
        }
        self.reloadContent();
    },

    createTypedButtonAction: function(button, urlInput, type, confirm) {
        var self = this;
        button.on('click', function() {
            var records = self.rowSelectionPlugin.getSelections();
            if (!records.length) {
                return;
            }
            if (confirm) {
                confirmWindow(String.format(confirm[0], SERVICE_NAMES[self.level]), String.format(confirm[1], SERVICE_NAMES[self.level]), function(callbackOnSuccess) {
		            var i;
		            var params = {'ids': Ext.util.JSON.encode(collectIds(records))};
		            var url = urlInput;
		            if (type === 0) {
		                if (self.level !== LIVE_TRADE_ENGINE) {
		                    params.portfolio_id = self.portfolio_id;
		                }
		                url += '_' + SERVICE_NAMES[self.level]
		            } else if (type === 1) {
		                params['id'] = self.portfolio_id;
		                params['element_type'] = SERVICE_NAMES[self.level]
		                params['add'] = 0;
                    } else if (type === 2) {
                        params.portfolio_id = self.portfolio_id;
                        params.account_id = self.account_id;
		            }
		            Ext.Ajax.request({
		                url: url,
		                params: params,
		                success: function(response) {
		                    response = ajaxSuccessHandle(response);
		                    if (!response) {
		                        return;
		                    }
		                    Ext.Msg.alert(locationStrings.success, response.msg);
		                    if (type === 1) {
		                        self.gridContent.getSelectionModel().clearSelections();
		                    }
		                    self.reloadContent();
                            callbackOnSuccess();
		                },
		                failure: function(response) {
		                    response = ajaxErrorHandle(response, null, true);
		                }
		            });
                });
            }
        }, self);
    },

    startStopTask: function(start) {
        var self = this;
        if (start) {
            self.task.delay(10000);
        } else {
            self.task.cancel();
        }
    }
});
