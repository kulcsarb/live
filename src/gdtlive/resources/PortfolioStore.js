PortfolioStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        PortfolioStore.superclass.constructor.call(this, Ext.apply({
            root: 'data',
            autoSave: false,
            batch: true,
            storeId: 'PortfolioStore',
            api: {
                read: {
                    url: 'portfolio/getAll',
                    method: 'POST'
                },
                create: {
                    url: 'portfolio/save',
                    method: 'POST'
                },
                update: {
                    url: 'portfolio/save',
                    method: 'POST'
                },
                destroy: {
                    url: 'portfolio/delete',
                    method: 'POST'
                }
            },
            fields: [
                {
                    name: 'id',
                    type: 'int'
                },
                {
                    name: 'name',
                    type: 'string'
                },
                {
                    name: 'description',
                    type: 'string'
                },
                {
                    name: 'management_type',
                    type: 'int'
                },
                {
                    name: 'management_type_name',
                    type: 'string'
                },
                {
                    name: 'state',
                    type: 'string'
                },
                {
                    name: 'start_time',
                    type: 'string'
                },
                {
                    name: 'end_time',
                    type: 'string'
                },
                {
                    name: 'benchmark_winning_trades',
                    type: 'int'
                },
                {
                    name: 'benchmark_losing_trades',
                    type: 'int'
                },
                {
                    name: 'benchmark_win_lose_diff',
                    type: 'int',
                    convert: function(v, record) {
                        return record.benchmark_winning_trades - record.benchmark_losing_trades;
                    }
                },
                {
                    name: 'benchmark_open_trades',
                    type: 'int'
                },
                {
                    name: 'benchmark_net_profit',
                    type: 'float'
                },
                {
                    name: 'benchmark_floating_profit',
                    type: 'float'
                }
            ]
        }, cfg));
    }
});
new PortfolioStore();