/**
 * @fileOverview
 * @author vendelin8, kulcsarb
 * @version 0.6
 */
/**
 * admin spread config edit functionality
 * @class AdminSpreadConfigEditWindow
 * @lends AdminSpreadConfigEditWindow.prototype
 */
AdminSpreadConfigEditWindow = Ext.extend(AdminSpreadConfigEditWindowUi, {
    mainTitle: 'Edit Spread Config',
    fldNameLabel: 'Spread name',
    fldNameBlank: 'Enter spread\'s name',
    fldNameEmpty: 'Spread\'s name here',
    fldCommissionLabel: 'Commission',
    fldCommissionBlank: 'Enter commission',
    fldCommissionEmpty: 'Commission here',
    fldDefaultspreadLabel: 'Default spread',
    fldDefaultspreadBlank: 'Enter default spread',
    fldDefaultspreadEmpty: 'Default spread here',
    btnSaveText: 'Save',
    btnCancelText: 'Cancel',
    /** @constructs */
    initComponent: function() {
        AdminSpreadConfigEditWindow.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        self.fldName.fieldLabel = self.fldNameLabel;
        self.fldName.blankText = self.fldNameBlank;
        self.fldName.emptyText = self.fldNameEmpty;
        self.fldCommission.fieldLabel = self.fldCommissionLabel;
        self.fldCommission.blankText = self.fldCommissionBlank;
        self.fldCommission.emptyText = self.fldCommissionEmpty;
        self.fldDefaultspread.fieldLabel = self.fldDefaultspreadLabel;
        self.fldDefaultspread.blankText = self.fldDefaultspreadBlank;
        self.fldDefaultspread.emptyText = self.fldDefaultspreadEmpty;
        self.btnSave.text = self.btnSaveText;
        self.btnCancel.text = self.btnCancelText;

        /**
         * fires when user clicks on 'Send' button
         *  saves new settings to spread config, or create a new one 
         * @event
         */
        self.btnSave.on('click', function() {
            var store = Ext.StoreMgr.lookup('AdminSpreadConfigStore');
            if (self.record) {  //update existing
                self.record.set('name', self.fldName.getValue());
                self.record.set('default_commission', self.fldCommission.getValue());
                self.record.set('default_spread', self.fldDefaultspread.getValue());
                self.record.set('commission_in_pips', self.radPiporusd0.getValue() ? false : true)
            } else {            //create new
                self.record = new store.recordType({
                    'id': 0,
                    'name': self.fldName.getValue(),
                    'default_commission': self.fldCommission.getValue(),
                    'default_spread': self.fldDefaultspread.getValue(),
                    'commission_in_pips': self.radPiporusd0.getValue() ? false : true
                });
                store.add(self.record);
            }
            if (store.save() === -1) {
                self.close();
            }
        }, self);

        /**
         * fires when user clicks on 'Cancel' button
         *  closes the window
         * @event
         */
        self.btnCancel.on('click', function() {self.close();}, self);

        /**
         * fires when the window is shown
         *  fills the window's fields in case of edit existing element
         * @event
         */
        self.on('show', function() {
            if (self.record) {
                self.fldName.setValue(self.record.get('name'));
                self.fldCommission.setValue(self.record.get('default_commission'));
                self.fldDefaultspread.setValue(self.record.get('default_spread'));
                if (self.record.get('commission_in_pips')) {
                    self.radPiporusd1.setValue(true);
                } else {
                    self.radPiporusd0.setValue(true);
                }
            } else {
                self.radPiporusd0.setValue(true);
            }
        }, self);
    }
});
