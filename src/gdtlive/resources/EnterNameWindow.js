/**
 * @fileOverview
 * @author vendelin8
 * @version 0.2
 */
/**
 * a modal window with a name textfield, Ok and Cancel buttons. The title, window description and handlers must be set after initialization
 * @class EnterNameWindow
 */
EnterNameWindow = Ext.extend(EnterNameWindowUi, {
    fldNameLabel: 'Name',
    fldNameBlank: 'Enter name',
    fldNameEmpty: 'Name here',
    btnOkText: 'OK',
    btnCancelText: 'Cancel',
    /** @constructs */
    initComponent: function() {
        EnterNameWindow.superclass.initComponent.call(this);
        var self = this;
        self.fldName.fieldLabel = self.fldNameLabel;
        self.fldName.blankText = self.fldNameBlank;
        self.fldName.emptyText = self.fldNameEmpty;
        self.btnOk.text = self.btnOkText;
        self.btnCancel.text = self.btnCancelText;

        self.defaultButton = self.btnSend;

        /**
         * fires when clicked on 'Cancel' button
         *  closes the window
         * @event
         */
        self.btnCancel.on('click', function() {
            self.close();
        });
    }
});
