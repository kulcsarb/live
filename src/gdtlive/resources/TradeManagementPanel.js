/**
 * @fileOverview
 * @author vendelin8
 * @version 0.3
 */
/**
 * candidate strategy management
 * @class CandidatesPanel
 * @lends CandidatesPanel.prototype
 */
TradeManagementPanel = Ext.extend(TradeManagementPanelUi, {
    mainTitle: 'Trade Management',
    gridHeaders: ['Id', 'Type', 'Portfolio', 'Account', 'StratId', 'Group', 'State', 'Symbol', 'Dir.', 'Amount', 'Curr. amount', 'Open price', 'Open fill time', 'Close price', 'Close fill time', 'Entry price', 'Fill price', 'Fill time'],
    btnRepairText: 'Repair',
    btnCloseText: 'Close',
    btnMarkasclosedText: 'Mark as Closed',
    btnFixmessagesText: 'Fix messages',
    btnSendopenText: 'Send Open',
    btnSendcloseText: 'Send Close',
    btnPlaceText: 'Place',
    btnCancelText: 'Cancel',
    btnMarkasfilledText: 'Mark as Filled',
    /** @constructs */
    initComponent: function() {
        TradeManagementPanel.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        setGridColums(self, self.gridHeaders, 1);
        self.btnRepair.text = self.btnRepairText;
        self.btnClose.text = self.btnCloseText;
        self.btnMarkasclosed.text = self.btnMarkasclosedText;
        self.btnFixmessages.text = self.btnFixmessagesText
        self.btnSendopen.text = self.btnSendopenText;
        self.btnSendclose.text = self.btnSendcloseText;
        self.btnPlace.text = self.btnPlaceText;
        self.btnCancel.text = self.btnCancelText;
        self.btnMarkasfilled.text = self.btnMarkasfilledText;

        //task for repeated load
        self.task = new Ext.util.DelayedTask(function() {
            self.tradeStore.reload();
        });

        self.tradeStore = TradeStore;
        self.tradeStore.on('load', function() {
            self.getSelectionModel().fireEvent('selectionchange', self.onTreeGridSelectionChange, self);
            if ((self.destroying) || (self.isDestroyed) || (!self.isVisible())) {
                return;
            }
            self.task.delay(10000);
        }, self);
        self.rowSelectionPlugin = self.initPlugin(new Ext.ux.grid.RowSelectionPaging());

        /**
         * fires when the content treegrid's selection changes
         *  sets buttons' enablity related to this grid's selection
         * @event
         */
        self.getSelectionModel().on('selectionchange', function() {
            var record = self.getSelectionModel().getSelected();
            var newVal = (record === undefined);
            var records = self.getSelectionModel().getSelections();
            if ((newVal) || (records.length > 1)) {
		        self.btnRepair.setDisabled(true);
		        self.btnClose.setDisabled(true);
		        self.btnMarkasclosed.setDisabled(true);
		        self.btnFixmessages.setDisabled(true);
		        self.btnSendopen.setDisabled(true);
		        self.btnSendclose.setDisabled(true);
		        self.btnPlace.setDisabled(true);
		        self.btnCancel.setDisabled(true);
		        self.btnMarkasfilled.setDisabled(true);
            } else {
                var newVal2 = record.get('type');
                self.btnRepair.setDisabled(newVal2 !== 'trade');
                self.btnClose.setDisabled(newVal2 !== 'trade');
                self.btnMarkasclosed.setDisabled(newVal2 !== 'trade');
                self.btnFixmessages.setDisabled(newVal2 !== 'trade');
                self.btnSendopen.setDisabled(newVal2 !== 'marketorder');
                self.btnSendclose.setDisabled(newVal2 !== 'marketorder');
                self.btnPlace.setDisabled((newVal2 !== 'stoploss') && (newVal2 !== 'takeprofit'));
                self.btnCancel.setDisabled((newVal2 !== 'stoploss') && (newVal2 !== 'takeprofit'));
                self.btnMarkasfilled.setDisabled((newVal2 !== 'stoploss') && (newVal2 !== 'takeprofit'));
            }
        }, self);

        /**
         * fires when the paging toolbar has been paged
         *  saves paging details to base params to be able to reload them when returning from another screen
         * @event
         */
        self.getBottomToolbar().on('change', function(tb, pageData) {
            self.rowSelectionPlugin.pagingEvent();
            if ((!self.lastBaseParams) || (self.destroying) || (self.isDestroyed) || (!self.isVisible())) {
                return;
            }
            self.lastBaseParams.start = (pageData.activePage - 1) * tb.pageSize;
        }, self);

        self.createButtonAction(self.btnRepair, urls.tradeRepair);
        self.createButtonAction(self.btnClose, urls.tradeClose);
        self.createButtonAction(self.btnMarkasclosed, urls.tradeMarkasclosed);
        self.createButtonAction(self.btnSendopen, urls.tradeSendopen);
        self.createButtonAction(self.btnSendclose, urls.tradeSendclose);
        self.createButtonAction(self.btnPlace, urls.tradePlace);
        self.createButtonAction(self.btnCancel, urls.tradeCancel);
        self.createButtonAction(self.btnMarkasfilled, urls.tradeMarkasfilled);

        self.btnFixmessages.on('click', function() {
            var selected = self.getSelectionModel().getSelected();
            if (!selected) {
                return;
            }
            var popup = new Ext.Window({title: String.format(locationStrings.tradeFixTitle, selected.get('portfolio_id'), selected.get('account_id'), selected.get('id')), width: 500, height: 500, layout: 'fit', modal: true,
                tbar: {xtype: 'toolbar', buttonAlign: 'left', items: [{xtype: 'button', text: 'Refresh', ref: '../btnRefresh'}]},
                items: [{xtype: 'propertygrid', ref: 'tabMessages', columnWidths: [160, 0]}]});
            popup.btnRefresh.on('click', function() {
                popup.tabMessages.reload();
            }, self);
            popup.tabMessages.url = urls.tradeFixmessages;
            popup.tabMessages.params = {'portfolio_id': selected.get('portfolio_id'), 'account_id': selected.get('account_id'), 'trade_id': selected.get('id')};
            popup.btnRefresh.fireEvent('click');
                popup.show();
        }, self);

        /**
         * fires when the panel is shown
         *  reloads store
         * @event
         */
        self.on('show', function() {
            if (!self.rendered) {
                self.loadWhenRedered = true;
            } else {
                self.tradeStore.reload();
            }
        }, self);

        /**
         * fires when the panel is rendered
         *  sets store paging size
         * @event
         */
        self.on('afterrender', function() {
            setPageSize(self, function() {
                if (self.loadWhenRedered) {
                    self.tradeStore.load();
                    delete self.loadWhenRedered;
                }
            });
        }, self);
    },

    createButtonAction: function(button, url) {
        var self = this;
        button.on('click', function() {
            var selected = self.getSelectionModel().getSelected();
            if (!selected) {
                return;
            }
            var trade;
            if (selected.get('type') === 'trade') {
                trade = selected;
            } else {
                trade = self.tradeStore.getAt(self.tradeStore.findExact('_id', selected.get('_parent')));
            }
            var params = {'portfolio_id': trade.get('portfolio_id'), 'account_id': trade.get('account_id'), 'trade_id': trade.get('id')}
            if (selected.get('type') === 'stoploss') {
                params.co_type = 0;
            } else if (selected.get('type') === 'takeprofit') {
                params.co_type = 1;
            }
            Ext.Ajax.request({
                url: url,
                params: params,
                success: function(response) {
                    response = ajaxSuccessHandle(response);
                    if (!response) {
                        return;
                    }
                    self.task.cancel();
                    self.tradeStore.reload();
                },
                failure: function(response) {
                    response = ajaxErrorHandle(response, null, true);
                }
            });
        }, self);
    }
});
