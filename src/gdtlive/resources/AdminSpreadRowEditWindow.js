/**
 * @fileOverview
 * @author vendelin8, kulcsarb
 * @version 0.5
 */
/**
 * admin spread row edit functionality
 * @class AdminSpreadRowEditWindow
 * @lends AdminSpreadRowEditWindow.prototype
 */
AdminSpreadRowEditWindow = Ext.extend(AdminSpreadRowEditWindowUi, {
    mainTitle: 'Edit Spread Config Row',
    cmbInstrumentLabel: 'Instrument',
    cmbInstrumentBlank: 'Select instrument',
    cmbInstrumentEmpty: 'Select instrument...',
    fldDefaultspreadLabel: 'Default spread',
    fldDefaultspreadBlank: 'Enter default spread',
    fldDefaultspreadEmpty: 'Default spread here',
    fldExceptionspreadLabel: 'Exceptional spread',
    fldExceptionspreadBlank: 'Enter exceptional spread',
    fldExceptionspreadEmpty: 'Exceptional spread here',
    lblExceptionfromText: 'Exceptional spread from:',
    fldExceptionfromBlank: 'Enter from hour',
    fldExceptionfromEmpty: 'From hour here',
    lblExceptiontoText: 'to:',
    fldExceptiontoBlank: 'Enter to hour',
    fldExceptiontoEmpty: 'To hour here',
    btnSendText: 'Send',
    btnCancelText: 'Cancel',
    /** @constructs */
    initComponent: function() {
        AdminSpreadRowEditWindow.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        self.cmbInstrument.fieldLabel = self.cmbInstrumentLabel;
        self.cmbInstrument.blankText = self.cmbInstrumentBlank;
        self.cmbInstrument.emptyText = self.cmbInstrumentEmpty;
        self.fldDefaultspread.fieldLabel = self.fldDefaultspreadLabel;
        self.fldDefaultspread.blankText = self.fldDefaultspreadBlank;
        self.fldDefaultspread.emptyText = self.fldDefaultspreadEmpty;
        self.fldExceptionspread.fieldLabel = self.fldExceptionspreadLabel;
        self.fldExceptionspread.blankText = self.fldExceptionspreadBlank;
        self.fldExceptionspread.emptyText = self.fldExceptionspreadEmpty;
        self.lblExceptionfrom.text = self.lblExceptionfromText;
        self.fldExceptionfrom.blankText = self.fldExceptionfromBlank;
        self.fldExceptionfrom.emptyText = self.fldExceptionfromEmpty;
        self.fldExceptionfrom.theOther = self.fldExceptionto;
        self.lblExceptionto.text = self.lblExceptiontoText;
        self.fldExceptionto.blankText = self.fldExceptiontoBlank;
        self.fldExceptionto.emptyText = self.fldExceptiontoEmpty;
        self.fldExceptionto.theOther = self.fldExceptionfrom;
        self.btnSend.text = self.btnSendText;
        self.btnCancel.text = self.btnCancelText;

        /**
         * fires when user clicks on 'Send' button
         *  saves new settings to spread row, or create a new one 
         * @event
         */
        self.btnSend.on('click', function() {
            var rowStore = Ext.StoreMgr.lookup('AdminSpreadRowStore');
            if ((!self.record) || (self.record.get('symbol') !== self.cmbInstrument.getValue())) {
                var sameIndex = rowStore.findExact('symbol', self.cmbInstrument.getValue());
                if (sameIndex !== -1) {
                    Ext.Msg.alert(locationStrings.failure, locationStrings.sameinstrumentFailure);
                    return;
                }
            }
            if (self.record) {  //update existing
                self.record.set('symbol', self.cmbInstrument.getValue());
                self.record.set('spread', self.fldDefaultspread.getValue());
                self.record.set('exception_spread', self.fldExceptionspread.getValue());
                self.record.set('exception_from', self.fldExceptionfrom.getValue());
                self.record.set('exception_to', self.fldExceptionto.getValue());
            } else {            //create new
                self.record = new rowStore.recordType({
                    'id': 0,
                    'configId': self.configId,
                    'symbol': self.cmbInstrument.getValue(),
                    'spread': self.fldDefaultspread.getValue(),
                    'exception_spread': self.fldExceptionspread.getValue(),
                    'exception_from': self.fldExceptionfrom.getValue(),
                    'exception_to': self.fldExceptionto.getValue()
                });
                rowStore.add(self.record);
            }
            if (rowStore.save() === -1) {
                self.close();
            }
        }, self);

        /**
         * fires when the a key has been released on exception spread field
         *  if this field is empty, the exception from and to fields may be blank
         * @event
         */
        self.fldExceptionspread.on('keyup', function() {
            var newVal = (self.fldExceptionspread.getValue() === '');
            self.fldExceptionfrom.allowBlank = newVal;
            self.fldExceptionfrom.validate();
            self.fldExceptionto.allowBlank = newVal;
            self.fldExceptionto.validate();
        });

        /**
         * fires when user clicks on 'Cancel' button
         *  closes the window
         * @event
         */
        self.btnCancel.on('click', function() {self.close();}, self);

        /**
         * fires when the window is shown
         *  fills the window's fields in case of edit existing element
         *  sends a keyup event to the exception spread field to evaluate field validation
         * @event
         */
        self.on('show', function() {
            if (self.record) {
                self.cmbInstrument.setValue(self.record.get('symbol'));
                self.fldDefaultspread.setValue(self.record.get('spread'));
                self.fldExceptionspread.setValue(self.record.get('exception_spread'));
                self.fldExceptionfrom.setValue(self.record.get('exception_from'));
                self.fldExceptionto.setValue(self.record.get('exception_to'));
            }
            self.fldExceptionspread.fireEvent('keyup');
        });
    }
});
