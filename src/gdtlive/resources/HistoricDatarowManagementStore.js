HistoricDatarowManagementStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        HistoricDatarowManagementStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'HistoricDatarowManagementStore',
            root: 'data',
            autoSave: false,
            api: {
                read: {
                    url: 'datarow/list',
                    method: 'POST'
                },
                create: {
                    url: 'download/new',
                    method: 'POST'
                },
                update: {
                    url: 'download/update',
                    method: 'POST'
                },
                destroy: {
                    url: 'datarow/delete',
                    method: 'POST'
                }
            },
            fields: [
                {
                    name: 'id',
                    type: 'int'
                },
                {
                    name: 'configRowId',
                    type: 'int'
                },
                {
                    name: 'pluginName',
                    type: 'string'
                },
                {
                    name: 'symbol',
                    type: 'string'
                },
                {
                    name: 'timeframe_str',
                    type: 'string'
                },
                {
                    name: 'available_from',
                    type: 'date',
                    dateFormat: 'Y-m-d'
                },
                {
                    name: 'from_date',
                    type: 'date',
                    dateFormat: 'Y-m-d'
                },
                {
                    name: 'to_date',
                    type: 'date',
                    dateFormat: 'Y-m-d'
                },
                {
                    name: 'price_type',
                    type: 'string'
                },
                {
                    name: 'status',
                    type: 'string'
                },
                {
                    name: 'status_text',
                    type: 'string'
                },
                {
                    name: 'download_date',
                    type: 'date',
                    dateFormat: 'Y-m-d H:i:s'
                },
                {
                    name: 'bar_count',
                    type: 'int'
                },
                {
                    name: 'timeframe_num',
                    type: 'int'
                }
            ]
        }, cfg));
    }
});
new HistoricDatarowManagementStore();