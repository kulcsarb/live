/**
 * @fileOverview
 * @author vendelin8
 * @version 0.3
 */
/**
 * admin rollover config edit functionality
 * @class AdminRolloverConfigEditWindow
 * @lends AdminRolloverConfigEditWindow.prototype
 */
AdminRolloverConfigEditWindow = Ext.extend(AdminRolloverConfigEditWindowUi, {
    mainTitle: 'Edit Rollover Config',
    fldNameLabel: 'Rollover config name',
    fldNameBlank: 'Enter rollover\'s name',
    fldNameEmpty: 'Rollover\'s name here',
    btnSaveText: 'Save',
    btnCancelText: 'Cancel',
    /** @constructs */
    initComponent: function() {
        AdminRolloverConfigEditWindow.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        self.fldName.fieldLabel = self.fldNameLabel;
        self.fldName.blankText = self.fldNameBlank;
        self.fldName.emptyText = self.fldNameEmpty;
        self.btnSave.text = self.btnSaveText;
        self.btnCancel.text = self.btnCancelText;

        /**
         * fires when user clicks on 'Save' button
         *  saves new settings to rollover config, or create a new one 
         * @event
         */
        self.btnSave.on('click', function() {
            var store = Ext.StoreMgr.lookup('AdminRolloverConfigStore');
            if (self.record) {  //update existing
                self.record.set('name', self.fldName.getValue());
            } else {            //create new
                self.record = new store.recordType({
                    'id' : 0,
                    'name' : self.fldName.getValue()
                });
                store.add(self.record);
            }
            if (store.save() === -1) {
                self.close();
            }
        }, self);

        /**
         * fires when user clicks on 'Cancel' button
         *  closes the window
         * @event
         */
        self.btnCancel.on('click', function() {self.close();}, self);

        /**
         * fires when the window is shown
         *  fills the window's fields in case of edit existing element
         * @event
         */
        self.on('show', function() {
            if (self.record) {
                self.fldName.setValue(self.record.get('name'));
            }
        }, self);
    }
});
