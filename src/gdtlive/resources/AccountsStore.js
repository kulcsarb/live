AccountsStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        AccountsStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AccountsStore',
            root: 'data',
            autoSave: false,
            batch: true,
            api: {
                read: {
                    url: 'liveaccount/getAll',
                    method: 'POST'
                },
                create: {
                    url: 'liveaccount/save',
                    method: 'POST'
                },
                update: {
                    url: 'liveaccount/save',
                    method: 'POST'
                },
                destroy: {
                    url: 'liveaccount/delete',
                    method: 'POST'
                }
            },
            fields: [
                {
                    name: 'id',
                    type: 'int'
                },
                {
                    name: 'broker_id',
                    type: 'int'
                },
                {
                    name: 'brokername',
                    type: 'string'
                },
                {
                    name: 'name',
                    type: 'string'
                },
                {
                    name: 'userId',
                    type: 'int'
                },
                {
                    name: 'username',
                    type: 'string'
                },
                {
                    name: 'account_number',
                    type: 'string'
                },
                {
                    name: 'sendercompid',
                    type: 'string'
                },
                {
                    name: 'targetcompid',
                    type: 'string'
                },
                {
                    name: 'password',
                    type: 'string'
                },
                {
                    name: 'base_equity',
                    type: 'float'
                },
                {
                    name: 'balance',
                    type: 'float'
                },
                {
                    name: 'leverage',
                    type: 'int'
                },
                {
                    name: 'risk_multiplier',
                    type: 'float'
                },
                {
                    name: 'server_url',
                    type: 'string'
                },
                {
                    name: 'server_port',
                    type: 'int'
                },
                {
                    name: 'use_ssl',
                    type: 'boolean'
                },
                {
                    name: 'state',
                    type: 'string'
                },
                {
                    name: 'connection_state',
                    type: 'string'
                },
                {
                    name: 'running_from',
                    type: 'string'
                },
                {
                    name: 'running_to',
                    type: 'string'
                },
                {
                    name: 'equity',
                    type: 'float'
                },
                {
                    name: 'floating_profit',
                    type: 'float'
                },
                {
                    name: 'trade_num',
                    type: 'int'
                },
                {
                    name: 'benchmark',
                    type: 'boolean'
                },
                {
                    name: 'winning_trades',
                    type: 'int'
                },
                {
                    name: 'losing_trades',
                    type: 'int'
                },
                {
                    name: 'win_lose_diff',
                    type: 'int',
                    convert: function(v, record) {
                        return record.winning_trades - record.losing_trades;
                    }
                },
                {
                    name: 'open_trades',
                    type: 'int'
                },
                {
                    name: 'net_profit',
                    type: 'float'
                }
            ]
        }, cfg));
    }
});
copyStore('AccountsStore', 'BrowseAccountsStore')
new AccountsStore();