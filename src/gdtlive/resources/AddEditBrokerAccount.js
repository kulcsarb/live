/**
 * @fileOverview
 * @author vendelin8
 * @version 0.4
 */
/**
 * add new or edit existing broker account details
 * @class AddEditBrokerAccount
 * @lends AddEditBrokerAccount.prototype
 */
AddEditBrokerAccount = Ext.extend(AddEditBrokerAccountUi, {
    mainTitle: 'Add/Edit Broker Account',
    cmbBrokernameLabel: 'Broker name',
    cmbBrokernameBlank: 'Enter broker name',
    cmbBrokernameEmpty: 'Broker name here',
    fldAccountnameBlank: 'Enter account name',
    fldAccountnameEmpty: 'Account name here',
    fldUsernameLabel: 'Username',
    fldUsernameBlank: 'Enter username',
    fldUsernameEmpty: 'Username here',
    fldAccountnumberLabel: 'Account number',
    fldPasswordLabel: 'Password',
    fldPasswordBlank: 'Enter password',
    fldPasswordEmpty: 'Password here',
    fldConfirmpasswordLabel: 'Confirm password',
    fldConfirmpasswordBlank: 'Retype password',
    fldConfirmpasswordEmpty: 'Password here',
    fldAccountnameLabel: 'Account name',
    fldSendercompidLabel: 'Sender Comp Id',
    fldSendercompidBlank: 'Enter Sender Comp Id',
    fldSendercompidEmpty: 'Sender Comp Id here',
    fldTargetcompidLabel: 'Target Comp Id',
    fldTargetcompidBlank: 'Enter Target Comp Id',
    fldTargetcompidEmpty: 'Target Comp Id here',
    fldBasecapitalLabel: 'Base capital (USD)',
    fldBasecapitalBlank: 'Enter base capital',
    fldBasecapitalEmpty: 'Base capital here',
    fldBalanceLabel: 'Balance',
    fldBalanceBlank: 'Enter balance',
    fldBalanceEmpty: 'Balance here',
    fldLeverageLabel: 'Leverage',
    fldLeverageBlank: 'Enter leverage',
    fldLeverageEmpty: 'Leverage here',
    fldRiskmultiplierLabel: 'Risk multiplier',
    fldRiskmultiplierBlank: 'Enter risk multiplier',
    fldRiskmultiplierEmpty: 'Risk multiplier here',
    fldServerurlLabel: 'Server url',
    fldServerurlBlank: 'Enter Server url',
    fldServerurlEmpty: 'Server url here',
    fldServerportLabel: 'Server port',
    fldServerportBlank: 'Enter Server port',
    fldServerportEmpty: 'Server port here',
    chkUsesslLabel: 'Use ssl',
    btnSaveText: 'Save',
    btnCancelText: 'Cancel',
    btnTestconnectionText: 'Test connection',
    testConnectionTitle: 'Connection state',
    /** @constructs */
    initComponent: function() {
        AddEditBrokerAccount.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        self.cmbBrokername.fieldLabel = self.cmbBrokernameLabel;
        self.cmbBrokername.blankText = self.cmbBrokernameBlank;
        self.cmbBrokername.emptyText = self.cmbBrokernameEmpty;
        self.fldAccountname.fieldLabel = self.fldAccountnameLabel;
        self.fldAccountname.blankText = self.fldAccountnameBlank;
        self.fldAccountname.emptyText = self.fldAccountnameEmpty;
        self.fldUsername.fieldLabel = self.fldUsernameLabel;
        self.fldUsername.blankText = self.fldUsernameBlank;
        self.fldUsername.emptyText = self.fldUsernameEmpty;
        self.fldAccountnumber.fieldLabel = self.fldAccountnumberLabel;
        self.fldPassword.fieldLabel = self.fldPasswordLabel;
        self.fldPassword.blankText = self.fldPasswordBlank;
        self.fldPassword.emptyText = self.fldPasswordEmpty;
        self.fldConfirmpassword.fieldLabel = self.fldConfirmpasswordLabel;
        self.fldConfirmpassword.blankText = self.fldConfirmpasswordBlank;
        self.fldConfirmpassword.emptyText = self.fldConfirmpasswordEmpty;
        self.fldSendercompid.fieldLabel = self.fldSendercompidLabel;
        self.fldSendercompid.blankText = self.fldSendercompidBlank;
        self.fldSendercompid.emptyText = self.fldSendercompidEmpty;
        self.fldTargetcompid.fieldLabel = self.fldTargetcompidLabel;
        self.fldTargetcompid.blankText = self.fldTargetcompidBlank;
        self.fldTargetcompid.emptyText = self.fldTargetcompidEmpty;
        self.fldBasecapital.fieldLabel = self.fldBasecapitalLabel;
        self.fldBasecapital.blankText = self.fldBasecapitalBlank;
        self.fldBasecapital.emptyText = self.fldBasecapitalEmpty;
        self.fldBalance.fieldLabel = self.fldBalanceLabel;
        self.fldBalance.blankText = self.fldBalanceBlank;
        self.fldBalance.emptyText = self.fldBalanceEmpty;
        self.fldLeverage.fieldLabel = self.fldLeverageLabel;
        self.fldLeverage.blankText = self.fldLeverageBlank;
        self.fldLeverage.emptyText = self.fldLeverageEmpty;
        self.fldRiskmultiplier.fieldLabel = self.fldRiskmultiplierLabel;
        self.fldRiskmultiplier.blankText = self.fldRiskmultiplierBlank;
        self.fldRiskmultiplier.emptyText = self.fldRiskmultiplierEmpty;
        self.fldServerurl.fieldLabel = self.fldServerurlLabel;
        self.fldServerurl.blankText = self.fldServerurlBlank;
        self.fldServerurl.emptyText = self.fldServerurlEmpty;
        self.fldServerport.fieldLabel = self.fldServerportLabel;
        self.fldServerport.blankText = self.fldServerportBlank;
        self.fldServerport.emptyText = self.fldServerportEmpty;
        self.chkUsessl.fieldLabel = self.chkUsesslLabel;
        self.btnSave.text = self.btnSaveText;
        self.btnCancel.text = self.btnCancelText;
        self.btnTestconnection.text = self.btnTestconnectionText;

        /**
         * fires when keyup on password field
         *  sets confirm field's allowblank property
         *  clears error messages if they are not needed any more
         * @event
         */
        self.fldPassword.on('keyup', function() {
            self.fldConfirmpassword.allowBlank = !self.fldPassword.getValue();
            if (self.fldConfirmpassword.allowBlank) {
                self.fldConfirmpassword.clearInvalid();
            }
        }, self);

        /**
         * called when clicked on "Save" button
         *  update the existing, or creates the new broker account
         * @event
         */
        self.btnSave.on('click', function() {
            var store;
            if (self.record) {    //update existing
                self.record.set('name', self.fldAccountname.getValue());
                self.record.set('username', self.fldUsername.getValue());
                self.record.set('account_number', self.fldAccountnumber.getValue());
                self.record.set('password', self.fldPassword.getValue());
                self.record.set('sendercompid', self.fldSendercompid.getValue());
                self.record.set('targetcompid', self.fldTargetcompid.getValue());
                self.record.set('base_equity', self.fldBasecapital.getValue());
                self.record.set('balance', self.fldBalance.getValue());
                self.record.set('leverage', self.fldLeverage.getValue());
                self.record.set('risk_multiplier', self.fldRiskmultiplier.getValue());
                self.record.set('server_url', self.fldServerurl.getValue());
                self.record.set('server_port', self.fldServerport.getValue());
                self.record.set('use_ssl', self.chkUsessl.getValue());
                store = self.record.store;
            } else {                //create new
                store = Ext.StoreMgr.lookup('AccountsStore');
                self.record = new store.recordType({
                    'id' : 0,
                    'user_id': currentUser.id,
                    'broker_id': self.cmbBrokername.getValue(),
                    'brokername': self.cmbBrokername.getRawValue(),
                    'name': self.fldAccountname.getValue(),
                    'username': self.fldUsername.getValue(),
                    'account_number': self.fldAccountnumber.getValue(),
                    'password': self.fldPassword.getValue(),
                    'sendercompid': self.fldSendercompid.getValue(),
                    'targetcompid': self.fldTargetcompid.getValue(),
                    'base_equity': self.fldBasecapital.getValue(),
                    'balance': self.fldBalance.getValue(),
                    'leverage': self.fldLeverage.getValue(),
                    'risk_multiplier': self.fldRiskmultiplier.getValue(),
                    'server_url': self.fldServerurl.getValue(),
                    'server_port': self.fldServerport.getValue(),
                    'use_ssl': self.chkUsessl.getValue()
                });
                store.add(self.record);
            }
            if (store.save() === -1) {
                self.close();
            }
        }, self);

        /**
         * called when clicked on "Cancel" button
         *  closes the window
         * @event
         */
        self.btnCancel.on('click', function() {self.close();}, self);

        /**
         * called when clicked on "Cancel" button
         *  closes the window
         * @event
         */
        self.btnTestconnection.on('click', function() {
            if (!self.record) {
                return;
            }
            Ext.Ajax.request({
                url: urls.testConnection,
                params: {'account_id': self.record.get('id')},
                success: function(response) {
                    response = ajaxSuccessHandle(response);
                    if (!response) {
                        return;
                    }
                    var popup = new AreaInfo({title: self.testConnectionTitle});
                    popup.area.setValue(response.msg);
                    popup.show();
                },
                failure: function(response) {
                    ajaxErrorHandle(response);
                }
            });
        }, self);

        /**
         * fires when the window is shown
         *  fills the window's fields in case of edit existing element
         * @event
         */
        self.on('show', function() {
            if (self.record) {
                self.fldPassword.allowBlank = true;
                self.cmbBrokername.setValue(self.record.get('broker_id'));
                self.fldAccountname.setValue(self.record.get('name'));
                self.fldUsername.setValue(self.record.get('username'));
                self.fldAccountnumber.setValue(self.record.get('account_number')),
                self.fldSendercompid.setValue(self.record.get('sendercompid'));
                self.fldTargetcompid.setValue(self.record.get('targetcompid'));
                self.fldBasecapital.setValue(self.record.get('base_equity'));
                self.fldBalance.setValue(self.record.get('balance')),
                self.fldLeverage.setValue(self.record.get('leverage'));
                self.fldRiskmultiplier.setValue(self.record.get('risk_multiplier'));
                self.fldServerurl.setValue(self.record.get('server_url'));
                self.fldServerport.setValue(self.record.get('server_port'));
                self.chkUsessl.setValue(self.record.get('use_ssl'));
            } else {
                self.fldPassword.allowBlank = false;
                self.btnTestconnection.setDisabled(true);
                self.fldRiskmultiplier.setValue(1);
            }
        }, self);
    },

    /**
     * selects the first broker if nothing is selected
     * @param {Array} records brokers
     */
    afterbrokernameAccountStoreLoad: function(records) {
        var self = this;
        if (!records.length) {
//            self.cmbBrokername.fireEvent('select', self.cmbBrokername, null);
            return;
        }
        var value = '';
        if (self.record) {
            value = self.record.get('broker_id');
        }
        comboSelect(self.cmbBrokername, value, 0);
    }
});
