/**
 * @fileOverview
 * @author vendelin8
 * @version 0.3
 */
/**
 * portfolio management
 * @class PortfolioPanel
 * @lends PortfolioPanel.prototype
 */
PortfolioPanel = Ext.extend(PortfolioPanelUi, {
    mainTitle: 'All Portfolios',
    gridHeaders: ['Id', 'Name', 'Description', 'Management type'],//, 'State'
    btnCreateText: 'Create',
    btnEditText: 'Edit',
    btnDeleteText: 'Delete',
    /** @constructs */
    initComponent: function() {
        PortfolioPanel.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        setGridColums(self, self.gridHeaders, 1);
        self.btnCreate.text = self.btnCreateText;
        self.btnEdit.text = self.btnEditText;
        self.btnDelete.text = self.btnDeleteText;

        /** portfolio store reference */
        self.portfolioStore = storeCRUD('PortfolioStore', self, self);
        /**
         * fires after the datarow data has been loaded or changed
         *  resets action buttons' enablity
         * @event
         */
        self.portfolioStore.on('load', function(store, records) {
            self.getSelectionModel().fireEvent('selectionchange');
        }, self);

        /**
         * fires when clicked on "Create Portfolio" button
         *  shows empty portfolio add/edit
         * @event
         */
        self.btnCreate.on('click', function() {
            self.popup = new AddEditPortfolio();
            self.popup.record = null;
            self.popup.show();
        }, self);

        /**
         * fires when clicked on "Edit Portfolio" button
         *  shows portfolio add/edit with portfolio properties
         * @event
         */
        self.btnEdit.on('click', function() {
            var selected = self.getSelectionModel().getSelected();
            if (!selected) {
                return;
            }
            self.popup = new AddEditPortfolio();
            self.popup.record = selected;
            self.popup.show();
        }, self);

         /**
         * fires when clicked on "Export" button
         *  shows "save as..." popup with the dns (dna) of the selected strategies
         * @event
         */
        self.btnDelete.on('click', function() {
            var selected = self.getSelectionModel().getSelections(); //get chosen datarow ids
            console.log(selected);
        }, self);

       /**
         * fires when the datarow grid's selection has been changed
         *  changes control buttons' state based on selected record's status
         * @event
         */
        self.getSelectionModel().on('selectionchange', function() {
            var record = self.getSelectionModel().getSelected();
            var newVal = (record === undefined);
            var records = self.getSelectionModel().getSelections();
            self.btnEdit.setDisabled((newVal) || (records.length > 1));
            self.btnDelete.setDisabled(newVal);
        }, self);

        /**
         * fires when the panel is rendered
         *  sets store paging size
         * @event
         */
        self.on('afterrender', function() {
            self.portfolioStore.load();
        }, self);
    }
});
