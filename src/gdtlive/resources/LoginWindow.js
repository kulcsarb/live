/**
 * @fileOverview
 * @author vendelin8, nicolas.vigh
 * @version 0.19
 */
/**
 * login functionality
 * @class LoginWindow
 * @lends LoginWindow.prototype
 */
LoginWindow = Ext.extend(LoginWindowUi, {
    mainTitle: 'Login to GDTLIVE',
    fldUsernameLabel: 'Username',
    fldUsernameBlank: 'Enter your username',
    fldUsernameEmpty: 'Your username here',
    fldPasswordLabel: 'Password',
    fldPasswordBlank: 'Enter your password',
    fldPasswordEmpty: 'Your password here',
    btnLoginText: 'Login',
    btnForgotpasswordText: 'I forgot my password',
    loginSuccess: 'Login successful',
    loginFailure: 'Login failed!',
    forgotTitle: 'Forgotten your password?',
    forgotSuccess: 'The message has been sent',
    forgotFailure: 'Message sending failed!',
    forgotHtml: 'To renew your password, type the new password  that you will use in GDTLIVE.',
    /** @constructs */
    initComponent: function() {
        LoginWindow.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        self.fldUsername.fieldLabel = self.fldUsernameLabel;
        self.fldUsername.blankText = self.fldUsernameBlank;
        self.fldUsername.emptyText = self.fldUsernameEmpty;
        self.fldPassword.fieldLabel = self.fldPasswordLabel;
        self.fldPassword.blankText = self.fldPasswordBlank;
        self.fldPassword.emptyText = self.fldPasswordEmpty;
        self.btnLogin.text = self.btnLoginText;
        self.btnForgotpassword.text = self.btnForgotpasswordText;

        /**
         * fires when clicked on "Login" button
         *  submits the login form to check authority
         * @event
         */
        self.btnLogin.on('click', function() {
            self.formLogin.getForm().submit({waitTitle: locationStrings.waitTitle,
                waitMsg: locationStrings.waitMsg,
                success: function() {
                    checkUser();
                },
                failure: function(form, action) {
                    submitErrorHandle(action, self.loginFailure);
                }});
        }, self);

        /**
         * fires when clicked on "I forgot my password" button
         *  shows enter email window
         *  hides login window
         * @event
         */
        self.btnForgotpassword.on('click', function() {
            /** initiates an {@link EnterEmailWindow} to get check user's email and send renew password link */
            var popup = new EnterEmailWindow({title: self.forgotTitle});
            popup.description.html = self.forgotHtml;

            /**
             * functionality after forgot window's "Send" click:
             *  sends email, hides forgot window, shows login window or shows error message
             * @event
             */
            popup.btnSend.on('click', function() {
                popup.formEmail.getForm().submit({waitTitle: locationStrings.waitTitle,
                    waitMsg: locationStrings.waitMsg,
                    success: function(form, action) {
                        submitSuccess(action, self.forgotSuccess, function() {
                            self.show();
                            popup.close();
                        });
                    },
                    failure: function(form, action) {
                        submitErrorHandle(action, self.forgotFailure);
                    }});
            }, self);
            /**
             * functionality after forgot window's "Cancel" click:
             *  hides forgot window, shows login window
             * @event
             */
            popup.btnCancel.on('click', function() {
                self.show();
            }, self);
            popup.formEmail.getForm().url = urls.forgotPass;
            popup.show();
            self.hide();
        }, self);

        /**
         * fires when the window is shown
         *  resets all fields on the window
         * @event
         */
        self.on('show', function() {
            self.fldUsername.reset();
            self.fldPassword.reset();
        }, self);
    }
});
