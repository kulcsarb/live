function downloadChart(id, from_date, to_date2, timeframe_num, chart) {
    var step = timeframe_num * 60000;
    var from_date_value = from_date.getTime();
    from_date_value -= from_date_value % step;
    var to_date2_value = to_date2.getTime();
    if (to_date2_value % step) {
        to_date2_value += step - (to_date2_value % step);
    }
    var downloadDetails = chart.dataToDownload(from_date_value, to_date2_value - step, to_date2_value);
//    var downloadDetails = chart.dataToDownload(from_date, to_date, to_date2);
    if (downloadDetails.length) {
	    from_date = new Date();
	    from_date.setTime(downloadDetails[0][0]);
        downloadData(timeframe_num, 0, downloadDetails, chart, from_date, {'id': id});
    }
}

/**
 * downloads next amount of data for the chart
 * @param {Date} from the date from we need the data
 */
function downloadData(timeframe_num, interval, downloadDetails, chart, from, params) {
    if (typeof(from) === 'number') {
        from = new Date(from);
    }
    var toInt = new Date(from).getTime() + timeframe_num * 300000000; //60(min->sec) * 1000(sec->millisec) * 5000(needed count)
    var finished = false;
    if (toInt >= downloadDetails[interval][1]) {
        toInt = downloadDetails[interval][1];
        finished = true;
    }
    var to_date = new Date(toInt);
    if ((!params) || (typeof(params) !== 'object')) {
        params = {};
    }
    params.fromTime = from;
    params.toTime = to_date;
    Ext.Ajax.request({
        url: urls.chartDatarow,
        params: params,
        success: function(response) {
            response = ajaxSuccessHandle(response);
            if (!response) {
                return;
            }
            response = chart.append(response, interval, finished);
            if (!finished) {
                to_date.setTime(to_date.getTime() + timeframe_num * 60000);
                downloadData(timeframe_num, interval, downloadDetails, chart, to_date, params);
            } else if (downloadDetails.length > interval + 1) {
                downloadData(timeframe_num, interval + 1, downloadDetails, chart, downloadDetails[interval + 1][0], params);
            }
        },
        failure: function(response) {
            ajaxErrorHandle(response, locationStrings.viewFailure);
        }
    });
}