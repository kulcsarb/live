/**
 * monkey patch to make Buttons in Menus available through autoref
 */
Ext.Button.prototype.initComponent = Ext.Button.prototype.initComponent.createInterceptor(function(){
    if (this.menu){
        this.menu.ownerCt = this;
    }
});

/**
 * monkey patch beacuse Ext Designer's default for JsonStore is "batch: false" (instead of Ext default's "batch: true")
 */
Ext.override(Ext.data.JsonStore, {batch: false});

/**
 * monkey patch for JsonStores that has no field for idProperty, the first will be selected,
 * so the records will not be phantom.
 */
Ext.data.JsonStore.prototype.constructor = Ext.data.JsonStore.prototype.constructor.createSequence(function(){
    var self = this;
    var i, key;
    var found = false;
    for (i in self.fields.keys) {
        key = self.fields.keys[i];
        if (typeof(key) === 'function') {
            continue;
        }
        if (self.idProperty === key) {
            found = true;
            break;
        }
    }
    if (!found) {
//        self.idProperty = self.fields.keys[0];
        console.log(self.storeId + ' has no id field, records will be phantom!');
    }
});

/**
 * monkey patch for changing Ext.form.Label's font size to 12px (it was smaller then the standard form elements' fieldLabel) 
 */
var extrafont = ';font-size:12px; padding:3px 3px 3px 0'
Ext.override(Ext.form.Label, {style: Ext.form.Label.prototype.style ? Ext.form.Label.prototype.style + extrafont : extrafont});

/**
 * monkey patch for Ext html editor that returns '<br>' for when the editor has been cleared,
 * so the editor returns '' in case of it only contains html tags
 */
Ext.override(Ext.form.HtmlEditor, {
    getValue: function() {
        this[this.sourceEditMode ? 'pushValue' : 'syncValue']();
        var value = Ext.form.HtmlEditor.superclass.getValue.call(this);
        value = value ? value : '';
        // only html tags
        value = (Ext.isEmpty(value.replace(/<[a-zA-Z][^>]*>/igm, ' ').trim())) ? '' : value;
        // ending <br>
        value = value.replace(/<br>$/im, '');

        return value;
    }
});

/**
 * monkey patch for Ext multislider to add a tooltip plugin that shows the thumbs' value while dragging
 */
Ext.slider.MultiSlider.prototype.initComponent = Ext.slider.MultiSlider.prototype.initComponent.createInterceptor(function(){
    var self = this;
    self.tooltip = new Ext.slider.Tip();
    self.initPlugin(self.tooltip);
});

/**
 * monkey patch for Ext gridpanel, because in case of a Ext Designer's gridpanel has a checkbox selection model,
 * it forgets to insert it as first column
 */
Ext.grid.GridPanel.prototype.afterRender = Ext.grid.GridPanel.prototype.afterRender.createSequence(function() {
    var self = this;
    if ((self.getSelectionModel() instanceof Ext.grid.CheckboxSelectionModel) && (self.getColumnModel().config[0] !== self.getSelectionModel())) {
        self.getColumnModel().config.unshift(self.getSelectionModel());
    }
});

/**
 * monkey patch for Ext html editor to accept keyup events
 */
Ext.override(Ext.form.HtmlEditor, {onKeyUp: function(e) {this.fireEvent('keyup', this, e)}});
Ext.form.HtmlEditor.prototype.initEditor = Ext.form.HtmlEditor.prototype.initEditor.createSequence(function() {
    Ext.EventManager.on(this.getDoc(), 'keyup', this.onKeyUp, this, {buffer: 100});
});

/**
 * monkey patch to auto submit on enter
 *  when a button is rendered with type == "submit", a recursive call sets all parents' submitButton property
 *      button property is set to this button
 *      level property is set to the distance from the button to the parent container
 *  an existing submitButton is overriden when the new distance is shorter than the existing one
 */
Ext.Button.prototype.render = Ext.Button.prototype.render.createSequence(function() {
    var cont, i;
    if (this.type === 'submit') {
        cont = this;
        i = 0;
        while (cont) {
            if ((!cont.submitButton) || ((cont.submitButton.button.hidden) && (cont.submitButton.level > i))) {
                cont.submitButton = {'button': this, 'level': i};
            }
            i++;
            cont = cont.ownerCt;
        }
    }
});
/**
 * the other part is calling these keys' "click" event when a TextField gets an enter
 *  in case of a enter it searches for the nearest parent container that has a submit button, and clicks it if there is one
 */
Ext.form.TextField.prototype.fireKey = Ext.form.TextField.prototype.fireKey.createSequence(function(e) {
    var self = this;
    if ((!e.isSpecialKey()) || (e.getKey() !== e.ENTER)) {
        return;
    }
    var cont = self;
    while (cont) {
        if ((cont.submitButton) && (!cont.submitButton.button.disabled)) {
            cont.submitButton.button.fireEvent('click');
            return;
        }
        cont = cont.ownerCt;
    }
});

/**
 * monkey patch to close (or hide if closeAction is hide) Window on escape
 */
Ext.override(Ext.Window, {fireKey: function(e) {
    if ((e.isSpecialKey()) && (e.getKey() === e.ESC)) {
        if (this.closeAction === 'close') {
            this.close();
        } else {
            this.hide();
        }
    }
}});
Ext.Window.prototype.initEvents = Ext.Window.prototype.initEvents.createSequence(function() {
    this.mon(this.el, Ext.EventManager.getKeyEvent(), this.fireKey,  this);
});

/**
 * monkey patch for aligning Windows to top-center
 */
Ext.Window.prototype.beforeShow = Ext.Window.prototype.beforeShow.createSequence(function() {
    if (this.getHeight() > Ext.getBody().getHeight()) {
        this.alignTo(Ext.getBody(), "t", [-this.getWidth() * 0.5, 0]);
    }
});

/**
 * monkey patch for dynamic setting of Field's label
 */
Ext.override(Ext.form.Field, {setFieldLabel: function(text) {
    if (this.rendered) {
        var labelSeparator = this.labelSeparator;
        if (typeof labelSeparator == 'undefined') {
            if (this.ownerCt && this.ownerCt.layout && typeof this.ownerCt.layout.labelSeparator != 'undefined') {
                labelSeparator = this.ownerCt.layout.labelSeparator;
            } else {
                labelSeparator = '';
            }
        }
        var formItem = this.el.up('.x-form-item', 10);
        if (formItem) {
            var label = formItem.child('.x-form-item-label');
            if (label) {
                label.update(text + labelSeparator);
            }
        }
    } else {
        this.fieldLabel = text;
    }
}});

/**
 * monkey patch for fixing ext bug: if a container is added to a form, the container's childs don't have any font set
 * tipically for checkboxes
 */
Ext.form.Checkbox.prototype.initComponent = Ext.form.Checkbox.prototype.initComponent.createSequence(function() {
    if ((this.ownerCt) && (!this.ownerCt.fieldLabel) && (this.ownerCt.layout !== 'form') && (this.ownerCt.layout.type !== 'form')) {
        this.ownerCt.fieldLabel = 'nincsneki';
        this.ownerCt.hideLabel = true;
    }
});

/**
 * monkey patch for combo box
 * this code needs to be called before the combo box's store has been filtered
 */
Ext.form.ComboBox.prototype.initComponent = Ext.form.ComboBox.prototype.initComponent.createSequence(function() {
    if (this.triggerAction == 'all') {
        this.doQuery(this.allQuery, true);
    } else {
        this.doQuery(this.getRawValue());
    }
});

/**
 * monkey patch for fieldset
 * auto disable non-disabled fields on collapse and restore them on expand
 */
Ext.form.FieldSet.prototype.initComponent = Ext.form.FieldSet.prototype.initComponent.createSequence(function() {
    /**
     * fires when the fieldset is collapsed
     *  disable form items that are not disabled, and saves newly diabled list
     */
    this.on('collapse', function() {
        var i;
        var autoDisabled = [];
        var items = this.items.items
        for (i in items) {
            if ((typeof(items[i]) === 'function') || (!items[i].setDisabled) || (typeof(items[i].setDisabled) !== 'function')) {
                continue;
            }
            if (!items[i].disabled) {
                autoDisabled.push(items[i]);
                items[i].setDisabled(true);
            }
        }
        this.autoDisabled = autoDisabled;
    }, this);

    /**
     * fires when the fieldset is expanded
     *  enables items that were disabled during last collapse
     */
    this.on('expand', function() {
        if ((!this.autoDisabled) || (!this.autoDisabled.length)) {
            return;
        }
        var i;
        for (i in this.autoDisabled) {
            if (typeof(this.autoDisabled[i]) === 'function') {
                continue;
            }
            this.autoDisabled[i].setDisabled(false);
        }
        delete this.autoDisabled;
    }, this);
});

/**
 * sets the given gridpanel's column titles to the given titles
 * @param {object} grid the gridpanel
 * @param {array} columns the title strings
 * @param {intege/undefined} startIndex where to start columns (eg. in case of checkbox selection it is > 0)
 */
function setGridColums(grid, columns, startIndex) {
    if (startIndex === undefined) {
        startIndex = 0;
    }
    grid.on('afterrender', function() {
        var cm = grid.getColumnModel();
        var max = columns.length - 1;
        var i;
        for (i = 0; i < max; i++) {
//            if (columns[i]) {
                cm.setColumnHeader(i + startIndex, columns[i]);
//            }
        }
    }, grid);
}

Ext.override(Ext.form.BasicForm, {
    getFieldValues: function(dirtyOnly) {
        var o = {},
            n,
            key,
            val;
        this.items.each(function(f) {
            if (!f.disabled && (f.name || f.submitValue) && (dirtyOnly !== true || f.isDirty())) {
                n = f.getName();
                key = o[n];
                val = f.getValue();
    
                if(Ext.isDefined(key)){
                    if(Ext.isArray(key)){
                        o[n].push(val);
                    }else{
                        o[n] = [key, val];
                    }
                }else{
                    o[n] = val;
                }
            }
        });
        return o;
    }
});

/**
 * monkey patch for default columnModel config params
 */
Ext.override(Ext.grid.Column, {editable: false, groupable: false});

/**
 * monkey patch for default Field config params
 */
Ext.override(Ext.form.Field, {msgTarget: 'side'});

/**
 * monkey patch for gridView getScrollOffset
 */
Ext.override(Ext.grid.GridView, {getScrollOffset: function() {
    var self = this;
    return hasScrollBar(self.grid) ? (Ext.getScrollBarWidth() + 3) : 5;
}});

function getRadioValue(owner, field, start, end) {
    for (i = start; i < end; i++) {
        if (owner[field + i].getValue()) {
            return i;
        }
    }
}

function setRadioValue(owner, field, value) {
    owner[field + value].setValue(true);
}

function getRowCount(grid) {
        var headerHeight = 24;
        var rowHeight = 21;
        return Math.floor((grid.getInnerHeight() - headerHeight) / rowHeight);
}

function setPageSize(grid, loadFunction, stores) {
    stores = stores || [];
    if (!stores.length) {
        stores.push(grid.getStore());
    }
    var i;
    grid.on('resize', function() {
        if (grid.pageSizeSet) {
            return;
        }
        grid.pageSizeSet = true;
        var pageSize = getRowCount(grid);
        grid.getBottomToolbar().pageSize = pageSize;
        for (i = 0; i < stores.length; i++) {
            stores[i].setBaseParam('limit', pageSize);
        }
        if (loadFunction) {
            loadFunction();
        }
    }, grid);
}

function copyStore(storeClass, newStoreId) {
    store = new (window[storeClass])()
    Ext.StoreMgr.unregister(store);
    store.storeId = newStoreId;
    Ext.StoreMgr.register(store);
}

//function addTreeNodeChildren(response, node, parentIds, childCallBack) {
//    parentIds = parentIds || [];
//    if (!isArray(parentIds)) {
//        parentIds = [parentIds];
//    }
//    if (typeof(response) === 'string') {
//        response = Ext.util.JSON.decode(response);
//    }
//    var i, j, params, child, newParentIds, date;
//    for (i in response) {
//        params = response[i];
//        if (typeof(params) === 'function') {
//            continue;
//        }
//        if ('children' in params) {
//            child = new Ext.tree.AsyncTreeNode({'text': params.name, 'leaf': false, 'expanded': true, 'expandable': true});
//            child.loaded = true;
//        } else {
//            child = new Ext.tree.TreeNode({'text': params.from_date + ' - ' + params.to_date, 'leaf': true});
//        }
//        if ('from_date' in params) {
//            child.loadParams = {'id': parseInt(i), 'parentIds': parentIds, 'from_date': Date.parseDate(params.from_date, "Y-m-d H:i:s"), 'to_date': Date.parseDate(params.to_date, "Y-m-d H:i:s")};
//        }
//        node.appendChild(child);
//        if ('children' in params) {
//            newParentIds = parentIds.slice();
//            newParentIds.push(parseInt(i));
//            addTreeNodeChildren(params.children, child, newParentIds, childCallBack);
//        } else if ((childCallBack) && (typeof(childCallBack) === 'function')) {
//            childCallBack(parseInt(i), parentIds);
//        }
//    }
//}

function addTreeNodeChildren(response, node, loadParamsCallback, parentIds) {
    parentIds = parentIds || [];
    if (!isArray(parentIds)) {
        parentIds = [parentIds];
    }
    if (typeof(response) === 'string') {
        response = Ext.util.JSON.decode(response);
    }
    var i, iInt, j, params, child, newParentIds, date;
    for (i in response) {
        params = response[i];
        if (typeof(params) === 'function') {
            continue;
        }
        iInt = parseInt(i);
        if ('children' in params) {
            child = new Ext.tree.AsyncTreeNode({'text': params.name, 'leaf': false, 'expanded': true, 'expandable': true});
            child.loaded = true;
        } else {
            child = new Ext.tree.TreeNode({'text': params.from_date + ' - ' + params.to_date, 'leaf': true});
        }
        node.appendChild(child);
        if ('from_date' in params) {
            child.loadParams = {'from_date': Date.parseDate(params.from_date, "Y-m-d H:i:s"), 'to_date': Date.parseDate(params.to_date, "Y-m-d H:i:s")};
            loadParamsCallback(child, iInt, parentIds);
        }
        if ('children' in params) {
            newParentIds = parentIds.slice();
            newParentIds.push(iInt);
            addTreeNodeChildren(params.children, child, loadParamsCallback, newParentIds);
        }
    }
}

function hasScrollBar(grid) {
    return getRowCount(grid) < grid.getStore().getCount();
//    return grid.scrollHeight > grid.clientHeight;
}

function comboSelect(combo, value, defaultIndex) {
    var store = combo.getStore();
    var record;
    if (value !== undefined) {
        if (value === true) {
            value = combo.getValue();
        }
        var searchIndex = store.findExact(combo.valueField, value);
        if (searchIndex !== -1) {
            record = store.getAt(searchIndex);
        }
    }
    if ((record === undefined) && (defaultIndex !== undefined)) {
	    try {
	        record = store.getAt(defaultIndex);
            value = record.get(combo.valueField);
	    } catch (e) {
            console.log('comboSelect storeId:', store.storeId, 'records', store.getRange().slice());
            console.trace();
	        throw e;
	    }
    }
    if (record !== undefined) {
	    combo.setValue(value);
	    combo.fireEvent('select', combo, record);
    }
}

function confirmWindow(title, html, callbackYes) {
    var popup = new YesNoWindow({title: title, html: html});
    /**
     * functionality after confirm window's 'Yes' click:
     * @event
     */
    popup.btnYes.on('click', function() {
        callbackYes(popup.close.bind(popup));
    });
    popup.show();
}