/**
 * compares 2 js objects by their properties
 * beware of putting Ext objects to it, because they contain chain-references to their parents and children which will probably lead to an infinity loop
 * @param {object} a the first object to compare
 * @param {object} b the second object to compare
 * @return {Boolean} if they are equal
 */
function compareObjects(a, b) {
    if ((a === undefined) || (b === undefined)) return false;
    if ((a === null) || (b === null)) return false;
    for (var p in a) {
        if (typeof(a[p]) !== typeof(b[p])) return false;
        if ((a[p] === null) !== (b[p] === null)) return false;
        switch (typeof(a[p])) {
            case 'undefined':
                if (typeof(b[p]) != 'undefined') return false;
                break;
            case 'object':
                if ((a[p] !== null) && (b[p] !== null) && ((a[p].constructor.toString() !== b[p].constructor.toString()) || (!compareObjects(a[p], b[p])))) return false;
                if ((a[p].valueOf) && (a[p].valueOf() !== b[p].valueOf())) return false;
                break;
            case 'function':
                if (p != 'equals' && a[p].toString() != b[p].toString()) return false;
                break;
            default:
                if (a[p] !== b[p]) return false;
        }
    }
    return true;
}

/**
 * desides from a string if it contains a class name or not
 * @param {String} c the input string wgich is probably a class name
 * @return {Boolean} if it is a class name ir not
 */
function classExist(c) {
    try {
        c = eval(c);
    } catch(e) {
        return false;
    }
    return (typeof(c) === "function") && (typeof(c.prototype) === "object");
}

/**
 * decodes a given JSON encoded string and joins it to "key: value\n" lines
 * @param {String} inputString the JSON encoded string
 * @return {String} the lines
 */
function truncateStringToLines(inputString) {
    var obj = decodeMsg(inputString);
    var ret = '';
    for (i in obj) {
        if (typeof(obj[i]) === 'function') {
            continue;
        }
        ret += i + ': ' + obj[i] + '\n';
    }
    return ret;
}

function isEmpty(ob){
    for (var i in ob) {
        return false;
    }
    return true;
}

function collectIds(records, idString) {
    idString = idString || 'id';
    var record, i;
    var ids = [];
    for (i in records) {
        record = records[i];
        if (typeof(record) === 'function') {
            continue;
        }
        ids.push(record.get(idString));
    }
    return ids;
}

function randomString(length) {
    length = length || 10;
    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 8;
    var randomstring = '';
    for (var i=0; i<string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum,rnum+1);
    }
    return randomstring;
}

function isInt(n) {
   return n % 1 === 0;
}

function clone(obj) {
    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        var copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        var copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}