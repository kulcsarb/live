/**
 * tries to JSON decode a given string
 * @param {String} msg a response string we need to decode
 * @return {String} decoded (or the original if unsuccessful) string
 */
function decodeMsg(msg) {
    var decoded = '';
    try {
        decoded = Ext.util.JSON.decode(msg);
    } catch (e) {
        decoded = msg;
    }
    return decoded;
}

/**
 * handles form submit error
 * @param {Object} action contains information about the error
 * @param {String|undefined} defaultError a default error message to show in case of the response does not contain one
 */
function submitErrorHandle(action, defaultError) {
    if (!defaultError) {
        defaultError = locationStrings.serverConnectionError;
    }
    if (action.failureType === Ext.form.Action.CONNECT_FAILURE) {
        Ext.Msg.alert(locationStrings.failure, locationStrings.serverConnectionError + action.response.status + ' ' + action.response.statusText);
        return;
    }
    var result = decodeMsg(action.result);
    if (!result) {
        Ext.Msg.alert(locationStrings.failure, defaultError);
        return;
    }
    var msg = decodeMsg(result.msg);
    if ((action.failureType === Ext.form.Action.SERVER_INVALID) && (msg)) {
        Ext.Msg.alert(locationStrings.failure, msg);
    } else {
        Ext.Msg.alert(locationStrings.failure, defaultError);
    }
}

/**
 * handles a successful form submit action
 *  shows message
 *  calls a given function if set
 * @param {Object} action contains response information
 * @param {String} defaultMessage a default message to show in case of the response does not contain one
 * @param {Function|undefined} functionAfter called after the message box has been closed
 */
function submitSuccess(action, defaultMessage, functionAfter) {
    if (action.result.msg) {
        Ext.Msg.alert(locationStrings.success, action.result.msg, functionAfter);
    } else {
        Ext.Msg.alert(locationStrings.success, defaultMessage, functionAfter);
    }
}

/**
 * handles ajax error
 * @param {object} response the response that contains information about the error
 * @param {String|undefined|null} defaultError a default error message to show if the response does not contain one
 * @param {Boolean|undefined} returnError to return or show the error message
 * @return {String|Boolean} the error message or false
 */
function ajaxErrorHandle(response, defaultError, returnError) {
    if (!defaultError) {
        defaultError = locationStrings.serverConnectionError;
    }
    response = decodeMsg(response);
    var error;
    if ((!response) || (!response.responseText)) {
        error = defaultError;
    } else {
        response = decodeMsg(response.responseText);
        if (!response.msg) {
            error = defaultError;
        } else {
            error = decodeMsg(response.msg);
        }
    }
    if (returnError) {
        return error;
    }
    Ext.Msg.alert(locationStrings.failure, error);
    return false;
}

/**
 * handles successful ajax call
 *  checks if it is really a successful response, calls error handle if not
 * @param {Object} response contains the response information
 * @param {String|undefined|null} defaultError a default error message to show in case of the response does not contain one
 * @param {Boolean|undefined} returnError to return or show the error message
 * @return {Boolean|Object} false on error, the decoded response otherwise
 */
function ajaxSuccessHandle(response, defaultError, returnError) {
    if (!defaultError) {
        defaultError = locationStrings.serverConnectionError;
    }
    if (returnError === undefined) {
        returnError = false;
    }
    response = decodeMsg(response);
    if (!response) {
        return ajaxErrorHandle(response, defaultError, returnError);
    }
    var responseText = decodeMsg(response.responseText);
    if ((!responseText) || (responseText.success === false)) {
        return ajaxErrorHandle(response, defaultError, returnError);
    }
    return responseText;
}

/**
 * tests if the given response really contains error
 * @param {Object} response the ajax response
 * @return {Boolean} false if it is not an error
 */
function storeTestError(response) {
    var responseText = decodeMsg(response.responseText);
    if (!responseText) {
        responseText = decodeMsg(response.raw);
    }
    if (responseText.success === true) {
        return false;
    }
    return true;
}

/**
 * handles a store exception
 * @param {Object} proxy the proxy object of the store
 * @param {String} type can be one of  ['response', 'remote'], first defines a communication error, second is a server error
 * @param {String} action the action while the error has occured, can be one of ['create', 'read', 'update', 'destroy']
 * @param {} options
 * @param {Object} response the error response that contains information about the error
 * @param {String|undefined|null} defaultError a default error message to show in case of the response does not contain one
 */
function storeErrorHandle(proxy, type, action, options, response, defaultError) {
    if (!defaultError) {
        defaultError = locationStrings.storeFailure;
    }
    if (!storeTestError(response)) {
        return false;
    }
    var responseText = decodeMsg(response.responseText);
    if ((type === 'response') && (response.status !== 200)) {
        Ext.Msg.alert(locationStrings.failure, locationStrings.serverConnectionError);
    } else if ((type === 'remote') || ((type === 'response') && (response.status === 200))) {
        if ((response.raw) && (response.raw.msg)) {
            Ext.Msg.alert(locationStrings.failure, decodeMsg(response.raw.msg));
        } else if ((responseText) && (responseText.msg)) {
            Ext.Msg.alert(locationStrings.failure, responseText.msg);
        } else {
            Ext.Msg.alert(locationStrings.failure, defaultError);
        }
    } else {
        Ext.Msg.alert(locationStrings.failure, locationStrings.unknownError);
    }
    return true;
}