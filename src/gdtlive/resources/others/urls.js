/**
 * the urls used by js components (without the ones used in 'do not hand edit' Ext Designer files)
 */
var urls = {
    abortBatch: 'batch/stop',
    abortLive: 'liverun/abort',
    actualBatch: 'batch/actual',
    accountEngineDetails: 'web/get_engine_details',
    accountEngineLog: 'web/get_engine_logs',
    accountEngineMessages: 'web/get_engine_messages',
    accountEngineConnect: 'web/engine_connect',
    accountEngineDisconnect: 'web/engine_disconnect',
    accountGraph: 'liveaccount/periods',
    accountInfo: 'liveaccount/info',
    accountRunHistoryTree: 'liveaccount/runHistoryTree',
    accountRunSymbolTimeframes: 'liveaccount/runSymbolTimeframes',
    accountPerformancesum: 'liveaccount/perf_summary',
    addBatch: 'batch/add',
    addremoveList: 'list/addRemove',
    addToPortfolio: 'portfolio/addSub',
    cancelDatarow: 'download/cancel',
    chartDatarow: 'datarow/get',
    chartStart: 'datarow/getStart',
    checkSearchEmpty: 'search/isEmpty',
    checkExport: 'strategy/isExportOk',
    cyclePerformancesum: 'evolcycle/perf_summary',
    cycleRunsettings: 'evolcycle/info',
    deleteHistory: 'history/delete',
    deleteLive: 'liverun/delete',
    deleteRecursiveCycles: 'evolcycle/deleteAll',
    deleteRecursiveEvolruns: 'evolrun/deleteAll',
    deleteRecursiveEvols: 'evolconfig/deleteAll',
    deleteRecursiveWorkflowruns: 'workflowrun/deleteAll',
    deleteRecursiveWorkflows: 'workflow/deleteAll',
    delLog: 'log/delete',
    evolGraph: 'evolrun/performance',
    evolInfo: 'evolconfig/info',
    forgotPass: 'login/forgotpass',
    getDistributions: 'distribution/getPlugins',
    getEvol: 'evolconfig/get',
    getGenomeTree: 'info/genometree',
    getLog: 'log/get',
    getPersonalSettings: 'settings/get',
    getSearch: 'search/get',
    getWorkflow: 'workflow/info',
    liveStart: 'web/start',
    liveStop: 'web/stop',
    liveAbort: 'web/abort',
    logLive: 'liverun/log',
    logout: 'login/logout',
    popupMessages: 'message/getRecents',
    saveEvol: 'evolconfig/save',
    savePersonalSettings: 'settings/save',
    saveWorkflow: 'workflow/save',
    sendLog: 'log/email',
    startBacktest: 'batch/startBacktest',
    strategyAddToLive: 'livelist/add',
    strategyDns: 'strategy/get_DNS',
    strategyExport: 'strategy/export',
    strategyGraph: 'strategyrun/periods',
    strategyInfo: 'strategy/info',
    strategyMm: 'strategy/mmplugin',
    strategyPerformancesum: 'strategyrun/perf_summary',
    strategyRunsettings: 'strategyrun/info',
    strategyRunHistoryTree: 'strategy/runHistoryTree',
    testConnection: 'web/test_connection',
    testHistoric: 'historic/testRow',
    tradeRepair: 'web/trade_repair',
    tradeClose: 'web/trade_close',
    tradesClose: 'web/trades_close',
    tradeMarkasclosed: 'web/trade_markasclosed',
    tradeFixmessages: 'web/trade_fixmessages',
    tradeSendopen: 'web/trade_mo_sendopen',
    tradeSendclose: 'web/trade_mo_sendclose',
    tradePlace: 'web/trade_co_place',
    tradeCancel: 'web/trade_co_cancel',
    tradeMarkasfilled: 'web/trade_co_filled',
    treeAccounts: 'liveaccount/treelist',
    treeCycles: 'evolcycle/treelist',
    treeEvolruns: 'evolrun/treelist',
    treeEvols: 'evolconfig/treelist',
    treePortfolios: 'portfolio/treelist',
    treeWorkflowruns: 'workflowrun/treelist',
    treeWorkflows: 'workflow/treelist',
    userInfo: 'login/user_info',
    workflowInfo: 'workflow/generalinfo'
};