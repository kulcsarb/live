/**
 * Additional vtypes.
 */
var fullnameCharsStart = 'a-zA-ZáàâäéèêíóòôöőúùûüűÁÀÂÄÉÈÊÍÓÒÔÖŐÚÙÛÜŰß';
var fullnameCharsExtra = " .";
Ext.apply(Ext.form.VTypes, {
    fullname: function(val) {
        var re = new RegExp("^[" + fullnameCharsStart + "][" + fullnameCharsStart + fullnameCharsExtra + "]*$");
        return re.test(val);
    },
    fullnameText: locationStrings.vtypeNameText,
    fullnameMask: new RegExp("[" + fullnameCharsStart + fullnameCharsExtra + "]"),

    password: function(val, field) {
        var passwordField = field.refOwner.fldPassword;
        if (passwordField) {
            return (val === passwordField.getValue());
        }
        return true;
    },
    passwordText: locationStrings.vtypeConfirmText,
    minfield: function(val, field) {
        if (!field.theOther) {
            return false;
        }
        if ((val !== undefined) && (val !== '')) {
            if (field.theOther.getValue() >= val) {
                field.theOther.clearInvalid();
                return true;
            } else {
                field.theOther.markInvalid(locationStrings.vtypeMaxfieldText);
                return false;
            }
        }
        return false;
    },
    minfieldText: locationStrings.vtypeMinfieldText,
    maxfield: function(val, field) {
        if (!field.theOther) {
            return false;
        }
        if ((val !== undefined) && (val !== '')) {
            if (field.theOther.getValue() <= val) {
                field.theOther.clearInvalid();
                return true;
            } else {
                field.theOther.markInvalid(locationStrings.vtypeMinfieldText);
                return false;
            }
        }
        return false;
    },
    maxfieldText: locationStrings.vtypeMaxfieldText,
    fromfield: function(val, field) {
        if (!field.theOther) {
            return false;
        }
        if ((val !== undefined) && (val !== '')) {
            if (field.theOther.getValue() > val) {
                field.theOther.clearInvalid();
                return true;
            } else {
                field.theOther.markInvalid(locationStrings.vtypeTofieldText);
                return false;
            }
        }
        return false;
    },
    fromfieldText: locationStrings.vtypeFromfieldText,
    tofield: function(val, field) {
        if (!field.theOther) {
            return false;
        }
        if ((val !== undefined) && (val !== '')) {
            if (field.theOther.getValue() < val) {
                field.theOther.clearInvalid();
                return true;
            } else {
                field.theOther.markInvalid(locationStrings.vtypeFromfieldText);
                return false;
            }
        }
        return false;
    },
    tofieldText: locationStrings.vtypeTofieldText,
    alphaCustom: function(val, field)  {
        var params = field.vtypeParams;
        var str = "[0-9]{1," + params[0] + "}";
        if (params[1] !== 0) {
            str += "(\\.([0-9]{1," + params[1] + "})?)?";
        }
        var re = new RegExp("^" + str + "$");
        return re.test(val);
    },
    alphaCustomText: locationStrings.vtypeCustomAlpha,
    email: function(val) {
        var re = new RegExp("^[^@]+@[^@]+$");
        return re.test(val);
    },
    emailText: locationStrings.emailText
});