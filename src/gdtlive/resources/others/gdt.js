function startEvol(ids, window) {
    if (!ids.length) {
        ids = [ids];
    }
    Ext.Ajax.request({
        url: urls.addBatch,
        params: {task_type: 1,          //evol
            userId: currentUser.id,     //user id
            task_configId: Ext.util.JSON.encode(ids)    //evol config id (based on the response of the recent save)
        },
        success: function(response) {
            response = ajaxSuccessHandle(response, locationStrings.startFailure);
            if (!response) {
                return;
            }
            if (window) {
                window.hide();
            }
            mainViewport.btnTasks.fireEvent('click');
        },
        failure: function(response) {
            ajaxErrorHandle(response, locationStrings.startFailure);
        }
    });
}

/**
 * function for Peak-through indicator data generator function.
 * @param {Array} xData x-axis data
 * @param {Array} yData y-axis data
 * @param {Array} parameters the paramteres of the indicator
 * @return {Array} the indicator data to be set on the plot
 */
vendelinPeakthrough = function(xData, yData, parameters) {
    var period = parameters[0];
    var evaluationFunction = parameters[1];
    var mult2 = parameters[2];
    var i, xDatai, newValue, diff, sqr, dir, dir2;
    var result = [];
    var sum = 0;
    var squares = [];
    var d = new Array(period);
    var oldValue = evaluationFunction(yData[0]);
    var mult = 1 / period;
    for (i = 1; i < period; i++) {
        newValue = evaluationFunction(yData[i])
        diff = newValue - oldValue;
        sqr = diff * diff;
        squares.push(sqr);
        sum += sqr;
        oldValue = newValue;
    }
    squares.push(0);
    var squareIndex = period - 1;
    for (; i < yData.length; i++) {
        sum -= squares[squareIndex];
        newValue = evaluationFunction(yData[i]);
        diff = newValue - oldValue;
        sqr = diff * diff;
        squares[squareIndex] = sqr;
        sum += sqr;
        oldValue = newValue;
        squareIndex = (squareIndex + 1) % period;
        d.push(Math.sqrt(sum * mult) * mult2);
    }
    oldValue = evaluationFunction(yData[0]);
    newValue = evaluationFunction(yData[1]);
    dir = (newValue > oldValue) ? 1 : 0;    //where the chart moves
    dir2 = dir;     //if the last saved was a peak or valley
    var lastValue = dir ? -Infinity : Infinity;   //last peak or valley value
    
    result.push({'time': xData[0], 'position': dir});
    var tmp = 0;//[oldValue, 0];
    oldValue = newValue;
    var lastValue2;       //before last peak or valley value
    for (i = 2; i < yData.length; i++) {
        if (!d[i]) {
            continue;
        }
        newValue = evaluationFunction(yData[i]);
        if ((dir == 1) != (newValue > oldValue)) {
            if (((dir2 == 0) && (lastValue - oldValue > d[i])) || ((dir2 == 1) && (oldValue - lastValue > d[i]))) {
                if (tmp) {
                    result.push({'time': xData[tmp[1]], 'position': dir2});
                    lastValue2 = lastValue;
                }
                tmp = [oldValue, i - 1];
                lastValue = oldValue;
                dir2 = 1 - dir2;
            }
        }

        dir = (newValue > oldValue) ? 1 : 0;

        if ((tmp) && (dir != dir2)) {
            if (((dir == 1) && (newValue >= tmp[0])) || ((dir == 0) && (newValue <= tmp[0]))) {
                tmp = 0;
                lastValue = lastValue2;
                dir2 = 1 - dir2;
            }
        }
        oldValue = newValue;
    }
    result.splice(0, 1);
    return result;
}

/**
 * The Peak-Through lines indicator data generator function.
 * @param {Array} xData x-axis data
 * @param {Array} yData y-axis data
 * @param {Array} parameters the paramteres of the indicator
 * @return {Array} the indicator data to be set on the plot
 */
vendelinLines = function(xData, yData, parameters) {
    var period = parameters[0];
    var evaluationFunction = parameters[1];
    var mult2 = parameters[2];
    var i, newValue, diff, sqr, dir, dir2;
    var sum = 0;
    var squares = new Array(period);
    var d = new Array(xData.length);
    d[0] = null;
    var result = new Array(xData.length);
    var oldValue = evaluationFunction(yData[0]);
    var mult = 1 / period;
    for (i = 1; i < period; i++) {
        newValue = evaluationFunction(yData[i]);
        diff = newValue - oldValue;
        sqr = diff * diff;
        squares[i - 1] = sqr;
        sum += sqr;
        oldValue = newValue;
        d[i] = null;
        result[i - 1] = null;
    }
    squares[period - 1] = 0;
    var squareIndex = period - 1;
    for (; i < yData.length; i++) {
        sum -= squares[squareIndex];
        newValue = evaluationFunction(yData[i]);
        diff = newValue - oldValue;
        sqr = diff * diff;
        squares[squareIndex] = sqr;
        sum += sqr;
        oldValue = newValue;
        squareIndex = (squareIndex + 1) % period;
        d[i] = Math.sqrt(sum * mult) * mult2;
    }
    i = 2;
    while ((!d[i]) && (i < yData.length)) {
        i++;
    }
    oldValue = evaluationFunction(yData[i - 2]);
    newValue = evaluationFunction(yData[i - 1]);
    dir = (newValue > oldValue) ? 1 : 0;    //where the chart moves
    dir2 = dir;     //if the last saved was a peak or valley
    var lastValue = dir ? -Infinity : Infinity;   //last peak or valley value
    var lastIndex = i - 1;
    var tmp = 0;
    oldValue = newValue;
    var peak, valley, lastValue2;       //before last peak or valley value
    var lastIndex2 = 0;
    var swap;
    for (; i < yData.length; i++) {
//console.log(new Date(xData[i]).format('d H'));
        newValue = evaluationFunction(yData[i]);
        if (((dir == 1) != (newValue > oldValue)) && (((dir2 == 0) && (lastValue - oldValue > d[i])) || ((dir2 == 1) && (oldValue - lastValue > d[i])))) {
//console.log('dir change');
            if ((tmp) || (lastIndex)) {
//console.log('tmp, save lastValue2, lastIndex2');
                lastValue2 = lastValue;
                lastIndex2 = lastIndex;
            }
            lastIndex = i - 1;
            tmp = oldValue;
            lastValue = oldValue;
            dir2 = 1 - dir2;
        }


        dir = (newValue > oldValue) ? 1 : 0;

        if (((tmp) && (dir != dir2)) && (((dir == 1) && (newValue >= tmp)) || ((dir == 0) && (newValue <= tmp)))) {
            tmp = 0;
//console.log('del tmp, lastValue2:', lastValue2, 'lastValue:', lastValue);
//            if (lastValue2) {
            swap = lastValue;
            lastValue = lastValue2;
            lastValue2 = swap;
//            } else {
//                lastValue = null;
//            }
            swap = lastIndex;
            lastIndex = lastIndex2;
            lastIndex2 = swap;
            dir2 = 1 - dir2;
        }

        if (dir2 == 0) {
            peak = yData[lastIndex][1];
            if (lastIndex2) {
                valley = yData[lastIndex2][2];
            } else {
                valley = null;
            }
        } else {
            valley = yData[lastIndex][2];
            if (lastIndex2) {
                peak = yData[lastIndex2][1];
            } else {
                peak = null;
            }
        }
//console.log('dir2 ==', dir2, ' peak:', peak, 'valley:', valley);
        oldValue = newValue;
        result[i - 1] = [peak, valley];
    }
    result[yData.length - 1] = result[yData.length - 2];
    return result;
}

/**
 * The Peak-Through lines indicator data generator function as TALIB MIN and MAX.
 * @param {Array} xData x-axis data
 * @param {Array} yData y-axis data
 * @param {Array} parameters the paramteres of the indicator
 * @return {Array} the indicator data to be set on the plot
 */
talibMinmax = function(xData, yData, parameters) {
    var period = parameters[0];
    var i, newElement, currentMax, currentMin, removeElement;
    var result = new Array(period - 1);
    var maxs = [];
    var mins = [];
    for (i = 0; i < period; i++) {
        newElement = yData[i];
        maxs.push(newElement[1]);
        mins.push(newElement[2]);
    }
    currentMax = maxs.max();
    currentMin = mins.min();
    result.push([currentMax, currentMin]);
    var modIndex = period - 1;
    for (; i < yData.length; i++) {
        newElement = yData[i];

        removeElement = maxs[modIndex];
        maxs[modIndex] = newElement[1];
        if (newElement[1] > currentMax) {
            currentMax = newElement[1];
        } else if (removeElement === currentMax) {
            currentMax = maxs.max();
        }   //else stays

        removeElement = mins[modIndex];
        mins[modIndex] = newElement[2];
        if (newElement[2] < currentMin) {
            currentMin = newElement[2];
        } else if (removeElement === currentMin) {
            currentMin = mins.min();
        }   //else stays

        result.push([currentMax, currentMin]);
        modIndex = (modIndex + 1) % period;
    }
    return result;
}

function candleValue(candle) {
//    return Math.max(candle[0], candle[3]);
//    return candle[2];
    return candle[0] + candle[1] + candle[2] + candle[3];
}

