/**
 * global location strings for the gui
 */
var locationStrings = {
    failure: 'Failure',
    success: 'Success',
    result: 'Result',
    notification: 'Notification',
    waitTitle: 'Sending...',
    waitMsg: 'Please wait...',
    serverConnectionError: 'Server connection error.',
    unknownError: 'unknown error',
    vtypeNameText: 'Invalid characters in name!',
    vtypeConfirmText: 'The passwords do not match!',
    vtypeMinfieldText: "This value must be smaller than or equal to the related max field's value!",
    vtypeMaxfieldText: "This value must be greater than or equal to the related min field's value!",
    vtypeFromfieldText: "This value must be smaller than the related to field's value!",
    vtypeTofieldText: "This value must be greater than the related from field's value!",
    vtypeCustomAlpha: 'The requested number is in an unacceptable format.',
    sameinstrumentFailure: 'This instrument exists already in this config!',
    samecurrencyFailure: 'This currency exists already in this config!',
    sameInstrumentTimeframeFailure: 'This instrument-timeframe pair exists already in this config!',
    storeFailure: 'The last action has been failed, there are probably unaccessible services on the server.',
    wrongChartData: 'The chart cannot be displayed with the given data!',
    viewFailure: 'Graph data download has been failed!',
    dnsTitle: 'Strategy ({0}, {1}) DNS',
    mmTitle: 'Strategy ({0}, {1}) MMPlugin',
    tradeFixTitle: 'FIX messages for Portfolio id: {0}, Account id: {1}, Trade id: {2}',
    startFailure: 'Starting evolution has been failed.',
    emailText: 'Email must contain a @ sign and any text before and after it.',
    listSaved: "The list has been saved.",
    elementsAdded: "The selected elements has been added to the list.",
    datarowFiltered: "The following datarow(s) have been filtered out, because they did not have at least {0} days of data: {1}"
};