/**
 * initializes a store for a screen in Lazy Read mode:
 *  gets the store through it's storeId
 *  adds exception handling to it
 *  loads it if it has not been loaded yet
 * @param {string} storeId the store's identifier string
 * @return {object} the store itself for other actions on the screen
 */
function lazyStoreR(storeId) {
    var store = Ext.StoreMgr.lookup(storeId);
    if (!store.lazyLoaded) {
        store.on('exception', storeErrorHandle, store);
        store.load();
        store.on('load', onLazyLoad, store);
    }
    return store;
}

function onLazyLoad(store) {
    store.lazyLoaded = true;
    store.un('load', onLazyLoad, store);
}

function setLazyValue(component, id, owner, name) {
    component.reset();
    component.setValue(id);
    owner[name] = 0;    //for select event, if needed
}

function selectLazy(component, store, owner, name) {
    if (owner[name] === 0) {
        componentSelectStoreId(component, store);
        delete owner[name];
    }
}

function autoSelectLazy(component, value) {
    component.setValue(value);
    var store = component.getStore();
    if (store.lazyLoaded) {
        componentSelectStoreId(component, store);
    } else {
        var addEvent = false;
        if (!store.autoSelectComponents) {
            store.autoSelectComponents = []
            addEvent = true;
        }
        store.autoSelectComponents.push(component);
        if (addEvent) {
            store.on('load', onLazyAutoLoad, store);
        }
    }
}

function onLazyAutoLoad(store) {
    var component;
    for (i in store.autoSelectComponents) {
        component = store.autoSelectComponents[i];
        if (typeof(component) === 'function') {
            continue;
        }
        componentSelectStoreId(component, store);
    }
    delete store.autoSelectComponents;
    store.un('load', onLazyAutoLoad, store);
}

function componentSelectStoreId(component, store) {
    var record = store.getById(component.getValue());
    if (!record) {
        return;
    }
    component.setValue(component.getValue());
    component.fireEvent('select', component, record);
}

function exceptionOnRD(proxy, type, action, options, response) {
    var store = this;
    if (!storeTestError(response)) {
        return;
    }
    store.rejectChanges();
    if (action === 'destroy') {
        store.reload();
    }
    storeErrorHandle(proxy, type, action, options, response);
}

function writeOnRD(store, action, result, res, rs) {
    var owner = this;
    store.commitChanges();
    if (owner.popup) {
        owner.popup.close();
        owner.popup = null;
    }
}

/**
 * initializes a store for a screen in Read-Destroy mode:
 *  gets the store through it's storeId
 *  adds a JsonReader and JsonWriter to it
 *  adds exception handling to it
 * @param {string} storeId the store's identifier string
 * @return {object} the store itself for other actions on the screen
 */
function storeRD(storeId, owner) {
    var store = Ext.StoreMgr.lookup(storeId);
    store.reader = new Ext.data.JsonReader({
        root: 'data',
        id: 'id'
    }, store.recordType);
    store.writer = new Ext.data.JsonWriter({
        encode: true,
        writeAllFields: true,   //TODO may be removed for run configuration
        meta: store.reader.meta
    });
    store.on('exception', exceptionOnRD, store);
    store.on('write', writeOnRD, owner);
    owner.on('beforedestroy', function() {
        store.un('exception', exceptionOnRD, store);
        store.un('write', writeOnRD, owner);
    }, owner);
    return store;
}

function exceptionOnCRUD(proxy, type, action, options, response) {
    var store = this;
    if (!storeTestError(response)) {
        return;
    }
    var modifieds = store.getModifiedRecords();
    var modified = null;
    for (modified in modifieds) {
        if (modifieds[modified].phantom) {
            store.remove(modifieds[modified]);
        }
    }
    store.rejectChanges();
    if (action === 'destroy') {
        store.reload();
    }
    storeErrorHandle(proxy, type, action, options, response);
}

/**
 * fires after successful write action
 *  commits changes and hides last popup window
 *  selects inserted record(s) if needed
 *  calls given function if needed
 * @event
 */
function writeOnCRUD(store, action, result, res, rs) {
    var owner = this;
    var params = store.paramCRUD[owner.id];
    var grid = params[0];
    store.commitChanges();
    if (owner.popup) {
        owner.popup.close();
        owner.popup = null;
    }
    if ((grid) && (action === 'create') && (rs)) {
        grid.getSelectionModel().selectRecords([rs]);
    }
}

/**
 * initializes a store for a screen in Create-Read-Update-Destroy mode:
 *  gets the store through it's storeId
 *  adds a JsonReader and JsonWriter to it
 *  adds exception handling to it
 * @param {string} storeId the store's identifier string
 * @return {object} the store itself for other actions on the screen
 */
function storeCRUD(storeId, owner, grid) {
    var store = Ext.StoreMgr.lookup(storeId);
    if (!store.paramCRUD) {
        store.paramCRUD = {};
    }
    store.paramCRUD[owner.id] = [grid];
    store.reader = new Ext.data.JsonReader({
        root: 'data',
        id: 'id'
    }, store.recordType);
    store.writer = new Ext.data.JsonWriter({
        encode: true,
        writeAllFields: true,   //TODO may be removed for run configuration
        meta: store.reader.meta
    });
    store.on('exception', exceptionOnCRUD, store);
    /**
     * fires after successful write action
     *  commits changes and hides last popup window
     *  if last action was an update, reloads store (may be removed in future)
     *  selects inserted record(s)
     * @event
     */
    store.on('write', writeOnCRUD, owner);
    owner.on('beforedestroy', function() {
        store.un('exception', exceptionOnCRUD, store);
        store.un('write', writeOnCRUD, owner);
    }, owner);
    return store;
}

function destroyRecord(store, record) {
    var idx = store.indexOf(record);
    store.destroyRecord(store, record, idx);
    store.removeAt(idx);
}

function destroyRecordSafe(grid) {
    var record = grid.getSelectionModel().getSelected();
    if (record === undefined) {
        return;
    }
    destroyRecord(grid.getStore(), record);
}

function destroySelected(store, selected) {
    for (var i in selected) {
        record = selected[i];
        if (typeof(record) === 'function') {
            continue;
        }
        destroyRecord(store, record);
    }
    store.save();
}

function confirmDestroyRecord(self, html, title, store, record) {
    self.popup = new YesNoWindow({html: html,
        title: title});
    self.popup.btnYes.on('click', function() {
        destroyRecord(store, record);
        store.save();
    });
    self.popup.show();
}

function confirmDestroyRecordSafe(self, grid, html, title) {
    var record = grid.getSelectionModel().getSelected();
    if (record === undefined) {
        return;
    }
    confirmDestroyRecord(self, html, title, grid.getStore(), record);
}

function confirmDestroySelected(self, html, title, store, selected) {
    self.popup = new YesNoWindow({html: html,
        title: title});
    self.popup.btnYes.on('click', function() {
        destroySelected(store, selected);
    });
    self.popup.show();
}