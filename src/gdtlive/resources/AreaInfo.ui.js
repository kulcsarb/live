AreaInfoUi = Ext.extend(Ext.Window, {
    width: 680,
    height: 550,
    layout: 'fit',
    initComponent: function() {
        this.items = [
            {
                xtype: 'textarea',
                ref: 'area'
            }
        ];
        AreaInfoUi.superclass.initComponent.call(this);
    }
});
