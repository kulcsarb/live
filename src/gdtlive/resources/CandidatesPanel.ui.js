CandidatesPanelUi = Ext.extend(Ext.grid.GridPanel, {
    title: 'Candidates',
    store: 'CandidatesStore',
    height: 550,
    initComponent: function() {
        this.selModel = new Ext.grid.CheckboxSelectionModel();
        this.columns = [
            this.selModel,
            {
                xtype: 'gridcolumn',
                dataIndex: 'id',
                header: 'Id',
                sortable: true,
                width: 50
            },
            {
                xtype: 'gridcolumn',
                dataIndex: 'name',
                header: 'Name',
                sortable: true,
                width: 100
            },
            {
                xtype: 'gridcolumn',
                header: 'Instruments',
                sortable: false,
                width: 70,
                dataIndex: 'instruments'
            },
            {
                xtype: 'gridcolumn',
                header: 'Timeframe',
                sortable: false,
                width: 65,
                dataIndex: 'timeframe_str'
            },
            {
                xtype: 'gridcolumn',
                header: 'Individual',
                sortable: false,
                width: 80,
                dataIndex: 'individual'
            },
            {
                xtype: 'gridcolumn',
                header: 'Evolved from',
                sortable: false,
                width: 72,
                dataIndex: 'evolved_from'
            },
            {
                xtype: 'gridcolumn',
                header: 'Evolved to',
                sortable: false,
                width: 72,
                dataIndex: 'evolved_to'
            },
            {
                xtype: 'gridcolumn',
                header: 'Live',
                sortable: false,
                width: 75,
                dataIndex: 'has_live_runs'
            },
            {
                xtype: 'gridcolumn',
                header: 'Winning trades',
                sortable: false,
                width: 80,
                dataIndex: 'winning_trades'
            },
            {
                xtype: 'gridcolumn',
                header: 'Losing trades',
                sortable: false,
                width: 80,
                dataIndex: 'losing_trades'
            },
            {
                xtype: 'gridcolumn',
                header: 'Win-lose diff',
                sortable: false,
                width: 80,
                dataIndex: 'win_lose_diff'
            },
            {
                xtype: 'numbercolumn',
                header: 'Net profit',
                sortable: false,
                width: 70,
                dataIndex: 'net_profit',
                format: '0,000.00'
            }
        ];
        this.tbar = {
            xtype: 'toolbar',
            buttonAlign: 'left',
            items: [
                {
                    xtype: 'button',
                    text: 'Add to Portfolio',
                    ref: '../btnAddtoportfolio'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Import',
                    ref: '../btnImport'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Export',
                    ref: '../btnExport'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View',
                    ref: '../btnView'
                }
            ]
        };
        this.bbar = {
            xtype: 'paging',
            store: 'CandidatesStore'
        }
        CandidatesPanelUi.superclass.initComponent.call(this);
    }
});
