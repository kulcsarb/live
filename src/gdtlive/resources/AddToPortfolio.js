/**
 * @fileOverview
 * @author vendelin8
 * @version 0.4
 */
/**
 * add strategy/account to portfolio
 * @class AddToPortfolio
 * @lends AddToPortfolio.prototype
 */
AddToPortfolio = Ext.extend(AddToPortfolioUi, {
    mainTitle: 'Add To Portfolio',
    cmbPortfolioLabel: 'Portfolio name',
    cmbPortfolioBlank: 'Select Portfolio',
    cmbPortfolioEmpty: 'Select Portfolio...',
    btnOkText: 'OK',
    btnCancelText: 'Cancel',
    /** @constructs */
    initComponent: function() {
        AddToPortfolio.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        self.cmbPortfolio.fieldLabel = self.cmbPortfolioLabel;
        self.cmbPortfolio.blankText = self.cmbPortfolioBlank;
        self.cmbPortfolio.emptyText = self.cmbPortfolioEmpty;
        self.btnOk.text = self.btnOkText;
        self.btnCancel.text = self.btnCancelText;

        /** portfolio store reference */
        self.portfolioStore = Ext.StoreMgr.lookup('PortfolioStore');

        /**
         * called when clicked on "Save" button
         *  adds selected strategies/accounts to selected portfolio
         * @event
         */
        self.btnOk.on('click', function() {
            Ext.Ajax.request({
                url: urls.addToPortfolio,
                params: {'id': self.cmbPortfolio.getValue(), 'ids': self.element_items, 'element_type': self.element_type, 'add': 1},
                success: function(response) {
                    response = ajaxSuccessHandle(response);
                    if (!response) {
                        return;
                    }
                    Ext.Msg.alert(locationStrings.success, response.msg);
                    self.close();
                },
                failure: function(response) {
                    ajaxErrorHandle(response);
                }
            });
        }, self);

        /**
         * called when clicked on "Cancel" button
         *  closes the window
         * @event
         */
        self.btnCancel.on('click', function() {self.close();}, self);

        /**
         * fires when the window is shown
         *  loads portfolios to combo
         * @event
         */
        self.on('show', function() {
            self.portfolioStore.load();
        }, self);
    }
});
