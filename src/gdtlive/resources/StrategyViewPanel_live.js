/**
 * @fileOverview
 * @author vendelin8
 * @version 0.5
 */
/**
 * strategy view
 * @class StrategyViewPanel
 * @lends StrategyViewPanel.prototype
 */
StrategyViewPanel = Ext.extend(StrategyViewPanelUi, {
    mainTitle: 'Strategy {0}',
    panOverviewTitle: 'StrategyOverview',
    btnViewdnsText: 'View DNS',
    btnMmpluginText: 'MMPlugin',
    treeRunHistoryTitle: 'Run history tree',
    btnReloadRunsText: 'Reload',
//    gridRunsettingsTitle: 'Run Settings',
    gridPerformancesumTitle: 'Performance Sum',
    performancesumHeaders: ['Variable', 'Currency', 'Points'],
    tabGraphTitle: 'Graph',
    tabChartsTitle: 'Charts',
    btnRefreshchartText: 'Refresh',
    gridTradelogTitle: 'Trade Log',
    tradelogHeaders: ['Id', 'State', 'Open time', 'Instrument', 'Price', 'Direction', 'Amount', 'SL', 'TP', 'Close time', 'Price', 'Type', 'Net profit', 'MAE', 'MFE'],
    btnRefreshtradelogText: 'Refresh',
    allDisplay: 'All',
    /** @constructs */
    initComponent: function() {
        StrategyViewPanel.superclass.initComponent.call(this);
        var self = this;
        self.panOverview.title = self.panOverviewTitle;
        self.btnViewdns.text = self.btnViewdnsText;
        self.btnMmplugin.text = self.btnMmpluginText;
        self.treeRunHistory.title = self.treeRunHistoryTitle;
        self.btnReloadRuns.text = self.btnReloadRunsText;
//        self.gridRunsettings.title = self.gridRunsettingsTitle;
        self.gridPerformancesum.title = self.gridPerformancesumTitle;
        self.gridPerformancesum.hideHeaders = false;
        setGridColums(self.gridPerformancesum, self.performancesumHeaders);
        self.tabGraph.title = self.tabGraphTitle;
        self.tabCharts.title = self.tabChartsTitle;
        self.btnRefreshchart.text = self.btnRefreshchartText;
        self.gridTradelog.title = self.gridTradelogTitle;
        setGridColums(self.gridTradelog, self.tradelogHeaders);
        self.btnRefreshtradelog.text = self.btnRefreshtradelogText;

//        self.gridRunsettings.setUrl(urls.strategyRunsettings);
//        self.gridRunsettings.order = ['evolconfigname', 'username', 'creation_time', 'from_date', 'to_date', 'symbols', 'timeframe_num', 'evolPluginName', 'fitnessPluginName', 'spreadConfigName', 'rolloverConfigName', 'base_equity', 'mmPluginName', 'mmplugin_parameters', 'slippage']
        self.gridPerformancesum.setUrl(urls.strategyPerformancesum);
        self.gridOverview.setUrl(urls.strategyInfo);

        self.graphData = {};
        self.charts = [];

        /** datarow store reference */
        self.datarowStore = Ext.StoreMgr.lookup('HistoricDatarowManagementStore');
        /** mm plugin store reference */
        self.mmpluginStore = Ext.StoreMgr.lookup('MMPluginStore');
        /** symbol store reference as static store */
        self.symbolStore = Ext.StoreMgr.lookup('StaticStore');
        /** symbol store all reference as static store */
        self.symbolStoreAll = Ext.StoreMgr.lookup('StaticStore2');
        /** run settings store reference */
//        self.runSettingsStore = Ext.StoreMgr.lookup('PropertyStore2');
        /** trade log store reference */
        self.tradelogStore = Ext.StoreMgr.lookup('TradeStore');
        self.tradelogStore.on('load', self.tradelogStoreLoad, self);

        /**
         * fires after the graph canvas is rendered
         *  inits graph plot
         * @event
         */
        self.canvasGraph.on('afterrender', function() {
            self.graphAccountBalance = jsplot(self.canvasGraph.id, {xType: 'custom', labelPattern: 'Day {0}', labelStartIndex: 1, type: 'line', maxXlabel: 'Day 1000', 'labels': ['account balance'], 'initZoom': 1});
            self.graphDrawdown = jsplot(self.canvasGraph.id, {xType: 'custom', labelPattern: 'Trade {0}', labelStartIndex: 1, type: 'line', maxXlabel: 'Trade 1000', 'labels': ['drawdown'], 'initZoom': 1, 'visible': false});
            self.graphProfitloss = jsplot(self.canvasGraph.id, {xType: 'custom', labelPattern: 'Trade {0}', labelStartIndex: 1, type: 'line', maxXlabel: 'Trade 1000', 'labels': ['profit/loss'], 'initZoom': 1, 'visible': false});
            self.graphDailyprofit = jsplot(self.canvasGraph.id, {xType: 'custom', labelPattern: 'Day {0}', labelStartIndex: 1, type: 'bar', maxXlabel: 'Day 1000', 'labels': ['profit'], 'visible': false});
            self.graphWeeklyprofit = jsplot(self.canvasGraph.id, {xType: 'date', type: 'bar', 'labels': ['profit'], 'visible': false});
            self.graphMonthlyprofit = jsplot(self.canvasGraph.id, {xType: 'string', indexedInput: true, type: 'bar', maxXlabel: '2000-08', 'labels': ['profit'], 'visible': false});
            self.graphYearlyprofit = jsplot(self.canvasGraph.id, {xType: 'string', indexedInput: true, type: 'bar', maxXlabel: '2000', 'labels': ['profit'], 'visible': false});
            self.graphMae = jsplot(self.canvasGraph.id, {xType: 'custom', labelPattern: 'Trade {0}', labelStartIndex: 1, type: 'line', maxXlabel: 'Trade 1000', 'labels': ['mae'], 'initZoom': 1, 'visible': false});
            self.graphMfe = jsplot(self.canvasGraph.id, {xType: 'custom', labelPattern: 'Trade {0}', labelStartIndex: 1, type: 'line', maxXlabel: 'Trade 1000', 'labels': ['mfe'], 'initZoom': 1, 'visible': false});
            self.graphBeta = jsplot(self.canvasGraph.id, {xType: 'custom', labelPattern: 'Month {0}', labelStartIndex: 1, type: 'bar', maxXlabel: 'Month 100', 'labels': ['beta'], 'visible': false});
            self.activeGraph = self.graphAccountBalance;
            if (self.graphFillAfterRender) {
                self.graphFillAfterRender = null;
                self.tabGraph.fireEvent('show');
            }
        }, self);
        /**
         * fires when an option is selected in graph type combo
         *  shows selected plot, hides the others
         * @event
         */
        self.cmbGraphtype.on('select', function(combo, record) {
            if (!self.graphAccountBalance) {
                return;
            }
            var activeGraph = self.activeGraph;
            if ((!record) || (record.get('value') === 'account_balance')) {
                self.activeGraph = self.graphAccountBalance;
            } else if (record.get('value') === 'drawdown') {
                self.activeGraph = self.graphDrawdown;
            } else if (record.get('value') === 'trades_profit_loss') {
                self.activeGraph = self.graphProfitloss;
            } else if (record.get('value') === 'daily_profit') {
                self.activeGraph = self.graphDailyprofit;
            } else if (record.get('value') === 'weekly_profit') {
                self.activeGraph = self.graphWeeklyprofit;
            } else if (record.get('value') === 'monthly_profit') {
                self.activeGraph = self.graphMonthlyprofit;
            } else if (record.get('value') === 'yearly_profit') {
                self.activeGraph = self.graphYearlyprofit;
            } else if (record.get('value') === 'MAE') {
                self.activeGraph = self.graphMae;
            } else if (record.get('value') === 'MFE') {
                self.activeGraph = self.graphMfe;
            } else if (record.get('value') === 'beta') {
                self.activeGraph = self.graphBeta;
            }
            if (activeGraph !== self.activeGraph) {
	            activeGraph.setVisible(false);
	            self.activeGraph.setVisible(true);
            }
        }, self);

        /**
         * called when the graph canvas has been resized
         *  calls a resize on the plot as well
         * @event
         */
        self.canvasGraph.on('resize', function() {
            if (!self.graphAccountBalance) {
                return;
            }
            self.graphAccountBalance.plot();
            self.graphDrawdown.plot();
            self.graphProfitloss.plot();
            self.graphDailyprofit.plot();
            self.graphWeeklyprofit.plot();
            self.graphMonthlyprofit.plot();
            self.graphYearlyprofit.plot();
            self.graphMae.plot();
            self.graphMfe.plot();
            self.graphBeta.plot();
        }, self);

        /**
         * called when the chart canvas has been resized
         *  calls a resize on the plot as well
         * @event
         */
        self.canvasChart.on('resize', function() {
            if (self.activeChart === undefined) {
                return;
            }
            self.activeChart.plot();
        }, self);

        /**
         * called when the graph tab is shown
         *  if the graph has not been rendered or there is no data, saves this information and returns
         *  if the run is the same as last time, it returns (no need to refresh)
         *      if the graph data is already saved for this run, plots it
         *      loads needed data and plots otherwise
         * @event
         */
        self.tabGraph.on('show', function() {
            if (!self.activeGraph) {
                self.graphFillAfterRender = true;
                return;
            }
            if (self.lastGraph === self.lastSelectedRun.id4) {
                return;
            }
            if (typeof(self.graphData[self.lastSelectedRun.id4]) === 'object') {
                self.readGraphs();
            } else {
                Ext.Ajax.request({
                    url: urls.strategyGraph,
                    params: {id: self.lastSelectedRun.id4},
                    success: function(response) {
                        response = ajaxSuccessHandle(response);
                        if (!response) {
                            return;
                        }
                        self.graphData[self.lastSelectedRun.id4] = response.data;
                        self.readGraphs();
                    },
                    failure: function(response) {
                        ajaxErrorHandle(response);
                    }
                });
            }
        }, self);

        /**
         * fires after the chart canvas is rendered
         *  inits chart plot
         * @event
         */
        self.canvasChart.on('afterrender', function() {
            var i, record, currParams;
            var records = self.symbolStore.getRange();
            var params = {'visible': true, 'extras': ['tradeTriangle'], 'dataReadyNeeded': true};
            for (i = 0; i < records.length; i++) {
                if (i === 1) {
                    params.visible = false;
                }
                currParams = clone(params);
                record = records[i];
                currParams.globalSource = constants.datarow + record.get('others')['datarowId'];
                self.charts.push(jsplot(self.canvasChart.id, currParams));
            }
            self.activeChart = self.charts[0];
            record = self.symbolStore.getAt(0);
            self.cmbChartsfilter.setValue(record.get('value'));
            self.cmbChartsfilter.fireEvent('select', self.cmbChartsfilter, record);
        }, self);

        /**
         * called when the charts tab is shown
         *  starts downloading chart data by loading strategy symbol store in case of no data
         *  shows first chart otherwise
         * @event
         */
        self.tabCharts.on('show', function() {
//            if (self.lastChart === self.lastSelectedRun.id4) {
//                return;
//            }
            self.lastChart = self.lastSelectedRun.id4;
            if (!self.cmbChartsfilter.getValue()) {
                self.cmbChartsfilter.setValue(self.symbolStore.getAt(0).get('value'));
            }
            self.cmbChartsfilter.fireEvent('select', self.cmbChartsfilter);
        }, self);

        self.btnRefreshchart.on('click', function() {
            var selected = self.cmbChartsfilter.findRecord(self.cmbChartsfilter.valueField, self.cmbChartsfilter.getValue());
            var others = selected.get('others');
            self.activeChart = self.charts[others.plotId];
            downloadChart(others.datarowId, self.lastSelectedRun.from_date, self.lastSelectedRun.to_date, self.timeframe_num, self.activeChart);
            self.drawingChart = self.activeChart;
            if (!self.reloadTradeLog()) {
                self.loadTradesToChart(self.tradelogStore.getRange(), self.drawingChart);
                delete self.drawingChart;
            }
        }, self);

        /**
         * fires when an option is selected in chart type combo
         *  downloads chart data in case of no data
         *  shows selected plot, hides the others
         * @event
         */
        self.cmbChartsfilter.on('select', function(combo, record) {
            if (!self.charts[0]) {
                return;
            }
            var activeChart = self.activeChart;
            self.btnRefreshchart.fireEvent('click');
            if (activeChart !== self.activeChart) {
                activeChart.setVisible(false);
                self.activeChart.setVisible(true);
            }
        }, self);

        /**
         * called when a the trade log filter combobox's selection has changed
         *  clears current filter, adds a new one if the current setting is not 'All'
         * @event
         */
        self.cmbTradelogfilter.on('select', function(combo, record) {
            self.tradelogStore.clearFilter();
            if (record.get('display') !== self.allDisplay) {
                self.tradelogStore.filter('symbol', record.get('display'), false, true);
            }
        }, self);

        /**
         * called when the trade log grid has been shown
         *  loads strategy symbol all store and trade log if needed
         * @event
         */
        self.gridTradelog.on('show', function() {
            self.reloadTradeLog();
        }, self);

        self.btnRefreshtradelog.on('click', function() {
            self.reloadTradeLog();
        }, self);

        /**
         * called when clicked on "View DNS" button
         *  shows strategy dns (dna) in a popup window
         * @event
         */
        self.btnViewdns.on('click', function() {
            Ext.Ajax.request({
                url: urls.strategyDns,
                params: {'id': self.recordId},
                success: function(response) {
                    response = ajaxSuccessHandle(response);
                    if (!response) {
                        return;
                    }
                    var name = self.recordName;
                    if ((!name) || (name === '')) {
                        name = '&lt;no name&gt;';
                    }
                    var popup = new AreaInfo({title: String.format(locationStrings.dnsTitle, self.recordId, name)});
                    popup.area.setValue(response.data);
                    popup.show();
                },
                failure: function(response) {
                    ajaxErrorHandle(response);
                }
            });
        }, self);

        self.btnMmplugin.on('click', function() {
            Ext.Ajax.request({
                url: urls.strategyMm,
                params: {'id': self.recordId},
                success: function(response) {
                    response = ajaxSuccessHandle(response);
                    if (!response) {
                        return;
                    }
                    response = response.data;
                    var name = self.recordName;
                    if ((!name) || (name === '')) {
                        name = '&lt;no name&gt;';
                    }
                    var popup = new Ext.Window({title: String.format(locationStrings.mmTitle, self.recordId, name), width: 500, height: 500, layout: 'fit', modal: true});
                    var index = self.mmpluginStore.findExact('id', response['id']);
                    if (index !== -1) {
                        var record = self.mmpluginStore.getAt(index);
                        if (!classExist(record.get('name'))) {
                            self.mmPlugin = new Ext.Panel();
                            self.mmPlugin.fillForm = function() {};
                            self.mmPlugin.setParams = function(params) {
                                params.mmplugin_parameters = '{}';
                                return params;
                            };
                        } else {
                            self.mmPlugin = eval('new ' + record.get('name') + '();');
                        }
                        self.mmPlugin.show();
                        popup.add(self.mmPlugin);
                        popup.doLayout();
                        self.mmPlugin.fillForm(response['params']);
                    }
                    popup.show();
                },
                failure: function(response) {
                    ajaxErrorHandle(response);
                }
            });
        }, self);

        /**
         * Reloads strategy runs in the tree.
         */
        self.btnReloadRuns.on('click', function() {
            var root = self.treeRunHistory.getRootNode();
            root.removeAll();
            Ext.Ajax.request({
                url: urls.strategyRunHistoryTree,
                params: {'id': self.recordId},
                success: function(response) {
                    response = ajaxSuccessHandle(response);
                    if (!response) {
                        return;
                    }
                    response = response.data;
                    var i, child;
                    var backtestNode = new Ext.tree.AsyncTreeNode({text: TREENODE_NAMES[BACKTEST_CONFIG], leaf: false, expanded: true, expandable: true});
                    backtestNode.loaded = true;
                    root.appendChild(backtestNode);
                    addTreeNodeChildren(response[0], backtestNode, self.selectOnAddToTree.bind(self), 2);
                    var liveNode = new Ext.tree.AsyncTreeNode({text: TREENODE_NAMES[LIVERUN_CONFIG], leaf: false, expanded: true, expandable: true});
                    liveNode.loaded = true;
                    root.appendChild(liveNode);
                    addTreeNodeChildren(response[1], liveNode, self.selectOnAddToTree.bind(self), 3);
                    if (self.lastSelectedRun === -1) {
                        if (backtestNode.hasChildNodes()) {
                            self.treeRunHistory.fireEvent('click', backtestNode.lastChild);
                        } else {
                            self.panRight.hide();
                        }
                    }
                },
                failure: function(response) {
                    ajaxErrorHandle(response);
                }
            });
        }, self);

        /**
         * fires when a node is collapsed
         *  resets loaded variable to maintain possible changes
         * @event
         */
        self.treeRunHistory.on('beforecollapsenode', function(node) {
            return false;
        }, self);

        /**
         * fires when a node is clicked
         *  loads the related data to the content grid
         * @event
         */
        self.treeRunHistory.on('click', function(node) {
            var loadParams = node.loadParams;
            if (!loadParams) {
                return false;
            }
            self.lastSelectedRun = loadParams;
//            self.gridRunsettings.setParams({'id': newSelectedRun});
            self.gridPerformancesum.setParams({'id': self.lastSelectedRun.id4});
            if (self.panRight.getActiveTab) {
                var activeTab = self.panRight.getActiveTab();
                if ((activeTab) && (activeTab.fireEvent)) {
                    self.panRight.getActiveTab().fireEvent('show');
                }
            }
            self.panRight.show();
        }, self);

        /**
         * fires after the panel is rendered
         *  initializes panel
         * @event
         */
        self.on('afterrender', function() {
            self.setTitle(String.format(self.mainTitle, self.recordId));
            self.panRight.setActiveTab(0);
        }, self);

        /**
         * fires before the panel is shown
         *  loads active tabs' grids' stores
         * @event
         */
        self.on('show', function() {
/*            var symbols = []
            var i, symbol, record;
            for (i in self.symbols) {
                symbol = self.symbols[i];
                if (typeof(symbol) === 'function') {
                    continue;
                }
                symbols.push({'value': symbol, 'display': symbol});
            }*/
            self.gridOverview.setParams({'id': self.recordId, 'userId': currentUser.id});
            self.gridOverview.fireEvent('show');
            if (!mainViewport.liveManagementP.hidden) {
                self.btnReloadRuns.fireEvent('click');
            }
            var records = [];
            var datarowStoreRecords = self.datarowStore.getRange();
            for (i = 0; i < datarowStoreRecords.length; i++) {
                record = datarowStoreRecords[i];
                if ((record.get('timeframe_str') === self.timeframe_str) && (self.symbols.indexOf(record.get('symbol')) !== -1)) {
                    records.push(record);
                }
            }
            var symbolRecords = [];
            var datarowId;
            for (i = 0; i < records.length; i++) {
                record = records[i];
                symbol = record.get('symbol');
                datarowId = record.get('id');
                symbolRecords.push({'value': symbol, 'display': symbol, 'others': {'datarowId': datarowId, 'plotId': i}});
            }
            self.timeframe_num = record.get('timeframe_num');
            self.symbolStore.loadData(symbolRecords);
            symbolRecords.push({'display': self.allDisplay, 'value': self.allDisplay});
            self.symbolStoreAll.loadData(symbolRecords);
        }, self);

        /**
         * fires when the window is destroyed
         *  destroys the chart components
         * @event
         */
        self.on('destroy', function() {
            if (self.graphAccountBalance) {
                self.graphAccountBalance.destroy();
                self.graphDrawdown.destroy();
                self.graphProfitloss.destroy();
                self.graphDailyprofit.destroy();
                self.graphWeeklyprofit.destroy();
                self.graphMonthlyprofit.destroy();
                self.graphYearlyprofit.destroy();
                self.graphMae.destroy();
                self.graphMfe.destroy();
                self.graphBeta.destroy();
            }
            if (self.charts[0]) {
                for (var i = 0; i < self.charts.length; i++) {
                    self.charts[i].destroy();
                }
            }
        }, self);
    },

    /**
     * reads graph data to canvas
     */
    readGraphs: function() {
        var self = this;
        var data = self.graphData[self.lastSelectedRun.id4];
        self.graphAccountBalance.read(data['account_balance']);
        self.graphDrawdown.read(data['drawdown']);
        self.graphProfitloss.read(data['trades_profit_loss']);
        self.graphDailyprofit.read(data['daily_profit']);
        self.graphWeeklyprofit.read(data['weekly_profit']);
        self.graphMonthlyprofit.read(data['monthly_profit']);
        self.graphYearlyprofit.read(data['yearly_profit']);
        self.graphMae.read(data['MAE']);
        self.graphMfe.read(data['MFE']);
        self.graphBeta.read(data['beta']);
        self.lastGraph = self.lastSelectedRun.id4;
        self.canvasGraph.fireEvent('resize');
    },

    /**
     * loads a new trade log if needed, or reads the saved data to the store
     */
    reloadTradeLog: function() {
        var self = this;
        var store = self.tradelogStore;
        if (store.currGrid !== self.gridTradelog) {
            if (store.getCount() > 0) {
                if (store.currGrid) {
                    store.currGrid.data = store.getRange();
                }
                store.removeAll();
            }
        }
        if ((self.lastTradelog === undefined) || (self.lastTradelog.id !== self.lastSelectedRun.id4) || (self.lastTradelog.to_date < self.lastSelectedRun.to_date) || (self.lastTradelog.from_date < self.lastSelectedRun.from_date)) {
            self.lastTradelog = {id: self.lastSelectedRun.id4, from_date: self.lastSelectedRun.from_date, to_date: self.lastSelectedRun.to_date};
            store.currGrid = self.gridTradelog;
            store.load({params: {'id4': self.lastSelectedRun.id4, 'type4': self.lastSelectedRun.type4}});
            return true;
        }
        if (self.gridTradelog.data) {
            store.currGrid = self.gridTradelog;
            store.add(self.gridTradelog.data);
            self.gridTradelog.data = null;
        }
        return false;
    },

    /**
     * loads trade log data to the 
     * @param {Array} records the trade log records to show
     * @param {Integer} chartIndex index of the chart to add the trade log records
     */
    loadTradesToChart: function(records, chart) {
        var self = this;
        var filterSymbol = self.cmbChartsfilter.getValue();
        var tradelogExtraData = [];
        var record, extrai;
        for (i in records) {
            record = records[i];
            if ((typeof(record) === 'function') || (record.get('symbol') !== filterSymbol)) {
                continue;
            }
            extrai = {'opentime': Date.parseDate(record.get('open_time'), "Y-m-d H:i:s").format('U') * 1000, 'openprice': record.get('open_price'), 'direction': record.get('direction'), 'amount': record.get('amount'), 'closeprice': record.get('close_price'), 'type': record.get('close_type'), 'profit': record.get('profit')};
            if (record.get('close_time')) {
                extrai.closetime = Date.parseDate(record.get('close_time'), "Y-m-d H:i:s").format('U') * 1000;
            }
            tradelogExtraData.push(extrai);
        }
        chart.readExtra(tradelogExtraData);
        self.canvasChart.fireEvent('resize');
    },

    /**
     * called when the trade log store is loaded
     *  selects "All" option in related (filter) combo box
     *  calls chart draw if available and needed
     */
    tradelogStoreLoad: function(store, records) {
        var self = this;
        if ((self.destroying) || (self.isDestroyed) || (!self.isVisible())) {
            return;
        }
        if (records.length === undefined) {
            records = [records];
        }
        var currentValue = self.cmbTradelogfilter.getValue();
        if (!currentValue) {
            comboSelect(self.cmbTradelogfilter, self.allDisplay);
        } else if (currentValue !== self.allDisplay) {
            comboSelect(self.cmbTradelogfilter, self.currentValue);
        }
        if (self.drawingChart !== undefined) {
            self.loadTradesToChart(records, self.drawingChart);
            delete self.drawingChart;
        }
    },

    selectOnAddToTree: function(child, childId, parentIds) {
        var self = this;
        child.loadParams.id4 = childId;
        child.loadParams.type4 = parentIds[0];
        if ((self.lastSelectedRun !== -1) && (childId === self.lastSelectedRun.id4)) {
            self.treeRunHistory.getSelectionModel().select(child);
            self.treeRunHistory.fireEvent('click', child);
        }
    }
});
