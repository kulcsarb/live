/**
 * @fileOverview
 * @author vendelin8
 * @version 0.2
 */
/**
 * start backtest
 * @class BacktestStartWindow
 * @lends BacktestStartWindow.prototype
 */
BacktestStartWindow = Ext.extend(BacktestStartWindowUi, {
    mainTitle: 'Create Retest Config',
    btnPreviousText: 'Previous',
    btnNextText: 'Next',
    btnCancelText: 'Cancel changes',
    btnStartText: 'Start Backtest',
    fsetHistoricsettingsTitle: 'Historic Data Settings',
    btnDownloadmoreText: 'Download more histroic data',
    datarowHeaders: ['From date', 'To date', 'Instrument', 'Timeframe', 'Source', 'Download date-time'],
    chosenTitle: 'Chosen datarows:',
    fldFromdateLabel: 'Runtime from',
    fldTodateLabel: 'Runtime to',
    btnRefreshText: 'Refresh',
    fsetMmsettingsTitle: 'Money Management Settings',
    fldBasecapitalLabel: 'Base capital (USD)',
    fldBasecapitalBlank: 'Enter base capital',
    fldBasecapitalEmpty: 'Base capital here',
    fldLeverageLabel: 'Leverage',
    fldLeverageBlank: 'Enter leverage',
    fldLeverageEmpty: 'Leverage here',
    cmbSpreadconfigLabel: 'Spread config',
    cmbSpreadconfigBlank: 'Select spread config',
    cmbSpreadconfigEmpty: 'Select spread config...',
    cmbRolloverconfigLabel: 'Rollover config',
    cmbRolloverconfigBlank: 'Select rollover config',
    cmbRolloverconfigEmpty: 'Select rollover config...',
    fldSlippageLabel: 'Slippage (pips)',
    fldFailedtradesLabel: 'Failed trades (%)',
    cmbMMpluginLabel: 'Money management plugin',
    cmbMMpluginBlank: 'Select MM plugin',
    cmbMMpluginEmpty: 'Select MM plugin...',
    fsetMmpluginsettingsTitle: 'Money management plugin settings',
    msgNoIntersection: 'The chosen datarows does not have a common time intersection!',
    datarowCountNotEqual: "The number of selected datarows must be equal with the selected strategies' number: {0}.",
    startFailure: 'Starting backtest has been failed',
    /** @constructs */
    initComponent: function() {
        BacktestStartWindow.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        self.btnPrevious.text = self.btnPreviousText;
        self.btnNext.text = self.btnNextText;
        self.btnCancel.text = self.btnCancelText;
        self.btnStart.text = self.btnStartText;
        self.fsetHistoricsettings.title = self.fsetHistoricsettingsTitle;
        self.btnDownloadmore.text = self.btnDownloadmoreText;
        setGridColums(self.gridDatarow, self.datarowHeaders, 1);
        self.gridChosen.title = self.chosenTitle;
        setGridColums(self.gridChosen, self.datarowHeaders, 1);
        self.fldFromdate.fieldLabel = self.fldFromdateLabel;
        self.fldTodate.fieldLabel = self.fldTodateLabel;
        self.btnRefresh.text = self.btnRefreshText;
        self.fsetMmsettings.title = self.fsetMmsettingsTitle;
        self.fldBasecapital.fieldLabel = self.fldBasecapitalLabel;
        self.fldBasecapital.blankText = self.fldBasecapitalBlank;
        self.fldBasecapital.emptyText = self.fldBasecapitalEmpty;
        self.fldLeverage.fieldLabel = self.fldLeverageLabel;
        self.fldLeverage.blankText = self.fldLeverageBlank;
        self.fldLeverage.emptyText = self.fldLeverageEmpty;
        self.cmbSpreadconfig.fieldLabel = self.cmbSpreadconfigLabel;
        self.cmbSpreadconfig.blankText = self.cmbSpreadconfigBlank;
        self.cmbSpreadconfig.emptyText = self.cmbSpreadconfigEmpty;
        self.cmbRolloverconfig.fieldLabel = self.cmbRolloverconfigLabel;
        self.cmbRolloverconfig.blankText = self.cmbRolloverconfigBlank;
        self.cmbRolloverconfig.emptyText = self.cmbRolloverconfigEmpty;
        self.fldSlippage.fieldLabel = self.fldSlippageLabel;
        self.fldFailedtrades.fieldLabel = self.fldFailedtradesLabel;
        self.cmbMMplugin.fieldLabel = self.cmbMMpluginLabel;
        self.cmbMMplugin.blankText = self.cmbMMpluginBlank;
        self.cmbMMplugin.emptyText = self.cmbMMpluginEmpty;
        self.fsetMmpluginsettings.title = self.fsetMmpluginsettingsTitle;

        //change slider's tooltip format to date from slider's real value: days
        Ext.apply(self.sldFromto.tooltip, {getText: function(thumb) {
            return self.getDate(thumb.value).format('Y-m-d');
        }});

        /**
         * fires when clicked on "Next" button
         *  shows next card
         * @event
         */
        self.btnNext.on('click', function() {
            if (self.activeIndex === 0) {
                if (self.gridChosen.getSelectionModel().getCount() !== self.recordNumberOfInstruments) {
                    Ext.Msg.alert(locationStrings.failure, String.format(self.datarowCountNotEqual, self.recordNumberOfInstruments));
                    return;
                }
            }
            self.changeCard(1);
        }, self);
        /**
         * fires when clicked on "Previous" button
         *  shows previous card
         * @event
         */
        self.btnPrevious.on('click', function() {
            self.changeCard(-1);
        }, self);
        /**
         * fires when clicked on "Cancel changes" button
         *  closes window
         * @event
         */
        self.btnCancel.on('click', function() {self.hide();}, self);

        /**
         * fires when clicked on "Download more historic datarows" button
         *  closes this window and opens Historic window
         * @event
         */
        self.btnDownloadmore.on('click', function() {
            self.hide();
            mainViewport.btnHistoric.fireEvent('click');
        }, self);

        /** datarow store reference */
        self.datarowStore = Ext.StoreMgr.lookup('HistoricDatarowManagementStore');
        self.datarowStore.on('exception', storeErrorHandle, self);

        /** chosen datarow store reference */
        self.chosenStore = Ext.StoreMgr.lookup('ChosenDatarowStore');

        /**
         * fires when a row is selected in datarow grid
         *  the same row must be added to chosen grid (and selected as well)
         * @event
         */
        self.gridDatarow.getSelectionModel().on('rowselect', function(sm, rowIndex, record) {
            if (self.sliderChange(record, true) !== true) {
                Ext.Msg.alert(locationStrings.failure, self.msgNoIntersection)
                self.gridDatarow.getSelectionModel().deselectRow(rowIndex);
                return;
            }
            self.chosenStore.add([record]);
            self.gridChosen.getSelectionModel().selectRecords([record], true);
            self.changeCard(0); //enable/disable next&previous button
        }, self);

        /** datarow store reference */
        self.populationStore = Ext.StoreMgr.lookup('PopulationStore');
        self.populationStore.on('exception', storeErrorHandle, self);
        /** money management store reference */
        self.mMPluginStore = lazyStoreR('MMPluginStore');
        /** spread config store reference */
        self.spreadConfigStore = Ext.StoreMgr.lookup('AdminSpreadConfigStore');
        self.spreadConfigStore.on('exception', storeErrorHandle, self);
        /** rollover config store reference */
        self.rolloverConfigStore = Ext.StoreMgr.lookup('AdminRolloverConfigStore');
        self.rolloverConfigStore.on('exception', storeErrorHandle, self);

        /**
         * fires when a row is deselected from datarow grid
         *  the same row must be removed from chosen grid
         *  so the slider's interval may change
         * @event
         */
        self.gridDatarow.getSelectionModel().on('rowdeselect', function(sm, rowIndex, record) {
            self.chosenStore.remove(record);
            self.sliderChange(record, false);
        }, self);
        /**
         * fires when a row is deselected from chosen grid
         *  the deselected row must be removed from chosen store and (if it is selected) deselected from datarow grid
         *  so the slider's interval may change
         * @event
         */
        self.gridChosen.getSelectionModel().on('rowdeselect', function(sm, rowIndex, record) {
            self.chosenStore.remove(record);
            var toDeselect = self.datarowStore.findExact('id', record.get('id'));
            if (toDeselect !== -1) {
                self.gridDatarow.getSelectionModel().deselectRow(toDeselect);
            }
            self.sliderChange(record, false);
        }, self);

        self.sldFromto.addThumb(0);
        /**
         * fires when clicked on "Refresh slider" button
         *  refreshes the slider's low and high thumb
         * @event
         */
        self.btnRefresh.on('click', function() {
            self.setHighThumb(self.fldTodate.getValue());
            self.setLowThumb(self.fldFromdate.getValue());
        }, self);
        /**
         * fires on from date keyup
         *  sets "Refresh slider" button's enablity based on from and to date fields' validity
         * @event
         */
        self.fldFromdate.on('keyup', function() {
            self.btnRefresh.setDisabled((!self.fldFromdate.isValid()) || (self.getDays(self.fldTodate.getValue()) <= self.getDays(self.fldFromdate.getValue())));
        }, self);
        /**
         * fires on to date keyup
         *  sets "Refresh slider" button's enablity based on from and to date fields' validity
         * @event
         */
        self.fldTodate.on('keyup', function() {
            self.btnRefresh.setDisabled((!self.fldTodate.isValid()) || (self.getDays(self.fldTodate.getValue()) <= self.getDays(self.fldFromdate.getValue())));
        }, self);
        self.sldFromto.on('change', function(slider, newValue, thumb) {
            if (thumb.index === 0) {
                if (newValue === self.sldFromto.getValue(1)) {
                    newValue -= 1;
                    self.sldFromto.setValue(0, newValue);
                }
                self.fldFromdate.setValue(self.getDate(newValue));
            } else {
                if (newValue === self.sldFromto.getValue(0)) {
                    newValue += 1;
                    self.sldFromto.setValue(1, newValue);
                }
                self.fldTodate.setValue(self.getDate(newValue));
            }
        }, self);

        /**
         * functionality after 'Start' click:
         *  calls 'Save' explicit and after successful save it calls start
         * @event
         */
        self.btnStart.on('click', function() {
            var i;
            var params = self.formHist.getForm().getFieldValues();  //variables from the first card
            var params2 = self.formMm.getForm().getFieldValues();       //variable from the second card
            for (i in params2) {
                params[i] = params2[i];
            }
            var selected = self.gridChosen.getSelectionModel().getSelections(); //get chosen datarow ids
            if (!selected.length) {
                selected = [selected];
            }
            params.datarowIds = Ext.encode(collectIds(selected));
            if (self.mmPlugin) {
                params = self.mmPlugin.setParams(params);
            }
            params.userId = currentUser.id;     //user id
            params.strategies = Ext.util.JSON.encode(self.strategies);
            Ext.Ajax.request({
                url: urls.startBacktest,
                params: params,
                success: function(response) {
                    response = ajaxSuccessHandle(response, self.startFailure)
                    if (!response) {
                        return;
                    }
                    self.hide();
                    if (self.callback) {
                        self.callback();
                    }
                },
                failure: function(response) {
                    ajaxErrorHandle(response, self.startFailure);
                }
            });
        }, self);

        /**
         * functionality the evol form has been validated
         *  if we are on card 2 (money management params) we enable or disable 'Start' button based on the form validity
         * @event
         */
        self.formMm.on('clientvalidation', function(form, valid) {
            if (self.activeIndex !== 1) {
                return;
            }
            self.btnStart.setDisabled(!valid);
        }, self);

        /**
         * fires when an option is selected from money management plugins' combo box
         * loads the gui element of the plugin to the form, or an empty panel if it does not exist
         * @event
         */
        self.cmbMMplugin.on('select', function(combo, record) {
            if (!classExist(record.get('name'))) {
                self.mmPlugin = new Ext.Panel();
                self.mmPlugin.fillForm = function() {};
                self.mmPlugin.setParams = function(params) {
                    params.mmplugin_parameters = '{}';
                    return params;
                };
            } else {
                self.mmPlugin = eval('new ' + record.get('name') + '();');
            }
            self.fsetMmpluginsettings.removeAll();
            self.fsetMmpluginsettings.add(self.mmPlugin);
            self.mmPlugin.show();
            self.fsetMmpluginsettings.doLayout();
        }, self);

        /**
         * fires when the window is shown
         *  clears window's content
         * @event
         */
        self.on('show', function() {
            self.datarowStore.on('load', self.datarowStoreOnLoad, self);
            self.activeIndex = 0;
            self.chosenStore.removeAll();
            self.gridDatarow.getSelectionModel().clearSelections(true);
            self.datarowStore.load();
            self.spreadConfigStore.load();
            self.rolloverConfigStore.load();
            self.changeCard(0); //enable/disable next&previous buttons
            self.btnNext.setDisabled(true);
            self.btnStart.setDisabled(true);
            self.mmpluginId = 1;
            self.fldBasecapital.setValue(10000);
            self.fldLeverage.setValue(30);
            self.rolloverconfigId = 1;
            self.spreadconfigId = 1;
        }, self)

        /**
         * fires after the window is rendered
         * @event
         */
        self.on('afterrender', function() {
            self.activeIndex = 0;
        }, self);

        /**
         * fires when the window is hidden (closed)
         *  removes datarow store's load event handler
         * @event
         */
        self.on('hide', function() {
            self.datarowStore.un('load', self.datarowStoreOnLoad, self);
        }, self);
    },

    /**
     * evaluates card change or current card paging buttons' enablity
     * @param {Integer} changeVal the card index increment, can be
     *  -1: previous
     *   0: refresh current paging buttons' enablity
     *   1: next
     */
    changeCard: function(changeVal) {
        var self = this;
        var record;
        self.activeIndex += changeVal;
        var newVal = Math.max(Math.min(self.activeIndex, self.items.length - 1), 0);
        self.getLayout().setActiveItem(newVal);
        self.btnNext.setDisabled(newVal === self.items.length - 1);
        self.btnPrevious.setDisabled(newVal === 0);
        if (newVal === 0) {     //refresh the slider's value based on the date fields' value
            self.setHighThumb(self.fldTodate.getValue());
            self.setLowThumb(self.fldFromdate.getValue());
        } else if (newVal === 1) {  //select money management plugin if set
            if ((self.cmbMMplugin.getValue() === '') && (self.mmpluginId)) {
                record = self.mMPluginStore.getById(self.mmpluginId);
                if (record) {
                    self.cmbMMplugin.setValue(self.mmpluginId);
                    self.cmbMMplugin.fireEvent('select', self.cmbMMplugin, record);
                }
                self.mmpluginId = 0;
            }
            if ((self.cmbSpreadconfig.getValue() === '') && (self.spreadconfigId)) {
                record = self.spreadConfigStore.getById(self.spreadconfigId);
                if (record) {
                    self.cmbSpreadconfig.setValue(self.spreadconfigId);
                }
                self.spreadconfigId = 0;
            }
            if ((self.cmbRolloverconfig.getValue() === '') && (self.rolloverconfigId)) {
                record = self.rolloverConfigStore.getById(self.rolloverconfigId);
                if (record) {
                    self.cmbRolloverconfig.setValue(self.rolloverconfigId);
                }
                self.rolloverconfigId = 0;
            }
        }
    },

    /**
     * evaluates slider change on card 0
     * @param {DatarowRecord} record the datarow record which is being added or removed
     * @param {Boolean} add if we add or remove the current datarow record
     */
    sliderChange: function(record, add) {
        var self = this;
        var dateFrom = record.get('from_date');     //get dates from record and convert them to days
        var dateFromDays = self.getDays(dateFrom);
        var dateTo = record.get('to_date');
        var dateToDays = self.getDays(dateTo);
        if (add) {                                  //in case of add we may reduce the slider's length
            if (self.chosenStore.getCount() === 0) {    //if there were no datarows before, we set min value and low thumb to from date, max value and high thumb to to date
                self.setSliderMax(dateTo, dateToDays);
                self.setSliderMin(dateFrom, dateFromDays);
                self.setHighThumb(dateTo, dateToDays);
                self.setLowThumb(dateFrom, dateFromDays);
            } else {
                if (self.sldFromto.minValue < dateFromDays) {   //min value should be increased
                    if (self.sldFromto.maxValue <= dateFromDays) {  //new min value is >= than the max value => this should not happen!!
                        return false;
                    }
                    self.setSliderMin(dateFrom, dateFromDays);  //set min value and low thumb
                    self.setHighThumb(self.getDate(self.sldFromto.maxValue), self.sldFromto.maxValue);
                }
                if (self.sldFromto.maxValue > dateToDays) {     //max value should be decreased
                    if (self.sldFromto.minValue >= dateToDays) {    //new max value is <= than the min value => this should not happen!!
                        return false;
                    }
                    self.setSliderMax(dateTo, dateToDays);      //set max value and high thumb
                    self.setLowThumb(self.getDate(self.sldFromto.minValue), self.sldFromto.minValue);
                }
            }
        } else {        //in case of datarow remove we may increase slider's length
            if (self.chosenStore.getCount() === 0) {    //we have no more datarows, the slider and the date fields should be reset
                self.resetSlider();
                self.btnNext.setDisabled(true);
            } else {    //we may increase slider's length based on the current state
                var changeLow = false;
                var changeHigh = false;
                var min = Infinity;
                var max = -Infinity;
                if (self.sldFromto.minValue <= dateFromDays) {  //the removed min value is the maximal slider value
                    changeLow = true;   //the min value may be changed
                }
                if (self.sldFromto.maxValue >= dateToDays) {    //the removed max value is the minimal slider value
                    changeHigh = true;  //the max value may be changed
                }
                if ((changeLow) || (changeHigh)) {
                    self.chosenStore.each(function(record) {
                        if (changeLow) {    //find new max value
                            max = Math.max(max, record.get('from_date'));
                        }
                        if (changeHigh) {   //find new min value
                            min = Math.min(record.get('to_date'), min);
                        }
                    });
                }
                if (changeLow) {            //set new min value
                    self.setSliderMin(new Date(max));
                }
                if (changeHigh) {           //set new max value
                    self.setSliderMax(new Date(min));
                }
            }
        }
        return true;
    },

    /**
     * resets slider and date field values to initial values
     */
    resetSlider: function() {
        var self = this;
        self.sldFromto.setMinValue(0);
        self.sldFromto.setMaxValue(0);
        self.fldFromdate.reset();
        self.fldTodate.reset();
    },

    /**
     * sets slider's low thumb
     * @param {Date} newVal new date to the thumb
     * @param {Integer|undefined} newValDays if set: the new date in days
     */
    setLowThumb: function(newVal, newValDays) {
        var self = this;
        if (!newVal) {
            return;
        }
        if (newValDays === undefined) { //set the new date in days if not set
            newValDays = self.getDays(newVal);
        }
        if (self.sldFromto.getValue(1) <= newValDays) { //if the new value is >= than the high thumb value, we set it to -1;
            newValDays = self.sldFromto.getValue(1) - 1;
            newVal = self.getDate(newValDays);
        }
        self.fldFromdate.setValue(newVal);      //same value to the from date field
        self.sldFromto.setValue(0, newValDays); //new value to thumb
        self.fldTodate.setMinValue(newVal);     //same value to the to date field as min value
    },

    /**
     * sets slider's high thumb
     * @param {Date} newVal new date to the thumb
     * @param {Integer|undefined} newValDays if set: the new date in days
     */
    setHighThumb: function(newVal, newValDays) {
        var self = this;
        if (!newVal) {
            return;
        }
        if (newValDays === undefined) { //set the new date in days if not set
            newValDays = self.getDays(newVal);
        }
        if (self.sldFromto.getValue(0) >= newValDays) {  //if the new value is <= than the low thumb value, we set it to +1;
            newValDays = self.sldFromto.getValue(0) + 1;
            newVal = self.getDate(newValDays);
        }
        self.fldTodate.setValue(newVal);        //same value to the to date field
        self.sldFromto.setValue(1, newValDays);  //new value to thumb
        self.fldFromdate.setMaxValue(newVal);   //same value to the from date field as max value
    },

    /**
     * sets slider's min value
     * @param {Date} newVal new date as min value
     * @param {Integer|undefined} newValDays if set: the new date in days
     */
    setSliderMin: function(newVal, newValDays) {
        var self = this;
        if (newValDays === undefined) { //set the new date in days if not set
            newValDays = self.getDays(newVal);
        }
        if (self.sldFromto.maxValue < newValDays) { //if the new value is > than the slider max value, this should not happen
            return false;
        }
        self.sldFromto.setMinValue(newValDays);     //set new min value
        self.fldFromdate.setMinValue(newVal);       //same value to the from date field as min value
        if (self.fldFromdate.getValue() < newVal) { //if the from date field has lower value than this
            self.fldFromdate.setValue(newVal);      //we need to increase it
        }
    },

    /**
     * sets slider's max value
     * @param {Date} newVal new date as max value
     * @param {Integer|undefined} newValDays if set: the new date in days
     */
    setSliderMax: function(newVal, newValDays) {
        var self = this;
        if (newValDays === undefined) { //set the new date in days if not set
            newValDays = self.getDays(newVal);
        }
        if (self.sldFromto.minValue > newValDays) { //if the new value is < than the slider min value, this should not happen
            return false;
        }
        self.sldFromto.setMaxValue(newValDays);     //set new max value
        self.fldTodate.setMaxValue(newVal);         //same value to the to date field as max value
        if (self.fldTodate.getValue() > newVal) {   //if the to date field has bigger value than this
            self.fldTodate.setValue(newVal);        //we need to decrease it
        }
    },

    /**
     * converts a Date to days
     * @param {Date} date the date we convert to days
     * @return {Integer} the converted int value
     */
    getDays: function(date) {
        return date.getTime() / constants.day2ms;
    },

    /**
     * converts a days to Date
     * @param {Integer} days the days we convert to Date
     * @return {Date} the converted Date
     */
    getDate: function(days) {
        var date = new Date();
        date.setTime(days * constants.day2ms);
        return date;
    },

    /**
     * called after the datarow store has been loaded
     *  the store should contain only downloadwd datarows (having "Done" status)
     *  shows error if some datarows has been filtered out
     * @param {JsonStore} store the datarow store
     * @param {Array} records the loaded records
     */
    datarowStoreOnLoad: function(store, records) {
        var self = this;
        self.datarowStore.filter('status', 'Done');
        var filteredOut = [];
        self.datarowStore.filterBy(function(record) {
            if (record.get('to_date') - record.get('from_date') < config.DATAROW_MINLENGTH_MS) {
                filteredOut.push(record.get('symbol') + '(' + record.get('timeframe_str') + ')');
                return false;
            } else {
                record.set('from_date', record.get('from_date').add(Date.DAY, config.PRELOAD_DAYS));
                record.set('to_date', record.get('to_date').add(Date.DAY, -config.POSTLOAD_DAYS));
                record.commit(true);
            }
            return true;
        });
        if (filteredOut.length > 0) {
            Ext.Msg.alert(locationStrings.failure, String.format(locationStrings.datarowFiltered, config.PRELOAD_DAYS + config.POSTLOAD_DAYS, filteredOut.join(', ')));
        }
    }
});
