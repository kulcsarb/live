AddEditPortfolioUi = Ext.extend(Ext.Window, {
    title: 'Add/Edit Portfolio',
    width: 600,
    modal: true,
    labelWidth: 120,
    autoHeight: true,
    resizable: false,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                monitorValid: true,
                labelWidth: 160,
                ref: 'formBase',
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Name',
                        anchor: '85%',
                        vtype: 'alphanum',
                        allowBlank: false,
                        blankText: 'Enter Portfolio name',
                        emptyText: 'Portfolio name here',
                        ref: '../fldName'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Description',
                        anchor: '85%',
                        allowBlank: true,
                        ref: '../fldDescription'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Management type',
                        store: 'ManagementTypeStore',
                        allowBlank: false,
                        displayField: 'name',
                        valueField: 'id',
                        emptyText: 'Select Management type...',
                        blankText: 'Select Management type',
                        triggerAction: 'all',
                        mode: 'local',
                        forceSelection: true,
                        ref: '../cmbManagementtype'
                    }/*,
                    {
                        xtype: 'combo',
                        fieldLabel: 'Money management plugin',
                        anchor: '85%',
                        store: 'MMPluginStore',
                        name: 'mmpluginId',
                        displayField: 'name',
                        valueField: 'id',
                        allowBlank: false,
                        blankText: 'Select MM plugin',
                        emptyText: 'Select MM plugin...',
                        triggerAction: 'all',
                        mode: 'local',
                        forceSelection: true,
                        ref: '../cmbMMplugin'
                    },
                    {
                        xtype: 'fieldset',
                        title: 'Money Management plugin settings',
                        height: 350,
                        autoScroll: true,
                        ref: '../fsetMmpluginsettings'
                    }*/
                ],
                fbar: {
                    xtype: 'toolbar',
                    buttonAlign: 'left',
                    items: [
                        {
                            xtype: 'button',
                            text: 'Save',
                            formBind: true,
                            type: 'submit',
                            ref: '../../btnSave'
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel',
                            ref: '../../btnCancel'
                        }
                    ]
                }
            }
        ];
        AddEditPortfolioUi.superclass.initComponent.call(this);
    }
});
