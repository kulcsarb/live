/**
 * @fileOverview
 * @author vendelin8
 * @version 0.3
 */
/**
 * admin control of live accounts and strategies
 * @class AdminLive
 * @lends AdminLive.prototype
 */
AdminLive = Ext.extend(AdminLiveUi, {
    mainTitle: 'Actually running strategies',
    actualHeaders: ['User ID', 'Strategy ID', 'Live run ID', 'Strategy name', 'Broker account name', 'Run start', 'Run status', 'Instruments', 'Timeframe', 'Open trades'],
    btnAbortactualText: 'Abort',
    btnViewlogText: 'View Log',
    removeLiverunTitle: 'Remove Live Run from the list?',
    removeLiverunHtml: 'The selected <b>Live Run</b> is going to be removed from the list! Do you want to perform it?',
    /** @constructs */
    initComponent: function() {
        AdminLive.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        setGridColums(self, self.actualHeaders, 1);
        self.btnAbortactual.text = self.btnAbortactualText;
        self.btnViewlog.text = self.btnViewlogText;

        //plugin: the grid's first column contains an expandable + sign which shows status information about the datarow under each row
        self.rowExpander = new Ext.ux.grid.RowExpander({
            tpl: new Ext.XTemplate('<div style="padding:2px;"><tpl for="."><div>{statmsg}</div></tpl></div>'),
            enableCaching: false,
            lazyRender: false
        });
        self.getColumnModel().config.unshift(self.rowExpander);
        self.initPlugin(self.rowExpander);

        //task for repeated load
        self.task = new Ext.util.DelayedTask(function() {
            if (!self.isVisible()) {
                return;
            }
            if (self.getSelectionModel().hasSelection()) {
                var i;
                self.savedSelection = self.getSelectionModel().getSelections();
                for (i = 0; i < self.savedSelection.length; i++) {
                    self.savedSelection[i] = self.actualStore.indexOf(self.savedSelection[i]);
                }
            }
            self.actualStore.reload();
        });

        /** actual store reference */
        self.actualStore = Ext.StoreMgr.lookup('AdminLiveStore');
        self.actualStore.on('exception', storeErrorHandle, self);
        self.actualStore.on('load', function(store, records) {
            for (i in records) {
                if (typeof(records[i]) === 'function') {
                    continue;
                }
//                if (records[i].get('runstatus').search(/fuckedup/i) !== -1) {
                    self.rowExpander.expandRow(parseInt(i), records[i]);
//                }
            }
            if (self.savedSelection) {
                for (var i = 0; i < self.savedSelection.length; i++) {
                    self.getSelectionModel().selectRow(self.savedSelection[i]);
                }
                self.savedSelection = null;
            }
            self.getSelectionModel().fireEvent('selectionchange');
            self.task.delay(1000);
        }, self),

        /**
         * fires when the actual grid's selection changes
         *  changes the modify/view buttons' enablity state
         * @event
         */
        self.getSelectionModel().on('selectionchange', function() {
            var record = self.getSelectionModel().getSelected();
            var newVal = (record === undefined);
            self.btnAbortactual.setDisabled(newVal);
            self.btnViewlog.setDisabled(newVal);
        });

        /**
         * fires when clicked on "Abort Actual" button
         *  aborts selected live run
         * @event
         */
        self.btnAbortactual.on('click', function() {
            var record = self.getSelectionModel().getSelected();
            if (record === undefined) {
                return;
            }
            Ext.Ajax.request({
                url: urls.abortLive,
                params: {'liverunconfigId': record.get('liverunconfigId')},
                success: function(response) {
                    ajaxSuccessHandle(response);
                },
                failure: function(response) {
                    ajaxErrorHandle(response);
                }
            });
        }, self);

        /**
         * fires when clicked on "View log" button
         *  shows log of the selected live run
         * @event
         */
        self.btnViewlog.on('click', function() {
            var record = self.getSelectionModel().getSelected();
            if (record === undefined) {
                return;
            }
            Ext.Ajax.request({
                url: urls.logLive,
                params: {'configId': record.get('liverunconfigId')},
                success: function(response) {
                    response = ajaxSuccessHandle(response);
                    if (!response) {
                        return;
                    }
                    var popup = new AreaInfo({title: self.logTitle});
                    popup.area.setValue(response.data);
                    popup.show();
                },
                failure: function(response) {
                    ajaxErrorHandle(response);
                }
            });
        }, self);

        /**
         * fires when the actual list grid (tab) is shown
         *  reloads the related store
         * @event
         */
        self.on('show', function() {
            self.actualStore.load();
        }, self);
    }
});
