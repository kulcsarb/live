/**
 * @fileOverview
 * @author kulcsarb, vendelin8
 * @version 0.12
 */
/**
 * application main window
 * @class MainViewport
 * @lends MainViewport.prototype
 */
MainViewport = Ext.extend(MainViewportUi, {
    popups: {},
    btnCandidatesText: 'Candidates',
    btnLivemanagementText: 'Live Management',
    btnTrademanagementText: 'Trade Management',
    btnPortfoliomanagementText: 'Portfolio Management',
    btnAccountmanagementText: 'Account Management',
    btnAdminText: 'Administration',
    btnRolloverText: 'Rollover',
    btnSpreadText: 'Spread',
    btnSettingsText: 'Settings',
    btnLogoutText: 'Logout',
    /** @constructs */
    initComponent: function() {
        MainViewport.superclass.initComponent.call(this);
        var self = this;
        self.btnCandidates.text = self.btnCandidatesText;
        self.btnLivemanagement.text = self.btnLivemanagementText;
        self.btnTrademanagement.text = self.btnTrademanagementText;
        self.btnPortfoliomanagement.text = self.btnPortfoliomanagementText;
        self.btnAccountmanagement.text = self.btnAccountmanagementText;
        self.btnAdmin.text = self.btnAdminText;
        self.btnRollover.text = self.btnRolloverText;
        self.btnSpread.text = self.btnSpreadText;
        self.btnSettings.text = self.btnSettingsText;
        self.btnLogout.text = self.btnLogoutText;
        self.headerPanel.title = 'Genetic Daytrader ' + VERSION + ' on ' + PYTHON + ', ' + PLATFORM;

        /**
         * fires when clicked on Candidates button
         *  shows Candidates panel to manage live strategies
         * @event
         */
        self.btnCandidates.on('click', function() {
            if (!self.candidatesP) {
                self.candidatesP = new CandidatesPanel();
            }
            self.showPanel(self.candidatesP, self.btnCandidates);
        });

        /**
         * fires when clicked on "Live Management" button
         *  shows Trade management panel to manage portfolios
         * @event
         */
        self.btnLivemanagement.on('click', function() {
            if (!self.liveManagementP) {
                self.liveManagementP = new LiveManagementPanel();
            }
            self.showPanel(self.liveManagementP, self.btnLivemanagement);
        });

        /**
         * fires when clicked on "Trade Management" button
         *  shows Trade management panel to manage trades
         * @event
         */
        self.btnTrademanagement.on('click', function() {
            if (!self.tradeManagementP) {
                self.tradeManagementP = new TradeManagementPanel();
            }
            self.showPanel(self.tradeManagementP, self.btnTrademanagement);
        });

        /**
         * fires when clicked on "Portfolio Management" button
         *  shows Portfolio management panel to manage portfolios
         * @event
         */
        self.btnPortfoliomanagement.on('click', function() {
            if (!self.portfolioManagementP) {
                self.portfolioManagementP = new PortfolioPanel();
            }
            self.showPanel(self.portfolioManagementP, self.btnPortfoliomanagement);
        });

        /**
         * fires when clicked on "Account Management" button
         *  shows Account management panel to manage broker accounts
         * @event
         */
        self.btnAccountmanagement.on('click', function() {
            if (!self.accountManagementP) {
                self.accountManagementP = new AccountsPanel();
            }
            self.showPanel(self.accountManagementP, self.btnAccountmanagement);
        });

        /**
         * fires when clicked on "Personal History" button
         *  shows personal panel with personal history tab
         * @event
         */
        self.btnSettings.on('click', function() {
//            if (!self.adminSettingsPanel) {
//                self.adminSettingsPanel = new SettingsPanel();
//            }
//            self.showPanel(self.adminSettingsPanel, self.btnAdmin);
        }, self);


        /**
         * fires when clicked on Rollover button
         *  shows rollover window to manage rollover configurations
         * @event
         */
        self.btnRollover.on('click', function() {
            if (!self.adminRolloverW) {
                self.adminRolloverW = new AdminRolloverConfigWindow({'closeAction': 'hide'});
            }
            self.showPopup(self.adminRolloverW, self.btnAdmin, 'adminrollover');
        });

        /**
         * fires when clicked on Spread button
         *  shows spread window to manage spread configurations
         * @event
         */
        self.btnSpread.on('click', function() {
            if (!self.adminSpreadW) {
                self.adminSpreadW = new AdminSpreadConfigWindow({'closeAction': 'hide'});
            }
            self.showPopup(self.adminSpreadW, self.btnAdmin, 'adminspread');
        });

        /**
         * fires when clicked on "Logout" button
         *  logs user out
         * @event
         */
        self.btnLogout.on('click', function() {
            logout();
        });
    },

    /**
     * called when the user clicked on a button, that shows a panel in the main container
     *  if the panel is not equal with the currently shown panel, the shows new panel
     *  if there was a another panel, unclicks the button related to the previous panel
     * @param {Panel} panel the new panel to show
     * @param {Button} button the button that is related to the given panel, and which should be pressed when the panel is shown
     */
    showPanel: function(panel, button) {
        var self = this;
        if (!button.pressed) {  //the button may be clicked through fireEvent, or repressed
            button.toggle();
        }
        var lastPanel = self.mainPanel.items.items[0];
        if (lastPanel === panel) {  //if the active panel's button has been clicked, we need nothing else (already pressed before
            if (lastPanel.button != button) {
                lastPanel.button.toggle();
                panel.button = button;
            }
            return;
        }
        self.mainPanel.removeAll(false);    //remove previous panel if exist
        if (lastPanel) {    //unclick previous panel's button
            lastPanel.hide();
            if ((lastPanel.button.pressed) && (lastPanel.button != button)) {
                lastPanel.button.toggle();
            }
        }
        if (panel) {    //show new panel
            self.mainPanel.add(panel);
            panel.button = button;
            panel.show();
        }
        self.mainPanel.doLayout();
    },

    /**
     * called when the user clicked on a button, that shows a popup window
     *  shows new popup window
     *  if there is a panel in the background, unclicks the button related to it
     * @param {Window} popup the new popup window
     * @param {Button} button the button that is related to the given popup, and which should be pressed when the popup is shown
     */
    showPopup: function(popup, button, name) {
        var self = this;
        if (self.mainPanel.items.items[0]) {
            self.mainPanel.items.items[0].button.toggle();
        }
        self.popup = popup;
        self.popup.show();
        self.popupButton = button;
        button.toggle();
        self.popup.onHide = function() {
            self.popupButton.toggle();
            if (self.mainPanel.items.items[0]) {
                self.mainPanel.items.items[0].button.toggle();
            }
        };
        self.popups.name = popup;
        
    },

    destroy: function() {
        var self = this;
        for (i in self.popups) {
            self.popups[i].destroy();
        }
        MainViewport.superclass.destroy.call(self);
    }
});
