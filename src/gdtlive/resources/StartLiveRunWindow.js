/**
 * @fileOverview
 * @author vendelin8
 * @version 0.3
 */
/**
 * start a new live run
 * @class StartLiveRunWindow
 * @lends StartLiveRunWindow.prototype
 */
StartLiveRunWindow = Ext.extend(StartLiveRunWindowUi, {
    mainTitle: 'Start live run',
//    fsetBrokeraccountTitle: 'Broker account',
    cmbAccountLabel: 'Broker accont',
    cmbAccountBlank: 'Select broker account',
    cmbAccountEmpty: 'Select broker account...',
    cmbTimeframeLabel: 'Timeframe',
    cmbTimeframeBlank: 'Select timeframe',
    cmbTimeframeEmpty: 'Select timeframe...',
    btnStartText: 'Start',
    btnCancelText: 'Cancel',
    /** @constructs */
    initComponent: function() {
        StartLiveRunWindow.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
//        self.fsetBrokeraccount.title = self.fsetBrokeraccountTitle;
        self.cmbAccount.fieldLabel = self.cmbAccountLabel;
        self.cmbAccount.blankText = self.cmbAccountBlank;
        self.cmbAccount.emptyText = self.cmbAccountEmpty;
        self.cmbTimeframe.fieldLabel = self.cmbTimeframeLabel;
        self.cmbTimeframe.blankText = self.cmbTimeframeBlank;
        self.cmbTimeframe.emptyText = self.cmbTimeframeEmpty;
        self.btnStart.text = self.btnStartText;
        self.btnCancel.text = self.btnCancelText;

        /** broker account store reference */
        self.brokerAccountStore = Ext.StoreMgr.lookup('BrokerAccountLiveStore');
        self.brokerAccountStore.on('exception', storeErrorHandle, self);
        /** timeframe store reference */
//        self.timeframeStore = Ext.StoreMgr.lookup('LiveTimeframeStore');
//        self.timeframeStore.on('exception', storeErrorHandle, self);
//        self.timeframeStore.on('load', function() {
//            if (self.timeframeStore.getCount() === 1) {
//                var record = self.timeframeStore.getAt(0);
//                if (record) {
//                    self.cmbTimeframe.setValue(record.get(self.cmbTimeframe.valueField));
//                }
//            }
//        }, self);
        self.timeframeStore = lazyStoreR('TimeframeStore');

        /**
         * fires when clicked on "Start" button
         *  starts live run
         * @event
         */
        self.btnStart.on('click', function() {
            var params = self.formStart.getForm().getFieldValues();  //variables from the first card
            params.userId = currentUser.id;
            params.strategyIds = self.recordIds;
            Ext.Ajax.request({
                url: urls.startLive,
                params: params,
                success: function(response) {
                    response = ajaxSuccessHandle(response);
                    if (!response) {
                        return;
                    }
                    if (self.showOnSuccess) {
                        self.showOnSuccess.show();
                    }
                    self.hide();
                },
                failure: function(response) {
                    ajaxErrorHandle(response);
                }
            });
        }, self);

        /**
         * fires when clicked on "Cancel" button
         *  closes window
         * @event
         */
        self.btnCancel.on('click', function() {self.hide();}, self);

        /**
         * fires when the window is shown
         *  resets the input fields and (re)loads necessary stores
         * @event
         */
        self.on('show', function() {
            self.brokerAccountStore.load({params: {'userId': currentUser.id}});
            self.cmbAccount.reset();
            self.cmbTimeframe.reset();
//            self.timeframeStore.load({params: {'strategyId': self.recordId}});
            self.timeframeStore.load();
        }, self);
    }
});
