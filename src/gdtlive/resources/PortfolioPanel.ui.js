PortfolioPanelUi = Ext.extend(Ext.grid.GridPanel, {
    title: 'All Portfolios',
    store: 'PortfolioStore',
    height: 550,
    padding: 5,
    initComponent: function() {
        this.selModel = new Ext.grid.CheckboxSelectionModel();
        this.columns = [
            this.selModel,
            {
                xtype: 'gridcolumn',
                dataIndex: 'id',
                header: 'Id',
                sortable: false,
                width: 50
            },
            {
                xtype: 'gridcolumn',
                dataIndex: 'name',
                header: 'Name',
                sortable: true,
                width: 60
            },
            {
                xtype: 'gridcolumn',
                header: 'Description',
                sortable: false,
                width: 150,
                dataIndex: 'description'
            },
//            {
//                xtype: 'gridcolumn',
//                header: 'State',
//                sortable: true,
//                width: 80,
//                dataIndex: 'state'
//            },
            {
                xtype: 'gridcolumn',
                header: 'Management type',
                sortable: true,
                width: 105,
                dataIndex: 'management_type_name'
            }
        ];
        this.tbar = {
            xtype: 'toolbar',
            buttonAlign: 'left',
            items: [
                {
                    xtype: 'button',
                    text: 'Create',
                    ref: '../btnCreate'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Delete',
                    ref: '../btnDelete'
                }
            ]
        };
        PortfolioPanelUi.superclass.initComponent.call(this);
    }
});
