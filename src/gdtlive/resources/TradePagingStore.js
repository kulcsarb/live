var TradeStore = new Ext.ux.maximgb.tg.AdjacencyListStore({
    url: 'trade/gridlist',
    autoSave: false,
    batch: true,
    storeId: 'TradePagingStore',
    remoteSort: true,
    baseParams: {
        start: 0,
        limit: 20,
        sort: "id",
        dir: "ASC"
    },
    reader: new Ext.data.JsonReader(
        {
            totalProperty: 'total',
            successProperty: 'success',
            id: '_id',
            root: 'data'
        },
        Ext.data.Record.create([
            {
                name: '_id',
                type: 'string'
            },
            {
                name: '_parent',
                type: 'auto'
            },
            {
                name: '_is_leaf',
                type: 'boolean'
            },
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'type',
                type: 'string'
            },
            {
                name: 'portfolio_name',
                type: 'string'
            },
            {
                name: 'account_name',
                type: 'string'
            },
            {
                name: 'strategy_id',
                type: 'auto'
            },
            {
                name: 'strategyrun_id',
                type: 'auto'
            },
            {
                name: 'group_id',
                type: 'auto'
            },
            {
                name: 'direction',
                type: 'string'
            },
            {
                name: 'amount',
                type: 'auto'
            },
            {
                name: 'current_amount',
                type: 'auto'
            },
            {
                name: 'open_price',
                type: 'auto'
            },
            {
                name: 'symbol',
                type: 'string'
            },
            {
                name: 'state',
                type: 'string'
            },
            {
                name: 'open_fill_time',
                type: 'date',
                dateFormat: 'Y-m-d H:i:s'
            },
            {
                name: 'close_price',
                type: 'auto'
            },
            {
                name: 'close_fill_time',
                type: 'date',
                dateFormat: 'Y-m-d H:i:s'
            },
            {
                name: 'entry_price',
                type: 'auto'
            },
            {
                name: 'fill_price',
                type: 'auto'
            },
            {
                name: 'fill_time',
                type: 'date',
                dateFormat: 'Y-m-d H:i:s'
            },
            {
                name: 'portfolio_id',
                type: 'int'
            },
            {
                name: 'account_id',
                type: 'int'
            }
        ])
    )
});
TradeStore.paramNames.active_node = 'tradeId';
