/**
 * @fileOverview
 * @author vendelin8
 * @version 0.3
 */
/**
 * portfolio management
 * @class PortfolioPanel
 * @lends PortfolioPanel.prototype
 */
AccountsPanel = Ext.extend(AccountsPanelUi, {
    mainTitle: 'All Accounts',
    gridHeaders: ['Id', 'Boker name', 'Account name', 'Username in Broker system', 'Sender Comp Id', 'Target Comp Id', 'Base Capital', 'Leverage', 'Risk multiplier', 'Server url', 'Server port', 'Use ssl'],
    btnCreateText: 'Create',
    btnEditText: 'Edit',
    btnAddtoportfolioText: 'Add to Portfolio',
    btnDeleteText: 'Delete',
    btnViewText: 'View',
    /** @constructs */
    initComponent: function() {
        AccountsPanel.superclass.initComponent.call(this);
        var self = this;
        self.setTitle(self.mainTitle);
        setGridColums(self, self.gridHeaders, 1);
        self.btnCreate.text = self.btnCreateText;
        self.btnEdit.text = self.btnEditText;
        self.btnAddtoportfolio.text = self.btnAddtoportfolioText;
        self.btnDelete.text = self.btnDeleteText;
        self.btnView.text = self.btnViewText;

        /** Broker Account store reference */
        self.AccountsStore = storeCRUD('AccountsStore', self, self);
        self.AccountsStore.on('load', function() {
            self.getSelectionModel().fireEvent('selectionchange');
        }, self);
        self.AccountsStore.on('write', function() {
            self.AccountsStore.reload();
        }, self);

        self.brokernameAccountStore = Ext.StoreMgr.lookup('BrokerNameStore');
        self.brokernameAccountStore.on('exception', storeErrorHandle, self);
        self.brokernameAccountStore.on('load', function(store, records) {
            if ((self.popup) && (self.popup.afterbrokernameAccountStoreLoad)) {
                self.popup.afterbrokernameAccountStoreLoad(records);
            }
            if ((records.length) && (gridCell === undefined)) {
                gridCell = self.getView().getCell(0, 2);
            }
        }, self);

        /**
         * fires when clicked on "Create Broker account" button
         *  shows empty AddEditBrokerAccount
         * @event
         */
        self.btnCreate.on('click', function() {
            self.popup = new AddEditBrokerAccount();
            self.popup.record = null;
            self.brokernameAccountStore.load();
            self.popup.show();
        }, self);

        /**
         * fires when clicked on "Edit Broker account" button
         *  shows AddEditBrokerAccount with account properties
         * @event
         */
        self.btnEdit.on('click', function() {
            var selected = self.getSelectionModel().getSelected();
            if (!selected) {
                return;
            }
            self.popup = new AddEditBrokerAccount();
            self.popup.record = selected;
            self.brokernameAccountStore.load();
            self.popup.show();
        }, self);

        /**
         * fires when clicked on "Add to Portfolio" button
         *  shows portfolio chooser window
         * @event
         */
        self.btnAddtoportfolio.on('click', function() {
            var selected = self.getSelectionModel().getSelections();
            if ((!selected) || (typeof(selected) !== 'object') || (selected.length < 1)) {
                return;
            }
            self.popup = new AddToPortfolio();
            self.popup.element_items = Ext.util.JSON.encode(collectIds(selected));
            self.popup.element_type = 'account';
            self.popup.show();
        }, self);

         /**
         * fires when clicked on "Export" button
         *  shows "save as..." popup with the dns (dna) of the selected strategies
         * @event
         */
        self.btnDelete.on('click', function() {
            var selected = self.getSelectionModel().getSelections(); //get chosen datarow ids
            console.log(selected);
        }, self);

        self.btnView.on('click', function() {
            var selected = self.getSelectionModel().getSelections();
            if ((!selected) || (typeof(selected) !== 'object') || (selected.length < 1)) {
                return;
            }
            mainViewport.liveManagementP.addViews(selected, LIVE_ACCOUNTS);
            mainViewport.btnLivemanagement.fireEvent('click');
        }, self);

       /**
         * fires when the datarow grid's selection has been changed
         *  changes control buttons' state based on selected record's status
         * @event
         */
        self.getSelectionModel().on('selectionchange', function() {
            var record = self.getSelectionModel().getSelected();
            var newVal = (record === undefined);
            var records = self.getSelectionModel().getSelections();
            self.btnEdit.setDisabled((newVal) || (records.length > 1));
            self.btnAddtoportfolio.setDisabled(newVal);
            self.btnDelete.setDisabled(newVal);
            self.btnView.setDisabled(newVal);
        }, self);

        /**
         * fires when the panel is rendered
         *  sets store paging size
         * @event
         */
        self.on('afterrender', function() {
            self.AccountsStore.load();
        }, self);
    }
});
