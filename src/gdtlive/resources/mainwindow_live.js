var popupNotifyDelay = 10000;
var messageAtTheSameTime = 10;
var logoutTitle = 'Logout from GDTLive?';
var logoutHtml = 'Do you really want to logout from GDTLive?';
var mainViewport, loginWindow, currentUser, config, browseAutoView, popupNotifyTask, configId, backtestWindow, gridCell;

Ext.Ajax.disableCaching = false;

LIVE_TRADE_ENGINE = 7;
LIVE_PORTFOLIO = 8;
LIVE_ACCOUNTS = 9;
LIVE_ACCOUNT = 10;
LIVE_STRATEGY_RUNS = 11;
BACKTEST_CONFIG = 12;
LIVERUN_CONFIG = 13;

TREE_CHILDREN = {}
TREE_CHILDREN[LIVE_TRADE_ENGINE] = LIVE_PORTFOLIO;
TREE_CHILDREN[LIVE_PORTFOLIO] = LIVE_ACCOUNTS;
TREE_CHILDREN[LIVE_ACCOUNTS] = LIVE_ACCOUNT;

TREENODE_NAMES = []
TREENODE_NAMES[LIVE_TRADE_ENGINE] = "Portfolios"
TREENODE_NAMES[LIVE_PORTFOLIO] = "Strategies"
TREENODE_NAMES[LIVE_ACCOUNTS] = "Accounts"
TREENODE_NAMES[BACKTEST_CONFIG] = "Backtests"
TREENODE_NAMES[LIVERUN_CONFIG] = "Live runs"

SERVICE_NAMES = []
SERVICE_NAMES[LIVE_TRADE_ENGINE] = "portfolio"
SERVICE_NAMES[LIVE_PORTFOLIO] = "strategy"
SERVICE_NAMES[LIVE_ACCOUNTS] = "account"
SERVICE_NAMES[LIVE_ACCOUNT] = "trade"

STATIC_NODES = {}
STATIC_NODES[LIVE_PORTFOLIO] = '';

function showMessages(response) {
    if (!response.length) {
        popupNotifyTask.delay(popupNotifyDelay);
        return;
    }
    Ext.Msg.alert('Notification', response.slice(0, messageAtTheSameTime).join("\n"), function() {
        showMessages(response.slice(messageAtTheSameTime));
    });
}

function startPopupNotify() {
    if (!popupNotifyTask) {
        popupNotifyTask = new Ext.util.DelayedTask(function() {
            var toScreen;
            Ext.Ajax.request({
                url: urls.popupMessages,
                params: {userId: currentUser.id},
                success: function(response) {
                    response = ajaxSuccessHandle(response);
                    if (!response) {
                        return;
                    }
                    response = response.data;
                    showMessages(response);
                },
                failure: function(response) {
                    ajaxErrorHandle(response);
                }
            });
        });
    }
    popupNotifyTask.delay(popupNotifyDelay);
}

function stopPopupNotify() {
    if (!popupNotifyTask) {
        return;
    }
    popupNotifyTask.cancel();
    delete popupNotifyTask;
    popupNotifyTask = null;
}

/**
 * shows main screen with the main menu in the header
 *  shows the buttons related and hides the ones unrelated to the user's authority
 * @param {Object} user the user object
 */
function showMainScreen(user) {
    if (loginWindow) {
        loginWindow.hide();
    }
    currentUser = user;
    if (currentUser.popup_notify) {
        startPopupNotify();
    }
    if (mainViewport) {
        mainViewport.destroy();
    }
    mainViewport = new MainViewport();
//    if ((mainViewport.gridContent.rendered) && (mainViewport.colModels) && (mainViewport.colModels[BROWSE_LIVE_STRATEGY])) {
//        mainViewport.reconfigureContent(BROWSE_LIVE_STRATEGY);
//    }
    mainViewport.headerPanel.show();
    mainViewport.mainPanel.show();
    mainViewport.doLayout();
    mainViewport.btnLivemanagement.fireEvent('click');
}

/**
 * shows login screen (when the user is not logged in)
 */
function showLoginScreen() {
    if (!loginWindow) {
        loginWindow = new LoginWindow();
    }
    loginWindow.show();
}

/**
 * closes main menu (on logout, or when the session is closed somehow)
 */
function closeMenu() {
    mainViewport.headerPanel.hide();
    mainViewport.mainPanel.hide();
    showLoginScreen();
}

/**
 * logout from gdtlive
 */
function logout() {
    confirmWindow(logoutTitle, logoutHtml, function(callbackOnSuccess) {
        stopPopupNotify();
        Ext.Ajax.request({
            url: urls.logout,
            disableCaching : false,
            success: function(response, opts) {
                response = ajaxSuccessHandle(response);
                if (!response) {
                    return;
                }
                closeMenu();
                callbackOnSuccess();
           },
           failure: function(response, opts) {
              ajaxErrorHandle(response);
           }
        });
    });
}

/**
 * gets session info from server
 *  saves config options
 *  if session is inactive, shows login screen
 *  otherwise shows main menu
 */
function checkUser() {
    Ext.Ajax.request({
        url: urls.userInfo,
        disableCaching : false,
        success: function(response, opts) {
            response = ajaxSuccessHandle(response);
            if (!response) {
                return;
            }
            if (response.loggedin === true) {
                config = response.config;
                config.DATAROW_MINLENGTH_MS = (config.PRELOAD_DAYS + config.POSTLOAD_DAYS) * constants.day2ms;
                showMainScreen(response.user);
            } else {
                showLoginScreen();
            }
        },
        failure: function(response, opts) {
            ajaxErrorHandle(response);
        }
    });
}

/**
 * fires when Ext is ready to run
 * starts gdtlive gui
 */
Ext.onReady(function() {
    Ext.QuickTips.init();
    Ext.Ajax.timeout = 4500000;
    mainViewport = new MainViewport();
    checkUser();
});
