# -*- encoding: utf-8 -*-
'''
A letöltő osztály, és különféle segédfunkciók a Dukascopy-hoz

@author: kulcsarb
'''
from gdtlive.utils import first0#, hashed, Candle, AskBidCandle
from gdtlive.constants import PRICE_ASK, PRICE_BID, PRICE_BIDASK, TIMEFRAME, STORAGE_PATH
from gdtlive.store.db import DatarowDescriptor, DatarowData
from gdtlive.config import MAX_DOWNLOAD_RETRIES, HOLIDAY_LIMIT_HOURS
import gdtlive.store.db as db
from sqlalchemy.exc import IntegrityError
from sqlalchemy.sql import and_
from datetime import timedelta, date, datetime, time
from os.path import dirname, normpath
#from time import sleep
#from collections import namedtuple
import traceback
import threading
import struct
import zipfile
import urllib
import os
#import random
import calendar
import logging

log = logging.getLogger('gdtlive.historic.plugin.dukascopy')

DUKAS_HISTORIC_DATA = {
        'AUDJPY' : {
                    15:     datetime(2004, 11, 1),
                    60:    date(2003, 12, 1),
                    1440:  date(2003, 12, 1),
                    10080:     date(2003, 1, 1),
                    302400:    date(2003, 1, 1),
                    },
        'AUDNZD' : {
                    60:    date(2009, 4, 1),
                    240:    date(2009, 4, 1),
                    1440:  date(2009, 4, 1),
                    },
        'AUDUSD' : {
                    1:      datetime(2003, 9, 1),
                    15:     datetime(2004, 11, 1),
                    60:    date(2003, 8, 1),
                    240:    date(2005, 4, 1),
                    1440:  date(2003, 8, 1),
                    10080:     date(2003, 1, 1),
                    302400:    date(2003, 1, 1),
                    },
        'CADJPY' : {
                    60:    date(2005, 5, 1),
                    240:    date(2005, 5, 1),
                    1440:  date(2005, 5, 1),
                    10080:     date(2005, 1, 1),
                    302400:    date(2005, 1, 1),
                    },
        'CHFJPY' : {
                    60:    date(2003, 8, 1),
                    240:    date(2005, 4, 1),
                    1440:  date(2003, 8, 1),
                    10080:     date(2003, 1, 1),
                    302400:    date(2003, 1, 1),
                    },
        'EURAUD' : {
                    60:    date(2008, 1, 1),
                    240:    date(2008, 1, 1),
                    1440:  date(2008, 1, 1),
                    10080:     date(2008, 1, 1),
                    302400:    date(2008, 1, 1),
                    },
        'EURCAD' : {
                    60:    date(2008, 10, 1),
                    240:    date(2008, 10, 1),
                    1440:  date(2008, 10, 1),
                    10080:     date(2009, 1, 1),
                    302400:    date(2009, 1, 1),
                    },
        'EURCHF' : {
                    1:      datetime(2003, 9, 1),
                    15:     datetime(2005, 1, 1),
                    60:    date(2003, 8, 1),
                    240:    date(2005, 4, 1),
                    1440:  date(2003, 8, 1),
                    10080:     date(2003, 1, 1),
                    302400:    date(2003, 1, 1),
                    },
        'EURGBP' : {
                    1:      datetime(2003, 9, 1),
                    15:     datetime(2005, 1, 1),
                    60:    date(2003, 8, 1),
                    240:    date(2005, 4, 1),
                    1440:  date(2003, 8, 1),
                    10080:     date(2003, 1, 1),
                    302400:    date(2003, 1, 1),
                    },
        'EURJPY' : {
                    1:      datetime(2003, 9, 1),
                    15:     datetime(2005, 1, 1),
                    60:    date(2003, 8, 1),
                    240:    date(2005, 4, 1),
                    1440:  date(2003, 8, 1),
                    10080:     date(2003, 1, 1),
                    302400:    date(2003, 1, 1),
                    },
        'EURUSD' : {
                    1:      datetime(2003, 9, 1),
                    15:     datetime(2004, 11, 1),
                    60:    date(2003, 8, 1),
                    240:    date(2005, 4, 1),
                    1440:  date(2000, 1, 1),
                    10080:     date(1986, 1, 1),
                    302400:    date(1986, 1, 1),
                    },
        'EURNOK' : {
                    60:    date(2009, 1, 1),
                    240:    date(2009, 1, 1),
                    1440:  date(2009, 1, 1),
                    10080:     date(2009, 1, 1),
                    302400:    date(2009, 1, 1),
                    },
        'EURSEK' : {
                    60:    date(2004, 11, 1),
                    240:    date(2004, 4, 1),
                    1440:  date(2004, 11, 1),
                    10080:     date(2004, 1, 1),
                    302400:    date(2004, 1, 1),
                    },
        'GBPCHF' : {
                    1:      datetime(2003, 9, 1),
                    15:     datetime(2004, 11, 1),
                    60:    date(2003, 1, 1),
                    240:    date(2005, 4, 1),
                    1440:  date(2003, 1, 1),
                    10080:     date(2003, 1, 1),
                    302400:    date(2003, 1, 1),
                    },
        'GBPJPY' : {
                    1:      datetime(2003, 9, 1),
                    15:     datetime(2004, 11, 1),
                    60:    date(2003, 8, 1),
                    240:    date(2005, 4, 1),
                    1440:  date(2003, 8, 1),
                    10080:     date(2003, 1, 1),
                    302400:    date(2003, 1, 1),
                    },
        'GBPUSD' : {
                    1:      datetime(2003, 9, 1),
                    15:     datetime(2004, 11, 1),
                    60:    date(2004, 1, 1),
                    240:    date(2005, 4, 1),
                    1440:  date(1998, 1, 1),
                    10080:     date(1996, 1, 1),
                    302400:    date(1997, 1, 1),
                    },
        'NZDUSD' : {
                    1:      datetime(2003, 9, 1),
                    15:     datetime(2004, 11, 1),
                    60:    date(2003, 8, 1),
                    240:    date(2005, 10, 1),
                    1440:  date(2003, 8, 1),
                    10080:     date(2003, 1, 1),
                    302400:    date(2003, 1, 1),
                    },
        'USDCAD' : {
                    1:      datetime(2003, 9, 1),
                    15:     datetime(2004, 11, 1),
                    60:    date(2003, 8, 1),
                    240:    date(2005, 4, 1),
                    1440:  date(2003, 8, 1),
                    10080:     date(2003, 1, 1),
                    302400:    date(2003, 1, 1),
                    },
        'USDCHF' : {
                    1:      datetime(2003, 9, 1),
                    15:     datetime(2004, 11, 1),
                    60:    date(2003, 8, 1),
                    240:    date(2005, 4, 1),
                    1440:  date(1998, 1, 1),
                    10080:     date(1996, 1, 1),
                    302400:    date(1997, 1, 1),
                    },
        'USDJPY' : {
                    1:      datetime(2003, 9, 1),
                    15:     datetime(2004, 11, 1),
                    60:    date(2003, 8, 1),
                    240:    date(2005, 4, 1),
                    1440:  date(1998, 8, 1),
                    10080:     date(1996, 1, 1),
                    302400:    date(1997, 1, 1),
                    },
        'USDNOK' : {
                    60:    date(2003, 8, 1),
                    240:    date(2008, 8, 1),
                    1440:  date(2003, 8, 1),
                    10080:     date(2003, 1, 1),
                    302400:    date(2003, 1, 1),
                    },
        'USDSEK' : {
                    60:    date(2003, 8, 1),
                    240:    date(2008, 8, 1),
                    1440:  date(2003, 8, 1),
                    10080:     date(2003, 1, 1),
                    302400:    date(2003, 1, 1),
                    }
        }



NAMES = {1 : 'min_1',
                5 : 'min_5',
                15 : 'min_15',
                30 : 'min_30',
                60 : 'hour_1',
                240 : 'hour_4',
                1440 : 'day_1',
                10080: 'week_1',
                302400: 'month_1'}



days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]


def initialize_historic_configs():
    session = db.Session()
    config = db.HistoricPluginsConfig('Dukascopy', 1)
    for symbol in sorted(DUKAS_HISTORIC_DATA.iterkeys()):
        for timeframe_num in sorted(DUKAS_HISTORIC_DATA[symbol].iterkeys()):
            config.rows.append(db.HistoricPluginsConfigsInstrument(symbol, timeframe_num, DUKAS_HISTORIC_DATA[symbol][timeframe_num]))
    session.add(config)
    session.commit()
    session.close()



def iteryear(start_date, end_date):
    year = start_date.year
    while year <= end_date.year:
        yield year
        year += 1

def itermonths(start_date, end_date):
    year = start_date.year
    month = start_date.month
    yield (year, month)
    while not (year == end_date.year and month == end_date.month):
        if month < 12:
            month += 1
        else:
            year += 1
            month = 1
        yield (year, month)


def iterdays(start_date, end_date):
    days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    e_year = end_date.year
    e_month = end_date.month
    e_day = end_date.day
    year = start_date.year
    month = start_date.month
    day = start_date.day
    if e_day == days_in_month[e_month - 1]:
        e_day = 1
        if e_month < 12:
            e_month += 1
        else:
            e_month = 1
            e_year += 1
    else:
        e_day += 1

    while not (year == e_year and month == e_month and day == e_day):
        yield (year, month, day)
        if day < days_in_month[month - 1]:
            day += 1
        else:
            if month == 2 and year % 4 == 0 and day == days_in_month[month - 1]:
                day += 1
            else:
                day = 1
                if month < 12:
                    month += 1
                else:
                    year += 1
                    month = 1

def candle_num(seq, timeframe):
    result = 0
    if timeframe == 302400:
        result = 12
    elif timeframe == 10080:
        result = 52
    elif timeframe in [1440, 240, 60]:
        days = days_in_month[seq[1] - 1]
        if seq[1] == 2 and seq[0] % 4 == 0:
            days += 1
        result = days * 1440 / timeframe
        # óra visszaállítás októberben....
        if seq[1] == 10 and timeframe == 60:
            result += 1
        # óra előreállitás márciusban
        if seq[1] == 3 and timeframe == 60:
            result -= 1
    else:
        result = 24 * 60 / timeframe

    return result


def create_path(symbol, t, timeframe, ptype = PRICE_ASK):
    result = ''
    price = {PRICE_ASK: 'ASK', PRICE_BID: 'BID'}
    try:
        if type(t) == int:
            result = "%s/%d/%s_candles_%s.bin" % (symbol, t, price[ptype], NAMES[timeframe])
        else :
            if len(t) == 2:
                result = "%s/%d/%02d/%s_candles_%s.bin" % (symbol, t[0], t[1] - 1, price[ptype], NAMES[timeframe])
            elif len(t) == 3:
                result = "%s/%d/%02d/%02d/%s_candles_%s.bin" % (symbol, t[0], t[1] - 1, t[2], price[ptype], NAMES[timeframe])
    except Exception, e:
        log.error(str(e))
    return result


def create_historic_filename(symbol, t, timeframe):
    result = None
    try:
        if type(t) == int:
            result = "%s/%s/%d/candles_%s.bin" % (DukascopyDownloader.storage_path, symbol, t, NAMES[timeframe])
        else :
            if len(t) == 2:
                result = "%s/%s/%d/%02d/candles_%s.bin" % (DukascopyDownloader.storage_path, symbol, t[0], t[1] - 1, NAMES[timeframe])
            elif len(t) == 3:
                result = "%s/%s/%d/%02d/%02d/candles_%s.bin" % (DukascopyDownloader.storage_path, symbol, t[0], t[1] - 1, t[2], NAMES[timeframe])
    except Exception, e:
        log.error(str(e))
    return result


def save_candles(candles, file, timeframe):
    path = dirname(file[PRICE_ASK])
    filename = "candles_%s.bin" % NAMES[timeframe]
    log.info('Saving %s' % filename)
    with open(path + os.sep + filename, 'wb+') as file:
        for candle in candles:
            buffer = struct.pack('>qddddddddd', candle[0], candle[1], candle[2], candle[3], candle[4], candle[5], candle[6], candle[7], candle[8], candle[9])
            file.write(buffer)



def load_historic_file(filename):
    candles = []
    try:
        with open(filename, 'rb') as file:
            data = file.read(80)
            while data:
                candles.append(struct.unpack('>qddddddddd', data))
                data = file.read(80)
    except:
        #log.error('file not found: %s' % filename)
        pass
    return candles


def load_candles(filename):
    result = []
    try:
        if zipfile.is_zipfile(filename):
            zip = zipfile.ZipFile(filename, 'r')
            f = zip.open(zip.namelist()[0], 'r')
            data = f.read(48)
            while data:
                result.append(unpackCandle(data))
                data = f.read(48)
            zip.close()
        else:
            with open(filename) as f:
                data = f.read(48)
                while data:
                    result.append(unpackCandle(data))
                    data = f.read(48)
    except (IOError, RuntimeError) as e:
        log.error(str(e))

    return result

def unpackCandle(data):
    result = struct.unpack('>qddddd', data)
    #        time            open        high        low        close     volume
    return (result[0] / 1000, result[1], result[4], result[3], result[2], result[5])


def first_time(t, timeframe):
    c = calendar.Calendar()
    result = None
    if timeframe == 302400:
        result = datetime(t, 1, 1)
    elif timeframe == 10080:
        result = c.itermonthdates(t, 1).next()
        if result.year < t:
            result += timedelta(weeks = 1)
        result = datetime(result.year, result.month, result.day)
    else :
        result = datetime(t[0], t[1], 1, 0, 0)

    return result


def last_time(t, timeframe):
    c = calendar.Calendar()
    result = None
    if timeframe == 302400:
        result = datetime(t, 12, 31)
    elif timeframe == 10080:
        result = c.itermonthdates(t + 1, 1).next()
        if result.year > t:
            result -= timedelta(weeks = 1)
        result = datetime(result.year, result.month, result.day)
    elif timeframe in [1440, 240, 60]:
        if t[1] < 12:
            result = datetime(t[0], t[1] + 1, 1)
        else :
            result = datetime(t[0] + 1, 1, 1)
        result -= timedelta(minutes = timeframe)
    else :
        result = datetime(t[0], t[1], t[2])
        result += timedelta(days = 1)
        result -= timedelta(minutes = timeframe)


    return result


def merge_candles(ask_candles, bid_candles):
    result = []
    try:
        length = len(ask_candles)
        for i in range(length):
            ask = ask_candles[i]
            bid = bid_candles[i]
            candle = (ask[0], ask[1], ask[2], ask[3], ask[4], bid[1], bid[2], bid[3], bid[4], ask[5] + bid[5])
            result.append(candle)
    except:
        log.error(traceback.format_exc())

    return result


def download_file(url, target_file):
    try:
        dir = dirname(target_file)
        if not os.path.exists(dir):
            os.makedirs(dir)
    except OSError, e:
        log.error(traceback.format_exc())
        return False

    try:
        log.info('downloading %s -> %s' % (url, target_file))
        urllib.urlretrieve(url, target_file)
        if os.path.exists(target_file) and zipfile.is_zipfile(target_file):
            return True
        else:
            log.error('downloaded file doesnt exists, or isnt zip archive! Maybe data is missing from Dukascopy website')
            try:
                os.remove(target_file)
            except OSError, e:
                log.error(traceback.format_exc())

    except Exception, e:
        log.error(traceback.format_exc())

    return False


def load_datarow(symbol, timeframe, from_date, to_date):
    sequences = [d for d in DukascopyDownloader.ITERATOR[timeframe](from_date, to_date)]
    candles = []    
    for seq in sequences:
        filename = create_historic_filename(symbol, seq, timeframe)
        if os.path.exists(filename):
            result = load_historic_file(filename)
            candles.extend(result)
        else:
            log.error('requested file doesnt exists: %s' % filename)
            break
    return candles


def load_datarow_first(symbol, timeframe, from_date):
    holidayDiff = timedelta(hours = HOLIDAY_LIMIT_HOURS)    #the maximum time that is not a holiday
    seq = [d for d in DukascopyDownloader.ITERATOR[timeframe](from_date - holidayDiff, from_date + holidayDiff)]
    filename = create_historic_filename(symbol, seq[0], timeframe)
    result = load_historic_file(filename)
    from_date = datetime.combine(from_date, time())
    tempTime = []   #list of the time values where volume == 0 one after each other
    for d in result:
        t = datetime.utcfromtimestamp(d[0])
        if t < from_date - holidayDiff:
            continue
        if d[1] == d[2] and d[2] == d[3] and d[3] == d[4]:        
            tempTime.append(t)  #add time value to the list
        elif t >= from_date:
            if len(tempTime) == 0 or tempTime[-1] - tempTime[0] > holidayDiff:
                return t
            elif tempTime[-1] > from_date:
                return tempTime[-1]
            else:
                return t
        else:
            tempTime = []


def check_datafiles(start_date, end_date, symbol, timeframe_num):
    sequences = [d for d in DukascopyDownloader.ITERATOR[timeframe_num](start_date, end_date)]
    from_date = to_date = None
    last_seq = sequences[0]
    candle_per_file = 0
    candle_num = 0
    for seq in sequences:
        filename = create_historic_filename(symbol, seq, timeframe_num)
        if not from_date:
            if os.path.exists(filename):
                candles = load_historic_file(filename)
                if candles:
                    candle_per_file = len(candles)
                    from_date = datetime.utcfromtimestamp(candles[0][0])

        if from_date and not os.path.exists(filename):
            break

        candle_num += candle_per_file
        last_seq = seq

    filename = create_historic_filename(symbol, last_seq, timeframe_num)
    candles = load_historic_file(filename)
    if candles:
        to_date = datetime.utcfromtimestamp(candles[-1][0])

    return from_date, to_date, candle_num


def cleanup_datarow(id):
    if not id:
        return
    try:
        session = db.Session()
        datarow = session.query(DatarowDescriptor).get(id)
        if datarow:
            log.info('cleaning up datarow: %s' % (datarow.symbol + TIMEFRAME[datarow.timeframe_num]))
            datarow.status = "Verifying"
            session.commit()

            start_date = datarow.from_date.date()
            end_date = date.today()
            #end_date = datarow.to_date.date()

            from_date, to_date, candle_num = check_datafiles(start_date, end_date, datarow.symbol, datarow.timeframe_num)

            if not from_date or not to_date:
                log.info('no data files found for datarow %s' % (datarow.symbol+TIMEFRAME[datarow.timeframe_num]))
                #log.info('no data file found, delete DatarowDescriptor')            
                #session.query(DatarowData).filter(DatarowData.datarowId==datarow.id).delete()
                #session.delete(datarow)
                pass
            else:
                log.info('updating datarow to current state')
                datarow.invalid_count = 0
                datarow.bar_count = candle_num
                datarow.status_text = 'datarow contains %d total, and %d invalid candles<br>' % (datarow.bar_count, datarow.invalid_count)
                datarow.from_date = from_date
                datarow.to_date = to_date
                datarow.download_date = datetime.now()
                datarow.price_type = PRICE_BIDASK
        else:
            log.error('datarow not found: %d' % id)
    except:
        log.error(traceback.format_exc())
    finally:
        if session:
            if datarow:
                datarow.status = 'Done'
                log.info('cleanup complete. datarow %d. contains %d invaild of total %d candle from %s to %s' % (datarow.id, datarow.invalid_count, datarow.bar_count, str(datarow.from_date), str(datarow.to_date)))
            session.commit()
            session.close()

def cleanup_db():
    try:
        session = db.Session()
        q = session.query(DatarowDescriptor.id).filter(DatarowDescriptor.pluginId == DukascopyDownloader.ID)
        for id, in q.all():
            cleanup_datarow(id)
    except:
        log.error(traceback.format_exc())
    finally:
        if session:
            session.close()


def initialize_datarow_descriptors():
    plugin_id = 1
    config_id = 1
    session = db.Session()
    for configrow in session.query(db.HistoricPluginsConfigsInstrument).order_by(db.HistoricPluginsConfigsInstrument.id).all():
        try:
            datarow_id = first0(session.query(db.DatarowDescriptor.id).filter(db.DatarowDescriptor.configRowId == configrow.id))
            if not datarow_id:
                start_date = configrow.available_from
                end_date = date.today()
                from_date, to_date, candle_num = check_datafiles(start_date, end_date, configrow.symbol, configrow.timeframe)
                if from_date and to_date:
                    log.info('Creating datarow %s %s' % (configrow.symbol, TIMEFRAME[configrow.timeframe]))
                    datarow = db.DatarowDescriptor(plugin_id, config_id, configrow.id, configrow.symbol, configrow.timeframe,
                                                   from_date, to_date, datetime.now(), PRICE_BIDASK, 'Done', 'imported succesfully', candle_num, 0)
                    session.add(datarow)
                    session.commit()
            #else:
            #    cleanup_datarow(datarow_id)
        except:
            log.error('error during datarow import: %s' + traceback.format_exc())

    session.close()


class DukascopyDownloader(threading.Thread):
    '''
    Historikus adatok letöltését végzi a Dukascopytól.
     
    '''
    ID = 1
    log = logging.getLogger('gdtlive.DukascopyDownloader')
    name = 'Dukascopy'
    storage_path = STORAGE_PATH
    base_url = 'http://www.dukascopy.com/datafeed/'
    ITERATOR = {1 : iterdays,
                    5 : iterdays,
                    15 : iterdays,
                    30 : iterdays,
                    60 : itermonths,
                    240 : itermonths,
                    1440 : itermonths,
                    10080: iteryear,
                    302400: iteryear}

    def __init__(self, descriptor_id = 0):
        threading.Thread.__init__(self)
        self.log.info('initialized with datarow id %d' % descriptor_id)
        self.descriptor_id = descriptor_id
        self.valid_candles = 0
        self.invalid_candles = 0
        self.status = ''
        self.datarow = None
        self.stop = threading.Event()
        self.filenames = {}
        self.session = None
        self.fromdate_modified = True
        self.todate_modified = True

    def run(self):
        self.log.info('thread started')
        try:
            self.session = db.Session()
            if self.loadDatarowDescriptor():
                try:
                    self.download_candle_files()
                    self.import_files()
                    self.remove_files()
                except Exception, e:
                    self.log.error(traceback.format_exc())
                finally:
                    cleanup_datarow(self.datarow.id)
                    pass
        except Exception, e:
            self.log.error(str(e))
        finally:
            if self.session:
                self.session.close()

    def loadDatarowDescriptor(self):
        if self.descriptor_id:
            self.datarow = self.session.query(DatarowDescriptor).get(self.descriptor_id)

        if not self.datarow:
            self.log.error('no DatarowDescriptor found for id %d' % self.descriptor_id)
            return False

        self.start_date = self.datarow.from_date.date()
        self.end_date = self.datarow.to_date.date()

        first = self.session.query(DatarowData).filter(and_(DatarowData.datarowId == self.datarow.id, DatarowData.time == self.datarow.from_date)).first()
        last = self.session.query(DatarowData).filter(and_(DatarowData.datarowId == self.datarow.id, DatarowData.time == self.datarow.to_date)).first()

        self.fromdate_modified = True if not first else False
        self.todate_modified = True if not last else False
        self.log.info('from date modified : %s' % self.fromdate_modified)
        self.log.info('to date modified : %s' % self.todate_modified)
        self.datarow.status_text = ''
        return True

    def halt(self):
        self.log.info('halt command received')
        self.stop.set()

    def create_url(self, t, ptype):
        return self.base_url + create_path(self.datarow.symbol, t, self.datarow.timeframe_num, ptype)

    def create_filename(self, t, symbol, timeframe_num, ptype):
        return normpath(self.storage_path + os.sep + create_path(symbol, t, timeframe_num, ptype))

    def update_status(self, str, current, total):
        if self.datarow:
            if total:
                percent = (float(current) / total) * 100
                self.datarow.status = "%s %.0f%%" % (str, percent)
                self.session.commit()

    def download_candle_files(self):
        self.log.info('downloading started')
        sequences = self.needs_to_download([d for d in self.ITERATOR[self.datarow.timeframe_num](self.start_date, self.end_date)])
        #sequences = [d for d in self.ITERATOR[self.datarow.timeframe_num](self.start_date, self.end_date)]
        self.log.info('after needs_to_download')
        seq_len = len(sequences)
        self.filenames = []
        self.datarow.status_text += 'Downloading<br>'
        for i in range(seq_len):
            seq = sequences[i]
            filename = {'seq' : seq}
            for ptype in PRICE_ASK, PRICE_BID:
                url = self.create_url(sequences[i], ptype)
                target_file = self.create_filename(sequences[i], self.datarow.symbol, self.datarow.timeframe_num, ptype)
                result = True
                if not os.path.exists(target_file):
                    result = download_file(url, target_file)
                if result:
                    filename[ptype] = target_file
                    self.log.info('Downloaded: ' + url)
                    #self.datarow.status_text += 'Downloaded: ' + url + '<br>'
                    self.datarow.status_text += '.'
                else:
                    filename[ptype] = None
                    self.datarow.status_text += '<br>Failed to download: ' + url + '<br>'
            self.filenames.append(filename)

            self.update_status('Downloading', i, seq_len)
            if self.stop.is_set():
                self.log.info('stop downloading')
                break

        self.update_status('Verifying', seq_len, seq_len)

        self.cleanup_filenames()

        # Amelyik file nem jött le rendesen, azt itt újra megpróbáljuk
        for file in self.filenames:
            for ptype in PRICE_ASK, PRICE_BID:
                if file[ptype] == None:
                    url = self.create_url(file['seq'], ptype)
                    target_file = self.create_filename(sequences[i], self.datarow.symbol, self.datarow.timeframe_num, ptype)
                    self.datarow.status_text += 'try again: ' + url + ' '
                    self.session.commit()
                    result = download_file(url, target_file)
                    if result:
                        file[ptype] = target_file
                        self.datarow.status_text += ' OK<br>'
                    else:
                        file[ptype] = None
                        self.datarow.status_text += ' Failed again<br>'
                    self.session.commit()

        self.update_status('Downloaded', seq_len, seq_len)

    def cleanup_filenames(self):
        if not len(self.filenames):
            return
        #print 'before ', self.filenames     
        while len(self.filenames) and self.filenames[0][PRICE_ASK] == None and self.filenames[0][PRICE_BID] == None:
            del self.filenames[0]
        if len(self.filenames) > 0:
            last = len(self.filenames) - 1
            while self.filenames[last][PRICE_ASK] == None and self.filenames[last][PRICE_BID] == None:
                del self.filenames[last]
                if len(self.filenames) > 0:
                    last = len(self.filenames) - 1
                else:
                    break
        #print 'after ', self.filenames


#    def import_files(self):   
#        if self.stop.is_set():
#            return 
#        self.log.info('importing candles to database started')             
#        sequences = self.needs_to_download([d for d in self.ITERATOR[self.datarow.timeframe_num](self.start_date, self.end_date)])        
#        seq_len = len(sequences)        
#        for i in range(seq_len):                
#            ask_candles = self.load_candles(self.filenames[i][PRICE_ASK], sequences[i])
#            bid_candles = self.load_candles(self.filenames[i][PRICE_BID], sequences[i])
#            candles = merge_candles(ask_candles, bid_candles)
#            candles = filter_candles(self.start_date, self.end_date, candles)
#            self.save_to_db(candles)
#            self.update_status('Processing', i, seq_len)
#        
#        self.update_status('Processed', seq_len, seq_len)        

    def import_files(self):
        if self.stop.is_set():
            return
        self.log.info('converting datarow started')
        i = 0
        seq_len = len(self.filenames)
        self.log.info('Converting %d files<br>' % seq_len)
        self.datarow.status_text += 'Converting %d files' % seq_len
        for file in self.filenames:
            ask_candles = self.load_candles(file[PRICE_ASK], file['seq'])
            bid_candles = self.load_candles(file[PRICE_BID], file['seq'])
            if ask_candles and bid_candles:
                candles = merge_candles(ask_candles, bid_candles)
                save_candles(candles, file, self.datarow.timeframe_num)
            #candles = filter_candles(self.start_date, self.end_date, candles)            
            #self.save_to_db(candles)
            self.datarow.status_text += '.'
            self.update_status('Converting', i, seq_len)
            i += 1

        #self.datarow.status_text += '<br>'
        #self.update_status('Converting', seq_len, seq_len)
        self.datarow.status = 'Done'
        self.datarow.status_text += 'Done.'
        self.session.commit()


    def load_candles(self, filename, sequence):
        self.log.info('processing file %s' % filename)
        if filename:
            candle_data = load_candles(filename)
            if len(candle_data) == 0 :
                return None
            return candle_data
        else:
            return None


    def save_to_db(self, candles):
        if len(candles) == 0:
            self.log.error('no data to save!')
            return False
        try:
            insert_data = []
            first_candle = min(candles, key = lambda c: c.time).time
            last_candle = max(candles, key = lambda c: c.time).time
            datarow = DatarowData.__table__

            for candle in candles:
                    insert_data.append({'datarowId':self.datarow.id,
                                        'time':candle.time,
                                        'volume':candle.volume,
                                        'ask_open': candle.ask_open,
                                        'ask_high':candle.ask_high,
                                        'ask_low':candle.ask_low,
                                        'ask_close':candle.ask_close,
                                        'bid_open': candle.bid_open,
                                        'bid_high':candle.bid_high,
                                        'bid_low':candle.bid_low,
                                        'bid_close':candle.bid_close,
                                        'invalid':not candle.valid})

            if self.valid_candles > 0 :
                #print 'deleting from ', first_candle, ' to ', last_candle                
                db.engine.execute(datarow.delete().where(and_(datarow.c.time.between(first_candle, last_candle), datarow.c.datarowId == self.datarow.id)))

            try:
                #print 'inserting from ', first_candle, ' to ', last_candle
                self.log.info('saving new data beetween %s - %s' % (str(first_candle), str(last_candle)))
                db.engine.execute(datarow.insert(), insert_data)
            except IntegrityError, e:
                print e
                pass

        except Exception, e:
            self.log.info('error occured while saving!')
            self.log.error(str(e))
            return False


        #FIXME: ha ezek kiker-lek, akkor lehal a testProcessMultipleFiles, de hogy mi a faszért, azt nem tudom 
        self.valid_candles = self.session.query(DatarowData).filter(DatarowData.datarowId == self.datarow.id).filter(DatarowData.invalid == False).count()
        self.invalid_candles = self.session.query(DatarowData).filter(DatarowData.datarowId == self.datarow.id).filter(DatarowData.invalid == True).count()
        self.log.info('datarow contains %d valid and %d invalid candles' % (self.valid_candles, self.invalid_candles))
        return True

    def completly_in_db(self, seq):
        first = first_time(seq, self.datarow.timeframe_num)
        last = last_time(seq, self.datarow.timeframe_num)
        candles = candle_num(seq, self.datarow.timeframe_num)
        query = self.session.query(DatarowData.invalid).filter(and_(DatarowData.time >= first, DatarowData.time <= last, DatarowData.datarowId == self.datarow.id))
        #print first, last, candles, query.count()  
        return query.count() == candles

    def is_file_downloaded(self, seq):
        filename = create_historic_filename(self.datarow.symbol, seq, self.datarow.timeframe_num)
        #print 'downloaded ? ', filename, os.path.exists(filename)  
        return os.path.exists(filename)

    def needs_to_download(self, sequences):
        #print 'needs_to_download:', sequences, self.fromdate_modified, self.todate_modified
        first = sequences[0]
        last = sequences[-1]
        filtered = []
        seq_len = len(sequences)
        for n, seq in enumerate(sequences):
            self.update_status('Checking dowloaded files...', n, seq_len)
            needs = not self.is_file_downloaded(seq)
            #needs = not self.completly_in_db(seq)
            #print seq, needs
#            if seq == first and not self.fromdate_modified:
#                needs = False
#            if seq == last and not self.todate_modified:
#                needs = False
            #print 'Needs to download: ', needs, seq        
            if needs:
                filtered.append(seq)
        self.update_status('Checking db...', seq_len, seq_len)
        return filtered


    def remove_files(self):
        self.log.info('deleting files')
        for file in self.filenames:
            for ptype in PRICE_ASK, PRICE_BID:
                if file[ptype]:
                    try:
                        os.remove(file[ptype])
                    except Exception, e:
                        self.log.error('deleting file %s failed. Error : %s' % (file[ptype], e))


    def test_instrument(self, symbol, timeframe_num, from_date):
        result = None
        try:
            seq = self.ITERATOR[timeframe_num](from_date, from_date).next()
            url = self.base_url + create_path(symbol, seq, timeframe_num, PRICE_ASK)
            target_file = self.create_filename(seq, symbol, timeframe_num, PRICE_ASK)
            result = download_file(url, target_file)
            if result:
                result = load_candles(target_file)
        except:
            print traceback.format_exc()
            pass
        finally:
            try:
                os.remove(target_file)
            except:
                pass

        return result


if __name__ == '__main__':
    
    result = load_datarow('EURUSD', 1440, datetime(2013,1,1), datetime(2013,7,20))
    print result
    
    quit()
    result = load_historic_file('/var/spool/gdt/historic/EURUSD/2004/09/01/candles_min_15.bin')
    for r in result:
        print r
    
    
    