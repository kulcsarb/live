# -*- encoding: utf-8 -*- 
'''
Historikus adatsorok kezelését végző web szervizek. 

A modul feladata kiszolgálni a hitorikus adatok kezelését végző képernyőt (HistoricDatarowManagementUi), 
biztositani annak az adatsorok információinak lekérését, az egyes adatosorok OHLC adatainak lekérését, 
valamint az megadott adatsor törlését.

@author: kulcsarb
'''
from gdtlive.store.db import DatarowDescriptor, DatarowData, HistoricPluginsConfigsInstrument
from gdtlive.languages.default import EXCEPTION_OCCURED, OK, NOT_FOUND
from gdtlive.utils import json_encode, first0, str2date, str2datetime#, datetime2str
from sqlalchemy import select, and_#, func, not_
from gdtlive.historic import prepareDescriptor
import gdtlive.store.db as db
import traceback, logging
from gdtlive.admin.system.log import errorlog, extra
from gdtlive.constants import PRICE_TIME, PRICE_ASKOPEN, PRICE_ASKHIGH, PRICE_ASKLOW, PRICE_ASKCLOSE, PRICE_BIDOPEN, PRICE_BIDHIGH, PRICE_BIDLOW, PRICE_BIDCLOSE, PRICE_VOLUME
from gdtlive.config import HOLIDAY_LIMIT_HOURS
import cherrypy
from gdtlive.control.router import ajax_wrapper, store_wrapper
#import time
from datetime import date, datetime, timedelta
from datetime import time as datetime_time

log = logging.getLogger('gdtlive.historic')

@cherrypy.expose
@ajax_wrapper
def list(start = 0, limit = 0):
    '''
    Visszaadja a rendszerben elérhető összes adatsor leírót, a hozzájuk tartozó elérhetőség kezdő dátummal (HISTORIC_PLUGINS_CONFIGS_INSTRUMENTS.available_from)

    @url:       /datarow/list    
    @rtype:     (bool, str, list(dict)) tuple 
    @return:    A művelet sikerességét jelző tuple, aminek utolsó tagja az adatsor leirók név-érték szótárának listája
    '''
    session = db.Session()

    try:
        datarows = [dict(i) for i in session.query(DatarowDescriptor).all()]
        for datarow in datarows:
            available_from = first0(session.query(HistoricPluginsConfigsInstrument.available_from).filter(HistoricPluginsConfigsInstrument.id == datarow['configRowId']))
            prepareDescriptor(datarow, available_from)
    except:
        errorlog.error('Exception raised during historic list', extra = extra(globals = globals(), locals = locals()), exc_info = traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        session.close()
    return True, OK, datarows

#@cherrypy.expose
#@ajax_wrapper
#def getStart(id, fromTime = None):
#    '''
#    Visszaadja, hogy mekkora az eltérés a lekért adatsor első indexe és a lekérés kezdete közt
#
#    @url:      /datarow/getStart
#
#    @type:     id: int
#    @param     id: Az adatsor datarowId-ja (referencia a DatarowData.datarowId mezőre)
#    @type:     fromTime: datetime
#    @param     fromTime: Az adatsorból a visszaadandó időintervallum kezdete
#
#    @rtype:    (bool, str, date) tuple
#    @return:    Állapotjelzés, szöveges üzenet, az eltérés
#    '''
#    from gdtlive.historic.plugin.dukascopy_fs import load_datarow_first
#    if fromTime:
#        fromTime = str2date(fromTime)
#    session = db.Session()
#    try:
#        datarow = session.query(db.DatarowDescriptor).get(id)
#        start_date = datarow.from_date.date()
#        if fromTime > start_date:
#            start_date = fromTime
#        result = load_datarow_first(datarow.symbol, datarow.timeframe_num, fromTime)

##!!!kéretik ezzel együtt frissíteni a load függvény szűrőjét is!!!
#        qry = session.query(func.min(DatarowData.time)).filter(DatarowData.datarowId==id).filter(not_(and_(DatarowData.ask_open==DatarowData.ask_high, DatarowData.ask_high==DatarowData.ask_low, DatarowData.ask_low==DatarowData.ask_close)))
#        if fromTime:
##            az előző dátumos megoldás, ami a hétvégéket szűri
##            t = str2datetime(fromTime)
##            weekday = (int(time.strftime("%w", t)) - 1) % 7 #0 = Monday
##            hour = int(time.strftime("%H", t))
##            diff = ((6 - weekday) * 24 + (23 - hour)) % (7 * 24)
##            if diff <= 48:
##                t = time.localtime(time.mktime(t) + diff * 60 * 60)
##                fromTime = time.strftime('%Y-%m-%dT%H:%M:%S', t)
#            qry = qry.filter(DatarowData.time>=fromTime)
#
#        result = first0(qry)
#        if result:
#            return True, OK, result.strftime('%Y-%m-%dT%H:%M:%S')
#        else:
#            return False, NOT_FOUND + 'datarow'
#    except:
#        errorlog.error('Exception raised during historic getStart', extra = extra(globals = globals(), locals = locals()), exc_info = traceback.format_exc())
#        return False, EXCEPTION_OCCURED
#    finally:
#        session.close()


@cherrypy.expose
def get(id, fromTime = None, toTime = None):
    '''       
    Visszaadja az adott adatsor összes árfolyamadatát adott időszak között

    @url:      /datarow/get

    @type:     id: int
    @param     id: Az adatsor datarowId-ja (referencia a DatarowData.datarowId mezőre)
    @type:     fromTime: datetime
    @param     fromTime: Az adatsorból a visszaadandó időintervallum kezdete
    @type:     toTime: datetime
    @param     toTime: Az adatsorból a visszaadandó időintervallum vége
    
    @rtype:    str
    @return:   Az adott adatsor összes árfolyamadatának adott időszak közötti listája JSON kódolásban. 
               [['time', open, high, low, close], ... ]     
    '''
    #session = db.Session()
    result = []
    fromTime_datetime = str2datetime(fromTime)
    toTime_datetime = str2datetime(toTime)
    try:
        candles = load(id, fromTime_datetime, toTime_datetime)
        candles = extend(id, fromTime_datetime, toTime_datetime, candles)
#        print fromTime, candles[PRICE_TIME]
        for i in xrange(len(candles[PRICE_TIME])):
            if candles[PRICE_TIME][i] < fromTime_datetime:
#                print 'candles[PRICE_TIME][i](', candles[PRICE_TIME][i], ') < fromTime_datetime(', fromTime_datetime
                continue
            if candles[PRICE_TIME][i] > toTime_datetime:
#                print 'candles[PRICE_TIME][i](', candles[PRICE_TIME][i], ') > toTime_datetime(', toTime_datetime
                break
            result.append([candles[PRICE_TIME][i].strftime('%Y-%m-%d %H:%M:%S'), candles[PRICE_ASKOPEN][i], candles[PRICE_ASKHIGH][i], candles[PRICE_ASKLOW][i], candles[PRICE_ASKCLOSE][i]])
#            result.append([candles[PRICE_TIME][i].strftime('%Y-%m-%d %H:%M:%S'), candles[PRICE_BIDOPEN][i], candles[PRICE_BIDHIGH][i], candles[PRICE_BIDLOW][i], candles[PRICE_BIDCLOSE][i]])
#        for candle in candles:
#            result.append([candle[PRICE_TIME].strftime('%Y-%m-%d %H:%M:%S'), candle[PRICE_ASKOPEN], candle[PRICE_ASKHIGH], candle[PRICE_ASKLOW], candle[PRICE_ASKCLOSE]])
#        query = session.query(DatarowData).filter(DatarowData.datarowId==id).filter(DatarowData.time>=fromTime).filter(DatarowData.time<toTime)    
#        for candle in query.all():
#            if not ((candle.time.weekday() == 4 and candle.time.hour >= 23) or 
#                    (candle.time.weekday() == 5) or
#                    (candle.time.weekday() == 6 and candle.time.hour < 23)):
#                result.append(candle.as_tohlc())
    except:
        errorlog.error('Exception raised during historic get', extra = extra(globals = globals(), locals = locals()), exc_info = traceback.format_exc())
        return False, EXCEPTION_OCCURED
    finally:
        pass
        #session.close()
    return json_encode(result)

#@cherrypy.expose
#@store_wrapper
#def delete(id):
#    '''
#    Törli a megadott azonositójú adatsorleírót és a datarowId-n keresztül hozzá kapcsolódó
#    adatsort
#
#    @url:      /datarow/delete
#
#    @type id:  int
#    @param id: datarowDescriptor.id (=datarowdata.datarowId) adatsorleíró azonositó
#    
#    @rtype:  (bool, str) tuple
#    @return: Sikeres törlés esetén (True, 'OK'), hiba esetén (False, "hibaüzenet", {"id": id})  
#    '''
##    from sqlalchemy.exc import IntegrityError as IntegrityError
#    session = db.Session()
#    try:
#        id = int(id)
#        confs = session.query(db.EvolConfig).all()
#        for conf in confs:
#            for descr in conf.datarows:
#                if descr.id == id:
#                    log.error('Error deleting datarow(%d) : cannot delete DatarowDescriptor associated to an EvolConfig' % id)
#                    return (False, "DatarowData cannot be deleted, the connected DatarowDescriptor is associated to an EvolConfig!", {"id":id})
#        session.query(DatarowData).filter(DatarowData.datarowId == id).delete()
#        session.query(DatarowDescriptor).filter(DatarowDescriptor.id == id).delete()
#        session.commit()
#        log.info('Deleted datarow: %d' % id)
##    except IntegrityError, e:
##        session.rollback()
##        log.error('Error deleting datarow(%d) : %s , cannot delet DatarowDescriptor associated to an EvolConfig' % (id, str(e)))
##        return (False,traceback.format_exc(),{"id":id})         
#    except Exception, e:
#        session.rollback()
#        errorlog.error('Exception raised during historic datarow delete', extra = extra(globals = globals(), locals = locals()), exc_info = traceback.format_exc())
#        return False, EXCEPTION_OCCURED
#    finally:
#        session.close()
#    return (True, OK)


def load(datarow_id, from_date, to_date):
    from gdtlive.historic.plugin.dukascopy_fs import load_datarow
    result = {
                  PRICE_TIME : [],
                  PRICE_ASKOPEN : [],
                  PRICE_ASKHIGH : [],
                  PRICE_ASKLOW : [],
                  PRICE_ASKCLOSE : [],
                  PRICE_BIDOPEN : [],
                  PRICE_BIDHIGH : [],
                  PRICE_BIDLOW : [],
                  PRICE_BIDCLOSE : [],
                  PRICE_VOLUME : []
              }
            
    session = db.Session()
    if type(from_date) == date:
        from_date = datetime.combine(from_date, datetime_time.min)
    if type(to_date) == date:
        to_date = datetime.combine(to_date, datetime_time.min)
    
    try:        
        datarow = session.query(db.DatarowDescriptor).get(datarow_id)                        

        from_date = from_date if from_date > datarow.from_date else datarow.from_date
        to_date = to_date if to_date < datarow.to_date else datarow.to_date
        
        if from_date > to_date:
            candles = []
        else:
            candles = load_datarow(datarow.symbol, datarow.timeframe_num, from_date, to_date)
                        
        tempTime = []   #list of the time values where volume == 0 one after each other
        tempAsk = 0     #the ask value for these times
        tempBid = 0     #the bid value for these times
        holidayDiff = timedelta(hours = HOLIDAY_LIMIT_HOURS)    #the maximum time that is not a holiday
        for row in candles:
            t = datetime.utcfromtimestamp(row[0])
            if t < from_date or (t > to_date and datarow.timeframe_num >= 1440) or (t >= to_date and datarow.timeframe_num < 1440):    #before or after the requested date
                continue                        
            if row[1] == row[2] and row[2] == row[3] and row[3] == row[4]:                
                if len(tempTime) == 0:  #set ask and bid for the first empty candle
                    tempAsk = row[1]
                    tempBid = row[5]
                tempTime.append(t)  #add time value to the list
            else:               #not empty candle
                #the space was too short for a holiday, add missing values 
                if len(tempTime) and t - tempTime[0] < holidayDiff:
                    result[PRICE_TIME] += tempTime
                    result[PRICE_ASKOPEN] += [tempAsk] * len(tempTime)
                    result[PRICE_ASKHIGH] += [tempAsk] * len(tempTime)
                    result[PRICE_ASKLOW] += [tempAsk] * len(tempTime)
                    result[PRICE_ASKCLOSE] += [tempAsk] * len(tempTime)
                    result[PRICE_BIDOPEN] += [tempBid] * len(tempTime)
                    result[PRICE_BIDHIGH] += [tempBid] * len(tempTime)
                    result[PRICE_BIDLOW] += [tempBid] * len(tempTime)
                    result[PRICE_BIDCLOSE] += [tempBid] * len(tempTime)
                    result[PRICE_VOLUME] += [0] * len(tempTime)
                tempTime = []
                #add new value
                result[PRICE_TIME].append(t)
                result[PRICE_ASKOPEN].append(row[1])
                result[PRICE_ASKHIGH].append(row[2])
                result[PRICE_ASKLOW].append(row[3])
                result[PRICE_ASKCLOSE].append(row[4])
                result[PRICE_BIDOPEN].append(row[5])
                result[PRICE_BIDHIGH].append(row[6])
                result[PRICE_BIDLOW].append(row[7])
                result[PRICE_BIDCLOSE].append(row[8])
                result[PRICE_VOLUME].append(row[9])
    except:        
        print traceback.format_exc()
        errorlog.error('Exception raised during datarow loading', extra = extra(globals = {}, locals = locals()), exc_info = traceback.format_exc())
    finally:
        session.close()
    return result


def extend(datarow_id, from_date, to_date, candles):
    import gdtlive.core.datafeed.historic as historic
    session = db.Session()
    
    try:
        datarow = session.query(db.DatarowDescriptor).get(datarow_id)
        timeframe = datarow.timeframe_num
        symbol = datarow.symbol
        
        try:
            if not len(candles[PRICE_TIME]):
                i = historic.DATAROW[timeframe][symbol][PRICE_TIME].index(from_date)
            else:
                i = historic.DATAROW[timeframe][symbol][PRICE_TIME].index(candles[PRICE_TIME][-1])
        except KeyError:            
            return candles
    
        if i:
            while i < len(historic.DATAROW[timeframe][symbol][PRICE_TIME]) and historic.DATAROW[timeframe][symbol][PRICE_TIME][i] <= to_date:                    
                for price in candles.keys():
                    candles[price].append(historic.DATAROW[timeframe][symbol][price][i])
                #print 'adding', candles[PRICE_TIME][-1]                                        
                i += 1
                            
    except ValueError:
        log.error('from_date %s not in historic.DATAROW' % from_date)
        pass
    except:
        log.error(traceback.format_exc())
    finally:
        session.close()
        
    return candles


#depricated
def load_sql(datarow_id, from_date, to_date):
    result = {
                  PRICE_TIME : [],
                  PRICE_ASKOPEN : [],
                  PRICE_ASKHIGH : [],
                  PRICE_ASKLOW : [],
                  PRICE_ASKCLOSE : [],
                  PRICE_BIDOPEN : [],
                  PRICE_BIDHIGH : [],
                  PRICE_BIDLOW : [],
                  PRICE_BIDCLOSE : [],
                  PRICE_VOLUME : []
                  }


    candle_table = db.DatarowData.__table__
    where = and_(candle_table.c.datarowId == datarow_id, candle_table.c.time >= from_date, candle_table.c.time < to_date)

    candle_data = db.engine.execute(select([candle_table], where).order_by(candle_table.c.time.asc()))
    for row in candle_data:
#!!!kéretik ezzel együtt frissíteni a getStart függvény szűrőjét is!!!
        if not ((row[2] == row[3]) and (row[3] == row[4]) and (row[4] == row[5])):
            result[PRICE_TIME].append(row[11])
            result[PRICE_ASKOPEN].append(row[2])
            result[PRICE_ASKHIGH].append(row[3])
            result[PRICE_ASKLOW].append(row[4])
            result[PRICE_ASKCLOSE].append(row[5])
            result[PRICE_BIDOPEN].append(row[6])
            result[PRICE_BIDHIGH].append(row[7])
            result[PRICE_BIDLOW].append(row[8])
            result[PRICE_BIDCLOSE].append(row[9])
            result[PRICE_VOLUME].append(row[10])

    return result
