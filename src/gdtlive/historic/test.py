import logging
import gdtlive.store.db as db
from datarow import load
from plugin.dukascopy_fs import initialize_datarow_descriptors
from gdtlive.utils import str2datetime

errorlog = logging.getLogger('errors.main')
log = logging.getLogger('gdtlive.main')    

db.init()
initialize_datarow_descriptors()

datarow_id = 12;
from_date = str2datetime('2013-01-29T09:18:00')
to_date = str2datetime('2013-01-29T09:47:00')
data = load(datarow_id, from_date, to_date)[0]
print data
#for i in data:
#    print i