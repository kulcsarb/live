# -*- encoding: utf-8 -*- 
'''
A historikus adatok letöltését kezelő csomag. 

A csomagban a historikus adatok kezelésével kapcsolatos konfigurációs, és segédfunkciók szerepelnek, 
melyeket a csomag moduljai használnak.

'''

from gdtlive.historic.plugin.dukascopy_fs import DukascopyDownloader
from gdtlive.constants import TIMEFRAME, PRICES
from gdtlive.utils import date2str

historic_plugins = {
                    1: DukascopyDownloader
                   }

        
def addPluginName(data):
    try:
        data['pluginName'] = historic_plugins[data['pluginId']].name
    except KeyError:
        data['pluginName'] = 'live' 
    return data 

def prepareDescriptor(datarow, available_from=None):
    try:
        datarow['from_date'] = datarow['from_date'].partition(' ')[0]
    except:  
        datarow['from_date'] = None
    try:
        datarow['to_date'] = datarow['to_date'].partition(' ')[0]
    except:
        datarow['to_date'] = None
    datarow['timeframe_str'] = TIMEFRAME.get(datarow['timeframe_num'], datarow['timeframe_num'])             
    datarow['price_type'] = PRICES.get(datarow['price_type'], datarow['price_type'])
    datarow['available_from'] = date2str(available_from)
    addPluginName(datarow)


def prepareConfigElement(row):
    row['available_from'] = date2str(row['available_from'])     
    row['timeframe_str'] = TIMEFRAME[row['timeframe']]
                                                                                   

