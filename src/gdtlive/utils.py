# -*- encoding: utf-8 -*-
'''
Általános segédfunkciók a szoftver moduljai számára

@status: Ellenőrizve, rendben.
@author: kulcsarb
'''
from logging.handlers import RotatingFileHandler, TimedRotatingFileHandler
from gdtlive.core.constants import LOG_FORMAT, LOG_KEEP_FILES 
from gdtlive.languages.default import DIFFERRING_PASSWORDS, WEAK_PASSWORD, OK
from collections import namedtuple
from datetime import datetime
from hashlib import md5
from _abcoll import MutableMapping
from operator import eq as _eq
from itertools import imap as _imap
import gdtlive.config as config
import httplib
import time
import traceback
import logging
import os


R = lambda f : f and round(f,5) or 0
R2 = lambda f : f and round(f,2) or 0

def remove_log_handlers(log):
    handlers = log.handlers[:]
    for h in handlers:
        log.removeHandler(h)

        
def add_log_handler(log, name, level):
    handler = TimedRotatingFileHandler(config.LOG_PATH + os.sep + name + '.' + logging.getLevelName(level) + '.log', when='midnight', interval=1, utc=True)
    #handler = RotatingFileHandler(config.LOG_PATH + os.sep + name + '.' + logging.getLevelName(level) + '.log','w+',20*1024*1024, LOG_KEEP_FILES)
    handler.setFormatter(logging.Formatter(LOG_FORMAT))
    handler.setLevel(level)        
    log.addHandler(handler)
            

def open_db_connection():
    import gdtlive.store.db as db
    retries = 3
    connection = None
    while retries:
        try:
            connection = db.engine.connect()
            connection = connection.execution_options(autocommit=True)
            print 'CONNECTION OPEN:', connection
            break            
        except:
            print traceback.format_exc()
            pass
        retries -= 1 
    return connection
    

def is_weekend(current = None):
    now = current if current else datetime.utcnow()        
    if (now.isoweekday() == 5 and now.hour >= config.WEEKEND_STARTHOUR) \
        or now.isoweekday() == 6 or now.isoweekday() == 7:
        return True        
    return False
    

def hashed(string):
    '''Előállítja a paraméterül adott string md5 hashét
    
    @type string: str
    @param string: hashelni kívánt szöveg
    
    @rtype: str
    @return: md5 hash, hexa formátumban
    '''
    hash = md5()
    hash.update(string)
    return hash.hexdigest()    

 
def isPasswordComply(password):
    '''Ellenőrzi, hogy a paraméterül adott jelszó megfelel-e a minőségi követelményeknek
    
    @type password: str
    @param password: jelszó
    @rtype: bool
    '''
    if len(password) > 0 :
        return True
    return False
    
def checkPassword(password, confirmPassword):
    '''Jelszóbeállítás esetén ellenőrzi a GUIról bejövő jelszó mezőket.
    
    Ellenőrzi a két paraméter egyezőségét, valamint hogy a jelszó megfelel-e a minőségi követelményeknek
    
    @type password: str
    @param password: jelszó
    @type confirmPassword: str
    @param confirmPassword: ellenőrző jelszó
    
    @rtype: (bool, str) tuple
    @return: A művelet sikerességét jelző boolean és szöveges érték
    '''
    if (password != confirmPassword):
        return (False,DIFFERRING_PASSWORDS)
    if (not isPasswordComply(password)):
        return (False,WEAK_PASSWORD)
    return (True,OK)


def http_statusline(code):
    '''A megadott HTTP státusz kódból szabványos HTTP státus mezőt állít elő
    
    @type code: int
    @param code: HTTP státusz kód
    
    @rtype: string    
    '''    
    try:
        return '%d %s' % (code, httplib.responses[code])
    except KeyError:
        return ''    
    
    
def str2date(str):
    try:
        d = datetime.strptime(str,'%Y-%m-%dT%H:%M:%S').date()
    except:
        d = None        
    return d


def str2datetime(str):
    try:
        d = datetime.strptime(str,'%Y-%m-%dT%H:%M:%S')
    except:
        d = None        
    return d


def date2str(d):
    if hasattr(d, 'strftime'):
        return d.strftime('%Y-%m-%d')
    else:
        return ''
    
    
def datetime2str(d):
    if hasattr(d, 'strftime'):
        return d.strftime('%Y-%m-%d %H:%M:%S')
    else:
        return ''
    
    
def json_encode(data):
    def float_repr(o):
        return '%.4f' % o
    import json.encoder
    FLOAT_REPR = json.encoder.FLOAT_REPR
    json.encoder.FLOAT_REPR = float_repr
    result = json.dumps(data)
    json.encoder.FLOAT_REPR = FLOAT_REPR
    return result
        
        
def first0(query, defValue = None):
    obj = query.first()
    if not obj:
        return defValue
    return obj[0]



class Candle(namedtuple('Candle','time open close low high volume')):
    '''Egyszerű árfolyamadatot reprezentáló osztály
    
    Az osztály tárolja a közvetlenül a fileokból felolvasott adatokat, és segédfunkciókat biztosit azok kezeléséhez
    '''
    __slots__ = ()        
    def is_valid(self):        
        if self.open == 0.0 or self.close == 0.0 or self.low == 0.0 or self.high == 0.0:
            return False
        else:
            return True
    @property
    def date(self):
        '''A gyertya time attribumának értékét adja vissza dátumként'''
        return self.time.date() #date(self.time.year, self.time.month, self.time.day)
        
    def between_date(self, begin, end):
        '''Visszaadja, hogy a gyertya a két dátum közé esik-e'''
        return self.date >= begin and self.date <= end


class AskBidCandle(namedtuple('AskBidCandle','time ask_open ask_high ask_low ask_close bid_open bid_high bid_low bid_close volume valid')):
    '''Ask és Bid árfolyamadatot együttesen tartalmazó osztály.
    
    Az osztály feladata, hogy reprezentáljon egy egységnyi árfolyamadatot, aminek mind az ASK, mind a BID ára adott,
    igy két OHLC értéket tartalmaz
    ''' 
    __slots__ = ()    
    @property
    def date(self):
        return self.time.date()
        
    def between_date(self, begin, end):
        return self.date >= begin and self.date <= end


################################################################################
### OrderedDict
################################################################################

class OrderedDict(dict, MutableMapping):
    'Dictionary that remembers insertion order'
    # An inherited dict maps keys to values.
    # The inherited dict provides __getitem__, __len__, __contains__, and get.
    # The remaining methods are order-aware.
    # Big-O running times for all methods are the same as for regular dictionaries.

    # The internal self.__map dictionary maps keys to links in a doubly linked list.
    # The circular doubly linked list starts and ends with a sentinel element.
    # The sentinel element never gets deleted (this simplifies the algorithm).
    # Each link is stored as a list of length three:  [PREV, NEXT, KEY].

    def __init__(self, *args, **kwds):
        '''Initialize an ordered dictionary.  Signature is the same as for
        regular dictionaries, but keyword arguments are not recommended
        because their insertion order is arbitrary.

        '''
        if len(args) > 1:
            raise TypeError('expected at most 1 arguments, got %d' % len(args))
        try:
            self.__root
        except AttributeError:
            self.__root = root = [None, None, None]     # sentinel node
            PREV = 0
            NEXT = 1
            root[PREV] = root[NEXT] = root
            self.__map = {}
        self.update(*args, **kwds)

    def __setitem__(self, key, value, PREV=0, NEXT=1, dict_setitem=dict.__setitem__):
        'od.__setitem__(i, y) <==> od[i]=y'
        # Setting a new item creates a new link which goes at the end of the linked
        # list, and the inherited dictionary is updated with the new key/value pair.
        if key not in self:
            root = self.__root
            last = root[PREV]
            last[NEXT] = root[PREV] = self.__map[key] = [last, root, key]
        dict_setitem(self, key, value)

    def __delitem__(self, key, PREV=0, NEXT=1, dict_delitem=dict.__delitem__):
        'od.__delitem__(y) <==> del od[y]'
        # Deleting an existing item uses self.__map to find the link which is
        # then removed by updating the links in the predecessor and successor nodes.
        dict_delitem(self, key)
        link = self.__map.pop(key)
        link_prev = link[PREV]
        link_next = link[NEXT]
        link_prev[NEXT] = link_next
        link_next[PREV] = link_prev

    def __iter__(self, NEXT=1, KEY=2):
        'od.__iter__() <==> iter(od)'
        # Traverse the linked list in order.
        root = self.__root
        curr = root[NEXT]
        while curr is not root:
            yield curr[KEY]
            curr = curr[NEXT]

    def __reversed__(self, PREV=0, KEY=2):
        'od.__reversed__() <==> reversed(od)'
        # Traverse the linked list in reverse order.
        root = self.__root
        curr = root[PREV]
        while curr is not root:
            yield curr[KEY]
            curr = curr[PREV]

    def __reduce__(self):
        'Return state information for pickling'
        items = [[k, self[k]] for k in self]
        tmp = self.__map, self.__root
        del self.__map, self.__root
        inst_dict = vars(self).copy()
        self.__map, self.__root = tmp
        if inst_dict:
            return (self.__class__, (items,), inst_dict)
        return self.__class__, (items,)

    def clear(self):
        'od.clear() -> None.  Remove all items from od.'
        try:
            for node in self.__map.itervalues():
                del node[:]
            self.__root[:] = [self.__root, self.__root, None]
            self.__map.clear()
        except AttributeError:
            pass
        dict.clear(self)

    setdefault = MutableMapping.setdefault
    update = MutableMapping.update
    pop = MutableMapping.pop
    keys = MutableMapping.keys
    values = MutableMapping.values
    items = MutableMapping.items
    iterkeys = MutableMapping.iterkeys
    itervalues = MutableMapping.itervalues
    iteritems = MutableMapping.iteritems
    __ne__ = MutableMapping.__ne__

    def popitem(self, last=True):
        '''od.popitem() -> (k, v), return and remove a (key, value) pair.
        Pairs are returned in LIFO order if last is true or FIFO order if false.

        '''
        if not self:
            raise KeyError('dictionary is empty')
        key = next(reversed(self) if last else iter(self))
        value = self.pop(key)
        return key, value

    def __repr__(self):
        'od.__repr__() <==> repr(od)'
        if not self:
            return '%s()' % (self.__class__.__name__,)
        return '%s(%r)' % (self.__class__.__name__, self.items())

    def copy(self):
        'od.copy() -> a shallow copy of od'
        return self.__class__(self)

    @classmethod
    def fromkeys(cls, iterable, value=None):
        '''OD.fromkeys(S[, v]) -> New ordered dictionary with keys from S
        and values equal to v (which defaults to None).

        '''
        d = cls()
        for key in iterable:
            d[key] = value
        return d

    def __eq__(self, other):
        '''od.__eq__(y) <==> od==y.  Comparison to another OD is order-sensitive
        while comparison to a regular mapping is order-insensitive.

        '''
        if isinstance(other, OrderedDict):
            return len(self)==len(other) and \
                   all(_imap(_eq, self.iteritems(), other.iteritems()))
        return dict.__eq__(self, other)

    def __del__(self):
        self.clear()                # eliminate cyclical references

    