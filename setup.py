# -*- encoding: utf-8 -*-
from ez_setup import use_setuptools
use_setuptools(version='0.6c11')
from setuptools import setup, find_packages
import sys
import re
import os
import os.path
import subprocess
import glob
import shutil
from socket import gethostname

VERSION = '1.0'
JS_FILE = 'version_live.js'
RESOURCES_PATH = os.path.normpath(os.path.abspath(os.path.dirname(__file__)) + '/src/gdtlive/resources')+os.path.sep 
REVISION = 'r'

JSBUILDER = 'thirdparty/jsbuilder2/JSBuilder2.jar'
JSBUILDER_CONFIG_TEMPLATE_FILE = 'gdt.jsb2.template'
JSBUILDER_CONFIG_FILE = 'gdt.jsb2'

fileinfo = os.stat(__file__)
GDT_USER = fileinfo.st_uid
GDT_GROUP = fileinfo.st_gid
PYTHON_VERSION = sys.version.split(' ',1)[0][:3]
STORAGE_PATH = '/var/spool/gdt/historic'


def revert_files():
    p = subprocess.Popen(['svn','revert',RESOURCES_PATH+JS_FILE], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    

def load_file(path):
    content = None
    f = None
    try:
        f = open(path, 'r')
        content = f.read()  
        print 'loaded: ', path
    except Exception, e:  
        print 'Nem sikerul a verzioinformációk átirása a javascript fileban'
        print e
    finally:
        if f:
            f.close()
    return content

def save_file(path, content):
    f = None
    try:
        f = open(path, 'wt+')
        f.write(content)
        print 'saved: ', path
    except Exception, e:
        print e
    finally:
        if f: 
            f.close()

def get_svn_revision_number():
    global REVISION        
    try:
        p = subprocess.Popen(['svn','info'], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        out = p.stdout.read()
    except OSError:
        print 'No SVN command line utility found. Please install it.  '
        raise OSError()
            
    m = re.search('Revision: (\d+)', out, re.M)
    if m:        
        REVISION = REVISION+m.group(1)
        print 'revision found:', REVISION
    else:
        print 'revision number not found'    
    
    
def insert_variables(content):    
    content = content.replace('<<VERSION>>', 'v'+VERSION+'-' + REVISION)    
    content = content.replace('<<PYTHON>>', 'Python '+PYTHON_VERSION)        
    content = content.replace('<<PLATFORM>>', sys.platform)
    return content
    
def prepare_js_version_info():    
    content = load_file(RESOURCES_PATH+JS_FILE)
    if content:                
        content = insert_variables(content)
        save_file(RESOURCES_PATH+JS_FILE, content)
    
    
def build_javascript_files():
     
    template = load_file(JSBUILDER_CONFIG_TEMPLATE_FILE)
    
    if not template:
        print 'JSBuilder template file not found!'
        return 
    
    print 'finding javascript files...'
    includes = []
    
    # user intarface fileok importja, ezeket kell elsonek, erre hivatkoznak a handlereket tartalmazo fileok
    for filename in glob.iglob(RESOURCES_PATH+'*.ui.js'):            
        includes.append('{"text":"","path":"%s"}' % filename.replace(RESOURCES_PATH,'') )
                
    # event handlereket definiáló fileok importja        
    for filename in glob.iglob(RESOURCES_PATH+'*.js'):
        #a kisbetüsek a Designer által generált startup fileok, az ui-kat meg az elöbb includeoltuk
        filename = filename.replace(RESOURCES_PATH,'')                    
        if not filename.islower() and not ".ui.js" in filename: 
            includes.append('{"text":"","path":"%s"}' % filename)       
     
    #egyedi, nem az Ext Designer által generált fileok 
    for filename in ['version.js','others.js','mainwindow.js']:
        includes.append('{"text":"","path":"%s"}' % filename)

    #beirjuk a template-be az összeállított json formátumú file listát                
    template = template.replace('<<JSFILES>>', ','.join(includes))
    
    print 'writing template.'
    save_file(RESOURCES_PATH+JSBUILDER_CONFIG_FILE, template)
    
    #meghivjuk a generáló programot
    print 'generating gdt-all.js'
    p = subprocess.Popen(['java','-jar',JSBUILDER,'-p',RESOURCES_PATH+JSBUILDER_CONFIG_FILE,'-d',RESOURCES_PATH], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    p.wait()    
    print p.stdout.read()
    print p.stderr.read()
    
    print 'removing JSBuilder config file'
    os.remove(RESOURCES_PATH+JSBUILDER_CONFIG_FILE)
    
                   
        
def init():
    pass
    #revert_files()
    #get_svn_revision_number()
    #prepare_js_version_info()
#     try:
#         build_javascript_files()
#     except:
#         print 'errors occured during generating javascirpt files'
#         print 'continuing build'
                

def compile_c():
    print 'compiling C codes'
    curr_dir = os.getcwd()
    
    install_path = '/usr/local/lib/python%s/dist-packages/Live-%s-py%s.egg' % (PYTHON_VERSION, VERSION, PYTHON_VERSION)     
    try:
        os.chdir(install_path + '/gdtlive/c')
    except OSError:
        print 'directory not found, compiling C sources failed.'
        return     
    try:
        p = subprocess.Popen(['python%s' % PYTHON_VERSION,'recompile-all.py'], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        p.wait()
        print p.stderr.read()
        print p.stdout.read() 
    except:
        try:
            print p.stderr.read()
            print p.stdout.read()
        except:
            pass
        print 'Failed to call recompile-all.py'
    finally:
        os.chdir(curr_dir)
    
    print 'chown install directory'
    p = subprocess.Popen(['chown','-R','%d:%d' % (GDT_USER,GDT_GROUP),install_path], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    p.wait()
    
#    print 'index-devel.html -> index.html'
#    content = load_file(install_path+'/gdt/etc/gdt.conf')
#    if content:
#        content = content.replace('index-devel.html','index.html')
#        save_file(install_path+'/gdt/etc/gdt.conf', content)
#    else:
#        print 'cant change index-devel.html to index.html in gdt.conf'
    
    
def install_configs():
    install_path = '/usr/local/lib/python%s/dist-packages/Live-%s-py%s.egg/gdt/etc' % (PYTHON_VERSION, VERSION, PYTHON_VERSION)
    
    if not os.path.exists('/etc/gdt'):
        try:
            os.mkdir('/etc/gdt')
        except:
            print 'Cannot create /etc/gdt !'
            return      
    
    for config_file in glob.glob(install_path+'/*.conf'):
        config_file = os.path.split(config_file)[1] 
        if not os.path.exists('/etc/gdt/' + config_file):
            try:                
                print 'copying %s to /etc/gdt/%s' % (config_file, config_file)
                shutil.copyfile(install_path+'/'+config_file, '/etc/gdt/'+config_file)
                #p = subprocess.Popen(['/bin/cp', '%s/%s' % (install_path, config_file), '/etc/gdt/%s' % config_file], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
                #p.wait()
                #print p.stderr.read()
                #print p.stdout.read()
            except Exception as e:
                print e
    
    custom_config_dir = './configs/%s/' % gethostname()
    if os.path.exists(custom_config_dir):
        print 'custom config directory found'                                
        for file_name in glob.glob(custom_config_dir+'*.conf'):
            try:
                file_name = os.path.split(file_name)[1]
                print 'installing ', file_name,' to gdtlive/etc'
                shutil.copyfile(custom_config_dir+file_name, install_path+'/'+file_name)
            except Exception as e:
                print e
            
    if not os.path.exists(STORAGE_PATH):
        try:
            os.makedirs(STORAGE_PATH)
            p = subprocess.Popen(['chown','-R','%d:%d' % (GDT_USER,GDT_GROUP),STORAGE_PATH], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
            p.wait()
        except:
            print 'Cannot create %s !' % STORAGE_PATH
            return
        

init()

setup(    
    name='Live',
    version = VERSION,
    
    packages = find_packages('src'),
    package_dir = {'':'src'},
    
    include_package_data = True, 
        
    entry_points = {
        'console_scripts': [
                'gdt-live=gdtlive.main:main',
                'live-eval=minilive.evaluator:main',
                'live-report=minilive.report:main',
                'live-closeall=minilive.closeall:main',
                'live-backtest=minilive.daily_backtest:main',
                'live-logintest=minilive.logintest:main',
                'live-show=minilive.show:main',
                'live-refresh=minilive.refresh:main',
                'live-transfer=minilive.transfer:main'
                ]
        },
        
    install_requires = [                        
                        'cherrypy>=3.2.0',
                        'cython>=0.14',
                        'matplotlib>=1.0.1',
                        'mako>=0.5',
                        'nose>=1.0.0',                        
                        'numpy>=1.5.1',
                        'SQLAlchemy>=0.7.2',
                        #'psycopg2',                        
                        ],    
    )


compile_c()

install_configs()

